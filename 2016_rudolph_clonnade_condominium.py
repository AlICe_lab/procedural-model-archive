# SPDX-FileCopyrightText: 2016 Corentin Coulon
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *

import math
from math import radians
from math import cos
from math import sin


bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["Circulations_verticales_duplex"].select = False
bpy.ops.object.delete()

#####################################################################################################
#           INPUTS
#####################################################################################################

etage = randint(20, 110)
# etage = 100
nbr_entraxe_x = 6
nbr_entraxe_y = 5
rez_vide = [3, 4, 6]

#####################################################################################################

#####################################################################################################


def colonne(location, etage):

    x, y, z = location

    hauteur = 1.5 * etage

    bpy.ops.mesh.primitive_cylinder_add(
        radius=0.5, location=(x, y, z + hauteur), vertices=150
    )

    bpy.ops.transform.resize(value=(0.8, 0.8, hauteur))


trame = []

for ix in range(0, nbr_entraxe_x):
    for iy in range(0, nbr_entraxe_y):

        x = ix * 5 + 4
        y = iy * 4 + 4

        trame.append((x, y, 0))
        trame.append((x, -y, 0))
        trame.append((-x, y, 0))
        trame.append((-x, -y, 0))

print(trame)


#######################################################################################
# DEFINITION BOITE, DALLE, MUR (duplex), CIRCULATIONS
#######################################################################################


def boite(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def dalle(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def mur(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


##### def circulations verticales generales#####


def ascenseurs(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def escaliers_boites(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def escaliers_cylindres(position, dimension):
    bpy.ops.mesh.primitive_cylinder_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def dalle(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def circulations_verticales_generales(location):

    x, y, z = location

    ascenseurs((x + 0, y + 3.75, chaque_etage * 3 + 1.5), (7.5, 4.5, 3))
    ascenseurs((x + 0, y - 3.75, chaque_etage * 3 + 1.5), (7.5, 4.5, 3))
    escaliers_boites((x + 24.75, y + 0, chaque_etage * 3 + 1.5), (5, 3, 3))
    escaliers_boites((x - 24.75, y + 0, chaque_etage * 3 + 1.5), (5, 3, 3))
    escaliers_cylindres((x + 27.25, y + 0, chaque_etage * 3), (3, 3, 2.7))
    escaliers_cylindres((x - 27.25, y + 0, chaque_etage * 3), (3, 3, 2.7))
    dalle((x * 22.25, y * 1.5, chaque_etage * 3), (44.5, 3, 0.2))


##### def circulations verticales duplex #####


def circulations_verticales_duplex(position):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Circulations_verticales_duplex"].select = True
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": True, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (position)},
    )


#######################################################################################
# QUARTIERS
#######################################################################################

#####colonnes####

for position in trame:
    colonne(position, etage)

#####étages#####

typeComposition = ["sauvage", "randomdensite", "stricte"]
typeCompositionChoisi = choice(typeComposition)


listquadrant = ["NE", "SE", "NO", "SO"]

for quadrant in listquadrant:
    if quadrant == "NE":
        x = 1
        y = 1

    if quadrant == "SE":
        x = 1
        y = -1

    if quadrant == "NO":
        x = -1
        y = 1

    if quadrant == "SO":
        x = -1
        y = -1

    chaque_etage = 0

    #####Définitions des types de composition#####

    while chaque_etage <= etage:
        circulations_verticales_generales((0, 0, chaque_etage * 3))
        typepossible = ["vide", "simplex", "duplex"]
        print(typeCompositionChoisi)

        # Composition possible #1

        if typeCompositionChoisi == "stricte":
            if quadrant == "NE":
                choixdutype = "vide"
            if quadrant == "SE":
                choixdutype = "simplex"
            if quadrant == "NO":
                choixdutype = "duplex"
            if quadrant == "SO":
                choixdutype = "vide"

            print("etage en cours de creation : ", chaque_etage)
            print(choixdutype)

        # Composition possible #2

        if typeCompositionChoisi == "sauvage":
            choixdutype = choice(typepossible)

            print("etage en cours de creation : ", chaque_etage)
            print(choixdutype)

        # Composition possible #3

        if typeCompositionChoisi == "unique":
            if etage >= 90:
                choixdutype = "simplex"
            if 70 >= etage < 90:
                choixdutype = "vide"
            if 55 >= etage < 70:
                choixdutype = "simplex"
            if 30 >= etage < 55:
                choixdutype = "vide"
            if 14 >= etage < 30:
                choixdutype = "duplex"
            if etage < 14:
                choixdutype = "vide"

        # Composition possible #4

        if typeCompositionChoisi == "strates":
            if chaque_etage > 90:
                choixdutype = "simplex"
            if 55 >= chaque_etage < 90:
                choixdutype = "vide"
            if 35 >= chaque_etage < 55:
                choixdutype = "duplex"
            if chaque_etage < 35:
                choixdutype = "vide"

        # Composition possible #5

        if typeCompositionChoisi == "randomdensite":
            if chaque_etage >= 90:
                typepossible_densite_minimale = list(typepossible)
                typepossible_densite_minimale.extend(
                    ["vide", "vide", "vide", "vide", "vide", "vide", "simplex"]
                )
                choixdutype = choice(typepossible_densite_minimale)

            if 75 >= chaque_etage < 90:
                typepossible_densite_maximale = list(typepossible)
                typepossible_densite_maximale.extend(
                    [
                        "simplex",
                        "simplex",
                        "simplex",
                        "vide",
                        "duplex",
                        "duplex",
                        "duplex",
                    ]
                )  # typepossible = typepossible_densite_maximale
                choixdutype = choice(typepossible_densite_maximale)

            if 60 >= chaque_etage < 75:
                typepossible_densite_minimale = list(typepossible)
                typepossible_densite_minimale.extend(
                    ["vide", "vide", "vide", "vide", "vide", "vide", "simplex"]
                )
                choixdutype = choice(typepossible_densite_minimale)

            if 35 >= chaque_etage < 60:
                typepossible_densite_moyenne = list(typepossible)
                typepossible_densite_moyenne.extend(
                    ["vide", "vide", "vide", "simplex", "simplex", "duplex", "duplex"]
                )
                choixdutype = choice(typepossible_densite_moyenne)

            if 10 >= chaque_etage < 35:
                typepossible_densite_minimale = list(typepossible)
                typepossible_densite_minimale.extend(
                    ["vide", "vide", "vide", "vide", "vide", "vide", "simplex"]
                )
                choixdutype = choice(typepossible_densite_minimale)

            print("etage en cours de creation : ", chaque_etage)
            print(choixdutype)

        ######définitions des typologies#####

        if choixdutype == "vide":
            print("cest un vide")
            chaque_etage = chaque_etage + 1

        if choixdutype == "simplex":
            print("cest un simplex")
            boite((x * 6.5, y * 22.5, chaque_etage * 3 + 1.5), (5.8, 6.5, 2.7))
            dalle((x * 16.5, y * 11.25, chaque_etage * 3), (25.5, 19.5, 0.2))
            chaque_etage = chaque_etage + 1

        if choixdutype == "duplex":
            print("cest un duplex")
            boite((x * 21.5, y * 18.5, chaque_etage * 3 + 4.5), (5.8, 7.8, 2.7))
            boite((x * 27.5, y * 18, chaque_etage * 3 + 1.5), (9.96, 4.8, 2.7))
            boite((x * 29.5, y * 6, chaque_etage * 3 + 1.5), (5, 4.8, 2.7))
            boite((x * 6, y * 14, chaque_etage * 3 + 4.5), (-9.96, 4.8, 2.7))
            dalle((x * 16.5, y * 11.25, chaque_etage * 3), (25.5, 19.5, 0.2))
            dalle((x * 16.5, y * 11.25, chaque_etage * 3 + 3), (25.5, 19.5, 0.2))
            mur((x * 18.65, y * 11.25, chaque_etage * 3 + 3), (0.1, 19.5, 5.9))
            circulations_verticales_duplex((x * 18.6, y * 8, chaque_etage * 3))
            chaque_etage = chaque_etage + 2


#######################################################################################################
# CAMERA
#######################################################################################################


def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 600
    if projection == "axonometrique":
        if canon == "isometrie":  # OK
            monAxoCam.name = "axoIsometrie"
            monAxoCam.rotation_euler = (radians(54.74), 0.0, radians(-45))
            # monAxoCam.location = (10.0, -10.0, 10.0)
            monAxoCam.location = (-95.0, -75.0, 268.45914)
            maScene.pixel_aspect_x = 1
            bpy.context.object.data.clip_end = 2500


axoCam("axonometrique", "isometrie")
