# SPDX-FileCopyrightText: 2014 Bertrand Etienne
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER Version 2.68 Sur Mac OS X Version 10.9.5


import bpy
import random
import math
import mathutils

bpy.app.debug_wm = True
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

print(
    "-----------------------------------------------Unité----------------------------------------------"
)

UN = 1.1556  # UNITE de la trame


print(
    "-----------------------------------------------Name----------------------------------------------"
)


def Name(k):
    bpy.context.object.name = k
    bpy.context.object.data.name = k


def SelectObject(kk):
    bpy.context.scene.objects.active = bpy.data.objects[kk]


def SelectObjectContext(erd):
    bpy.context.scene.objects[erd].select = True


def Supprimer(kkk):
    bpy.ops.object.delete(use_global=False)


def Clean(pl):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.paths_clear()


print(
    "-----------------------------------------------Modificateurs----------------------------------------------"
)


def place_obj(obj, x=0, y=0, z=0):
    bpy.data.objects[obj].location = (x, y, z)


def Smooth(de):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.subdivide(
        number_cuts=4,
        smoothness=0,
        quadtri=False,
        quadcorner="STRAIGHT_CUT",
        fractal=0,
        fractal_along_normal=0,
        seed=0,
    )
    bpy.ops.mesh.vertices_smooth(repeat=1, xaxis=True, yaxis=True, zaxis=True)
    bpy.ops.object.editmode_toggle()


def Multiplication(g, h):
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].count = g
    bpy.context.object.modifiers["Array"].use_relative_offset = False
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].constant_offset_displace[2] = h
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")


def Soustraction(i, j):
    bpy.context.scene.objects.active = bpy.data.objects[i]
    if i is not "Boolean":
        bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    if i is not "Boolean":
        bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[j]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects[j]
    bpy.ops.object.delete(use_global=False)


def Union(ii):
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[ii]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")


def Joindre(i, j):
    SelectObjectContext(i)
    bpy.context.scene.objects[i].select = True
    SelectObjectContext(j)
    bpy.context.scene.objects[j].select = True
    bpy.ops.object.join()


def Deplacer(v, w, x):
    bpy.ops.transform.translate(
        value=(v, w, x),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )


def Rotation(Z1, Z2):
    bpy.context.object.rotation_euler[Z1] = Z2


def Dupliquer(m12, m13, m14):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (m12, m13, m14),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "release_confirm": False,
        },
    )


def Resize(m15, m16, m17):
    bpy.ops.transform.resize(
        value=(m15, m16, m17),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


print(
    "-----------------------------------------------Base Volumétrique----------------------------------------------"
)


def Boite(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        view_align=False,
        enter_editmode=False,
        location=(o, p, q),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(r, s, t),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


def Cylindre(x=0, y=0, z=1.5, dim_x=10.4, dim_y=10.4, dim_z=1.5):
    bpy.ops.mesh.primitive_cylinder_add(vertices=80, location=(x, y, z), radius=0.5)
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("Cylindre")


def Diamant(
    x=0, y=0, z=1.5, dim_x=(math.sqrt(54.08)), dim_y=(math.sqrt(54.08)), dim_z=3
):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=(math.pi / 4), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    Name("Diamant")


def Colonne(x=0, y=0, z=1.5, dim_x=0.3, dim_y=0.3, dim_z=1.5):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z), radius=0.5)
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Carre(x=0, y=0, z=1.5, dim_x=10.4, dim_y=10.4, dim_z=3):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("Carre")


def Smooth(de):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.subdivide(
        number_cuts=4,
        smoothness=0,
        quadtri=False,
        quadcorner="STRAIGHT_CUT",
        fractal=0,
        fractal_along_normal=0,
        seed=0,
    )
    bpy.ops.mesh.vertices_smooth(repeat=1, xaxis=True, yaxis=True, zaxis=True)
    bpy.ops.object.editmode_toggle()


print(
    "-----------------------------------------------Volumes de soustractions pour Bolean----------------------------------------------"
)


def NegativeHalf(x=0, y=2.25, z=1, dim_x=9, dim_y=2.25, dim_z=2):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("NEGATIVEHALF")


def NegativeHalf2(x=-2.25, y=0, z=1, dim_x=2.25, dim_y=9, dim_z=2):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("NEGATIVEHALF2")


def NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(1, 1, 2))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("NEGATIVETHREEQUARTER")


print(
    "-----------------------------------------------Constructeur Escalier----------------------------------------------"
)


def escalierA(
    OrigineX=0.0, OrigineY=0.0, OrigineZ=0.0, HTot=1.2, LTot=2.0, LargeurMarche=0.8
):

    NbMarches = (int(HTot) * 10) // 2
    HMarche = int(HTot) / NbMarches
    ProfMarche = LTot / (NbMarches - 1)
    for i in range(0, NbMarches - 1):
        Boite(
            OrigineX,
            OrigineY + ProfMarche / 2 + i * ProfMarche,
            (OrigineZ + HMarche / 2) + i * HMarche,
            LargeurMarche,
            ProfMarche,
            HMarche,
        )
        Name("MarcheA")
    bpy.ops.object.select_pattern(pattern="MarcheA*")
    bpy.context.scene.objects.active = bpy.data.objects["MarcheA"]
    bpy.ops.object.join()


def escalierY(
    OrigineX=0.0, OrigineY=0.0, OrigineZ=0.0, HTot=1.2, LTot=2.0, LargeurMarche=0.8
):

    bpy.context.scene.cursor_location = mathutils.Vector((0.0, 0.0, 0.0))

    NbMarches = (int(HTot * 10)) // 2
    HMarche = HTot / NbMarches
    ProfMarche = LTot / NbMarches

    # Creation de la premiere marche (plate)
    Boite(
        OrigineX,
        OrigineY + ProfMarche / 2,
        (OrigineZ + HMarche / 2),
        LargeurMarche,
        ProfMarche,
        HMarche,
    )
    Name("Marche")

    # CrÈation de la marche initiale trapezoidale

    # Les points sont des tuples de 3 flottants
    coords = [
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ + HMarche * 2),
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ + HMarche * 2),
        (
            OrigineX - LargeurMarche / 2,
            OrigineY + ProfMarche * 2,
            OrigineZ + HMarche * 2,
        ),
        (
            OrigineX + LargeurMarche / 2,
            OrigineY + ProfMarche * 2,
            OrigineZ + HMarche * 2,
        ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche * 2, OrigineZ + HMarche),
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche * 2, OrigineZ + HMarche),
    ]

    # Faces dÈfinies par des tuples dÈfinissant les index des points (en partant de 0)

    faces = [
        (0, 1, 2, 3),
        (2, 5, 4, 3),
        (4, 5, 6, 7),
        (7, 6, 1, 0),
        (0, 3, 4, 7),
        (1, 2, 5, 6),
    ]

    me = bpy.data.meshes.new("MarcheTMesh")  # creation d'un nouveau mesh
    ob = bpy.data.objects.new("MarcheT", me)
    ob.location = bpy.context.scene.cursor_location
    bpy.context.scene.objects.link(ob)

    me.from_pydata(coords, [], faces)
    me.update(calc_edges=True)

    # Generation de l'escalier
    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["MarcheT"].select = True
    bpy.context.scene.objects.active = bpy.data.objects["MarcheT"]
    for i in range(0, NbMarches - 2):
        Dupliquer(0, ProfMarche, HMarche)
    bpy.ops.object.select_pattern(pattern="Marche*")
    bpy.context.scene.objects.active = bpy.data.objects["Marche"]
    bpy.ops.object.join()


print(
    "---------------------------------------------FORMES PRIMAIRES----------------------------------------------------"
)

#####################
### QUART CYLINDRE
#####################


def QCylindre1(x=0, y=0, z=1.5):
    Cylindre()
    Name("QCYLINDRE1")
    NegativeThreeQuarter()
    Soustraction("QCYLINDRE1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE1", "NEGATIVEHALF")
    place_obj("QCYLINDRE1", x, y, z)


def QCylindre2(x=0, y=0, z=1.5):
    Cylindre()
    Name("QCYLINDRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE2", "NEGATIVEHALF")
    place_obj("QCYLINDRE2", x, y, z)


def QCylindre3(x=0, y=0, z=1.5):
    Cylindre()
    Name("QCYLINDRE3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE3", "NEGATIVEHALF")
    place_obj("QCYLINDRE3", x, y, z)


def QCylindre4(x=0, y=0, z=1.5):
    Cylindre()
    Name("QCYLINDRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE4", "NEGATIVEHALF")
    place_obj("QCYLINDRE4", x, y, z)


#####################
### QUART DIAMANT
#####################


def QDiamant1(x=0, y=0, z=1.5):
    Diamant()
    Name("QDIAMANT1")
    NegativeThreeQuarter()
    Soustraction("QDIAMANT1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT1", "NEGATIVEHALF")
    place_obj("QDIAMANT1", x, y, z)


def QDiamant2(x=0, y=0, z=1.5):
    Diamant()
    Name("QDIAMANT2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT2", "NEGATIVEHALF")
    place_obj("QDIAMANT2", x, y, z)


def QDiamant3(x=0, y=0, z=1.5):
    Diamant()
    Name("QDIAMANT3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT3", "NEGATIVEHALF")
    place_obj("QDIAMANT3", x, y, z)


def QDiamant4(x=0, y=0, z=1.5):
    Diamant()
    Name("QDIAMANT4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT4", "NEGATIVEHALF")
    place_obj("QDIAMANT4", x, y, z)


#####################
### QUART CARRE
#####################


def QCarre1(x=0, y=0, z=1.5):
    Carre()
    Name("QCARRE1")
    NegativeThreeQuarter()
    Soustraction("QCARRE1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE1", "NEGATIVEHALF")
    place_obj("QCARRE1", x, y, z)


def QCarre2(x=0, y=0, z=1.5):
    Carre()
    Name("QCARRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE2", "NEGATIVEHALF")
    place_obj("QCARRE2", x, y, z)


def QCarre3(x=0, y=0, z=1.5):
    Carre()
    Name("QCARRE3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE3", "NEGATIVEHALF")
    place_obj("QCARRE3", x, y, z)


def QCarre4(x=0, y=0, z=1.5):
    Carre()
    Name("QCARRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE4", "NEGATIVEHALF")
    place_obj("QCARRE4", x, y, z)


#####################
### DEMI CYLINDRE
#####################


def DCylindre1(x=0, y=0, z=1.5):
    Cylindre()
    Name("DCYLINDRE1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCYLINDRE1", "NEGATIVEHALF")
    place_obj("DCYLINDRE1", x, y, z)


def DCylindre2(x=0, y=0, z=1.5):
    Cylindre()
    Name("DCYLINDRE2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCYLINDRE2", "NEGATIVEHALF2")
    place_obj("DCYLINDRE2", x, y, z)


def DCylindre3(x=0, y=0, z=1.5):
    Cylindre()
    Name("DCYLINDRE3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCYLINDRE3", "NEGATIVEHALF")
    place_obj("DCYLINDRE3", x, y, z)


def DCylindre4(x=0, y=0, z=1.5):
    Cylindre()
    Name("DCYLINDRE4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCYLINDRE4", "NEGATIVEHALF2")
    place_obj("DCYLINDRE4", x, y, z)


#####################
### DEMI DIAMANT
#####################


def DDiamant1(x=0, y=0, z=1.5):
    Diamant()
    Name("DDIAMANT1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DDIAMANT1", "NEGATIVEHALF")
    place_obj("DDIAMANT1", x, y, z)


def DDiamant2(x=0, y=0, z=1.5):
    Diamant()
    Name("DDIAMANT2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DDIAMANT2", "NEGATIVEHALF2")
    place_obj("DDIAMANT2", x, y, z)


def DDiamant3(x=0, y=0, z=1.5):
    Diamant()
    Name("DDIAMANT3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DDIAMANT3", "NEGATIVEHALF")
    place_obj("DDIAMANT3", x, y, z)


def DDiamant4(x=0, y=0, z=1.5):
    Diamant()
    Name("DDIAMANT4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DDIAMANT4", "NEGATIVEHALF2")
    place_obj("DDIAMANT4", x, y, z)


#####################
### DEMI CARRE
#####################


def DCarre1(x=0, y=0, z=1.5):
    Carre()
    Name("DCARRE1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCARRE1", "NEGATIVEHALF")
    place_obj("DCARRE1", x, y, z)


def DCarre2(x=0, y=0, z=1.5):
    Carre()
    Name("DCARRE2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCARRE2", "NEGATIVEHALF2")
    place_obj("DCARRE2", x, y, z)


def DCarre3(x=0, y=0, z=1.5):
    Carre()
    Name("DCARRE3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCARRE3", "NEGATIVEHALF")
    place_obj("DCARRE3", x, y, z)


def DCarre4(x=0, y=0, z=1.5):
    Carre()
    Name("DCARRE4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCARRE4", "NEGATIVEHALF2")
    place_obj("DCARRE4", x, y, z)


#####################
#### TROIS QUART CYLINDRE
#####################


def TQCylindre1(x=0, y=0, z=1.5):
    Cylindre()
    Name("TQCYLINDRE1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE1", "NEGATIVETHREEQUARTER")
    place_obj("TQCYLINDRE1", x, y, z)


def TQCylindre2(x=0, y=0, z=1.5):
    Cylindre()
    Name("TQCYLINDRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE2", "NEGATIVETHREEQUARTER")
    place_obj("TQCYLINDRE2", x, y, z)


def TQCylindre3(x=0, y=0, z=1.5):
    Cylindre()
    Name("TQCYLINDRE3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE3", "NEGATIVETHREEQUARTER")
    place_obj("TQCYLINDRE3", x, y, z)


def TQCylindre4(x=0, y=0, z=1.5):
    Cylindre()
    Name("TQCYLINDRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE4", "NEGATIVETHREEQUARTER")
    place_obj("TQCYLINDRE4", x, y, z)


#####################
### TROIS QUART DIAMANT
#####################


def TQDiamant1(x=0, y=0, z=1.5):
    Diamant()
    Name("TQDIAMANT1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT1", "NEGATIVETHREEQUARTER")
    place_obj("TQDIAMANT1", x, y, z)


def TQDiamant2(x=0, y=0, z=1.5):
    Diamant()
    Name("TQDIAMANT2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT2", "NEGATIVETHREEQUARTER")
    place_obj("TQDIAMANT2", x, y, z)


def TQDiamant3(x=0, y=0, z=1.5):
    Diamant()
    Name("TQDIAMANT3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT3", "NEGATIVETHREEQUARTER")
    place_obj("TQDIAMANT3", x, y, z)


def TQDiamant4(x=0, y=0, z=1.5):
    Diamant()
    Name("TQDIAMANT4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT4", "NEGATIVETHREEQUARTER")
    place_obj("TQDIAMANT4", x, y, z)


#####################
### TROIS QUART CARRE
#####################


def TQCarre1(x=0, y=0, z=1.5):
    Carre()
    Name("TQCARRE1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE1", "NEGATIVETHREEQUARTER")
    place_obj("TQCARRE1", x, y, z)


def TQCarre2(x=0, y=0, z=1.5):
    Carre()
    Name("TQCARRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE2", "NEGATIVETHREEQUARTER")
    place_obj("TQCARRE2", x, y, z)


def TQCarre3(x=0, y=0, z=1.5):
    Carre()
    Name("TQCARRE3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE3", "NEGATIVETHREEQUARTER")
    place_obj("TQCARRE3", x, y, z)


def TQCarre4(x=0, y=0, z=1.5):
    Carre()
    Name("TQCARRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE4", "NEGATIVETHREEQUARTER")
    place_obj("TQCARRE4", x, y, z)


print(
    "---------------------------------------------FORMES PRIMAIRES AVEC COLONNES----------------------------------------------------"
)

##########################################
### QUART CYLINDRE + COLONNES
##########################################


def QCylindre1C(x=0, y=0, z=1.5):

    Colonne(-0.4166, -0.4166, -1.5)
    Name("TEUB")
    Colonne(-0.4166, -4.5768, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-3.3583, -3.3583, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.5768, -0.4166, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Cylindre()
    Name("QCYLINDRE1")
    NegativeThreeQuarter()
    Soustraction("QCYLINDRE1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE1", "NEGATIVEHALF")

    SelectObject("QCYLINDRE1")
    Joindre("TEUB3", "QCYLINDRE1")
    place_obj("QCYLINDRE1", x, y, z)


def QCylindre2C(x=0, y=0, z=1.5):

    Colonne(0.4166, -0.4166, -1.5)
    Name("TEUB")
    Colonne(0.4166, -4.5768, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(3.3583, -3.3583, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.5768, -0.4166, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Cylindre()
    Name("QCYLINDRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE2", "NEGATIVEHALF")

    SelectObject("QCYLINDRE2")
    Joindre("TEUB3", "QCYLINDRE2")
    place_obj("QCYLINDRE2", x, y, z)


def QCylindre3C(x=0, y=0, z=1.5):

    Colonne(0.4166, 0.4166, -1.5)
    Name("TEUB")
    Colonne(0.4166, 4.5768, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(3.3583, 3.3583, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.5768, 0.4166, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Cylindre()
    Name("QCYLINDRE3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE3", "NEGATIVEHALF")

    SelectObject("QCYLINDRE3")
    Joindre("TEUB3", "QCYLINDRE3")
    place_obj("QCYLINDRE3", x, y, z)


def QCylindre4C(x=0, y=0, z=1.5):

    Colonne(-0.4166, 0.4166, -1.5)
    Name("TEUB")
    Colonne(-0.4166, 4.5768, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-3.3583, 3.3583, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.5768, 0.4166, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Cylindre()
    Name("QCYLINDRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCYLINDRE4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCYLINDRE4", "NEGATIVEHALF")

    SelectObject("QCYLINDRE4")
    Joindre("TEUB3", "QCYLINDRE4")
    place_obj("QCYLINDRE4", x, y, z)


##########################################
### QUART DIAMANT + COLONNES
##########################################


def QDiamant1C(x=0, y=0, z=1.5):

    Colonne(-0.3046, -0.3046, -1.5)
    Name("TEUB")
    Colonne(-0.3046, -4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-2.3847, -2.3847, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.4648, -0.3046, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("QDIAMANT1")
    NegativeThreeQuarter()
    Soustraction("QDIAMANT1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT1", "NEGATIVEHALF")

    SelectObject("QDIAMANT1")
    Joindre("TEUB3", "QDIAMANT1")
    place_obj("QDIAMANT1", x, y, z)


def QDiamant2C(x=0, y=0, z=1.5):

    Colonne(0.3046, -0.3046, -1.5)
    Name("TEUB")
    Colonne(0.3046, -4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(2.3847, -2.3847, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.4648, -0.3046, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("QDIAMANT2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT2", "NEGATIVEHALF")

    SelectObject("QDIAMANT2")
    Joindre("TEUB3", "QDIAMANT2")
    place_obj("QDIAMANT2", x, y, z)


def QDiamant3C(x=0, y=0, z=1.5):

    Colonne(0.3046, 0.3046, -1.5)
    Name("TEUB")
    Colonne(0.3046, 4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(2.3847, 2.3847, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.4648, 0.3046, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("QDIAMANT3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT3", "NEGATIVEHALF")

    SelectObject("QDIAMANT3")
    Joindre("TEUB3", "QDIAMANT3")
    place_obj("QDIAMANT3", x, y, z)


def QDiamant4C(x=0, y=0, z=1.5):

    Colonne(-0.3046, 0.3046, -1.5)
    Name("TEUB")
    Colonne(-0.3046, 4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-2.3847, 2.3847, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.4648, 0.3046, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("QDIAMANT4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QDIAMANT4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QDIAMANT4", "NEGATIVEHALF")

    SelectObject("QDIAMANT4")
    Joindre("TEUB3", "QDIAMANT4")
    place_obj("QDIAMANT4", x, y, z)


##########################################
### QUART CARRE + COLONNES
##########################################


def QCarre1C(x=0, y=0, z=1.5):

    Colonne(-0.52, -0.52, -1.5)
    Name("TEUB")
    Colonne(-0.52, -4.6802, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.6802, -4.6802, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.6802, -0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Carre()
    Name("QCARRE1")
    NegativeThreeQuarter()
    Soustraction("QCARRE1", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE1", "NEGATIVEHALF")

    SelectObject("QCARRE1")
    Joindre("TEUB3", "QCARRE1")
    place_obj("QCARRE1", x, y, z)


def QCarre2C(x=0, y=0, z=1.5):

    Colonne(0.52, -0.52, -1.5)
    Name("TEUB")
    Colonne(0.52, -4.6802, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.6802, -4.6802, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.6802, -0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Carre()
    Name("QCARRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE2", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE2", "NEGATIVEHALF")

    SelectObject("QCARRE2")
    Joindre("TEUB3", "QCARRE2")
    place_obj("QCARRE2", x, y, z)


def QCarre3C(x=0, y=0, z=1.5):

    Colonne(0.52, 0.52, -1.5)
    Name("TEUB")
    Colonne(0.52, 4.6802, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.6802, 4.6802, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.6802, 0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Carre()
    Name("QCARRE3")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE3", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE3", "NEGATIVEHALF")

    SelectObject("QCARRE3")
    Joindre("TEUB3", "QCARRE3")
    place_obj("QCARRE3", x, y, z)


def QCarre4C(x=0, y=0, z=1.5):

    Colonne(-0.52, 0.52, -1.5)
    Name("TEUB")
    Colonne(-0.52, 4.6802, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.6802, 4.6802, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.6802, 0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Carre()
    Name("QCARRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("QCARRE4", "NEGATIVETHREEQUARTER")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("QCARRE4", "NEGATIVEHALF")

    SelectObject("QCARRE4")
    Joindre("TEUB3", "QCARRE4")
    place_obj("QCARRE4", x, y, z)


##########################################
### DEMI CYLINDRE + COLONNES
##########################################


def DCylindre1C(x=0, y=0, z=1.5):

    Colonne(-4.6510, -0.52, -1.5)
    Name("TEUB")
    Colonne(-3.039, -3.559, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, -4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(3.039, -3.559, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(4.651, -0.52, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("DCYLINDRE1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCYLINDRE1", "NEGATIVEHALF")

    SelectObject("DCYLINDRE1")
    Joindre("TEUB4", "DCYLINDRE1")
    place_obj("DCYLINDRE1", x, y, z)


def DCylindre2C(x=0, y=0, z=1.5):

    Colonne(0.52, 4.6510, -1.5)
    Name("TEUB")
    Colonne(3.039, 3.559, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.68, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(3.039, -3.559, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(0.52, -4.6510, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("DCYLINDRE2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCYLINDRE2", "NEGATIVEHALF2")

    SelectObject("DCYLINDRE2")
    Joindre("TEUB4", "DCYLINDRE2")
    place_obj("DCYLINDRE2", x, y, z)


def DCylindre3C(x=0, y=0, z=1.5):

    Colonne(-4.6510, 0.52, -1.5)
    Name("TEUB")
    Colonne(-3.039, 3.559, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, 4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(3.039, 3.559, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(4.651, 0.52, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("DCYLINDRE3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCYLINDRE3", "NEGATIVEHALF")

    SelectObject("DCYLINDRE3")
    Joindre("TEUB4", "DCYLINDRE3")
    place_obj("DCYLINDRE3", x, y, z)


def DCylindre4C(x=0, y=0, z=1.5):

    Colonne(-0.52, 4.6510, -1.5)
    Name("TEUB")
    Colonne(-3.039, 3.559, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-3.039, -3.559, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-0.52, -4.6510, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("DCYLINDRE4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCYLINDRE4", "NEGATIVEHALF2")

    SelectObject("DCYLINDRE4")
    Joindre("TEUB4", "DCYLINDRE4")
    place_obj("DCYLINDRE4", x, y, z)


##########################################
### DEMI DIAMANT + COLONNES
##########################################


def DDiamant1C(x=0, y=0, z=1.5):

    Colonne(4.16, -0.4308, -1.5)
    Name("TEUB")
    Colonne(0, -4.5908, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.16, -0.4308, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")

    Diamant()
    Name("DDIAMANT1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DDIAMANT1", "NEGATIVEHALF")

    SelectObject("DDIAMANT1")
    Joindre("TEUB2", "DDIAMANT1")
    place_obj("DDIAMANT1", x, y, z)


def DDiamant2C(x=0, y=0, z=1.5):

    Colonne(0.4308, 4.16, -1.5)
    Name("TEUB")
    Colonne(4.5908, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0.4308, -4.16, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")

    Diamant()
    Name("DDIAMANT2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DDIAMANT2", "NEGATIVEHALF2")

    SelectObject("DDIAMANT2")
    Joindre("TEUB2", "DDIAMANT2")
    place_obj("DDIAMANT2", x, y, z)


def DDiamant3C(x=0, y=0, z=1.5):

    Colonne(4.16, 0.4308, -1.5)
    Name("TEUB")
    Colonne(0, 4.5908, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.16, 0.4308, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")

    Diamant()
    Name("DDIAMANT3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DDIAMANT3", "NEGATIVEHALF")

    SelectObject("DDIAMANT3")
    Joindre("TEUB2", "DDIAMANT3")
    place_obj("DDIAMANT3", x, y, z)


def DDiamant4C(x=0, y=0, z=1.5):

    Colonne(-0.4308, 4.16, -1.5)
    Name("TEUB")
    Colonne(-4.5908, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-0.4308, -4.16, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")

    Diamant()
    Name("DDIAMANT4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DDIAMANT4", "NEGATIVEHALF2")

    SelectObject("DDIAMANT4")
    Joindre("TEUB2", "DDIAMANT4")
    place_obj("DDIAMANT4", x, y, z)


##########################################
### DEMI CARRE + COLONNES
##########################################


def DCarre1C(x=0, y=0, z=1.5):

    Colonne(4.68, -0.52, -1.5)
    Name("TEUB")
    Colonne(4.68, -4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, -4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.68, -4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-4.68, -0.52, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("DCARRE1")
    NegativeHalf(x=0, y=4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCARRE1", "NEGATIVEHALF")

    SelectObject("DCARRE1")
    Joindre("TEUB4", "DCARRE1")
    place_obj("DCARRE1", x, y, z)


def DCarre2C(x=0, y=0, z=1.5):

    Colonne(0.52, 4.68, -1.5)
    Name("TEUB")
    Colonne(4.68, 4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.68, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.68, -4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(0.52, -4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("DCARRE2")
    NegativeHalf2(x=-4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCARRE2", "NEGATIVEHALF2")

    SelectObject("DCARRE2")
    Joindre("TEUB4", "DCARRE2")
    place_obj("DCARRE2", x, y, z)


def DCarre3C(x=0, y=0, z=1.5):

    Colonne(4.68, 0.52, -1.5)
    Name("TEUB")
    Colonne(4.68, 4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, 4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.68, 4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-4.68, 0.52, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("DCARRE3")
    NegativeHalf(x=0, y=-4, z=1, dim_x=9, dim_y=4, dim_z=2)
    Soustraction("DCARRE3", "NEGATIVEHALF")

    SelectObject("DCARRE3")
    Joindre("TEUB4", "DCARRE3")
    place_obj("DCARRE3", x, y, z)


def DCarre4C(x=0, y=0, z=1.5):

    Colonne(-0.52, 4.68, -1.5)
    Name("TEUB")
    Colonne(-4.68, 4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.68, -4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-0.52, -4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("DCARRE4")
    NegativeHalf2(x=4, y=0, z=1, dim_x=4, dim_y=9, dim_z=2)
    Soustraction("DCARRE4", "NEGATIVEHALF2")

    SelectObject("DCARRE4")
    Joindre("TEUB4", "DCARRE4")
    place_obj("DCARRE4", x, y, z)


##########################################
#### TROIS QUART CYLINDRE + COLONNES
##########################################


def TQCylindre1C(x=0, y=0, z=1.5):

    Colonne(-3.3093, -3.3093, -1.5)
    Name("TEUB")
    Colonne(0, -4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(3.3093, -3.3093, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.68, 0, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(3.3093, 3.3093, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("TQCYLINDRE1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE1", "NEGATIVETHREEQUARTER")

    SelectObject("TQCYLINDRE1")
    Joindre("TEUB4", "TQCYLINDRE1")
    place_obj("TQCYLINDRE1", x, y, z)


def TQCylindre2C(x=0, y=0, z=1.5):

    Colonne(3.3093, -3.3093, -1.5)
    Name("TEUB")
    Colonne(4.68, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(3.3093, 3.3093, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(0, 4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-3.3093, 3.3093, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("TQCYLINDRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE2", "NEGATIVETHREEQUARTER")

    SelectObject("TQCYLINDRE2")
    Joindre("TEUB4", "TQCYLINDRE2")
    place_obj("TQCYLINDRE2", x, y, z)


def TQCylindre3C(x=0, y=0, z=1.5):

    Colonne(3.3093, 3.3093, -1.5)
    Name("TEUB")
    Colonne(0, 4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-3.3093, 3.3093, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-3.3093, -3.3093, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("TQCYLINDRE3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE3", "NEGATIVETHREEQUARTER")

    SelectObject("TQCYLINDRE3")
    Joindre("TEUB4", "TQCYLINDRE3")
    place_obj("TQCYLINDRE3", x, y, z)


def TQCylindre4C(x=0, y=0, z=1.5):

    Colonne(-3.3093, 3.3093, -1.5)
    Name("TEUB")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-3.3093, -3.3093, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(0, -4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(3.3093, -3.3093, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Cylindre()
    Name("TQCYLINDRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCYLINDRE4", "NEGATIVETHREEQUARTER")

    SelectObject("TQCYLINDRE4")
    Joindre("TEUB4", "TQCYLINDRE4")
    place_obj("TQCYLINDRE4", x, y, z)


##########################################
### TROIS QUART DIAMANT + COLONNES
##########################################


def TQDiamant1C(x=0, y=0, z=1.5):

    Colonne(-3.9448, -0.52, -1.5)
    Name("TEUB")
    Colonne(0, -4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.4648, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(0.52, 3.9448, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("TQDIAMANT1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT1", "NEGATIVETHREEQUARTER")

    SelectObject("TQDIAMANT1")
    Joindre("TEUB3", "TQDIAMANT1")
    place_obj("TQDIAMANT1", x, y, z)


def TQDiamant2C(x=0, y=0, z=1.5):

    Colonne(0.52, -3.9448, -1.5)
    Name("TEUB")
    Colonne(4.4648, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, 4.4648, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-3.9448, 0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("TQDIAMANT2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT2", "NEGATIVETHREEQUARTER")

    SelectObject("TQDIAMANT2")
    Joindre("TEUB3", "TQDIAMANT2")
    place_obj("TQDIAMANT2", x, y, z)


def TQDiamant3C(x=0, y=0, z=1.5):

    Colonne(3.9448, 0.52, -1.5)
    Name("TEUB")
    Colonne(0, 4.4648, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.4648, 0, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-0.52, -3.9448, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("TQDIAMANT3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT3", "NEGATIVETHREEQUARTER")

    SelectObject("TQDIAMANT3")
    Joindre("TEUB3", "TQDIAMANT3")
    place_obj("TQDIAMANT3", x, y, z)


def TQDiamant4C(x=0, y=0, z=1.5):

    Colonne(-0.52, 3.9448, -1.5)
    Name("TEUB")
    Colonne(-4.4648, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(0, -4.4648, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(3.9448, -0.52, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")

    Diamant()
    Name("TQDIAMANT4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQDIAMANT4", "NEGATIVETHREEQUARTER")

    SelectObject("TQDIAMANT4")
    Joindre("TEUB3", "TQDIAMANT4")
    place_obj("TQDIAMANT4", x, y, z)


##########################################
### TROIS QUART CARRE + COLONNES
##########################################


def TQCarre1C(x=0, y=0, z=1.5):

    Colonne(4.68, 4.68, -1.5)
    Name("TEUB")
    Colonne(4.68, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.68, -4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(0, -4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-4.68, -4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("TQCARRE1")
    NegativeThreeQuarter(x=-4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE1", "NEGATIVETHREEQUARTER")

    SelectObject("TQCARRE1")
    Joindre("TEUB4", "TQCARRE1")
    place_obj("TQCARRE1", x, y, z)


def TQCarre2C(x=0, y=0, z=1.5):

    Colonne(-4.68, 4.68, -1.5)
    Name("TEUB")
    Colonne(0, 4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(4.68, 4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(4.68, 0, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(4.68, -4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("TQCARRE2")
    NegativeThreeQuarter(x=-4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE2", "NEGATIVETHREEQUARTER")

    SelectObject("TQCARRE2")
    Joindre("TEUB4", "TQCARRE2")
    place_obj("TQCARRE2", x, y, z)


def TQCarre3C(x=0, y=0, z=1.5):

    Colonne(-4.68, -4.68, -1.5)
    Name("TEUB")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.68, 4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(0, 4.68, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(4.68, 4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("TQCARRE3")
    NegativeThreeQuarter(x=4.5, y=-4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE3", "NEGATIVETHREEQUARTER")

    SelectObject("TQCARRE3")
    Joindre("TEUB4", "TQCARRE3")
    place_obj("TQCARRE3", x, y, z)


def TQCarre4C(x=0, y=0, z=1.5):

    Colonne(4.68, -4.68, -1.5)
    Name("TEUB")
    Colonne(0, -4.68, -1.5)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")
    Colonne(-4.68, -4.68, -1.5)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(-4.68, 0, -1.5)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(-4.68, 4.68, -1.5)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")

    Carre()
    Name("TQCARRE4")
    NegativeThreeQuarter(x=4.5, y=4.5, z=1, dim_x=4.5, dim_y=4.5, dim_z=1)
    Soustraction("TQCARRE4", "NEGATIVETHREEQUARTER")

    SelectObject("TQCARRE4")
    Joindre("TEUB4", "TQCARRE4")
    place_obj("TQCARRE4", x, y, z)


print(
    "---------------------------------------------LISTES VOLUMES----------------------------------------------------"
)

# LISTE CYLINDRE
L_QCY = [QCylindre1, QCylindre2, QCylindre3, QCylindre4]
L_DCY = [DCylindre1, DCylindre2, DCylindre3, DCylindre4]
L_TQCY = [TQCylindre1, TQCylindre2, TQCylindre3, TQCylindre4]

L_CY = [L_QCY, L_DCY, L_TQCY]


# LISTE DAIAMANT
L_QD = [QDiamant1, QDiamant2, QDiamant3, QDiamant4]
L_DD = [DDiamant1, DDiamant2, DDiamant3, DDiamant4]
L_TQD = [TQDiamant1, TQDiamant2, TQDiamant3, TQDiamant4]

L_D = [L_QD, L_DD, L_TQD]


# LISTE CARRE
L_QCA = [QCarre1, QCarre2, QCarre3, QCarre4]
L_DCA = [DCarre1, DCarre2, DCarre3, DCarre4]
L_TQCA = [TQCarre1, TQCarre2, TQCarre3, TQCarre4]

L_CA = [L_QCA, L_DCA, L_TQCA]


# LISTE TOTAL
ListeTotal = [L_CY, L_D, L_CA]


print(
    "---------------------------------------------LISTES VOLUMES AVEC COLONNES----------------------------------------------------"
)

# LISTE CYLINDRE
L_QCY_C = [QCylindre1C, QCylindre2C, QCylindre3C, QCylindre4C]
L_DCY_C = [DCylindre1C, DCylindre2C, DCylindre3C, DCylindre4C]
L_TQCY_C = [TQCylindre1C, TQCylindre2C, TQCylindre3C, TQCylindre4C]

L_CY_C = [L_QCY_C, L_DCY_C, L_TQCY_C]


# LISTE DAIAMANT
L_QD_C = [QDiamant1C, QDiamant2C, QDiamant3C, QDiamant4C]
L_DD_C = [DDiamant1C, DDiamant2C, DDiamant3C, DDiamant4C]
L_TQD_C = [TQDiamant1C, TQDiamant2C, TQDiamant3C, TQDiamant4C]

L_D_C = [L_QD_C, L_DD_C, L_TQD_C]


# LISTE CARRE
L_QCA_C = [QCarre1C, QCarre2C, QCarre3C, QCarre4C]
L_DCA_C = [DCarre1C, DCarre2C, DCarre3C, DCarre4C]
L_TQCA_C = [TQCarre1C, TQCarre2C, TQCarre3C, TQCarre4C]

L_CA_C = [L_QCA_C, L_DCA_C, L_TQCA_C]


# LISTE TOTAL
ListeTotalColonne = [L_CY_C, L_D_C, L_CA_C]


print(
    "---------------------------------------------CIRCULATION, COULOIR, PASSERELLE----------------------------------------------------"
)


def Couloir(x=0, y=0, z=0, VAR=1):
    Boite(o=x, p=y, q=z + 1.5, r=VAR * UN, s=UN, t=3)
    Name("COULOIR")


def CouloirL(x=0, y=0, z=0, VAR=15):
    Boite(
        o=-(VAR * UN) / 2 + UN / 2, p=(VAR * UN) / 2, q=z + 1.5, r=UN, s=VAR * UN, t=3
    )
    Name("TEUB")
    Boite(o=0, p=y, q=z + 1.5, r=VAR * UN, s=UN, t=3)
    Name("COULOIRL")
    Joindre("TEUB", "COULOIRL")


def CouloirH(x=0, y=0, z=0, VAR=15, VAR2=0, VAR3=1):
    Boite(
        o=-(VAR * UN) / 2,
        p=y + VAR2 * UN,
        q=z + 1.5,
        r=UN,
        s=VAR3 * UN + 4.5 * UN * 2,
        t=3,
    )
    Name("TEUB")
    Boite(o=x, p=y, q=z + 1.5, r=VAR * UN, s=UN, t=3)
    Name("COULOIRH")
    Joindre("TEUB", "COULOIRH")


def CouloirLarge(x=0, y=0, z=0, VAR=1):
    Boite(o=x, p=y, q=z + 1.5, r=VAR * UN, s=1.7334, t=3)
    Name("COULOIRLARGE")


def Circulation(x=0, y=0, z=1.5, VAR=1, VAR2=1, VAR3=1):
    Boite(o=x + (UN / 2) + VAR * UN / 2, p=y + VAR2 * UN, q=z, r=UN, s=VAR3 * UN, t=3)
    Name("CIRCULATION")


def CirculationTQH(x=0, y=0, VAR=1):
    Boite(o=x, p=y, q=1.5, r=1.7334, s=VAR, t=3)
    Name("CIRCULATIONTQH")


def Passerelle(x=0, y=0, z=0, VAR=1, VAR2=1, VAR3=1, VAR4=1):
    Boite(
        o=x + (UN / 2) + VAR * (UN / 2),
        p=y + VAR2 * UN,
        q=2.85,
        r=VAR4 * UN,
        s=VAR3 * UN,
        t=0.3,
    )
    Name("PASSERELLE")


def Passerelle1(x=0, y=0, z=2.85, VAR=1, VAR2=1, VAR3=1, VAR4=1):
    Boite(o=0, p=0, q=2.85, r=VAR4 * UN, s=VAR3 * UN, t=0.3)
    Name("PASSERELLE1")
    place_obj("PASSERELLE1", x, y, z)


def Passerelle2(x=0, y=0, z=2.85, VAR=1, VAR2=1, VAR3=1, VAR4=1):
    Boite(o=0, p=0, q=2.85, r=VAR4 * UN, s=VAR3 * UN, t=0.3)
    Name("PASSERELLE2")
    place_obj("PASSERELLE2", x, y, z)


def Passerelle3(x=0, y=0, z=2.85, VAR=1, VAR2=1, VAR3=1, VAR4=1):
    Boite(o=0, p=0, q=2.85, r=VAR4 * UN, s=VAR3 * UN, t=0.3)
    Name("PASSERELLE3")
    place_obj("PASSERELLE3", x, y, z)


def Mur(x=0, y=0, z=3, VAR=27):
    Boite(o=0, p=0, q=3, r=0.3, s=UN * VAR, t=6)
    Name("MUR")
    place_obj("MUR", x, y + UN * VAR / 2, z)


def PassColonnes(x=0, y=0, z=2.85, VAR=21):
    Colonne(x=0, y=9.5046, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB1")
    Colonne(x=0, y=4.8822, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Colonne(x=0, y=0.2598, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Colonne(x=0, y=-4.3626, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")
    Colonne(x=0, y=-8.9850, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB5")
    Joindre("TEUB4", "TEUB5")

    Boite(o=0, p=0, q=2.85, r=UN, s=VAR * UN, t=0.3)
    Name("PASSCOLONNES")
    Joindre("TEUB5", "PASSCOLONNES")
    place_obj("PASSCOLONNES", x, y, z)


print(
    "---------------------------------------------RAMPES----------------------------------------------------"
)


def RampeI(x=0, y=0, z=1.36, VAR0=25.5):
    Boite((-5.7 * ((VAR0 * UN) / 2) / 12.75), 0, 0.3, 0.15, UN, 0.6)
    Name("TEUB1")
    Boite((-1 * ((VAR0 * UN) / 2) / 12.75), 0, 0.6, 0.15, UN, 1.2)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")
    Boite((3.6 * ((VAR0 * UN) / 2) / 12.75), 0, 0.9, 0.15, UN, 1.8)
    Name("TEUB3")
    Joindre("TEUB2", "TEUB3")
    Boite((8.2 * ((VAR0 * UN) / 2) / 12.75), 0, 1.1, 0.15, UN, 2.2)
    Name("TEUB4")
    Joindre("TEUB3", "TEUB4")
    Boite(0, 0, 1.36, (VAR0 * UN), UN, 0.3)
    bpy.ops.transform.rotate(
        value=-0.11868238913561, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )
    Name("RAMPEI")
    Joindre("TEUB4", "RAMPEI")
    Boite(-12, 0, -1.5, 4, 4, 3)
    Name("TEUB")
    Soustraction("RAMPEI", "TEUB")
    place_obj("RAMPEI", x, y, z)


def RampeU(x=0, y=0, z=2.034):
    Boite(10.5 + 1, 0, 1.895 / 2, 19, UN, 0.3)
    bpy.ops.transform.rotate(
        value=-0.115, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )
    Name("TEUB1")
    ####
    Boite(0, 0, -1.5, 10, 10, 3)
    Name("TEUB")
    Soustraction("TEUB1", "TEUB")
    Boite(o=20.90 + UN / 2, p=(-2.8890 / 2 + UN / 2), q=2.034, r=UN, s=2.8890, t=1)
    Name("TEUB")
    Soustraction("TEUB1", "TEUB")
    ####
    Boite(o=21 - 3.5, p=-1.7334, q=2.439, r=7, s=UN, t=0.3)
    bpy.ops.transform.rotate(
        value=0.1188, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )
    Name("TEUB2")
    ####
    Boite(o=21 - 6.9336 - 0.15, p=(-2.8890 / 2 + UN / 2), q=3.15, r=0.3, s=5, t=5)
    Name("TEUB")
    Soustraction("TEUB2", "TEUB")
    Boite(o=20.90 + UN / 2, p=(-2.8890 / 2 + UN / 2), q=2.034, r=UN, s=2.8890, t=1)
    Name("TEUB")
    Soustraction("TEUB2", "TEUB")
    ####
    bpy.context.scene.objects.active = bpy.data.objects["TEUB1"]
    Joindre("TEUB1", "TEUB2")

    Colonne(x=10, y=0, z=0.35, dim_x=0.3, dim_y=0.3, dim_z=0.35)
    Name("TEUB4")
    Joindre("TEUB1", "TEUB4")

    Colonne(x=21 - 5.2, y=0, z=0.7, dim_x=0.3, dim_y=0.3, dim_z=0.7)
    Name("TEUB5")
    Joindre("TEUB4", "TEUB5")

    Colonne(x=21 + UN / 2, y=0, z=1, dim_x=0.3, dim_y=0.3, dim_z=1)
    Name("TEUB6")
    Joindre("TEUB5", "TEUB6")

    Colonne(x=21 + UN / 2, y=-1.7334, z=1, dim_x=0.3, dim_y=0.3, dim_z=1)
    Name("TEUB7")
    Joindre("TEUB6", "TEUB7")

    Colonne(x=21 - 5.2, y=-1.7334, z=1.35, dim_x=0.3, dim_y=0.3, dim_z=1.35)
    Name("TEUB8")
    Joindre("TEUB7", "TEUB8")

    Boite(o=20.90 + UN / 2, p=(-2.8890 / 2 + UN / 2), q=2.034, r=UN, s=2.8890, t=0.3)
    Name("RAMPEU")
    Joindre("TEUB8", "RAMPEU")
    bpy.ops.transform.rotate(
        value=-math.pi / 2, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    place_obj("RAMPEU", x + 0.1389, y - UN / 2, z)


print(
    "---------------------------------------------MODULES THREE QUARTER HOUSE----------------------------------------------------"
)


def ModuleEquilibre(x=0, y=0, z=1.5):
    # Soustraction1
    Boite(o=-6.7892, p=-0.4334, q=1.5, r=1.4445, s=2.0223, t=3)
    Name("TEUB0")
    Cylindre(-6.0669, -1.4445, 1.5, 0.5778, 0.5778, 1.5)
    Name("TEUB1")
    Soustraction("TEUB0", "TEUB1")
    # Soustraction2
    Boite(o=-11.2671, p=-1.7334, q=1.5, r=4.0446, s=4.6224, t=3)
    Name("TEUB2")
    Joindre("TEUB0", "TEUB2")
    Cylindre(-9.2448, -4.0446, 1.5, UN, UN, 1.5)
    Name("TEUB3")
    Soustraction("TEUB2", "TEUB3")
    # Cylindre Remplissage
    Cylindre(-3.4668, 0, 1.5, 2.3112, 2.3112, 1.5)
    Name("TEUB4")
    Joindre("TEUB2", "TEUB4")
    Cylindre(-7.5114, -2.3112, 1.5, 2.3112, 2.3112, 1.5)
    Name("TEUB5")
    Joindre("TEUB4", "TEUB5")
    Cylindre(-11.5560, -4.0446, 1.5, 3.4668, 3.4668, 1.5)
    Name("TEUB6")
    Joindre("TEUB5", "TEUB6")
    # Boites Remplissage
    Boite(o=-8.3781, p=-1.445, q=1.5, r=1.7334, s=4.0446, t=3)
    Name("TEUB7")
    Joindre("TEUB6", "TEUB7")
    Boite(o=-6.9336, p=-1.8779, q=1.5, r=UN, s=0.8667, t=3)
    Name("TEUB8")
    Joindre("TEUB7", "TEUB8")
    Boite(o=-4.7669, p=-0.2889, q=1.5, r=2.6001, s=1.7334, t=3)
    Name("TEUB9")
    Joindre("TEUB8", "TEUB9")
    Boite(o=0, p=0.8667, q=1.5, r=23.1121, s=1.7334, t=3)
    Name("MODULEEQUILIBRE")
    Joindre("TEUB9", "MODULEEQUILIBRE")
    Name("MODULEEQUILIBRE")
    place_obj("MODULEEQUILIBRE", x, y, z)


def ModuleRaccord(x=0, y=0, z=1.5):

    Cylindre(9.2448, 1.7334, 1.5, 3.4668, 3.4668, 1.5)
    Name("TEUB")
    Boite(o=11.2671, p=-0.8667, q=1.5, r=0.5778, s=0.5778, t=3)
    Name("TEUB1")

    Boite(o=9.8226, p=-2.0223, q=1.5, r=5.8956, s=1.7334, t=3)
    Name("TEUB2")

    Boite(o=14.4450, p=-2.3112, q=1.5, r=3.4668, s=2.3112, t=3)
    Name("TEUB3")

    Boite(o=17.9118, p=-1.7334, q=1.5, r=3.4668, s=3.4668, t=3)
    Name("TEUB4")

    Boite(o=18.4896, p=-4.6224, q=1.5, r=2.3112, s=2.3112, t=3)
    Name("TEUB5")

    Diamant(x=8.0191, y=1.7348, z=1.5, dim_x=1.7334, dim_y=1.7334, dim_z=3)
    Name("TEUB6")

    Boite(o=9.5337, p=-0.5778, q=1.5, r=2.8890, s=UN, t=3)
    Name("TEUB7")

    Cylindre(8.0892, -0.5778, 1.5, UN, UN, 1.5)
    Name("TEUB8")
    Soustraction("TEUB7", "TEUB8")

    Cylindre(11.5560, -0.5778, 1.5, UN, UN, 1.5)
    Name("TEUB10")
    Soustraction("TEUB1", "TEUB10")

    Boite(o=5.4891, p=0.8667, q=1.5, r=10.9782, s=1.7334, t=3)
    Name("MODULERACCORD")
    # joindre
    Joindre("TEUB", "TEUB1")
    Joindre("TEUB2", "TEUB3")
    Joindre("TEUB4", "TEUB5")
    Joindre("TEUB6", "TEUB7")
    place_obj("MODULERACCORD", x, y, z)


print(
    "---------------------------------------------ESCALIERS----------------------------------------------------"
)


def EscalierQHA(x=0, y=0, z=1.35):
    Boite(o=2.0112, p=0, q=1.35, r=UN, s=1.7334, t=0.3)

    for i in range(0, 10):
        Boite(
            o=1.4334 + 0.28536 * -i / 2,
            p=-0.43335,
            q=1.425 + 0.15 * i,
            r=0.28536,
            s=0.8667,
            t=0.15,
        )

    for i in range(0, 11):
        Boite(
            o=0.15408 + 0.28536 * i / 2,
            p=0.43335,
            q=0.075 + 0.15 * i,
            r=0.28536,
            s=0.8667,
            t=0.15,
        )

    Name("ESCALIERQHA")
    place_obj("ESCALIERQHA", x, y, z)


def EscalierQuarterHouseD(x=0, y=0, z=1.35):
    Cylindre(1.722, 0, 1.35, 1.7334, 1.7334, 0.15)
    Boite(o=0.8556, p=0, q=1.35, r=UN, s=1.7334, t=0.3)
    Name("TEUB2")
    Soustraction("Cylindre", "TEUB2")
    Boite(o=1.578, p=0, q=1.35, r=0.29, s=1.7334, t=0.3)
    for j in range(0, 10):
        Boite(
            o=0.15408 + 0.28536 * j / 2,
            p=0.43335,
            q=0.075 + 0.15 * j,
            r=0.28536,
            s=0.8667,
            t=0.15,
        )
    for j in range(0, 11):
        Boite(
            o=1.4334 - 0.28536 * j / 2,
            p=-0.43335,
            q=1.425 + 0.15 * j,
            r=0.28536,
            s=0.8667,
            t=0.15,
        )
    Boite(o=0.8556, p=0, q=1.35, r=UN, s=1.7334, t=0.3)
    Name("TEUB2")
    Soustraction("Cylindre", "TEUB2")
    Boite(o=1.578, p=0, q=1.35, r=0.29, s=1.7334, t=0.3)

    Name("ESCALIERQUARTERHOUSED")
    place_obj("ESCALIERQUARTERHOUSED", x, y, z)


############


def EscalierI(x=0, y=0, z=2.85):
    escalierY(
        OrigineX=0.0,
        OrigineY=0.8556 / 2,
        OrigineZ=3,
        HTot=3,
        LTot=4.0446,
        LargeurMarche=0.8556,
    )

    Boite(o=0, p=4.0446 + 0.8556, q=5.85, r=0.8556, s=0.8556, t=0.3)
    Name("TEUB")
    Joindre("Marche", "TEUB")

    Boite(o=0.8556 / 2 + 0.15, p=4.0446 + 0.8556 + UN / 2, q=3, r=0.3, s=UN, t=6)
    Name("TEUB1")
    Joindre("TEUB", "TEUB1")

    Boite(o=0, p=0, q=2.85, r=0.8556, s=0.8556, t=0.3)
    Name("ESCALIERI")

    Joindre("TEUB1", "ESCALIERI")

    place_obj("ESCALIERI", x - 0.8556 / 2 - 0.3, y + 0.8556 / 2, z)


############


def EscalierL(x=0, y=0, z=2.85):
    escalierY(
        OrigineX=2.8890 / 2 - UN / 2,
        OrigineY=-3.1201 - UN / 2,
        OrigineZ=0,
        HTot=3,
        LTot=3.1201,
        LargeurMarche=UN,
    )

    Colonne(x=0.8667, y=0, z=1.485, dim_x=0.3, dim_y=0.3, dim_z=1.485)
    Name("TEUB1")
    Joindre("Marche", "TEUB1")

    Colonne(x=-0.8667, y=0, z=1.485, dim_x=0.3, dim_y=0.3, dim_z=1.485)
    Name("TEUB2")
    Joindre("TEUB1", "TEUB2")

    Boite(o=0, p=0, q=2.85, r=2.8890, s=UN, t=0.3)
    Name("ESCALIERL")
    Joindre("ESCALIERL", "TEUB2")

    place_obj("ESCALIERL", x + 0.1389, y + UN / 2, z)


##########


def EscalierH(x=0, y=0, z=1.35):
    escalierY(
        OrigineX=-UN / 2,
        OrigineY=0,
        OrigineZ=0,
        HTot=1.5,
        LTot=-1.8490,
        LargeurMarche=UN,
    )
    escalierY(
        OrigineX=UN / 2,
        OrigineY=-1.8490,
        OrigineZ=1.5,
        HTot=1.5,
        LTot=1.8490,
        LargeurMarche=UN,
    )
    Boite(o=0, p=-1.8490 - UN - 0.15, q=1.5, r=UN * 2, s=0.3, t=3)  # pas obligatoire
    Name("TEUB")
    Joindre("TEUB", "Marche")
    Boite(o=0, p=-1.8490 - UN / 2, q=1.35, r=UN * 2, s=UN, t=0.3)
    Name("ESCALIERH")
    Joindre("ESCALIERH", "TEUB")
    bpy.ops.transform.rotate(
        value=-math.pi / 2, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    place_obj("ESCALIERH", x - 1.8490 - UN / 2, y, z)


##########


def EscalierU(x=0, y=0, z=1.35):

    Cylindre(x=0, y=-1.8490, z=1.5, dim_x=UN * 2 + 0.3, dim_y=UN * 2 + 0.3, dim_z=1.5)
    Name("TEUB1")

    Boite(o=0, p=-1.8490 + 1.5, q=2, r=5, s=3, t=6)
    Name("TEUB5")
    bpy.context.scene.objects.active = bpy.data.objects["TEUB5"]
    Soustraction("TEUB1", "TEUB5")

    Cylindre(x=0, y=-1.8490, z=1.5, dim_x=UN * 2, dim_y=UN * 2, dim_z=2)
    Name("TEUB")

    Soustraction("TEUB1", "TEUB")

    escalierY(
        OrigineX=-UN / 2,
        OrigineY=0,
        OrigineZ=0,
        HTot=1.5,
        LTot=-1.8490,
        LargeurMarche=UN,
    )
    escalierY(
        OrigineX=UN / 2,
        OrigineY=-1.8490,
        OrigineZ=1.5,
        HTot=1.5,
        LTot=1.8490,
        LargeurMarche=UN,
    )

    Cylindre(x=0, y=-1.8490, z=1.35, dim_x=UN * 2, dim_y=UN * 2, dim_z=0.15)
    Name("ESCALIERU")
    Boite(o=0, p=-1.8490 + UN / 2, q=1.35, r=UN * 2, s=UN, t=0.3)
    Name("TEUB2")
    Soustraction("ESCALIERU", "TEUB2")
    bpy.context.scene.objects.active = bpy.data.objects["ESCALIERU"]
    Joindre("ESCALIERU", "TEUB1")
    Joindre("ESCALIERU", "Marche")
    bpy.ops.transform.rotate(
        value=-math.pi / 2, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    place_obj("ESCALIERU", x - 1.8490, y - 0.15, z)


print(
    "---------------------------------------------ASCENSSEUR----------------------------------------------------"
)


def AscensseurA(x=0, y=0, z=1.5):
    Boite(0.5, 0.5, 1.5, 1, 1, 3)
    Name("TEUB")
    Cylindre(x=0, y=0, z=1.5, dim_x=2, dim_y=2, dim_z=1.5)
    Name("ASCENSSEURA")
    Joindre("TEUB", "ASCENSSEURA")
    place_obj("ASCENSSEURA", x, y, z)


def AscensseurA1(x=0, y=0, z=0):
    Boite(0.5, 0.5, 3, 1, 1, 6)
    Name("TEUB")
    Cylindre(x=0, y=0, z=3, dim_x=2, dim_y=2, dim_z=3)
    Name("ASCENSSEURA1")
    Joindre("TEUB", "ASCENSSEURA1")
    place_obj("ASCENSSEURA1", x, y, z)
    bpy.ops.transform.rotate(
        value=(-math.pi / 2), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    place_obj("ASCENSSEURA1", x - 1, y + 1, z)


def AscensseurB(x=0, y=0, z=0):
    Cylindre(x=0, y=0, z=3, dim_x=1.8, dim_y=1.8, dim_z=3)
    Name("TEUB")
    Boite(0, -1, 3, 1.8, 2, 6)
    Name("NEGATIVEASCENSSEURB")
    Soustraction("TEUB", "NEGATIVEASCENSSEURB")
    Boite(0, -0.35, 3, 1.8, 0.7, 6)
    Name("ASCENSSEURB")
    Joindre("TEUB", "ASCENSSEURB")
    place_obj("ASCENSSEURB", x, y, z + 3)


def AscensseurB1(x=0, y=0, z=0):
    AscensseurB()
    bpy.ops.transform.rotate(
        value=(-math.pi / 2), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )
    Name("ASCENSSEURB1")
    place_obj("ASCENSSEURB1", x + 0.7 / 2, y, z + 3)


#############################################################################################################
#############################################################################################################
#############################################################################################################

print(
    "---------------------------------------------CODE----------------------------------------------------"
)

#############################################################################################################
#############################################################################################################
#############################################################################################################


# TYPE D'AXE
CHOIX = random.randint(0, 4)

########################################################################
# CODE TYPE HOUSE QUART
########################################################################

if CHOIX == 0:

    XA = 8
    XB = 0
    # CHOIX ELEMENT DE COULOIR, PENTE
    X = random.randint(10, 15)
    Y = random.randint(-2, 2)
    Z = random.randint(10, 15)

    CHOIXCOILOIR = random.randint(0, 1)
    if CHOIXCOILOIR == 0:
        Couloir(VAR=X)
    if CHOIXCOILOIR == 1:
        CouloirH(x=0, y=0, z=0, VAR=X, VAR2=Y, VAR3=Z)

    # CHOIX TYPE DE CIRCULATION
    Circulation(VAR=X, VAR2=Y, VAR3=Z)

    # ESCALIER
    CHOIXESCALIER = random.randint(0, 3)
    if CHOIXESCALIER == 0:
        EscalierU(x=X * (UN / 2), y=-UN - UN / 2)
    if CHOIXESCALIER == 1:
        EscalierH(x=X * (UN / 2), y=-UN - UN / 2)
    if CHOIXESCALIER == 2:
        EscalierU(x=X * (UN / 2), y=+UN + UN / 2 + 0.3)
    if CHOIXESCALIER == 3:
        EscalierH(x=X * (UN / 2), y=+UN + UN / 2 + 0.3)

    # CHOIX 1ER FORMES

    LTalias = ListeTotal
    # cylindre,diamant,carre
    XA = random.randint(0, len(LTalias) - 1)
    # Quart,Demi,TQuart
    XB = random.randint(0, 1)
    # 1,2,3,4
    XC = 1

    LTalias[XA][XB][XC](x=X * UN / 2 + UN, y=Y * UN)
    del LTalias[XA]

    # CHOIX 2E FORMES

    XA = random.randint(0, len(LTalias) - 1)
    XB = random.randint(0, 2)
    XC = 2

    LTalias[XA][XB][XC](x=X * UN / 2, y=((Z * UN) / 2) + (Y * UN))
    del LTalias[XA]

    # CHOIX 3E FORMES

    XA = random.randint(0, len(LTalias) - 1)
    XB = random.randint(0, 2)
    XC = 0

    LTalias[XA][XB][XC](x=X * UN / 2 + UN, y=(-(Z * UN) / 2) + (Y * UN))

    def EtageCouloir():
        bpy.ops.object.select_all(action="TOGGLE")
        if CHOIXCOILOIR == 0:
            bpy.context.scene.objects["COULOIR"].select = False
        if CHOIXCOILOIR == 1:
            bpy.context.scene.objects["COULOIRH"].select = False
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={
                "value": (0, 0, 3),
                "constraint_axis": (False, False, False),
                "constraint_orientation": "GLOBAL",
                "mirror": False,
                "proportional": "DISABLED",
                "proportional_edit_falloff": "SMOOTH",
                "proportional_size": 1,
                "snap": False,
                "snap_target": "CLOSEST",
                "snap_point": (0, 0, 0),
                "snap_align": False,
                "snap_normal": (0, 0, 0),
                "texture_space": False,
                "release_confirm": False,
            },
        )
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={
                "value": (0, 0, 3),
                "constraint_axis": (False, False, False),
                "constraint_orientation": "GLOBAL",
                "mirror": False,
                "proportional": "DISABLED",
                "proportional_edit_falloff": "SMOOTH",
                "proportional_size": 1,
                "snap": False,
                "snap_target": "CLOSEST",
                "snap_point": (0, 0, 0),
                "snap_align": False,
                "snap_normal": (0, 0, 0),
                "texture_space": False,
                "release_confirm": False,
            },
        )
        CHOIXETAGE3 = random.randint(0, 1)
        # CHOIX D'UN 3e ETAGE OU NON
        if CHOIXETAGE3 == 0:
            bpy.ops.object.duplicate_move(
                OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
                TRANSFORM_OT_translate={
                    "value": (0, 0, 3),
                    "constraint_axis": (False, False, False),
                    "constraint_orientation": "GLOBAL",
                    "mirror": False,
                    "proportional": "DISABLED",
                    "proportional_edit_falloff": "SMOOTH",
                    "proportional_size": 1,
                    "snap": False,
                    "snap_target": "CLOSEST",
                    "snap_point": (0, 0, 0),
                    "snap_align": False,
                    "snap_normal": (0, 0, 0),
                    "texture_space": False,
                    "release_confirm": False,
                },
            )

    EtageCouloir()


########################################################################
# CODE TYPE HOUSE TROIS QUART 01
########################################################################


if CHOIX == 1:

    XA = 8
    XB = 0
    # CHOIX ELEMENT 1
    X = random.randint(10, 30)

    CHOIX = random.randint(0, 1)
    if CHOIX == 0:
        CouloirL(VAR=X)
        CouloirL(VAR=X, z=3)
    if CHOIX == 1:
        Couloir(VAR=X)
        Couloir(VAR=X, z=3)

    # CHOIX TYPE DE CIRCULATION

    Y = random.randint(-3, 3)
    Z = random.randint(7, 10)

    ModuleRaccord(x=X * UN / 2 + 5.5, y=0)

    # CHOIX 1ER FORMES

    LTalias3 = ListeTotalColonne
    # cylindre,diamant,carre
    XA = random.randint(0, len(LTalias3) - 1)
    # Quart,Demi,TQuart
    XB = random.randint(0, 2)
    # 1,2,3,4
    XC = 1

    LTalias3[XA][XB][XC](
        x=X * UN / 2 + 19.6452, y=-0.8673, z=4.5
    )  # 19.6452 taille totale de ModuleRaccord
    del LTalias3[XA]

    # CHOIX 2E FORMES

    XA = random.randint(0, len(LTalias3) - 1)
    XB = 0
    XC = 0

    LTalias3[XA][XB][XC](x=X * UN / 2 + 17.3340, y=-4.3335, z=4.5)
    del LTalias3[XA]

    # NOUVELLES CIRCULATION
    A = random.randint(30, 100)

    CouloirLarge(
        x=X * UN / 2 + 6.9336 - ((A * UN) / 2), y=-2.8890, z=0, VAR=A
    )  # trucs incomprehanssible sont la position par rapport a l'ensemble crée avant
    CouloirLarge(x=X * UN / 2 + 6.9336 - ((A * UN) / 2), y=-2.8890, z=3, VAR=A)
    # CHOIX 3E FORMES

    XA = random.randint(0, len(LTalias3) - 1)
    XB = random.randint(0, 2)
    XC = random.randint(2, 3)

    LTalias3[XA][XB][XC](x=X * UN / 2 + 6.9336 - ((A * UN)), y=-2.0223, z=4.5)

    if A > 50:
        ModuleEquilibre(x=-((4.5 / 5 * A) / 2), y=-3.7557 - 0.8667, z=1.5)

    bpy.context.scene.objects["MODULERACCORD"].select = True
    # bpy.context.scene.objects['PASSERELLE'].select=False
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (0, 0, 3),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "release_confirm": False,
        },
    )


########################################################################
# CODE TYPE HOUSE TROIS QUART 02
########################################################################

if CHOIX == 2:

    XA = 8
    XB = 0
    # CHOIX ELEMENT 1

    X = random.randint(10, 30)

    CHOIX = random.randint(0, 1)
    if CHOIX == 0:
        CouloirL(VAR=X)

    if CHOIX == 1:
        Couloir(VAR=X)

    # CHOIX TYPE DE CIRCULATION

    Y = random.randint(-3, 3)
    Z = random.randint(7, 10)

    ModuleRaccord(x=X * UN / 2 + 5.5, y=0)

    # CHOIX 1ER FORMES

    LTalias3 = ListeTotal
    # cylindre,diamant,carre
    XA = random.randint(0, len(LTalias3) - 1)
    # Quart,Demi,TQuart
    XB = random.randint(0, 2)
    # 1,2,3,4
    XC = 1

    LTalias3[XA][XB][XC](
        x=X * UN / 2 + 19.6452, y=-0.8673
    )  # 19.6452 taille totale de ModuleRaccord
    del LTalias3[XA]

    # CHOIX 2E FORMES

    XA = random.randint(0, len(LTalias3) - 1)
    XB = 0
    XC = 0

    LTalias3[XA][XB][XC](x=X * UN / 2 + 17.3340, y=-4.3335)
    del LTalias3[XA]

    # NOUVELLES CIRCULATION
    A = random.randint(30, 100)

    CouloirLarge(
        x=X * UN / 2 + 6.9336 - ((A * UN) / 2), y=-2.8890, z=0, VAR=A
    )  # trucs incomprehanssible sont la position par rapport a l'ensemble crée avant

    # CHOIX 3E FORMES

    XA = random.randint(0, len(LTalias3) - 1)
    XB = random.randint(0, 2)
    XC = random.randint(2, 3)

    LTalias3[XA][XB][XC](x=X * UN / 2 + 6.9336 - ((A * UN)), y=-2.0223)

    # POSSIBILITE D'EQUILIBRE > ModuleEquilibre

    if A > 50:
        ModuleEquilibre(x=-((4.5 / 5 * A) / 2), y=-3.7557 - 0.8667, z=1.5)

    # CirculationTQH(x=(X*UN-A*UN/2),y=-3.7557-A/4,VAR=A/2)


########################################################################
# CODE TYPE HOUSE QUART 03
########################################################################

if CHOIX == 3:

    XA = 8
    XB = 0
    # CHOIX AXE
    X = random.randint(18, 23)

    RampeI(x=0, y=0, z=1.36, VAR0=X)

    # CHOIX TYPE DE CIRCULATION

    Y = random.randint(-2, 2)
    Z = random.randint(5, 15)

    if Z > 7:

        Passerelle(VAR=X, VAR2=Y, VAR3=Z)

        # ESCALIER
        CHOIXESCALIER = random.randint(0, 1)
        if CHOIXESCALIER == 0:
            EscalierU(x=X * (UN / 2), y=-UN - UN / 2)
        if CHOIXESCALIER == 1:
            EscalierH(x=X * (UN / 2), y=-UN - UN / 2)

        # CHOIX 1ER FORMES

        LTalias2 = ListeTotalColonne
        # cylindre,diamant,carre
        XA = random.randint(0, len(LTalias2) - 1)
        # Quart,Demi,TQuart
        XB = random.randint(0, 1)
        # 1,2,3,4
        XC = 1

        LTalias2[XA][XB][XC](x=X * UN / 2 + UN, y=Y * UN, z=4.5)
        del LTalias2[XA]

        # CHOIX 2E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = random.randint(0, 2)
        XC = 2

        LTalias2[XA][XB][XC](x=X * UN / 2, y=((Z * UN) / 2) + (Y * UN), z=4.5)
        del LTalias2[XA]

        # CHOIX 3E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = random.randint(0, 2)
        XC = 0

        LTalias2[XA][XB][XC](x=X * UN / 2 + UN, y=(-(Z * UN) / 2) + (Y * UN), z=4.5)

        # CHOIX DES ETAGES

        def EtageRampeI():
            Circulation(z=4.5, VAR=X, VAR2=Y, VAR3=Z)
            bpy.ops.object.select_all(action="TOGGLE")
            bpy.ops.object.select_all(action="TOGGLE")
            bpy.context.scene.objects["RAMPEI"].select = False
            bpy.context.scene.objects["PASSERELLE"].select = False
            bpy.ops.object.duplicate_move(
                OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
                TRANSFORM_OT_translate={
                    "value": (0, 0, 3),
                    "constraint_axis": (False, False, False),
                    "constraint_orientation": "GLOBAL",
                    "mirror": False,
                    "proportional": "DISABLED",
                    "proportional_edit_falloff": "SMOOTH",
                    "proportional_size": 1,
                    "snap": False,
                    "snap_target": "CLOSEST",
                    "snap_point": (0, 0, 0),
                    "snap_align": False,
                    "snap_normal": (0, 0, 0),
                    "texture_space": False,
                    "release_confirm": False,
                },
            )
            # bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(0, 0, 3), "constraint_axis":(False, False, False), "constraint_orientation":'GLOBAL', "mirror":False, "proportional":'DISABLED', "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "texture_space":False, "release_confirm":False})

        EtageRampeI()

    if Z <= 7:
        Y = 0
        Passerelle(VAR=X, VAR2=Y, VAR3=Z)

        # ESCALIER
        CHOIXESCALIER = random.randint(0, 1)
        if CHOIXESCALIER == 0:
            EscalierU(x=X * (UN / 2), y=-UN - UN / 2)
        if CHOIXESCALIER == 1:
            EscalierH(x=X * (UN / 2), y=-UN - UN / 2)

        # CHOIX 1ER FORMES

        LTalias2 = ListeTotalColonne
        # cylindre,diamant,carre
        XA = random.randint(0, len(LTalias2) - 1)
        # Quart,Demi,TQuart
        XB = random.randint(1, 2)
        # 1,2,3,4
        XC = 1

        # CHOIX 2E FORMES
        if XB == (1):
            XC = 2
            LTalias2[XA][XB][XC](x=X * UN / 2, y=((Z * UN) / 2) + (Y * UN), z=4.5)

        # CHOIX 3E FORMES
        if XB == 2:
            XC = 1
            LTalias2[XA][XB][XC](x=X * UN / 2 + UN, y=((Z * UN) / 2) + (Y * UN), z=4.5)

        # CHOIX DES ASCENSSEUR

        AscensseurA(x=X * UN / 2 + UN - 1, y=(-(Z * UN) / 2) + (Y * UN) - 1, z=1.5)
        AscensseurA(x=X * UN / 2 + UN - 1, y=(-(Z * UN) / 2) + (Y * UN) - 1, z=4.5)

        # CHOIX DES ETAGES

        Circulation(z=4.5, VAR=X, VAR2=Y, VAR3=Z)
        bpy.ops.object.select_all(action="TOGGLE")
        bpy.ops.object.select_all(action="TOGGLE")
        bpy.context.scene.objects["RAMPEI"].select = False
        bpy.context.scene.objects["PASSERELLE"].select = False
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={
                "value": (0, 0, 3),
                "constraint_axis": (False, False, False),
                "constraint_orientation": "GLOBAL",
                "mirror": False,
                "proportional": "DISABLED",
                "proportional_edit_falloff": "SMOOTH",
                "proportional_size": 1,
                "snap": False,
                "snap_target": "CLOSEST",
                "snap_point": (0, 0, 0),
                "snap_align": False,
                "snap_normal": (0, 0, 0),
                "texture_space": False,
                "release_confirm": False,
            },
        )


########################################################################
# CODE TYPE SOUS-MARIN
########################################################################


if CHOIX == 4:

    # CHOIX ELEMENT 1
    X = 27

    Mur(x=0.15, VAR=X)  # ajouter VAR=X pour modification de la longueur

    # CHOIX TYPE DE CIRCULATION
    Y = 17  # inutile

    PassColonnes(
        x=-UN / 2, y=UN * X - 12.1338
    )  # ajouter VAR=X pour modification de la longueur

    # AJOUT DE RAMPES
    RampeU(x=0.15, y=0)

    CHOIX2 = random.randint(0, 1)

    if CHOIX2 == 0:

        # AJOUT D'ESCALIER
        EscalierL(x=0.15, y=UN * X)

        # AJOUT PASSERELLE 1
        Passerelle1(x=-UN / 2 - UN, y=6.9337 + 13.5 * UN / 2, VAR3=13.5)

        # CHOIX 1ER FORMES

        LTalias2 = ListeTotalColonne
        # cylindre,diamant,carre
        XA = random.randint(0, len(LTalias2) - 1)
        # Quart,Demi,TQuart
        XB = 0  # random.randint(0,1)
        # 1,2,3,4
        XC = 0

        LTalias2[XA][XB][XC](x=-UN * 2, y=6.9337 + 4.5 * UN, z=4.5)
        del LTalias2[XA]

        # AJOUT PASSERELLE 2
        Passerelle2(x=-UN / 2 - UN - UN, y=6.9337 + 9 * UN / 2 + UN * 4.5, VAR3=9)

        # CHOIX 2E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = 0
        XC = 0

        LTalias2[XA][XB][XC](x=-UN * 2 - UN, y=6.9337 + 4.5 * UN * 2, z=4.5)
        del LTalias2[XA]

        # AJOUT PASSERELLE 3
        Passerelle3(x=-UN / 2 - 3 * UN, y=6.9337 + 4.5 * UN / 2 + UN * 9, VAR3=4.5)

        # CHOIX 3E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = 0
        XC = 3

        LTalias2[XA][XB][XC](x=-UN * 3 - UN, y=6.9337 + 4.5 * UN * 2, z=4.5)

        # CHOIX DE L'ESCALIER PARASITE
        EscalierI(x=-UN, y=6.9337 + 13.5 * UN, z=2.85)

        # CHOIX DES ASCENSSEUR
        AscensseurB1(x=0.3, y=6.9337 + 13.5 * UN - 1.8 / 2)

        # CHOIX DES ETAGES
        CHOIX3 = random.randint(0, 1)

        if CHOIX3 == 0:

            bpy.ops.object.select_all(action="TOGGLE")
            bpy.ops.object.select_all(action="TOGGLE")
            bpy.context.scene.objects["RAMPEU"].select = False
            bpy.context.scene.objects["ESCALIERI"].select = False
            bpy.context.scene.objects["ESCALIERL"].select = False
            bpy.ops.object.duplicate_move(
                OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
                TRANSFORM_OT_translate={
                    "value": (0, 0, 3),
                    "constraint_axis": (False, False, False),
                    "constraint_orientation": "GLOBAL",
                    "mirror": False,
                    "proportional": "DISABLED",
                    "proportional_edit_falloff": "SMOOTH",
                    "proportional_size": 1,
                    "snap": False,
                    "snap_target": "CLOSEST",
                    "snap_point": (0, 0, 0),
                    "snap_align": False,
                    "snap_normal": (0, 0, 0),
                    "texture_space": False,
                    "release_confirm": False,
                },
            )

    if CHOIX2 == 1:

        # AJOUT PASSERELLE 1
        Passerelle1(x=-UN / 2 - UN, y=6.9337 + 13.5 * UN / 2, VAR3=13.5)

        # CHOIX 1ER FORMES

        LTalias2 = ListeTotalColonne
        # cylindre,diamant,carre
        XA = random.randint(0, len(LTalias2) - 1)
        # Quart,Demi,TQuart
        XB = 0  # random.randint(0,1)
        # 1,2,3,4
        XC = 0

        LTalias2[XA][XB][XC](x=-UN * 2, y=6.9337 + 4.5 * UN, z=4.5)
        del LTalias2[XA]

        # AJOUT PASSERELLE 2
        Passerelle2(x=-UN / 2 - UN - UN, y=6.9337 + 9 * UN / 2 + UN * 4.5, VAR3=9)

        # CHOIX 2E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = 1
        XC = 3

        LTalias2[XA][XB][XC](x=-UN * 2 - UN, y=6.9337 + 4.5 * UN * 2, z=4.5)
        del LTalias2[XA]

        # CHOIX 3E FORMES

        XA = random.randint(0, len(LTalias2) - 1)
        XB = random.randint(1, 2)
        XC = 1

        LTalias2[XA][XB][XC](x=0.3, y=X * UN, z=4.5)

        # CHOIX DE ESCALIER PARASITE
        EscalierI(x=-UN, y=6.9337 + 13.5 * UN, z=2.85)

        # CHOIX DE L'ASCENSSEUR
        if XB == 2:
            AscensseurB1(x=0.3, y=6.9337 + 13.5 * UN - 1.8 / 2)
        if XB == 1:
            AscensseurA1(x=0, y=X * UN, z=3)

        # CHOIX DES ASCENSSEUR
        CHOIX3 = random.randint(0, 1)
        if CHOIX3 == 0:

            bpy.ops.object.select_all(action="TOGGLE")
            bpy.ops.object.select_all(action="TOGGLE")
            bpy.context.scene.objects["RAMPEU"].select = False
            bpy.context.scene.objects["ESCALIERI"].select = False
            bpy.ops.object.duplicate_move(
                OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
                TRANSFORM_OT_translate={
                    "value": (0, 0, 3),
                    "constraint_axis": (False, False, False),
                    "constraint_orientation": "GLOBAL",
                    "mirror": False,
                    "proportional": "DISABLED",
                    "proportional_edit_falloff": "SMOOTH",
                    "proportional_size": 1,
                    "snap": False,
                    "snap_target": "CLOSEST",
                    "snap_point": (0, 0, 0),
                    "snap_align": False,
                    "snap_normal": (0, 0, 0),
                    "texture_space": False,
                    "release_confirm": False,
                },
            )
