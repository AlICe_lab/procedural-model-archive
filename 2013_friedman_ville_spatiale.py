# SPDX-FileCopyrightText: 2013 Nicolas Bourguignon
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# exercice Python Blender 17/10/2013
# développé sous Blender 2.64 / windows

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# ---------------------------------------------------------------------------------------------
# élément de composition:
# ---------------------------------------------------------------------------------------------

# définition des minicarrés:


def Minicarre(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.75, 0.75, 0.1))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))
    bpy.context.object.name = nom


# définition des colonnes de structures:


def MaBoite(pos_X, pos_Y, pos_Z, radius, depht):
    bpy.ops.mesh.primitive_cylinder_add(
        location=(pos_X, pos_Y, pos_Z + 0.1), radius=0.15, depth=3
    )


for k in range(0, 3):
    for j in range(0, 7):
        for i in range(0, 11):
            MaBoite(i * 3 - 15, j * 3 - 9, k * 3 + 1.4, 1, 1)

# ---------------------------------------------------------------------------------------------
# liste de l'entièreté des minicarrés forment les étages:
# ---------------------------------------------------------------------------------------------

maliste = []

for j in range(0, 12):
    for i in range(0, 20):
        for k in range(0, 3):
            Minicarre(
                i * 1.5 - (14.25),
                j * 1.5 - (8.25),
                k * 3 + 0.1,
                1,
                1,
                1,
                "Minicarre" + str(i) + "x" + str(j) + "y" + str(k) + "z",
            )
            maliste.append((i, j))


# ---------------------------------------------------------------------------------------------
# trou centrale rez:
# ---------------------------------------------------------------------------------------------

# hypothèse 1: trou de 6 horizontal

liste1_x = []
liste1_y = []
liste1_z = []
coord_liste_1 = []
element_delete_liste1 = []

for x in range(8, 10):
    liste1_x.append(x)
    rand_liste1_x = random.choice(liste1_x)

for y in range(2, 9):
    liste1_y.append(y)
    rand_liste1_y = random.choice(liste1_y)


for z in range(0, 3):
    element_delete_liste1.append(
        "Minicarre" + str(rand_liste1_x) + "x" + str(rand_liste1_y) + "y" + str(z) + "z"
    )
    element_delete_liste1.append(
        "Minicarre"
        + str(rand_liste1_x + 1)
        + "x"
        + str(rand_liste1_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste1.append(
        "Minicarre"
        + str(rand_liste1_x + 2)
        + "x"
        + str(rand_liste1_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste1.append(
        "Minicarre"
        + str(rand_liste1_x)
        + "x"
        + str(rand_liste1_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste1.append(
        "Minicarre"
        + str(rand_liste1_x + 1)
        + "x"
        + str(rand_liste1_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste1.append(
        "Minicarre"
        + str(rand_liste1_x + 2)
        + "x"
        + str(rand_liste1_y + 1)
        + "y"
        + str(z)
        + "z"
    )

# hypothèse 2: trou de 6 vertical

liste2_x = []
liste2_y = []
liste2_z = []
coord_liste_2 = []
element_delete_liste2 = []

for x in range(8, 11):
    liste2_x.append(x)
    rand_liste2_x = random.choice(liste2_x)

for y in range(2, 8):
    liste2_y.append(y)
    rand_liste2_y = random.choice(liste2_y)

for z in range(0, 3):
    element_delete_liste2.append(
        "Minicarre" + str(rand_liste2_x) + "x" + str(rand_liste2_y) + "y" + str(z) + "z"
    )
    element_delete_liste2.append(
        "Minicarre"
        + str(rand_liste2_x)
        + "x"
        + str(rand_liste2_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste2.append(
        "Minicarre"
        + str(rand_liste2_x)
        + "x"
        + str(rand_liste2_y + 2)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste2.append(
        "Minicarre"
        + str(rand_liste2_x + 1)
        + "x"
        + str(rand_liste2_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste2.append(
        "Minicarre"
        + str(rand_liste2_x + 1)
        + "x"
        + str(rand_liste2_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste2.append(
        "Minicarre"
        + str(rand_liste2_x + 1)
        + "x"
        + str(rand_liste2_y + 2)
        + "y"
        + str(z)
        + "z"
    )

element_delete_trou_centre = [(element_delete_liste1), (element_delete_liste2)]
rand_element_delete_trou_centre = random.choice(element_delete_trou_centre)


# ---------------------------------------------------------------------------------------------
# trou de gauche:
# ---------------------------------------------------------------------------------------------


liste3_x = []
liste3_y = []
coord_liste_3 = []
element_delete_liste3 = []

for x in range(2, 7):
    liste3_x.append(x)
    rand_liste3_x = random.choice(liste3_x)

for y in range(2, 8):
    liste3_y.append(y)
    rand_liste3_y = random.choice(liste3_y)

for z in range(0, 3):
    element_delete_liste3.append(
        "Minicarre" + str(rand_liste3_x) + "x" + str(rand_liste3_y) + "y" + str(z) + "z"
    )
    element_delete_liste3.append(
        "Minicarre"
        + str(rand_liste3_x + 1)
        + "x"
        + str(rand_liste3_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste3.append(
        "Minicarre"
        + str(rand_liste3_x)
        + "x"
        + str(rand_liste3_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste3.append(
        "Minicarre"
        + str(rand_liste3_x + 1)
        + "x"
        + str(rand_liste3_y + 1)
        + "y"
        + str(z)
        + "z"
    )

rand_element_delete_trou_gauche = element_delete_liste3

# ---------------------------------------------------------------------------------------------
# trou de droite:
# ---------------------------------------------------------------------------------------------

liste4_x = []
liste4_y = []
coord_liste_4 = []
element_delete_liste4 = []

for x in range(12, 17):
    liste4_x.append(x)
    rand_liste4_x = random.choice(liste4_x)

for y in range(2, 8):
    liste4_y.append(y)
    rand_liste4_y = random.choice(liste4_y)

for z in range(0, 3):
    element_delete_liste4.append(
        "Minicarre" + str(rand_liste4_x) + "x" + str(rand_liste4_y) + "y" + str(z) + "z"
    )
    element_delete_liste4.append(
        "Minicarre"
        + str(rand_liste4_x + 1)
        + "x"
        + str(rand_liste4_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste4.append(
        "Minicarre"
        + str(rand_liste4_x)
        + "x"
        + str(rand_liste4_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste4.append(
        "Minicarre"
        + str(rand_liste4_x + 1)
        + "x"
        + str(rand_liste4_y + 1)
        + "y"
        + str(z)
        + "z"
    )

rand_element_delete_trou_droite = element_delete_liste4

# ---------------------------------------------------------------------------------------------
# trou supplémentaire de droite ou de gauche:
# ---------------------------------------------------------------------------------------------

# trou au hasard à gauche ou à droite:

trou_sup = ["gauche", "droite"]
rand_trou_sup = random.choice(trou_sup)


# position en fonction du Minicarre de base du trou gauche ou droit
liste5_x = []
liste5_y = []
element_delete_liste5 = []

if rand_trou_sup == "gauche":

    if 1 < rand_liste3_x < 7 and 1 < rand_liste3_y < 5:
        for x in range(2, 7):
            liste5_x.append(x)
            rand_liste5_x = random.choice(liste5_x)
        for y in range(6, 9):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)

    if 1 < rand_liste3_x < 7 and 5 < rand_liste3_y < 9:
        for x in range(2, 7):
            liste5_x.append(x)
            rand_liste5_x = random.choice(liste5_x)
        for y in range(2, 5):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)

    if rand_liste3_x == 2 and rand_liste3_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(2, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(4, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste3_x == 3 and rand_liste3_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(2, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(5, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste3_x == 4 and rand_liste3_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(2, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                liste5_x.append(2)
                liste5_x.append(6)
                rand_liste5_x = random.choice(liste5_x)

    if rand_liste3_x == 5 and rand_liste3_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(2, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(2, 4):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste3_x == 6 and rand_liste3_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(2, 7):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(2, 5):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

if rand_trou_sup == "droite":
    if 11 < rand_liste4_x < 17 and 1 < rand_liste4_y < 5:
        for x in range(12, 17):
            liste5_x.append(x)
            rand_liste5_x = random.choice(liste5_x)
        for y in range(6, 9):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)

    if 11 < rand_liste4_x < 17 and 5 < rand_liste4_y < 9:
        for x in range(12, 17):
            liste5_x.append(x)
            rand_liste5_x = random.choice(liste5_x)
        for y in range(2, 5):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)

    if rand_liste4_x == 12 and rand_liste4_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(12, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(14, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste4_x == 13 and rand_liste4_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(12, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(15, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste4_x == 14 and rand_liste4_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(12, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                liste5_x.append(12)
                liste5_x.append(16)
                rand_liste5_x = random.choice(liste5_x)

    if rand_liste4_x == 15 and rand_liste4_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(12, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(12, 14):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

    if rand_liste4_x == 16 and rand_liste4_y == 5:
        for y in range(2, 6):
            liste5_y.append(y)
            rand_liste5_y = random.choice(liste5_y)
            if 1 < rand_liste5_y < 4:
                for x in range(12, 17):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)
            if 3 < rand_liste5_y < 6:
                for x in range(12, 15):
                    liste5_x.append(x)
                    rand_liste5_x = random.choice(liste5_x)

for z in range(0, 3):
    element_delete_liste5.append(
        "Minicarre" + str(rand_liste5_x) + "x" + str(rand_liste5_y) + "y" + str(z) + "z"
    )
    element_delete_liste5.append(
        "Minicarre"
        + str(rand_liste5_x + 1)
        + "x"
        + str(rand_liste5_y)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste5.append(
        "Minicarre"
        + str(rand_liste5_x)
        + "x"
        + str(rand_liste5_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste5.append(
        "Minicarre"
        + str(rand_liste5_x + 1)
        + "x"
        + str(rand_liste5_y + 1)
        + "y"
        + str(z)
        + "z"
    )


# -----------------------------------------------------------------------------------------------------------------------------------------
# trou périphérie:
# -----------------------------------------------------------------------------------------------------------------------------------------

liste6_x = []
liste6_y = []
element_delete_liste6_horizontal_haut = []
element_delete_liste6_horizontal_bas = []

# Cas ou le trou supplémentaire est à gauche:

if rand_trou_sup == "gauche":
    liste6_y = [0, 11]
    for x in range(12, 17):
        liste6_x.append(x)
        rand_liste6_x = random.choice(liste6_x)
    if 11 < rand_liste4_x < 17 and 1 < rand_liste4_y < 5:
        rand_liste6_y = 11
        for z in range(0, 3):
            element_delete_liste6_horizontal_haut.append(
                "Minicarre"
                + str(rand_liste6_x)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_liste6_horizontal_haut.append(
                "Minicarre"
                + str(rand_liste6_x + 1)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
        element_delete_liste6 = element_delete_liste6_horizontal_haut
    if 11 < rand_liste4_x < 17 and 5 < rand_liste4_y < 9:
        rand_liste6_y = 0
        for z in range(0, 3):
            element_delete_liste6_horizontal_bas.append(
                "Minicarre"
                + str(rand_liste6_x)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_liste6_horizontal_bas.append(
                "Minicarre"
                + str(rand_liste6_x + 1)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
        element_delete_liste6 = element_delete_liste6_horizontal_bas
    if 11 < rand_liste4_x < 17 and rand_liste4_y == 5:
        rand_liste6_y = random.choice(liste6_y)
        if rand_liste6_y == 0:
            for z in range(0, 3):
                element_delete_liste6_horizontal_bas.append(
                    "Minicarre"
                    + str(rand_liste6_x)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_liste6_horizontal_bas.append(
                    "Minicarre"
                    + str(rand_liste6_x + 1)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            element_delete_liste6 = element_delete_liste6_horizontal_bas
        if rand_liste6_y == 11:
            for z in range(0, 3):
                element_delete_liste6_horizontal_haut.append(
                    "Minicarre"
                    + str(rand_liste6_x)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_liste6_horizontal_haut.append(
                    "Minicarre"
                    + str(rand_liste6_x + 1)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            element_delete_liste6 = element_delete_liste6_horizontal_haut

# Cas ou le trou supplémentaire est à droite:

if rand_trou_sup == "droite":
    print("droite")
    liste6_y = (0, 11)
    for x in range(2, 7):
        liste6_x.append(x)
        rand_liste6_x = random.choice(liste6_x)
    if 1 < rand_liste3_x < 7 and 1 < rand_liste3_y < 5:
        rand_liste6_y = 11
        for z in range(0, 3):
            element_delete_liste6_horizontal_haut.append(
                "Minicarre"
                + str(rand_liste6_x)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_liste6_horizontal_haut.append(
                "Minicarre"
                + str(rand_liste6_x + 1)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
        element_delete_liste6 = element_delete_liste6_horizontal_haut
    if 1 < rand_liste3_x < 7 and 5 < rand_liste3_y < 9:
        rand_liste6_y = 0
        for z in range(0, 3):
            element_delete_liste6_horizontal_bas.append(
                "Minicarre"
                + str(rand_liste6_x)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_liste6_horizontal_bas.append(
                "Minicarre"
                + str(rand_liste6_x + 1)
                + "x"
                + str(rand_liste6_y)
                + "y"
                + str(z)
                + "z"
            )
        element_delete_liste6 = element_delete_liste6_horizontal_bas
    if 1 < rand_liste3_x < 7 and rand_liste3_y == 5:
        rand_liste6_y = random.choice(liste6_y)
        if rand_liste6_y == 0:
            for z in range(0, 3):
                element_delete_liste6_horizontal_bas.append(
                    "Minicarre"
                    + str(rand_liste6_x)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_liste6_horizontal_bas.append(
                    "Minicarre"
                    + str(rand_liste6_x + 1)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            element_delete_liste6 = element_delete_liste6_horizontal_bas
        if rand_liste6_y == 11:
            for z in range(0, 3):
                element_delete_liste6_horizontal_haut.append(
                    "Minicarre"
                    + str(rand_liste6_x)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_liste6_horizontal_haut.append(
                    "Minicarre"
                    + str(rand_liste6_x + 1)
                    + "x"
                    + str(rand_liste6_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            element_delete_liste6 = element_delete_liste6_horizontal_haut

# -----------------------------------------------------------------------------------------------------------------------------------------
# Agrandissement du trou périphérie en fonction des étages:
# -----------------------------------------------------------------------------------------------------------------------------------------

# agrandissement 2 ème étage :

for z in range(1, 3):
    element_delete_liste6_horizontal_haut.append(
        "Minicarre"
        + str(rand_liste6_x)
        + "x"
        + str(rand_liste6_y - 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste6_horizontal_haut.append(
        "Minicarre"
        + str(rand_liste6_x + 1)
        + "x"
        + str(rand_liste6_y - 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste6_horizontal_bas.append(
        "Minicarre"
        + str(rand_liste6_x)
        + "x"
        + str(rand_liste6_y + 1)
        + "y"
        + str(z)
        + "z"
    )
    element_delete_liste6_horizontal_bas.append(
        "Minicarre"
        + str(rand_liste6_x + 1)
        + "x"
        + str(rand_liste6_y + 1)
        + "y"
        + str(z)
        + "z"
    )


# agrandissement 3 ème étage:

z = 2
element_delete_liste6_horizontal_haut.append(
    "Minicarre" + str(rand_liste6_x) + "x" + str(rand_liste6_y - 2) + "y" + str(z) + "z"
)
element_delete_liste6_horizontal_haut.append(
    "Minicarre"
    + str(rand_liste6_x + 1)
    + "x"
    + str(rand_liste6_y - 2)
    + "y"
    + str(z)
    + "z"
)
element_delete_liste6_horizontal_bas.append(
    "Minicarre" + str(rand_liste6_x) + "x" + str(rand_liste6_y + 2) + "y" + str(z) + "z"
)
element_delete_liste6_horizontal_bas.append(
    "Minicarre"
    + str(rand_liste6_x + 1)
    + "x"
    + str(rand_liste6_y + 2)
    + "y"
    + str(z)
    + "z"
)

# -----------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation au 1ère étage:
# -----------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------

# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation du 1ère étage au bord du trou central:
# -----------------------------------------------------------------------------------------------------------------------------------------

# liste_1 = position horizontale et liste_2 = position verticale

up_or_down = ("up", "down")
rand_up_or_down = random.choice(up_or_down)
left_or_right = ("left", "right")
rand_left_or_right = random.choice(left_or_right)

element_vegetation_liste1_left = []
element_vegetation_liste1_right = []
element_vegetation_liste1_up_left = []
element_vegetation_liste1_up_middle = []
element_vegetation_liste1_up_right = []
element_vegetation_liste1_down_left = []
element_vegetation_liste1_down_middle = []
element_vegetation_liste1_down_right = []
element_vegetation_liste2_left_down = []
element_vegetation_liste2_left_middle = []
element_vegetation_liste2_left_up = []
element_vegetation_liste2_right_down = []
element_vegetation_liste2_right_middle = []
element_vegetation_liste2_right_up = []
element_vegetation_liste2_up = []
element_vegetation_liste2_down = []

# bloc de végétation de 2 de long à gauche du trou central en position horizontal

element_vegetation_liste1_left.append(
    "Minicarre" + str(rand_liste1_x - 1) + "x" + str(rand_liste1_y) + "y" + str(z) + "z"
)
element_vegetation_liste1_left.append(
    "Minicarre"
    + str(rand_liste1_x - 1)
    + "x"
    + str(rand_liste1_y + 1)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 2 de long à droite du trou central en position horizontal

element_vegetation_liste1_right.append(
    "Minicarre" + str(rand_liste1_x + 3) + "x" + str(rand_liste1_y) + "y" + str(z) + "z"
)
element_vegetation_liste1_right.append(
    "Minicarre"
    + str(rand_liste1_x + 3)
    + "x"
    + str(rand_liste1_y + 1)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 3 de large au dessus du trou central en position horizontal

element_vegetation_liste1_up_left.append(
    "Minicarre" + str(rand_liste1_x) + "x" + str(rand_liste1_y + 2) + "y" + str(z) + "z"
)
element_vegetation_liste1_up_middle.append(
    "Minicarre"
    + str(rand_liste1_x + 1)
    + "x"
    + str(rand_liste1_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste1_up_right.append(
    "Minicarre"
    + str(rand_liste1_x + 2)
    + "x"
    + str(rand_liste1_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste1_up = (
    element_vegetation_liste1_up_right
    + element_vegetation_liste1_up_middle
    + element_vegetation_liste1_up_left
)

# bloc de végétation de 3 de large en dessous du trou central en position horizontal

element_vegetation_liste1_down_left.append(
    "Minicarre" + str(rand_liste1_x) + "x" + str(rand_liste1_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste1_down_middle.append(
    "Minicarre"
    + str(rand_liste1_x + 1)
    + "x"
    + str(rand_liste1_y - 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste1_down_right.append(
    "Minicarre"
    + str(rand_liste1_x + 2)
    + "x"
    + str(rand_liste1_y - 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste1_down = (
    element_vegetation_liste1_down_right
    + element_vegetation_liste1_down_middle
    + element_vegetation_liste1_down_left
)

if rand_element_delete_trou_centre == (element_delete_liste1):
    z = 0
    if rand_liste1_x == 8 and rand_liste1_y == 2:
        element_vegetation_liste1 = (
            element_vegetation_liste1_right + element_vegetation_liste1_up
        )

    if rand_liste1_x == 9 and rand_liste1_y == 2:
        element_vegetation_liste1 = (
            element_vegetation_liste1_left + element_vegetation_liste1_up
        )

    if rand_liste1_x == 8 and rand_liste1_y == 8:
        element_vegetation_liste1 = (
            element_vegetation_liste1_right + element_vegetation_liste1_down
        )

    if rand_liste1_x == 9 and rand_liste1_y == 8:
        element_vegetation_liste1 = (
            element_vegetation_liste1_left + element_vegetation_liste1_down
        )

    if rand_liste1_x == 8 and 2 < rand_liste1_y < 8:
        if rand_up_or_down == "up":
            element_vegetation_liste1 = (
                element_vegetation_liste1_right + element_vegetation_liste1_up
            )
        if rand_up_or_down == "down":
            element_vegetation_liste1 = (
                element_vegetation_liste1_right + element_vegetation_liste1_down
            )

    if rand_liste1_x == 9 and 2 < rand_liste1_y < 8:
        if rand_up_or_down == "up":
            element_vegetation_liste1 = (
                element_vegetation_liste1_left + element_vegetation_liste1_up
            )
        if rand_up_or_down == "down":
            element_vegetation_liste1 = (
                element_vegetation_liste1_left + element_vegetation_liste1_down
            )

    # bloc de végétation de 2 de large au dessus du trou central en position vertical

element_vegetation_liste2_up.append(
    "Minicarre" + str(rand_liste2_x) + "x" + str(rand_liste2_y + 3) + "y" + str(z) + "z"
)
element_vegetation_liste2_up.append(
    "Minicarre"
    + str(rand_liste2_x + 1)
    + "x"
    + str(rand_liste2_y + 3)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 2 de large en dessous du trou central en position vertical

element_vegetation_liste2_down.append(
    "Minicarre" + str(rand_liste2_x) + "x" + str(rand_liste2_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste2_down.append(
    "Minicarre"
    + str(rand_liste2_x + 1)
    + "x"
    + str(rand_liste2_y - 1)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 3 de long à gauche du trou central en position vertical

element_vegetation_liste2_left_down.append(
    "Minicarre" + str(rand_liste2_x - 1) + "x" + str(rand_liste2_y) + "y" + str(z) + "z"
)
element_vegetation_liste2_left_middle.append(
    "Minicarre"
    + str(rand_liste2_x - 1)
    + "x"
    + str(rand_liste2_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste2_left_up.append(
    "Minicarre"
    + str(rand_liste2_x - 1)
    + "x"
    + str(rand_liste2_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste2_left = (
    element_vegetation_liste2_left_up
    + element_vegetation_liste2_left_middle
    + element_vegetation_liste2_left_down
)

# bloc de végétation de 3 de long à droite du trou central en position vertical

element_vegetation_liste2_right_down.append(
    "Minicarre" + str(rand_liste2_x + 2) + "x" + str(rand_liste2_y) + "y" + str(z) + "z"
)
element_vegetation_liste2_right_middle.append(
    "Minicarre"
    + str(rand_liste2_x + 2)
    + "x"
    + str(rand_liste2_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste2_right_up.append(
    "Minicarre"
    + str(rand_liste2_x + 2)
    + "x"
    + str(rand_liste2_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste2_right = (
    element_vegetation_liste2_right_up
    + element_vegetation_liste2_right_middle
    + element_vegetation_liste2_right_down
)

if rand_element_delete_trou_centre == (element_delete_liste2):
    z = 0
    if rand_liste2_x == 8 and rand_liste2_y == 2:
        element_vegetation_liste2 = (
            element_vegetation_liste2_right + element_vegetation_liste2_up
        )

    if rand_liste2_x == 9 and rand_liste2_y == 2:
        if rand_left_or_right == "left":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_up
            )
        if rand_left_or_right == "right":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_up
            )

    if rand_liste2_x == 10 and rand_liste2_y == 2:
        element_vegetation_liste2 = (
            element_vegetation_liste2_left + element_vegetation_liste2_up
        )

    if rand_liste2_x == 8 and rand_liste2_y == 7:
        element_vegetation_liste2 = (
            element_vegetation_liste2_right + element_vegetation_liste2_down
        )

    if rand_liste2_x == 9 and rand_liste2_y == 7:
        if rand_left_or_right == "left":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_down
            )
        if rand_left_or_right == "right":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_down
            )

    if rand_liste2_x == 10 and rand_liste2_y == 7:
        element_vegetation_liste2 = (
            element_vegetation_liste2_left + element_vegetation_liste2_up
        )

    if rand_liste2_x == 8 and 2 < rand_liste2_y < 7:
        if rand_up_or_down == "up":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_up
            )
        if rand_up_or_down == "down":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_down
            )

    if rand_liste2_x == 9 and 2 < rand_liste2_y < 7:
        if rand_left_or_right == "left" and rand_up_or_down == "up":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_up
            )
        if rand_left_or_right == "left" and rand_up_or_down == "down":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_down
            )
        if rand_left_or_right == "right" and rand_up_or_down == "up":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_up
            )
        if rand_left_or_right == "right" and rand_up_or_down == "down":
            element_vegetation_liste2 = (
                element_vegetation_liste2_right + element_vegetation_liste2_down
            )

    if rand_liste2_x == 10 and 2 < rand_liste2_y < 7:
        if rand_up_or_down == "up":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_up
            )
        if rand_up_or_down == "down":
            element_vegetation_liste2 = (
                element_vegetation_liste2_left + element_vegetation_liste2_down
            )

# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation du 2ère étage au bord du trou central:
# -----------------------------------------------------------------------------------------------------------------------------------------

up_or_down_bis = ("up", "down")
rand_up_or_down_bis = random.choice(up_or_down)
left_or_right_bis = ("left", "right")
rand_left_or_right_bis = random.choice(left_or_right)


# liste1: horizontal

if rand_element_delete_trou_centre == (element_delete_liste1):
    z = 1
    if (
        element_vegetation_liste1
        == element_vegetation_liste1_left + element_vegetation_liste1_up
    ):
        if rand_left_or_right_bis == "left":
            element_vegetation_liste1_up_étage_2 = (
                element_vegetation_liste1_up_middle + element_vegetation_liste1_up_left
            )
        if rand_left_or_right_bis == "right":
            element_vegetation_liste1_up_étage_2 = (
                element_vegetation_liste1_up_middle + element_vegetation_liste1_up_right
            )
        element_vegetation_liste1_étage_2 = (
            element_vegetation_liste1_left + element_vegetation_liste1_up_étage_2
        )

    if (
        element_vegetation_liste1
        == element_vegetation_liste1_right + element_vegetation_liste1_up
    ):
        if rand_left_or_right_bis == "left":
            element_vegetation_liste1_up_étage_2 = (
                element_vegetation_liste1_up_middle + element_vegetation_liste1_up_left
            )
        if rand_left_or_right_bis == "right":
            element_vegetation_liste1_up_étage_2 = (
                element_vegetation_liste1_up_middle + element_vegetation_liste1_up_right
            )
        element_vegetation_liste1_étage_2 = (
            element_vegetation_liste1_right + element_vegetation_liste1_up_étage_2
        )

    if (
        element_vegetation_liste1
        == element_vegetation_liste1_left + element_vegetation_liste1_down
    ):
        if rand_left_or_right_bis == "left":
            element_vegetation_liste1_down_étage_2 = (
                element_vegetation_liste1_down_middle
                + element_vegetation_liste1_down_left
            )
        if rand_left_or_right_bis == "right":
            element_vegetation_liste1_down_étage_2 = (
                element_vegetation_liste1_down_middle
                + element_vegetation_liste1_down_right
            )
        element_vegetation_liste1_étage_2 = (
            element_vegetation_liste1_left + element_vegetation_liste1_down_étage_2
        )

    if (
        element_vegetation_liste1
        == element_vegetation_liste1_right + element_vegetation_liste1_down
    ):
        if rand_left_or_right_bis == "left":
            element_vegetation_liste1_down_étage_2 = (
                element_vegetation_liste1_down_middle
                + element_vegetation_liste1_down_left
            )
        if rand_left_or_right_bis == "right":
            element_vegetation_liste1_down_étage_2 = (
                element_vegetation_liste1_down_middle
                + element_vegetation_liste1_down_right
            )
        element_vegetation_liste1_étage_2 = (
            element_vegetation_liste1_right + element_vegetation_liste1_down_étage_2
        )

# liste2: vertical

if rand_element_delete_trou_centre == (element_delete_liste2):
    z = 1
    if (
        element_vegetation_liste2
        == element_vegetation_liste2_left + element_vegetation_liste2_up
    ):
        if rand_up_or_down_bis == "up":
            element_vegetation_liste2_left_étage_2 = (
                element_vegetation_liste2_left_middle
                + element_vegetation_liste2_left_up
            )
        if rand_up_or_down_bis == "down":
            element_vegetation_liste2_left_étage_2 = (
                element_vegetation_liste2_left_middle
                + element_vegetation_liste2_left_down
            )
        element_vegetation_liste2_étage_2 = (
            element_vegetation_liste2_left_étage_2 + element_vegetation_liste2_up
        )

    if (
        element_vegetation_liste2
        == element_vegetation_liste2_left + element_vegetation_liste2_down
    ):
        if rand_up_or_down_bis == "up":
            element_vegetation_liste2_left_étage_2 = (
                element_vegetation_liste2_left_middle
                + element_vegetation_liste2_left_up
            )
        if rand_up_or_down_bis == "down":
            element_vegetation_liste2_left_étage_2 = (
                element_vegetation_liste2_left_middle
                + element_vegetation_liste2_left_down
            )
        element_vegetation_liste2_étage_2 = (
            element_vegetation_liste2_left_étage_2 + element_vegetation_liste2_down
        )

    if (
        element_vegetation_liste2
        == element_vegetation_liste2_right + element_vegetation_liste2_down
    ):
        if rand_up_or_down_bis == "up":
            element_vegetation_liste2_right_étage_2 = (
                element_vegetation_liste2_right_middle
                + element_vegetation_liste2_right_up
            )
        if rand_up_or_down_bis == "down":
            element_vegetation_liste2_right_étage_2 = (
                element_vegetation_liste2_right_middle
                + element_vegetation_liste2_right_down
            )
        element_vegetation_liste2_étage_2 = (
            element_vegetation_liste2_right_étage_2 + element_vegetation_liste2_down
        )

    if (
        element_vegetation_liste2
        == element_vegetation_liste2_right + element_vegetation_liste2_up
    ):
        if rand_up_or_down_bis == "up":
            element_vegetation_liste2_right_étage_2 = (
                element_vegetation_liste2_right_middle
                + element_vegetation_liste2_right_up
            )
        if rand_up_or_down_bis == "down":
            element_vegetation_liste2_right_étage_2 = (
                element_vegetation_liste2_right_middle
                + element_vegetation_liste2_right_down
            )
        element_vegetation_liste2_étage_2 = (
            element_vegetation_liste2_right_étage_2 + element_vegetation_liste2_up
        )

# -----------------------------------------------------------------------------------------------------------------------------------------
# évasement du trou central au 3ème étage:
# -----------------------------------------------------------------------------------------------------------------------------------------

element_delete_évasement_liste1_étage_3 = []
element_delete_évasement_liste2_étage_3 = []

if rand_element_delete_trou_centre == (element_delete_liste1):
    z = 2
    element_delete_évasement_liste1_étage_3 = element_vegetation_liste1_étage_2

if rand_element_delete_trou_centre == (element_delete_liste2):
    z = 2
    element_delete_évasement_liste2_étage_3 = element_vegetation_liste2_étage_2


# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation du 1ère étage au bord du trou gauche, droite et leur variante en fonction de la position du trou supp à gauche et à droite:
# -----------------------------------------------------------------------------------------------------------------------------------------

# parenthèse possibilité de position de la végétation: cette partie du script permet de réaliser un double choix dans une même liste

# /////////////////////////////////////////////////////////////////////////////////////////////////////

# cas ou le trou ne se situe contre aucun bord
possibilité_de_4orientation = ["left", "right", "up", "down"]

position1 = random.randint(0, len(possibilité_de_4orientation) - 1)
elem = possibilité_de_4orientation[position1]

nb_elem = 2
liste_position = []
while nb_elem > 0:
    position1 = random.randint(0, len(possibilité_de_4orientation) - 1)
    while (
        position1 in liste_position
    ):  # tant que le tirage redonne une position déjà choisie
        position1 = random.randint(0, len(possibilité_de_4orientation) - 1)
    liste_position.append(position1)
    nb_elem = nb_elem - 1
rand_liste_4orientation = []
for index in rand_liste_4orientation:
    rand_liste_4orientation.append(possibilité_de_4orientation[index])


# cas ou le trou ne se situe contre le bord inférieur
possibilité_de_3orientation_down = ["left", "right", "up"]

position1 = random.randint(0, len(possibilité_de_3orientation_down) - 1)
elem = possibilité_de_3orientation_down[position1]

nb_elem = 2
liste_position = []
while nb_elem > 0:
    position1 = random.randint(0, len(possibilité_de_3orientation_down) - 1)
    while position1 in liste_position:
        position1 = random.randint(0, len(possibilité_de_3orientation_down) - 1)
    liste_position.append(position1)
    nb_elem = nb_elem - 1
rand_liste_3orientation_down = []
for index in rand_liste_3orientation_down:
    rand_liste_3orientation_down.append(possibilité_de_3orientation_down[index])


# cas ou le trou ne se situe contre le bord supérieur
possibilité_de_3orientation_up = ["left", "right", "down"]

position1 = random.randint(0, len(possibilité_de_3orientation_up) - 1)
elem = possibilité_de_3orientation_up[position1]

nb_elem = 2
liste_position = []
while nb_elem > 0:
    position1 = random.randint(0, len(possibilité_de_3orientation_up) - 1)
    while position1 in liste_position:
        position1 = random.randint(0, len(possibilité_de_3orientation_up) - 1)
    liste_position.append(position1)
    nb_elem = nb_elem - 1
rand_liste_3orientation_up = []
for index in rand_liste_3orientation_up:
    rand_liste_3orientation_up.append(possibilité_de_3orientation_up[index])


# cas ou le trou ne se situe contre le bord droit
possibilité_de_3orientation_right = ["left", "up", "down"]

position1 = random.randint(0, len(possibilité_de_3orientation_right) - 1)
elem = possibilité_de_3orientation_right[position1]

nb_elem = 2
liste_position = []
while nb_elem > 0:
    position1 = random.randint(0, len(possibilité_de_3orientation_right) - 1)
    while position1 in liste_position:
        position1 = random.randint(0, len(possibilité_de_3orientation_right) - 1)
    liste_position.append(position1)
    nb_elem = nb_elem - 1
rand_liste_3orientation_right = []
for index in rand_liste_3orientation_right:
    rand_liste_3orientation_right.append(possibilité_de_3orientation_right[index])


# cas ou le trou ne se situe contre le bord gauche
possibilité_de_3orientation_left = ["right", "up", "down"]

position1 = random.randint(0, len(possibilité_de_3orientation_left) - 1)
elem = possibilité_de_3orientation_left[position1]

nb_elem = 2
liste_position = []
while nb_elem > 0:
    position1 = random.randint(0, len(possibilité_de_3orientation_left) - 1)
    while position1 in liste_position:
        position1 = random.randint(0, len(possibilité_de_3orientation_left) - 1)
    liste_position.append(position1)
    nb_elem = nb_elem - 1
rand_liste_3orientation_left = []
for index in rand_liste_3orientation_left:
    rand_liste_3orientation_left.append(possibilité_de_3orientation_left[index])


# /////////////////////////////////////////////////////////////////////////////////////////////////

# au abord du trou gauche:

element_vegetation_liste3 = []
element_vegetation_liste3 = []
element_vegetation_liste3_left_down = []
element_vegetation_liste3_left_up = []
element_vegetation_liste3_right_down = []
element_vegetation_liste3_right_up = []
element_vegetation_liste3_up_left = []
element_vegetation_liste3_up_right = []
element_vegetation_liste3_down_left = []
element_vegetation_liste3_down_right = []
z = 0
# bloc de végétation de 2 de large au dessus du trou gauche

element_vegetation_liste3_up_left.append(
    "Minicarre" + str(rand_liste3_x) + "x" + str(rand_liste3_y + 2) + "y" + str(z) + "z"
)
element_vegetation_liste3_up_right.append(
    "Minicarre"
    + str(rand_liste3_x + 1)
    + "x"
    + str(rand_liste3_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste3_up = (
    element_vegetation_liste3_up_left + element_vegetation_liste3_up_right
)

# bloc de végétation de 2 de large en dessous du trou gauche

element_vegetation_liste3_down_left.append(
    "Minicarre" + str(rand_liste3_x) + "x" + str(rand_liste3_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste3_down_right.append(
    "Minicarre"
    + str(rand_liste3_x + 1)
    + "x"
    + str(rand_liste3_y - 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste3_down = (
    element_vegetation_liste3_down_right + element_vegetation_liste3_down_left
)

# bloc de végétation de 2 de long à gauche du trou gauche

element_vegetation_liste3_left_down.append(
    "Minicarre" + str(rand_liste3_x - 1) + "x" + str(rand_liste3_y) + "y" + str(z) + "z"
)
element_vegetation_liste3_left_up.append(
    "Minicarre"
    + str(rand_liste3_x - 1)
    + "x"
    + str(rand_liste3_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste3_left = (
    element_vegetation_liste3_left_up + element_vegetation_liste3_left_down
)
# bloc de végétation de 2 de long à droite du trou gauche

element_vegetation_liste3_right_down.append(
    "Minicarre" + str(rand_liste3_x + 2) + "x" + str(rand_liste3_y) + "y" + str(z) + "z"
)
element_vegetation_liste3_right_up.append(
    "Minicarre"
    + str(rand_liste3_x + 2)
    + "x"
    + str(rand_liste3_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste3_right = (
    element_vegetation_liste3_right_up + element_vegetation_liste3_right_down
)

# au abord du trou droit:

element_vegetation_liste4 = []
element_vegetation_liste4_left_down = []
element_vegetation_liste4_left_up = []
element_vegetation_liste4_right_down = []
element_vegetation_liste4_right_up = []
element_vegetation_liste4_up_left = []
element_vegetation_liste4_up_right = []
element_vegetation_liste4_down_left = []
element_vegetation_liste4_down_right = []
z = 0
# bloc de végétation de 2 de large au dessus du trou droit

element_vegetation_liste4_up_left.append(
    "Minicarre" + str(rand_liste4_x) + "x" + str(rand_liste4_y + 2) + "y" + str(z) + "z"
)
element_vegetation_liste4_up_right.append(
    "Minicarre"
    + str(rand_liste4_x + 1)
    + "x"
    + str(rand_liste4_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste4_up = (
    element_vegetation_liste4_up_left + element_vegetation_liste4_up_right
)

# bloc de végétation de 2 de large en dessous du trou droit

element_vegetation_liste4_down_left.append(
    "Minicarre" + str(rand_liste4_x) + "x" + str(rand_liste4_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste4_down_right.append(
    "Minicarre"
    + str(rand_liste4_x + 1)
    + "x"
    + str(rand_liste4_y - 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste4_down = (
    element_vegetation_liste4_down_right + element_vegetation_liste4_down_left
)

# bloc de végétation de 2 de long à gauche du trou droit

element_vegetation_liste4_left_down.append(
    "Minicarre" + str(rand_liste4_x - 1) + "x" + str(rand_liste4_y) + "y" + str(z) + "z"
)
element_vegetation_liste4_left_up.append(
    "Minicarre"
    + str(rand_liste4_x - 1)
    + "x"
    + str(rand_liste4_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste4_left = (
    element_vegetation_liste4_left_up + element_vegetation_liste4_left_down
)

# bloc de végétation de 2 de long à droite du trou droit

element_vegetation_liste4_right_down.append(
    "Minicarre" + str(rand_liste4_x + 2) + "x" + str(rand_liste4_y) + "y" + str(z) + "z"
)
element_vegetation_liste4_right_up.append(
    "Minicarre"
    + str(rand_liste4_x + 2)
    + "x"
    + str(rand_liste4_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste4_right = (
    element_vegetation_liste4_right_up + element_vegetation_liste4_right_down
)

# au abord du trou sup:

element_vegetation_liste5_left_up = []
element_vegetation_liste5_left_down = []
element_vegetation_liste5_right_up = []
element_vegetation_liste5_right_down = []
element_vegetation_liste5_up_left = []
element_vegetation_liste5_up_right = []
element_vegetation_liste5_down_left = []
element_vegetation_liste5_down_right = []
z = 0
# bloc de végétation de 2 de large au dessus du trou sup

element_vegetation_liste5_up_left.append(
    "Minicarre" + str(rand_liste5_x) + "x" + str(rand_liste5_y + 2) + "y" + str(z) + "z"
)
element_vegetation_liste5_up_right.append(
    "Minicarre"
    + str(rand_liste5_x + 1)
    + "x"
    + str(rand_liste5_y + 2)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste5_up = (
    element_vegetation_liste5_up_left + element_vegetation_liste5_up_right
)

# bloc de végétation de 2 de large en dessous du trou sup

element_vegetation_liste5_down_left.append(
    "Minicarre" + str(rand_liste5_x) + "x" + str(rand_liste5_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste5_down_right.append(
    "Minicarre"
    + str(rand_liste5_x + 1)
    + "x"
    + str(rand_liste5_y - 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste5_down = (
    element_vegetation_liste5_down_right + element_vegetation_liste5_down_left
)

# bloc de végétation de 2 de long à gauche du trou sup

element_vegetation_liste5_left_down.append(
    "Minicarre" + str(rand_liste5_x - 1) + "x" + str(rand_liste5_y) + "y" + str(z) + "z"
)
element_vegetation_liste5_left_up.append(
    "Minicarre"
    + str(rand_liste5_x - 1)
    + "x"
    + str(rand_liste5_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste5_left = (
    element_vegetation_liste5_left_up + element_vegetation_liste5_left_down
)

# bloc de végétation de 2 de long à droite du trou sup

element_vegetation_liste5_right_down.append(
    "Minicarre" + str(rand_liste5_x + 2) + "x" + str(rand_liste5_y) + "y" + str(z) + "z"
)
element_vegetation_liste5_right_up.append(
    "Minicarre"
    + str(rand_liste5_x + 2)
    + "x"
    + str(rand_liste5_y + 1)
    + "y"
    + str(z)
    + "z"
)
element_vegetation_liste5_right = (
    element_vegetation_liste5_right_up + element_vegetation_liste5_right_down
)

# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation du 1ère étage au abord du trou périphérique:
# -----------------------------------------------------------------------------------------------------------------------------------------

element_vegetation_liste6_left = []
element_vegetation_liste6_right = []
element_vegetation_liste6_up = []
element_vegetation_liste6_down = []

# bloc de végétation de 2 de large au dessus du trou périphérique

element_vegetation_liste6_up.append(
    "Minicarre" + str(rand_liste6_x) + "x" + str(rand_liste6_y + 3) + "y" + str(z) + "z"
)
element_vegetation_liste6_up.append(
    "Minicarre"
    + str(rand_liste6_x + 1)
    + "x"
    + str(rand_liste6_y + 3)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 2 de large en dessous du trou périphérique

element_vegetation_liste6_down.append(
    "Minicarre" + str(rand_liste6_x) + "x" + str(rand_liste6_y - 1) + "y" + str(z) + "z"
)
element_vegetation_liste6_down.append(
    "Minicarre"
    + str(rand_liste6_x + 1)
    + "x"
    + str(rand_liste5_y - 1)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 2 de long à gauche du trou périphérique

element_vegetation_liste6_left.append(
    "Minicarre" + str(rand_liste6_x - 1) + "x" + str(rand_liste6_y) + "y" + str(z) + "z"
)
element_vegetation_liste6_left.append(
    "Minicarre"
    + str(rand_liste6_x - 1)
    + "x"
    + str(rand_liste6_y + 1)
    + "y"
    + str(z)
    + "z"
)

# bloc de végétation de 2 de long à droite du trou périphérique

element_vegetation_liste6_right.append(
    "Minicarre" + str(rand_liste6_x + 2) + "x" + str(rand_liste6_y) + "y" + str(z) + "z"
)
element_vegetation_liste6_right.append(
    "Minicarre"
    + str(rand_liste6_x + 2)
    + "x"
    + str(rand_liste6_y + 1)
    + "y"
    + str(z)
    + "z"
)

# partie non finie par manque de temps

# -----------------------------------------------------------------------------------------------------------------------------------------
# Végétation du 1ère étage en périphérie:
# -----------------------------------------------------------------------------------------------------------------------------------------

# bloc de végétation de 2 sur 2 du coté trou périphérique:

liste7_x_1 = []
liste7_x_2 = []

element_végétation_bloc_2sur2_trou_périphérique_bas = []
element_végétation_bloc_2sur2_trou_périphérique_bas_étage1 = []
element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1 = []
element_végétation_bloc_2sur2_trou_périphérique_haut = []
element_végétation_bloc_2sur2_trou_périphérique_haut_étage1 = []
element_végétation_bloc_2sur2_trou_supp_bas = []
element_végétation_bloc_2sur2_trou_supp_bas_étage1 = []
element_végétation_bloc_2sur2_trou_supp_haut = []
element_végétation_bloc_2sur2_trou_supp_haut_étage1 = []
element_delete_végétation_bloc_2sur2_trou_périphérique_bas = []
element_delete_végétation_bloc_2sur2_trou_supp_bas = []
element_delete_végétation_bloc_2sur2_trou_périphérique_haut = []
element_delete_végétation_bloc_2sur2_trou_supp_haut = []
element_végétation_bloc_2sur2_trou_périphérique_étage1 = []
element_végétation_bloc_2sur2_trou_supp_étage1 = []
element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1 = []
element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1 = []
element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1 = []
element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1 = []
element_delete_végétation_bloc_2sur2_trou_supp_étage1 = []
element_végétation_bloc_2sur2_trou_périphérique_bas_étage2 = []
element_végétation_bloc_2sur2_trou_périphérique_haut_étage2 = []
element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1 = []
element_delete_végétation_bloc_2sur2_trou_supp_bas_étage2 = []
element_delete_végétation_bloc_2sur2_trou_supp_haut_étage2 = []
element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1 = []

gauche_ou_droite = ("gauche", "droite")
rand_gauche_ou_droite = random.choice(gauche_ou_droite)

for x_1 in range(2, 7):
    liste7_x_1.append(x_1)
for x_2 in range(12, 17):
    liste7_x_2.append(x_2)

if -1 < rand_liste6_x < 7:
    if -1 < rand_liste6_y < 6:
        rand_liste7_x = 0
        rand_liste7_y = 10

    if 5 < rand_liste6_y < 12:
        rand_liste7_x = 0
        rand_liste7_y = 0

if 11 < rand_liste6_x < 19:
    if -1 < rand_liste6_y < 6:
        rand_liste7_x = 18
        rand_liste7_y = 10

    if 5 < rand_liste6_y < 12:
        rand_liste7_x = 18
        rand_liste7_y = 0

    # déterminer la position des blocs de végétation de 2 sur 2 au rez
for z in range(0, 3):
    if z == 0:
        element_végétation_bloc_2sur2_trou_périphérique_bas.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_périphérique_bas.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_périphérique_haut.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_périphérique_haut.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_périphérique = (
            element_végétation_bloc_2sur2_trou_périphérique_haut
            + element_végétation_bloc_2sur2_trou_périphérique_bas
        )

    if z == 1:
        # déterminer la position des trous au dessus des bloc de 2 sur 2
        if rand_liste7_x == 18:
            position = "droit"
            if rand_liste7_y == 10:
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                )
            if rand_liste7_y == 0:
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                )
        if rand_liste7_x == 0:
            position = "gauche"
            if rand_liste7_y == 10:
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                )
            if rand_liste7_y == 0:
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                )
        # déterminer la position des blocs de végétation de 2 sur 2 au 1er étage
        if (
            element_delete_végétation_bloc_2sur2_trou_périphérique_étage1
            == element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
            or element_delete_végétation_bloc_2sur2_trou_périphérique_étage1
            == element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
        ):
            if position == "droit":
                element_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                    + element_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                    + element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1
                )
            if position == "gauche":
                element_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_périphérique_étage1 = (
                    element_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                    + element_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                    + element_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1
                )

    if (
        z == 2
    ):  # déterminer la position des trous au dessus des bloc de 2 sur 2 du 1 ère étage
        element_delete_végétation_bloc_2sur2_trou_périphérique_bas.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_bloc_2sur2_trou_périphérique_bas.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_bloc_2sur2_trou_périphérique_haut.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_bloc_2sur2_trou_périphérique_haut.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_bloc_2sur2_trou_périphérique = (
            element_delete_végétation_bloc_2sur2_trou_périphérique_haut
            + element_delete_végétation_bloc_2sur2_trou_périphérique_bas
        )

        if (
            element_delete_végétation_bloc_2sur2_trou_périphérique_étage1
            == element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
            or element_delete_végétation_bloc_2sur2_trou_périphérique_étage1
            == element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
        ):
            if position == "droit":
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage2 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                    + element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                    + element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1
                )
            if position == "gauche":
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_périphérique_étage2 = (
                    element_delete_végétation_bloc_2sur2_trou_périphérique_haut_étage1
                    + element_delete_végétation_bloc_2sur2_trou_périphérique_bas_étage1
                    + element_delete_végétation_bloc_2sur2_trou_périphérique_bas_reste_rez_étage1
                )

        element_delete_végétation_bloc_2sur2_trou_périphérique_étage2 = (
            element_delete_végétation_bloc_2sur2_trou_périphérique_étage2
            + element_delete_végétation_bloc_2sur2_trou_périphérique
        )

# bloc de végétation de 2 sur 2 du coté trou supp:

if -1 < rand_liste5_x < 7:
    if -1 < rand_liste5_y < 6:
        rand_liste7_x = random.choice(liste7_x_1)
        rand_liste7_y = 10

    if 5 < rand_liste5_y < 12:
        rand_liste7_x = random.choice(liste7_x_1)
        rand_liste7_y = 0

if 11 < rand_liste5_x < 19:
    if -1 < rand_liste5_y < 6:
        rand_liste7_x = random.choice(liste7_x_2)
        rand_liste7_y = 10

    if 5 < rand_liste5_y < 12:
        rand_liste7_x = random.choice(liste7_x_2)
        rand_liste7_y = 0

for z in range(0, 3):
    if z == 0:
        # déterminer la position des blocs de végétation de 2 sur 2 au rez
        element_végétation_bloc_2sur2_trou_supp_bas.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_supp_bas.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_supp_haut.append(
            "Minicarre"
            + str(rand_liste7_x)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_supp_haut.append(
            "Minicarre"
            + str(rand_liste7_x + 1)
            + "x"
            + str(rand_liste7_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_bloc_2sur2_trou_supp = (
            element_végétation_bloc_2sur2_trou_supp_haut
            + element_végétation_bloc_2sur2_trou_supp_bas
        )

    if z == 1:
        # déterminer la position des trous au dessus des bloc de 2 sur 2
        if rand_liste7_y == 0:
            element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                "Minicarre"
                + str(rand_liste7_x)
                + "x"
                + str(rand_liste7_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                "Minicarre"
                + str(rand_liste7_x + 1)
                + "x"
                + str(rand_liste7_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
            )
        if rand_liste7_y == 10:
            element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                "Minicarre"
                + str(rand_liste7_x)
                + "x"
                + str(rand_liste7_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                "Minicarre"
                + str(rand_liste7_x + 1)
                + "x"
                + str(rand_liste7_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
            )
        # déterminer la position des blocs de végétation de 2 sur 2 au 1er étage
        if (
            element_delete_végétation_bloc_2sur2_trou_supp_étage1
            == element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
            or element_delete_végétation_bloc_2sur2_trou_supp_étage1
            == element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
        ):
            if 1 < rand_liste7_x < 4 or 11 < rand_liste7_x < 14:
                element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_étage1 = (
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1
                    + element_végétation_bloc_2sur2_trou_supp_bas_étage1
                )
            if rand_liste7_x == 4 or rand_liste7_x == 14:
                if rand_gauche_ou_droite == "gauche":
                    element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 2)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 1)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 2)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 1)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_étage1 = (
                        element_végétation_bloc_2sur2_trou_supp_haut_étage1
                        + element_végétation_bloc_2sur2_trou_supp_bas_étage1
                    )
                if rand_gauche_ou_droite == "droite":
                    element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 2)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 3)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 2)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 3)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_bloc_2sur2_trou_supp_étage1 = (
                        element_végétation_bloc_2sur2_trou_supp_haut_étage1
                        + element_végétation_bloc_2sur2_trou_supp_bas_étage1
                    )
            if 4 < rand_liste7_x < 8 or 14 < rand_liste7_x < 18:
                element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_bloc_2sur2_trou_supp_étage1 = (
                    element_végétation_bloc_2sur2_trou_supp_haut_étage1
                    + element_végétation_bloc_2sur2_trou_supp_bas_étage1
                )
    if z == 2:
        # déterminer la position des trous au dessus des bloc de 2 sur 2
        if rand_liste7_y == 0:
            element_delete_végétation_bloc_2sur2_trou_supp_bas_étage2.append(
                "Minicarre"
                + str(rand_liste7_x)
                + "x"
                + str(rand_liste7_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_bas_étage2.append(
                "Minicarre"
                + str(rand_liste7_x + 1)
                + "x"
                + str(rand_liste7_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_étage2 = (
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage2
            )
        if rand_liste7_y == 10:
            element_delete_végétation_bloc_2sur2_trou_supp_haut_étage2.append(
                "Minicarre"
                + str(rand_liste7_x)
                + "x"
                + str(rand_liste7_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_haut_étage2.append(
                "Minicarre"
                + str(rand_liste7_x + 1)
                + "x"
                + str(rand_liste7_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_bloc_2sur2_trou_supp_étage2 = (
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage2
            )
        if (
            element_delete_végétation_bloc_2sur2_trou_supp_étage1
            == element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
            or element_delete_végétation_bloc_2sur2_trou_supp_étage1
            == element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
        ):
            if 1 < rand_liste7_x < 4 or 11 < rand_liste7_x < 14:
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x + 3)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                if rand_liste7_x == 2 or rand_liste7_x == 12:
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 4)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 5)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 4)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 5)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                if rand_liste7_x == 3 or rand_liste7_x == 13:
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 1)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 4)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 1)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 4)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
                    + element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
                    + element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1
                )

            if rand_liste7_x == 4 or rand_liste7_x == 14:
                if rand_gauche_ou_droite == "gauche":
                    element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 2)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 1)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 2)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x - 1)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 2)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 3)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 2)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 3)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                        element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
                        + element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
                        + element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1
                    )
                if rand_gauche_ou_droite == "droite":
                    element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 2)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 3)
                        + "x"
                        + str(rand_liste7_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 2)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                        "Minicarre"
                        + str(rand_liste7_x + 3)
                        + "x"
                        + str(rand_liste7_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 2)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 1)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 2)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 1)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                        element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
                        + element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
                        + element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1
                    )
            if 4 < rand_liste7_x < 8 or 14 < rand_liste7_x < 17:
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 2)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1.append(
                    "Minicarre"
                    + str(rand_liste7_x - 1)
                    + "x"
                    + str(rand_liste7_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_bloc_2sur2_trou_supp_étage1 = (
                    element_delete_végétation_bloc_2sur2_trou_supp_haut_étage1
                    + element_delete_végétation_bloc_2sur2_trou_supp_bas_étage1
                )
                if rand_liste7_x == 5 or rand_liste7_x == 15:
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 2)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 3)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x + 2)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 3)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                if rand_liste7_x == 6 or rand_liste7_x == 16:
                    if rand_liste7_y == 0:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 3)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 4)
                            + "x"
                            + str(rand_liste7_y)
                            + "y"
                            + str(z)
                            + "z"
                        )
                    if rand_liste7_y == 10:
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 3)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )
                        element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1.append(
                            "Minicarre"
                            + str(rand_liste7_x - 4)
                            + "x"
                            + str(rand_liste7_y + 1)
                            + "y"
                            + str(z)
                            + "z"
                        )

        element_delete_végétation_bloc_2sur2_trou_supp_étage2 = (
            element_delete_végétation_bloc_2sur2_trou_supp_étage1
            + element_delete_végétation_bloc_2sur2_trou_supp_étage2
            + element_delete_végétation_bloc_2sur2_trou_supp_plus_étage1
        )


# végétation rez: bloc de 4 de long du coté trou supp
element_végétation_coté_latéral_trou_supp = []
element_delete_végétation_coté_latéral_trou_supp_étage1 = []
element_végétation_coté_latéral_trou_supp_étage1 = []
element_delete_végétation_coté_latéral_trou_supp_étage2 = []


rand_liste8_y = 4
if rand_trou_sup == "gauche":
    rand_liste8_x = 0
if rand_trou_sup == "droite":
    rand_liste8_x = 19

for z in range(0, 3):
    if z == 0:
        # déterminer la position des blocs de végétation au rez
        element_végétation_coté_latéral_trou_supp.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_supp.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_supp.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 2)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_supp.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 3)
            + "y"
            + str(z)
            + "z"
        )
    if z == 1:
        # déterminer la position des trous au dessus de la végétation
        element_delete_végétation_coté_latéral_trou_supp_étage1.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage1.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage1.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 2)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage1.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 3)
            + "y"
            + str(z)
            + "z"
        )
        # déterminer la position de la végétation au 1er étage
        if rand_trou_sup == "gauche":
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x + 1)
                + "x"
                + str(rand_liste8_y)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x + 1)
                + "x"
                + str(rand_liste8_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x + 1)
                + "x"
                + str(rand_liste8_y + 2)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x + 1)
                + "x"
                + str(rand_liste8_y + 3)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x)
                + "x"
                + str(rand_liste8_y - 1)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x)
                + "x"
                + str(rand_liste8_y + 4)
                + "y"
                + str(z)
                + "z"
            )
        if rand_trou_sup == "droite":
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x - 1)
                + "x"
                + str(rand_liste8_y)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x - 1)
                + "x"
                + str(rand_liste8_y + 1)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x - 1)
                + "x"
                + str(rand_liste8_y + 2)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x - 1)
                + "x"
                + str(rand_liste8_y + 3)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x)
                + "x"
                + str(rand_liste8_y - 1)
                + "y"
                + str(z)
                + "z"
            )
            element_végétation_coté_latéral_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste8_x)
                + "x"
                + str(rand_liste8_y + 4)
                + "y"
                + str(z)
                + "z"
            )

    if z == 2:
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 2)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 3)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y - 1)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_coté_latéral_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste8_x)
            + "x"
            + str(rand_liste8_y + 4)
            + "y"
            + str(z)
            + "z"
        )
        hasard = ("haut", "milieu", "bas")
        rand_hasard = random.choice(hasard)
        if rand_trou_sup == "gauche":
            if rand_hasard == "haut":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard == "milieu":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard == "bas":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x + 1)
                    + "x"
                    + str(rand_liste8_y)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_trou_sup == "droite":
            if rand_hasard == "haut":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard == "milieu":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard == "bas":
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_supp_étage2.append(
                    "Minicarre"
                    + str(rand_liste8_x - 1)
                    + "x"
                    + str(rand_liste8_y)
                    + "y"
                    + str(z)
                    + "z"
                )

# végétation rez: bloc de 4 de long du coté trou périphérique
liste9_y = []
element_végétation_coté_latéral_trou_périphérique = []
element_delete_végétation_coté_latéral_trou_périphérique_étage1 = []
element_végétation_coté_latéral_trou_périphérique_étage1 = []
element_delete_végétation_coté_latéral_trou_périphérique_étage2 = []

hasard = ("bas_bas", "bas", "haut", "haut_haut")
rand_hasard = random.choice(hasard)
hasard_bis = ("haut", "bas")
rand_hasard_bis = random.choice(hasard_bis)

for y in range(3, 6):
    liste9_y.append(y)
    rand_liste9_y = random.choice(liste9_y)
if rand_trou_sup == "gauche":
    rand_liste9_x = 19
if rand_trou_sup == "droite":
    rand_liste9_x = 0

for z in range(0, 3):
    if z == 0:
        # déterminer la position des blocs de végétation au rez
        element_végétation_coté_latéral_trou_périphérique.append(
            "Minicarre"
            + str(rand_liste9_x)
            + "x"
            + str(rand_liste9_y)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_périphérique.append(
            "Minicarre"
            + str(rand_liste9_x)
            + "x"
            + str(rand_liste9_y + 1)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_périphérique.append(
            "Minicarre"
            + str(rand_liste9_x)
            + "x"
            + str(rand_liste9_y + 2)
            + "y"
            + str(z)
            + "z"
        )
        element_végétation_coté_latéral_trou_périphérique.append(
            "Minicarre"
            + str(rand_liste9_x)
            + "x"
            + str(rand_liste9_y + 3)
            + "y"
            + str(z)
            + "z"
        )
    if z == 1:
        # déterminer la position des trous au dessus de la végétation
        if rand_hasard == "bas_bas":
            element_delete_végétation_coté_latéral_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste9_x)
                + "x"
                + str(rand_liste9_y)
                + "y"
                + str(z)
                + "z"
            )
        if rand_hasard == "bas":
            element_delete_végétation_coté_latéral_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste9_x)
                + "x"
                + str(rand_liste9_y + 1)
                + "y"
                + str(z)
                + "z"
            )
        if rand_hasard == "haut":
            element_delete_végétation_coté_latéral_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste9_x)
                + "x"
                + str(rand_liste9_y + 2)
                + "y"
                + str(z)
                + "z"
            )
        if rand_hasard == "haut_haut":
            element_delete_végétation_coté_latéral_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste9_x)
                + "x"
                + str(rand_liste9_y + 3)
                + "y"
                + str(z)
                + "z"
            )
        # déterminer la position de la végétation au 1er étage
        if rand_hasard == "bas_bas":
            if rand_hasard_bis == "haut":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y - 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y - 2)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "bas":
            if rand_hasard_bis == "haut":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "haut":
            if rand_hasard_bis == "haut":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 4)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "haut_haut":
            if rand_hasard_bis == "haut":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 4)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 5)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coté_latéral_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )

    if z == 2:
        if rand_hasard == "bas_bas":
            if rand_hasard_bis == "haut":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y - 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y - 2)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "bas":
            if rand_hasard_bis == "haut":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "haut":
            if rand_hasard_bis == "haut":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 3)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 4)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_hasard == "haut_haut":
            if rand_hasard_bis == "haut":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 4)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 5)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_hasard_bis == "bas":
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 2)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coté_latéral_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste9_x)
                    + "x"
                    + str(rand_liste9_y + 1)
                    + "y"
                    + str(z)
                    + "z"
                )


# végétation rez: bloc de 1 de long du coté trou supp et à son opposé

element_végétation_coin_opposé_trou_supp_étage1 = []
element_delete_végétation_coin_opposé_trou_supp_étage2 = []

if rand_trou_sup == "gauche":
    rand_liste10_x = 0
if rand_trou_sup == "droite":
    rand_liste10_x = 19

for z in range(1, 3):
    if z == 1:
        # déterminer la position des trous au dessus de la végétation
        if 1 < rand_liste5_y < 6:
            rand_liste10_y = 0
            element_végétation_coin_opposé_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste10_x)
                + "x"
                + str(rand_liste10_y)
                + "y"
                + str(z)
                + "z"
            )
        if 5 < rand_liste5_y < 9:
            rand_liste10_y = 11
            element_végétation_coin_opposé_trou_supp_étage1.append(
                "Minicarre"
                + str(rand_liste10_x)
                + "x"
                + str(rand_liste10_y)
                + "y"
                + str(z)
                + "z"
            )
    if z == 2:
        if 1 < rand_liste5_y < 6:
            rand_liste10_y = 0
            element_delete_végétation_coin_opposé_trou_supp_étage2.append(
                "Minicarre"
                + str(rand_liste10_x)
                + "x"
                + str(rand_liste10_y)
                + "y"
                + str(z)
                + "z"
            )
        if 5 < rand_liste5_y < 9:
            rand_liste10_y = 11
            element_delete_végétation_coin_opposé_trou_supp_étage2.append(
                "Minicarre"
                + str(rand_liste10_x)
                + "x"
                + str(rand_liste10_y)
                + "y"
                + str(z)
                + "z"
            )

# végétation rez: bloc de 2 de long du coté trou supp
element_végétation_coin_trou_supp = []
element_végétation_coin_trou_supp_étage1 = []
element_delete_végétation_coin_trou_supp_étage1 = []
element_delete_végétation_coin_trou_supp_étage2 = []

if rand_trou_sup == "gauche":
    rand_liste11_x = 0
if rand_trou_sup == "droite":
    rand_liste11_x = 19

for z in range(0, 2):
    if z == 0:
        # déterminer la position des trous au dessus de la végétation
        if rand_trou_sup == "gauche":
            if 1 < rand_liste5_y < 6:
                rand_liste11_y = 11
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x + 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if 5 < rand_liste5_y < 9:
                rand_liste11_y = 0
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x + 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
        if rand_trou_sup == "droite":
            if 1 < rand_liste5_y < 6:
                rand_liste11_y = 11
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x - 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if 5 < rand_liste5_y < 9:
                rand_liste11_y = 0
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_supp.append(
                    "Minicarre"
                    + str(rand_liste11_x - 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )

    if z == 1:
        if rand_trou_sup == "gauche":
            if 1 < rand_liste5_y < 6:
                rand_liste11_y = 11
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x + 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )

            if 5 < rand_liste5_y < 9:
                rand_liste11_y = 0
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x + 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )

        if rand_trou_sup == "droite":
            if 1 < rand_liste5_y < 6:
                rand_liste11_y = 11
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x - 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )

            if 5 < rand_liste5_y < 9:
                rand_liste11_y = 0
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_supp_étage1.append(
                    "Minicarre"
                    + str(rand_liste11_x - 1)
                    + "x"
                    + str(rand_liste11_y)
                    + "y"
                    + str(z)
                    + "z"
                )

# végétation rez: bloc de 2 de long du coté trou périph

element_végétation_coin_trou_périphérique_étage1 = []
element_delete_végétation_coin_trou_périphérique_étage2 = []

if rand_trou_sup == "gauche":
    rand_liste12_x = 18
if rand_trou_sup == "droite":
    rand_liste12_x = 0

for z in range(1, 3):
    if z == 1:
        # déterminer la position des trous au dessus de la végétation
        if rand_trou_sup == "gauche":
            if rand_liste6_y == 0:
                rand_liste12_y = 0
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_liste6_y == 11:
                rand_liste12_y = 11
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )

        if rand_trou_sup == "droite":
            if rand_liste6_y == 0:
                rand_liste12_y = 0
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_liste6_y == 11:
                rand_liste12_y = 11
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )

    if z == 2:
        if rand_trou_sup == "gauche":
            if rand_liste6_y == 0:
                rand_liste12_y = 0
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_liste6_y == 11:
                rand_liste12_y = 11
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )

        if rand_trou_sup == "droite":
            if rand_liste6_y == 0:
                rand_liste12_y = 0
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_liste6_y == 11:
                rand_liste12_y = 11
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_trou_périphérique_étage2.append(
                    "Minicarre"
                    + str(rand_liste12_x + 1)
                    + "x"
                    + str(rand_liste12_y)
                    + "y"
                    + str(z)
                    + "z"
                )

# végétation rez: bloc de 3 de long du coté trou périph opposé
liste13_x = []
element_végétation_coin_opposé_trou_périphérique = []
element_végétation_coin_opposé_trou_périphérique_étage1 = []
element_delete_végétation_coin_opposé_trou_périphérique_étage2 = []
element_delete_végétation_coin_opposé_trou_périphérique_étage1 = []

hasard = ("gauche", "droite")
rand_hasard = random.choice(hasard)

if rand_trou_sup == "gauche":
    for x in range(10, 12):
        liste13_x.append(x)
        rand_liste13_x = random.choice(liste13_x)
if rand_trou_sup == "droite":
    for x in range(6, 8):
        liste13_x.append(x)
        rand_liste13_x = random.choice(liste13_x)

for z in range(0, 3):
    if z == 0:
        # déterminer la position des trous au dessus de la végétation
        if rand_trou_sup == "gauche":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )

            if rand_liste6_y == 11:
                rand_liste13_y = 0
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )

        if rand_trou_sup == "droite":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
            if rand_liste6_y == 11:
                rand_liste13_y = 0
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_végétation_coin_opposé_trou_périphérique.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
    if z == 1:
        # déterminer la position des trous au dessus de la végétation
        if rand_trou_sup == "gauche":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                if rand_hasard == "gauche":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )

            if rand_liste6_y == 11:
                rand_liste13_y = 0
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                if rand_hasard == "gauche":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )

        if rand_trou_sup == "droite":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                if rand_hasard == "gauche":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )

            if rand_liste6_y == 11:
                rand_liste13_y = 0
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 1)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                element_delete_végétation_coin_opposé_trou_périphérique_étage1.append(
                    "Minicarre"
                    + str(rand_liste13_x + 2)
                    + "x"
                    + str(rand_liste13_y)
                    + "y"
                    + str(z)
                    + "z"
                )
                if rand_hasard == "gauche":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_végétation_coin_opposé_trou_périphérique_étage1.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )

    if z == 2:
        if rand_trou_sup == "gauche":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                if rand_hasard == "gauche":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
            if rand_liste6_y == 11:
                rand_liste13_y = 0
                if rand_hasard == "gauche":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )

        if rand_trou_sup == "droite":
            if rand_liste6_y == 0:
                rand_liste13_y = 11
                if rand_hasard == "gauche":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y - 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
            if rand_liste6_y == 11:
                rand_liste13_y = 0
                if rand_hasard == "gauche":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                if rand_hasard == "droite":
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y + 1)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 2)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )
                    element_delete_végétation_coin_opposé_trou_périphérique_étage2.append(
                        "Minicarre"
                        + str(rand_liste13_x + 1)
                        + "x"
                        + str(rand_liste13_y)
                        + "y"
                        + str(z)
                        + "z"
                    )


# végétation rez: bloc de 4 de long au abord du trou périph
liste14_x = []
element_delete_végétation_abord_trou_périphérique_étage1 = []

if rand_liste6_y == 11:
    rand_liste14_y = 11
if rand_liste6_y == 0:
    rand_liste14_y = 0
if rand_liste6_y == 0:
    rand_liste14_y = 0
if rand_liste6_y == 11:
    rand_liste14_y = 11

z = 1
if rand_trou_sup == "gauche":
    if rand_liste6_x == 12:
        hasard = ("gauche", "droite")
        rand_hasard = random.choice(hasard)
        if rand_hasard == "gauche":
            rand_liste14_x = 8
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 1)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 2)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 3)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
        if rand_hasard == "droite":
            rand_liste14_x = 14
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 1)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 2)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 3)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )

    if 12 < rand_liste6_x < 17:
        rand_liste14_x = rand_liste6_x - 2
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 1)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 2)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 3)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )


if rand_trou_sup == "droite":
    if rand_liste6_x == 6:
        hasard = ("gauche", "droite")
        rand_hasard = random.choice(hasard)
        if rand_hasard == "gauche":
            rand_liste14_x = 8
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 1)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 2)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 3)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
        if rand_hasard == "droite":
            rand_liste14_x = 2
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 1)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 2)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )
            element_delete_végétation_abord_trou_périphérique_étage1.append(
                "Minicarre"
                + str(rand_liste14_x + 3)
                + "x"
                + str(rand_liste14_y)
                + "y"
                + str(z)
                + "z"
            )

    if 1 < rand_liste6_x < 6:
        rand_liste14_x = rand_liste6_x + 2
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 1)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 2)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_abord_trou_périphérique_étage1.append(
            "Minicarre"
            + str(rand_liste14_x + 3)
            + "x"
            + str(rand_liste14_y)
            + "y"
            + str(z)
            + "z"
        )


# végétation rez: bloc de 2 de long
liste15_x = []
element_delete_végétation_opposé_trou_supp_étage2 = []
z = 2
if rand_trou_sup == "gauche":
    for x in range(3, 7):
        liste15_x.append(x)
        rand_liste15_x = random.choice(liste15_x)
    if 1 < rand_liste5_y < 6:
        rand_liste15_y = 0
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x + 1)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
    if 6 < rand_liste5_y < 9:
        rand_liste15_y = 11
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x + 1)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
if rand_trou_sup == "droite":
    for x in range(12, 17):
        liste15_x.append(x)
        rand_liste15_x = random.choice(liste15_x)
    if 1 < rand_liste5_y < 6:
        rand_liste15_y = 0
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x + 1)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
    if 6 < rand_liste5_y < 9:
        rand_liste15_y = 11
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )
        element_delete_végétation_opposé_trou_supp_étage2.append(
            "Minicarre"
            + str(rand_liste15_x + 1)
            + "x"
            + str(rand_liste15_y)
            + "y"
            + str(z)
            + "z"
        )

# -----------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------
# Suppression de tout les élément constituant les trous:
# -----------------------------------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------------------------------

element_delete_liste = (
    rand_element_delete_trou_centre
    + rand_element_delete_trou_gauche
    + rand_element_delete_trou_droite
    + element_delete_liste5
    + element_delete_liste6
    + element_delete_évasement_liste1_étage_3
    + element_delete_évasement_liste2_étage_3
    + element_delete_végétation_bloc_2sur2_trou_périphérique_étage1
    + element_delete_végétation_bloc_2sur2_trou_supp_étage1
    + element_delete_végétation_bloc_2sur2_trou_périphérique_étage2
    + element_delete_végétation_bloc_2sur2_trou_supp_étage2
    + element_delete_végétation_coté_latéral_trou_supp_étage1
    + element_delete_végétation_coté_latéral_trou_supp_étage2
    + element_delete_végétation_coté_latéral_trou_périphérique_étage1
    + element_delete_végétation_coté_latéral_trou_périphérique_étage2
    + element_delete_végétation_coin_opposé_trou_supp_étage2
    + element_delete_végétation_coin_trou_supp_étage1
    + element_delete_végétation_coin_trou_périphérique_étage2
    + element_delete_végétation_coin_opposé_trou_périphérique_étage1
    + element_delete_végétation_coin_opposé_trou_périphérique_étage2
    + element_delete_végétation_opposé_trou_supp_étage2
    + element_delete_végétation_abord_trou_périphérique_étage1
)

bpy.ops.object.select_all(action="DESELECT")

for object_name in element_delete_liste:
    bpy.data.objects[object_name].select = True

bpy.ops.object.delete()

print(
    "////////////////////////////////////////////////////////////////////////////////"
)
print(
    "////////////////////////////////////////////////////////////////////////////////"
)
print(
    "////////////////////////////////////////////////////////////////////////////////"
)
print("")
if rand_element_delete_trou_centre == (element_delete_liste1):
    print("position du trou central: horizontal")
if rand_element_delete_trou_centre == (element_delete_liste2):
    print("position du trou central: vertical")
print("")
print(
    "coordonnées des éléments constituant le trou central:",
    rand_element_delete_trou_centre,
)
print("")
print(
    "coordonnées des éléments constituant le trou à gauche:",
    rand_element_delete_trou_gauche,
)
print("")
print(
    "coordonnées des éléments constituant le trou à droite:",
    rand_element_delete_trou_droite,
)
print("")
print("position du trou périphérique:", rand_trou_sup)
print("")
print(
    "coordonnées des éléments constituant le trou supplémentaire:",
    element_delete_liste5,
)
print("")
print(
    "coordonnées des éléments constituant le trou périphérique:", element_delete_liste6
)
print("")
print(
    "////////////////////////////////////////////////////////////////////////////////"
)
print(
    "////////////////////////////////////////////////////////////////////////////////"
)
print(
    "////////////////////////////////////////////////////////////////////////////////"
)
