# SPDX-FileCopyrightText: 2016 Camille Pons-Foucault de Pavant
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy

# import random
from random import choice, randint
import math
from math import radians
from math import cos
from math import sin


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
rayon_colonne = 2.5
largeur_passe = 10 / rayon_colonne
etage = 1
nbr_entraxe_x = 4
nbr_entraxe_y = 2
nbr_etage_z = 4
entraxe_x = 17.5
entraxe_y = 7
elancement = 4
epaisseur_dalle = 1
epaisseur_colonne = elancement
epaisseur_mur = elancement - epaisseur_dalle
longueur_passe_x = entraxe_x - largeur_passe
longueur_passe_y = entraxe_y - largeur_passe
longueur_passe_milieu = 18


##################################################################################


def colonne(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_colonne / 2

    bpy.ops.mesh.primitive_cylinder_add(
        radius=rayon_colonne,
        depth=epaisseur_colonne,
        location=(x, y, z + hauteur),
        vertices=50,
    )
    bpy.ops.transform.translate(value=(0, 0, 0.1))


def plaque_colonne(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_colonne / 2
    bpy.ops.mesh.primitive_cube_add(
        radius=rayon_colonne / 5, location=(x, y, z + hauteur)
    )
    bpy.ops.transform.resize(value=(largeur_passe, largeur_passe, epaisseur_dalle))
    bpy.ops.transform.translate(
        value=(0, 0, epaisseur_colonne / 2 - epaisseur_dalle / 2)
    )


def passerelle_y(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_dalle / 2

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(value=(largeur_passe, longueur_passe_y, epaisseur_dalle))
    bpy.ops.transform.translate(value=(0, -entraxe_y / 2, 0))


def passerelle_x(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_dalle / 2

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(value=(longueur_passe_x, largeur_passe, epaisseur_dalle))
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, 0))


def dalle(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_dalle / 2

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(
        value=(longueur_passe_x, longueur_passe_y, epaisseur_dalle)
    )
    bpy.ops.transform.translate(value=(-entraxe_x / 2, -entraxe_y / 2, 0))


def passerelle_milieu(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_dalle / 2

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(
        value=(
            largeur_passe,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            epaisseur_dalle,
        )
    )


def dalle_centre(location, etage):

    x, y, z = location

    hauteur = elancement * etage - epaisseur_dalle / 2

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(
        value=(
            longueur_passe_x,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            epaisseur_dalle,
        )
    )
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, 0))


def poutrelle(location, etage):
    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(value=(0.3, 0.3, epaisseur_mur))


def plinte(location, etage):
    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))


def big_plinte(location, etage):
    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))


def small_plinte(location, etage):
    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))


def mur_x(location, etage):

    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2

    poutrelle(location, etage)
    bpy.ops.transform.translate(
        value=(-entraxe_x * 2 / 3, -largeur_passe / 2 + 0.15, -epaisseur_dalle)
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (entraxe_x / 3, 0, 0)},
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (0, largeur_passe - 0.3, 0)},
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (-entraxe_x / 3, 0, 0)},
    )
    big_plinte(location, etage)
    bpy.ops.transform.resize(value=(longueur_passe_x, largeur_passe + 0.05, 0.7))
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, -epaisseur_dalle * 1.9))
    small_plinte(location, etage)
    bpy.ops.transform.resize(value=(longueur_passe_x, largeur_passe + 0.05, 0.2))
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, -epaisseur_dalle * 1.2))
    plinte(location, etage)
    bpy.ops.transform.resize(value=(longueur_passe_x, largeur_passe + 0.05, 0.3))
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, -epaisseur_dalle / 10))


def mur_y(location, etage):

    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(value=(largeur_passe, longueur_passe_y, epaisseur_mur))
    bpy.ops.transform.translate(value=(0, -entraxe_y / 2, -epaisseur_dalle))


def mur_dalle(location, etage):

    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(value=(longueur_passe_x, longueur_passe_y, epaisseur_mur))
    bpy.ops.transform.translate(
        value=(-entraxe_x / 2, -entraxe_y / 2, -epaisseur_dalle)
    )


def mur_milieu(location, etage):

    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    poutrelle(location, etage)
    bpy.ops.transform.translate(
        value=(
            largeur_passe / 2 - 0.15,
            (longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe)) / 4,
            -epaisseur_dalle,
        )
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (-largeur_passe + 0.3, 0, 0)},
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (
                0,
                -(longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe)) / 4,
                0,
            )
        },
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (largeur_passe - 0.3, 0, 0)},
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (
                0,
                -(longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe)) / 4,
                0,
            )
        },
    )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (-largeur_passe + 0.3, 0, 0)},
    )

    big_plinte(location, etage)
    bpy.ops.transform.resize(
        value=(
            largeur_passe,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            0.7,
        )
    )
    bpy.ops.transform.translate(value=(0, 0, -epaisseur_dalle * 1.9))
    small_plinte(location, etage)
    bpy.ops.transform.resize(
        value=(
            largeur_passe,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            0.2,
        )
    )
    bpy.ops.transform.translate(value=(0, 0, -epaisseur_dalle * 1.2))
    plinte(location, etage)
    bpy.ops.transform.resize(
        value=(
            largeur_passe,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            0.3,
        )
    )
    bpy.ops.transform.translate(value=(0, 0, -epaisseur_dalle / 10))


def mur_centre(location, etage):

    x, y, z = location
    hauteur = elancement * etage - epaisseur_mur / 2
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y, z + hauteur))
    bpy.ops.transform.resize(
        value=(
            longueur_passe_x,
            longueur_passe_milieu + (rayon_colonne * 2 - largeur_passe),
            epaisseur_mur,
        )
    )
    bpy.ops.transform.translate(value=(-entraxe_x / 2, 0, -epaisseur_dalle))


###############################################################################################

trame2 = []
for jz in range(0, nbr_etage_z):
    for jx in range(0, nbr_entraxe_x):
        for jy in range(0, nbr_entraxe_y):
            z = jz * elancement
            x = jx * entraxe_x
            y = jy * entraxe_y + (entraxe_y + rayon_colonne * 2 + longueur_passe_milieu)

            trame2.append((x, y, z))

for location in trame2:

    colonne(location, etage)
    plaque_colonne(location, etage)

###############################################################################################

trame = []
for iz in range(0, nbr_etage_z):
    for ix in range(0, nbr_entraxe_x):
        for iy in range(0, nbr_entraxe_y):
            z = iz * elancement
            x = ix * entraxe_x
            y = iy * entraxe_y

            trame.append((x, y, z))


for location in trame:

    colonne(location, etage)
    plaque_colonne(location, etage)

trameb = []

for ix in range(0, nbr_entraxe_x):
    for iy in range(0, nbr_entraxe_y):
        for iz in range(nbr_etage_z, nbr_etage_z + randint(0, 3)):
            z = iz * elancement
            x = ix * entraxe_x
            y = iy * entraxe_y

            trameb.append((x, y, z))


for location in trameb:

    colonne(location, etage)
    plaque_colonne(location, etage)

tramed = []

for ix in range(0, nbr_entraxe_x):
    for iy in range(0, nbr_entraxe_y):
        for iz in range(nbr_etage_z, nbr_etage_z + randint(0, 2)):
            z = iz * elancement
            x = ix * entraxe_x
            y = iy * entraxe_y + (entraxe_y + rayon_colonne * 2 + longueur_passe_milieu)

            tramed.append((x, y, z))


for location in tramed:

    colonne(location, etage)
    plaque_colonne(location, etage)
###############################################################################################

###############################################################################################
l_passe = []
for iy in range(nbr_entraxe_y):

    l = []

    for colonne in trame:
        l.append(colonne)
    for colonne in trame2:
        l.append(colonne)
    for colonne in trameb:
        l.append(colonne)
    for colonne in tramed:
        l.append(colonne)

    for index in range(0, len(l) - 1):
        if (
            l[index + 1][1] - l[index][1] == entraxe_y
            and l[index + 1][0] == l[index][0]
        ):
            l_passe.append(l[index + 1])

# print (l_passe)
for i in range(0, randint(0, nbr_entraxe_x * nbr_entraxe_y * iz)):
    choix = choice(l_passe)
    l_passe.remove(choix)
lp = set(l_passe)

for location in lp:
    passerelle_y(location, etage)
###############################################################################################

###############################################################################################

l_dalle = []
for ix in range(nbr_entraxe_x):
    ld = []

    for passerelle_y in l_passe:
        ld.append(passerelle_y)

    for index in range(len(ld) - 1):
        if (
            ld[index + 1][0] - ld[index][0] == entraxe_x
            and ld[index + 1][1] == ld[index][1]
        ):
            l_dalle.append(ld[index + 1])

l = set(l_dalle)
print(len(l), len(l_dalle))


for location in l:
    dalle(location, etage)

###############################################################################################


passe_x = []

for mx in range(0, nbr_entraxe_x - 1):
    for my in range(0, nbr_entraxe_y):
        for mz in range(0, nbr_etage_z):
            z = mz * elancement
            x = mx * entraxe_x + entraxe_x
            y = my * entraxe_y

            passe_x.append((x, y, z))
            passe_x.append(
                (x, y + (entraxe_y + rayon_colonne * 2 + longueur_passe_milieu), z)
            )


for m in range(0, randint(0, 12 * iz)):
    choix_x = choice(passe_x)
    passe_x.remove(choix_x)

for position in passe_x:
    passerelle_x(position, etage)

##################################################################################################


passe_milieu = []
for iz in range(0, nbr_etage_z):
    for ix in range(0, nbr_entraxe_x):
        for iy in range(0, nbr_entraxe_y - 1):

            z = iz * elancement
            x = ix * entraxe_x
            y = iy + (entraxe_y + rayon_colonne + longueur_passe_milieu / 2)

            passe_milieu.append((x, y, z))

for i in range(0, randint(0, 3 * iz)):
    choix_milieu = choice(passe_milieu)
    passe_milieu.remove(choix_milieu)
for position in passe_milieu:
    passerelle_milieu(position, etage)


#######################################################################################################

trame_centre = []
for ix in range(nbr_entraxe_x):
    lg = []

    for passerelle_milieu in passe_milieu:
        lg.append(passerelle_milieu)

    for index in range(len(lg) - 1):
        if (
            lg[index + 1][0] - lg[index][0] == entraxe_x
            and lg[index + 1][1] == lg[index][1]
            and lg[index + 1][2] == lg[index][2]
        ):
            trame_centre.append(lg[index + 1])

lu = set(trame_centre)

for location in lu:
    dalle_centre(location, etage)

###############################################################################################
passe_x.sort()
l_mur_x = []
for ix in range(nbr_entraxe_x):
    lm = []

    for passerelle_x in passe_x:
        lm.append(passerelle_x)

    for index in range(len(lm) - 1):
        if (
            lm[index + 1][2] - lm[index][2] == 4
            and lm[index + 1][1] == lm[index][1]
            and lm[index + 1][0] == lm[index][0]
        ):
            l_mur_x.append(lm[index + 1])

lr = set(l_mur_x)


for location in lr:
    mur_x(location, etage)
##########################################################################################################
l_passe.sort()
l_mur_y = []
for ix in range(nbr_entraxe_x):
    lu = []

    for passerelle_y in l_passe:
        lu.append(passerelle_y)

    for index in range(len(lu) - 1):
        if (
            lu[index + 1][2] - lu[index][2] == 4
            and lu[index + 1][1] == lu[index][1]
            and lu[index + 1][0] == lu[index][0]
        ):
            l_mur_y.append(lu[index + 1])

lo = set(l_mur_y)


for location in lo:
    mur_y(location, etage)
##########################################################################################################
l_dalle.sort()
l_mur_dalle = []
for ix in range(nbr_entraxe_x):
    lk = []

    for dalle in l_dalle:
        lk.append(dalle)

    for index in range(len(lk) - 1):
        if (
            lk[index + 1][2] - lk[index][2] == 4
            and lk[index + 1][1] == lk[index][1]
            and lk[index + 1][0] == lk[index][0]
        ):
            l_mur_dalle.append(lk[index + 1])

lv = set(l_mur_dalle)


for location in lv:
    mur_dalle(location, etage)
##########################################################################################################
passe_milieu.sort()
l_mur_milieu = []
for ix in range(nbr_entraxe_x):
    lq = []

    for passerelle_milieu in passe_milieu:
        lq.append(passerelle_milieu)

    for index in range(len(lq) - 1):
        if (
            lq[index + 1][2] - lq[index][2] == 4
            and lq[index + 1][1] == lq[index][1]
            and lq[index + 1][0] == lq[index][0]
        ):
            l_mur_milieu.append(lq[index + 1])

lt = set(l_mur_milieu)


for location in lt:
    mur_milieu(location, etage)
##########################################################################################################

trame_centre.sort()
l_mur_centre = []
for ix in range(nbr_entraxe_x):
    lf = []

    for dalle_centre in trame_centre:
        lf.append(dalle_centre)

    for index in range(len(lf) - 1):
        if (
            lf[index + 1][2] - lf[index][2] == 4
            and lf[index + 1][1] == lf[index][1]
            and lf[index + 1][0] == lf[index][0]
        ):
            l_mur_centre.append(lf[index + 1])

ls = set(l_mur_centre)


for location in ls:
    mur_centre(location, etage)
