# SPDX-FileCopyrightText: 2013 Quentin Delaval & Quentin Parete
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# script - AIM - Module I - Ledoux - 23/01/2014
# développé sous Blender 2.66a / Windows 64bits
# Quentins Parete/Delaval

import bpy
import random
from random import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

bpy.ops.mesh.primitive_plane_add(radius=50, location=(0, 0, 0))

bpy.ops.object.lamp_add(
    type="SUN",
    view_align=False,
    location=(5, 32, 1),
    rotation=(-0.38, 0.45, 0.65),
    layers=(
        False,
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)


def Camera_Axono():
    bpy.ops.object.camera_add(location=(8.0, -15.0, 12.0), rotation=(1.2, 0.0, -5.7))
    ob = bpy.context.object
    ob.data.type = "ORTHO"
    ob.data.ortho_scale = 50.0
    bpy.context.scene.camera = ob

    for area in bpy.context.screen.areas:
        if area.type == "VIEW_3D":
            for region in area.regions:
                if region.type == " WINDOW":
                    override = {
                        " window": bpycontextwindow,
                        "area": area,
                        "region": region,
                        "scene": bpyc.ontext.scene,
                        "active_object": ob,
                    }
                    bpy.ops.view3d.object_as_camera(override)


Camera_Axono()

print("---[DEFINITION DES VARIABLES]---")

global NbrToises
global NbToises
global HauteurSocle
global C


NbToises = choice([3.5, 5, 6, 7.5])
NbrToises = choice([4.935, 7.05, 8.46, 10.575])
HauteurSocle = choice([0, 1.5, 2])
LocationSocle = choice([-1, -0.75, 0, 0.75, 1])
C = choice([0, 1, 2, 2.5, 3, 4])

print("---> Base de dimensions", (NbrToises / 1.41) * 2, "toises")


# ---------------------------------------------------------
# Définition de la base
# ---------------------------------------------------------

if HauteurSocle == 2:
    LocationSocle = choice([1, -1])
elif HauteurSocle == 1.5:
    LocationSocle = choice([-0.75, 0.75])
elif HauteurSocle == 0:
    LocationSocle = 0

bpy.ops.mesh.primitive_cylinder_add(
    vertices=4,
    radius=NbrToises,
    depth=HauteurSocle,
    location=(0, 0, LocationSocle),
    rotation=(0, 0, 0.78539816339745),
)


if LocationSocle < 0:
    print("---> Sous-bassement enterré")
if LocationSocle == 0:
    print("---> Absence de Sous-bassement")
if LocationSocle > 0:
    print("---> Sous-bassement en surface")

if HauteurSocle == 1.5:
    print("---> Hauteur Sous-bassement = 1.5 toises")
if HauteurSocle == 2:
    print("---> Hauteur Sous-bassement = 2 toises")

if LocationSocle <= 0:
    print("---> Piano Nobile positionné au sol")
if LocationSocle == 0.75:
    print("---> Piano Nobile positionné à 1.5 toises de haut")
if LocationSocle == 1:
    print("---> Piano Nobile positionné à 2 toises de haut")

bpy.context.object.scale[0] = 1.2
bpy.context.object.scale[1] = 1.2

# -----------------------------------------------------------
# Définition du carré de base
# -----------------------------------------------------------

HauteurBati = choice([2.5, 4, 4.5, 5, 6, 7])
bpy.ops.mesh.primitive_cylinder_add(
    vertices=4,
    radius=NbrToises,
    depth=HauteurBati,
    location=(0, 0, LocationSocle),
    rotation=(0, 0, 0.78539816339745),
)

if LocationSocle == -1:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == -0.75:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == 0:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == 0.75:
    bpy.context.object.location[2] = 1.5 + HauteurBati / 2
if LocationSocle == 1:
    bpy.context.object.location[2] = 2 + HauteurBati / 2

print("---> Répétition du Piano Nobile sur", HauteurBati, "toises de haut")


# -----------------------------------
# Définition du cercle
# -----------------------------------------------------------

if NbrToises == 4.935:
    NbToises = 3.5
elif NbrToises == 7.05:
    NbToises = 5
elif NbrToises == 8.46:
    NbToises = 6
elif NbrToises == 10.575:
    NbToises = 7.5

if NbToises == 3.5:
    Rcercle = 0
elif NbToises == 5:
    Rcercle = choice([0, 1, 2])
elif NbToises == 6:
    Rcercle = choice([0, 1, 2])
elif NbToises == 7.5:
    Rcercle = choice([0, 1, 2])

if Rcercle != 0:
    print("---> cercle de rayon :", Rcercle, "toises")
else:
    print("---> Absence de cercle")

A = choice([0, 1.5, 2])

bpy.ops.mesh.primitive_cylinder_add(
    vertices=128,
    radius=Rcercle,
    depth=HauteurBati + A,
    location=(0, 0, LocationSocle),
    rotation=(0, 0, 0.78539816339745),
)

if LocationSocle == -1:
    bpy.context.object.location[2] = (HauteurBati + A) / 2
if LocationSocle == -0.75:
    bpy.context.object.location[2] = (HauteurBati + A) / 2
if LocationSocle == 0:
    bpy.context.object.location[2] = (HauteurBati + A) / 2
if LocationSocle == 0.75:
    bpy.context.object.location[2] = 1.5 + (HauteurBati + A) / 2
if LocationSocle == 1:
    bpy.context.object.location[2] = 2 + (HauteurBati + A) / 2

cercle = bpy.context.object


# ------------------------------------------------------------
# Définition du carré
# ------------------------------------------------------------

bpy.ops.object.modifier_add(type="BOOLEAN")

if NbrToises == 4.935:
    NbToises = 3.5
elif NbrToises == 7.05:
    NbToises = 5
elif NbrToises == 8.46:
    NbToises = 6
elif NbrToises == 10.575:
    NbToises = 7.5

if NbToises == 3.5:
    Rcarre = 0
elif NbToises == 5:
    Rcarre = choice([0, 2.5, 3])
elif NbToises == 6:
    Rcarre = choice([0, 3, 4.5])
elif NbToises == 7.5:
    Rcarre = choice([3.5, 4.5])

if Rcarre != 0:
    print("---> carré de côté :", Rcarre * 2, "toises")
else:
    print("---> Absence de carré")

B = choice([0.5, 1])

bpy.ops.mesh.primitive_cylinder_add(
    vertices=4,
    radius=Rcarre * 1.41,
    depth=HauteurBati + B,
    location=(0, 0, LocationSocle),
    rotation=(0, 0, 0.78539816339745),
)

if LocationSocle == -1:
    bpy.context.object.location[2] = (HauteurBati + B) / 2
if LocationSocle == -0.75:
    bpy.context.object.location[2] = (HauteurBati + B) / 2
if LocationSocle == 0:
    bpy.context.object.location[2] = (HauteurBati + B) / 2
if LocationSocle == 0.75:
    bpy.context.object.location[2] = 1.5 + (HauteurBati + B) / 2
if LocationSocle == 1:
    bpy.context.object.location[2] = 2 + (HauteurBati + B) / 2

carre = bpy.context.object

creuser = cercle.modifiers["Boolean"]
creuser.object = carre
creuser.operation = choice(["DIFFERENCE", "INTERSECT"])

bpy.ops.object.move_to_layer(
    layers=(
        False,
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    )
)


# ------------------------------------------------------------
# Définition du déambulatoire
# ------------------------------------------------------------

if NbrToises == 4.935:
    NbToises = 3.5
elif NbrToises == 7.05:
    NbToises = 5
elif NbrToises == 8.46:
    NbToises = 6
elif NbrToises == 10.575:
    NbToises = 7.5

if NbToises == 3.5:
    Rcarre = 0
elif NbToises == 5:
    Rcarre = choice([0, 4, 4.25])
elif NbToises == 6:
    Rcarre = choice([0, 5, 5.25])
elif NbToises == 7.5:
    Rcarre = choice([6.5, 6.75])

bpy.ops.mesh.primitive_cylinder_add(
    vertices=4,
    radius=Rcarre * 1.41,
    depth=HauteurBati,
    location=(0, 0, LocationSocle),
    rotation=(0, 0, 0.78539816339745),
)

if LocationSocle == -1:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == -0.75:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == 0:
    bpy.context.object.location[2] = HauteurBati / 2
if LocationSocle == 0.75:
    bpy.context.object.location[2] = 1.5 + HauteurBati / 2
if LocationSocle == 1:
    bpy.context.object.location[2] = 2 + HauteurBati / 2

if Rcarre != 0:
    print("---> Déambulatoire de largeur :", NbToises - Rcarre, "toises")
else:
    print("---> Absence de déambulatoire")


# ----------------------------------------------------------
# Définition de la croix
# ----------------------------------------------------------

# Définition des 4 carrés pour NbToises = 7

RpetitCarre = choice([1.41, 2.115])
if RpetitCarre == 1.41 and NbToises == 3.5:
    posX = 2.5
    posY = 2.5
if RpetitCarre == 2.115 and NbToises == 3.5:
    posX = 2
    posY = 2
if LocationSocle <= 0:
    posZ = (HauteurBati + C) / 2
if LocationSocle == 0.75:
    posZ = 1.5 + (HauteurBati + C) / 2
if LocationSocle == 1:
    posZ = 2 + (HauteurBati + C) / 2

if NbToises == 3.5:
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    print("---> Travée de largeur", (NbToises - (RpetitCarre / 1.41) * 2) * 2, "toises")

# Définition des 4 carrés pour NbToises = 10

RpetitCarre = choice([2.4675, 2.115])
if RpetitCarre == 2.4675 and NbToises == 5:
    posX = 3.25
    posY = 3.25
if RpetitCarre == 2.115 and NbToises == 5:
    posX = 3.5
    posY = 3.5
if LocationSocle <= 0:
    posZ = (HauteurBati + C) / 2
if LocationSocle == 0.75:
    posZ = 1.5 + (HauteurBati + C) / 2
if LocationSocle == 1:
    posZ = 2 + (HauteurBati + C) / 2

if NbToises == 5:
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    print("---> Travée de largeur", (NbToises - (RpetitCarre / 1.41) * 2) * 2, "toises")


# Définition des 4 carrés pour NbToises = 12

RpetitCarre = choice([2.4675, 2.82])
if RpetitCarre == 2.4675 and NbToises == 6:
    posX = 4.25
    posY = 4.25
if RpetitCarre == 2.82 and NbToises == 6:
    posX = 4
    posY = 4
if LocationSocle <= 0:
    posZ = (HauteurBati + C) / 2
if LocationSocle == 0.75:
    posZ = 1.5 + (HauteurBati + C) / 2
if LocationSocle == 1:
    posZ = 2 + (HauteurBati + C) / 2

if NbToises == 6:
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    print("---> Travée de largeur", (NbToises - (RpetitCarre / 1.41) * 2) * 2, "toises")

# Définition des 4 carrés pour NbToises = 15

RpetitCarre = choice([3.525, 2.82])
if RpetitCarre == 3.525 and NbToises == 7.5:
    posX = 5
    posY = 5
if RpetitCarre == 2.82 and NbToises == 7.5:
    posX = 5.5
    posY = 5.5
if LocationSocle <= 0:
    posZ = (HauteurBati + C) / 2
if LocationSocle == 0.75:
    posZ = 1.5 + (HauteurBati + C) / 2
if LocationSocle == 1:
    posZ = 2 + (HauteurBati + C) / 2

if NbToises == 7.5:
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4,
        radius=RpetitCarre,
        depth=HauteurBati + C,
        location=(-posX, -posY, posZ),
        rotation=(0, 0, 0.78539816339745),
    )
    print("---> Travée de largeur", (NbToises - (RpetitCarre / 1.41) * 2) * 2, "toises")

# ----------------------------------------------------------
# Définition du Nombre d'étages
# ----------------------------------------------------------

if NbrToises == 4.935:
    NbToises = 3.5
elif NbrToises == 7.05:
    NbToises = 5
elif NbrToises == 8.46:
    NbToises = 6
elif NbrToises == 10.575:
    NbToises = 7.5

# Bâti de 2.5 Toises de haut - Position des planchers

if HauteurBati == 2.5 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 1.5))
    print("---> Hauteur sous plafond du Piano Nobile : 1.5 toises")
    print("---> Hauteur sous plafond du second niveau : 1 toises")

elif HauteurBati == 2.5 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3))
    print("---> Hauteur sous plafond du Piano Nobile : 1.5 toises")
    print("---> Hauteur sous plafond du second niveau : 1 toises")

elif HauteurBati == 2.5 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3.5))
    print("---> Hauteur sous plafond du Piano Nobile : 1.5 toises")
    print("---> Hauteur sous plafond du second niveau : 1 toises")

# Bâti de 4 Toises de haut - Position des planchers

if HauteurBati == 4 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 2))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

elif HauteurBati == 4 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3.5))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

elif HauteurBati == 4 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

# Bâti de 4.5 Toises de haut - Position des planchers

if HauteurBati == 4.5 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 2.5))
    print("---> Hauteur sous plafond du Piano Nobile : 2.5 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

elif HauteurBati == 4.5 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4))
    print("---> Hauteur sous plafond du Piano Nobile : 2.5 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

elif HauteurBati == 4.5 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4.5))
    print("---> Hauteur sous plafond du Piano Nobile : 2.5 toises")
    print("---> Hauteur sous plafond du second niveau : 2 toises")

# Bâti de 5 Toises de haut - Position des planchers

if HauteurBati == 5 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 2))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3.5))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

elif HauteurBati == 5 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3.5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 5))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

elif HauteurBati == 5 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4.5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 5.5))
    print("---> Hauteur sous plafond du Piano Nobile : 2 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

# Bâti de 6 Toises de haut - Position des planchers

if HauteurBati == 6 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4.5))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")
elif HauteurBati == 6 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4.5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 6))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

elif HauteurBati == 6 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 6.5))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 1.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

# Bâti de 7 Toises de haut - Position des planchers

if HauteurBati == 7 and LocationSocle <= 0:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 3))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 5.5))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 2.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

elif HauteurBati == 7 and HauteurSocle == 1.5:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 4.5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 7))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 2.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

elif HauteurBati == 7 and HauteurSocle == 2:
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 5))
    bpy.ops.mesh.primitive_plane_add(radius=NbToises, location=(0, 0, 7.5))
    print("---> Hauteur sous plafond du Piano Nobile : 3 toises")
    print("---> Hauteur sous plafond du second niveau : 2.5 toises")
    print("---> Hauteur sous plafond du troisème niveau : 1.5 toises")

if A == 1.5 and Rcercle > 0:
    print("---> Présence d'une tour circulaire de 1.5 toises de hauteur")
if A == 2 and Rcercle > 0:
    print("---> Présence d'une tour circulaire de 2 toises de hauteur")

if B == 0.5 and Rcarre > 0:
    print("---> Présence d'une tour carrée de 0.5 toises de hauteur")
if B == 1 and Rcarre > 0:
    print("---> Présence d'une tour carrée de 1 toises de hauteur")

if C == 1 and Rcarre > 0:
    print("---> Présence de tourelles carrées de 1 toises de hauteur")
if C == 2 and Rcarre > 0:
    print("---> Présence de tourelles carrées de 2 toises de hauteur")
if C == 2.5 and Rcarre > 0:
    print("---> Présence de tourelles carrées de 2.5 toises de hauteur")
if C == 3 and Rcarre > 0:
    print("---> Présence de tourelles carrées de 3 toises de hauteur")
if C == 4 and Rcarre > 0:
    print("---> Présence de tourelles carrées de 4 toises de hauteur")
