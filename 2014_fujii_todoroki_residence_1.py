# SPDX-FileCopyrightText: 2014 Hong Trang Mai
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=True)

# Blender 7.1
# Windows Vista & Windows 7

ladder1 = []
ladder2 = []

# ---------------------------------------------------------------------------
# Floor
# ---------------------------------------------------------------------------

# Ground
bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, -0.15))
bpy.ops.transform.resize(value=(10.5, 10.5, 0.3))

# Between 1st floor and 2nd florr
bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, 5.05))
bpy.ops.transform.resize(value=(10, 10, 0.1))

# ---------------------------------------------------------------------------
# Box 1st Floor
# ---------------------------------------------------------------------------


# Définir 4x BoxBig
def BoxBig(x):
    # Colonnes:
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):
            # y-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 2.35 * i, x[1] + 2.15 * k, 2.5)
            )
            bpy.ops.transform.resize(value=(0.3, 0.7, 5))
            # x-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 2 * i, x[1] + 2.35 * k, 2.5)
            )
            bpy.ops.transform.resize(value=(0.4, 0.3, 5))

    # Linteaux
    for i in range(-1, 2, 2):
        # x-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0], x[1] + 2.35 * i, 4.65)
        )
        bpy.ops.transform.resize(value=(3.6, 0.3, 0.7))
        # y-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0] - 2.35 * i, x[1], 4.65)
        )
        bpy.ops.transform.resize(value=(0.3, 3.6, 0.7))


def BoxMedium(x):
    # 3x BoxMedium
    # Colonnes
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):
            # y-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 1.85 * i, x[1] + 1.65 * k, 2.25)
            )
            bpy.ops.transform.resize(value=(0.3, 0.7, 4.5))
            # x-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 1.5 * i, x[1] + 1.85 * k, 2.25)
            )
            bpy.ops.transform.resize(value=(0.4, 0.3, 4.5))

    # Linteaux
    for i in range(-1, 2, 2):
        # x-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0], x[1] + 1.85 * i, 3.75)
        )
        bpy.ops.transform.resize(value=(2.6, 0.3, 1.5))
        # y-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0] - 1.85 * i, x[1], 3.75)
        )
        bpy.ops.transform.resize(value=(0.3, 2.6, 1.5))


# Définir 1x BoxSmall -> déplacer 0,5m en x-&y-direction -> centralisé
def BoxSmall(x):
    # Colonnes
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):

            if x[0] > 0:
                # QuadrantI: x:positive, y:positive
                if x[1] > 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1.35 * i, x[1] - 0.5 + 1.15 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1 * i, x[1] - 0.5 + 1.35 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))
                # QuadrantIV: x:positive, y:negative
                if x[1] < 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1.35 * i, x[1] + 0.5 + 1.15 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1 * i, x[1] + 0.5 + 1.35 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))
            if x[0] < 0:
                # QuadrantII: x:negative, y:positive
                if x[1] > 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1.35 * i, x[1] - 0.5 + 1.15 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1 * i, x[1] - 0.5 + 1.35 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))
                # QuadrantIII: x:negative, y:negative
                if x[1] < 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1.35 * i, x[1] + 0.5 + 1.15 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1 * i, x[1] + 0.5 + 1.35 * k, 1.5),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))

    # Linteaux
    for i in range(-1, 2, 2):
        if x[0] > 0:
            # QuadrantI: x:positive, y:positive
            if x[1] > 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5, x[1] - 0.5 - 1.35 * i, 2.75)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5 - 1.35 * i, x[1] - 0.5, 2.75)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
            # QuadrantIV: x:positive, y:negative
            if x[1] < 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5, x[1] + 0.5 + 1.35 * i, 2.75)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5 - 1.35 * i, x[1] + 0.5, 2.75)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
        if x[0] < 0:
            # QuadrantII: x:negative, y:positive
            if x[1] > 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5, x[1] - 0.5 + 1.35 * i, 2.75)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5 - 1.35 * i, x[1] - 0.5, 2.75)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
            # QuadrantIII: x:negative, y:negative
            if x[1] < 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5, x[1] + 0.5 + 1.35 * i, 2.75)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5 - 1.35 * i, x[1] + 0.5, 2.75)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))


# Crée BoxBig
coordinates1 = []
for i in range(-1, 3, 2):
    for k in range(-1, 2, 2):
        coordinates1.append((i * 2.55, k * 2.55, 2.5))
for i in range(len(coordinates1)):
    BoxBig(coordinates1[i])


# Crée BoxMedium
coordinates2 = []
for i in range(-1, 2, 2):
    for k in range(-1, 2, 2):
        coordinates2.append((i * 2.05, k * 2.05, 2))
print(coordinates2)
a = random.randint(0, len(coordinates2) - 1)
del coordinates2[a]
for i in range(len(coordinates2)):
    BoxMedium(coordinates2[i])


# Crée BoxSmall dans BoxMedium
coordinates3 = []
print(coordinates2)
positionSmallCube = random.choice(coordinates2)
print(positionSmallCube)

# BoxSmall jamais sous BoxSmallII
BoxSmall(positionSmallCube)

exclude = (positionSmallCube[0], positionSmallCube[1], 7)
print("test:", exclude)

# ---------------------------------------------------------------------------
# Box 2nd Floor
# ---------------------------------------------------------------------------


# Définir 4x BoxBigII
def BoxBigII(x):
    # Colonnes
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):
            # y-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 2.35 * i, x[1] + 2.15 * k, 7.6)
            )
            bpy.ops.transform.resize(value=(0.3, 0.7, 5))
            # x-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 2 * i, x[1] + 2.35 * k, 7.6)
            )
            bpy.ops.transform.resize(value=(0.4, 0.3, 5))

    # Linteaux
    for i in range(-1, 2, 2):
        # x-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0], x[1] + 2.35 * i, 9.75)
        )
        bpy.ops.transform.resize(value=(3.6, 0.3, 0.7))
        # y-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0] - 2.35 * i, x[1], 9.75)
        )
        bpy.ops.transform.resize(value=(0.3, 3.6, 0.7))


# Définir 3x BoxMediumII
def BoxMediumII(x):
    # Colonnes
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):
            # y-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 1.85 * i, x[1] + 1.65 * k, 7.25)
            )
            bpy.ops.transform.resize(value=(0.3, 0.7, 4.3))
            # x-direction-segment
            bpy.ops.mesh.primitive_cube_add(
                radius=0.5, location=(x[0] + 1.5 * i, x[1] + 1.85 * k, 7.25)
            )
            bpy.ops.transform.resize(value=(0.4, 0.3, 4.3))

    # Linteaux
    for i in range(-1, 2, 2):
        # x-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0], x[1] + 1.85 * i, 8.65)
        )
        bpy.ops.transform.resize(value=(2.6, 0.3, 1.5))
        # y-direction
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(x[0] - 1.85 * i, x[1], 8.65)
        )
        bpy.ops.transform.resize(value=(0.3, 2.6, 1.5))


# Définir 1x BoxSmall -> déplacer 0,5m en x-&y-direction -> décentralisé
def BoxSmallII(x):
    if x[0] > 0:
        print("test1")
    if x[0] > 0:
        print("test2")

    # Colonnes
    for i in range(-1, 2, 2):
        for k in range(-1, 2, 2):

            if x[0] > 0:
                # QuadrantI: x:positive, y:positive
                if x[1] > 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1.35 * i, x[1] + 0.5 + 1.15 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1 * i, x[1] + 0.5 + 1.35 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))
                # QuadrantIV: x:positive, y:negative
                if x[1] < 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1.35 * i, x[1] - 0.5 + 1.15 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] + 0.5 + 1 * i, x[1] - 0.5 + 1.35 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))

            if x[0] < 0:
                # QuadrantII: x:negative, y:positive
                if x[1] > 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1.35 * i, x[1] + 0.5 + 1.15 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1 * i, x[1] + 0.5 + 1.35 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))
                # QuadrantIII: x:negative, y:negative
                if x[1] < 0:
                    # y-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1.35 * i, x[1] - 0.5 + 1.15 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.3, 0.7, 3))
                    # x-direction-segment
                    bpy.ops.mesh.primitive_cube_add(
                        radius=0.5,
                        location=(x[0] - 0.5 + 1 * i, x[1] - 0.5 + 1.35 * k, 6.6),
                    )
                    bpy.ops.transform.resize(value=(0.4, 0.3, 3))

    # Linteaux
    for i in range(-1, 2, 2):
        if x[0] > 0:
            # QuadrantI: x:positive, y:positive
            if x[1] > 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5, x[1] + 0.5 + 1.35 * i, 7.85)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5 - 1.35 * i, x[1] + 0.5, 7.85)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
            # QuadrantIV: x:positive, y:negative
            if x[1] < 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5, x[1] - 0.5 + 1.35 * i, 7.85)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] + 0.5 - 1.35 * i, x[1] - 0.5, 7.85)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
        if x[0] < 0:
            # QuadrantII: x:negative, y:positive
            if x[1] > 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5, x[1] + 0.5 + 1.35 * i, 7.85)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5 - 1.35 * i, x[1] + 0.5, 7.85)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))
            # QuadrantIII: x:negative, y:negative
            if x[1] < 0:
                # x-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5, x[1] - 0.5 + 1.35 * i, 7.85)
                )
                bpy.ops.transform.resize(value=(1.6, 0.3, 0.5))
                # y-direction
                bpy.ops.mesh.primitive_cube_add(
                    radius=0.5, location=(x[0] - 0.5 - 1.35 * i, x[1] - 0.5, 7.85)
                )
                bpy.ops.transform.resize(value=(0.3, 1.6, 0.5))


# Crée BoxBig
coordinates1II = []
for i in range(-1, 3, 2):
    for k in range(-1, 2, 2):
        coordinates1II.append((i * 2.55, k * 2.55, 7.6))
for i in range(len(coordinates1II)):
    BoxBigII(coordinates1II[i])

# Crée BoxMedium
coordinates2II = []
for i in range(-1, 2, 2):
    for k in range(-1, 2, 2):
        coordinates2II.append((i * 2.05, k * 2.05, 7.1))
print(coordinates2II)
a = random.randint(0, len(coordinates2II) - 1)
del coordinates2II[a]
for i in range(len(coordinates2II)):
    BoxMediumII(coordinates2II[i])

# Crée BoxSmall dans BoxMedium
coordinates3II = []
print(coordinates2II)
positionSmallCube2 = random.choice(coordinates2II)
print(positionSmallCube2)
BoxSmallII(positionSmallCube2)


# ---------------------------------------------------------------------------
# Murs et ouvertures
# ---------------------------------------------------------------------------


# Define Exterior walls
def ex_walls_X(x):
    # walls x-direction
    if abs(x[0]) < abs(x[1]):
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(3.6, 0.3, 4.3))
    # walls y-direction
    else:
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(0.3, 3.6, 4.3))


# Crée exterior walls random
walls = []
for i in range(-1, 2, 2):
    for j in range(-1, 2, 2):
        walls.append((2.55 * j, 4.9 * i, 2.15))
        walls.append((-4.9 * j, 2.55 * i, 2.15))

for _ in range(0, 4):
    a = random.choice(walls)
    ex_walls_X(a)
    ladder1.append(a)
    walls.remove(a)


# Inner walls
def in_walls_X(x):
    # walls x-direction
    if abs(x[0]) > abs(x[1]):
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(3, 0.7, 5))
    # walls y-direction
    else:
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(0.7, 3, 5))


# Crée interior walls random
walls2 = []
for i in range(-1, 2, 2):
    walls2.append((0, 3.5 * i, 2.5))
    walls2.append((3.5 * i, 0, 2.5))

print(walls2)

for _ in range(0, 3):
    a = random.choice(walls2)
    in_walls_X(a)
    walls2.remove(a)

# ---------------------------------------------------------------------------
# Walls2nd storey
# ---------------------------------------------------------------------------


# Exterior walls


def ex_walls_X(x):
    # walls x-direction
    if abs(x[0]) < abs(x[1]):
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(3.6, 0.3, 4.3))
    # walls y-direction
    else:
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(0.3, 3.6, 4.3))


# Crée exterior walls random
walls2nd = []
for i in range(-1, 2, 2):
    for j in range(-1, 2, 2):
        walls2nd.append((2.55 * j, 4.9 * i, 7.25))
        walls2nd.append((-4.9 * j, 2.55 * i, 7.25))

for _ in range(0, 4):
    a = random.choice(walls2nd)
    ex_walls_X(a)
    ladder2.append(a)
    walls2nd.remove(a)


# Inner walls


def in_walls_X(x):
    # walls x-direction
    if abs(x[0]) > abs(x[1]):
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(3, 0.6, 5))
    # walls y-direction
    else:
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
        bpy.ops.transform.resize(value=(0.6, 3, 5))


# Crée interior walls random
walls2 = []
for i in range(-1, 2, 2):
    walls2.append((0, 3.5 * i, 7.6))
    walls2.append((3.5 * i, 0, 7.6))

print(walls2)

for _ in range(0, 3):
    a = random.choice(walls2)
    in_walls_X(a)
    walls2.remove(a)


# ---------------------------------------------------------------------------
# Windows
# ---------------------------------------------------------------------------


# Define fenêtre
def fenêtre(x):
    # lower wall x-direction
    if abs(x[0]) < abs(x[1]):
        for a in range(0, 1, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(3.6, 0.3, 0.2))
            bpy.ops.transform.translate(value=(0, 0, a - 2.05))
    # lower wall y-direction
    else:
        for a in range(0, 1, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.3, 3.6, 0.2))
            bpy.ops.transform.translate(value=(0, 0, a - 2.05))

    # sash bar horizontal x-direction
    if abs(x[0]) < abs(x[1]):
        for a in range(0, 3, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(3.3, 0.1, 0.1))
            bpy.ops.transform.translate(value=(0, 0, a - 0.9))
    # sash bar horizontal y-direction
    else:
        for a in range(0, 3, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.1, 3.3, 0.1))
            bpy.ops.transform.translate(value=(0, 0, a - 0.9))

    # sash bar vertical x-direction
    if abs(x[0]) < abs(x[1]):
        for a in range(0, 3, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.1, 0.1, 3.6))
            bpy.ops.transform.translate(value=(a - 1.33, 0, 0.1))

    # sash bar vertical y-direction
    else:
        for a in range(0, 3, 1):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.1, 0.1, 3.6))
            bpy.ops.transform.translate(value=(0, a - 1.33, 0.1))

    # châssis de fenêtre x-direction horizontal
    if abs(x[0]) < abs(x[1]):
        for a in range(-1, 3, 2):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(3.6, 0.1, 0.25))
            bpy.ops.transform.translate(value=(0, 0, a * 3.85 / 2 + 0.1))

    # châssis de fenêtre y-direction horizontal
    else:
        for a in range(-1, 3, 2):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.1, 3.6, 0.25))
            bpy.ops.transform.translate(value=(0, 0, a * 3.85 / 2 + 0.1))

    # châssis de fenêtre x-direction vertical
    if abs(x[0]) < abs(x[1]):
        for a in range(-1, 3, 2):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.15, 0.1, 3.6))
            bpy.ops.transform.translate(value=(a * 3.45 / 2, 0, 0.1))

    # châssis de fenêtre y-direction vertical
    else:
        for a in range(-1, 3, 2):
            bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
            bpy.ops.transform.resize(value=(0.1, 0.15, 3.6))
            bpy.ops.transform.translate(value=(0, a * 3.45 / 2, 0.1))


# Crée fenêtre random

for _ in range(0, 4):
    a = random.choice(walls)
    fenêtre(a)
    walls.remove(a)

for _ in range(0, 4):
    a = random.choice(walls2nd)
    fenêtre(a)
    walls2nd.remove(a)

# ---------------------------------------------------------------------------
# échelle
# ---------------------------------------------------------------------------


def échelle(x):
    # ladder beam x-direction
    if abs(x[0]) < abs(x[1]):
        # for QuadrantI & QuadrantII
        if x[1] > 0:
            for a in range(0, 2, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.05, 9.15))
                bpy.ops.transform.translate(value=(a * 0.45 - 0.225, 0.175, 3.3))
        # for QuadrantIII & QuadrantIV
        else:
            for a in range(0, 2, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.05, 9.15))
                bpy.ops.transform.translate(value=(a * 0.45 - 0.225, -0.175, 3.3))

    # ladder beam vertical y-direction
    else:
        # for QuadrantII & QuadrantIII
        if x[0] < 0:
            for a in range(0, 2, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.05, 9.15))
                bpy.ops.transform.translate(value=(-0.175, a * 0.45 - 0.225, 3.3))
        # for QuadrantI & QuadrantIV
        else:
            for a in range(0, 2, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.05, 9.15))
                bpy.ops.transform.translate(value=(0.175, a * 0.45 - 0.225, 3.3))

    # ladder stroke x-direction
    if abs(x[0]) < abs(x[1]):
        # for QuadrantI & QuadrantII
        if x[1] > 0:
            for a in range(-3, 24, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.4, 0.05, 0.05))
                bpy.ops.transform.translate(value=(0, 0.175, a * 0.35 - 0.2))
        # for QuadrantIII & QuadrantIV
        else:
            for a in range(-3, 24, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.4, 0.05, 0.05))
                bpy.ops.transform.translate(value=(0, -0.175, a * 0.35 - 0.2))

    # ladder stroke y-direction
    else:
        # for QuadrantII & QuadrantIII
        if x[0] < 0:
            for a in range(-3, 24, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.4, 0.05))
                bpy.ops.transform.translate(value=(-0.175, 0, a * 0.35 - 0.2))
        # for QuadrantI & QuadrantIV
        else:
            for a in range(-3, 24, 1):
                bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
                bpy.ops.transform.resize(value=(0.05, 0.4, 0.05))
                bpy.ops.transform.translate(value=(0.175, 0, a * 0.35 - 0.2))


# Crée ladder random

ladder = []

for a in ladder1:
    #    for _ in range (0,17):
    for x in range(0, 4):
        if a[0] == (ladder2[x])[0] and a[1] == (ladder2[x])[1]:
            ladder.append(a)

échelle(random.choice(ladder))
