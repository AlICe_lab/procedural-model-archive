# SPDX-FileCopyrightText: 2015 Adrien Laügt
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# JURY AIM MODULE 1 // 14 JANVIER 2016
# THEME : LA CENTRAAL BEHEER DE HERMAN HERTZBERGER
# développé par ADRIEN LAUGT sous Blender 2.70 hash 19e627c / Windows 8

# 0. BIBLIOTHEQUES ET NETTOYAGE DE BASE //////////////////////////////////////////////

import random
from random import *
import bpy
import numpy

bpy.context.scene.layers[2] = False
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
layer1 = (
    True,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
)
layer2 = (
    False,
    True,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
)
layer4 = (
    False,
    False,
    False,
    True,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
)
layer5 = (
    False,
    False,
    False,
    False,
    True,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
    False,
)
circulations = []
liste_modules = []
pNodes = [(0, 0)]
storeyList = []
cellq1, cellq2, cellq3, cellq4 = [], [], [], []
cellList = [cellq1, cellq2, cellq3, cellq4]
floor_list = []

# ATTENTION POUR UTILISER LE SCRIPT IL FAUT ALLUMER LAYERS 1,2,4,5 et jamais le 3 ou se trouve la caméra

# 1. COORDONNEES////////////////////////////////////////////////////////////////////////


def CellZ():  # trame hauteurs
    cellZ = []
    global cellZ
    z = 0
    while z < 3:
        z = z + 0.3
        cellZ.append(z)


def qCoord(i, m, nodeLoc):  # coordonnées pour générer les pyramides
    node = pNodes[nodeLoc]
    x, y = node
    q1x, q1y, q2x, q2y, q3x, q3y, q4x, q4y = x - 1, y - 1, x + 1, y - 2, x + 1, y, x, y
    circulations = [
        (x, y),
        (x - 1, y),
        (x - 2, y),
        (x, y - 1),
        (x, y - 2),
        (x + 1, y - 1),
        (x + 1, y),
        (x + 1, y + 1),
        (x + 1, y + 2),
        (x + 2, y - 1),
    ]
    for a in range(1, i):
        f1, g1 = q1x - a + 1, q1y
        f2, g2 = q2x, q2y - a + 1
        f3, g3 = q3x + a, q3y
        f4, g4 = q4x, q4y + a
        cellq1.append([])
        cellq2.append([])
        cellq3.append([])
        cellq4.append([])
        floor_list.append([])
        for b in range(0, a):
            cellq1[-1].append((f1 + b, g1 - b))
            cellq2[-1].append((f2 + b, g2 + b))
            cellq3[-1].append((f3 - b, g3 + b))
            cellq4[-1].append((f4 - b, g4 - b))
            floor_list[-1].append(m - a)
        storeyList.append(len(cellq1))


def resetCoord():  # remet à zéro l'ensemble des coordoonnées
    storeyList, cellq1, cellq2, cellq3, cellq4 = [], [], [], [], []
    cellList = [cellq1, cellq2, cellq3, cellq4]
    global storeyList, cellq1, cellq2, cellq3, cellq4, cellList


# ASSEMBALGE DE CELLULES DANS UNE CENTRAAL BEHEER//////////////////////////////////////////////////////


def Cell(location, locz, dimx, dimy, dimz, layer):
    locx, locy = location
    bpy.ops.mesh.primitive_cube_add(location=(locx, locy, locz), layers=layer)
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dimx, dimy, dimz))


def Roof(location, locz, height):
    locx, locy = location
    Cell((locx + 0.4, locy), height / 2, 0.05, 0.2, height, layer2)
    Cell((locx, locy + 0.4), height / 2, 0.2, 0.05, height, layer2)
    Cell((locx - 0.4, locy), height / 2, 0.05, 0.2, height, layer2)
    Cell((locx, locy - 0.4), height / 2, 0.2, 0.05, height, layer2)


def acrotere(location, locz):
    locx, locy = location
    bpy.ops.mesh.primitive_plane_add(
        radius=0.45, location=(locx, locy, locz), layers=layer2
    )
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.inset(thickness=0.05)
    bpy.ops.mesh.delete(type="ONLY_FACE")
    bpy.ops.mesh.select_all(action="SELECT")
    bpy.ops.mesh.extrude_region_move(
        MESH_OT_extrude_region={"mirror": False},
        TRANSFORM_OT_translate={
            "value": (0, 0, 0.1),
            "constraint_axis": (False, False, True),
            "constraint_orientation": "NORMAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )
    bpy.ops.object.editmode_toggle()


def Quadrant(
    o, r, s, ground_offset, detail_level
):  # génère un quart de centraal beheer nivea ude détail à choisir manuellement (roof et acrotère)
    a = cellList[r]
    cellZ.insert(o - 1, o * 0.3 - 0.3)
    for j in range(o - s):
        truc = len(a[j])
        for i in range(truc):
            full_height = cellZ[o - j]
            if ground_offset == 1:
                Cell(
                    a[j][i],
                    (full_height - 0.3) / 2
                    + (full_height / 2 - (full_height - 0.3) / 2)
                    + 0.15,
                    0.8,
                    0.8,
                    full_height - 0.3,
                    layer1,
                )
                # Roof (a[j][i],cellZ[o-j],cellZ[o-j])
                # acrotere (a[j][i],cellZ[o-j]-0.02)
            else:
                Cell(a[j][i], cellZ[o - j] / 2, 0.8, 0.8, cellZ[o - j], layer1)
                # Roof (a[j][i],cellZ[o-j],cellZ[o-j])
                # acrotere (a[j][i],cellZ[o-j]-0.02,)
    cellZ.remove(cellZ[o])


"""
    for j in range (o):                             #cette boucle génère des étages
        truc = len(a[j])
        floor = floor_list[j]
        for i in range (truc):
            for z in range (floor[i]):
                Cell(a[j][i],cellZ[z],0.8,0.8,0.01,layer4)
"""


def Circulations(m, detail_level):
    for loc in range(len(circulations)):
        Cell(circulations[loc], cellZ[m] / 2, 0.8, 0.8, cellZ[m], layer5)
        # Roof (circulations[loc],cellZ[m],cellZ[m])
        # acrotere (circulations[loc],cellZ[m]-0.02)
        for z in range(m):
            Cell(circulations[loc], cellZ[z], 0.8, 0.8, 0.01, layer2)


class cBeheer:
    """génère une centraal beheer"""

    def __init__(
        self, q1, q2, q3, q4, nodeLoc
    ):  # génère quatre quadrants de dimensions données autour d'un noeud
        self.q1 = q1
        self.q2 = q2
        self.q3 = q3
        self.q4 = q4
        self.nodeLoc = nodeLoc
        CellZ()
        height_list = [q1, q2, q3, q4]
        m = max(
            height_list
        )  # le quadrant le plus grand détermine la hauteur des autres
        global m
        qCoord(m + 10, m, nodeLoc)
        for a in range(0, len(height_list)):
            ground = [0, 1]
            offset = choice(ground)
            Quadrant(m, a, m - height_list[a], offset, 0)
        # Circulations (m,0)
        liste_modules.append(self)
        resetCoord()


# MISE EN RESEAU DES CENTRAAL BEHEER ///////////////////////////////////////////////////////////////////

previousChoice = [0, 1]


def GrowthPattern(
    dChoice,
):  # N=1, S=2, E=3,O=4 Détermine un schéma de croissance à partir d'un noeud, selon quatres direction possibles
    x, y = pNodes[
        -1
    ]  # la distance entre le noeud i et le noeud i+1 est aussi fonction du choix, aléatoire ou non( voir plus bas) des dimensions des quadrants situés sur l'axe de croissanace
    global pNodes
    qChoiceActual = [
        randint(inf, sup),
        randint(inf, sup),
        randint(inf, sup),
        randint(inf, sup),
    ]
    qChoiceNext = [
        randint(inf, sup),
        randint(inf, sup),
        randint(inf, sup),
        randint(inf, sup),
    ]
    if dChoice == 1:  # North
        pNodes.append(
            (
                x + 1,
                y
                + max(qChoiceActual[2], qChoiceActual[3])
                + min(qChoiceNext[0], qChoiceNext[1]),
            )
        )
    elif dChoice == 2:  # South
        pNodes.append(
            (
                x - 1,
                y
                - max(qChoiceActual[0], qChoiceActual[1])
                - max(qChoiceNext[3], qChoiceNext[2]),
            )
        )
    elif dChoice == 3:  # East
        pNodes.append(
            (
                x
                + max(qChoiceActual[0], qChoiceActual[3])
                + max(qChoiceNext[1], qChoiceNext[2]),
                y - 1,
            )
        )
    elif dChoice == 4:  # West
        pNodes.append(
            (
                x
                - max(qChoiceActual[1], qChoiceActual[2])
                - max(qChoiceNext[0], qChoiceNext[3]),
                y + 1,
            )
        )
    cBeheer(qChoiceActual[0], qChoiceActual[1], qChoiceActual[2], qChoiceActual[3], -1)


p = [1, 2, 3, 4]
prevChoice = [1]


def Orientation():  # crée les choix parmi les 4 orientations possibles, en éliminant la possibilité de retourner en arrière
    chosen = choice(p)
    prevChoice.append(chosen)
    global p
    if (
        prevChoice[-1] == 1
    ):  # si le choix précédent est Nord on ne peut pas retourner au Sud!
        p = [3, 4, 1]
        # print ('North')
    elif prevChoice[-1] == 2:
        p = [3, 4, 2]
        # print ('South')
    elif prevChoice[-1] == 3:
        p = [1, 2, 3]
        # print ('East')
    elif prevChoice[-1] == 4:
        p = [1, 2, 4]
        # print ('West')


def cBeheerNetwork(
    network, maxRange
):  # génère un réseau de n Centraal Beheer en suivant un chemin orthogonal aléatoire ;
    inf, sup = (
        maxRange  # maxrange est la plage de variation de la taille des quadrants par unité
    )
    global inf
    global sup
    maybe = ["Dilatation", False, False]
    start = 0
    while start < network:
        start = start + 1
        surprise = choice(maybe)
        if surprise == "Dilatation":
            print("WOOOW")
            fat = randint(2, 2)
            inf = inf + fat
            sup = sup + fat
            Orientation()
            GrowthPattern(prevChoice[-1])
            inf, sup = maxRange
            del surprise
        Orientation()
        GrowthPattern(prevChoice[-1])
    start = 0
    global prevChoice


cBeheerNetwork(6, (4, 4))
