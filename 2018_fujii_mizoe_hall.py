# SPDX-FileCopyrightText: 2018 Arnaud Naome
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# -------------------------------------------
# Script made by Arnaud Naômé in Blender 2.79
# -------------------------------------------
# 29.11.2018
# -------------------------------------------

###0.0 - Setting Up :

# 0.1 - Import :

import bpy
import random
import mathutils

from bpy import data as DD

# 0.2 - Deleting existing objects :

for item in DD.objects:
    DD.objects.remove(item)
for item in DD.meshes:
    DD.meshes.remove(item)

###1.0 - Manipulators :

# 1.1 - Fonctions Aléatoires :

LgtRdm1 = random.randint(-10, 5) * 0.2
LgtRdm2 = (
    random.randint(-10, 15) * 0.2
)  # Problème correction hauteur et tourne dans un seul sens
HvyRdm = random.randint(0, 50)

# 1.2 - Dimensions Box :

epmur = 0.1

# 1.3 - Values GeneratorTrame :

epmurTrm = epmur - 0.08

# 1.4 - (Option) Choix d'univers architectural ( 10*10*10 Box or 2*6*2 Box) :

r = random.choice(["Fujii", "10^3-Box"])  # Bloque sur 10^3-Box
r = "Fujii"

WidthGB = 2 if r == "Fujii" else 10
LenghtGB = 6 if r == "Fujii" else 10
HeightGB = 2 if r == "Fujii" else 10

# 1.5 - Values BoxPrimordial :

WidthBP = WidthGB - epmur
HeightBP = HeightGB - epmur
LengthBP = LenghtGB - epmur

DftBP = (WidthBP, LengthBP, HeightBP)

# 1.6 - Values Cln =

WidhtClng = WidthBP + epmur / 2
LenghtClng = LengthBP
HeightClng = epmur * 3

DftClng = (WidhtClng, LenghtClng, HeightClng)

# 1.7 -Values ClngCorr :

WidhtClngCorr = WidthBP
LenghtClngCorr = LengthBP
HeightClngCorr = epmur * 4

DftClngCorr = (WidhtClngCorr, LenghtClngCorr, HeightClngCorr)

# 1.8 - Values Positions of the Different Boxes :

posBoxPri = (0, 0, 0 + HeightBP / 2 + epmur)

posClng = (0, 0, 0 + HeightBP + 1.5 * epmur + 0.09)

posO = (0, 0, 0)

# 1.9 - Values for the Way :

Way1 = 0.785398 / 2.5
Way2 = 0.785398 / 2.5
Way3 = 0.785398 / 2
Way4 = 0.785398 / 2

LocWay1 = (1.8, 1.8, 0)
LocWay2 = (-1.8, 1.8, 0)
LocWay3 = (1.8, -1.8, 0)
LocWay4 = (-1.8, -1.8, 0)

WaysChoice = random.choice([Way1, Way2, Way3, Way4])
LocChoice = random.choice([LocWay1, LocWay2, LocWay3, LocWay4])

# 1.10 - Values for the Corners:

Corner1 = (0, 6, HeightGB / 2)
Corner2 = (2, 6, HeightGB / 2)
Corner3 = (0, -0.5, HeightGB / 2)
Corner4 = (2, -0.5, HeightGB / 2)

Corners = random.choice([Corner1, Corner2, Corner3, Corner4])

###2.0 - List of Function :

# 2.1 - List of Simple Actions :


def Unsee(name):
    # Hide the object
    bpy.data.objects[name].hide = True


def See(name):
    # Unhide the object
    bpy.data.objects[name].hide = False


def BlnDifMsh(name1, name2):
    # Boolean Difference between 2 objects
    bpy.context.scene.objects.active = bpy.data.objects[name1]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].solver = "BMESH"
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[name2]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects[name2]
    bpy.ops.object.delete(use_global=False)  # Delete Videmain
    bpy.ops.object.select_all(action="TOGGLE")


def BoolInt(name1="Aggregate", name2="BoolBox"):
    # Boolean Intersection between 2 objects
    bpy.context.scene.objects.active = bpy.data.objects[name1]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].solver = "BMESH"
    bpy.context.object.modifiers["Boolean"].operation = "INTERSECT"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[name2]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects[name2]
    bpy.ops.object.delete(use_global=False)  # Delete Videmain
    bpy.ops.object.select_all(action="TOGGLE")


def boiteTrm(position, dimension, nom):
    # Box for the Function "trame" used later in the code
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.name = nom


def RotaZaxi(α=2.5):
    # Rotation of an object on the Z axis
    bpy.context.object.rotation_euler[2] = α * 0.1

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)


def RotaYaxi(α=2.5):
    # Rotation of an object on the Y axis
    bpy.context.object.rotation_euler[1] = α * 1

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)


def RotaXaxi(α=2.5):
    # Rotation of an object on the X axis
    bpy.context.object.rotation_euler[0] = α * 1

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)


def ScaleZ(scale=0.8):
    # Scale on the Z axis
    bpy.context.object.scale[2] = 1 * scale

    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)


def MoveDiag(distance):
    # Moving the object on a diagonale in X and Y
    bpy.context.object.location[0] = distance * 1
    bpy.context.object.location[1] = distance * 1

    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


def MoveX(distance):
    # Moving the object  in X
    bpy.context.object.location[0] = distance * 1
    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


def MoveY(distance):
    # Moving the object  in Y
    bpy.context.object.location[1] = distance * 1
    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


def MoveZ(distance):
    # Moving the object  in Z
    bpy.context.object.location[2] = distance * 1
    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


# 2.2 - Solid Objects :


def SolPan(name="CC"):
    # Create pannels for the "L-shaped" objects
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4, 0 + epmur)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan1"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4 + WidthGB / 2, 0 + epmur)
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan2"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4 + WidthGB / 2, 0 + WidthGB / 4, 0 + epmur)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan3"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 1 * WidthGB / 2,
            0 + epmur,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan4"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 2 * WidthGB / 2, 0 + epmur),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan5"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 2 * WidthGB / 2,
            0 + epmur,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan6"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 3 * WidthGB / 2, 0 + epmur),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan7"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 3 * WidthGB / 2,
            0 + epmur,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan8"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 4 * WidthGB / 2, 0 + epmur),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan9"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 4 * WidthGB / 2,
            0 + epmur,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan10"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 5 * WidthGB / 2, 0 + epmur),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan11"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 5 * WidthGB / 2,
            0 + epmur,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan12"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + LenghtGB / 2, 0 + epmur / 2)
    )
    bpy.ops.transform.resize(value=(WidthGB, 6, epmur / 4))
    bpy.context.object.name = "CCSol"

    bpy.context.scene.objects.active = bpy.data.objects["CCSol"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("CC")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name


def SolPanH(name="CC"):
    # Create pannels for the "L-shaped" objects on the horizontal sides
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4, 0 + epmur / 4)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan1"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan2"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4 + WidthGB / 2, 0 + WidthGB / 4, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan3"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 1 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan4"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 2 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan5"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 2 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan6"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 3 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan7"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 3 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan8"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 4 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan9"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 4 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan10"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 5 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "CCPan11"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 5 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "CCPan12"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + LenghtGB / 2, 0 + epmur / 8)
    )
    bpy.ops.transform.resize(value=(WidthGB, 6, epmur / 4))
    bpy.context.object.name = "CCSol"

    bpy.context.scene.objects.active = bpy.data.objects["CCSol"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("CC")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name


def SolPanH1(name="H", NbRot=1):
    # Create pannels for the "L-shaped" objects on the horizontal sides
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4, 0 + epmur / 4)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan1"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan2"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4 + WidthGB / 2, 0 + WidthGB / 4, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan3"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 1 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan4"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 2 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan5"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 2 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan6"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 3 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan7"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 3 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan8"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 4 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan9"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 4 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan10"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + 5 * WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan11"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 5 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan12"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + LenghtGB / 2, 0 + epmur / 8)
    )
    bpy.ops.transform.resize(value=(WidthGB, 6, epmur / 4))
    bpy.context.object.name = name + "Sol"

    bpy.context.scene.objects.active = bpy.data.objects[(name + "Sol")]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith(name)
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name

    bpy.context.object.rotation_euler[1] = NbRot * (-1.5708)


def SolPanV1(name="DD"):
    # Create pannels for the "L-shaped" objects on the sides sides
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4, 0 + epmur / 2 / 4)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 4)
    )
    bpy.context.object.name = "DDPan1"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "DDPan2"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4 + WidthGB / 2, 0 + WidthGB / 4, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = "DDPan3"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 1 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = "DDPan4"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + WidthGB / 2, 0 + epmur / 8)
    )
    bpy.ops.transform.resize(value=(WidthGB, WidthGB, epmur / 4))
    bpy.context.object.name = "DDSol"

    bpy.context.scene.objects.active = bpy.data.objects["DDSol"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("DD")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, WidthGB / 2, epmur / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name

    bpy.context.object.location[1] = -WidthGB / 2 + epmur

    bpy.context.object.rotation_euler[0] = -1.5708

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    bpy.context.object.location[1] = epmur / 2
    bpy.context.object.location[2] = WidthGB / 2


def SolPanV2(name="DD", NmbRot=1):
    # Create pannels for the "L-shaped" objects on the vertical sides
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 4, 0 + WidthGB / 4, 0 + epmur / 4)
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan1"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4, 0 + WidthGB / 4 + WidthGB / 2, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan2"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(0 + WidthGB / 4 + WidthGB / 2, 0 + WidthGB / 4, 0 + epmur / 4),
    )
    bpy.ops.transform.resize(
        value=(WidthGB / 2 - epmur, WidthGB / 2 - epmur, epmur / 2)
    )
    bpy.context.object.name = name + "Pan3"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5,
        location=(
            0 + WidthGB / 4 + WidthGB / 2,
            0 + WidthGB / 4 + 1 * WidthGB / 2,
            0 + epmur / 4,
        ),
    )
    bpy.ops.transform.resize(value=(0.8, 0.8, epmur / 2))
    bpy.context.object.name = name + "Pan4"

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + WidthGB / 2, 0 + epmur / 8)
    )
    bpy.ops.transform.resize(value=(WidthGB, WidthGB, epmur / 4))
    bpy.context.object.name = name + "Sol"

    bpy.context.scene.objects.active = bpy.data.objects[name + "Sol"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith(name)
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, WidthGB / 2, epmur / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name

    bpy.context.scene.objects.active = bpy.data.objects[name]

    bpy.context.object.location[1] = -WidthGB / 2 + epmur

    bpy.context.object.rotation_euler[0] = NmbRot * (-1.5708)

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    bpy.context.object.location[1] = epmur / 2
    bpy.context.object.location[2] = WidthGB / 2

    bpy.ops.object.transform_apply(location=True, rotation=True, scale=False)


def SplitedBox(name1="PartA", name2="PartB"):
    # Create an ensemble of objects with the previous function, it creates 2 main ensemble with 2 different names for later distinction
    SolPanH1("1Socle", 0)
    SolPanV2("1SmallWall", 1)
    SolPanH1("1LongWall", 3)
    bpy.context.object.location[0] = WidthGB / 2

    bpy.context.scene.objects.active = bpy.data.objects["1Socle"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("1")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name1

    SolPanH1("2Socle", 0)
    SolPanV2("2SmallWall", 3)
    bpy.context.object.location[1] = LenghtGB - epmur
    SolPanH1("2LongWall")

    bpy.context.scene.objects.active = bpy.data.objects["2Socle"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("2")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name2


def SplitedBoxToit(name1="PartA", name2="PartB", nameToit=""):
    # Same as previous but with a "ceilling". Create an ensemble of objects with the previous function, it creates 2 main ensemble with 2 different names for later distinction but with a Ceilling
    SolPanH1("1Socle", 0)
    SolPanV2("1SmallWall", 1)
    SolPanH1("1LongWall", 3)
    bpy.context.object.location[0] = WidthGB / 2

    bpy.context.scene.objects.active = bpy.data.objects["1Socle"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("1")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name1

    SolPanH1("2Socle", 0)
    SolPanV2("2SmallWall", 3)
    bpy.context.object.location[1] = LenghtGB - epmur
    SolPanH1("2LongWall")

    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(0 + WidthGB / 2, 0 + LenghtGB / 2, HeightGB)
    )
    bpy.ops.transform.resize(value=(WidthGB, LenghtGB, epmur / 4))
    bpy.context.object.name = nameToit + "Toit"

    bpy.context.scene.objects.active = bpy.data.objects["2Socle"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("2")
    ]

    bpy.ops.object.join()

    bpy.context.scene.cursor_location = (WidthGB / 2, LenghtGB / 2, HeightGB / 2)
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = name2


# 2.3 - Trame :


def BoxTrm(name="BoxTrm", x=1, y=1, z=1):

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, 0))
    bpy.ops.transform.resize(value=(x, y, z))
    bpy.context.object.name = name


def Trame(name="Tr", Loco=0, LocY=0, LocZ=0):

    for x in range(0, WidthGB + 1):

        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, 0))

        bpy.ops.transform.resize(value=((WidthGB, epmur, epmur)))

        bpy.ops.transform.translate(
            value=(
                WidthGB / 2,
                epmur / 2,
                ((HeightGB / 2 * x) - epmur / 2 * x) + epmur / 2,
            )
        )

        bpy.context.object.name = "BoxT"

        bpy.context.scene.objects.active = bpy.data.objects["BoxT"]

        [
            setattr(obj, "select", True)
            for obj in bpy.data.objects
            if obj.name.startswith("BoxT")
        ]

        bpy.ops.object.join()

        bpy.context.scene.cursor_location = (0, 0, 0)

        bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = "TRM"

    bpy.ops.transform.translate(value=(Loco, LocY, LocZ))

    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


def TrmGB():

    Trame(0, 0, 0)

    Trame(0, 0, 1)

    Trame(0, 0, 2)

    Trame(0, 0, 3)

    Trame(0, 0, 4)

    Trame(0, 0, 5)

    Trame(0, 0, 6)

    bpy.context.scene.objects.active = bpy.data.objects["TRM"]

    [
        setattr(obj, "select", True)
        for obj in bpy.data.objects
        if obj.name.startswith("TRM")
    ]

    bpy.ops.object.join()

    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)

    bpy.context.object.scale[1] = 0.9677419354838709

    bpy.context.scene.cursor_location = (WidthGB / 2, 3, HeightGB / 2)

    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)


def Trame2(name="Tr", Loco=0, LocY=0, LocZ=0):

    for x in range(0, 6):

        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, 0))

        bpy.ops.transform.resize(value=((WidthGB, epmur, epmur)))

        bpy.ops.transform.translate(
            value=(
                WidthGB / 2,
                epmur / 2,
                ((HeightGB / 2 * x) - epmur / 2 * x) + epmur / 2,
            )
        )

        bpy.context.object.name = "BoxT"

        bpy.context.scene.objects.active = bpy.data.objects["BoxT"]

        [
            setattr(obj, "select", True)
            for obj in bpy.data.objects
            if obj.name.startswith("BoxT")
        ]

        bpy.ops.object.join()

        bpy.context.scene.cursor_location = (0, 0, 0)

        bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.context.object.name = "TRM2"

    bpy.ops.transform.translate(value=(Loco, LocY, LocZ))

    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


def LastTrame():

    TrmGB()

    bpy.context.object.name = "Trame12"

    TrmGB()

    bpy.context.object.name = "Trame13"

    bpy.context.object.rotation_euler[1] = 1.5708

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    bpy.ops.transform.translate(value=(0, 0, HeightGB))

    Trame(0, 0, 0)

    bpy.context.object.rotation_euler[2] = 1.5708

    bpy.ops.transform.translate(value=(epmur, 0, 0))

    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

    bpy.context.object.scale[1] = 3

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    Trame(0, 0, 0)

    bpy.context.object.rotation_euler[2] = 1.5708

    bpy.ops.transform.translate(value=(epmur / 2 + 1, 0, 0))

    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

    bpy.context.object.scale[1] = 3

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    Trame(0, 0, 0)

    bpy.context.object.rotation_euler[2] = 1.5708

    bpy.ops.transform.translate(value=(2, 0, 0))

    bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

    bpy.context.object.scale[1] = 3

    bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

    bpy.ops.object.select_all(action="SELECT")

    bpy.context.scene.objects.active = bpy.data.objects["TRM"]

    bpy.ops.object.join()


# 2.4 - Finishing Operations :


def BoolIntBox():
    # Create the box for the Boolean Intersection
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(Corners))

    bpy.ops.transform.resize(value=(2.5, 2.5, 2.5))

    bpy.context.object.name = "BoolBox"


def Socle():
    # Create the box for the soccle
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(0, 0, -2.5))

    bpy.ops.transform.resize(value=(5, 5, 5))
    bpy.context.object.name = "Socle"


def Way():
    # Create the volume of the way. It will be used later to carve in the socle the way.
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(LocChoice))

    bpy.ops.transform.resize(value=(0.25, 20, 0.25))

    bpy.context.object.rotation_euler[2] = WaysChoice

    bpy.context.object.name = "Way"


###3.0 - Main :

LastTrame()

bpy.ops.transform.translate(value=(-0.5 * LgtRdm1, -0.5 * LgtRdm1, 0))
Trame()

bpy.context.object.rotation_euler[1] = 1.5708

bpy.ops.transform.translate(value=(0, LenghtGB, HeightGB))


Trame2()

bpy.context.object.name = "ZTrame101"

bpy.context.object.rotation_euler[0] = -1.5708
bpy.context.object.rotation_euler[1] = 1.5708

bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

bpy.ops.transform.translate(value=(0, 0, HeightGB))

bpy.ops.transform.translate(value=(0 + epmur, 0, 0))


Trame()

bpy.context.object.name = "ZTrame102"

bpy.context.object.rotation_euler[2] = 1.5708

bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

bpy.ops.transform.translate(value=(epmur, 0, 0))


bpy.context.object.scale[1] = 0.3

Trame()

bpy.ops.object.transform_apply(location=False, rotation=True, scale=False)

bpy.ops.transform.translate(value=(0, LenghtGB, 0))

bpy.context.object.name = "ZTrame103"


bpy.context.scene.objects.active = bpy.data.objects["TRM.001"]

[setattr(obj, "select", True) for obj in bpy.data.objects if obj.name.startswith("ZT")]

bpy.ops.object.join()

bpy.context.scene.cursor_location = (0, 0, 0)

bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

bpy.context.object.name = "ZTRM2"

bpy.ops.transform.translate(value=(0, 0, 0))

bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


bpy.context.scene.objects.active = bpy.data.objects["ZTrame102"]

[setattr(obj, "select", True) for obj in bpy.data.objects if obj.name.startswith("ZT")]

bpy.ops.object.join()

bpy.context.scene.cursor_location = (0, 0, 0)

bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

bpy.context.object.name = "TRM-2"

bpy.ops.transform.translate(value=(0, 0, 0))

bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)


RotaXaxi(0.09 * LgtRdm2)


SplitedBox("BoxA-1", "BoxB-1")

object = bpy.data.objects["BoxB-1"]
object.select = False

object = bpy.data.objects["BoxA-1"]
object.select = True

bpy.ops.transform.translate(value=(-1 * LgtRdm1, -1 * LgtRdm1, 0))

bpy.context.scene.objects.active = bpy.data.objects["BoxA-1"]

bpy.context.object.rotation_euler = (0, 0, 0.10 * LgtRdm1)


SplitedBox("BoxA-2", "BoxB-2")

object = bpy.data.objects["BoxA-2"]
object.select = True

bpy.ops.transform.translate(value=(-0.25 * LgtRdm2, -0.25 * LgtRdm2, 0))

bpy.context.object.rotation_euler = (0, 0, 0.05 * LgtRdm1)

object = bpy.data.objects["BoxB-2"]
object.select = True

bpy.ops.transform.translate(value=(-0.25 * LgtRdm2, -0.25 * LgtRdm2, 0))

bpy.context.object.rotation_euler = (0, 0, 0.10 * LgtRdm1)


SplitedBoxToit("BoxA-3", "BoxB-3")

bpy.context.scene.objects.active = bpy.data.objects["BoxA-3"]

RotaXaxi(-0.15 * LgtRdm1)

bpy.context.scene.objects.active = bpy.data.objects["BoxB-3"]

RotaXaxi(-0.15 * LgtRdm1)
RotaYaxi(-0.15 * LgtRdm1)
bpy.ops.transform.translate(value=(-0.15 * LgtRdm1, -0.15 * LgtRdm1, 0))


SplitedBoxToit("BoxA-4", "BoxB-4")

bpy.context.scene.objects.active = bpy.data.objects["BoxA-4"]

RotaXaxi(-0.15 * LgtRdm1)

bpy.context.scene.objects.active = bpy.data.objects["BoxB-4"]

RotaXaxi(-0.25 * LgtRdm1)
RotaYaxi(0.25 * LgtRdm1)
bpy.ops.transform.translate(value=(0.2 * LgtRdm1, 0.3 * LgtRdm1, 0))


# 3.5 - Finishing Operations :


# 3.5.0 :

for boite in bpy.data.objects[1:]:
    bpy.context.scene.objects.active = boite
    boolean = boite.modifiers.new("MyBoolean", "BOOLEAN")
    boolean.object = bpy.data.objects[0]
    boolean.operation = "INTERSECT"


# 3.5.1 - Aggregate :

bpy.ops.object.select_all(action="SELECT")

bpy.ops.object.join()

bpy.context.scene.cursor_location = (0, 0, 0)

bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

bpy.context.object.name = "Aggregate"


# 3.5.2 - Last Boolean to Intersect the solids in the 10*10*10 Box :

BoolIntBox()

BoolInt()

bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_MASS")

bpy.context.scene.cursor_location = (0, 0, 0)

bpy.context.scene.objects.active = bpy.data.objects["Aggregate"]
bpy.context.object.location[0] = 0
bpy.context.object.location[1] = 0

# 3.5.3 - Socle and Way :

Socle()

Way()

BlnDifMsh("Socle", "Way")

# 5.1 - Camera Settings :
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(-6, 9, 9.5),
    rotation=(0.832997, -1.29286e-007, -2.58726),
)

bpy.context.object.data.type = "ORTHO"
bpy.context.object.data.ortho_scale = 17.5

# bpy.ops.view3d.viewnumpad(type='CAMERA')
bpy.context.scene.objects.active = bpy.data.objects["Camera"]


# Definition de la fonction de création de camera en projection parallele
# It set up the camera
def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 30
    if projection == "axonometrique":
        if canon == "isometrie":  # OK
            monAxoCam.name = "axoIsometrie"
            monAxoCam.rotation_euler = (radians(54.74), 0.0, radians(45))
            monAxoCam.location = (10.0, -10.0, 10.0)
            maScene.pixel_aspect_x = 1
        if canon == "dimetrie":  # OK
            monAxoCam.name = "axoDimetrie"
            monAxoCam.rotation_euler = (radians(60), 0.0, radians(23))
            monAxoCam.location = (5.53, -13.04, 8.18)
            maScene.pixel_aspect_x = 1
        """if canon == 'trimetrie': #OK
            monAxoCam.name = 'axoTrimetrie'
            monAxoCam.rotation_euler = (radians(67), 0.0, radians(34))   
            monAxoCam.location = (8.59, -12.734, 6.52)
            maScene.pixel_aspect_x = 1"""
        if canon == "trimetrie":
            monAxoCam.name = "axoTrimetrie"
            monAxoCam.rotation_euler = (radians(67), 0.0, radians(34))
            monAxoCam.location = (17, -20, 5)
            maScene.pixel_aspect_x = 1


# 6.1 - Printing of the different informations
print(str(" "))
print(str(" "))
print(str("................"))
print(str(" "))
print(str(" "))
print(str("+ Generation Data +"))
print(str("-------------------"))
print(str(" "))

print("---GLOBAL---")

print(str(" "))
print(str("Global-Length :"))
print(str(LenghtGB))

print(str(" "))
print(str("Global-Width :"))
print(str(WidthGB))

print(str(" "))
print(str("Global-Height : "))
print(str(HeightGB))

print(str(" "))
print(str("WallThickness: "))
print(str(epmur))

print(str(" "))
print(str("Position of the Primordial Box :"))

print(posBoxPri)

print(str(" "))
print(str("---RANDOM FACTORS---"))

print(str(" "))
print(str("Light Random 1 :"))
print(str(LgtRdm1))

print(str(" "))
print(str("Light Random 2 :"))
print(str(LgtRdm2))


print(str(" "))
print(str("Heavy Random :"))
print(str(HvyRdm))

print(str(" "))
print("---SOCLE AND WAY---")
print(str(" "))
print("Choice of the Corner where the cutting box is put :")
print(Corners)
print(str(" "))
print("Choice of the Location of the Box that make the way:")
print(LocChoice)
print(str(" "))
print("Choice of the Rotation of the Box that make the way:")
print(WaysChoice)

print(str(" "))
print(str("................"))
