# SPDX-FileCopyrightText: 2021 Carl Haddad & Stéphanie Milan
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Wolfgang Amadeus Mozart, & al.:Musikalisches Würfelspiel,Würfel-Menuet (End of XVIIIth c.).
# Carl Haddad et Stephanie Milan
# AIM MODULE I - 2021-2022

import bpy
from random import randint
import random

# KEEP THE ORIGINAL ELEMENTS. / DELETE THE DUPLICATES WHEN MOVING FROM ORIGIN INTO THE MODEL.

bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["E_C"].select_set(False)
bpy.data.objects["F3_B_1"].select_set(False)
bpy.data.objects["F3_B_2"].select_set(False)
bpy.data.objects["F3_B_3"].select_set(False)
bpy.data.objects["F3_B_4"].select_set(False)
bpy.data.objects["F3_B_5"].select_set(False)
bpy.data.objects["F3_B_6"].select_set(False)
bpy.data.objects["F3_H_1"].select_set(False)
bpy.data.objects["F3_H_2"].select_set(False)
bpy.data.objects["F3_H_3"].select_set(False)
bpy.data.objects["F3_H_4"].select_set(False)
bpy.data.objects["F3_H_5"].select_set(False)
bpy.data.objects["F4_B_1"].select_set(False)
bpy.data.objects["F4_B_2"].select_set(False)
bpy.data.objects["F4_B_3"].select_set(False)
bpy.data.objects["F4_B_4"].select_set(False)
bpy.data.objects["F4_C_1"].select_set(False)
bpy.data.objects["F4_C_2"].select_set(False)
bpy.data.objects["F4_C_3"].select_set(False)
bpy.data.objects["F4_C_4"].select_set(False)
bpy.data.objects["F4_H_1"].select_set(False)
bpy.data.objects["F4_H_2"].select_set(False)
bpy.data.objects["F4_H_3"].select_set(False)
bpy.data.objects["F4_H_4"].select_set(False)
bpy.data.objects["F4_H_5"].select_set(False)
bpy.data.objects["F5_C_1"].select_set(False)
bpy.data.objects["F5_C_2"].select_set(False)
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()


# LISTING OF ELEMENTS.
# F = FAMILLE DE FORME / B = BAS / C = CENTRE / H = HAUT / CHIFFRE = NOMBRE DE FACES CONNECTABLES / EC = ESCALIER CENTRAL.

F3_B = ["F3_B_1", "F3_B_2", "F3_B_3", "F3_B_4", "F3_B_5", "F3_B_6"]
F3_H = ["F3_H_1", "F3_H_2", "F3_H_3", "F3_H_4", "F3_H_5"]
F4_B = ["F4_B_1", "F4_B_2", "F4_B_3", "F4_B_4"]
F4_C = ["F4_C_1", "F4_C_2", "F4_C_3", "F4_C_4"]
F4_H = ["F4_H_1", "F4_H_2", "F4_H_3", "F4_H_4", "F4_H_5"]
F5_C = ["F5_C_1", "F5_C_2"]


# SPACE 1
Position_1 = random.choice(F3_B)
bpy.data.objects[Position_1].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2, 10 + 3.33 / 2, 10 + 3.33 / 2))
bpy.ops.transform.rotate(value=-1.5708, orient_axis="Z")
bpy.ops.object.select_all(action="DESELECT")

# SPACE 2
Position_2 = random.choice(F4_B)
bpy.data.objects[Position_2].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2, 10 + 3.33 / 2))
bpy.ops.transform.rotate(value=-1.5708, orient_axis="Z")
bpy.ops.transform.rotate(value=3.14159, orient_axis="Z")


bpy.ops.object.select_all(action="DESELECT")

# SPACE 3
Position_3 = random.choice(F3_B)
bpy.data.objects[Position_3].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2, 10 + 3.33 / 2))
bpy.ops.object.select_all(action="DESELECT")

# SPACE 4
Position_4 = random.choice(F4_B)
bpy.data.objects[Position_4].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2))
bpy.ops.transform.rotate(value=-3.14159, orient_axis="Z")

bpy.ops.object.select_all(action="DESELECT")

# SPACE 5
Position_5 = random.choice(F4_B)
bpy.data.objects[Position_5].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 6
Position_6 = random.choice(F3_B)
bpy.data.objects[Position_6].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2))
bpy.ops.object.select_all(action="DESELECT")

# SPACE 7
Position_7 = random.choice(F4_B)
bpy.data.objects[Position_7].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2)
)
bpy.ops.transform.rotate(value=1.5708, orient_axis="Z")

bpy.ops.object.select_all(action="DESELECT")

# SPACE 8
Position_8 = random.choice(F3_B)
bpy.data.objects[Position_8].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 9
Position_9 = random.choice(F4_C)
bpy.data.objects[Position_9].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2, 10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33))
bpy.ops.object.select_all(action="DESELECT")

# SPACE 10
Position_10 = random.choice(F5_C)
bpy.data.objects[Position_10].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.transform.rotate(value=1.5708, orient_axis="Z")
bpy.ops.object.select_all(action="DESELECT")


# SPACE 11
Position_11 = random.choice(F4_C)
bpy.data.objects[Position_10].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 12
Position_12 = random.choice(F5_C)
bpy.data.objects[Position_12].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 13
Position_13 = random.choice(F5_C)
bpy.data.objects[Position_13].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 14
Position_14 = random.choice(F4_C)
bpy.data.objects[Position_14].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 15
Position_15 = random.choice(F5_C)
bpy.data.objects[Position_15].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 16
Position_16 = random.choice(F4_C)
bpy.data.objects[Position_16].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 17
Position_17 = random.choice(F3_H)
bpy.data.objects[Position_17].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(value=(10 + 3.33 / 2, 10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66))
bpy.ops.transform.rotate(value=1.5708, orient_axis="Z")
bpy.ops.object.select_all(action="DESELECT")

# SPACE 18
Position_18 = random.choice(F4_H)
bpy.data.objects[Position_18].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 19
Position_19 = random.choice(F3_H)
bpy.data.objects[Position_19].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 20
Position_20 = random.choice(F4_H)
bpy.data.objects[Position_20].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 21
Position_21 = random.choice(F4_H)
bpy.data.objects[Position_21].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 22
Position_22 = random.choice(F3_H)
bpy.data.objects[Position_22].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 23
Position_23 = random.choice(F4_H)
bpy.data.objects[Position_23].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 3.33, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# SPACE 24
Position_24 = random.choice(F3_H)
bpy.data.objects[Position_24].select_set(True)

bpy.ops.object.duplicate_move()
bpy.ops.transform.translate(
    value=(10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66, 10 + 3.33 / 2 + 6.66)
)
bpy.ops.object.select_all(action="DESELECT")

# Thank you.
