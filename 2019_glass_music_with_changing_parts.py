# SPDX-FileCopyrightText: 2019 Sacha Jonckers & Anthony Villegas del Val
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# -*- coding:Utf8 -*-

#################################
# Philip Glass                  #
# Authors: [sacha,anthony]      #
# Date: [23.01.2020]            #
# Blender version: [2.8 & hash] #
# OS: [Windows 10]              #
#################################


#################################
# importation des fct. externes :
import bpy
import random
import re


#################################
# corps principal du programme :

# Clear le modèle précédent
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False, confirm=False)

# Liste des points de spawn de la spirale

translation_matrix = [
    (
        0,
        0,
        0,
    ),
    (
        0,
        1,
        0,
    ),
    (
        1,
        1,
        1.57,
    ),
    (
        1,
        0,
        1.57,
    ),
    (
        1,
        -1,
        3.14,
    ),
    (
        0,
        -1,
        3.14,
    ),
    (
        -1,
        -1,
        3.14,
    ),
    (
        -1,
        0,
        4.71,
    ),
    (
        -1,
        1,
        4.71,
    ),
    (
        -1,
        2,
        4.71,
    ),
    (
        0,
        2,
        0,
    ),
    (
        1,
        2,
        0,
    ),
    (
        2,
        2,
        0,
    ),
    (
        2,
        1,
        1.57,
    ),
    (
        2,
        0,
        1.57,
    ),
    (
        2,
        -1,
        1.57,
    ),
    (
        2,
        -2,
        1.57,
    ),
    (
        1,
        -2,
        3.14,
    ),
    (
        0,
        -2,
        3.14,
    ),
    (
        -1,
        -2,
        3.14,
    ),
    (
        -2,
        -2,
        3.14,
    ),
    (
        -2,
        -1,
        4.71,
    ),
    (
        -2,
        0,
        4.71,
    ),
    (
        -2,
        1,
        4.71,
    ),
    (
        -2,
        2,
        4.71,
    ),
]


# Tableau des loops/mesures/paternes:
####################################

measure_structure_mg = [
    [("CG", "A"), ("CF", "A"), ("CG", "A"), ("CF", "A")],
    [("L1M1",), ("L1M2", "DF", "A"), ("EG", "A"), ("EG", "C")],
    [("L2M1",), ("L2M2",), ("L2M3",), ("L2M4",)],
    [("L2M1",), ("L2M2",), ("L2M3",), ("L2M4",)],
    [("L2M1",), ("L2M2",), ("L2M3",), ("L2M4",)],
]


measure_structure_md = [
    [("CG", "V"), ("CF", "V"), ("CG", "V"), ("CF", "V")],
    [("L1M1",), ("L1M2", "DF", "V"), ("EG", "V"), ("EG", "V")],
    [("L2M1", "A"), ("L2M2", "A"), ("L2M3", "A"), ("L2M4", "B"), ("L2M4", "C")],
    [("L3M1",), ("L2M2",), ("L3M3",), ("L3M4",)],
    [("L3M1", "L4M1"), ("L3M2", "L4M2"), ("L3M3", "L4M3"), ("L3M4", "L4M4")],
]


# Position des Argeggios dans le tableau
arpeggio_structure = [False, False, False, True, False]


# Dictionnaire des paternes
patterns_list = {
    "V": 1,
    "A": 1,
    "B": 1.5,
    "C": 2,
    "CG": 4,
    "CF": 5,
    "DF": 4.5,
    "EG": 3,
}

### AUTRES TABLEAUX POUR AUTRES CONFIGURATION ###
# à coupler aux changement de valeurs dans le spawn de cubes

# Tableau de composition CUBE_2
""" 
measure_structure_mg = [
    [('DF', 'C'), ('EG', 'C'), ('DF', 'C'), ('EG', 'C')],
    [('L1M1',), ('L1M2',  'EG', 'B'), ('A', 'B'), ('CG', 'V')],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],    
]   


measure_structure_md = [
    [('DF', 'V'), ('EG', 'V'), ('DF', 'V'), ('EG', 'V')],
    [('L1M1',), ('L1M2',  'EG', 'V'), ('A', 'V'), ('CG', 'V')],
    [('L2M1', 'B'), ('L2M2', 'B'), ('L2M3', 'A'), ('L2M4', 'C'), ('L2M4', 'DF')],
    [('L3M1',), ('L2M2',), ('L3M3',), ('L3M4',)],
    [('L3M1', 'L4M1'), ('L3M2', 'L4M2'), ('L3M3', 'L4M3'), ('L3M4', 'L4M4')],
]

 
# Position des Argeggios dans le tableau
arpeggio_structure = [False, False, False, False, False]

"""
# Tableau de composition CUBE_3
""" 
measure_structure_mg = [
    [('CF', 'DF'), ('EG', 'B'), ('CF', 'A'), ('CG', 'C')],
    [('L1M1',), ('L1M2',  'DF', 'A'), ('C', 'A'), ('CG', 'C')],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],    
]   


measure_structure_md = [
    [('CF', 'V'), ('EG', 'V'), ('CF', 'V'), ('CG', 'V')],
    [('L1M1',), ('L1M2',  'DF', 'V'), ('C', 'V'), ('CG', 'V')],
    [('L2M1', 'C'), ('L2M2', 'C'), ('L2M3', 'A'), ('L2M4', 'A'), ('L2M4', 'V')],
    [('L3M1',), ('L2M2',), ('L3M3',), ('L3M4',)],
    [('L3M1', 'L4M1'), ('L3M2', 'L4M2'), ('L3M3', 'L4M3'), ('L3M4', 'L4M4')],
]

 
# Position des Argeggios dans le tableau
arpeggio_structure = [False, False, False, True, True]

"""

# Tableau de composition CUBE_4
""" 
measure_structure_mg = [
    [('C', 'A'), ('C', 'EG'), ('V', 'A'), ('B', 'EG')],
    [('L1M1',), ('L1M2',  'A', 'CG'), ('EG', 'A'), ('DF', 'CF')],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],
    [('L2M1',),('L2M2',), ('L2M3',), ('L2M4',)],    
]   


measure_structure_md = [
    [('C', 'V'), ('C', 'V'), ('V', 'V'), ('B', 'V')],
    [('L1M1',), ('L1M2',  'A', 'CG'), ('EG', 'V'), ('DF', 'V')],
    [('L2M1', 'CF'), ('L2M2', 'A'), ('L2M3', 'A'), ('L2M4', 'EG'), ('L2M4', 'B')],
    [('L3M1',), ('L2M2',), ('L3M3',), ('L3M4',)],
    [('L3M1', 'L4M1'), ('L3M2', 'L4M2'), ('L3M3', 'L4M3'), ('L3M4', 'L4M4')],
]

 
# Position des Argeggios dans le tableau
arpeggio_structure = [False, False, True, False, False]

"""


#################################
# definition locale de fonctions :


def translate_matrix(matrix):
    final_matrix = []
    for loop_idx, loop in enumerate(matrix):
        final_matrix.append([])
        for mesure in loop:
            mesures = []
            for k in mesure:
                loop_search = re.search(r"^L\d+", k)
                mesure_search = re.search(r"M\d+$", k)
                if loop_search and mesure_search:
                    loop_search = int(loop_search.group(0)[1:])
                    mesure_search = int(mesure_search.group(0)[1:])
                    mesures += final_matrix[loop_search - 1][mesure_search - 1]
                else:
                    mesures.append(k)
            final_matrix[loop_idx].append(mesures)
    return final_matrix


# Spawn des cubes selon la matrice


def create_pattern(key, doublon, x, y, z, is_g, rot):
    pattern_number = patterns_list[key]
    for c in range(doublon):
        is_doublon = c > 0
        if is_g:
            # bpy.ops.mesh.primitive_cylinder_add(radius=1, depth=2, enter_editmode=False, location=(x, y - 0.5 + (pattern_number * 0.125), z + 0.5))
            # bpy.ops.mesh.primitive_cylinder_add(radius=0.3, vertices=20, enter_editmode=False, location=(x, y - 0.5 + (pattern_number * 0.125), z + 0.5))
            bpy.ops.mesh.primitive_cube_add(
                size=1,
                enter_editmode=False,
                location=(x, y - 0.5 + (pattern_number * 0.125), z + 0.5),
            )
            bpy.ops.transform.resize(
                value=(
                    pattern_number * 0.05,
                    1,
                    0.02 * pattern_number,
                )
            )
            bpy.ops.transform.rotate(
                value=rot, orient_axis="Y"
            )  # (1,57 radian = 90 deg)

        else:
            bpy.ops.mesh.primitive_cube_add(
                size=0.4,
                enter_editmode=False,
                location=(x, y, z + (pattern_number * 0.125)),
            )
            bpy.ops.transform.resize(
                value=(
                    1,
                    1,
                    0.18 * pattern_number,
                )
            )
            bpy.ops.transform.rotate(
                value=rot, orient_axis="Z"
            )  # (1,57 radian = 90 deg)
        if is_doublon:
            bpy.ops.mesh.primitive_cube_add(
                size=0.3,
                enter_editmode=False,
                location=(x + (pattern_number * 0.005), y, z + 0.3),
            )
            # bpy.ops.transform.resize(value=(pattern_number * 0.3, 1, 0.125 * pattern_number,))
            # bpy.ops.transform.rotate(value=rot, orient_axis='X') #(1,57 radian = 90 deg)
            # bpy.ops.transform.rotate(value=rot-0.75, orient_axis='Z') #(1,57 radian = 90 deg)
            # bpy.ops.object.modifier_add(type='WIREFRAME')
            # bpy.context.object.modifiers["Wireframe"].thickness = 0.08
        if is_g:
            bpy.ops.transform.rotate(
                value=1.57, orient_axis="X"
            )  # (1,57 radian = 90 deg)
        if arpeggio_structure[z]:
            bpy.ops.mesh.primitive_cube_add(
                size=0.55,
                enter_editmode=False,
                location=(x, y, z + (pattern_number * 0.125)),
            )
            bpy.ops.transform.resize(
                value=(1, (1 - 0.02 * pattern_number), 0.03 * pattern_number)
            )


# Spawn de la structure


def create_frame(x, y, z):
    bpy.ops.mesh.primitive_cube_add(
        size=1, enter_editmode=False, location=(x, y, z + 0.5)
    )
    bpy.ops.object.modifier_add(type="WIREFRAME")
    bpy.context.object.modifiers["Wireframe"].thickness = 0.07


# Fonction principale


def matrix_to_cube():
    matrix_g_translate = translate_matrix(measure_structure_mg)
    matrix_d_translate = translate_matrix(measure_structure_md)
    print(measure_structure_mg)
    print(measure_structure_md)
    matrix_lengh = len(matrix_g_translate)
    for i in range(matrix_lengh):
        loop_g = matrix_g_translate[i]
        loop_d = matrix_d_translate[i]
        measure_lengh = len(loop_g)
        create_frame(0, 0, i)
        position = 1
        for j in range(measure_lengh):
            doublon_g = {}
            doublon_d = {}
            for k in loop_g[j]:
                doublon_g[k] = doublon_g.get(k, 0) + 1
            for k in loop_d[j]:
                doublon_d[k] = doublon_d.get(k, 0) + 1
            for l in range(6):
                x = translation_matrix[position][0]
                y = translation_matrix[position][1]
                rot = translation_matrix[position][2]
                # create_frame(x, y, i)  ### fait apparaitre la strucutre ###
                for key in doublon_g:
                    create_pattern(key, doublon_g[key], x, y, i, True, rot)
                for key in doublon_d:
                    create_pattern(key, doublon_d[key], x, y, i, False, rot)
                position += 1


matrix_to_cube()
# create_grid()


### CHANGEMENT DE MATRICE POSSIBLE ###

"""
translation_matrix = [(0,0,0,),(0,1,0,),(1,1, 3.14,),(1,0, 3.14,),(1,-1, 4.71,),(0,-1, 3.14,),
                      (-1,-1, 3.14,),(-1,0, 1.57,),(-1,1, 4.71,),(-1,2, 4.71,),(0,2, 1.57,),
                      (1,2, 4.71,),(2,2, 3.14,),(2,1, 1.57,),(2,0, 1.57,),(2,-1, 4.71,),
                      (2,-2, 1.57,),(1,-2, 3.14,),(0,-2, 4.71,),(-1,-2, 3.14,),(-2,-2, 3.14,),
                      (-2,-1, 4.71,),(-2,0, 3.14,),(-2,1, 4.71,),(-2,2, 1.57,)]
                      
"""
