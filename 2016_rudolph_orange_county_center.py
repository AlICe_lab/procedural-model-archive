# SPDX-FileCopyrightText: 2016 Jordan Zerki
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import radians

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()


# scale pour premiere_travee

col = [0.4, 1.2, 2]
etage1 = [1.4, 2.8, 4.8]
etage2 = [2.8, 4.8, 3.2]

# scale pour travee_petit & travee_grand

col1 = [0.4, 1.2, 2]
col2 = [0.4, 1.2, 2.4]
rangea = [1.4, 2.8, 4.4]
rangeb = []


# Définition cube de base


def cube(xyz, scale, nom):
    x, y, z = xyz
    sx, sy, sz = scale
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(x, y - sy / 2, z + sz / 2))
    bpy.ops.transform.resize(value=scale)
    bpy.context.object.name = nom


def alveole(xyz, scale, nom):
    x, y, z = xyz
    sx, sy, sz = scale
    ep = 0.2
    ep1 = 0.6
    # dalle sol
    cube((x, y, z), (sx, sy, ep1), nom)
    # mur gauche
    cube((x - sx / 2 + ep / 2, y, ep1 + z), (ep, sy, sz - 2 * ep1 + 0.4), nom)
    cube((x + sx / 2 - ep / 2, y, ep1 + z), (ep, sy, sz - 2 * ep1 + 0.4), nom)
    cube((x, y, ep + z + sz - 2 * ep), (sx, sy, ep), nom)


def cubea(xyz, scale, nom):
    x, y, z = xyz
    sx, sy, sz = scale
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(x - sx, y - sy / 2, z + sz / 2)
    )
    bpy.ops.transform.resize(value=scale)
    bpy.context.object.name = nom


# definition des travee fin et debut


def premiere_travee(location, miroirY, miroirX):

    x, y = location

    # colonne
    cube((x, y + miroirX * col[1], 0), (col[0], col[1], col[2]), "colonne 1")
    # premier etage
    alveole(
        (x, y + miroirX * etage1[1], col[2]),
        (etage1[0], etage1[1], etage1[2]),
        "etage 1",
    )
    cube(
        (x, y + miroirX * etage1[1], col[2]),
        (etage1[0], etage1[1] - Y, etage1[2]),
        "etage 1",
    )
    # deuxieme etage
    alveole(
        (x + miroirY * etage1[0] / 2, y + miroirX * etage2[1], col[2] + 4.8),
        (etage2[0], etage2[1], etage2[2]),
        "etage 2",
    )
    alveole(
        (x + miroirY * etage1[0] / 2, y + miroirX * etage2[1], col[2] + 4.8),
        (etage2[0], etage2[1] - Y, etage2[2]),
        "etage 2",
    )


def travee_petit(location, size, miroirY, miroirX):

    sy, sz = size
    x, y = location
    ep = 0.2
    alveole((x, y + miroirX * col1[1], 0), (col1[0], col1[1], col1[2]), "colonne 1")
    alveole(
        (x, y + miroirX * rangea[1], col[2]),
        (rangea[0], rangea[1], rangea[2]),
        "etage1",
    )
    alveole((x, y + miroirX * sy, col[2] + 4.4), (rangea[0], sy, sz), "etage2")


def travee_grand(location, size, vide, miroirY, miroirX):

    sy, sz = size
    x, y = location

    # larg = 0
    haut = 0
    if vide == 1:
        haut = rangea[2]
        # larg = choice ([4.4,5.4])

    if miroirX == 1:
        cube((x + 1.8, y + 3, 0), (col2[0], col2[1], col2[2] + haut), "colonne2")
        cube((x - 1.8, y + 3, 0), (col2[0], col2[1], col2[2] + haut), "colonne2")
        if vide != 1:
            alveole(
                (x, y + miroirX * rangea[1] + 1.2, 2.4),
                (4, rangea[1] + 1.2, rangea[2]),
                "etage1",
            )
    else:
        cube((x + 1.8, y - 2, 0), (col2[0], col2[1], col2[2] + haut), "colonne2")
        cube((x - 1.8, y - 2, 0), (col2[0], col2[1], col2[2] + haut), "colonne2")
        if vide != 1:
            alveole((x, y, 2.4), (4, rangea[1] + 1.2, rangea[2]), "etage1")

    alveole((x, y + miroirX * sy, 2.4 + 4.4), (4, sy, sz), "etage2")


########## EXE ############


Y = randint(15, 30)

nbr_travee = randint(2, 15)

premiere_travee((0, 0), -1, 0)
# premiere_travee ((0,Y),-1,1)

PosX = 0.7

groscube = 1


Vide = randint(0, nbr_travee)

for travee in range(nbr_travee):

    if groscube % 2 == 0:
        cube((PosX, Y, 4 + sz), (randint(10, 13), randint(10, 25), randint(3, 5)), "")
        cube((PosX, Y, 4 + sz), (randint(4, 10), randint(10, 25), randint(3, 4)), "")
        cube((PosX, Y, 4 + sz), (randint(5, 10), randint(10, 20), randint(5, 6)), "")
    else:
        print("pasdegroscube")

    groscube = groscube + 1

    if Vide == travee:
        vide = 1
    else:
        vide = 0

    sy = choice([4, 4.4, 4.8, 5.2, 5.8])
    sz = choice([4, 4.4, 4.8, 5.2, 5.8])

    PosX = PosX + 2
    travee_grand((PosX, 0), (sy, sz), vide, -1, 0)
    # travee_grand((PosX,Y),(sy,sz),vide,1,1)
    cube((PosX, Y, 0), (4, Y, 2.4 + 4.4 + sz), "")
    PosX = PosX + 2

    if travee != nbr_travee - 1:
        sy = choice([3.6, 4, 4.4, 6, 6.4, 6.8, 7.2])
        sz = choice([3.6, 4, 4.4, 6, 6.4, 6.8, 7.2])

        PosX = PosX + 0.7
        travee_petit((PosX, 0), (sy, sz), -1, 0)
        # travee_petit((PosX,Y),(sy,sz),-1,1)
        cube((PosX, Y, 0), (1.4, Y, 2.4 + 4 + sz), "")
        PosX = PosX + 0.7

PosX = PosX + 0.7
premiere_travee((PosX, 0), 1, 0)
# premiere_travee ((PosX,Y),1,1)
