# SPDX-FileCopyrightText: 2018 Alice Leclercq & Marie Willame
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Alice Leclercq - Marie Willame
# AIM - Module 1
# Variation compositionnelle par modèle procédural
# Diamond Houses - John Hejduk
# Préjury - 20 décembre 2018
# macOS 10.13.4 / blender 2.79 hash 5bd8ac9
# tools

## BASE ##
import bpy
import random
import bmesh


## OUTILS GÉNÉRAUX ##
# supprimer data
def delete_all():
    for item in bpy.data.objects:
        bpy.data.objects.remove(item)
    for item in bpy.data.meshes:
        bpy.data.meshes.remove(item)


# ajouter
def add_cube(coord):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=coord)


def add_plane(coord):
    bpy.ops.mesh.primitive_plane_add(radius=0.5, location=coord)


def add_cylinder(coord):
    bpy.ops.mesh.primitive_cylinder_add(radius=0.5, depth=1, location=coord)


# transformer
def scale_object(dim):
    bpy.ops.transform.resize(value=dim, constraint_orientation="GLOBAL")


def rotate_object(angle, axis):
    bpy.ops.transform.rotate(value=angle, axis=axis)


def rename_object(name):
    bpy.context.object.name = name


def inset_face():
    bpy.ops.mesh.inset(thickness=0.05)


def translate_object(value):
    bpy.ops.transform.translate(value=value)


def subdivide_object(cuts):
    bpy.ops.mesh.subdivide(number_cuts=cuts, smoothness=0)


def join_object():
    bpy.ops.object.join()


def duplicate_mesh():
    bpy.ops.mesh.duplicate_move()


def duplicate_object():
    bpy.ops.object.duplicate_move()


def extrude_mesh(dim):
    bpy.ops.mesh.extrude_region_move(
        MESH_OT_extrude_region={"mirror": False},
        TRANSFORM_OT_translate={"value": (dim), "constraint_orientation": "NORMAL"},
    )


# autre
def select_object(objName):
    bpy.data.objects[objName].select = True


def select_all(action):
    bpy.ops.object.select_all(action=action)


def select_mesh(action):
    bpy.ops.mesh.select_all(action=action)


def change_mode(mode):
    bpy.ops.object.mode_set(mode=mode)


def switch_selectMode(selectType):
    bpy.ops.mesh.select_mode(type=selectType)


def delete(deleteType):
    bpy.ops.mesh.delete(type=deleteType)


def boolean(name2):
    translate_object((0, 0, 1))
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "INTERSECT"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[name2]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.data.objects[name2].hide = True
    translate_object((0, 0, -1))


## PROJET ##
# sol
def add_sol(coord, dim, angle, axis, name):
    add_cube(coord)
    scale_object(dim)
    rotate_object(angle, axis)
    rename_object(name)


# périmètre
def add_perimetre():
    change_mode("EDIT")
    switch_selectMode("FACE")
    bm = bmesh.from_edit_mesh(bpy.context.object.data)
    select_mesh("DESELECT")
    bm.faces.ensure_lookup_table()
    bm.faces[5].select = True
    inset_face()


# sortir centre (pour boolean plus tard)
def exclude_center():
    bm = bmesh.from_edit_mesh(bpy.context.object.data)
    duplicate_mesh()
    bpy.ops.mesh.separate(type="SELECTED")
    change_mode("OBJECT")
    bpy.context.scene.objects.active = bpy.data.objects["étage_0.001"]
    rename_object("center")
    change_mode("EDIT")
    select_mesh("SELECT")
    extrude_mesh((0, 0, 5))
    change_mode("OBJECT")


# sortir périmètre (pour boolean plus tard)
def exclude_perimetre():
    bpy.context.scene.objects.active = bpy.data.objects["étage_0"]
    change_mode("EDIT")
    bm = bmesh.from_edit_mesh(bpy.context.object.data)
    select_mesh("DESELECT")
    bm.faces.ensure_lookup_table()
    bm.faces[6].select = True
    bm.faces[7].select = True
    bm.faces[8].select = True
    bm.faces[9].select = True
    duplicate_mesh()
    bpy.ops.mesh.separate(type="SELECTED")
    change_mode("OBJECT")
    bpy.context.scene.objects.active = bpy.data.objects["étage_0.001"]
    rename_object("périmètre")
    change_mode("EDIT")
    select_mesh("SELECT")
    extrude_mesh((0, 0, 5))
    change_mode("OBJECT")


# grille de base
# subdiv = 5
subdiv = random.randint(5, 10)


def add_baseGrid():
    add_plane((0, 0, 0))
    scale_object((16, 16, 1))
    change_mode("EDIT")
    subdivide_object(subdiv)
    delete("ONLY_FACE")
    switch_selectMode("VERT")
    rename_object("grilleBase")
    change_mode("OBJECT")
    print("subdiv=", subdiv)


# cloisons+colonnes ou murs porteurs

# colonnes
# variables pour adaptation des colonnes à la subdivision
nVert = subdiv + 2
inc = 16 / (subdiv + 1)


# fonctions pour colonnes
def add_column(coord, dim, name):
    add_cylinder(coord)
    scale_object(dim)
    rename_object(name)


def select_column(name):
    listColumn = []
    for item in bpy.data.objects:
        if name in item.name:
            listColumn.append(item)
    bpy.ops.object.select_all(action="DESELECT")
    for item in listColumn:
        item.select = True


# grille de l'artiste
def add_gridPart(coord, dim, name):
    add_cube(coord)
    scale_object(dim)
    rename_object(name)


# facteurs de division / correction pour adaptation de la grille de l'artiste aux différentes subdivisions possibles
fac = (subdiv + 1) / 2
fract = 8 / fac
cor = 8 / (subdiv + 1)
e = 0.2
# cloisons+colonnes ou murs porteurs
structure = ["colonnes", "murs"]


def choice_structure():
    choice = random.choice(structure)
    print(choice)
    if choice == "colonnes":
        for x in range(1, nVert - 1):
            for y in range(1, nVert - 1):
                add_column(
                    (x * inc - 8, y * inc - 8, 2.05 / 2 + 0.1),
                    (0.1, 0.1, 2.05),
                    "colonnes",
                )
        select_column("colonnes")
        join_object()
        boolean("center")
        e = 0.1
        artGrid((1, 1, 1))
        select_object("escalierDroit")
        select_object("escalierColimaçon")
        translate_object((0.5, 0.5, 0))
        bpy.ops.object.select_all(action="DESELECT")
        select_object("colonnes")
        select_object("grilleArtiste")
        join_object()
        rename_object("choixStructure")
    else:
        e = 0.2
        artGrid((1, 1, 1))
        bpy.ops.object.select_all(action="DESELECT")
        select_object("grilleArtiste")
        rename_object("choixStructure")


# grille 1 : Charmion von Wiegand
def add_gridCW(dim):
    ##4 positions possible
    translationList = [
        (-2 * fract, 0, 0),
        (0, -1 * fract, 0),
        (-3 * fract, -1 * fract, 0),
        (5 * fract, 4 * fract, 0),
    ]
    # configuration 1
    # translationCoord = (-2*fract,0,0)
    # configuration 2
    # translationCoord = (0,-1*fract,0)
    # configuration 3
    # translationCoord = (-3*fract,-1*fract,0)
    # configuration 4
    # translationCoord = (5*fract,4*fract,0)
    # choix aléatoire de configurations
    translationCoord = random.choice(translationList)
    ##positions escaliers
    # configuration 1
    if translationCoord == (-2 * fract, 0, 0):
        add_escalierDroit((-0.5 * fract - 0.2, 0.5 * fract + 0.1, 0), 0, (0, 0, 0))
        add_escalierColimacon((fract, -fract, 0), 0, (0, 0, 0))
    # configuration 2
    if translationCoord == (0, -1 * fract, 0):
        add_escalierDroit((-1 * fract, -0.5 * fract + 0.1, 0), 0, (0, 0, 0))
        add_escalierColimacon((fract, -fract, 0), 0, (0, 0, 0))
    # configuration 3
    if translationCoord == (-3 * fract, -1 * fract, 0):
        add_escalierDroit((-0.5 * fract - 0.6, 0.5 * fract, 0), 1.5708, (0, 0, 1))
        add_escalierColimacon((0 * fract, -2 * fract, 0), 0, (0, 0, 0))
    # configuration 4
    if translationCoord == (5 * fract, 4 * fract, 0):
        add_escalierDroit((-0.5 * fract + 0.6, -1.5 * fract, 0), 1.5708, (0, 0, 1))
        add_escalierColimacon((-1 * fract, 1 * fract, 0), 0, (0, 0, 0))
    select_object("escalierDroit")
    select_object("escalierColimaçon.001")
    # correction de la position des escaliers pour les grilles impaires
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    ##lignes
    add_gridPart(
        (1.5 * fract, 3.5 * fract, 2.05 / 2 + 0.1),
        (25 * fract, e, 2.05),
        "grille_VW_l1",
    )
    change_mode("EDIT")
    add_gridPart(
        (0, 0.5 * fract, 2.05 / 2 + 0.1), (5 * fract + e, e, 2.05), "grille_VW_l2"
    )
    add_gridPart(
        (8 * fract, -0.5 * fract, 2.05 / 2 + 0.1),
        (11 * fract + e, e, 2.05),
        "grille_VW_l3",
    )
    add_gridPart(
        (1.5 * fract, -1.5 * fract, 2.05 / 2 + 0.1),
        (2 * fract + e, e, 2.05),
        "grille_VW_l4",
    )
    add_gridPart(
        (-7.25 * fract, -3.5 * fract, 2.05 / 2 + 0.1),
        (7.5 * fract, e, 2.05),
        "grille_VW_l5",
    )
    add_gridPart(
        (1.5 * fract, -10.5 * fract, 2.05 / 2 + 0.1),
        (25 * fract, e, 2.05),
        "grille_VW_l6",
    )
    add_gridPart(
        (1.5 * fract, -20.5 * fract, 2.05 / 2 + 0.1),
        (25 * fract, e, 2.05),
        "grille_VW_l7",
    )
    ##colonnes
    add_gridPart(
        (-5.5 * fract, -3.5 * fract, 2.05 / 2 + 0.1),
        (e, 14 * fract + e, 2.05),
        "grille_VW_c1",
    )
    add_gridPart(
        (-2.5 * fract, 2 * fract, 2.05 / 2 + 0.1),
        (e, 3 * fract + e, 2.05),
        "grille_VW_c2",
    )
    add_gridPart(
        (0.5 * fract, -1.5 * fract + e / 4, 2.05 / 2 + 0.1),
        (e, 4 * fract + e / 2, 2.05),
        "grille_VW_c3",
    )
    add_gridPart(
        (2.5 * fract, 2 * fract, 2.05 / 2 + 0.1), (e, 16 * fract, 2.05), "grille_VW_c4"
    )
    add_gridPart(
        (2.5 * fract, -15.5 * fract, 2.05 / 2 + 0.1),
        (e, 10 * fract + e, 2.05),
        "grille_VW",
    )
    ##scale
    change_mode("OBJECT")
    scale_object(dim)
    ##translation pour différentes positions
    translate_object(translationCoord)
    ##correction grille impaire
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    ##printCHoix
    print("Charmion von Wiegand")


# grille 2 : Sophie Taeuber-Arp
def add_gridATP(dim):
    ##4 positions possible
    translationList = [
        (-1 * fract, -3 * fract, 0),
        (5 * fract, 9 * fract, 0),
        (0, -5 * fract, 0),
        (-6 * fract, 9 * fract, 0),
    ]
    # configuration 1
    # translationCoord = (-1*fract,-3*fract,0)
    # configuration 2
    # translationCoord = (5*fract,9*fract,0)
    # configuration 3
    # translationCoord = (0,-5*fract,0)
    # configuration 4
    # translationCoord = (-6*fract,9*fract,0)
    # choix aléatoire configurations
    translationCoord = random.choice(translationList)
    ##positions escaliers
    # configuration 1
    if translationCoord == (-1 * fract, -3 * fract, 0):
        add_escalierDroit((0.5 * fract, -0.5 * fract - 1.1, 0), 0, (0, 0, 0))
        add_escalierColimacon((-2 * fract, 0 * fract, 0), 0, (0, 0, 0))
    # configuration 2
    if translationCoord == (5 * fract, 9 * fract, 0):
        add_escalierDroit((-0.5 * fract - 0.6, -0 * fract, 0), 1.5708, (0, 0, 1))
        add_escalierColimacon((0 * fract, -2 * fract, 0), 0, (0, 0, 0))
    # configuration 3
    if translationCoord == (0, -5 * fract, 0):
        add_escalierDroit((-1.5 * fract, 0.5 * fract - 1.1, 0), 0, (0, 0, 0))
        add_escalierColimacon((0 * fract, -2 * fract, 0), 0, (0, 0, 0))
    # configuration 4
    if translationCoord == (-6 * fract, 9 * fract, 0):
        add_escalierDroit((0.5 * fract + 0.6, 0 * fract - 0.5, 0), 1.5708, (0, 0, 1))
        add_escalierColimacon((-2 * fract, 0 * fract, 0), 0, (0, 0, 0))
    select_object("escalierDroit")
    select_object("escalierColimaçon.001")
    # correction pour grille impaire
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    ##lignes
    add_gridPart(
        (-5.5 * fract, 5.5 * fract, 2.05 / 2 + 0.1),
        (10 * fract + e, e, 2.05),
        "grille_ST-A_l1",
    )
    change_mode("EDIT")
    add_gridPart(
        (-0.5 * fract, 2.5 * fract, 2.05 / 2 + 0.1),
        (20 * fract, e, 2.05),
        "grille_ST-A_l2",
    )
    add_gridPart(
        (2 * fract, -0.5 * fract, 2.05 / 2 + 0.1),
        (5 * fract + e, e, 2.05),
        "grille_ST-A_l3",
    )
    add_gridPart(
        (-8 * fract, -7.5 * fract, 2.05 / 2 + 0.1),
        (5 * fract + e, e, 2.05),
        "grille_ST-A_l4",
    )
    add_gridPart(
        (-7 * fract, -10.5 * fract, 2.05 / 2 + 0.1),
        (7 * fract + e, e, 2.05),
        "grille_ST-A_l5",
    )
    add_gridPart(
        (4.5 * fract, -10.5 * fract, 2.05 / 2 + 0.1),
        (10 * fract + e, e, 2.05),
        "grille_ST-A_l6",
    )
    add_gridPart(
        (-0.5 * fract, -17.5 * fract, 2.05 / 2 + 0.1),
        (20 * fract, e, 2.05),
        "grille_ST-A_l7",
    )
    ##colonnes
    add_gridPart(
        (-5.5 * fract, -4.5 * fract, 2.05 / 2 + 0.1),
        (e, 26 * fract + e, 2.25),
        "grille_ST-A_c1",
    )
    add_gridPart(
        (-3.5 * fract, -11 * fract - e / 4, 2.05 / 2 + 0.1),
        (e, 13 * fract + e / 2, 2.25),
        "grille_ST-A_c2",
    )
    add_gridPart(
        (-0.5 * fract, -4.5 * fract, 2.05 / 2 + 0.1),
        (e, 26 * fract + e, 2.05),
        "grille_ST-A_c3",
    )
    add_gridPart(
        (1.5 * fract, 5.5 * fract, 2.05 / 2 + 0.1),
        (e, 6 * fract + e, 2.05),
        "grille_ST-A_c4",
    )
    add_gridPart(
        (4.5 * fract, -4.5 * fract, 2.05 / 2 + 0.1),
        (e, 26 * fract + e, 2.05),
        "grille_ST-A_c5",
    )
    add_gridPart(
        (6.5 * fract, -4 * fract, 2.05 / 2 + 0.1),
        (e, 13 * fract + e, 2.05),
        "grille_ST-A",
    )
    ##scale
    scale_object(dim)
    change_mode("OBJECT")
    ##translation pour différentes positions
    translate_object(translationCoord)
    ##correction pour grilles impaires
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    ##printCHoix
    print("Sophie Taeuber-Arp")


# grille 3 : Léon Polk Smith
def add_gridLPS(dim):
    ##4 positions possible
    translationList = [
        (-2 * fract, 0, 0),
        (3, -3 * fract, 0),
        (5 * fract, -5 * fract, 0),
        (-3 * fract, -4 * fract, 0),
    ]
    translationCoord = random.choice(translationList)
    # translationCoord = (-2*fract,0,0)
    # translationCoord = (3,-3*fract,0)
    # translationCoord = (5*fract,-5*fract,0)
    # translationCoord = (-3*fract,-4*fract,0)
    ##escaliers
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    if subdiv == 5:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit(
                (0 * fract + 0.6, 1 * fract - 0.2 + 0.1, 0), 1.5708, (0, 0, 1)
            )
            add_escalierColimacon(
                (0 * fract - 0.2, -1.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit((0 * fract - 0.2, -2 * fract + 0.2, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon(
                (0 * fract + 0.2, 1.5 * fract - 0.2 + 0.1, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit(
                (1 * fract + 0.5, 0.5 * fract + 0.2, 0), -1.5708, (0, 0, 1)
            )
            add_escalierColimacon(
                (0 * fract + 0.2, 1.5 * fract - 0.2 + 0.1, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit(
                (1 * fract + 0.5, 0.5 * fract + 0.2, 0), -1.5708, (0, 0, 1)
            )
            add_escalierColimacon(
                (-1 * fract, 0.5 * fract + 0.2, 0), -1.5708, (0, 0, -1)
            )
    if subdiv == 6:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit(
                (-1 * fract + 0.6, 0.5 * fract - 0.2 + 0.1, 0), 0, (0, 0, 1)
            )
            add_escalierColimacon(
                (-1 * fract - 0.2, -0.8 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit((0 * fract, 1 * fract - 0.2 + 0.1, 0), 0, (0, 0, 1))
            add_escalierColimacon(
                (2 * fract - 0.2, 0 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit(
                (-1 * fract + 0.6, -1 * fract - 0.2 + 0.1, 0), 1.5708, (0, 0, 1)
            )
            add_escalierColimacon(
                (1.5 * fract, -0.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit(
                (1 * fract + 0.6, -0.1 * fract - 0.2 + 0.1, 0), -1.5708, (0, 0, 1)
            )
            add_escalierColimacon(
                (0 * fract - 0.2, 0.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
    if subdiv == 7:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit((0 * fract + 0.6, 1 * fract - 0.2 + 0.1, 0), 0, (0, 0, 1))
            add_escalierColimacon(
                (-2 * fract - 0.2, 0 * fract - 0.6, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit(
                (-1.5 * fract + 0.6, 1 * fract - 0.2 + 0.1, 0), 0, (0, 0, 1)
            )
            add_escalierColimacon(
                (0 * fract - 0.2, -1.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit((1 * fract, -0.5 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((2 * fract + 0.2, 0 * fract + 0.2, 0), 0, (0, 0, -1))
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit((2 * fract, 1 * fract - 0.2 + 0.1, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon(
                (-1 * fract - 0.2, 0.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
    if subdiv == 8:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit((0 * fract, -1.5 * fract - 0.2 + 0.1, 0), 0, (0, 0, 1))
            add_escalierColimacon((0 * fract, 2 * fract + 0.2, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit((2 * fract, -1 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((0 * fract, -1 * fract, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit(
                (2 * fract - 0.2, -1.5 * fract + 0.2, 0), 1.5708, (0, 0, 1)
            )
            add_escalierColimacon((-0.5 * fract, 1.5 * fract, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit((1 * fract, 0 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((0 * fract - 0.4, 2 * fract + 0.6, 0), 0, (0, 0, -1))
    if subdiv == 9:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit((1 * fract, -1 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon(
                (-2 * fract - 0.2, -0.5 * fract + 0.2, 0), 1.5708, (0, 0, -1)
            )
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit((-2 * fract, -1 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((0 * fract, -2 * fract + 0.4, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit((-2 * fract, 0 * fract, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon((0 * fract, 2 * fract, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit((2 * fract, -1 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((0 * fract, 1 * fract, 0), 1.5708, (0, 0, -1))
    if subdiv == 10:
        if translationCoord == (-2 * fract, 0, 0):
            add_escalierDroit((0 * fract, -3 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((2 * fract + 0.6, 1 * fract, 0), 0, (0, 0, -1))
        elif translationCoord == (3, -3 * fract, 0):
            add_escalierDroit((0 * fract, -2 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((-3 * fract, 0 * fract, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (5 * fract, -5 * fract, 0):
            add_escalierDroit((2 * fract, 0 * fract, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon((1 * fract + 0.6, 2 * fract, 0), 1.5708, (0, 0, -1))
        elif translationCoord == (-3 * fract, -4 * fract, 0):
            add_escalierDroit((1.5 * fract, -0.5 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((-0.5 * fract, 2.5 * fract, 0), 0, (0, 0, -1))
    ##Murs
    # verticaux
    add_gridPart(
        (-8 * fract, 6.2 * fract, 2.05 / 2 + 0.1),
        (e, 23 * fract + e, 2.05),
        "grille_LPS.001",
    )
    change_mode("EDIT")
    add_gridPart(
        (0 * fract, -4.2 * fract, 2.05 / 2 + 0.1),
        (e, 19.7 * fract + e, 2.05),
        "grille_LPS.002",
    )
    add_gridPart(
        (-4 * fract, -4.4 * fract, 2.05 / 2 + 0.1),
        (e, 19.7 * fract + e, 2.05),
        "grille_LPS.003",
    )
    add_gridPart(
        (3.5 * fract, 8.9 * fract, 2.05 / 2 + 0.1),
        (e, 17.7 * fract + e, 2.05),
        "grille_LPS.004",
    )
    add_gridPart(
        (-4.5 * fract, 11.5 * fract, 2.05 / 2 + 0.1),
        (e, 11.8 * fract + e, 2.05),
        "grille_LPS.005",
    )
    # horizontaux
    add_gridPart(
        (3.4 * fract, 0 * fract, 2.05 / 2 + 0.1),
        (14.7 * fract + e, e, 2.05),
        "grille_LPS.006",
    )
    add_gridPart(
        (3.1 * fract, 14 * fract, 2.05 / 2 + 0.1),
        (15 * fract + e, e, 2.05),
        "grille_LPS.007",
    )
    add_gridPart(
        (0 * fract, 5.5 * fract, 2.05 / 2 + 0.1),
        (22 * fract + e, e, 2.05),
        "grille_LPS.008",
    )
    add_gridPart(
        (-6 * fract, -5.5 * fract, 2.05 / 2 + 0.1),
        (12 * fract + e, e, 2.05),
        "grille_LPS",
    )
    ##scale
    scale_object(dim)
    change_mode("OBJECT")
    ##translation
    translate_object(translationCoord)
    ##printCHoix
    print("Léon Polk Smith")


# grille 4 : Ilya Bolotowsky
def add_gridIB(dim):
    ##4 positions possible
    translationList = [
        (-1 * fract, -1 * fract, 0),
        (-3 * fract, 5 * fract, 0),
        (0 * fract, -1 * fract, 0),
        (-7.5 * fract, 3 * fract, 0),
    ]
    translationCoord = random.choice(translationList)
    # translationCoord = (-1*fract,-1*fract,0)
    # translationCoord = (-3*fract,5*fract,0)
    # translationCoord = (0*fract,-1*fract,0)
    # translationCoord = (-7.5*fract,3*fract,0)
    ##escacliers
    if subdiv == 5 or subdiv == 7 or subdiv == 9:
        translate_object((cor, cor, 0))
    if subdiv == 5:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((0 * fract, -1 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((-1 * fract, 0.5 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((-0.5 * fract, 1.5 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((-1.5 * fract, 0 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((1 * fract, 0 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((0 * fract, 1.5 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((0 * fract, -1 * fract - 0.6, 0), 0, (0, 0, 1))
            add_escalierColimacon((-1.5 * fract, 0 * fract, 0), 1.5708, (0, 0, -1))
    if subdiv == 6:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((-1 * fract, -1 * fract - 0.6, 0), 0, (0, 0, 1))
            add_escalierColimacon((-1.5 * fract - 0.6, 0 * fract, 0), 0, (0, 0, -1))
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((0 * fract, 1 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-1 * fract, -1 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((1 * fract, 1.5 * fract, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon((-1 * fract, 1 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((-0.5 * fract, -2 * fract + 0.4, 0), 0, (0, 0, 1))
            add_escalierColimacon((1 * fract, 1 * fract, 0), 1.5708, (0, 0, -1))
    if subdiv == 7:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((-1 * fract + 0.4, 0 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((0 * fract, -2 * fract, 0), -1.5708, (0, 0, -1))
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((-0.5 * fract, -2 * fract + 0.4, 0), 0, (0, 0, 1))
            add_escalierColimacon((-1.5 * fract, 0.5 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((1 * fract - 0.4, 0 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon(
                (-1.5 * fract - 0.6, 0.5 * fract, 0), 1.5708, (0, 0, -1)
            )
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((-0.5 * fract, -1.5 * fract + 0.4, 0), 0, (0, 0, 1))
            add_escalierColimacon((0 * fract, 2 * fract + 0.6, 0), 1.5708, (0, 0, -1))
    if subdiv == 8:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((0 * fract + 0.4, -3 * fract + 0.4, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-2 * fract, 0 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((-2 * fract + 0.4, -0.5 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-0.5 * fract, 2 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((-2 * fract, 0 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((2.5 * fract, 0 * fract, 0), -1.5708, (0, 0, -1))
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((-1 * fract, -2 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon(
                (2 * fract, -0.5 * fract - 0.6, 0), -1.5708, (0, 0, -1)
            )
    if subdiv == 9:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((-1 * fract, -1 * fract + 0.2, 0), 0, (0, 0, 1))
            add_escalierColimacon(
                (1.5 * fract + 0.4, -1.5 * fract + 0.4, 0), -1.5708, (0, 0, -1)
            )
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((0 * fract, -2 * fract, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon((-2 * fract, 0 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((-2 * fract, 0 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((1 * fract, -2 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((0 * fract, -2 * fract, 0), 0, (0, 0, 1))
            add_escalierColimacon((0 * fract, 2 * fract, 0), 1.5708, (0, 0, -1))
    if subdiv == 10:
        if translationCoord == (-1 * fract, -1 * fract, 0):
            add_escalierDroit((0 * fract, -3 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-2 * fract, 1 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (-3 * fract, 5 * fract, 0):
            add_escalierDroit((-2 * fract, 0 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-0.5 * fract, 2 * fract, 0), 1.5708, (0, 0, -1))
        if translationCoord == (0 * fract, -1 * fract, 0):
            add_escalierDroit((-2 * fract, 1 * fract, 0), -1.5708, (0, 0, 1))
            add_escalierColimacon((2.5 * fract, 0.5 * fract, 0), 0, (0, 0, -1))
        if translationCoord == (-7.5 * fract, 3 * fract, 0):
            add_escalierDroit((0 * fract, 0 * fract, 0), 1.5708, (0, 0, 1))
            add_escalierColimacon((-0.5 * fract, -2.5 * fract, 0), 1.5708, (0, 0, -1))
    ##murs
    # verticaux
    add_gridPart(
        (0 * fract, 6 * fract, 2.05 / 2 + 0.1),
        (e, 12 * fract + e, 2.05),
        "grille_IB.001",
    )
    change_mode("EDIT")
    add_gridPart(
        (-3 * fract, 2.5 * fract, 2.05 / 2 + 0.1),
        (e, 5 * fract + e, 2.05),
        "grille_IB.002",
    )
    add_gridPart(
        (-6 * fract, 2.5 * fract, 2.05 / 2 + 0.1),
        (e, 5 * fract + e, 2.05),
        "grille_IB.003",
    )
    add_gridPart(
        (3.4 * fract, -0.5 * fract, 2.05 / 2 + 0.1),
        (e, 7.8 * fract + e, 2.05),
        "grille_IB.004",
    )
    add_gridPart(
        (8 * fract, 2.5 * fract, 2.05 / 2 + 0.1),
        (e, 14 * fract + e, 2.05),
        "grille_IB.005",
    )
    add_gridPart(
        (4.5 * fract, 6 * fract, 2.05 / 2 + 0.1),
        (e, 5 * fract + e, 2.05),
        "grille_IB.006",
    )
    add_gridPart(
        (1.5 * fract, -3.5 * fract, 2.05 / 2 + 0.1),
        (e, 7 * fract + e, 2.05),
        "grille_IB.007",
    )
    add_gridPart(
        (6 * fract, -6.5 * fract, 2.05 / 2 + 0.1),
        (e, 4 * fract + e, 2.05),
        "grille_IB.008",
    )
    # horizontaux
    add_gridPart(
        (-1.7 * fract, 0 * fract, 2.05 / 2 + 0.1),
        (10 * fract + e, e, 2.05),
        "grille_IB.009",
    )
    add_gridPart(
        (4 * fract, 3.5 * fract, 2.05 / 2 + 0.1),
        (7.8 * fract + e, e, 2.05),
        "grille_IB.010",
    )
    add_gridPart(
        (4 * fract, 8.5 * fract, 2.05 / 2 + 0.1),
        (7.8 * fract + e, e, 2.05),
        "grille_IB.011",
    )
    add_gridPart(
        (1 * fract, -7 * fract, 2.05 / 2 + 0.1),
        (10 * fract + e, e, 2.05),
        "grille_IB.012",
    )
    add_gridPart(
        (7.5 * fract, -4.5 * fract, 2.05 / 2 + 0.1),
        (12 * fract + e, e, 2.05),
        "grille_IB.013",
    )
    add_gridPart(
        (10.5 * fract, -2.5 * fract, 2.05 / 2 + 0.1),
        (5 * fract + e, e, 2.05),
        "grille_IB.014",
    )
    add_gridPart(
        (11.5 * fract, 2.5 * fract, 2.05 / 2 + 0.1),
        (7 * fract + e, e, 2.05),
        "grille_IB.015",
    )
    add_gridPart(
        (11.5 * fract, 6.5 * fract, 2.05 / 2 + 0.1),
        (7 * fract + e, e, 2.05),
        "grille_IB",
    )
    ##scale
    scale_object(dim)
    change_mode("OBJECT")
    ##translation
    translate_object(translationCoord)
    ##printCHoix
    print("Ilya Bolotowsky")


# choix aléatoire entre artistes
listArtGrid = [add_gridCW, add_gridATP, add_gridLPS, add_gridIB]


def artGrid(dim):
    random.choice(listArtGrid)(dim)
    rename_object("grilleArtiste")


# escaliers
def escalier(coord, dim, name):
    add_cube(coord)
    scale_object(dim)
    rename_object(name)


def add_escalierDroit(coord, angle, axis):
    # escalier((-fract,-fract+.5,.2),(.25,1,.2),"escalierDroit")
    escalier((0, 0, 0.2), (0.5, 1, 0.2), "escalierDroit")
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 0.5
    bpy.context.object.modifiers["Array"].relative_offset_displace[2] = 1
    bpy.context.object.modifiers["Array"].count = 11
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
    bpy.data.objects["escalierDroit"].location[0] = 0
    bpy.data.objects["escalierDroit"].location[1] = 0.5
    translate_object(coord)
    rotate_object(angle, axis)


def add_escalierColimacon(coord, angle, axis):
    select_all("DESELECT")
    select_object("escalierColimaçon")
    duplicate_object()
    select_all("DESELECT")
    select_object("escalierColimaçon.001")
    bpy.data.objects["escalierColimaçon.001"].location[0] = 0
    bpy.data.objects["escalierColimaçon.001"].location[1] = 0
    bpy.data.objects["escalierColimaçon.001"].location[2] = 0.1
    bpy.data.objects["escalierColimaçon.001"].rotation_euler[0] = 0
    bpy.data.objects["escalierColimaçon.001"].rotation_euler[1] = 0
    bpy.data.objects["escalierColimaçon.001"].rotation_euler[2] = 0
    translate_object(coord)
    rotate_object(angle, axis)


# brise-soleil
def add_briseSoleil(coord, dim, name):
    add_cube(coord)
    scale_object(dim)
    rename_object(name)


def select_briseSoleil(name):
    listBS = []
    for item in bpy.data.objects:
        if name in item.name:
            listBS.append(item)
    bpy.ops.object.select_all(action="DESELECT")
    for item in listBS:
        item.select = True


##trame 2/3
div = subdiv + 1
nBS = 10
c = 8 / (div * 3)
if div == 6:
    nBS == 10
elif div == 7:
    nBS == 11
elif div == 8:
    nBS == 13
elif div == 9:
    nBS == 14
elif div == 10:
    nBS == 16
elif div == 11:
    nBS == 17
# facteur de correction pour variations
if div == 7 or div == 9 or div == 11:
    c = 8 / (div * 3)
elif div == 6 or div == 8 or div == 10:
    c = 0


def briseSoleil_tiers(dim):
    for x in range(-2 * nBS, 2 * nBS + 1, 2):
        add_briseSoleil(
            (c + x * 16 / (div * 3), 0, 2.05 / 2 + 0.1),
            (0.06, 16, 2.05),
            "brise-soleil",
        )
    scale_object(dim)


##trame irrégulière
def briseSoleil_irréguliers(dim):
    for x in range(-10, 40):
        add_briseSoleil(
            (random.randint(1, 2) * x, 0, 2.05 / 2 + 0.1),
            (0.06, 16, 2.05),
            "brise-soleil",
        )
    scale_object(dim)


# choix entre 2 trames
trame_BS = [briseSoleil_tiers, briseSoleil_irréguliers]


def add_randomBS(dim):
    random.choice(trame_BS)(dim)
    select_briseSoleil("brise-soleil")
    join_object()
    boolean("périmètre")
