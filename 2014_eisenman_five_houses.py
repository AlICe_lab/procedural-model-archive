# SPDX-FileCopyrightText: 2014 Marine Serette
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER 2.71   Hash:9337574 / Windows64 bit

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

print("   ")
print("   ")
print("--------------------")
print("   ")

###-----------------------------------------------------------------------------------------------###
###-----------------------------------------------------------------------------------------------###
###                                         CHOIX CUBE OU 1/2                                     ###
###-----------------------------------------------------------------------------------------------###
###-----------------------------------------------------------------------------------------------###

a = random.randint(1, 3)

if a == 1:
    hauteur = 2.25
    print("Volume Maison = 1/2 Cube  => Houses I - II")
else:
    hauteur = 4.5
    print("Volume Maison = Cube  => Houses III - IV - VI")

print("    ")

###-----------------------------------------------------------------------------------------------###
###-----------------------------------------------------------------------------------------------###
###                                            Coordonnées                                        ###
###-----------------------------------------------------------------------------------------------###
###-----------------------------------------------------------------------------------------------###

b = random.randint(1, 4)

if b == 1:  # a-a-a
    print("Division en plan sur le modele House II et House III")
    coord1 = 0.05
    coord2 = 2.7
    coord3 = 5.4
    coord4 = 8.1
    coord1bis = 0.9
    coord2bis = 3.6
    coord3bis = 6.3
    coord4bis = 9
    loc1 = 0.05
    loc2 = 3
    loc3 = 6
    loc4 = 9

elif b == 2:  # b-2a-b
    print("Division en plan sur le modele House I")
    coord1 = 0.05
    coord2 = 1.35
    coord3 = 6.75
    coord4 = 8.1
    coord1bis = 0.9
    coord2bis = 2.25
    coord3bis = 7.65
    coord4bis = 9
    loc1 = 0.05
    loc2 = 1.5
    loc3 = 7.5
    loc4 = 9

elif b == 3:
    print("Division en plan sur le modele House IV")
    coord1 = 0.05
    coord2 = (1.25 / 3) * 8.1
    coord3 = (1.75 / 3) * 8.1
    coord4 = 8.1
    coord1bis = 0.9
    coord2bis = 0.9 + (1.25 / 3) * 8.1
    coord3bis = 0.9 + (1.75 / 3) * 8.1
    coord4bis = 9
    loc1 = 0.05
    loc2 = (1.25 / 3) * 9
    loc3 = (1.75 / 3) * 9
    loc4 = 9

else:  # b-b-2a
    print("Division en plan sur le modele House VI")
    coord1 = 0.05
    coord2 = 1.35
    coord3 = 2.7
    coord4 = 8.1
    coord1bis = 0.9
    coord2bis = 2.25
    coord3bis = 4.5
    coord4bis = 9
    loc1 = 0.05
    loc2 = 1.5
    loc3 = 3
    loc4 = 9


###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###
###                                       HOUSE III                                       ###
###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###


def HouseIII():

    def HouseIIIRotation():

        def Sol(pos_X, pos_Y, pos_Z):
            bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
            bpy.ops.transform.resize(value=(4.5, 4.5, 0.05))

        Sol(4.5, 4.5, -0.05)

        def MurCote(pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
            bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
            bpy.ops.transform.resize(value=(0.05, dim_Y, dim_Z))

        MurCote(
            coord1,
            (coord4 - coord1) / 2,
            (coord4 - coord1) / 2,
            0.3,
            (hauteur * 2 - 0.30),
        )
        MurCote(
            coord1, (coord4 - coord1) / 2, (coord4 - coord1) / 2, 0.3, (hauteur - 0.3)
        )
        MurCote(
            coord4bis - 0.05,
            (coord4bis - coord1bis) / 2,
            coord1bis + (coord4bis - coord1bis) / 2,
            0.3,
            (hauteur * 2 - 0.30),
        )
        MurCote(
            coord4bis - 0.05,
            (coord4bis - coord1bis) / 2,
            coord1bis + (coord4bis - coord1bis) / 2,
            0.3,
            (hauteur - 0.3),
        )

        def MurAvantHouseIII(dim_X, pos_Y, dim_Z, pos_Z):
            pos_X = dim_X
            bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
            bpy.ops.transform.resize(value=(dim_X, 0.05, dim_Z))

        def MurArriereHouseIII(dim_X, pos_X, pos_Y, dim_Z, pos_Z):
            bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
            bpy.ops.transform.resize(value=(dim_X, 0.05, dim_Z))

        def ColonnesHouseIII(pos_X, pos_Y, pos_Z):
            dim_Z = hauteur
            bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
            bpy.ops.transform.resize(value=(0.05, 0.05, hauteur))

        MurAvantHouseIII((coord4 - coord1) / 2, 0.05, 0.3, (hauteur * 2 - 0.30))
        MurAvantHouseIII((coord4 - coord1) / 2, 0.05, 0.3, (hauteur - 0.3))
        ColonnesHouseIII(coord1, coord1, hauteur)
        ColonnesHouseIII(coord2, coord1, hauteur)
        ColonnesHouseIII(coord3, coord1, hauteur)
        ColonnesHouseIII(coord4, coord1, hauteur)

        MurAvantHouseIII((coord3 - coord1) / 2, coord2, 0.3, (hauteur * 2 - 0.30))
        MurAvantHouseIII((coord3 - coord1) / 2, coord2, 0.3, (hauteur - 0.30))
        ColonnesHouseIII(coord1, coord2, hauteur)
        ColonnesHouseIII(coord2, coord2, hauteur)
        ColonnesHouseIII(coord3, coord2, hauteur)

        MurAvantHouseIII((coord2 - coord1) / 2, coord3, 0.3, (hauteur * 2 - 0.30))
        MurAvantHouseIII((coord2 - coord1) / 2, coord3, 0.3, (hauteur - 0.30))
        ColonnesHouseIII(0.05, coord3, hauteur)
        ColonnesHouseIII(coord2, coord3, hauteur)

        ColonnesHouseIII(coord1, coord4, hauteur)

        ColonnesHouseIII(coord4bis - 0.05, coord1, hauteur)

        MurArriereHouseIII(
            (coord4bis - coord3bis) / 2,
            coord3bis + (coord4bis - coord3bis) / 2,
            coord2bis,
            0.3,
            (hauteur * 2 - 0.30),
        )
        MurArriereHouseIII(
            (coord4bis - coord3bis) / 2,
            coord3bis + (coord4bis - coord3bis) / 2,
            coord2bis,
            0.3,
            (hauteur - 0.30),
        )
        ColonnesHouseIII(coord3bis, coord2bis, hauteur)
        ColonnesHouseIII(coord4bis - 0.05, coord2bis, hauteur)

        MurArriereHouseIII(
            (coord4bis - coord2bis) / 2,
            coord2bis + (coord4bis - coord2bis) / 2,
            coord3bis,
            0.3,
            (hauteur * 2 - 0.30),
        )
        MurArriereHouseIII(
            (coord4bis - coord2bis) / 2,
            coord2bis + (coord4bis - coord2bis) / 2,
            coord3bis,
            0.3,
            (hauteur - 0.30),
        )
        ColonnesHouseIII(coord2bis, coord3bis, hauteur)
        ColonnesHouseIII(coord3bis, coord3bis, hauteur)
        ColonnesHouseIII(coord4bis - 0.05, coord3bis, hauteur)

        MurArriereHouseIII(
            (coord4bis - coord1bis) / 2,
            coord1bis + (coord4bis - coord1bis) / 2,
            coord4bis - 0.05,
            0.3,
            (hauteur * 2 - 0.30),
        )
        MurArriereHouseIII(
            (coord4bis - coord1bis) / 2,
            coord1bis + (coord4bis - coord1bis) / 2,
            coord4bis - 0.05,
            0.3,
            (hauteur - 0.3),
        )
        ColonnesHouseIII(coord1bis, coord4bis - 0.05, hauteur)
        ColonnesHouseIII(coord2bis, coord4bis - 0.05, hauteur)
        ColonnesHouseIII(coord3bis, coord4bis - 0.05, hauteur)
        ColonnesHouseIII(coord4bis - 0.05, coord4bis - 0.05, hauteur)

    HouseIIIRotation()

    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.transform.resize(
        value=(1.1, 1, 1),
        constraint_axis=(True, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.transform.resize(
        value=(1, 1.1, 1),
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )

    bpy.ops.transform.translate(
        value=(1.25, 0, 0),
        constraint_axis=(True, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.transform.translate(
        value=(0, 1, 0),
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )

    bpy.ops.transform.rotate(
        value=0.785398,
        axis=(0, 0, 1),
        constraint_axis=(False, False, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.object.select_all(action="DESELECT")


f = random.randint(1, 5)
# f=2
if f == 1:
    HouseIII()
    print("HouseIII")

###--------------------------------###
###--------------------------------###
###----------DALLE DE SOL----------###
###--------------------------------###
###--------------------------------###


def DalleSol(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(4.525, 4.5, 0.05))


DalleSol(4.525, 4.5, -0.05)

###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###
###                                        HOUSE I                                        ###
###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###


def HouseI():

    def MurHouseI(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    MurHouseI(
        ((1 / 4) * (loc2 - loc1)) / 2,
        ((1 / 4) * (loc2 - loc1)) / 2,
        0.05,
        loc2,
        hauteur,
        hauteur,
    )  # Mur 1
    MurHouseI(
        0.05 - 0.025 + ((loc3 - loc2) * (1 / 5)) / 2,
        loc2 - 0.05 - 0.025 + (loc3 - loc2) * (1 / 5) + ((loc3 - loc2) * (1 / 5)) / 2,
        0.05,
        loc1,
        hauteur,
        hauteur,
    )  # Mur 7
    MurHouseI(
        -0.025 + ((1 / 5) * (loc3 - loc2) / 2) / 2 + ((loc3 - loc2) * (2 / 5)) / 2,
        loc2
        + 0.025
        + (loc3 - loc2) * (2 / 5)
        + ((loc3 - loc2) * (2 / 5)) / 2
        + ((1 / 5) * (loc3 - loc2) / 2) / 2,
        0.05,
        loc1,
        hauteur,
        hauteur,
    )  # Mur 8
    MurHouseI(
        (loc4 - loc3) * (1 / 3) / 2,
        loc4 - (loc4 - loc3) * (1 / 3) / 2,
        0.05,
        loc1,
        hauteur,
        hauteur,
    )  # Mur 11
    MurHouseI(
        ((1 / 5) * (loc3 - loc2) / 2) / 2 + (loc4 - loc3) * (2 / 3) / 2,
        loc3 + (loc4 - loc3) * (2 / 3) / 2 - ((1 / 5) * (loc3 - loc2) / 2) / 2,
        0.05,
        loc1,
        hauteur / 1.5,
        hauteur / 1.5,
    )  # Mur 10
    MurHouseI(
        (loc4 - loc3) * (1 / 3) / 2,
        loc4 - (loc4 - loc3) * (1 / 3) / 2,
        0.05,
        loc2,
        hauteur,
        hauteur,
    )  # 13
    MurHouseI(
        0.05,
        loc4 - 0.05,
        ((loc2 + ((loc3 - loc2) * (1 / 5) * 3)) - loc2) / 2,
        loc2 + ((loc2 + ((loc3 - loc2) * (1 / 5) * 3)) - loc2) / 2,
        hauteur,
        hauteur,
    )  # 14
    MurHouseI(0.05, loc4 - 0.05, loc2 / 2, loc2 / 2, 0.05, hauteur * 1.25)  # 12

    MurHouseI(
        ((1 / 5) * (loc3 - loc2)) / 2,
        loc2 + ((1 / 5) * (loc3 - loc2)) / 2,
        0.05,
        loc4 - 0.05,
        hauteur / 2,
        hauteur * 1.5,
    )  # 20 étage
    MurHouseI(
        0.05, loc2 + 0.05, 0.05, loc4 - 0.05, hauteur / 2, hauteur / 2
    )  # Colonne Rez sous mur 20
    MurHouseI(
        (loc4 - (loc2 + (2 / 5) * (loc3 - loc2))) / 2,
        loc4 - (loc4 - (loc2 + (2 / 5) * (loc3 - loc2))) / 2,
        0.05,
        loc4,
        hauteur,
        hauteur,
    )  # Mur fond
    MurHouseI(
        loc2 / 2, loc2 / 2, 0.05, loc4 - 0.05, 0.1, hauteur + 0.1
    )  # Poutre coord 4 en Y
    MurHouseI(
        (3.5 / 5) * (loc3 - loc2) / 2,
        loc2 + (1 / 5) * (loc3 - loc2) + (3.5 / 5) * (loc3 - loc2) / 2,
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        0.05,
        hauteur + 0.05,
    )  # Poutre 2 coord 4 en Y

    MurHouseI(
        0.05,
        loc2 - loc2 * (1 / 3),
        (2 / 5) * (loc3 - loc2) / 2,
        loc2 - 0.05 + (1 / 5) * (loc3 - loc2) + (2 / 5) * (loc3 - loc2) / 2,
        0.025 + hauteur / 2,
        -0.025 + hauteur * 1.5,
    )  # Mur 4 étage
    MurHouseI(
        0.05,
        loc1 - 0.1 + (1 / 4) * (loc2 - loc1),
        ((1 / 5) * (loc3 - loc2)) / 2,
        loc2 + ((1 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur * 1.25,
    )  # Poutre entrée

    MurHouseI(
        0.025,
        loc2,
        0.025,
        loc2 + (1 / 5) * (loc3 - loc2) + (2 / 5) * (loc3 - loc2) / 2,
        hauteur,
        hauteur,
    )  # Colonne sur X 1 => 15
    MurHouseI(
        0.025, loc2, 0.025, loc2 + ((loc3 - loc2) * (1 / 5) * 3), hauteur, hauteur
    )  # Colonne sur X 1 => 16
    MurHouseI(
        0.025, loc2, 0.025, loc3 - (1 / 2) * (1 / 5) * (loc3 - loc2), hauteur, hauteur
    )  # Colonne sur X 1 =>17
    MurHouseI(
        0.025, loc2, 0.025, loc3 + 0.025, hauteur, hauteur
    )  # Colonne sur X 1 =>18
    MurHouseI(
        0.05,
        loc2 - 0.05 + (1 / 2) * (1 / 5) * (loc3 - loc2),
        -0.025 + ((1.5 / 5) * (loc3 - loc2)) / 2,
        loc3
        + 0.025
        - (1 / 2) * (1 / 5) * (loc3 - loc2)
        - (((1.5 / 5) * (loc3 - loc2))) / 2,
        hauteur / 2,
        hauteur * 1.5,
    )  # etage
    MurHouseI(
        0.05,
        loc2,
        -0.025 + ((1.5 / 5) * (loc3 - loc2)) / 2,
        loc3
        + 0.025
        - (1 / 2) * (1 / 5) * (loc3 - loc2)
        - (((1.5 / 5) * (loc3 - loc2))) / 2,
        hauteur / 2,
        hauteur * 1.5,
    )

    MurHouseI(
        0.05,
        loc1,
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
        hauteur,
    )  # Colonne Rect Fond   19
    MurHouseI(
        0.025 + (loc3 - loc2) * (2 / 5) / 2,
        loc3 - 0.025 - (loc3 - loc2) * (2 / 5),
        0.20,
        loc2 - 0.1 + ((loc3 - loc2) * (1 / 5) * 3),
        0.05,
        hauteur * 1.25 + 0.25,
    )  # Poutre perpendiculaire plate
    MurHouseI(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)),
        0.2,
        loc2 - 0.1 + ((loc3 - loc2) * (1 / 5) * 3),
        hauteur,
        hauteur,
    )
    MurHouseI(
        0.05, loc2 + ((loc3 - loc2) * (2 / 5)), 0.05, loc1, hauteur / 1.5, hauteur / 1.5
    )
    MurHouseI(
        0.05,
        loc4 - 0.05,
        (loc4 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        loc4 - (loc4 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        hauteur,
        hauteur,
    )
    MurHouseI(0.1, loc4 - 0.15, 0.05, loc3, hauteur / 2.5, hauteur * 1.5)
    MurHouseI(
        0.1,
        loc4 - 0.15,
        0.05,
        loc2 + ((loc3 - loc2) * (1 / 5) * 3),
        hauteur / 2.5,
        hauteur * 1.5,
    )
    MurHouseI(
        0.1,
        loc4 - 0.15,
        0.05 + (loc3 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        loc3 - (loc3 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur * 1.5 + hauteur / 2.5,
    )
    MurHouseI(
        0.1,
        loc4 - 0.15,
        0.05 + (loc3 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        loc3 - (loc3 - loc2 - (3 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur * 1.5 - hauteur / 2.5,
    )

    MurHouseI(
        0.025,
        loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2),
        0.025,
        loc1 + 0.025,
        hauteur * 0.95,
        hauteur * 0.95,
    )  # Entrée
    MurHouseI(
        0.025,
        loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2),
        0.025,
        loc2,
        hauteur * 0.95,
        hauteur * 0.95,
    )  # Entrée
    MurHouseI(
        0.025,
        loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2),
        -0.1 + 0.05 + (loc2 + (1 / 5) * (loc3 - loc2)) / 2,
        (loc2 + (1 / 5) * (loc3 - loc2)) / 2,
        0.025,
        hauteur * 1.9,
    )  # Entrée
    MurHouseI(
        ((1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        loc2 + ((3 / 4) * (1 / 5) * (loc3 - loc2)),
        0.025,
        loc1 + 0.025,
        0.025,
        hauteur * 1.9,
    )  # Entrée
    MurHouseI(
        (loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        (loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        0.05,
        loc2,
        0.05,
        hauteur * 1.25,
    )

    MurHouseI(
        ((3.5 / 5) * (loc3 - loc2)) / 2,
        loc2 + ((3.5 / 5) * (loc3 - loc2)) / 2 + (1 / 5) * (loc3 - loc2),
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur / 4,
        hauteur * 0.75,
    )
    MurHouseI(
        0.05,
        loc2 - 0.05 + (1 / 2) * (1 / 5) * (loc3 - loc2),
        ((1 / 5) * (loc3 - loc2)) / 2,
        loc2 + (2 / 5) * (loc3 - loc2) + ((1 / 5) * (loc3 - loc2)) / 2,
        hauteur / 2,
        hauteur * 1.5,
    )

    def SolHouseI(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    SolHouseI(
        0.025 + (loc3 - (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        0.025 + ((loc3 - (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2),
        0.025 + loc3 / 2 - ((2 / 5) * (loc3 - loc2)) / 2,
        0.025 + (loc3 / 2 - ((2 / 5) * (loc3 - loc2)) / 2),
        (hauteur / 2) * (1 / 3),
        (hauteur / 2) * (1 / 3),
    )  # Plancher intermediaire
    SolHouseI(
        (loc4 - loc2 - (2 / 5) * (loc3 - loc2)) / 2,
        loc4 - (loc4 - loc2 - (2 / 5) * (loc3 - loc2)) / 2,
        (loc3 - loc2 - (3.5 / 5) * (loc3 - loc2)) / 2,
        loc3 - (loc3 - loc2 - (3.5 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur,
    )  # Etage
    SolHouseI(
        (loc2 + ((loc3 - loc2) * (2 / 5))) / 2,
        (loc2 + ((loc3 - loc2) * (2 / 5))) / 2,
        (1 / 2) * (1 / 5) * (loc3 - loc2) / 2,
        loc3 - ((0.5 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur,
    )  # Etage
    SolHouseI(
        0.025 + ((loc3 - loc2) * (1 / 5) / 2) / 2,
        loc2 - 0.025 + ((loc3 - loc2) * (1 / 5) / 2) / 2,
        0.025 + ((1.5 / 5) * (loc3 - loc2)) / 2,
        loc3
        - 0.025
        - (1 / 2) * (1 / 5) * (loc3 - loc2)
        - (((1.5 / 5) * (loc3 - loc2))) / 2,
        0.05,
        hauteur,
    )  # Etage
    SolHouseI(
        (loc2 - (2 / 3) * loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        (2 / 3) * loc2
        + (loc2 - (2 / 3) * loc2 + (1 / 2) * (1 / 5) * (loc3 - loc2)) / 2,
        0.025 + ((2 / 5) * (loc3 - loc2)) / 2,
        loc2 + 0.025 + (1 / 5) * (loc3 - loc2) + ((2 / 5) * (loc3 - loc2)) / 2,
        0.05,
        hauteur,
    )
    SolHouseI(
        (loc2 - 0.05 - loc2 * (1 / 3)) / 2,
        (loc2 - 0.05 - loc2 * (1 / 3)) / 2,
        0.025 + ((1.5 / 5) * (loc3 - loc2)) / 2,
        loc3
        - 0.025
        - (1 / 2) * (1 / 5) * (loc3 - loc2)
        - (((1.5 / 5) * (loc3 - loc2))) / 2,
        0.05,
        hauteur,
    )
    SolHouseI(
        ((1 / 4) * (loc2 - loc1)) / 2,
        ((1 / 4) * (loc2 - loc1)) / 2,
        (2 / 5) * (loc3 - loc2) / 2,
        loc2 - 0.05 + (1 / 5) * (loc3 - loc2) + (2 / 5) * (loc3 - loc2) / 2,
        0.05,
        hauteur,
    )
    SolHouseI(0.05, loc1, (loc4 - loc3) / 2, loc3 + (loc4 - loc3) / 2, 0.05, hauteur)
    SolHouseI(
        ((1 / 4) * (loc2 - loc1)) / 2,
        ((1 / 4) * (loc2 - loc1)) / 2,
        ((1.5 / 5) * (loc3 - loc2)) / 2,
        loc3 - (1 / 2) * (1 / 5) * (loc3 - loc2) - ((1.5 / 5) * (loc3 - loc2)) / 2,
        (hauteur / 2) * (1 / 3),
        (hauteur / 2) * (1 / 3),
    )


def ElementHouseI():

    def Rythme(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    def ColonneRonde(pos_X, pos_Y, pos_Z):
        bpy.ops.mesh.primitive_cylinder_add(
            radius=0.05, depth=hauteur * 2, location=(pos_X, pos_Y, pos_Z)
        )

    ### Rythme 1 Lame 1

    Rythme(
        0.05,
        loc2 + ((loc3 - loc2) * (1 / 5)),
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
        hauteur,
    )  # Colonne Rect Fond
    Rythme(
        0.05,
        loc2 + ((loc3 - loc2) * (1 / 5)),
        ((loc4 - loc3) + (loc3 - loc2) * (2 / 5)) / 2,
        loc4 - (((loc4 - loc3) + (loc3 - loc2) * (2 / 5)) / 2),
        0.05,
        hauteur - 0.1,
    )  ## Poutre fond
    ColonneRonde(
        loc2 + ((loc3 - loc2) * (1 / 5)), loc2 + ((loc3 - loc2) * (1 / 5) * 3), hauteur
    )  ## Colonne Ronde
    Rythme(
        0.05,
        loc2 + ((loc3 - loc2) * (1 / 5)),
        (loc2 + (3 / 5) * (loc3 - loc2)) / 2,
        (loc2 + (3 / 5) * (loc3 - loc2)) / 2,
        0.15,
        hauteur * 1.25,
    )  ## Poutre Avant

    ### Rythme 1 Lame 2

    ColonneRonde(
        loc2 + ((loc3 - loc2) * (2 / 5)), loc2 + ((loc3 - loc2) * (1 / 5) * 3), hauteur
    )  ## Colonne Ronde
    Rythme(
        0.05,
        loc2 + ((loc3 - loc2) * (2 / 5)),
        (loc2 + (3 / 5) * (loc3 - loc2)) / 2,
        (loc2 + (3 / 5) * (loc3 - loc2)) / 2,
        0.15,
        hauteur * 1.25,
    )  ## Poutre Avant

    ### Rythme 1 Lame 3

    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)),
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
        hauteur,
    )  ## Colonne Rect Fond

    ### Rythme 1 Lame 4

    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2,
        ((loc4 - loc3) * (1 / 3)) / 2,
        loc4 - ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
        hauteur,
    )  ## Colonne Rect Fond
    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2,
        (loc4 - (loc2 + 0.025)) / 2,
        loc2 + (loc4 - (loc4 - loc3)) / 2,
        0.05,
        hauteur * 2 - 0.05,
    )  ## Poutre
    ColonneRonde(
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2, loc2, hauteur
    )  ## Colonne Ronde avant

    ### Rythme 2 Volume 1

    Rythme(
        ((1 / 4) * (loc2 - loc1)) / 2,
        ((1 / 4) * (loc2 - loc1)) / 2,
        0.05,
        loc2 + (loc3 - loc2) * (1 / 5),
        hauteur,
        hauteur,
    )  # Mur 2
    Rythme(
        -0.05 + (loc3 - loc2) * (1 / 5) / 2,
        loc2 - 0.05 + (loc3 - loc2) * (1 / 5) / 2,
        0.05,
        loc2 + ((loc3 - loc2) * (1 / 5)),
        hauteur,
        hauteur,
    )  # Mur 3 sur plan
    Rythme(
        0.05,
        loc2,
        0.05 + (2 / 5) * (loc3 - loc2) / 2,
        loc2 + (1 / 5) * (loc3 - loc2) + (2 / 5) * (loc3 - loc2) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 4
    Rythme(
        loc2 / 2, loc2 / 2, 0.05, loc2 + (loc3 - loc2) * (1 / 5), 0.05, hauteur
    )  # Poutre entre mur 2 et 4
    Rythme(
        ((loc3 - loc2) * (1 / 5) / 2) / 2,
        loc2 + ((loc3 - loc2) * (1 / 5) / 2) / 2,
        0.05,
        loc2 + ((loc3 - loc2) * (3 / 5)),
        hauteur / 2,
        hauteur / 2,
    )  # Mur 6
    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2,
        ((1 / 5) * (loc3 - loc2)) / 2,
        loc2 + (1 / 5) * (loc3 - loc2) + ((1 / 5) * (loc3 - loc2)) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # 29
    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2,
        (3 / 4) * ((1 / 5) * (loc3 - loc2)) / 2,
        loc2
        + (1 / 5) * (loc3 - loc2)
        + (1 / 5) * (loc3 - loc2)
        + (3 / 4) * ((1 / 5) * (loc3 - loc2)) / 2,
        (hauteur / 2.5) / 2,
        (hauteur / 2.5) * 2,
    )  # 31
    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)) / 2,
        (1 / 4) * ((1 / 5) * (loc3 - loc2)) / 2,
        loc2
        + 0.05
        + (1 / 5) * (loc3 - loc2)
        + (1 / 5) * (loc3 - loc2)
        + (3 / 4) * (1 / 5) * (loc3 - loc2)
        + ((1 / 4) * ((1 / 5) * (loc3 - loc2)) / 2) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # 28
    Rythme(
        0.2,
        loc3 + 0.2,
        0.2,
        loc2 - 0.2 + ((loc3 - loc2) * (1 / 5) * 3),
        hauteur,
        hauteur,
    )  # 30

    ### Rythme 2 Volume 2

    Rythme(
        0.05,
        loc3 - ((loc3 - loc2) * (1 / 5)),
        (1 / 5) * (loc3 - loc2) / 2,
        loc3 - (1 / 5) * (loc3 - loc2),
        hauteur,
        hauteur,
    )  # 33
    Rythme(
        0.05,
        loc2 + ((loc3 - loc2) * (2 / 5)),
        (1 / 5) * (loc3 - loc2) / 2,
        loc3 - (1 / 5) * (loc3 - loc2),
        hauteur,
        hauteur,
    )  # 32
    Rythme(
        0.2,
        loc3 - (loc3 - loc2) * (2 / 5),
        (1 / 5) * (loc3 - loc2) / 2,
        loc3 - (1 / 5) * (loc3 - loc2),
        hauteur,
        hauteur,
    )  # 34
    Rythme(
        (loc3 - loc2) * (2 / 5) / 2,
        loc3 - (loc3 - loc2) * (2 / 5),
        0.05,
        loc2 + 0.05 + (3 / 5) * (loc3 - loc2) + (1 / 5) * (loc3 - loc2) / 2,
        (hauteur / 2) / 2,
        hauteur * 1.75,
    )  # 35


###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###
###                                        HOUSE II                                       ###
###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###


def HouseII():

    ################
    ###  Colonnes
    ################

    def ColonnesHouseII(pos_X, pos_Y, dim_Z):
        pos_Z = dim_Z
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(0.05, 0.05, dim_Z))

    ColonnesHouseII(coord1, coord1, hauteur + 0.2)
    ColonnesHouseII(coord2, coord1, hauteur + 0.2)
    ColonnesHouseII(coord3, coord1, hauteur + 0.2)
    ColonnesHouseII(coord4, coord1, hauteur + 0.2)

    ColonnesHouseII(coord1, coord2, hauteur)
    ColonnesHouseII(coord2, coord2, hauteur)
    ColonnesHouseII(coord3, coord2, hauteur + 0.2)
    ColonnesHouseII(coord4, coord2, hauteur + 0.2)

    ColonnesHouseII(coord1, coord3, hauteur)
    ColonnesHouseII(coord2, coord3, hauteur)
    ColonnesHouseII(coord3, coord3, hauteur)
    ColonnesHouseII(coord4, coord3, hauteur)

    ColonnesHouseII(coord1, coord4, hauteur)
    ColonnesHouseII(coord2, coord4, hauteur - 0.33)
    ColonnesHouseII(coord3, coord4, hauteur - 0.33)
    ColonnesHouseII(coord4, coord4, hauteur - 0.33)

    ################
    ###  Planchers et toit
    ################

    def PlanchersHouseII(dim_X, pos_X, dim_Y, pos_Y, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, 0.05))

    PlanchersHouseII(
        (coord4 / 2),
        (coord4 / 2) + 0.025,
        ((coord4 - coord3) / 2) * 0.75,
        coord3 - 0.05 + ((coord4 - coord3) / 2) * 0.75,
        0.05 + (hauteur - (0.165 * 3)),
    )
    PlanchersHouseII(
        -0.05 + (coord2 - coord1) / 2,
        0.05 + (coord2 - coord1) / 2,
        ((coord3 - coord2) / 2) * 0.25,
        coord3 - ((coord3 - coord2) / 2) * 0.25,
        0.05 + (hauteur - (0.165 * 3)),
    )

    PlanchersHouseII(
        -0.05 + ((coord4 - coord2) - ((coord4 - coord2) * 0.25 / 2)) / 2,
        0.05 + coord2 + (((coord4 - coord2) - (coord4 - coord2) * 0.25 / 2) / 2) - 0.05,
        ((coord3 - coord2) / 2) * 0.75,
        coord2 - 0.05 + ((coord3 - coord2) / 2) * 0.75,
        hauteur,
    )
    PlanchersHouseII(
        ((coord3 - coord2) / 2) + 0.05,
        coord2 + ((coord3 - coord2) / 2),
        ((coord2 - coord1) / 2) * 0.25,
        coord2 - ((coord2 - coord1) / 2) * 0.25,
        hauteur,
    )
    PlanchersHouseII(
        ((coord4 - coord3) * 0.30) / 2,
        coord4 + 0.05 - ((coord4 - coord3) * 0.30) / 2,
        ((coord3 - coord2) * 0.25) / 2,
        coord2 - 0.05 + ((coord3 - coord2) * 0.25) / 2,
        hauteur,
    )

    PlanchersHouseII(
        ((coord4 - coord3) - ((coord4 - coord3) * 0.25)) / 2,
        coord3 - 0.05 + ((coord4 - coord3) - ((coord4 - coord3) * 0.25)) / 2,
        coord2 / 2,
        coord2 / 2 - ((coord2 - coord1) * 0.25),
        (hauteur + (0.165 * 3)),
    )
    PlanchersHouseII(
        ((coord4 - coord3) * 0.3) / 2,
        coord4 + 0.05 - ((coord4 - coord3) * 0.3) / 2,
        coord2 - (coord2 + 0.05 - ((coord2 - coord1) * 0.3) / 2),
        coord2 - (coord2 + 0.05 - ((coord2 - coord1) * 0.3) / 2),
        (hauteur + (0.165 * 3)),
    )

    ### Toit

    PlanchersHouseII(
        -0.05 + (coord4 / 2) + 0.025,
        0.05 + (coord4 / 2) + 0.025,
        ((coord4 - coord3) / 2) * 0.75,
        coord3 - 0.025 + ((coord4 - coord3) / 2) * 0.75,
        (hauteur * 2 - (0.165 * 3) - 0.1),
    )
    PlanchersHouseII(
        -0.05 + (coord2 - coord1) / 2,
        0.05 + (coord2 - coord1) / 2,
        ((coord3 - coord2) / 2) * 0.25,
        coord3 - ((coord3 - coord2) / 2) * 0.25,
        (hauteur * 2 - (0.165 * 3)),
    )

    PlanchersHouseII(
        -0.05 + ((coord4 - coord2) - ((coord4 - coord2) * 0.25 / 2)) / 2,
        0.05 + coord2 + (((coord4 - coord2) - (coord4 - coord2) * 0.25 / 2) / 2) - 0.05,
        ((coord3 - coord2) / 2) * 0.75,
        coord2 - 0.05 + ((coord3 - coord2) / 2) * 0.75,
        -0.05 + hauteur * 2,
    )
    PlanchersHouseII(
        ((coord3 - coord2) / 2) + 0.05,
        coord2 + ((coord3 - coord2) / 2),
        ((coord2 - coord1) / 2) * 0.25,
        coord2 - ((coord2 - coord1) / 2) * 0.25,
        -0.05 + hauteur * 2,
    )
    PlanchersHouseII(
        ((coord4 - coord3) * 0.30) / 2,
        coord4 + 0.05 - ((coord4 - coord3) * 0.30) / 2,
        ((coord3 - coord2) * 0.25) / 2,
        coord2 - 0.05 + ((coord3 - coord2) * 0.25) / 2,
        -0.05 + hauteur * 2,
    )

    PlanchersHouseII(
        -0.025 + ((coord4 - coord3) - ((coord4 - coord3) * 0.25)) / 2,
        0.025 + coord3 - 0.05 + ((coord4 - coord3) - ((coord4 - coord3) * 0.25)) / 2,
        coord2 / 2,
        coord2 / 2 - ((coord2 - coord1) * 0.25),
        hauteur * 2 + (0.165 * 3) - 0.225,
    )
    PlanchersHouseII(
        ((coord4 - coord3) * 0.3) / 2,
        coord4 + 0.05 - ((coord4 - coord3) * 0.3) / 2,
        coord2 - (coord2 + 0.05 - ((coord2 - coord1) * 0.3) / 2),
        coord2 - (coord2 + 0.05 - ((coord2 - coord1) * 0.3) / 2),
        hauteur * 2 + (0.165 * 3) - 0.225,
    )

    ##########
    ### Murs Ouest => Est   =====> Murs sur Axe Y
    ##########

    def Mur1(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    Mur1(
        (coord3 - coord1bis) / 2,
        coord1bis + (coord3 - coord1bis) / 2,
        0.05,
        coord4bis - 0.05,
        0.45,
        0.45,
    )  # Mur bas zone 1
    Mur1(
        0.05, coord2, 0.05, coord4bis - 0.05, hauteur - 0.33, hauteur - 0.33
    )  # Colonne position 2
    Mur1(
        0.05,
        coord1bis,
        0.05,
        coord4bis - 0.05,
        ((hauteur - (0.165 * 3)) / 2),
        ((hauteur - (0.165 * 3)) / 2),
    )  # Colonne Partie Basse zone 1
    Mur1(
        0.05 + (coord3 - coord1bis) / 2,
        coord1bis + (coord3 - coord1bis) / 2,
        0.05,
        coord4bis - 0.05,
        0.05,
        (hauteur - (0.165 * 3) + 0.05),
    )  # Poutre plancher bas
    Mur1(
        (coord2bis - coord2) / 2,
        coord2 + (coord2bis - coord2) / 2,
        0.05,
        coord4bis - 0.05,
        ((hauteur - (0.165 * 3)) / 2),
        ((hauteur - (0.165 * 3)) / 2),
    )  # Colonne Partie basse zone 2

    Mur1(
        0.05, coord3, 0.05, coord4bis - 0.05, hauteur - 0.33, hauteur - 0.33
    )  # Colonne position 3
    Mur1(
        0.025 + (coord3bis - coord3) / 2,
        0.025 + coord3 + (coord3bis - coord3) / 2,
        0.05,
        8.95,
        ((0.1 + hauteur - (0.165 * 3)) / 2),
        ((0.1 + hauteur - (0.165 * 3)) / 2),
    )  # Colonne Partie basse zone 2
    Mur1(
        0.05 + (coord4 - coord1bis) / 2,
        coord1bis + (coord4 - coord1bis) / 2,
        0.05,
        coord4bis - 0.05,
        2 * 0.165,
        (0.125 + ((hauteur * 2) - (0.165 * 3))),
    )  # Poutre Haute Large
    Mur1(
        0.05 + (coord3 - coord1bis) / 2,
        coord1bis + (coord3 - coord1bis) / 2,
        0.05,
        8.95,
        0.1,
        0.125 + (hauteur * 2) - (0.165 / 2),
    )  # Poutre haute

    ### Murs position 2

    Mur1(
        0.025 + coord1bis / 2, 0.025 + coord1bis / 2, 0.05, coord4, 0.45, 0.45
    )  # Mur bas avant (zone 1)  aligné avec paroi
    Mur1(
        coord1bis / 2, coord1bis / 2, 0.05, coord4, 0.2, (hauteur - (0.165 * 3) - 0.1)
    )  # Mur haut avant (zone 1)
    Mur1(
        0.05,
        coord1bis - 0.05,
        0.025,
        coord4,
        (hauteur / 2) - (0.83),
        (hauteur / 2) + 0.05,
    )  # Montant entre murs bas et haut avant (zone 1)
    Mur1(
        (coord3 - coord1bis) / 2,
        coord1bis + (coord3 - coord1bis) / 2,
        0.05,
        coord4,
        0.05,
        (hauteur - (0.165 * 3) + 0.05),
    )  # Poutre plancher zone 1

    Mur1(
        0.025 + (coord3bis - coord3) / 2,
        0.025 + coord3 + (coord3bis - coord3) / 2,
        0.05,
        coord4,
        (hauteur / 2) - (3 * 0.165) / 2,
        (hauteur / 2) - (3 * 0.165) / 2,
    )  # Mur zone 3
    Mur1(
        (coord4bis - coord3) / 2,
        coord3 + (coord4bis - coord3) / 2,
        0.05,
        coord4,
        0.2,
        (hauteur - (0.165 * 3) - 0.1),
    )  # Poutre plancher zone 3
    Mur1(
        coord4 / 2,
        (coord4 / 2) + 0.05,
        0.05,
        coord4,
        0.05,
        (hauteur * 2) - (0.165 * 3) - 0.15,
    )  # Poutre toiture zone 3

    ### Murs position 3

    Mur1(
        (coord2 / 2) / 2,
        coord2 / 2,
        0.05,
        coord3 - 0.1 + ((coord4 - coord3)) * 0.75,
        hauteur * 0.275,
        (hauteur - (3 * 0.165)) + (hauteur * 0.275),
    )
    Mur1(
        ((coord3 - coord2) / 2) - ((1 / 4) * ((coord3 - coord2) / 2)),
        coord2 + ((coord3 - coord2) - ((1 / 4) * (coord3 - coord2))) / 2,
        0.05,
        coord3 - 0.1 + ((coord4 - coord3)) * 0.75,
        hauteur * 0.275,
        (hauteur - (3 * 0.165)) + (hauteur * 0.275),
    )
    Mur1(
        0.05 + (coord3bis - coord3) / 2,
        coord3 + (coord3bis - coord3) / 2,
        0.05,
        coord3 - 0.1 + ((coord4 - coord3)) * 0.75,
        hauteur * (3 * 0.165),
        (hauteur - 3 * 0.165) + (hauteur * 3 * 0.165),
    )
    Mur1(
        ((coord4 - coord3bis) / 2) / 2,
        coord3bis + ((coord4 - coord3bis) / 2) / 2,
        0.05,
        coord3 - 0.1 + ((coord4 - coord3)) * 0.75,
        hauteur * 0.275,
        (hauteur - (3 * 0.165)) + (hauteur * 0.275),
    )

    ### Murs position 4

    Mur1(
        (coord4bis - coord2) / 2,
        coord2 + (coord4bis - coord2) / 2,
        0.05,
        coord3,
        0.30,
        (2 * hauteur - 0.30),
    )  # Poutre toiture zones 2 + 3 + 4
    Mur1(
        (coord2bis - coord2) / 2,
        coord2 - 0.05 + (coord2bis - coord2) / 2,
        0.05,
        coord3,
        hauteur * 0.25,
        (hauteur - 3 * 0.165) + (hauteur * 0.25),
    )
    Mur1(
        ((coord3 - coord2) / 2)
        - ((1 / 4) * ((coord3 - coord2) / 2))
        - (coord2bis - coord2) / 2,
        coord2bis
        + ((coord3 - coord2) / 2)
        - ((1 / 4) * ((coord3 - coord2) / 2))
        - (coord2bis - coord2) / 2,
        0.05,
        coord3,
        hauteur * 0.25,
        (hauteur - 3 * 0.165) + (hauteur * 0.25),
    )
    # ((coord3-coord2)/2)*0.25
    ### Murs position 5

    Mur1(
        (coord2 / 2) - ((1 / 4) * coord2) / 2,
        0.1 + ((coord2 - coord1) / 2) - ((1 / 4) * coord2) / 2,
        0.05,
        coord3 + 0.05 - ((coord3 - coord2) * 0.25),
        hauteur * 0.25,
        (hauteur - 3 * 0.165) + (hauteur * 0.25),
    )  # mur bas

    ### Murs position 6

    Mur1(
        (coord4bis - coord3) / 2,
        coord3 - 0.05 + (coord4bis - coord3) / 2,
        0.05,
        coord2,
        0.30,
        (2 * hauteur + 0.1),
    )  # Poutre toiture zones 2 + 3 + 4
    Mur1(
        (coord3bis - coord3) / 2,
        coord3 + (coord3bis - coord3) / 2,
        0.05,
        coord2,
        hauteur * 0.175,
        (hauteur - 0.28) + (hauteur * 0.175) + 0.3,
    )
    Mur1(
        coord4bis / 2,
        coord4bis / 2,
        (coord2bis - coord2) / 2,
        coord2 + 0.05 + (coord2bis - coord2) / 2,
        0.05,
        0.05 + (hauteur - 3 * 0.165),
    )  # Poutre niveau plancher bas sous plancher intermediaire

    ### Murs position 7

    Mur1(
        (coord2bis - coord2) / 2,
        coord2 + (coord2bis - coord2) / 2,
        0.05,
        coord2 + 0.05 - (coord2 - coord1) * 0.25,
        hauteur * 0.1,
        (hauteur - 0.2) + (hauteur * 0.175 / 2) + 0.275,
    )
    Mur1(
        ((coord3 - coord2) / 2)
        - ((1 / 4) * ((coord3 - coord2) / 2))
        - (coord2bis - coord2) / 2,
        coord2bis
        + ((coord3 - coord2) / 2)
        - ((1 / 4) * ((coord3 - coord2) / 2))
        - (coord2bis - coord2) / 2,
        0.05,
        coord2 + 0.05 - (coord2 - coord1) * 0.25,
        hauteur * 0.1,
        (hauteur - 0.2) + (hauteur * 0.175 / 2) + 0.3,
    )
    Mur1(
        (coord3bis - coord3) / 2,
        coord3 + (coord3bis - coord3) / 2,
        0.05,
        coord1 - 0.05 + ((coord2 - coord1)) * 0.75,
        hauteur * 0.175,
        (hauteur + 3 * 0.165) + (hauteur * 0.175),
    )

    ### Murs position 8

    Mur1(
        (coord4bis - coord1bis) / 2,
        coord1bis + (coord4bis - coord1bis) / 2,
        0.05,
        coord1bis + 0.05,
        0.2,
        (hauteur - 3 * 0.165),
    )
    Mur1(0.05, coord4, 0.025, coord1bis + 0.05, hauteur / 2, hauteur / 2)
    Mur1(0.05, coord3, 0.05, coord1bis + 0.05, (hauteur / 2) + 0.2, (hauteur / 2) + 0.2)

    ### Murs position 9

    Mur1(coord3 / 2, coord3 / 2, 0.05, coord1, 0.4, hauteur * 2)
    Mur1(
        (coord4bis - coord3) / 2,
        coord3 - 0.05 + (coord4bis - coord3) / 2,
        0.05,
        coord1,
        0.05,
        hauteur * 2 + 2 * 0.175,
    )
    Mur1(
        (coord4 - coord1) / 2,
        0.05 + (coord4 - coord1) / 2,
        0.05,
        coord1,
        0.05,
        hauteur + 3 * 0.165,
    )
    Mur1(
        (coord4 - coord3) / 2,
        coord3 + (coord4 - coord3) / 2,
        0.05,
        coord1,
        0.4,
        hauteur + 0.05,
    )
    Mur1(0.05, coord3bis, 0.025, coord1, (hauteur / 2), hauteur / 2)

    ### Murs position 10

    Mur1(
        0.05,
        coord3,
        0.025,
        coord1 / 2 - ((coord2 - coord1) * 0.25),
        (hauteur / 2) - 0.1,
        hauteur * 1.5 + 3 * 0.165 - 0.1,
    )
    Mur1(
        0.05,
        coord3bis,
        0.025,
        coord1 / 2 - ((coord2 - coord1) * 0.25),
        (hauteur / 2) - 0.1,
        hauteur * 1.5 + 3 * 0.165 - 0.1,
    )

    ##########
    ### Murs Nord => Sud   =====> Murs sur Axe X
    ##########

    ### Murs Position 1

    Mur1(
        0.05,
        coord4,
        (coord4bis - coord4) / 2,
        coord4 + (coord4bis - coord4) / 2,
        0.05,
        (hauteur * 2) - (0.165 * 3) - 0.15,
    )  # Poutre
    Mur1(
        0.05, coord4bis - 0.1, 0.025 + coord2 / 2, 0.025 + coord2 / 2, 0.4, hauteur * 2
    )  # Mur fond étage contre plan //
    Mur1(
        0.05,
        coord4bis - 0.1,
        coord1bis / 2,
        coord1bis / 2,
        hauteur / 2,
        hauteur * 1.45 + 3 * 0.165,
    )  # Mur fond étage contre plan // retour bas
    Mur1(0.05, coord4, coord2 / 2, 0.05 + coord2 / 2, 0.05, hauteur + 3 * 0.165)
    Mur1(
        0.05,
        coord4,
        coord2 - (coord2 + 0.05 - ((coord2 - coord1) * 0.3) / 2),
        coord1 - ((coord2 - coord1) * 0.3) / 2,
        (hauteur / 2) - 0.05,
        hauteur * 1.45 + 3 * 0.165,
    )  # Mur sort façade

    ### Murs Position 2

    # Mur()
    # Mur()

    ### Murs Position 3

    Mur1(
        0.05,
        coord3bis,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur * 2 - 3 * 0.165 - 0.2,
    )
    Mur1(
        0.05,
        coord3bis,
        (coord4bis - coord4) / 2,
        coord4 + (coord4bis - coord4) / 2,
        0.45,
        0.45,
    )
    Mur1(
        0.05,
        coord3bis,
        (coord4bis - coord4) / 2,
        coord4 + (coord4bis - coord4) / 2,
        0.05,
        hauteur - (0.165 * 3) + 0.05,
    )

    ### Murs Position 4

    Mur1(
        0.05,
        coord3,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur - (0.165 * 3) + 0.05,
    )
    Mur1(
        0.05,
        coord3,
        0.05 + ((coord4 - coord3) * 0.25) / 2,
        coord4 - 0.05 - ((coord4 - coord3) * 0.25) / 2,
        0.05,
        (hauteur * 2) - (0.165 * 3) - 0.15,
    )
    Mur1(0.05, coord3, (coord2 - coord1) / 2, (coord2 - coord1) / 2, 0.4, hauteur * 2)

    ### Murs Position 5

    Mur1(
        0.05,
        coord2bis,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur * 2 - 3 * 0.165 - 0.2,
    )

    ### Murs Position 6

    Mur1(
        0.05,
        coord2,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur - (0.165 * 3) + 0.05,
    )  # Poutre Basse
    Mur1(
        0.05,
        coord2,
        0.05 + ((coord4 - coord3) * 0.25) / 2,
        coord4 - 0.05 - ((coord4 - coord3) * 0.25) / 2,
        0.05,
        (hauteur * 2) - (0.165 * 3) - 0.15,
    )  # Poutre Haute
    Mur1(
        0.05,
        coord2,
        (coord3 - coord2) / 2,
        coord2 + (coord3 - coord2) / 2,
        0.30,
        (2 * hauteur - 0.30),
    )
    Mur1(
        0.05,
        coord2,
        ((coord2 - coord1) / 2) * 0.25,
        coord2 - ((coord2 - coord1) / 2) * 0.25,
        hauteur * 0.1,
        (hauteur - 0.2) + (hauteur * 0.175 / 2) + 0.275,
    )
    Mur1(
        0.05,
        coord2,
        ((coord3 - coord2) / 2) - ((coord3 - coord2) / 2) * 0.25,
        coord2 - 0.05 + ((coord3 - coord2) / 2) - ((coord3 - coord2) / 2) * 0.25,
        hauteur * 0.1,
        (hauteur - 0.2) + (hauteur * 0.175 / 2) + 0.275,
    )

    ### Murs Position 7

    Mur1(
        0.05,
        coord1bis,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur * 2 - 3 * 0.165 - 0.2,
    )
    Mur1(
        0.05,
        coord1bis,
        0.05 + ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        coord4bis - 0.05 - ((coord4bis - coord4) + (coord4 - coord3) * 0.25) / 2,
        0.05,
        hauteur - (0.165 * 3) + 0.05,
    )
    Mur1(
        0.05,
        coord1bis,
        0.025 + (coord4bis - coord4) / 2,
        coord4 - 0.025 + (coord4bis - coord4) / 2,
        0.45,
        0.45,
    )

    ### Murs Position 8

    Mur1(
        0.05,
        coord1,
        (coord4 - coord1) / 2,
        (coord4 - coord1) / 2,
        0.05,
        (hauteur * 2) - (0.165 * 3) - 0.15,
    )
    Mur1(
        0.05,
        coord1,
        (coord4 - coord1bis) / 2,
        coord4 - (coord4 - coord1bis) / 2,
        0.3,
        (hauteur * 2) - (0.165 * 3) + 0.2,
    )
    Mur1(
        0.05,
        coord1,
        (coord4 - coord1) / 2,
        (coord4 - coord1) / 2,
        0.05,
        (hauteur - (0.165 * 3) + 0.05),
    )
    Mur1(
        0.05,
        coord1,
        ((coord4 - coord3) / 2) - ((coord4 - coord3) / 2) * 0.5,
        coord4 - 0.05 - (coord4 - coord3) / 2,
        hauteur * 0.35,
        (hauteur - (3 * 0.165)) + (hauteur * 0.35),
    )  # Mur façade étage milieu
    Mur1(
        0.05,
        coord1,
        ((coord3 - coord2) / 2) * 0.25,
        coord3 - ((coord3 - coord2) / 2) * 0.25,
        hauteur * 0.35,
        (hauteur - (3 * 0.165)) + (hauteur * 0.35),
    )  # Mur façade étage droite
    Mur1(
        0.05,
        coord1,
        ((coord4 - coord3) / 2) * 0.25,
        coord4 - ((coord4 - coord3) / 2) * 0.25,
        hauteur * 0.35,
        (hauteur - (3 * 0.165)) + (hauteur * 0.35),
    )  # Mur façade étage gauche


def ElementHouseII():

    ################
    ###  Plans Parallèles
    ################

    def MurHouseII(pos_X, dim_Y, pos_Y, dim_Z):
        pos_Z = dim_Z
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(0.05, dim_Y, dim_Z))

    MurHouseII(
        coord1bis,
        ((coord2bis - coord1bis) * 0.2) / 2,
        coord1bis + (((coord2bis - coord1bis) * 0.2) / 2),
        hauteur,
    )
    MurHouseII(
        coord2bis,
        (coord2bis - coord1bis) / 2,
        coord1bis + ((coord2bis - coord1bis) / 2),
        hauteur,
    )
    MurHouseII(
        coord3bis,
        (coord3 - coord1bis) / 2,
        coord1bis + ((coord3 - coord1bis) / 2),
        hauteur,
    )
    MurHouseII(
        coord4bis,
        (coord4bis - coord1bis) / 2,
        coord1bis + ((coord4bis - coord1bis) / 2),
        hauteur,
    )


###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###
###                                           HOUSE IV                                    ###
###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###


def HouseIV():

    def MursHouseIV(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    def SolsHouseIV(dim_X, pos_X, dim_Y, pos_Y, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, 0.05))

    ### Sols

    SolsHouseIV(
        loc4 / 2,
        (loc4 / 2) - (1 / 3) * (loc4 - loc3),
        loc2 / 2,
        loc2 / 2,
        hauteur * (3.75 / 3),
    )  # Sol étage V1
    SolsHouseIV(
        loc4 / 2,
        (loc4 / 2) + (1 / 3) * (loc4 - loc3),
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        hauteur * (2.25 / 3),
    )  # Sol étage V2

    SolsHouseIV(
        (loc4 - (loc4 - loc3) * (1 / 3)) / 2,
        (loc4 - (loc4 - loc3) * (1 / 3)) / 2,
        loc2 / 2,
        loc2 / 2,
        hauteur * (3.75 / 3) + hauteur - 0.05,
    )  # Toit V1
    SolsHouseIV(
        (loc4 - (loc4 - loc3) * (1 / 3)) / 2,
        (loc4 - (loc4 - loc3) * (1 / 3)) / 2 + (1 / 3) * (loc4 - loc3),
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        hauteur * (2.25 / 3) + hauteur - 0.05,
    )  # Toit V2

    ### Murs

    MursHouseIV(
        0.05,
        loc3,
        (loc3 - (3 / 4) * (loc2 - loc1)) / 2,
        loc3 - (loc3 - (3 / 4) * (loc2 - loc1)) / 2,
        hauteur,
        hauteur,
    )  # Mur Centre
    MursHouseIV(
        0.05,
        loc2,
        (loc3 - loc2 + (1 / 4) * (loc4 - loc3)) / 2,
        loc2 + (loc3 - loc2 + (1 / 4) * (loc4 - loc3)) / 2,
        hauteur,
        hauteur,
    )  # Mur Centre

    MursHouseIV(
        0.025 + loc2 / 2,
        0.025 + loc2 / 2,
        0.05,
        loc2,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Mur étage plancher avant
    MursHouseIV(
        ((2 / 3) * (loc4 - loc3)) / 2,
        loc3 + ((2 / 3) * (loc4 - loc3)) / 2,
        0.05,
        loc2,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Mur étage plancher avant

    MursHouseIV(
        ((2 / 3) * (loc2 - loc1)) / 2,
        loc2 - ((2 / 3) * (loc2 - loc1)) / 2,
        0.05,
        loc3,
        hauteur / 2,
        hauteur * (2.25 / 3) + hauteur / 2,
    )  # Mur étage plancher arrière

    MursHouseIV(
        0.1 + (1 / 3) * (loc2) / 2,
        0.1 + (1 / 3) * (loc2) / 2,
        0.05,
        loc4 - (1 / 3) * (loc4 - loc3),
        hauteur,
        hauteur,
    )  # Mur cuisine
    MursHouseIV(
        0.025 + (2 / 3) * (loc2) / 2,
        loc2 + 0.025 - ((2 / 3) * (loc2)) / 2,
        0.05,
        loc4 - (1 / 3) * (loc4 - loc3),
        hauteur / 2,
        hauteur * (2.25 / 3) + hauteur / 2,
    )  # Mur étage prolongement mur cuisine

    MursHouseIV(
        (1 / 3) * (loc4 - loc3) / 2,
        loc4 + ((1 / 3) * (loc3 - loc4)) / 2,
        0.05,
        loc1 + (1 / 3) * (loc4 - loc3),
        hauteur,
        hauteur,
    )  # Mur living
    MursHouseIV(
        (2 / 3) * (loc4 - loc3) / 2,
        loc3 - ((2 / 3) * (loc3 - loc4)) / 2,
        0.05,
        loc1 + (1 / 3) * (loc4 - loc3),
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Mur étage prolongement mur living

    MursHouseIV(
        0.05,
        (1 / 3) * (loc2 - loc1),
        ((1 / 3) * (loc2 - loc1)) / 2,
        loc1 + ((1 / 3) * (loc2 - loc1)) / 2,
        hauteur * (3.75 / 3) / 2,
        hauteur * (3.75 / 3) / 2,
    )  # Mur Dining
    MursHouseIV(
        0.05,
        (1 / 3) * (loc2 - loc1),
        0.05,
        loc3,
        hauteur * (2.25 / 3) / 2,
        hauteur * (2.25 / 3) / 2,
    )  # Colonne Cuisine
    MursHouseIV(
        0.05,
        loc2,
        0.05,
        (1 / 3) * (loc2 - loc1),
        hauteur * (3.75 / 3) / 2,
        hauteur * (3.75 / 3) / 2,
    )  # Colonne Dining
    MursHouseIV(
        0.05,
        loc3,
        0.05,
        loc3 + (1 / 3) * (loc4 - loc3),
        hauteur * (2.25 / 3) / 2,
        hauteur * (2.25 / 3) / 2,
    )  # Colonne Sym Dining
    MursHouseIV(
        0.05,
        loc3 + (2 / 3) * (loc4 - loc3),
        ((1 / 3) * (loc4 - loc3)) / 2,
        loc4 - ((1 / 3) * (loc4 - loc3)) / 2,
        hauteur * (2.25 / 3) / 2,
        hauteur * (2.25 / 3) / 2,
    )  # Mur Sym Dining
    MursHouseIV(
        0.05,
        loc3 + (2 / 3) * (loc4 - loc3),
        0.05,
        loc2,
        hauteur * (3.75 / 3) / 2,
        hauteur * (3.75 / 3) / 2,
    )  # Colonne Living

    MursHouseIV(
        0.05,
        loc2 + (2 / 3) * (loc3 - loc2),
        ((2 / 3) * (loc2 - loc1)) / 2,
        loc1 + ((2 / 3) * (loc2 - loc1)) / 2,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Bloc étage Volume avant
    MursHouseIV(
        ((2 / 3) * (loc3 - loc2) + (1 / 4) * (loc2 - loc1)) / 2,
        loc3
        - (1 / 3) * (loc3 - loc2)
        - ((2 / 3) * (loc3 - loc2) + (1 / 4) * (loc2 - loc1)) / 2,
        0.05,
        loc2 - 0.05 - (1 / 3) * (loc2 - loc1),
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Bloc étage Volume avant

    MursHouseIV(
        0.05,
        loc2 + (1 / 3) * (loc3 - loc2),
        ((2 / 3) * (loc4 - loc3)) / 2,
        loc4 - ((2 / 3) * (loc4 - loc3)) / 2,
        hauteur / 2,
        hauteur * (2.25 / 3) + hauteur / 2,
    )  # Bloc étage Volume arrière

    MursHouseIV(
        loc4 / 2,
        (loc4 / 2) - (1 / 3) * (loc4 - loc3),
        0.05,
        loc1,
        0.05,
        hauteur * (3.75 / 3) + hauteur - 0.05,
    )  # Volume 1  poutre
    MursHouseIV(
        (loc2 + (1 / 3) * (loc3 - loc2)) / 2,
        (loc2 + (1 / 3) * (loc3 - loc2)) / 2,
        0.05,
        loc1,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Volume 1
    MursHouseIV(
        (loc4 - loc3) * (2 / 3) / 2,
        loc3 + (loc4 - loc3) * (2 / 3) / 2,
        0.05,
        loc1,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Volume 1
    MursHouseIV(
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        0.05,
        loc1,
        hauteur * (3.75 / 3) / 2,
        hauteur * (3.75 / 3) / 2,
    )  # Volume 1
    MursHouseIV(
        (loc2 - loc1) / 2,
        (loc2 - loc1) / 2,
        0.05,
        loc1,
        hauteur * (3.75 / 3) / 2,
        hauteur * (3.75 / 3) / 2,
    )  # Volume 1
    MursHouseIV(
        0.05,
        loc3 - 0.05 + (2 / 3) * (loc4 - loc3),
        (2 / 3) * (loc2 - loc1) / 2,
        loc2 - (2 / 3) * (loc2 - loc1) / 2,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Volume 1
    MursHouseIV(
        0.05,
        -(1 / 3) * (loc4 - loc3),
        0.05 + (loc2 - loc1) / 2,
        0.05 + (loc2 - loc1) / 2,
        hauteur / 2,
        hauteur * (3.75 / 3) + hauteur / 2,
    )  # Volume 1  Mur façade
    MursHouseIV(
        loc4 / 2,
        (loc4 / 2) - (1 / 3) * (loc4 - loc3),
        0.05,
        loc2,
        0.05,
        hauteur * (3.75 / 3) + hauteur - 0.05,
    )  # Volume 1  poutre

    MursHouseIV(
        0.05,
        loc1 + 0.025 + (1 / 3) * (loc4 - loc3),
        ((2 / 3) * (loc4 - loc3)) / 2,
        loc3 + ((2 / 3) * (loc4 - loc3)) / 2,
        hauteur / 2,
        hauteur * (2.25 / 3) + hauteur / 2,
    )  # Volume 2  Façade
    MursHouseIV(
        -0.05 + (1 / 3) * (loc4 - loc3) / 2,
        loc4 + (1 / 3) * (loc4 - loc3) / 2,
        0.05,
        loc3,
        0.05,
        hauteur * (2.25 / 3) + hauteur - 0.05,
    )  # Volume 2 Poutre
    MursHouseIV(
        -0.05 + (1 / 3) * (loc4 - loc3) / 2,
        loc4 + (1 / 3) * (loc4 - loc3) / 2,
        0.05,
        loc4,
        0.05,
        hauteur * (2.25 / 3) + hauteur - 0.05,
    )  # Volume 2 Poutre
    MursHouseIV(
        0.05,
        loc4 - 0.075 + 0.025 + (1 / 3) * (loc4 - loc3),
        0.05 + (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        hauteur / 2,
        hauteur * (2.25 / 3) + hauteur / 2,
    )  # Volume 2  Façade


def ElementHouseIV():

    def Grille(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    def Facades(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    ### Colonnes

    Grille(0.05, loc1, 0.05, loc1, hauteur, hauteur)
    Grille(0.05, loc1, 0.05, loc2, hauteur, hauteur)
    Grille(0.05, loc1, 0.05, loc3, hauteur, hauteur)
    Grille(0.05, loc1, 0.05, loc4 - 0.05, hauteur, hauteur)
    Grille(0.05, loc2, 0.05, loc1, hauteur, hauteur)
    Grille(0.05, loc2, 0.05, loc4 - 0.05, hauteur, hauteur)
    Grille(0.05, loc3, 0.05, loc1, hauteur, hauteur)
    Grille(0.05, loc3, 0.05, loc4 - 0.05, hauteur, hauteur)
    Grille(0.05, loc4, 0.05, loc1, hauteur, hauteur)
    Grille(0.05, loc4, 0.05, loc2, hauteur, hauteur)
    Grille(0.05, loc4, 0.05, loc3, hauteur, hauteur)
    Grille(0.05, loc4, 0.05, loc4 - 0.05, hauteur, hauteur)

    ### Poutres

    # Grille(0.05,loc1,loc4/2,loc4/2,0.05,hauteur)
    Grille(0.05, loc1, loc4 / 2, loc4 / 2, 0.05, hauteur * (2.25 / 3))
    Grille(0.05, loc1, loc4 / 2, loc4 / 2, 0.05, hauteur * (3.75 / 3))
    Grille(0.05, loc1, loc4 / 2, loc4 / 2, 0.05, hauteur * 2)

    Grille(0.05, loc2, loc4 / 2, loc4 / 2, 0.05, hauteur * (2.25 / 3))
    Grille(0.05, loc2, loc4 / 2, loc4 / 2, 0.05, hauteur * (3.75 / 3))
    Grille(0.05, loc2, loc4 / 2, loc4 / 2, 0.05, hauteur * 2)

    Grille(0.05, loc3, loc4 / 2, loc4 / 2, 0.05, hauteur * (2.25 / 3))
    Grille(0.05, loc3, loc4 / 2, loc4 / 2, 0.05, hauteur * (3.75 / 3))
    Grille(0.05, loc3, loc4 / 2, loc4 / 2, 0.05, hauteur * 2)

    Grille(0.05, loc4, loc4 / 2, loc4 / 2, 0.05, hauteur * (2.25 / 3))
    Grille(0.05, loc4, loc4 / 2, loc4 / 2, 0.05, hauteur * (3.75 / 3))
    Grille(0.05, loc4, loc4 / 2, loc4 / 2, 0.05, hauteur * 2)

    Grille(loc4 / 2, loc4 / 2, 0.05, loc1, 0.05, hauteur * (2.25 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc1, 0.05, hauteur * (3.75 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc1, 0.05, hauteur * 2)

    Grille(loc4 / 2, loc4 / 2, 0.05, loc2, 0.05, hauteur * (2.25 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc2, 0.05, hauteur * (3.75 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc2, 0.05, hauteur * 2)

    Grille(loc4 / 2, loc4 / 2, 0.05, loc3, 0.05, hauteur * (2.25 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc3, 0.05, hauteur * (3.75 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc3, 0.05, hauteur * 2)

    Grille(loc4 / 2, loc4 / 2, 0.05, loc4 - 0.05, 0.05, hauteur * (2.25 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc4 - 0.05, 0.05, hauteur * (3.75 / 3))
    Grille(loc4 / 2, loc4 / 2, 0.05, loc4 - 0.05, 0.05, hauteur * 2)

    ### Façade avant

    Facades(
        0.05, -(1 / 3) * (loc4 - loc3), 0.05, (1 / 3) * (loc1 - loc2), hauteur, hauteur
    )  # Colonne 1 gauche
    Facades(
        0.05, loc1, 0.05, (1 / 3) * (loc1 - loc2), hauteur, hauteur
    )  # Colonne2 gauche
    Facades(
        (1 / 3) * (loc4 - loc3) / 2,
        loc1 - ((1 / 3) * (loc4 - loc3)) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2),
        (hauteur / 2) * (3.75 / 3),
        (hauteur / 2) * (3.75 / 3),
    )  # Mur Gauche
    Facades(
        0.05, loc4, 0.05, (1 / 3) * (loc1 - loc2), hauteur, hauteur
    )  # Colonne 2 droite
    Facades(
        0.05,
        loc4 - 0.05 + (1 / 3) * (loc4 - loc3),
        0.05,
        (1 / 3) * (loc1 - loc2),
        hauteur,
        hauteur,
    )  # Colonne 1 droite
    Facades(
        ((1 / 3) * (loc4 - loc3)) / 2,
        loc4 + (1 / 3) * (loc4 - loc3) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2),
        0.05 + (hauteur / 2) * (2.25 / 3),
        0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3),
    )  # Mur Droite haut
    Facades(
        (loc2 - loc1) / 2,
        (loc2 - loc1) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2) + 0.05,
        (hauteur / 2) * (2.25 / 3),
        (hauteur / 2) * (2.25 / 3),
    )  # Mur Bas entre loc1 et loc2
    Facades(
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2) - 0.055,
        (hauteur / 2) * (2.25 / 3) - 0.05,
        (hauteur / 2) * (2.25 / 3) - 0.05,
    )  # Mur Bas entre loc3 et loc4
    Facades(
        (loc3 - loc2) / 2,
        loc2 + (loc3 - loc2) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2),
        (hauteur / 2) * (2.25 / 3),
        (hauteur / 2) * (2.25 / 3),
    )  # Mur Bas entre loc2 et loc3
    Facades(
        0.05, loc2, 0.05, (1 / 3) * (loc1 - loc2), hauteur, hauteur
    )  # Colonne en loc 2
    Facades(
        0.05, loc3, 0.05, (1 / 3) * (loc1 - loc2), hauteur, hauteur
    )  # Colonne en loc 3
    Facades(
        0.05,
        loc3,
        (1 / 3) * (loc1 - loc2) / 2,
        (1 / 3) * (loc1 - loc2) / 2,
        hauteur,
        hauteur,
    )  # Colonne en loc 3 sur X
    Facades(
        loc4 / 2, loc4 / 2, 0.05, (1 / 3) * (loc1 - loc2), 0.1, hauteur * (2.25 / 3)
    )  # Poutre
    Facades(
        loc4 / 2, loc4 / 2, 0.05, (1 / 3) * (loc1 - loc2), 0.1, hauteur * (3.75 / 3)
    )  # Poutre
    Facades(
        loc4 / 2, loc4 / 2, 0.05, (1 / 3) * (loc1 - loc2), 0.1, hauteur * 2
    )  # Poutre
    Facades(
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2),
        0.2,
        hauteur * (3.75 / 3) - 0.2,
    )  # Poutre
    Facades(
        -0.05 + (loc2 - loc1) / 2,
        0.05 + (loc2 - loc1) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2) - 0.055,
        -0.05 + (hauteur / 2) * (2.25 / 3),
        -0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3),
    )  # Mur entre loc 1 et 2 haut
    Facades(
        (loc4 - loc3) / 2,
        0.05 + loc3 + (loc4 - loc3) / 2,
        0.05,
        (1 / 3) * (loc1 - loc2),
        -0.05 + (hauteur / 2) * (2.25 / 3) / 2,
        -0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3) / 2,
    )  # Mur entre loc 3 et 4 haut

    ### Façade arrière

    Facades(
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        hauteur,
        hauteur,
    )  # Colonne 1 gauche
    Facades(
        0.05, loc4, 0.05, loc4 + (1 / 3) * (loc4 - loc3), hauteur, hauteur
    )  # Colonne2 gauche
    Facades(
        (1 / 3) * (loc4 - loc3) / 2,
        loc4 + ((1 / 3) * (loc4 - loc3)) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        (hauteur / 2) * (3.75 / 3),
        (hauteur / 2) * (3.75 / 3),
    )  # Mur Gauche
    Facades(
        0.05, loc1, 0.05, loc4 + (1 / 3) * (loc4 - loc3), hauteur, hauteur
    )  # Colonne 2 droite
    Facades(
        0.05,
        loc1 + 0.05 - (1 / 3) * (loc2 - loc1),
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        hauteur,
        hauteur,
    )  # Colonne 1 droite
    Facades(
        ((1 / 3) * (loc2 - loc1)) / 2,
        loc1 - (1 / 3) * (loc2 - loc1) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        0.05 + (hauteur / 2) * (2.25 / 3),
        0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3),
    )  # Mur Droite haut
    Facades(
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3) + 0.05,
        (hauteur / 2) * (2.25 / 3),
        (hauteur / 2) * (2.25 / 3),
    )  # Mur Bas entre loc3 et loc4
    Facades(
        (loc2 - loc1) / 2,
        loc1 + (loc2 - loc1) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3) - 0.055,
        (hauteur / 2) * (2.25 / 3) - 0.05,
        (hauteur / 2) * (2.25 / 3) - 0.05,
    )  # Mur Bas entre loc1 et loc2
    Facades(
        (loc3 - loc2) / 2,
        loc2 + (loc3 - loc2) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        (hauteur / 2) * (2.25 / 3),
        (hauteur / 2) * (2.25 / 3),
    )  # Mur Bas entre loc2 et loc3
    Facades(
        0.05, loc2, 0.05, loc4 + (1 / 3) * (loc4 - loc3), hauteur, hauteur
    )  # Colonne en loc 2
    Facades(
        0.05, loc3, 0.05, loc4 + (1 / 3) * (loc4 - loc3), hauteur, hauteur
    )  # Colonne en loc 3
    Facades(
        0.05,
        loc2,
        (1 / 3) * (loc4 - loc3) / 2,
        loc4 + (1 / 3) * (loc4 - loc3) / 2,
        hauteur,
        hauteur,
    )  # Mur en loc 1 sur X
    Facades(
        loc4 / 2,
        loc4 / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        0.1,
        hauteur * (2.25 / 3),
    )  # Poutre
    Facades(
        loc4 / 2,
        loc4 / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        0.1,
        hauteur * (3.75 / 3),
    )  # Poutre
    Facades(
        loc4 / 2, loc4 / 2, 0.05, loc4 + (1 / 3) * (loc4 - loc3), 0.1, hauteur * 2
    )  # Poutre
    Facades(
        (loc2 - loc1) / 2,
        loc1 + (loc2 - loc1) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        0.2,
        hauteur * (3.75 / 3) - 0.2,
    )  # Poutre
    Facades(
        -0.05 + (loc4 - loc3) / 2,
        loc3 + 0.05 + (loc4 - loc3) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3) - 0.055,
        -0.05 + (hauteur / 2) * (2.25 / 3),
        -0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3),
    )  # Mur entre loc 1 et 2 haut
    Facades(
        (loc2 - loc1) / 2,
        0.05 + loc1 + (loc2 - loc1) / 2,
        0.05,
        loc4 + (1 / 3) * (loc4 - loc3),
        -0.05 + (hauteur / 2) * (2.25 / 3) / 2,
        -0.05 + hauteur * (3.75 / 3) + (hauteur / 2) * (2.25 / 3) / 2,
    )  # Mur entre loc 3 et 4 haut


###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###
###                                           HOUSE VI                                    ###
###---------------------------------------------------------------------------------------###
###---------------------------------------------------------------------------------------###


def HouseVI():

    def MursHouseVI(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    MursHouseVI(
        0.05,
        loc3 + (loc4 - loc3) * (1 / 4),
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur,
        hauteur,
    )  # Mur 9 sur plan
    MursHouseVI(
        0.05,
        loc3 + (loc4 - loc3) * (1 / 4),
        ((loc4 - loc3) * (1 / 3) - (loc4 - loc3) * (1 / 6)) / 2,
        loc4 + 0.05 - ((loc4 - loc3) * (1 / 3) + (loc4 - loc3) * (1 / 6)) / 2,
        hauteur,
        hauteur,
    )  # Mur 10 sur plan
    MursHouseVI(
        0.05, loc3 + (loc4 - loc3) * (1 / 4), 0.15, loc4 - 0.15, hauteur, hauteur
    )  # Mur 11 sur plan

    MursHouseVI(
        0.05,
        loc4 + (loc4 - loc3) * (1 / 4),
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur / 2,
        hauteur / 2,
    )  # Mur 16 sur plan
    MursHouseVI(
        0.05,
        loc4 + (loc4 - loc3) * (1 / 4),
        ((loc4 - loc3) * (1 / 3) - (loc4 - loc3) * (1 / 6)) / 2,
        loc4 + 0.05 - ((loc4 - loc3) * (1 / 3) + (loc4 - loc3) * (1 / 6)) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 17 sur plan
    MursHouseVI(
        0.05,
        loc4 + (loc4 - loc3) * (1 / 4),
        0.15,
        loc4 - 0.15,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 18 sur plan
    MursHouseVI(
        0.05,
        loc4,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur / 2,
        hauteur * 1.5,
    )  # Mur 27 sur plan
    MursHouseVI(
        0.05,
        loc4,
        ((loc4 - loc3) * (1 / 3) - (loc4 - loc3) * (1 / 6)) / 2,
        loc4 + 0.05 - ((loc4 - loc3) * (1 / 3) + (loc4 - loc3) * (1 / 6)) / 2,
        hauteur / 2,
        hauteur * 1.5,
    )  # Mur 28 sur plan
    MursHouseVI(
        0.05, loc4, 0.15, loc4 - 0.15, hauteur / 2, hauteur * 1.5
    )  # Mur 29 sur plan
    MursHouseVI(
        (loc4 - loc3) * (1 / 4) / 2,
        loc4 + (loc4 - loc3) * (1 / 4) / 2,
        0.05,
        loc3 + (loc4 - loc3) * (1 / 3),
        hauteur / 2,
        hauteur / 2,
    )  # Mur 15 sur plan

    MursHouseVI(
        (loc4 - loc3) * (1 / 4) / 2,
        loc4 - (loc4 - loc3) * (1 / 4) / 2,
        0.05,
        loc4 - 0.05,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 14 sur plan
    MursHouseVI(
        ((loc4 - loc3) - 0.05) / 2 / 2,
        loc3 - 0.05 + (loc4 - loc3) / 2 / 2,
        0.05,
        loc4 + (loc4 - loc3) * (1 / 3) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 12 sur plan
    MursHouseVI(
        ((loc4 - loc3) - 0.05) / 4 / 2,
        loc3 + ((loc4 - loc3) + 0.05) / 2 + (loc4 - loc3) / 4 / 2,
        0.05,
        loc4 + (loc4 - loc3) * (1 / 3) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 13 sur plan

    MursHouseVI(
        0.05,
        loc1,
        ((loc3 - loc2) + (loc4 - loc3) * (1 / 3)) / 2,
        loc2 + ((loc3 - loc2) + (loc4 - loc3) * (1 / 3)) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 5 sur plan
    MursHouseVI(
        0.05,
        loc1,
        (((loc4 - loc3) * (1 / 3)) / 2) / 2,
        loc3 + ((loc4 - loc3) * (1 / 3)) + ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 6 sur plan

    MursHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125,
        loc3 - 0.125 + ((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2,
        0.05,
        loc1,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 1 sur plan
    MursHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) / 2,
        loc4
        + 0.05
        - 0.2
        - (loc4 - loc2) / 2
        + (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) / 2,
        0.05,
        loc1,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 2 sur plan
    MursHouseVI(
        0.05,
        loc4 + 0.05 - (loc4 - loc3) * (1 / 4),
        0.05,
        loc1,
        hauteur / 2,
        hauteur / 2,
    )  # Mur 3 sur plan

    MursHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.025,
        loc3 - 0.125 + 0.025 + ((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2,
        0.05,
        loc1 - loc2,
        hauteur / 2,
        hauteur * 1.5,
    )  # Mur 19 sur plan
    MursHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) / 2,
        loc4
        + 0.05
        - 0.2
        - (loc4 - loc2) / 2
        + (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) / 2,
        0.05,
        loc1 - loc2,
        hauteur / 2,
        hauteur * 1.5,
    )  # Mur 20 sur plan
    # MursHouseVI(0.05,loc4+0.05-(loc4-loc3)*(1/4),0.05,loc1-loc2,hauteur/2,hauteur*1.5)   #Mur 21 sur plan

    MursHouseVI(
        0.05, loc3 + 0.1, loc2 / 2, (loc1 - loc2) / 2, hauteur / 2, hauteur * 1.5
    )

    def SolHouseVI(dim_X, pos_X, dim_Y, pos_Y, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, 0.05))

    SolHouseVI(
        0.05 + (loc4 - loc3) * (1 / 4) / 2,
        loc4 + (loc4 - loc3) * (1 / 4) / 2,
        0.025 + (loc4 - loc3) * (1 / 3),
        loc4 - 0.025 - (loc4 - loc3) * (1 / 3),
        -0.05,
    )  # Plancher Rez zone Murs 15, 16, 17...

    SolHouseVI(
        0.05 + (loc4 - loc3) * (1 / 4) / 2,
        loc4 + (loc4 - loc3) * (1 / 4) / 2,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur,
    )  # Toit rez Murs 15 - 16
    SolHouseVI(
        0.05 + (loc4 - loc3) * (1 / 4) / 2,
        loc4 + (loc4 - loc3) * (1 / 4) / 2,
        -0.025 + (loc4 - loc3) * (1 / 3) / 2,
        loc4 + 0.025 - (loc4 - loc3) * (1 / 3) / 2,
        hauteur,
    )  # Toit rez Murs 17 - 18
    SolHouseVI(
        0.025 + (loc2 - loc1) / 2,
        loc1 - 0.025 + (loc2 - loc1) / 2,
        ((loc3 - loc2) + (loc4 - loc3) * (1 / 3)) / 2,
        loc2 + ((loc3 - loc2) + (loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
    )  # Toit rez Mur 5
    SolHouseVI(
        0.025 + (loc2 - loc1) / 2,
        loc1 - 0.025 + (loc2 - loc1) / 2,
        (((loc4 - loc3) * (1 / 3)) / 2) / 2,
        loc3 + ((loc4 - loc3) * (1 / 3)) + ((loc4 - loc3) * (1 / 3)) / 2,
        hauteur,
    )  # Toit rez Mur 6
    SolHouseVI(
        ((loc4 - loc3) - 0.05) / 2 / 2,
        loc3 - 0.05 + (loc4 - loc3) / 2 / 2,
        0.025 + ((loc4 - loc3) * (1 / 3) / 2) / 2,
        loc4 + 0.025 + ((loc4 - loc3) * (1 / 3) / 2) / 2,
        hauteur,
    )  # Toit rez Mur 12
    SolHouseVI(
        ((loc4 - loc3) - 0.05) / 4 / 2,
        loc3 + ((loc4 - loc3) + 0.05) / 2 + (loc4 - loc3) / 4 / 2,
        0.025 + ((loc4 - loc3) * (1 / 3) / 2) / 2,
        loc4 + 0.025 + ((loc4 - loc3) * (1 / 3) / 2) / 2,
        hauteur,
    )  # Toit rez Mur 13
    SolHouseVI(
        0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        loc3 + 0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        0.05 + (loc2 - loc1) / 2,
        loc2 - (loc2 - loc1) / 2,
        hauteur,
    )  # Plancher étage sur entrée
    SolHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02,
        loc3 - 0.125 + 0.05 - 0.025 + ((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2,
        (loc1 + loc2) / 2,
        (loc1 - loc2) / 2,
        hauteur,
    )  # Plancher Mur 19
    SolHouseVI(
        -0.025
        + ((loc4 - loc3) * (3 / 4)) / 2
        - ((((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02),
        loc4
        + 0.05
        - 0.2
        - (loc4 - loc2) / 2
        + (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2)
        + 0.125,
        (loc1 + loc2) / 2,
        (loc1 - loc2) / 2,
        hauteur,
    )  # Plancher Mur 20
    SolHouseVI(
        0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        loc3 + 0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        (loc3 - loc2) / 2,
        loc3 - (loc3 - loc2) / 2,
        hauteur,
    )  # Entrée
    SolHouseVI(
        0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        loc3 + 0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 + (loc4 - loc3) * (1 / 3) / 2,
        hauteur,
    )  # Plancher Parois loc3 sur Y et mur 9
    SolHouseVI(
        0.025 + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        loc3
        - 0.025
        + (loc4 - loc3) * (1 / 4)
        + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        -0.025 + (loc4 - loc3) * (1 / 3) / 2,
        loc4 + 0.025 - (loc4 - loc3) * (1 / 3) / 2,
        hauteur,
    )  # Plancher murs 10 - 17
    SolHouseVI(
        0.025 + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        loc3
        - 0.025
        + (loc4 - loc3) * (1 / 4)
        + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur,
    )  # Plancher Murs 9 - 16

    SolHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02,
        loc3 - 0.125 + 0.05 - 0.025 + ((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2,
        (loc2 / 2) / 2 + (loc1 + loc2) / 2,
        (loc2 / 2) / 2 + (loc1 - loc2) / 2,
        hauteur * 2 - 0.05,
    )  # Toit Mur 19
    SolHouseVI(
        -0.025
        + ((loc4 - loc3) * (3 / 4)) / 2
        - ((((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02),
        loc4
        + 0.05
        - 0.2
        - (loc4 - loc2) / 2
        + (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2)
        + 0.125,
        (loc2 / 2) / 2 + (loc1 + loc2) / 2,
        (loc2 / 2) / 2 + (loc1 - loc2) / 2,
        hauteur * 2 - 0.05,
    )  # Toit Mur 20
    SolHouseVI(
        0.025 + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        loc3
        - 0.025
        + (loc4 - loc3) * (1 / 4)
        + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        -0.025 + (loc4 - loc3) * (1 / 3) / 2,
        loc4 + 0.025 - (loc4 - loc3) * (1 / 3) / 2,
        hauteur * 2 - 0.05,
    )  # Plancher murs 10 - 17
    SolHouseVI(
        0.025 + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        loc3
        - 0.025
        + (loc4 - loc3) * (1 / 4)
        + ((loc4 - loc3) - (loc4 - loc3) * (1 / 4)) / 2,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 - 0.05 + (loc4 - loc3) * (1 / 3) / 2 + (loc4 - loc3) * (1 / 3),
        hauteur * 2 - 0.05,
    )  # Plancher Murs 9 - 16
    SolHouseVI(
        0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        loc3 + 0.05 + ((loc4 - loc3) * (3 / 4)) / 2,
        (loc4 - loc3) * (1 / 3) / 2,
        loc3 + (loc4 - loc3) * (1 / 3) / 2,
        hauteur * 2 - 0.05,
    )  # Toit Parois loc3 sur Y et mur 9
    SolHouseVI(
        (loc3 - loc2) / 2,
        loc2 + (loc3 - loc2) / 2,
        (loc4 - loc2) / 2,
        loc2 + (loc4 - loc2) / 2,
        hauteur * 2 - 0.05,
    )
    SolHouseVI(
        (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02,
        loc3 - 0.125 + 0.05 - 0.025 + ((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2,
        -0.05 + (loc3 - loc2) / 2,
        loc2 - 0.05 + (loc3 - loc2) / 2,
        hauteur * 2 - 0.05,
    )
    SolHouseVI(
        -0.025
        + ((loc4 - loc3) * (3 / 4)) / 2
        - ((((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2) - 0.125 - 0.005 - 0.02),
        loc4
        + 0.05
        - 0.2
        - (loc4 - loc2) / 2
        + (((loc4 - loc2) / 2) / 2 - (loc3 - loc2) / 2)
        + 0.125,
        -0.05 + (loc3 - loc2) / 2,
        loc2 - 0.05 + (loc3 - loc2) / 2,
        hauteur * 2 - 0.05,
    )


def ElementHouseVI():

    def CouplesLames(dim_X, pos_X, dim_Y, pos_Y, dim_Z, pos_Z):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
        bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))

    ### Lame 1 position 2 sur X
    CouplesLames(
        0.05, loc2, (loc2 * 2) / 2, loc1, hauteur / 2 + 0.25, hauteur / 2 + 0.25
    )
    # CouplesLames(0.05,loc2,(loc4-loc3)*(1/3)/2,loc3+(loc4-loc3)*(1/3)/1.75,hauteur/2,hauteur/2)
    CouplesLames(
        0.05,
        loc2,
        (loc4 - loc3) * (1 / 4) / 2,
        loc4 - (loc4 - loc3) * (2 / 3) / 2,
        hauteur / 2 + 0.25,
        hauteur / 2 + 0.25,
    )
    CouplesLames(
        0.05,
        loc2,
        (loc3 - loc2) / 2,
        loc2 + (loc3 - loc2) / 2,
        hauteur / 2 + 0.25,
        hauteur / 2 + 0.25,
    )
    CouplesLames(
        0.05,
        loc2,
        (2 / 3) * (loc4 - loc3) / 2,
        loc3 + (2 / 3) * (loc4 - loc3) / 2,
        hauteur / 2 + 0.25,
        hauteur / 2 + 0.25,
    )
    CouplesLames(
        0.1,
        loc2 - 0.05,
        0.05,
        loc1 + 0.05 - loc2,
        hauteur / 2 - 0.25,
        hauteur * 1.5 + 0.25,
    )  # Colonne
    CouplesLames(
        0.1, loc2 - 0.05, 0.05, loc2, hauteur / 2 - 0.25, hauteur * 1.5 + 0.25
    )  # Colonne
    CouplesLames(
        0.1,
        loc2 - 0.05,
        0.05,
        loc3 - 0.05 + (1 / 3) * (loc4 - loc3),
        hauteur / 2 - 0.25,
        hauteur * 1.5 + 0.25,
    )  # Colonne
    CouplesLames(
        0.1,
        loc2 - 0.05,
        0.05,
        loc3 - 0.05 + ((loc4 - loc3) * (2 / 3)) + (loc4 - loc3) * (1 / 4) / 2,
        hauteur / 2 - 0.25,
        hauteur * 1.5 + 0.25,
    )  # Colonne

    ###Lame 2 position 3 sur X
    CouplesLames(
        0.05,
        loc3,
        (loc4 - loc3) * (1 / 3) / 2,
        loc4 + (loc4 - loc3) * (1 / 3) / 2,
        hauteur + (hauteur / 3) - 0.05,
        hauteur + (hauteur / 3) - 0.05,
    )
    CouplesLames(
        0.05,
        loc3,
        (loc4 - loc3) / 2,
        loc3 + (loc4 - loc3) / 2,
        hauteur / 3,
        hauteur * 2 + hauteur / 3 - 0.1,
    )  # Partie haute
    CouplesLames(
        0.05,
        loc3,
        (loc3 - loc2) / 2,
        loc2 + (loc3 - loc2) / 2,
        0.05,
        (hauteur * 2) - 0.05,
    )  # Poutre entre pos3 et pos2
    CouplesLames(0.05, loc3, loc2 / 2, loc2 / 2, hauteur, hauteur)

    CouplesLames(
        0.1,
        loc3 + 0.05,
        0.05,
        loc4 - 0.05 + (loc4 - loc3) * (1 / 3),
        hauteur + (hauteur / 3) - 0.05,
        hauteur + (hauteur / 3) - 0.05,
    )  # Colonne
    CouplesLames(
        0.1,
        loc3 + 0.05,
        0.05,
        loc4 - (loc4 - loc3) * (1 / 3),
        hauteur + (hauteur / 3) - 0.05,
        hauteur + (hauteur / 3) - 0.05,
    )  # Colonne
    CouplesLames(
        0.1, loc3 + 0.05, 0.05, loc3, hauteur / 3, hauteur * 2 + hauteur / 3 - 0.1
    )  # Colonne
    CouplesLames(
        0.1, loc3 + 0.05, 0.05, loc2, hauteur / 3, hauteur * 2 + hauteur / 3 - 0.1
    )  # Colonne
    CouplesLames(
        0.1,
        loc3 + 0.05,
        0.05,
        loc1,
        hauteur + (hauteur / 3) - 0.05,
        hauteur + (hauteur / 3) - 0.05,
    )  # Colonne

    CouplesLames(
        0.05,
        loc3 + 0.1,
        (loc3 - loc1) / 2,
        (loc3 - loc1) / 2,
        hauteur / 3,
        hauteur * 2 + hauteur / 3 - 0.1,
    )  # Doublage partie haute
    # CouplesLames(0.05,loc3+0.1,loc2/2,loc2/2,hauteur,hauteur)                                                                #Doublage mur

    ###Lame 1 position 2 sur X
    CouplesLames(
        0.05 + loc2 / 2,
        loc1 + 0.05 - loc2 / 2,
        0.05,
        loc2,
        hauteur + (hauteur / 3),
        hauteur - (hauteur / 3),
    )
    CouplesLames(loc4 / 2, loc4 / 2, 0.05, loc2, (hauteur / 3), -(hauteur / 3))
    CouplesLames(
        (loc4 - loc3) * (1 / 4) / 2,
        loc4 - 0.05 + 0.2 - (loc4 - loc3) * (1 / 4) / 2,
        0.05,
        loc2,
        hauteur + (hauteur / 3),
        hauteur - (hauteur / 3),
    )
    CouplesLames(
        0.05,
        loc1 - loc2 + 0.05,
        0.1,
        loc2 - 0.05,
        hauteur + (hauteur / 3),
        hauteur - (hauteur / 3),
    )  # Doublage Colonne
    CouplesLames(
        0.05, loc2, 0.1, loc2 - 0.05, hauteur + (hauteur / 3), hauteur - (hauteur / 3)
    )  # Doublage Colonne
    CouplesLames(
        0.05,
        loc4 + 0.2,
        0.1,
        loc2 - 0.05,
        hauteur + (hauteur / 3),
        hauteur - (hauteur / 3),
    )  # Doublage Colonne
    CouplesLames(
        0.05,
        loc4 - 0.2 - (loc4 - loc2) / 2,
        0.1,
        loc2 - 0.05,
        hauteur + (hauteur / 3),
        hauteur - (hauteur / 3),
    )  # Doublage Colonne

    ###Lame 2 position 3 sur X
    CouplesLames(
        loc4 / 2,
        loc4 / 2,
        0.05,
        loc3,
        (hauteur / 2) + 0.25,
        hauteur * 2 - hauteur / 2 - 0.1 - 0.25,
    )
    CouplesLames(0.05, loc1, 0.1, loc3 + 0.05, hauteur / 2 - 0.3, hauteur / 2 - 0.3)
    CouplesLames(0.05, loc2, 0.1, loc3 + 0.05, hauteur / 2 - 0.3, hauteur / 2 - 0.3)
    CouplesLames(
        0.05,
        loc3 + (loc4 - loc3) / 2,
        0.1,
        loc3 + 0.05,
        hauteur / 2 - 0.3,
        hauteur / 2 - 0.3,
    )
    CouplesLames(
        0.05, loc4 - 0.05, 0.1, loc3 + 0.05, hauteur / 2 - 0.3, hauteur / 2 - 0.3
    )


m = random.randint(1, 4)

if m == 1:
    print("House I")
    HouseI()
    ElementHouseI()
elif m == 2:
    print("House II")
    HouseII()
    ElementHouseII()
elif m == 3:
    print("House IV")
    HouseIV()
    ElementHouseIV()
else:
    print("House VI")
    HouseVI()
    ElementHouseVI()


d = random.randint(0, 5)
e = 0
while e <= d:
    n = random.randint(1, 4)
    if n == 1:
        print("Element House I")
        ElementHouseI()
    elif n == 2:
        print("Element House II")
        ElementHouseII()
    elif n == 3:
        print("Element House IV")
        ElementHouseIV()
    else:
        print("Element House VI")
        ElementHouseVI()
    e = e + 1
