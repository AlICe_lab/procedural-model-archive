# SPDX-FileCopyrightText: 2016 Benjamin Marc
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
from bpy import context
import random
from random import randint
import math
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# Definition de la fonction de création de camera en projection parallele
def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 15
    if projection == "axonometrique":

        monAxoCam.name = "axoTrimetrie"
        monAxoCam.rotation_euler = (radians(67), 0.0, radians(34))
        monAxoCam.location = (8.59, -12.734, 6.52)
        maScene.pixel_aspect_x = 1


axoCam("axonometrique", "trimetrie")


# définition de la fonction copier/coller
def CopyPaste(Forme1, cube1):

    bpy.context.scene.objects.active = bpy.data.objects[Forme1]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(cube1, obj.data)
    context.scene.objects.link(Copy)

    return Copy


pointBaseX = 0
pointBaseY = 0

# création des listes
listecubes = []
listecoins = []
listefaces = []
listefacesnorth = []
listefacessouth = []
listefaceseast = []
listefaceswest = []
listesol = []
listepiliers = []

# listes nettoyées
liste_cubes_clean = []
liste_coins_clean = []
liste_faces_clean = []
listesol_clean = []


# listes pour essai nettoyage
listefacesnorthsup = []
listefacessouthsup = []
listefaceseastsup = []
listefaceswestsup = []

for i in range(0, 2):
    if len(listecubes) == 0:

        listecubes.append((pointBaseX, pointBaseY, 0))

    else:
        for point in listecubes:
            proba = 2
            x, y, z = point

            # nombre de cubes générés
            if len(listecubes) < 20:

                # développement horizontal
                construiraConstruiraPas = randint(1, proba)
                if construiraConstruiraPas != 1:
                    listecubes.append((x - 1, y, z))
                construiraConstruiraPas = randint(1, proba)
                if construiraConstruiraPas != 1:
                    listecubes.append((x + 1, y, z))
                construiraConstruiraPas = randint(1, proba)
                if construiraConstruiraPas != 1:
                    listecubes.append((x, y - 1, z))
                construiraConstruiraPas = randint(1, proba)
                if construiraConstruiraPas != 1:
                    listecubes.append((x, y + 1, z))
                # développement inférieur
                """construiraConstruiraPas = randint(1,proba)
                    if construiraConstruiraPas != 1:
                    listecubes.append ((x,y,z-1))"""
                # développement supérieur
                construiraConstruiraPas = randint(1, proba)
                if construiraConstruiraPas != 1:
                    listecubes.append((x, y, z + 1))

liste_cubes_clean = set(listecubes)

for point in liste_cubes_clean:
    x, y, z = point
    listecubes.append(CopyPaste("monCube", "monCubeCopie"))
    listecubes[-1].location = [x, y, z]

    # ajout des données dans les listes
    listefacesnorth.append((x, y + 0.5, z))
    listefacessouth.append((x, y - 0.5, z))
    listefaceseast.append((x - 0.5, y, z))
    listefaceswest.append((x + 0.5, y, z))
    listesol.append((x, y, z + 0.5))
    listesol.append((x, y, z - 0.5))
    listecoins.append((x - 0.5, y - 0.5, z - 0.5))
    listecoins.append((x - 0.5, y - 0.5, z + 0.5))
    listecoins.append((x - 0.5, y + 0.5, z - 0.5))
    listecoins.append((x - 0.5, y + 0.5, z + 0.5))
    listecoins.append((x + 0.5, y - 0.5, z - 0.5))
    listecoins.append((x + 0.5, y - 0.5, z + 0.5))
    listecoins.append((x + 0.5, y + 0.5, z - 0.5))
    listecoins.append((x + 0.5, y + 0.5, z + 0.5))

    # Essai nettoyage capsules indésirables
    # Eviter la copie des capsule sur les faces à l'intérieur
    ##liste faces nord
    listefacesnorthsup.append((x, y + 0.5, z))
    listefacesnorthsup.append((x, y - 0.5, z))
    listefacesnorthsup.append((x, y - 0.5, z))
    listecleannorth = set(listefacesnorthsup)

    ##liste faces sud
    listefacessouthsup.append((x, y - 0.5, z))
    listefacessouthsup.append((x, y + 0.5, z))
    listefacessouthsup.append((x, y + 0.5, z))
    listecleansouth = set(listefacessouthsup)

    # retour dans Terminal pour comparaison
print("faces nord : ", len(listefacesnorth))
print("faces nord sup : ", len(listefacesnorthsup))
print("faces nord après nettoyage : ", len(listecleannorth))
"""
    ##liste face est           
    listefaceseastsup.append((x+0.5,y,z))   
    listefaceseastsup.append((x-0.5,y,z)),
    listefaceseastsup.append((x-0.5,y,z))     

    ##liste face ouest
    listefaceswestsup.append((x-0.5,y,z))
    listefaceswestsup.append((x+0.5,y,z)),
    listefaceswestsup.append((x+0.5,y,z))
   
    #liste faces horizontales
    
    listefacestop.append((x,y,z+0.5))      
    listefaceslow.append((x,y,z-0.5))

    listesol.append((x,y,z+0.5))      
    listesol.append((x,y,z-0.5))
    
    listefaces.append((x,y+0.5,z))
    listefaces.append((x,y-0.5,z))
    listefaces.append((x+0.5,y,z))    
    listefaces.append((x-0.5,y,z))    
    listefaces.append((x,y,z+0.5))    
    listefaces.append((x,y,z-0.5))
 """

"""    
print('')
print('nombre de faces : ')    
print(len(listefaces))

if liste_coins_clean = 1
    listepiliers.append((x,y,-1))

    listecollision.append((x+1,y,z))
    listecollision.append((x-1,y,z))
    listecollision.append((x,y+1,z))
    listecollision.append((x,y-1,z))
"""


# nettoyage des doublons dans les liste
liste_cubes_clean = set(listecubes)
liste_coins_clean = set(listecoins)
liste_north_clean = set(listefacesnorthsup)
liste_south_clean = set(listefacessouthsup)
liste_east_clean = set(listefaceseastsup)
liste_west_clean = set(listefaceswestsup)
listesol_clean = set(listesol)


# retour numérique dans le terminal
print("")
print("nombre de cubes avant nettoyage :", len(listecubes))
print("nombre de cubes après nettoyage :", len(liste_cubes_clean))
# print('coordonnées des cubes :',liste_cubes_clean)
print("")
print("nombre de coins avant nettoyage", len(listecoins))
print("nombre de coins après nettoyage", len(liste_coins_clean))
# print('coordonnées des coins :',liste_coins_clean)
print("")
print("nombre faces avant nettoyage", len(listefaces))
print("nombre des faces après nettoyage", len(liste_faces_clean))
# print('coordonnées des faces : ',liste_faces_clean)
print("")
print("________________________________________")


# fonction copier coller#
##Ajout des objets sur coordonnées

# Croix Structurelle
for point in liste_coins_clean:
    x, y, z = point
    listecoins.append(CopyPaste("module", "moduleCopie"))
    listecoins[-1].location = [x, y, z]

# Toit/Sol
for point in listesol_clean:
    x, y, z = point
    listesol.append(CopyPaste("Sol", "SolCopie"))
    listesol[-1].location = [x, y, z + 0.05]

# Modules sur côté Est
for point in liste_east_clean:
    x, y, z = point
    listefaceseast.append(CopyPaste("Capsuleeast", "capsuleeastcopie"))
    listefaceseast[-1].location = [x + 0.25, y, z]

# Modules sur côté Ouest
for point in liste_west_clean:
    x, y, z = point
    listefaceswest.append(CopyPaste("Capsulewest", "capsulewestcopie"))
    listefaceswest[-1].location = [x - 0.25, y, z]

# Modules sur côté Nord
for point in liste_north_clean:
    x, y, z = point
    listefacesnorth.append(CopyPaste("Capsulenorth", "capsulenorthcopie"))
    listecoins[-1].location = [x, y + 0.25, z]

# Modules sur côté Sud
for point in liste_south_clean:
    x, y, z = point
    listefacessouth.append(CopyPaste("Capsulesouth", "capsulesouthcopie"))
    listefacessouth[-1].location = [x, y - 0.25, z]

# Piliers au sol
for point in liste_coins_clean:
    # if liste_coins_clean = 1
    x, y, z = point
    listecoins.append(CopyPaste("pilier", "piliercopie"))
    listecoins[-1].location = [x, y, -1.5]
