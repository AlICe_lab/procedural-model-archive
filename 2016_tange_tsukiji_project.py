# SPDX-FileCopyrightText: 2016 Robin Dufayard
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

####### Importations des fonctions ########
import bpy
import random
from random import randint
import math
from math import radians
from math import cos
from math import sin
from mathutils import Vector

####### Preparation plateau ########

bpy.ops.object.select_all(action="SELECT")
"""bpy.data.objects["Camera"].select = False
bpy.data.objects["Sun"].select = False"""
bpy.data.objects["batigrand"].select = False
bpy.data.objects["batimoyen"].select = False
bpy.data.objects["batipetit"].select = False
bpy.data.objects["bloctransversalh16"].select = False
bpy.data.objects["bloctransversalh8"].select = False
bpy.ops.object.delete(use_global=False)
print("---------------start----------------")


####### Definition des fonctions ########


def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 470
    monAxoCam.data.clip_start = 0
    monAxoCam.data.clip_end = 2000
    if projection == "axonometrique":
        if canon == "isometrie":  # OK
            monAxoCam.name = "axoIsometrie"
            monAxoCam.rotation_euler = (radians(54.74), 0.0, radians(45))
            monAxoCam.location = (
                (largeur * 8 / 2) + 200,
                (longueur * 15 / 2) - 200,
                180,
            )
            maScene.pixel_aspect_x = 1
            bpy.data.objects["axoIsometrie"].select = False


def boite(coord, nom):
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(coord[0] * trameX, coord[1] * trameY, coord[2])
    )
    # bpy.ops.transform.translate(value=((i // largeur)*trameX,(i % largeur*trameY),0))
    bpy.context.object.name = nom


def noyausecondaire(coord, taille):
    boite(
        (
            coord[0] - 5 / trameX,
            coord[1],
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  1"),
    )
    bpy.ops.transform.resize(value=(0.2, 2, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(-5,0,(hnoyausecondaire*taille/3/2)))
    boite(
        (
            coord[0] + 5 / trameX,
            coord[1],
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  2"),
    )
    bpy.ops.transform.resize(value=(0.2, 2, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(5,0,(hnoyausecondaire*taille/3/2)))
    boite(
        (
            coord[0],
            coord[1] - 2.5 / trameY,
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  3"),
    )
    bpy.ops.transform.resize(value=(6.8, 0.2, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(0,-2.5,(hnoyausecondaire*taille/3/2)))
    boite(
        (
            coord[0],
            coord[1] + 2.5 / trameY,
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  4"),
    )
    bpy.ops.transform.resize(value=(6.8, 0.2, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(0,2.5,(hnoyausecondaire*taille/3/2)))
    boite(
        (
            coord[0] - 3.3 / trameX,
            coord[1],
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  5"),
    )
    bpy.ops.transform.resize(value=(0.2, 4.8, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(-3.3,0,(hnoyausecondaire*taille/3/2)))
    boite(
        (
            coord[0] + 3.3 / trameX,
            coord[1],
            coord[2] + (hnoyausecondaire * taille / 3 / 2),
        ),
        ("noyau simple x=" + str(coord[0]) + " y=" + str(coord[1]) + "  6"),
    )
    bpy.ops.transform.resize(value=(0.2, 4.8, hnoyausecondaire * taille / 3))
    # bpy.ops.transform.translate(value=(3.3,0,(hnoyausecondaire/2)))


def noyauprincipal(coord, taille):
    boite(
        (
            coord[0] - 5 / trameX,
            coord[1],
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  1"),
    )
    bpy.ops.transform.resize(value=(0.2, 2, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(-5,0,(hnoyauprincipal*taille/3/2)))
    boite(
        (
            coord[0] + 5 / trameX,
            coord[1],
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  2"),
    )
    bpy.ops.transform.resize(value=(0.2, 2, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(5,0,(hnoyauprincipal*taille/3/2)))
    boite(
        (
            coord[0],
            coord[1] - 2.5 / trameY,
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  3"),
    )
    bpy.ops.transform.resize(value=(6.8, 0.2, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(0,-2.5,(hnoyauprincipal*taille/3/2)))
    boite(
        (
            coord[0],
            coord[1] + 2.5 / trameY,
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  4"),
    )
    bpy.ops.transform.resize(value=(6.8, 0.2, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(0,2.5,(hnoyauprincipal*taille/3/2)))
    boite(
        (
            coord[0] - 3.3 / trameX,
            coord[1],
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  5"),
    )
    bpy.ops.transform.resize(value=(0.2, 4.8, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(-3.3,0,(hnoyauprincipal*taille/3/2)))
    boite(
        (
            coord[0] + 3.3 / trameX,
            coord[1],
            coord[2] + (hnoyauprincipal * taille / 3 / 2),
        ),
        ("noyau double x=" + str(coord[0]) + " y=" + str(coord[1]) + "  6"),
    )
    bpy.ops.transform.resize(value=(0.2, 4.8, hnoyauprincipal * taille / 3))
    # bpy.ops.transform.translate(value=(3.3,0,(hnoyauprincipal/2)))


def routeprincipalebase(coord):
    boite(
        (coord[0], coord[1], coord[2] + (hroute / 2)),
        ("route principale simple x=" + str(coord[0]) + "y=" + str(coord[1])),
    )
    bpy.ops.transform.resize(value=(15, 8, hroute))
    # bpy.ops.transform.translate(value=(0, 0, hroute/2))


def routeprincipale(coord):
    boite(
        (coord[0], coord[1], coord[2] + (hroute / 2)),
        ("route principale x=" + str(coord[0]) + "y=" + str(coord[1])),
    )
    bpy.ops.transform.resize(value=(15, 8, hroute))
    # bpy.ops.transform.translate(value=(0, 0, hroute/2))
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (0, 0, 3.6)},
    )


def routesecondaire(coord):
    boite(
        (coord[0], coord[1], coord[2] + (hroute / 2)),
        ("route secondaire x=" + str(coord[0]) + "y=" + str(coord[1])),
    )
    bpy.ops.transform.resize(value=(15, 8, hroute))
    # bpy.ops.transform.translate(value=(0, 0, hroute/2) )
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (0, 0, 3.6)},
    )


############## ENCODAGE DIMENSIONS ################

trameX = 15
trameY = 8
longueur = 10  # nb case
largeur = 12
hroute = 0.4
hnoyausecondaire = 74
hnoyauprincipal = 78
probabilite = 4

############## FONCTIONS AUXILIAIRES ##############


def probanoyau(proba):

    if proba == 0:
        proba = 1
    elif proba > 10:
        proba = 10
    tmp = randint(0, 10)
    if tmp <= proba:
        return True
    else:
        return False


def probahauteur():

    tmp = randint(0, 10)
    if tmp < 3:
        return 1
    elif tmp > 7:
        return 3
    else:
        return 2


def translatemodule(compt, nom, coord):

    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects[nom].select = True
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (0, 0, 20)},
    )

    if compt < 10:
        tmp = nom + str(".") + str(0) + str(0) + str(compt)
    elif compt < 100:
        tmp = nom + str(".") + str(0) + str(compt)
    elif compt < 1000:
        tmp = nom + str(".") + str(compt)
    else:
        print("Compteur overflow")

    bpy.data.objects[tmp].location[0] = coord[0] * trameX
    bpy.data.objects[tmp].location[1] = coord[1] * trameY
    bpy.data.objects[tmp].location[2] = coord[2]


def placeboite(type, coord1, compt1):

    if type == 3:
        translatemodule(compt1, "batigrand", coord1)
    elif type == 2:
        translatemodule(compt1, "batimoyen", coord1)
    elif type == 1:
        translatemodule(compt1, "batipetit", coord1)
    elif type == 4:
        translatemodule(compt1, "bloctransversalh8", coord1)


##################### MAIN ########################

nbroute = 1
while ((largeur - 6 * nbroute) / (nbroute + 1)) > 7:

    nbroute = nbroute + 1
    # print('nbroute')
    # print(nbroute)


# print ('espace')
espace = randint(5, 7)
# print(espace)
# print ('largeur')
largeur = nbroute * 6 + (nbroute + 1) * espace
# print(largeur)

nbroutebis = 1
while ((longueur - nbroutebis) / (nbroutebis + 1)) > 5:

    nbroutebis = nbroutebis + 1
    # print('nbroutebis')
    # print(nbroutebis)

print("espacebis")
espacebis = randint(3, 5)
# print(espacebis)

print("longueur")
longueur = nbroutebis + (nbroutebis + 1) * espacebis
# print(longueur)

###

# matrice = list(())
matrice = [[] for x in range(longueur * largeur)]
# print(matrice)

compt = True
i = 0
while i < longueur * largeur:

    Xi = i // largeur
    Yi = i % largeur
    Zi = 0

    if compt:

        j = i
        while i < j + espace:
            Xi = i // largeur
            Yi = i % largeur
            if ((Xi % (espacebis)) == 0) and (Xi != 0):
                matrice[i] = [Xi, Yi, Zi, 2, 0, 0]
            else:
                matrice[i] = [Xi, Yi, Zi, 0, 0, 0]
            i = i + 1
            # print(i)
        if (i % largeur) != 0:
            compt = False

    else:

        j = i
        while i < j + 6:
            Xi = i // largeur
            Yi = i % largeur
            matrice[i] = [Xi, Yi, Zi, 1, 0, 0]

            # route principale

            i = i + 1
            # print(i)

        compt = True

# print(espacebis)
# print(matrice)

listetrans = [[] for x in range(largeur + 1)]
listenoyau = [[] for x in range(longueur + 1)]
# print(listenoyau)
i = 0
j = 0
stop = True
while i < longueur * largeur:
    Xi = i // largeur
    Yi = i % largeur
    print(i // largeur)
    print(i % largeur)

    if matrice[i][3] == 0:
        if probanoyau(probabilite) and stop:
            matrice[i][4] = 1
            # placer noyau selon proba
            listenoyau[j].append(i)
            # print(listenoyau)
            stop = False
        else:
            stop = True
        i = i + 1

    elif matrice[i][3] == 1:  # route principale

        if probanoyau(probabilite * 3 / 2):
            matrice[i][4] = 2
            listenoyau[j].append(i)
            matrice[i + 1][4] = 2
            listenoyau[j].append(i + 1)
        i = i + 4
        if probanoyau(probabilite * 3 / 2):
            matrice[i][4] = 2
            listenoyau[j].append(i)
            matrice[i + 1][4] = 2
            listenoyau[j].append(i + 1)
        i = i + 2
        stop = True

    else:
        i = i + 1
        stop = True

    if (i % largeur) == 0:
        j = j + 1
        # print(j)

# print(listenoyau)

j = 0
for j in range(longueur):
    if len(listenoyau[j]) < 2:
        # print('probleme')
        matrice[j * largeur + largeur - 1][4] = 1
        listenoyau[j].append(j * largeur + largeur - 1)

    i = 0
    if len(listenoyau[j]) == 2:
        tmp = probahauteur()
        matrice[listenoyau[j][i]][5] = tmp
        matrice[listenoyau[j][i + 1]][5] = tmp

    else:

        while i < len(listenoyau[j]) - 1:

            if i == 0:
                matrice[listenoyau[j][i]][5] = probahauteur()
                i = i + 1
            else:
                matrice[listenoyau[j][i + 1]][5] = probahauteur()
                matrice[listenoyau[j][i]][5] = max(
                    matrice[listenoyau[j][i + 1]][5], matrice[listenoyau[j][i - 1]][5]
                )
                i = i + 2


i = 0
while i < longueur * largeur:

    # constructions des routes
    if matrice[i][3] == 2:
        routesecondaire((matrice[i][0], matrice[i][1], 0))
    elif matrice[i][3] == 1:
        routeprincipale((matrice[i][0], matrice[i][1], 0))
    i = i + 1

j = 0
i = 0
while i < longueur * largeur:
    # construction des noyaux

    if matrice[i][3] == 2:
        i = i + largeur - 1

    if matrice[i][4] == 2:
        noyauprincipal((matrice[i][0], matrice[i][1], 0), matrice[i][5])
    #        listetrans[j].append(i)
    elif matrice[i][4] == 1:
        noyausecondaire((matrice[i][0], matrice[i][1], 0), matrice[i][5])

    i = i + 1

# construction batiments
compteur = [0, 0, 0, 0, 0]

for j in range(longueur - 1):

    i = 0
    while i < len(listenoyau[j]) - 1:

        val = min(matrice[listenoyau[j][i + 1]][5], matrice[listenoyau[j][i]][5])
        k = listenoyau[j][i]
        tmp = val
        while tmp > 0:
            type = probahauteur()
            if probahauteur() in range(1, 2):
                tampon = True
            else:
                tampon = False
            if tmp == val:
                while k <= listenoyau[j][i + 1]:
                    # creer boite hauteur tmp
                    compteur[type] = compteur[type] + 1
                    placeboite(
                        type,
                        (
                            matrice[k][0],
                            matrice[k][1],
                            hnoyausecondaire / (4 - tmp) + 4,
                        ),
                        compteur[type],
                    )
                    k = k + 1

            elif tampon:
                while k <= listenoyau[j][i + 1]:
                    # creer boite hauteur tmp
                    compteur[type] = compteur[type] + 1
                    placeboite(
                        type,
                        (
                            matrice[k][0],
                            matrice[k][1],
                            (hnoyausecondaire / (4 - tmp) + 4),
                        ),
                        compteur[type],
                    )
                    k = k + 1

            tmp = tmp - 1
            if tmp != 0:
                k = listenoyau[j][i]

        i = i + 1

##########

# transversales

###

"""listetrans = [[],[]]

j = 0
while j < largeur :
    if matrice[j][3] == 1 :
        listetrans.append([])
        while i < longueur :
            if matrice[j+largeur*i][4] == 2 :
                listetrans[j].append(j+largeur*i)
            i = i+1
    j = j+1

for j in range(largeur-1) :
    
    i = 0
    while i < len(listetrans[j])-1 :
            
        val =  min(matrice[listetrans[j][i+1]][5], matrice[listetrans[j][i]][5])
        k = listetrans[j][i]
        tmp = val
        while tmp > 0 :
            type = 4
            if probahauteur() in range(1,2) :
                tampon = True
            else :
                tampon = False
            if tmp == val :
                while k <= listetrans[j][i+1] :
                    #creer boite hauteur tmp
                    compteur[type] = compteur[type] + 1
                    placeboite(type, (matrice[k][0], matrice[k][1], hnoyausecondaire/(4-tmp)+4), compteur[type])
                    k = k+largeur
                    
            elif tampon :
                while k <= listetrans[j][i+1] :
                    #creer boite hauteur tmp
                    compteur[type] = compteur[type] + 1
                    placeboite(type, (matrice[k][0], matrice[k][1], (hnoyausecondaire/(4-tmp)+4)), compteur[type])
                    k = k+largeur
                    
            tmp = tmp-1
            if tmp != 0 :
                k = listenoyau[j][i]
            
        
        i = i+1

                 
"""


# print('espace :')
# print(espace)
# print('espacebis :')
# print(espacebis)
# print('matrice :')
# print(matrice)
# print(probanoyau(5))


axoCam("axonometrique", "isometrie")

print("ca va")


# placeboite(2, (0,0,0), 1)
