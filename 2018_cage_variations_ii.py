# SPDX-FileCopyrightText: 2018 David Abrantes Pinto & Mihai Pop
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# exercice Python Blender pour la remise du 17/01/2019
# développé sous Blender hash f4dc9f9d68b
# étudiants: Mihai Pop, David Abrantes Pinto


print(50 * "-", end="\n\n\n")

import bpy
import random
from random import choice
from bpy import data as D


def delete_all():
    for item in D.objects:
        D.objects.remove(item)
    for item in D.meshes:
        D.meshes.remove(item)


delete_all()


###CREATION CADRE DE BASE###
def Cadre(position, dimension, nom):
    bpy.ops.mesh.primitive_cube_add(location=position)
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.name = nom
    bpy.context.object.show_wire = False
    bpy.context.object.draw_type = "WIRE"


Cadre((10, 10, 10), (10, 10, 10), "Cadre de base")

###CREATION DES CUBES###
##CUBE1##


def Cube1(position, dimension, rotation, translation, nom):

    # POSITION
    bpy.ops.mesh.primitive_cube_add(location=position)

    # DIMENSION, HAUTEUR
    bpy.ops.transform.resize(value=dimension)

    # NOMBRE DE COPIES
    nc = random.randint(0, 20)
    for i in range(0, nc):
        # Choix random de l'axe de translation
        liste = [False, False, False]
        liste[random.randint(0, 2)] = True
        tuple(liste)
        # Translation dans l'axe choisi
        bpy.ops.object.duplicate_move_linked(
            OBJECT_OT_duplicate={"linked": True},
            TRANSFORM_OT_translate={
                "value": translation,
                "constraint_axis": tuple(liste),
            },
        )

    # SELECTION DES CUBES
    bpy.ops.object.select_linked(type="OBDATA")

    # NOM
    bpy.context.object.name = nom

    # ROTATION DE LA FORME FINIE
    bpy.ops.transform.rotate(value=-0.6, axis=rotation)

    # AFFICHAGE DES DIFFERENTS PARAMETRES
    print(
        "Cube 1",
        "\n\n",
        "Taille (x, y, z) =",
        dimension,
        "\n",
        "Nombre des copies =",
        nc,
        "\n",
        "Espace entre les copies (x, y, z) =",
        translation,
        "\n",
        "Rotation (x, y, z) =",
        rotation,
        "\n",
        "Position (x, y, z) =",
        position,
    )
    print("\n\n\n", 50 * "-", end="\n\n\n")


# VALEURS DES PARAMETRES (POSITION, DIMENSION, ROTATION, TRANSLATION)

Cube1(
    (8, 8, 8),
    (random.randint(1, 20), random.randint(1, 20), 3),
    (-0.6, -0.6, -0.6),
    (4, 4, random.randint(1, 20)),
    "Cube1",
)


##CUBE2##


def Cube2(position, dimension, rotation, translation, nom):

    # POSITION
    bpy.ops.mesh.primitive_cube_add(location=position)

    # DIMENSION, HAUTEUR
    bpy.ops.transform.resize(value=dimension)

    # NOMBRE DE COPIES
    nc = random.randint(0, 20)
    for i in range(0, nc):
        # Choix random de l'axe de translation
        liste = [False, False, False]
        liste[random.randint(0, 2)] = True
        tuple(liste)
        # Translation dans l'axe choisi
        bpy.ops.object.duplicate_move_linked(
            OBJECT_OT_duplicate={"linked": True},
            TRANSFORM_OT_translate={
                "value": translation,
                "constraint_axis": tuple(liste),
            },
        )

    # SELECTION DES CUBES
    bpy.ops.object.select_linked(type="OBDATA")

    # NOM
    bpy.context.object.name = nom

    # ROTATION DE LA FORME FINIE
    bpy.ops.transform.rotate(axis=rotation)

    # AFFICHAGE DES DIFFERENTS PARAMETRES
    print(
        "Cube 2",
        "\n\n",
        "Taille (x, y, z) =",
        dimension,
        "\n",
        "Nombre des copies =",
        nc,
        "\n",
        "Espace entre les copies (x, y, z) =",
        translation,
        "\n",
        "Rotation (x, y, z) =",
        rotation,
        "\n",
        "Position (x, y, z) =",
        position,
    )
    print("\n\n\n", 50 * "-", end="\n\n\n")


# VALEURS DES PARAMETRES (POSITION, DIMENSION, ROTATION, TRANSLATION)

Cube2(
    (10, 10, 10),
    (random.randint(1, 20), random.randint(1, 20), 2),
    (25, 25, 25),
    (4, 4, random.randint(1, 20)),
    "Cube2",
)


##CUBE3##


def Cube3(position, dimension, rotation, translation, nom):

    # POSITION
    bpy.ops.mesh.primitive_cube_add(location=position)

    # DIMENSION, HAUTEUR
    bpy.ops.transform.resize(value=dimension)

    # NOMBRE DE COPIES
    nc = random.randint(0, 20)
    for i in range(0, nc):
        # Choix random de l'axe de translation
        liste = [False, False, False]
        liste[random.randint(0, 2)] = True
        tuple(liste)
        # Translation dans l'axe choisi
        bpy.ops.object.duplicate_move_linked(
            OBJECT_OT_duplicate={"linked": True},
            TRANSFORM_OT_translate={
                "value": translation,
                "constraint_axis": tuple(liste),
            },
        )

    # SELECTION DES CUBES
    bpy.ops.object.select_linked(type="OBDATA")

    # NOM
    bpy.context.object.name = nom

    # ROTATION DE LA FORME FINIE
    bpy.ops.transform.rotate(axis=rotation)

    # AFFICHAGE DES DIFFERENTS PARAMETRES
    print(
        "Cube 3",
        "\n\n",
        "Taille (x, y, z) =",
        dimension,
        "\n",
        "Nombre des copies =",
        nc,
        "\n",
        "Espace entre les copies (x, y, z) =",
        translation,
        "\n",
        "Rotation (x, y, z) =",
        rotation,
        "\n",
        "Position (x, y, z) =",
        position,
    )
    print("\n\n\n", 50 * "-", end="\n\n\n")


# VALEURS DES PARAMETRES (POSITION, DIMENSION, ROTATION, TRANSLATION)

Cube3(
    (10, 5, 2),
    (random.randint(1, 20), random.randint(1, 20), 2),
    (15, 15, 15),
    (4, 4, random.randint(1, 20)),
    "Cube3",
)


##CUBE4##


def Cube4(position, dimension, rotation, translation, nom):

    # POSITION
    bpy.ops.mesh.primitive_cube_add(location=position)

    # DIMENSION, HAUTEUR
    bpy.ops.transform.resize(value=dimension)

    # NOMBRE DE COPIES
    nc = random.randint(0, 20)
    for i in range(0, nc):
        # Choix random de l'axe de translation
        liste = [False, False, False]
        liste[random.randint(0, 2)] = True
        tuple(liste)
        # Translation dans l'axe choisi
        bpy.ops.object.duplicate_move_linked(
            OBJECT_OT_duplicate={"linked": True},
            TRANSFORM_OT_translate={
                "value": translation,
                "constraint_axis": tuple(liste),
            },
        )

    # SELECTION DES CUBES
    bpy.ops.object.select_linked(type="OBDATA")

    # NOM
    bpy.context.object.name = nom

    # ROTATION DE LA FORME FINIE
    bpy.ops.transform.rotate(axis=rotation)

    # AFFICHAGE DES DIFFERENTS PARAMETRES
    print(
        "Cube 4",
        "\n\n",
        "Taille (x, y, z) =",
        dimension,
        "\n",
        "Nombre des copies =",
        nc,
        "\n",
        "Espace entre les copies (x, y, z) =",
        translation,
        "\n",
        "Rotation (x, y, z) =",
        rotation,
        "\n",
        "Position (x, y, z) =",
        position,
    )
    print("\n\n\n", 50 * "-", end="\n\n\n")


# VALEURS DES PARAMETRES (POSITION, DIMENSION, ROTATION, TRANSLATION)

Cube4(
    (6, 6, 8),
    (random.randint(1, 20), random.randint(1, 20), 1),
    (-50, -50, -50),
    (2, 2, random.randint(1, 20)),
    "Cube4",
)


##CUBE4##


def Cube5(position, dimension, rotation, translation, nom):

    # POSITION
    bpy.ops.mesh.primitive_cube_add(location=position)

    # DIMENSION, HAUTEUR
    bpy.ops.transform.resize(value=dimension)

    # NOMBRE DE COPIES
    nc = random.randint(0, 20)
    for i in range(0, nc):
        # Choix random de l'axe de translation
        liste = [False, False, False]
        liste[random.randint(0, 2)] = True
        tuple(liste)
        # Translation dans l'axe choisi
        bpy.ops.object.duplicate_move_linked(
            OBJECT_OT_duplicate={"linked": True},
            TRANSFORM_OT_translate={
                "value": translation,
                "constraint_axis": tuple(liste),
            },
        )

    # SELECTION DES CUBES
    bpy.ops.object.select_linked(type="OBDATA")

    # NOM
    bpy.context.object.name = nom

    # ROTATION DE LA FORME FINIE
    bpy.ops.transform.rotate(axis=rotation)

    # AFFICHAGE DES DIFFERENTS PARAMETRES
    print(
        "Cube 5",
        "\n\n",
        "Taille (x, y, z) =",
        dimension,
        "\n",
        "Nombre des copies =",
        nc,
        "\n",
        "Espace entre les copies (x, y, z) =",
        translation,
        "\n",
        "Rotation (x, y, z) =",
        rotation,
        "\n",
        "Position (x, y, z) =",
        position,
    )
    print("\n\n\n", 50 * "-", end="\n\n\n")


# VALEURS DES PARAMETRES (POSITION, DIMENSION, ROTATION, TRANSLATION)

Cube5(
    (9, 9, 11),
    (random.randint(1, 20), random.randint(1, 20), 1),
    (-50, -50, -50),
    (2, 2, random.randint(1, 20)),
    "Cube5",
)


###Boolean Difference###
for boite in bpy.data.objects[1:]:
    bpy.context.scene.objects.active = boite
    boolean = boite.modifiers.new("MyBoolean", "BOOLEAN")
    boolean.object = bpy.data.objects[0]
    boolean.operation = "INTERSECT"
