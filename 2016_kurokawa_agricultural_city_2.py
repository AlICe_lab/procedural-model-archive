# SPDX-FileCopyrightText: 2016 Dalila Gillet
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import radians
from math import cos
from math import sin

bpy.ops.object.select_all(action="SELECT")
# bpy.data.objects["Plane"].select=False
bpy.ops.object.delete(use_global=False)


# Definition de la fonction de camera en projection oblique:
def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 60  # si 1 quartier si non modifier scale et location

    if projection == "oblique":
        if canon == "militaire":
            monAxoCam.name = "oblMilitaire"
            monAxoCam.rotation_euler = (radians(45), 0.0, radians(30))
            monAxoCam.location = (7.071, -12.247, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(radians(45))


axoCam("oblique", "militaire")


######################
#  maison champignon
######################


def CopyPaste(Maison, M1):

    bpy.context.scene.objects.active = bpy.data.objects[Maison]
    obj = bpy.context.active_object
    Copy = bpy.data.objects.new(M1, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


def CopyPaste2(Maison2, M2):

    bpy.context.scene.objects.active = bpy.data.objects[Maison2]
    obj = bpy.context.active_object
    Copy = bpy.data.objects.new(M2, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


###################################################
# construction de route entre les micro-cellule
###################################################
# ne fonctionne pas pour crere de maniere aléatoire des routes


def coordonneroute1(positionroute1):
    x, y = positionroute1
    X = 0.05
    Xx = 0.05
    Zz = 0.02
    Z = 0.02

    List_y = [2.05, 2.05, 0, 0]

    Liste_positionroute1 = []
    for Rx in range(0, 1):
        for Ry in range(0, 3):
            Liste_positionroute1.append(((Rx) + x, (Ry) + y, True))

    # print ('route',Liste_positionroute1)

    for point in Liste_positionroute1:
        x, y, boolean = point
        Hau_Var = choice(List_y)
        if Hau_Var == 0:
            Xx = 0
            Z = 0
        # routes en X
        bpy.ops.mesh.primitive_cube_add(location=(x + 1.5, y + 0.5, 0.06))
        bpy.ops.transform.resize(value=(Hau_Var, Xx, Z))

    Liste_positionroute2 = []
    for Rrx in range(0, 3):
        for Rry in range(0, 1):
            Liste_positionroute2.append(((Rrx) + x, (Rry) + y, True))

    for point2 in Liste_positionroute2:
        x, y, boolean = point2
        Hau_Var = choice(List_y)
        if Hau_Var == 0:
            X = 0
            Zz = 0
        # routes en Y
        bpy.ops.mesh.primitive_cube_add(location=(x + 0.5, y - 0.5, 0.06))
        bpy.ops.transform.resize(value=(X, Hau_Var, Zz))


###############################################################
# cellule comprenant un ensemble de micro-cellule pleine : vide
###############################################################


def coordonnecellule(location):
    Liste_maison = (0, 1, 2, 3)
    Liste_chiffre = (0, 1, 1, 1, 2, 2, 10, 10, 20, 20)
    chiffre = choice(Liste_chiffre)
    # print('rapport plein vide 1/', chiffre)

    x, y = location

    Liste_position = []

    for ix in range(0, 4):
        for iy in range(0, 4):
            if randint(0, chiffre) == 0:
                Liste_position.append((ix + x, iy + y, True))
            else:
                Liste_position.append((ix + x, iy + y, False))
    # print('ma liste de cellule', Liste_position)

    for point in Liste_position:
        x, y, boolean = point

        if boolean == True:
            bpy.ops.mesh.primitive_cube_add(location=(x, y, 0))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.05))

            champignon = choice(Liste_maison)
            if champignon == 1:
                Maison = CopyPaste("Maison", "M1")
                Maison.location = (x, y + 0.2, 0.05)
                Maison.scale = (0.02, 0.02, 0.02)
                print("nbr de maison  ", Maison)
            if champignon == 2:
                Maison2 = CopyPaste2("Maison2", "M2")
                Maison2.location = (x + 0.2, y, 0.05)
                Maison2.scale = (0.02, 0.02, 0.02)

                print("nbr de maison  2 ", Maison2)


#########################################
# route autour des quartiers
#########################################


def coordonneroute(positionroute):
    x, y = positionroute
    Liste_positionroute = []
    for rx in range(0, 1):
        for ry in range(0, 1):
            Liste_positionroute.append(((rx) * 4 + x, (ry) * 4 + y, True))

    for point in Liste_positionroute:
        x, y, boolean = point

        # routes en X
        bpy.ops.mesh.primitive_cube_add(location=((x - 0.5), (y + 1.5), 0.06))
        bpy.ops.transform.resize(value=(0.15, 2.12, 0.1))
        bpy.ops.mesh.primitive_cube_add(location=((x + 3.5), (y + 1.5), 0.06))
        bpy.ops.transform.resize(value=(0.15, 2.12, 0.1))
        # poteaux en X
        bpy.ops.mesh.primitive_cube_add(location=(x - 0.5, y - 0.5, -0.5))
        bpy.ops.transform.resize(value=(0.1, 0.1, 0.5))
        bpy.ops.mesh.primitive_cube_add(location=(x + 3.5, y - 0.5, -0.5))
        bpy.ops.transform.resize(value=(0.1, 0.1, 0.5))
        # routes en Y
        bpy.ops.mesh.primitive_cube_add(location=(x + 1.5, (y - 0.5), 0.06))
        bpy.ops.transform.resize(value=(2.12, 0.15, 0.1))
        bpy.ops.mesh.primitive_cube_add(location=(x + 1.5, (y + 3.5), 0.06))
        bpy.ops.transform.resize(value=(2.12, 0.15, 0.1))
        # poteaux en Y
        bpy.ops.mesh.primitive_cube_add(location=(x + 3.5, y + 3.5, -0.5))
        bpy.ops.transform.resize(value=(0.1, 0.1, 0.5))
        bpy.ops.mesh.primitive_cube_add(location=(x - 0.5, y + 3.5, -0.5))
        bpy.ops.transform.resize(value=(0.1, 0.1, 0.5))


#########################################
# quartier comprenant un ensemble de cellules
#########################################


def coordonnecelluledansquartier(positionquartier):
    x, y = positionquartier
    Liste_positionquartier = []
    for jx in range(0, 5):
        for jy in range(0, 5):
            Liste_positionquartier.append((jx * 4 + x, jy * 4 + y))

    for point in Liste_positionquartier:

        coordonnecellule(point)
        coordonneroute(point)
        coordonneroute1(point)


monquartier = coordonnecelluledansquartier((0.5, 0.5))


#########################################
# nouvelles cellules autour du quartier
#########################################


def nouvellescellulesX(positionNCX):
    x, y = positionNCX
    Liste_positionNCX = []
    for jx in range(0, 6):
        Liste_positionNCX.append((jx * 4 + x, 0.5 + y))
    for jX in range(0, 6):
        Liste_positionNCX.append((jX * 4 + x, 24.5 + y))
    for jy in range(1, 6):
        Liste_positionNCX.append((x, jy * 4 + (0.5 + y)))
    for jY in range(1, 6):
        Liste_positionNCX.append((x + 24, jY * 4 + (0.5 + y)))
    for y in range(1, randint(4, 6)):
        for i in range(1, randint(4, 6)):
            position2 = choice(Liste_positionNCX)
            Liste_positionNCX.remove(position2)

    for point in Liste_positionNCX:

        coordonnecellule(point)
        coordonneroute(point)
        coordonneroute1(point)


mesNouvellesCellulesX = nouvellescellulesX((-3.5, -4))
monsecondquartier = coordonnecelluledansquartier((-35.5, -3.5))
mesNouvellesCellulesX2 = nouvellescellulesX((-39.5, -8))
print("+++++++++")


bpy.ops.mesh.primitive_cube_add(location=(-12.12, 0, 0.06))
bpy.ops.transform.resize(value=(12.2, 0.15, 0.1))

bpy.ops.mesh.primitive_cube_add(location=(-12.12, 12, 0.06))
bpy.ops.transform.resize(value=(12.2, 0.15, 0.1))
print("---------")
