<!--
SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>

SPDX-License-Identifier: CC0-1.0
-->

# Procedural Model Archive

This repository contains the students' Blender Python scripts developed during the _Architectural Analysis and Computer Process_ (2013-2016) and the _Formal Analysis and Computer Process_ (2018-2024) teaching modules from the AlICe research laboratory master studio.

| Year | Theme | Students | Blender version |
|---|---|---|---|
| 2013 | Architectural Analysis & Computer Process I | Nourdin Ben Abbou · Nicolas Bourguignon · Edouard Capel · Quentin Delaval · Javi Garcia · Arthur Lachard · Johnny Leya · Elzbieta Liguz · Diego Necochea · Liesbet Neesen · Gennaro Notaro · Krystian Polanski · Evelien Ral ·  Quentin Parete · Julien Rippinger | 2.79b |
| 2014 | Architectural Analysis & Computer Process II | Morgan Baufils · Yani Bex · Khalil Bouguerch · Bertrand Etienne · David Facho Santos · Marcello Frontini · Nikita Itenberg · Nitay Lehrer · Manuel Leon Fanjul · Geoffrey Minne · Odair Nelson Rodrigues · Tom Pariente · Simone Peluso · Marine Serette · Thanh Thi Truong · Hong Trang Mai | 2.79b |
| 2015 | Architectural Analysis & Computer Process III | Raphaëlle Bouvart · Mathieu Chabbert · Marc-Antoine Froger · Adrien Laügt · Jean Trottet | 2.79b |
| 2016 | Architectural Analysis & Computer Process IV | Ali Alami Talbi · Hugo Cormerais · Corentin Coulon · Vasco Da Re · Robin Dufayard · Juliette Fuhs · Dalila Gillet · Nicolas Jonard · Mathieu Jonckheere · Quincy Jones Deldaele · Benjamin Marc · Adam Masset-Clément Panier des Touches · Antoine Moulin · Camille Pons-Foucault de Pavant · Axel Ricbourg · Michaël Vignano · Jordan Zerki | 2.79b |
| 2018 | Formal Analysis and Computer Process - Medley I/II | David Abrantes Pinto · Gonzalo Auger Portillo · Boris Bardonneau · Nestor Beguin · Hakim Ben Baghdad · Michela Cardia · Natalia Casas · Giovanni Chionetti · Charles-Antoine Grignard · Michael Hoebeke · Alice Leclercq ·Alba Llevadot Massanes · Charles Mestdagh · Arnaud Naome · Daniel O'Driscoll · Rodrigo Oliveira Rodrigues · Mihai Pop · Pierre Quinet · Marion Vancoppenolle · Marie Willame · Pierre Zeytoune | 2.79b |
| 2019 | Formal Analysis and Computer Process - Medley II/II | Julia Barnoin · Guillaume Charinet · Gabriel Châtel · Adrien Dumont · Arnaud Froment · Ayman Ghazali · Timothée Gignac · Sacha Jonckers · Pâris Lamboray · Jeano Mertens · Johan Metzger · Thomas Paquot · Charles Preham · Nicolas Schiltz · Arthur Schweisthal · Aleksandar Stoimenov · Anthony Villegas del Val | 2.93 |
| 2020 | Formal Analysis and Computer Process - The Algorists | Ammar Aharchi Azami-Housni · Pareli Akelian · Lauren Amrani · Bassam Boughaba · Christophe Demiralp · Kadir Kaya · Spyridon Kellergis · Azélie Louarn · Jeanne Mujawamariya · Sara Paolitto · Thomas Pennesi · Camille Salignon · Eliott Steylemans | 2.93 |
| 2021 | Formal Analysis and Computer Process - Algorithmic Music I/III | Omar Asif · Leyloiu Boey · Sascha Ciudad Hidalgo · Gaston Demoulin · Matthieu Desruelles · Estelle De Vos · Jules Dewez · Dina El Khattabi · Loïc Gras · Carl Haddad · Johan Kouaho · Noémie Lobry · Loïc Mabire · Oriel Malka · Jacques Martens · Stéphanie Milan · Raban Ohlhoff · Ayman Ouiriarhli Ameur · Guillaume Pirard · Alexandre Troc | 2.93 |
| 2022 | Formal Analysis and Computer Process - Algorithmic Music II/III | Araprima Aji · Sebastian Big · Elias Chabbi · Lina El Ouali · Marin Georges · Jules Halbardier · Mario Hidalgo Venegas · Ji Eun Kim · Loan Le Merlus · Quentin Leroy · Violette Morelle · Francesco Piloni · Etienne Poinas · Khawla Rachadi · Benjamin Tousch · Lucas Van Melderen | 3.6 |
| 2024 | Formal Analysis and Computer Process - Algorithmic Music III/III | Loïs Anseel · Carl Elebaut · Maëlia Lecouturier · Angela Farina · Arthur Bihay · Julien Rialland · Nousseyba Darkaoui · Alyssar Dunia · Alix Pairoux · Chloé Coppens · Hana Chibani · Jakub Niemynski · Lara Pietkowicz · Margot Chambon · Maëlle Durin · Metin Özden · Margot Schuster · Yasmina El youmni · Léo Joachim · Camille frejafon · Florian Dubois · Tetiana Chernysh · Alina Matiienko · Carla Treinen · Kaede Otsubo · Larissa Silva Feital · Takashi Miyase · Yuki Fujieda · Ana Beatriz Bonfim Ferreira | 4.2 |

## Disclaimer

Please note that the scripts are provided in their original form and may contain bugs or errors. Should any issues or bugs be encountered with the scripts in this archive, we invite you to open an issue or submit a pull request with a proposed fix. We are open to contributions that may enhance the scripts' development.

## License

The scripts are licensed under the GNU General Public License v3.0 or later.

## Citation

Rippinger, Julien, Denis Derycke, and Michel Lefèvre. “Procedural Model Archive.” Zenodo, 2025. https://doi.org/10.5281/ZENODO.12731897.

```bib
@misc{https://doi.org/10.5281/zenodo.12731897,
  doi = {10.5281/ZENODO.12731897},
  url = {https://zenodo.org/doi/10.5281/zenodo.12731897},
  author = {Rippinger,  Julien and Derycke,  Denis and Lefèvre,  Michel},
  keywords = {Blender,  Architecture,  FOS: Civil engineering},
  title = {Procedural Model Archive},
  publisher = {Zenodo},
  year = {2025},
  copyright = {GNU General Public License v3.0 or later}
}
```

For individual scripts, please also refer to their specific authors. See the file headers for attributions.

## Development tips

1. Call external scripts from within Blender
```python
import bpy
import os

filename = os.path.join(os.path.dirname(bpy.data.filepath), "script.py")
exec(compile(open(filename).read(), filename, 'exec'))
```

2. Or execute scripts from terminal
```bash
blender -P script.py
```

3. Install fake-bpy-module for autocomplete suggestions
```bash
python -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
python -m pip install fake-bpy-module-4.2  # install necessary version
```

4. Code formatting
```bash
source .venv/bin/activate
python -m pip install black
black .
```
