# SPDX-FileCopyrightText: 2022 Jules Halbardier & Violette Morelle
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

##start##
import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()


##Grid creation##
def randX(mesh):
    x_mapping = {}
    for vert in mesh.vertices:
        if vert.co.z != 0:
            vert_x = vert.co.x
            if vert_x not in x_mapping:
                x_mapping[vert_x] = random.uniform(vert.co.z, vert.co.z + 0.3)
            elevation = x_mapping[vert_x]
            vert.co.z = elevation
    # wiggles#
    for vert in mesh.vertices:
        if vert.co.z != 0:
            vert.co.x += random.randint(-1, 2) * 0.01


##Boolean application##
def applyBool():
    #    Items, va donner le nom de l'object et l'object lui meme
    for key, object in bpy.data.objects.items():
        #        On veut pas apply sur les grilles, donc si "Grid" est ds le nom on skip
        if "Grid" not in key:
            bpy.ops.object.modifier_add(type="BOOLEAN")
            bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
            bpy.context.object.modifiers["Boolean"].object = object
            bpy.context.object.modifiers["Boolean"].use_self = True
            bpy.ops.object.modifier_apply(modifier="Boolean")


##remove objects after boolean##
def removeTruc():
    for key, object in bpy.data.objects.items():
        if "Grid" not in key:
            object.select_set(True)
    bpy.ops.object.delete()


##grid+space##
def getGirdsHeight():
    totalHeight = 2
    trou = 0.5

    reste = totalHeight - trou

    limite = trou / 2

    first = random.uniform(0.3, reste)
    if first > limite:
        second = random.uniform(0.3, reste - first)
    else:
        second = random.uniform(limite, reste - first)

    return first, second


##cubes for boolean##
bpy.ops.mesh.primitive_cube_add(
    location=(0.8, random.uniform(-0.7, 0), 1), scale=(0.35, 0.07, 1.1)
)
bpy.ops.mesh.primitive_cube_add(
    location=(0.8, random.uniform(0, 0.9), 1), scale=(0.35, 0.07, 1.1)
)
bpy.ops.mesh.primitive_cube_add(
    location=(0.8, random.uniform(0, 0.9), 1), scale=(0.35, 0.07, 1.1)
)

# cylinders#
bpy.ops.mesh.primitive_cylinder_add(
    radius=0.1,
    depth=2,
    location=(random.uniform(-0.8, 0.8), random.uniform(-0.8, 0.8), 1),
    scale=(1, 1, 1.1),
)
bpy.ops.mesh.primitive_cylinder_add(
    radius=0.1,
    depth=2,
    location=(random.uniform(-0.8, 0.8), random.uniform(-0.8, 0.8), 1),
    scale=(2, 2, 1.1),
)
bpy.ops.mesh.primitive_cylinder_add(
    radius=0.1,
    depth=2,
    location=(random.uniform(-0.8, 0.8), random.uniform(-0.8, 0.8), 1),
    scale=(1.5, 1.5, 1.1),
)

firstHeight, secondHeight = getGirdsHeight()

"""get vertices"""
object = bpy.data.objects["Cylinder"]
vertices = list(object.data.vertices)

"""sort vertices"""
stacked_vertices = {}
for vert in vertices:
    #    vert.co.z = abs(vert.co.z)
    z = round(vert.co.z, 2)
    if z not in stacked_vertices.keys():
        stacked_vertices[z] = [vert]
    else:
        stacked_vertices[z].append(vert)

"""select vertices per level"""
for level in stacked_vertices.values():
    # for each z value vertices
    bpy.ops.object.mode_set(mode="OBJECT")
    obj = bpy.context.active_object
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="VERT")
    bpy.ops.mesh.select_all(action="DESELECT")
    bpy.ops.object.mode_set(mode="OBJECT")
    for vert in level:
        vert.select = True
    bpy.ops.object.mode_set(mode="EDIT")
    # change scale of selected vertices
    s = random.uniform(0.15, 2.5)
    bpy.ops.transform.resize(value=(s, s, s))
    # end
    bpy.ops.mesh.select_all(action="DESELECT")
    bpy.ops.object.editmode_toggle()

##first grid##
bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=70, y_subdivisions=30, size=2, location=(0, 0, 0), scale=(1, 1, 1)
)

##extrude##
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, firstHeight)})
bpy.ops.object.editmode_toggle()

##peaks##
obj = bpy.data.objects["Grid"]
mesh = obj.data
randX(mesh)

# Applique les boolean a la grille du dessous
applyBool()

##second grid##
bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=70, y_subdivisions=30, size=2, location=(0, 0, 0), scale=(1, 1, 1)
)

##extrude##
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, secondHeight)})
bpy.ops.object.editmode_toggle()

##peaks##
obj = bpy.data.objects["Grid.001"]
mesh = obj.data
randX(mesh)

##rotation and location##
bpy.context.object.rotation_euler[1] = 3.14159
bpy.context.object.location[2] = 2


##Applique les boolean a la grille du dessus
applyBool()

##On deselect pcq la grid.001 est le dernier object cree et est donc select de base
bpy.ops.object.select_all(action="DESELECT")

##Boolean objects removal##
removeTruc()
