# SPDX-FileCopyrightText: 2020 Thomas Pennesi
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# ULB FA 2020-21 - First scripts
# Rework of an algorist artwork - Frieder Nake 'Rechteckschraffuren'
# Pennesi Thomas
# 000392043
# 20/01/2021 - v19
# Option AIM 2020-2021 module 1
# Dev. Blender 2.90.1


# ========================
# import modules

import bpy
import math
import random
from random import randint

# ========================


# Clean space
def clean_all():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)
    bpy.ops.outliner.orphans_purge()


clean_all()


# ------------------------#
#  Cubes of S l i c e s  #
# ------------------------#


# Create a frame cube of 10*10*10
def Frame(position, dimension, name):
    bpy.ops.mesh.primitive_cube_add(location=position)
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.name = name
    bpy.context.object.show_wire = False
    bpy.context.object.display_type = "WIRE"


Frame((0, 0, 0), (10, 10, 10), "Frame limits")


# Create the colors materials collection
mat_black = bpy.data.materials.new(name="Black")  # set new material to variable
bpy.data.materials["Black"].diffuse_color = (
    0.0134117,
    0.0134117,
    0.0134117,
    1,
)  # change color
mat_orange = bpy.data.materials.new(name="Orange")  # set new material to variable
bpy.data.materials["Orange"].diffuse_color = (
    0.807346,
    0.151573,
    0.00743982,
    1,
)  # change color
mat_types = (mat_black, mat_orange)  # List the color types


# Bloc create cubes as slices using an array modifier
def Bloc():

    # Create first slice
    bpy.ops.mesh.primitive_cube_add(
        size=1, location=centerposition, scale=(long, large, height)
    )  # position & size attributes
    bpy.ops.transform.rotate(
        value=1.5708, orient_axis=rotation
    )  # rotation 90* attribute

    # Colorize the active object
    activeObject = bpy.context.active_object  # set active object to variable
    activeObject.data.materials.append(colors)  # add the material to the object

    # Use array to multiply the slice
    bpy.ops.object.modifier_add(type="ARRAY")  # load array
    # change factor direction X to factor Z with relative displace attribute
    bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 0
    bpy.context.object.modifiers["Array"].relative_offset_displace[2] = centerdistance
    bpy.context.object.modifiers["Array"].count = (
        numberslice  # number of slice attribute
    )
    # ? transform-translate of half backward the array axis


# Generate 3 first blocs as limits of the cube
# ------------------------------------------

# Type of the slices
height = 0.4
colors = mat_types[0]

# Directions of the slices
chooserot = ["X", "Y", "Z"]  # axis of displace
rotation = random.choice(chooserot)
rotation = "Z"

for bloclimits in range(3):

    # Dimensions of the slices
    long = random.uniform(10, 20)
    large = random.uniform(10, 20)

    # Density of the slices
    numberslice = randint(6, 36)
    centerdistance = random.uniform(1, 2)  # Relative - 1 is equivalent to height

    # Point the 8 angles of the limit cube with bloc array in Z axis
    cp_a = (10.0 - large / 4, 10.0 - long / 4, -10.0 + height / 4)
    cp_b = (-10.0 + large / 4, 10.0 - long / 4, -10.0 + height / 4)
    cp_c = (-10.0 + large / 4, -10.0 + long / 4, -10.0 + height / 4)
    cp_d = (10.0 - large / 4, -10.0 + long / 4, -10.0 + height / 4)
    cp_e = (
        10.0 - large / 4,
        10.0 - long / 4,
        10.0 - (numberslice * (height * centerdistance)) / 2,
    )
    cp_f = (
        -10.0 + large / 4,
        10.0 - long / 4,
        10.0 - (numberslice * (height * centerdistance)) / 2,
    )
    cp_g = (
        -10.0 + large / 4,
        -10.0 + long / 4,
        10.0 - (numberslice * (height * centerdistance)) / 2,
    )
    cp_h = (
        10.0 - large / 4,
        -10.0 + long / 4,
        10.0 - (numberslice * (height * centerdistance)) / 2,
    )
    cp_random = (cp_a, cp_b, cp_c, cp_d, cp_e, cp_f, cp_g, cp_h)
    points = random.choice(cp_random)

    # Assure 3 different angles
    points_a = ()
    if points != points_a:
        centerposition = points
        Bloc()
    points_a = points
    print("--- cp_limits: ---")
    print(points)
    print("------")

# ------------------------------------------


# Generate two bunch of blocs with various parameters
# ---------------------------------------------------

# First loop to generate type_a of multiblocs with different bunch of characteristics
multiblocs_a = 0
while multiblocs_a < 12:
    multiblocs_a = multiblocs_a + 1

    # Parameters of the blocs
    height = 0.4
    colors = mat_types[0]
    centerposition = (
        random.uniform(-5, 5),
        random.uniform(-5, 5),
        random.uniform(-5, 5),
    )
    rotation = random.choice(chooserot)
    centerdistance = randint(1, 2)  # Relative - 1 is equivalent to height
    numberslice = randint(1, 12)
    long = random.uniform(1, 20)
    large = random.uniform(1, 20)

    # Movements of the blocs
    new_centerposition = []
    for i in centerposition:
        new_centerposition.append(i + height)
    centerposition = new_centerposition
    Bloc()

    enum_ablocs = str(multiblocs_a)
    print("------cp_a:" + enum_ablocs + "------")
    print(centerposition)
    print("--------------------")


# Second loop to generate type_b of multiblocs with different bunch of characteristics
multiblocs_b = 0
while multiblocs_b < 12:
    multiblocs_b = multiblocs_b + 1

    # Parameters of the blocs
    height = 0.8
    colors = mat_types[1]
    centerposition = (
        random.uniform(-5, 5),
        random.uniform(-5, 5),
        random.uniform(-5, 5),
    )
    rotation = random.choice(chooserot)
    centerdistance = randint(1, 2)  # Relative - 1 is equivalent to height
    numberslice = randint(1, 6)
    long = random.uniform(1, 20)
    large = random.uniform(1, 20)

    # Movements of the blocs
    new_centerposition = []
    for i in centerposition:
        new_centerposition.append(i + height)
    centerposition = new_centerposition
    Bloc()

    enum_bblocs = str(multiblocs_b)
    print("----cp_b-----" + enum_bblocs)
    print(centerposition)
    print("-------------")

# ---------------------------------------------------


"""
print ('INFO BLOC   -----')
print ('_TYPE:     ', height, colors)
print ('_ROTATION: ', rotation)
print ('_POSITION: ', centerposition)
print ('_DIMENSION:', long, 'x', large)
print ('_DENSITY:  ', numberslice, 'x', centerdistance)
print ('---------   -----')
"""

# # # # # # # # #
