# SPDX-FileCopyrightText: 2014 Morgan Baufils
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# script ECOLES PRIMAIRES TOULOUSE LE MIRAIL
# développé sous Blender 2.71 / hash 9337574
# windows 8

import bpy
import random
from mathutils import Vector


# NETTOYAGE DU MODELE
def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)


nettoyage()

#################################### PLAN ####################################

# ETAPE1:
#    -Définition de la salle de classe de base
# ETAPE2:
#   -Définition des différents modules de base
#   -Choix aléatoire
# ETAPE3:
#   -Définition des transformations
#   -Définition des modules de second niveau
#   -Choix aléatoire
# ETAPE4:
#   -Définition de l'étage
# ETAPE5:
#   -Définition des modules de dernier niveau
#   -Choix aléatoire = projet total
# CONSOLE


################################### ETAPE1 ###################################

# DEFINITION DE LA SALLE DE CLASSE DE BASE

pos_X = 0
pos_Y = 0
pos_Z = 0
dim_X = 7.2
dim_Y = 7.2
dim_Z = 3
z = 0


def classe(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


################################### ETAPE2 ###################################

# DEFINITION DU MODULE 11
"MODULE11 trois classes disposées en triangle"


#
# #
def module11():
    classe(pos_X + (dim_X / 2), pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    classe(pos_X + dim_X, pos_Y, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)
    classe(pos_X, pos_Y, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)


# DEFINITION DU MODULE 12
"MODULE12 quatre classes disposées en S"


# #
# #
def module12():
    classe(pos_X - (dim_X / 2), pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    module11()


# DEFINITION DU MODULE 13
"MODULE13 quatre classes disposées en U"


#    #
# #
def module13():
    classe(pos_X - (dim_X / 2), pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    classe(pos_X + (dim_X / 2), pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    classe(pos_X + dim_X, pos_Y, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)
    classe(pos_X - dim_X, pos_Y, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)


# DEFINITION DU MODULE 14
"MODULE14 quatre classes disposées en diagonales"
# #
# #
#


def module14():
    classe(pos_X - dim_X, pos_Y + dim_Y * 1.5, dim_Z / 2, dim_X, dim_Y, dim_Z)
    classe(pos_X, pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    classe(pos_X - dim_X, pos_Y + dim_Y / 2, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)
    classe(pos_X + dim_X, pos_Y + dim_Y / 2, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)
    classe(pos_X, pos_Y, (dim_Z + z) / 2, dim_X, dim_Y, dim_Z + z)


# DEFINITION DU MODULE 15
"MODULE15 cinq classes disposées en W"


# # #
# #
def module15():
    classe(pos_X + dim_X * 1.5, pos_Y + dim_Y, dim_Z / 2, dim_X, dim_Y, dim_Z)
    module12()


# DEFINITION DU MODULE DE BASE
"Choix aléatoire parmis les modules 11, 12, 13, 14, 15"
"Etabissement d'un père et groupement des classes"

liste = [module11, module12, module13, module14, module15]
m = random.choice(liste)

global moduleDeBase


def moduleDeBase():
    global lo
    global m
    if m == module11 or m == module12 or m == module15:
        lo = dim_X * 2  # longueur des modules
    if m == module13 or m == module14:
        lo = dim_X * 3  # longueur des modules
    m()
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.parent_set(type="OBJECT", keep_transform=False)
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.join()


################################### ETAPE3 ###################################

# DEFINITION DES TRANSFORMATIONS
"Définition des différents moyens d'expansion"


def duplication(x, y, z):
    bpy.ops.object.duplicate_move(TRANSFORM_OT_translate={"value": (x, y, z)})


def translation(x, y, z):
    context = bpy.context
    obj = context.active_object
    obj.location += Vector([x, y, z])


def rotation(x):
    bpy.context.object.rotation_euler[2] = x


def symetrie():
    bpy.ops.transform.mirror(constraint_axis=(False, True, False))


def symetrie2():
    bpy.ops.transform.mirror(constraint_axis=(True, False, False))


# DEFINITION DU MODULE 21
# MODULE21 4*modules de base
#         rotation et décalage
#         organisation autours de l'espace servant carré


def espaceServant11():
    classe(pos_X, pos_Y - lo + dim_Y / 2, (dim_Z + z) / 2, lo, lo, dim_Z + z)


def espaceServant12():
    classe(pos_X - dim_X / 2, pos_Y - lo + dim_Y, (dim_Z + z) / 2, lo, lo, dim_Z + z)


def espaceServant13():
    classe(
        pos_X + dim_X / 2,
        pos_Y - lo + dim_Y,
        (dim_Z + z) / 2,
        lo + dim_X,
        lo + dim_Y,
        dim_Z + z,
    )


def module21():
    i = 0
    while i < 4:
        moduleDeBase()
        translation(-2 * dim_X + dim_X / 2, -lo + dim_Y / 2, 0)
        rotation(1.5708)
        i = i + 1
    if m == module11 or m == module12 or m == module15:
        espaceServant11()
    if m == module13:
        espaceServant12()
    if m == module14:
        espaceServant13()


# DEFINITION DU MODULE 22
# MODULE22 3*modules de base
#          symétrie et décalage
#          disposition de part et d'autre d'un espace servant longitudinal

c = random.randint(2, 4)


def espaceServant21():
    classe(
        pos_X + (c * 2 * dim_X + dim_X),
        pos_Y - 0.75 * dim_Y,
        (dim_Z + z) / 2,
        dim_X + c * 3 * dim_X + dim_X,
        dim_Y / 2,
        dim_Z + z,
    )


def espaceServant22():
    classe(
        pos_X + (c * 2 * dim_X + dim_X),
        pos_Y - 0.75 * dim_Y,
        (dim_Z + z) / 2,
        dim_X + c * lo + dim_X,
        dim_Y + dim_Y / 2,
        dim_Z + z,
    )


def module22():
    i = 0
    while i < 3:
        moduleDeBase()
        translation(c * dim_X, -1.5 * dim_Y, 0)
        symetrie()
        i = i + 1

    if m == module11 or m == module12 or m == module15 or m == module13:
        espaceServant21()
    if m == module14:
        espaceServant22()


# DEFINITION DU MODULE 23
# MODULE23 4*modules de base
#          rotation et alignement
#          disposition autours de l'espace servant carré


def espaceServant31():
    classe(
        pos_X + dim_X / 4,
        pos_Y - (dim_Y) - dim_Y / 4,
        (dim_Z + z) / 2,
        lo - dim_X / 2,
        lo - dim_Y / 2,
        dim_Z + z,
    )


def espaceServant32():
    classe(
        pos_X - dim_X / 4,
        pos_Y - lo + dim_Y + dim_Y / 4,
        (dim_Z + z) / 2,
        lo - dim_X / 2,
        lo - dim_Y / 2,
        dim_Z + z,
    )


def espaceServant33():
    classe(
        pos_X + dim_X - dim_X / 4,
        pos_Y - lo + dim_Y + dim_Y / 4,
        (dim_Z + z) / 2,
        lo + dim_X / 2,
        lo + dim_Y / 2,
        dim_Z + z,
    )


def module23():
    i = 0
    while i < 4:
        moduleDeBase()
        translation(-1.5 * dim_X + dim_X / 2, -lo + dim_Y / 2, 0)
        rotation(1.5708)
        i = i + 1
    if m == module11 or m == module12 or m == module15:
        espaceServant31()
    if m == module13:
        espaceServant32()
    if m == module14:
        espaceServant33()


# DEFINITION DU MODULE 24
# MODULE24 3*modules de base
#          rotation, alignement et décalage
#          disposition autours de l'espace servant rectangulaire


def espaceServant41():
    classe(pos_X + dim_X / 2, pos_Y - dim_Y, (dim_Z + z) / 2, lo, lo / 2, dim_Z + z)


def espaceServant42():
    classe(
        pos_X,
        pos_Y - lo + dim_Y + dim_Y / 2,
        (dim_Z + z) / 2,
        lo,
        lo - dim_Y,
        dim_Z + z,
    )


def espaceServant43():
    classe(pos_X, pos_Y - dim_Y, (dim_Z + z) / 2, lo, lo / 2 + dim_X / 2, dim_Z + z)


def module24():
    if m == module11 or m == module13 or m == module15:
        i = 1
        while i < 3:
            if i == 1:
                moduleDeBase()
                translation(-1.5 * dim_X + dim_X / 2, -lo + dim_Y / 2, 0)
                rotation(1.5708)
            if i == 2:
                moduleDeBase()
                symetrie()
                translation(0, -lo, 0)
            i = i + 1
        moduleDeBase()
        if m == module11 or m == module15:
            espaceServant41()
        if m == module13:
            espaceServant42()

    if m == module12:
        i = 1
        while i < 3:
            if i == 1:
                moduleDeBase()
                translation(-1.5 * dim_X + dim_X / 2, -lo + dim_Y / 2, 0)
                rotation(1.5708)
            if i == 2:
                moduleDeBase()
                symetrie()
                translation(0, -lo, 0)
            i = i + 1
        moduleDeBase()
        espaceServant41()

    if m == module14:
        i = 1
        while i < 3:
            if i == 1:
                moduleDeBase()
                translation(-2 * dim_X + dim_X / 2, -lo + dim_Y * 2, 0)
                rotation(1.5708)
            if i == 2:
                moduleDeBase()
                symetrie()
                translation(0, -2 * dim_Y, 0)
            i = i + 1
        moduleDeBase()
        espaceServant43()


# DEFINITION DU MODULE 25
# MODULE23 2*modules de base
#          symétrie
#          disposition de part et d'autre d'un espace servant longitudinal


def espaceServant51():
    classe(
        pos_X, pos_Y + dim_Y * 1.75, (dim_Z + z) / 2, dim_X + lo, dim_Y / 2, dim_Z + z
    )


def espaceServant52():
    classe(
        pos_X,
        pos_Y + dim_Y * (-0.75),
        (dim_Z + z) / 2,
        dim_X + lo,
        dim_Y / 2,
        dim_Z + z,
    )


def module25():
    i = 0
    while i < 2:
        moduleDeBase()
        translation(0, -1.5 * dim_Y, 0)
        symetrie()
        i = i + 1
    if m == module11 or m == module12 or m == module15 or m == module14:
        espaceServant52()
    if m == module13:
        espaceServant52()


# DEFINITION DU MODULE DE BASE 2
"Choix aléatoire parmis les modules 21, 22, 23, 24, 25"

liste2 = [module21, module22, module23, module24, module25]
mm = random.choice(liste2)

global moduleDeBase2


def moduleDeBase2():
    global mm
    global LO
    mm()
    if mm == module21 or mm == module22 or mm == module25:
        LO = dim_X * 2
    if mm == module23 or mm == module24:
        LO = dim_X * 3
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.join()


################################### ETAPE4 ###################################
"Etablissement de l'étage"

# DEFINITION DE L'ETAGE1
"Etage sur tout l'espace servant et sur une partie du RDC"

z = z + 3


def moduleDeBase3():
    moduleDeBase2()


################################### ETAPE5 ###################################

# DEFINITION DU MODULE 41
# MODULE21 2* modules de base3
#         symétrie, translation et imbrication


def module41():
    moduleDeBase3()
    if mm == module21 or mm == module23:
        duplication(pos_X + lo, pos_Y + lo, pos_Z)
        symetrie()

    if mm == module22:
        duplication(pos_X + dim_X * 1.5, pos_Y + lo + dim_Y / 2, pos_Z)
        symetrie()

    if mm == module24:
        if m == module11 or m == module12:
            duplication(pos_X, pos_Y + 2 * lo, pos_Z)
            symetrie()
        if m == module13:
            duplication(pos_X + dim_X, pos_Y + 2 * lo - dim_Y, pos_Z)
            symetrie()
        if m == module14 or m == module15:
            duplication(pos_X, pos_Y + lo + dim_Y * 1.5, pos_Z)
            symetrie()

    if mm == module25:
        if m == module11:
            duplication(pos_X + lo / 3, pos_Y + lo * 2 - dim_Y / 2, pos_Z)
            symetrie()
        if m == module12 or m == module15:
            duplication(pos_X + lo, pos_Y + lo * 2 - dim_Y / 2, pos_Z)
            symetrie()
        if m == module13 or m == module14:
            duplication(pos_X + dim_X * 2, pos_Y + lo + dim_Y / 2, pos_Z)
            symetrie()


# DEFINITION DU MODULE 42
# MODULE21 2* modules de base3
#         symétrie, translation et addition


def module42():
    moduleDeBase3()
    if mm == module21 or mm == module23:
        if m == module11:
            duplication(pos_X + lo, pos_Y + lo * 3, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X, pos_Y + lo, pos_Z + dim_Z / 2, dim_X * 2, dim_Y, dim_Z
            )
        if m == module12:
            duplication(pos_X + lo, pos_Y + lo * 3, pos_Z)
            symetrie()
            classe(pos_X, pos_Y + lo, pos_Z + dim_Z / 2, dim_X * 2, dim_Y, dim_Z)
        if m == module13:
            duplication(pos_X + 2 * dim_X, pos_Y + lo * 2 + dim_Y, pos_Z)
            symetrie()
            classe(pos_X, pos_Y + dim_Y * 2, pos_Z + dim_Z / 2, dim_X * 2, dim_Y, dim_Z)
        if m == module14:
            duplication(pos_X + 2 * dim_X, pos_Y + lo * 2 + dim_Y, pos_Z)
            symetrie()
            classe(pos_X, pos_Y + dim_Y * 2, pos_Z + dim_Z / 2, dim_X, dim_Y, dim_Z)
        if m == module15:
            duplication(pos_X + lo + dim_X, pos_Y + lo * 3, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
    if mm == module22:
        if m == module11 or m == module12:
            duplication(pos_X + lo, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
            classe(
                pos_X + (c + 1) * lo,
                pos_Y + lo,
                pos_Z + dim_Z / 2,
                dim_X * 3,
                dim_Y,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + lo, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
            classe(
                pos_X + (c + 1) * 2 * dim_X,
                pos_Y + 2 * dim_Y,
                pos_Z + dim_Z / 2,
                dim_X * 3,
                dim_Y,
                dim_Z,
            )
        if m == module14 or m == module15:
            duplication(pos_X + lo, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
            classe(
                pos_X + (c) * 2.25 * dim_X,
                pos_Y + 2 * dim_Y,
                pos_Z + dim_Z / 2,
                dim_X * 4,
                dim_Y,
                dim_Z,
            )
    if mm == module24:
        if m == module12:
            duplication(pos_X + lo, pos_Y + lo * 3, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module11:
            duplication(pos_X + lo + dim_X, pos_Y + dim_Y * 5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 1.5,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 3,
                dim_Y,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + lo, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module14:
            duplication(pos_X + 4 * dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
            classe(
                pos_X + 1.5 * dim_X,
                pos_Y + dim_Y * 1.5,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module15:
            duplication(pos_X + lo, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
    if mm == module25:
        if m == module12:
            duplication(pos_X + lo, pos_Y + lo * 2.75, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module11:
            duplication(pos_X + lo + dim_X, pos_Y + dim_Y * 5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 1.5,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 3,
                dim_Y,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + lo, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module14:
            duplication(pos_X + 4 * dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
            classe(
                pos_X + 1.5 * dim_X,
                pos_Y + dim_Y * 1.5,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module15:
            duplication(pos_X + lo, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X,
                pos_Y + dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )


# DEFINITION DU MODULE 43
# MODULE21 2* modules de base3
#         symétrie, translation et accolement


def module43():
    moduleDeBase3()
    if mm == module21:
        if m == module11:
            duplication(pos_X + dim_X, pos_Y + dim_Y * 5, pos_Z)
            symetrie()
        if m == module12:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 5, pos_Z)
            symetrie()
        if m == module13 or m == module15:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 6, pos_Z)
            symetrie()
        if m == module14:
            duplication(pos_X + dim_X * 3, pos_Y + dim_Y * 6, pos_Z)
            symetrie()
    if mm == module22 or mm == module25:
        if m == module11 or m == module12 or m == module13 or m == module15:
            duplication(pos_X + dim_X / 2, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
        if m == module14:
            duplication(pos_X + dim_X / 2, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
    if mm == module23:
        if m == module11:
            duplication(pos_X + dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
        if m == module12:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 4.5, pos_Z)
            symetrie()
        if m == module13 or m == module15:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
        if m == module14:
            duplication(pos_X + dim_X * 3, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()
    if mm == module24:
        if m == module11:
            duplication(pos_X + dim_X, pos_Y + dim_Y * 4, pos_Z)
            symetrie()
        if m == module12:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 4, pos_Z)
            symetrie()
        if m == module13 or m == module15:
            duplication(pos_X + dim_X * 2, pos_Y + dim_Y * 5, pos_Z)
            symetrie()
        if m == module14:
            duplication(pos_X + dim_X, pos_Y + dim_Y * 5.5, pos_Z)
            symetrie()


# DEFINITION DU MODULE 44
# MODULE21 2* modules de base3
#         rotation et accolement


def module44():
    moduleDeBase3()
    if mm == module21:
        if m == module11 or m == module12:
            duplication(pos_X + lo + dim_X / 2, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
        if m == module13:
            duplication(pos_X + lo, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
        if m == module14:
            duplication(pos_X + dim_X, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
        if m == module15:
            duplication(pos_X + lo + dim_X * 1.5, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
    if mm == module22:
        duplication(pos_X - dim_X * 4, pos_Y + dim_Y * 4, pos_Z)
        rotation(1.5708)
    if mm == module23:
        if m == module11:
            duplication(pos_X + lo, pos_Y + dim_Y * 3.5, pos_Z)
            rotation(1.5708)
        if m == module12:
            duplication(pos_X + 1.5 * dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            rotation(1.5708)
        if m == module13:
            duplication(pos_X + lo, pos_Y + lo * 1.5, pos_Z)
            rotation(1.5708)
        if m == module14:
            duplication(pos_X + dim_X, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
        if m == module15:
            duplication(pos_X + lo + dim_X * 1.5, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
    if mm == module24:
        if m == module11:
            duplication(pos_X + 1.5 * dim_X, pos_Y + dim_Y * 3.5, pos_Z)
            rotation(1.5708)
        if m == module12:
            duplication(pos_X + dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            rotation(1.5708)
        if m == module13:
            duplication(pos_X + 2.5 * dim_X, pos_Y + lo * 1.5, pos_Z)
            rotation(1.5708)
        if m == module14:
            duplication(pos_X + dim_X, pos_Y + lo * 2, pos_Z)
            rotation(1.5708)
        if m == module15:
            duplication(pos_X + lo + dim_X, pos_Y + dim_Y * 4.5, pos_Z)
            rotation(1.5708)
    if mm == module25:
        if m == module11:
            duplication(pos_X + 1.25 * dim_X, pos_Y + dim_Y * 2.75, pos_Z)
            rotation(1.5708)
        if m == module12:
            duplication(pos_X + 1.25 * dim_X, pos_Y + dim_Y * 3.25, pos_Z)
            rotation(1.5708)
        if m == module13:
            duplication(pos_X + 2.25 * dim_X, pos_Y + 3.25 * dim_Y, pos_Z)
            rotation(1.5708)
        if m == module14:
            duplication(pos_X + lo * 0.75, pos_Y + lo * 1.25, pos_Z)
            rotation(1.5708)
        if m == module15:
            duplication(pos_X + 2.25 * dim_X, pos_Y + dim_Y * 3.25, pos_Z)
            rotation(1.5708)


# DEFINITION DU MODULE 45
# MODULE21 2* modules de base3
#         symétrie et addition


def module45():
    moduleDeBase3()
    if mm == module21:
        if m == module11 or m == module12:
            duplication(pos_X + 4 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4,
                pos_Y - dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + 3 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4,
                pos_Y - dim_Y * 2.5,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y * 2,
                dim_Z,
            )
        if m == module14:
            duplication(pos_X + 3 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 5,
                pos_Y - dim_Y / 2,
                pos_Z + dim_Z / 2,
                dim_X,
                dim_Y,
                dim_Z,
            )
        if m == module15:
            duplication(pos_X + 4 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4,
                pos_Y - dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y * 3,
                dim_Z,
            )
    if mm == module22:
        duplication(pos_X + 2.5 * lo, pos_Y, pos_Z)
        symetrie()
    if mm == module23:
        if m == module11:
            duplication(pos_X + 4 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4.25,
                pos_Y - dim_Y * 1.5,
                pos_Z + dim_Z / 2,
                dim_X * 2.5,
                dim_Y,
                dim_Z,
            )
        if m == module12:
            duplication(pos_X + 4 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4.25,
                pos_Y - dim_Y,
                pos_Z + dim_Z / 2,
                dim_X * 2.5,
                dim_Y * 2,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + 3 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 4.25,
                pos_Y - dim_Y * 2,
                pos_Z + dim_Z / 2,
                dim_X * 2.5,
                dim_Y * 2,
                dim_Z,
            )
        if m == module14:
            duplication(pos_X + 3 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 5.25,
                pos_Y,
                pos_Z + dim_Z / 2,
                dim_X * 1.5,
                dim_Y,
                dim_Z,
            )
        if m == module15:
            duplication(pos_X + 3.25 * lo, pos_Y, pos_Z)
            symetrie()
            classe(
                pos_X + dim_X * 3.5,
                pos_Y - dim_Y * 1.5,
                pos_Z + dim_Z / 2,
                dim_X,
                dim_Y * 3,
                dim_Z,
            )
    if mm == module24:
        if m == module11 or m == module12 or m == module15:
            duplication(pos_X + 4 * dim_X, pos_Y, pos_Z)
            symetrie2()
            classe(
                pos_X + dim_X * 2.5,
                pos_Y - dim_Y,
                pos_Z + dim_Z / 2,
                dim_X * 2,
                dim_Y * 3,
                dim_Z,
            )
        if m == module13:
            duplication(pos_X + 4 * dim_X, pos_Y, pos_Z)
            symetrie2()
            classe(
                pos_X + dim_X * 2,
                pos_Y - dim_Y * 1.5,
                pos_Z + dim_Z / 2,
                dim_X,
                dim_Y * 4,
                dim_Z,
            )
        if m == module14:
            duplication(pos_X + 6 * dim_X, pos_Y, pos_Z)
            symetrie2()
            classe(
                pos_X + dim_X * 3,
                pos_Y - dim_Y,
                pos_Z + dim_Z / 2,
                dim_X * 3,
                dim_Y * 3,
                dim_Z,
            )
    if mm == module25:
        duplication(pos_X + 5 * dim_X, pos_Y, pos_Z)
        symetrie2()
        classe(
            pos_X + dim_X * 2.5,
            pos_Y - dim_Y * 0.75,
            pos_Z + dim_Z / 2,
            dim_X * 2,
            dim_Y * 2.5,
            dim_Z,
        )


# DEFINITION DU PROJET TOTAL
"Choix aléatoire parmis les modules 41, 42, 43, 44, 45"

liste3 = [module41, module42, module43, module44, module45]
mmmm = random.choice(liste3)


def projetTotal():
    mmmm()
    bpy.ops.object.select_all(action="SELECT")


projetTotal()


################################## CONSOLE ###################################


# DEFINITION DES PARAMETRES CONSOLE

if m == module11:
    nom1 = "trois classes disposees en triangle"
    nb1 = 5
    c1 = 11
if m == module12:
    nom1 = "quatre classes disposees en S"
    nb1 = 6
    c1 = 12
if m == module13:
    nom1 = "quatre classes disposees en U"
    nb1 = 6
    c1 = 13
if m == module14:
    nom1 = "quatre classes disposees en diagonales"
    nb1 = 7
    c1 = 14
if m == module15:
    nom1 = "cinq classes disposees en W"
    nb1 = 7
    c1 = 15


if mm == module21:
    nom2 = "    -rotation et decalage \n    -organisation autours de l'espace servant carre"
    nb2 = 4
    c2 = 21
if mm == module22:
    nom2 = "    -symetrie et decalage\n    -disposition de part et d'autre d'un espace servant longitudinal"
    nb2 = 3
    c2 = 22
if mm == module23:
    nom2 = "    -rotation et alignement\n    -disposition autours de l'espace servant carre"
    nb2 = 4
    c2 = 23
if mm == module24:
    nom2 = "    -rotation, alignement et decalage\n    -disposition autours de l'espace servant rectangulaire"
    nb2 = 3
    c2 = 24
if mm == module25:
    nom2 = "    -symetrie\n    -disposition de part et d'autre d'un espace servant longitudinal"
    nb2 = 2
    c2 = 25

if mmmm == module41:
    nom3 = "    -symetrie, translation et imbrication"
    c3 = 41
if mmmm == module42:
    nom3 = "    -symetrie, translation et addition"
    c3 = 42
if mmmm == module43:
    nom3 = "    -symetrie, translation et accolement"
    c3 = 43
if mmmm == module44:
    nom3 = "    -rotation et accolement"
    c3 = 44
if mmmm == module45:
    nom3 = "    -symetrie et addition"
    c3 = 45


print(
    " \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n "
)


print("---------------- ECOLES PRIMAIRES TOULOUSE LE MIRAIL  ----------------")
print()
print(" Classe de dimensions: ", (dim_X), " x ", (dim_Y), " x ", (dim_Z), " m")
print()
print(" Le module de base est constitue de ", (nom1), ".")
print()
print(" Le module de base est reproduit ", (nb2), " fois, puis ils sont disposes par:")
print(nom2)
print()
print(" Etage sur tout l'espace servant et sur une partie du RDC")
print()
print(" L'ensemble est alors duplique, puis organise par :")
print(nom3)
print()
print(
    " Au total, il y a ",
    (nb1) * (nb2) * 2,
    "salles de classes pour cette configuration.",
)
print()
# print("C:",c1,"-",c2,"-",c3)
# print()
print("----------------------------------------------------------------------")

print()
