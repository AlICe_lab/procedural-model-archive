# SPDX-FileCopyrightText: 2025 Angela Farina & Léo Joachim
# SPDX-FileCopyrightText: 2025 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random

# Suppression des objets existants
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()


def cube_quadrant(nbr_cubes, block_sizes, x_positive=True, y_positive=True):
    # Définir l'orientation du quadrant
    orientation_x = 1 if x_positive else -1
    orientation_y = 1 if y_positive else -1

    for key, val in nbr_cubes.items():
        for _ in range(val):
            # Définir les dimensions et la position aléatoire des cubes
            z_height = random.choice([2, 2.5, 3]) if "structure" not in key else 10
            x_width = block_sizes[key][0]
            y_depth = block_sizes[key][1]
            # rotation alétoire
            rotee = random.choice([True, False])
            if rotee == True:
                x_width = block_sizes[key][1]
                y_depth = block_sizes[key][0]

            x = random.uniform(0.5 + x_width / 2, 5 - x_width / 2)
            y = random.uniform(0.5 + y_depth / 2, 5 - y_depth / 2)
            z = random.uniform(0 + z_height / 2, 10 - z_height / 2)

            # Créer le cube avec une rotation aléatoire
            make_cube(
                (x_width, y_depth, z_height),
                (x * orientation_x, y * orientation_y, z),
                key,
            )
            bpy.ops.object.select_all(action="DESELECT")


def make_cube(size, location, name):
    # Ajouter un cube
    bpy.ops.mesh.primitive_cube_add(
        size=1,
        scale=(size[0], size[1], size[2]),
        location=(location[0], location[1], location[2]),
    )
    cube = bpy.context.object
    cube.name = name


# Deuxième quadrant 5X5X10
nbr_cubes = {
    "epaisse": 1,
    "moyen epaisse": 6,
    "moyen fine": 5,
    "fin": 4,
    "tres fine": 22,
}
block_sizes = {
    "epaisse": (0.15, 4.5),
    "moyen epaisse": (0.15, 4),
    "moyen fine": (0.15, 2.5),
    "fin": (0.15, 1.5),
    "tres fine": (0.15, 0.8),
}
cube_quadrant(nbr_cubes, block_sizes, x_positive=False, y_positive=True)


# Premier quadrant 5X5X10
nbr_cubes = {
    "epaisse": 0,
    "moyen epaisse": 5,
    "moyen fine": 7,
    "fin": 5,
    "tres fine": 21,
}
block_sizes = {
    "epaisse": (0.15, 4.5),
    "moyen epaisse": (0.15, 4),
    "moyen fine": (0.15, 2.5),
    "fin": (0.15, 1.5),
    "tres fine": (0.15, 0.8),
}
cube_quadrant(nbr_cubes, block_sizes, x_positive=True, y_positive=True)


# Troisième quadrant 5X5X10
nbr_cubes = {
    "epaisse": 1,
    "moyen epaisse": 3,
    "moyen fine": 7,
    "fin": 4,
    "tres fine": 28,
}
block_sizes = {
    "epaisse": (0.15, 4.5),
    "moyen epaisse": (0.15, 4),
    "moyen fine": (0.15, 2.5),
    "fin": (0.15, 1.5),
    "tres fine": (0.15, 0.8),
}
cube_quadrant(nbr_cubes, block_sizes, x_positive=True, y_positive=False)

# Quatrième quadrant 5X5X10
nbr_cubes = {
    "epaisse": 1,
    "moyen epaisse": 2,
    "moyen fine": 10,
    "fin": 8,
    "tres fine": 36,
}
block_sizes = {
    "epaisse": (0.15, 4.5),
    "moyen epaisse": (0.15, 4),
    "moyen fine": (0.15, 2.5),
    "fin": (0.15, 1.5),
    "tres fine": (0.15, 0.8),
}
cube_quadrant(nbr_cubes, block_sizes, x_positive=False, y_positive=False)
