# SPDX-FileCopyrightText: 2020 Azélie Louarn & Pareli Akelian
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Gearg Nees - Schotter & ?
# Louarn Azelie and Akelian Pareli
# date 21/01/2021
# Option AIM 2020-2021 module 1

# ========================
# final variation script of the project

import bpy
from math import radians
from random import randint
from random import uniform

# we translated the Schotter by Goerg Nees through the script below.
# the script is first composed by a definiton of the list of angles from the work and secondly, the definition of the different layers of the project cube.
# first part : list of angles from the Schotter art of Goerg Nees analysis.
# the equation is starting with original settings (named variable : in order to find the value of the second step; first step : in order to find the second angle of rotation; constante value : in order to create a loop into the equation).


def make_steps(length, min=0, init=9, const=4):
    steps = [0]
    n = init
    while len(steps) < length:
        if n % 3 == 0:
            steps.append(steps[-1] + n)
            n += 1
        else:
            steps.append(steps[-1] + n)
            n -= const
    return steps


list_of_angles = make_steps(13)
print(list_of_angles)

# we changed the degrees values of the list of angles to radians ones in order to use them for the second part

radian_list_of_angle = [round(radians(angle), 2) for angle in list_of_angles]
print(radian_list_of_angle)

# second part : definition of the different layers wich composed our interpretation of the original work of the artist. Those are created depending on a probability (des), and so, each layers are composed by cubes of original settings : size (s), location (l), rotation (r) and variable settings : size (sv), location (lv), rotation (rv).

# n : the starting point of the location of the layer
# m : the last point of the location of the layer

# des: random throw between rand_1 et rand_2
# rand_1 : valeur minimal
# rand_2 : valeur maximal
# limit : limite de la valeur du "des"

# s : size
# l : location
# r : rotation

# sv : the size variable
# lv : the location variable
# rv1 : the rotation variable
# rv2 : the rotation variable

# axonometric view of the project


def each_layers_of_axonometric_view(
    n, m, rand_1, rand_2, limit, s, l, r, sv, lv, rv1, rv2
):
    for i in range(n, m):
        for j in range(n, m):
            for k in range(n, m):
                if i == n or j == n or k == n:
                    des = randint(rand_1, rand_2)
                    if des <= limit:
                        bpy.ops.mesh.primitive_cube_add(
                            size=s, location=(l * i, l * j, l * k), rotation=(r, r, r)
                        )
                    else:
                        bpy.ops.mesh.primitive_cube_add(
                            size=uniform(sv, s),
                            location=(
                                uniform(lv, l) * i,
                                uniform(lv, l) * j,
                                uniform(lv, l) * k,
                            ),
                            rotation=(
                                uniform(rv1, rv2),
                                uniform(rv1, rv2),
                                uniform(rv1, rv2),
                            ),
                        )


# structure of the code : each_layers_of_axonometric_view(n,m,rand_1,rand_2,limit,s,l,r,sv,lv,rv1,rv2)

# Generating the original script
each_layers_of_axonometric_view(
    0,
    10,
    0,
    10,
    10,
    10,
    10,
    0,
    10,
    10,
    radian_list_of_angle[0],
    radian_list_of_angle[0],
)
each_layers_of_axonometric_view(
    1,
    9,
    0,
    10,
    5,
    10,
    10,
    0,
    10.5,
    9.8,
    radian_list_of_angle[0],
    radian_list_of_angle[4],
)
each_layers_of_axonometric_view(
    2,
    8,
    0,
    10,
    3,
    10,
    10,
    0,
    11.5,
    9.5,
    radian_list_of_angle[0],
    radian_list_of_angle[4],
)
each_layers_of_axonometric_view(
    3, 7, 0, 10, 2, 10, 10, 0, 12, 9.5, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    4, 6, 0, 10, 1, 10, 10, 0, 13, 10, radian_list_of_angle[0], radian_list_of_angle[4]
)

# Variation of the original script:

# Hight limit variation :
each_layers_of_axonometric_view(
    0,
    10,
    0,
    10,
    10,
    10,
    10,
    0,
    10,
    10,
    radian_list_of_angle[0],
    radian_list_of_angle[0],
)
each_layers_of_axonometric_view(
    1,
    9,
    0,
    10,
    7.5,
    10,
    10,
    0,
    20,
    10.5,
    radian_list_of_angle[0],
    radian_list_of_angle[4],
)
each_layers_of_axonometric_view(
    2, 8, 0, 10, 5, 10, 10, 0, 20, 9.8, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    3, 7, 0, 10, 2, 10, 10, 0, 30, 9.9, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    4, 6, 0, 10, 1, 10, 10, 0, 30, 10, radian_list_of_angle[0], radian_list_of_angle[4]
)

# Variation of several simultaneous parameters :
each_layers_of_axonometric_view(
    0,
    10,
    0,
    10,
    10,
    10,
    10,
    0,
    10,
    10,
    radian_list_of_angle[0],
    radian_list_of_angle[0],
)
each_layers_of_axonometric_view(
    1,
    9,
    0,
    10,
    7.5,
    10,
    10,
    0,
    20,
    10.5,
    radian_list_of_angle[0],
    radian_list_of_angle[4],
)
each_layers_of_axonometric_view(
    2, 8, 0, 10, 5, 10, 10, 0, 20, 9.8, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    3, 7, 0, 10, 2, 10, 10, 0, 30, 9.9, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    4, 6, 0, 10, 1, 10, 10, 0, 30, 10, radian_list_of_angle[0], radian_list_of_angle[4]
)

# Variation of location and size:
each_layers_of_axonometric_view(
    0,
    10,
    0,
    10,
    10,
    10,
    10,
    0,
    10,
    10,
    radian_list_of_angle[0],
    radian_list_of_angle[0],
)
each_layers_of_axonometric_view(
    1, 9, 0, 10, 5, 7, 10, 0, 15, 7, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    2, 8, 0, 10, 3, 5, 10, 0, 16, 8, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    3, 7, 0, 10, 2, 8, 10, 0, 17, 8, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    4, 6, 0, 10, 1, 8, 10, 0, 20, 10, radian_list_of_angle[0], radian_list_of_angle[4]
)

# Variation of the step (in the first part of the script) and limite and size :
each_layers_of_axonometric_view(
    0,
    10,
    0,
    10,
    10,
    10,
    10,
    0,
    10,
    10,
    radian_list_of_angle[0],
    radian_list_of_angle[0],
)
each_layers_of_axonometric_view(
    1, 9, 0, 10, 0, 10, 10, 0, 12, 9.8, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    2, 8, 0, 10, 0, 10, 10, 0, 15, 9.5, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    3, 7, 0, 10, 0, 10, 10, 0, 17, 9.5, radian_list_of_angle[0], radian_list_of_angle[4]
)
each_layers_of_axonometric_view(
    4, 6, 0, 10, 0, 10, 10, 0, 20, 10, radian_list_of_angle[0], radian_list_of_angle[4]
)
