# SPDX-FileCopyrightText: 2014 Marcello Frontini
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

##Blender 2.71 pour MacOS Hash:9337574

# MAISONS DE MEIER#

import bpy
import random


# Nettoyage
def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)


nettoyage()


# Definition des sols
def Sol(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4, 4 - b / 2, 0.4))


# Condition pour la position et la dimension des colonnes, des sols et de la toiture
b = random.randrange(0, 4, 3)

# Condition pour la position des escaliers
e = random.randrange(0, 11, 10)

# Condition pour la position du sol du deuxieme etage
s = random.randrange(1, 9, 7)

# Création de la passerelle d'access
bpy.ops.mesh.primitive_cube_add(location=(6, -4.5, 3))
bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
bpy.ops.transform.resize(value=(1, 5, 0.4))

# Création de la cheminée
bpy.ops.mesh.primitive_cube_add(location=(6, 6.7, 7))
bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
bpy.ops.transform.resize(value=(2, 1, 14))

# Definition du deplacement du premier etage
Deplacement_1 = random.randint(0, 1)

for i in range(-9, 0, 4):
    Sol(-i, 0, 3)
    Sol(-i - Deplacement_1, 4, 3)
    Sol(-i, 0 - b / 3, 6 - b / 2)

for i in range(-9, 0, 4):
    Sol(-i, 0 - b / 3, 6 - b / 2)


# Definition du balcon
def Sol_2(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4, 4.5 + b, 0.4))


# Création du sol du deuxieme etage
Sol_2(s + 0.5, 4, 6 - b / 2)

# Création du mur du long cote
bpy.ops.mesh.primitive_cube_add(location=(5, -2, 4.5))
bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
bpy.ops.transform.resize(value=(12.5, 0.5, 9 - b))


# Definition des murs des cotes courts
def Murs_CoteCourts(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.5, 4.5 - b / 3, 9 - b))


for i in range(-1, 12, 12):
    Murs_CoteCourts(i, 0.5 - b / 4, 4.5)


# Definition des piliers
def Pilier(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cylinder_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 0.3, 9 - b))


for i in range(-10, -6, 3):
    Pilier(-i + 0.1, 5.5, 4.5)

for i in range(-3, 1, 3):
    Pilier(-i + 0.1, 5.5, 4.5)

# Création de la toiture
bpy.ops.mesh.primitive_cube_add(location=(5, 2, 9 - b / 2))
bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
bpy.ops.transform.resize(value=(12, 8, 0.4))


def GardeCorps_CoteLong(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(11.5, 0.5, 1.5))


for i in range(-2, 7, 8):
    GardeCorps_CoteLong(5, i, 9.75 - b / 2)


def GardeCorps_CoteCourt(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.5, 8.5, 1.5))


for i in range(-1, 12, 12):
    GardeCorps_CoteCourt(i, 2, 9.75 - b / 2)

# Importation des escaliers
bpy.ops.import_scene.autodesk_3ds(
    filepath="/Users/marcellofrontini/Desktop/Escaliers.3ds"
)
bpy.ops.transform.resize(value=(0.6, 0.6, 0.6))
bpy.ops.transform.translate(value=(e + 0.4, 5.5, 0))

# Smith house - conditions
if Deplacement_1 == 1 and e == 0 and b == 0 and s == 1:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(9, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4.5, 4.5, 3.5))
    # Création d'un autre volume
    bpy.ops.mesh.primitive_cube_add(location=(-1.8, 0, 7.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(1, 4, 2.5))

# Douglas house - conditions
if Deplacement_1 == 0 and e == 0 and b == 0 and s == 1:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(5, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(12.5, 4.5, 3))

# Shamberg house - conditions
if b == 3 and s == 8:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, 1.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    for i in range(-10, -6, 3):
        Pilier(-i + 0.2, 3, 4.5)
    for i in range(-3, 1, 3):
        Pilier(-i + 0.2, 2, 4.5)
    # Nettoyage des sols du premier etage et de la cheminée
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.059"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.060"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.063"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.042"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.043"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.045"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.046"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.048"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.049"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.041"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Model.002"].select = True
    bpy.ops.object.delete(use_global=False)

# Récomposition 1 - conditions
if Deplacement_1 == 1 and b == 0 and e == 10 and s == 1:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(5, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(12.5, 4.5, 3))

# Récomposition 2 - conditions
if Deplacement_1 == 0 and b == 0 and e == 10 and s == 8:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(9, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4.5, 4.5, 3))

# Récomposition 3 - conditions
if Deplacement_1 == 1 and b == 0 and e == 10 and s == 8:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(5, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(12.5, 4.5, 3))

# Récomposition 4 - conditions
if Deplacement_1 == 0 and b == 0 and e == 10 and s == 1:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(9, 0, 11))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4.5, 4.5, 3))

# Récomposition 5 - conditions
if b == 3 and s == 1:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, 1.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
    for i in range(-10, -6, 3):
        Pilier(-i + 0.2, 3, 4.5)
    for i in range(-3, 1, 3):
        Pilier(-i + 0.2, 2, 4.5)
    # Création de l'access à la toiture
    bpy.ops.mesh.primitive_cube_add(location=(5, 0, 9))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(12.5, 4.5, 3))
    # Nettoyage des sols du premier etage et de la cheminée
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.059"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.060"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.063"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.042"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.043"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.045"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.046"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.048"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.049"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Cube.041"].select = True
    bpy.ops.object.delete(use_global=False)
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["Model.002"].select = True
    bpy.ops.object.delete(use_global=False)

# Récomposition 6 - conditions
if Deplacement_1 == 0 and b == 0 and e == 0 and s == 8:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))

# Récomposition 7 - conditions
if Deplacement_1 == 1 and b == 0 and e == 0 and s == 8:
    # Création de la base
    bpy.ops.mesh.primitive_cube_add(location=(5, 2, -0.2))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(30, 30, 0.6))
