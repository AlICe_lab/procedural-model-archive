# SPDX-FileCopyrightText: 2013 Edouard Capel
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Blender 2.68.0 - Windows - r58537
# Par : Edouard capel
# Script-METABOLISME

# -------------------------------------------------------------------------------------------#
#                                M  E  T  A  B  O  L  I  S  M  E                            #
# -------------------------------------------------------------------------------------------#

"""                 ----------------------------------------------------------
                            D E F A U L T
                    ----------------------------------------------------------"""

# Input
import bpy
import random
import mathutils
from math import sqrt

# --------------------------------------------------
# Delete All
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
# --------------------------------------------------
# Scene
SCENE = bpy.context.scene

"""                 ----------------------------------------------------------
                            G L O B A L   V A L U E
                    ----------------------------------------------------------"""

# EXPLICATION DES VARIABLES
# C_ = Contrainte   Contraints les possibilitees (Voir Console)
# H_ = Hasard       Valeur prise au hasard
# I_ = Indice       Indice direct sur l'objet
# L_ = Liste        Liste (Variant selon les choix)
# R_ = Range        Variable inscrit dans un Range
# T_ = Tuple        Tuple (Variant selon les choix)
# V_ = Valeur       Valeur (Variant selon les choix)
# --------------------------------------------------
# Format
I_GLOBALE = 5  # 4 < Petit / # 5 = Normal / # 6 > Grand   ( Attention != 0 )
# --------------------------------------------------
# Valeur a ZERO
I_REEL = 0  # Doit etre = 0
# --------------------------------------------------
# Rotation
PI = 3.1415926535897932384626433832795
ROTATION = [
    0,  # 0 - 0°
    (PI / 6),  # 1 - 30°
    (PI / 3),  # 2 - 60°
    (PI / 2),  # 3 - 90°
    (PI / 6 * 4),  # 4 - 120°
    (PI / 6 * 5),  # 5 - 150°
    PI,  # 6 - 180°
    PI + (PI / 6),  # 7 - 210°
    PI + (PI / 3),  # 8 - 240°
    PI * 1.5,  # 9 - 270°
    PI + (PI / 6 * 4),  # 10 - 300°
    PI + (PI / 6 * 5),
]  # 11 - 330°
# --------------------------------------------------
# Couleur - ROUGE
ROUGE = bpy.data.materials.new("Rouge")
ROUGE.diffuse_color = (1, 0, 0)
ROUGE.diffuse_intensity = 1
ROUGE.specular_color = (1, 1, 1)
ROUGE.specular_intensity = 0.5
ROUGE.alpha = 1
ROUGE.ambient = 1
# --------------------------------------------------
# Couleur - BLANC
BLANC = bpy.data.materials.new("Blanc")
BLANC.diffuse_color = (1, 1, 1)
BLANC.diffuse_intensity = 1
BLANC.specular_color = (1, 1, 1)
BLANC.specular_intensity = 1
BLANC.alpha = 1
BLANC.ambient = 1
# --------------------------------------------------
# Point de Depart et Curseur
DEP_x = 0  # Position x dans l'espace
DEP_y = 0  # Position y dans l'espace
DEP_z = 0  # Position z dans l'espace
bpy.context.scene.cursor_location = (DEP_x, DEP_y, DEP_z)
L_COORD = [(DEP_x, DEP_y, DEP_z)]
# --------------------------------------------------

"""                 ----------------------------------------------------------
                            C O N S O L E   ( TEST )
                    ----------------------------------------------------------"""

# Contrainte --->           # 0:Aucune contrainte
# --------------------------------------------------
# 00 - METABOLISME
C_METABOLISME = 2  # 1:Megastructure / 2:Reseau / 3:Master-Form
# --------------------------------------------------
# 01 - NOEUD
C_NOEUD = 0  # 1:Ortho / 2:Diago
# --------------------------------------------------
# 02 - RESEAU
C_RESEAU = 0  # 1:Trame / 2:Dynamique / 3:Lineaire / 4:Croissant
# --------------------------------------------------
#   VALEURS
# --------------------------------------------------
# Valeur X,Y,Z,N,A
VAL_X = 1  # Largeur Unitaire
VAL_Y = I_GLOBALE  # Profondeur Unitaire
VAL_Z = 1  # Hauteur Unitaire
VAL_N = 1  # Taille du Noeud
VAL_A = 1  # Taille de l'Architecture (Attention VAL_Y doit etre divisible par le VAL_A en nombre entier)
VAL_H = 3  # Hauteur de l'Architecture
VAL_YA = VAL_Y / VAL_A  # Archi (Debug)
# --------------------------------------------------
# Contrainte - Affichage
C_AFFICHAGE = (
    0  # 1:Reseau (Uniquement les Reseaux) / 2:Architecture (Uniquement les Bâtiments)
)
# --------------------------------------------------
# Contrainte de Formation
C_FORMATION = 0  # 0:Normal (Accepte le Hasard de Formation) / 1:Parfait (Formation Idéale) / 2:Dictée (Formation dictée selon les listes)
# --------------------------------------------------
#   LISTES
# --------------------------------------------------
# Reseau
L_REWIND = [0]
L_TYPE_RESEAU = [0]
L_TYPE_ARCHI = [0]
L_RESEAU_TRUE = [1]
L_ARCHI_TRUE = [0]
L_MOVE_TRUE = [1]

# --------------------------------------------------
# Indice de Masse
if C_FORMATION == 2:
    I_MASSE = L_MOVE_TRUE.count(0) + L_MOVE_TRUE.count(1)

"""                 ----------------------------------------------------------
                            A R B R E   D E   D E C I S I O N
                    ----------------------------------------------------------"""

"""""" """ """ """""" """""" """ """ """""" """ 0 0 """ """""" """ """ """""" """""" """ """ """"""
#   Selection Hasard = Type de Metabolisme
#   0:Megastructure / 1:Reseau / 2:Master-Form
# --------------------------------------------------
if C_METABOLISME == 0:  # Verification si contraint
    H_METABOLISME = random.randint(0, 2)
# CONTRAINTE
else:
    H_METABOLISME = C_METABOLISME - 1
# --------------------------------------------------
# Valeur et Titre
L_METABOLISME = ["Megastructure", "Reseau", "Master-Form"]

"""""" """ """ """""" """""" """ """ """""" """ 0 1 """ """""" """ """ """""" """""" """ """ """"""
#   Selection Hasard = Type de Noeud
#   0:Orthogonale / 1:Diagonale
# --------------------------------------------------
if C_NOEUD == 0:  # Verification si contraint
    H_NOEUD = random.randint(0, 1)
# CONTRAINTE
else:
    H_NOEUD = C_NOEUD - 1
# --------------------------------------------------
# Valeur et Titre
L_NOEUD = ["Orthogonale", "Diagonale"]

"""""" """ """ """""" """""" """ """ """""" """ 0 2 """ """""" """ """ """""" """""" """ """ """"""
#   Selection Hasard = Type de Reseau
#   0:Tramé / 1:Dynamique / 2:Linéaire / 3:Croissant
#   Indice de Masse
# --------------------------------------------------
if C_RESEAU == 0:  # Verification si contraint
    H_RESEAU = random.randint(0, 3)
# CONTRAINTE
else:
    H_RESEAU = C_RESEAU - 1
# --------------------------------------------------
# Valeur et Titre
L_RESEAU = ["Tramé", "Dynamique", "Linéaire", "Croissant"]

"""                 ----------------------------------------------------------
                            D E F I N I T I O N S   D E S   O B J E T S
                    ----------------------------------------------------------"""


# Dessiner le Mesh de l'Objet
def OBJET(
    NUMERO, TYPE, DRAW_TRUE, ARCH_X, ARCH_Y, ARCH_Z, POS_ARCH_X, POS_ARCH_Y, POS_ARCH_Z
):
    def COORDONEE(X, Y, Z, V_x, V_y, V_z):
        L_COORD.extend(
            [
                (DEP_x + V_x, DEP_y + V_y, DEP_z + V_z),  # 1 - Cube
                (DEP_x + V_x, DEP_y + Y + V_y, DEP_z + V_z),  # 2 - Cube Y
                (DEP_x + X + V_x, DEP_y + Y + V_y, DEP_z + V_z),  # 3 - Cube XY
                (DEP_x + X + V_x, DEP_y + V_y, DEP_z + V_z),  # 4 - Cube X
                (DEP_x + V_x, DEP_y + V_y, DEP_z + Z + V_z),  # 5 - Cube Z
                (DEP_x + V_x, DEP_y + Y + V_y, DEP_z + Z + V_z),  # 6 - Cube YZ
                (DEP_x + X + V_x, DEP_y + Y + V_y, DEP_z + Z + V_z),  # 7 - Cube XYZ
                (DEP_x + X + V_x, DEP_y + V_y, DEP_z + Z + V_z),  # 8 - Cube XZ
                (
                    DEP_x + X / 2 + V_x,
                    DEP_y + Y * 0.866 + V_y,
                    DEP_z + V_z,
                ),  # 9 - Pointe Diago
                (
                    DEP_x + X / 2 + V_x,
                    DEP_y + Y * 0.866 + V_y,
                    DEP_z + Z + V_z,
                ),  # 10 - Pointe Diago Z
                (
                    DEP_x + V_x,
                    DEP_y + Y * 0.577 + V_y,
                    DEP_z + Z + V_z,
                ),  # 11 - Pointe Helix Inverse Z
                (
                    DEP_x + X + V_x,
                    DEP_y + Y * 0.577 + V_y,
                    DEP_z + Z + V_z,
                ),  # 12 - Pointe Helix Inverse XZ
                (
                    DEP_x + X / 2 + V_x,
                    DEP_y - Y * 0.288 + V_y,
                    DEP_z + Z + V_z,
                ),  # 13 - Pointe Helix Inverse YZ
                (DEP_x + V_x, DEP_y - VAL_Y + V_y, DEP_z + V_z),  # 14 - Helix -1
                (DEP_x + X + V_x, DEP_y - VAL_Y + V_y, DEP_z + V_z),  # 15 - Helix -1 X
                (
                    DEP_x - VAL_Y * 0.866 + V_x,
                    DEP_y + VAL_Y * 0.5 + V_y,
                    DEP_z + V_z,
                ),  # 16 - Helix -2
                (
                    DEP_x + X / 2 - VAL_Y * 0.866 + V_x,
                    DEP_y + VAL_Y * 0.5 + Y * 0.866 + V_y,
                    DEP_z + V_z,
                ),  # 17 - Helix -2 X
                (
                    DEP_x + X / 2 + VAL_Y * 0.866 + V_x,
                    DEP_y + VAL_Y * 0.5 + Y * 0.866 + V_y,
                    DEP_z + V_z,
                ),  # 18 - Helix -3
                (
                    DEP_x + X + VAL_Y * 0.866 + V_x,
                    DEP_y + VAL_Y * 0.5 + V_y,
                    DEP_z + V_z,
                ),  # 19 - Helix -3 X
                (
                    DEP_x + V_x,
                    DEP_y + Y * 0.577 + VAL_Y + V_y,
                    DEP_z + Z + V_z,
                ),  # 20 - Helix -1 Z
                (
                    DEP_x + X + V_x,
                    DEP_y + Y * 0.577 + VAL_Y + V_y,
                    DEP_z + Z + V_z,
                ),  # 21 - Helix -1 XZ
                (
                    DEP_x + X + VAL_Y * 0.866 + V_x,
                    DEP_y - VAL_Y * 0.5 + Y * 0.577 - +V_y,
                    DEP_z + Z + V_z,
                ),  # 22 - Helix -2 Z
                (
                    DEP_x + X / 2 + VAL_Y * 0.866 + V_x,
                    DEP_y - VAL_Y * 0.5 - Y * 0.288 + V_y,
                    DEP_z + Z + V_z,
                ),  # 23 - Helix -2 XZ
                (
                    DEP_x + X / 2 - VAL_Y * 0.866 + V_x,
                    DEP_y - VAL_Y * 0.5 - Y * 0.288 + V_y,
                    DEP_z + Z + V_z,
                ),  # 24 - Helix -3 Z
                (
                    DEP_x - VAL_Y * 0.866 + V_x,
                    DEP_y - VAL_Y * 0.5 + Y * 0.577 + V_y,
                    DEP_z + Z + V_z,
                ),
            ]
        )  # 25 - Helix -3 XZ

    if TYPE == 0:  # Lien
        L_COORD = [(DEP_x, DEP_y, DEP_z)]
        COORDONEE(VAL_X, VAL_Y, VAL_Z / 2, 0, 0, 0)
        L_VERTICE = [
            (1, 2, 3, 4),
            (1, 5, 6, 2),
            (5, 6, 7, 8),
            (3, 4, 8, 7),
            (1, 4, 8, 5),
            (2, 3, 7, 6),
        ]
        COULEUR = ROUGE
        NOM = "0" + str(I_REEL + 1) + " Lien"
    elif 1 <= TYPE <= 5:  # Noeud
        L_COORD = [(DEP_x, DEP_y, DEP_z)]
        COORDONEE(VAL_X, VAL_N, VAL_Z / 2, 0, 0, 0)
        if 1 <= TYPE <= 3:  # Ortho
            L_VERTICE = [
                (1, 2, 3, 4),
                (1, 5, 6, 2),
                (5, 6, 7, 8),
                (3, 4, 8, 7),
                (1, 4, 8, 5),
                (2, 3, 7, 6),
            ]
        elif 4 <= TYPE <= 5:  # Diago
            L_VERTICE = [
                (1, 9, 4),
                (1, 5, 10, 9),
                (4, 8, 10, 9),
                (1, 4, 8, 5),
                (5, 8, 10),
            ]
        COULEUR = ROUGE
        NOM = "0" + str(I_REEL + 1) + " Noeud"
    elif TYPE == 6:  # Archi - Cube
        L_COORD = [(DEP_x, DEP_y, DEP_z)]
        COORDONEE(ARCH_X, ARCH_Y, ARCH_Z, POS_ARCH_X, POS_ARCH_Y, POS_ARCH_Z)
        L_VERTICE = [
            (1, 2, 3, 4),
            (1, 5, 6, 2),
            (5, 6, 7, 8),
            (3, 4, 8, 7),
            (1, 4, 8, 5),
            (2, 3, 7, 6),
        ]
        COULEUR = BLANC
        NOM = "0" + str(I_REEL + 1) + " Bâtiment ." + str(NUMERO)
    elif TYPE == 7:  # Helix
        L_COORD = [(DEP_x, DEP_y, DEP_z)]
        COORDONEE(VAL_X, VAL_N, VAL_H * 1.5, 0, 0, VAL_Z / 2)
        L_VERTICE = [
            (1, 16, 17, 9, 18, 19, 4, 15, 14),
            (11, 20, 21, 12, 22, 23, 13, 24, 25),
            (13, 14, 15),
            (11, 16, 17),
            (12, 18, 19),
            (1, 24, 25),
            (9, 20, 21),
            (4, 22, 23),
            (1, 14, 24),
            (14, 24, 13),
            (1, 25, 16),
            (25, 16, 11),
            (9, 17, 20),
            (17, 20, 11),
            (9, 21, 18),
            (21, 18, 12),
            (19, 12, 22),
            (22, 19, 4),
            (4, 15, 23),
            (13, 15, 23),
        ]
        COULEUR = BLANC
        NOM = "0" + str(I_REEL + 1) + " Helix"
    if DRAW_TRUE != 0:
        MESH = bpy.data.meshes.new(NOM)
        LIEN = bpy.data.objects.new(NOM, MESH)
        SCENE.objects.link(LIEN)
        MESH.from_pydata(L_COORD, [], L_VERTICE)
        MESH.materials.append(COULEUR)


# --------------------------------------------------
# Action de l'Architecture
def ARCHI_ACTION(
    NUMERO,
    D_TYPE,
    G_TYPE,
    D_TRUE,
    G_TRUE,
    D_X,
    D_Y,
    D_Z,
    D_POS_X,
    D_POS_Y,
    D_POS_Z,
    G_X,
    G_Y,
    G_Z,
    G_POS_X,
    G_POS_Y,
    G_POS_Z,
):
    OBJET(NUMERO, D_TYPE, D_TRUE, D_X, D_Y, D_Z, D_POS_X, D_POS_Y, D_POS_Z)
    OBJET(NUMERO, G_TYPE, G_TRUE, G_X, G_Y, G_Z, G_POS_X, G_POS_Y, G_POS_Z)


# --------------------------------------------------
# Appel du type d'Architecture
def ARCHI(NUMERO, TYPE, TYPE_RESEAU):
    if TYPE == 0:  # Archi - Cube Globale
        if TYPE_RESEAU == 0:
            H_ARCHI_X = random.randint(1, VAL_Y)
            ARCHI_ACTION(
                NUMERO,
                6,
                6,
                random.randint(0, 1),
                random.randint(0, 1),
                -(random.randint(1, VAL_Y)),
                VAL_Y,
                random.randint(1, VAL_H),
                0,
                0,
                0,
                random.randint(1, VAL_Y),
                VAL_Y,
                random.randint(1, VAL_H),
                1,
                0,
                0,
            )
    if TYPE == 1:  # Archi - Cube Individuel
        if TYPE_RESEAU == 0:
            for R_ARCHI in range(0, VAL_Y):
                ARCHI_ACTION(
                    NUMERO,
                    6,
                    6,
                    random.randint(0, 1),
                    random.randint(0, 1),
                    -(random.randint(1, VAL_Y)),
                    1,
                    random.randint(1, VAL_H),
                    0,
                    R_ARCHI,
                    0,
                    random.randint(1, 3),
                    1,
                    random.randint(1, VAL_H),
                    1,
                    R_ARCHI,
                    0,
                )
        if 1 <= TYPE_RESEAU <= 3:
            ARCHI_ACTION(
                NUMERO,
                6,
                6,
                random.randint(0, 1),
                random.randint(0, 1),
                -(random.randint(1, VAL_Y)),
                VAL_N,
                random.randint(1, VAL_H),
                0,
                0,
                0,
                random.randint(1, 3),
                VAL_N,
                random.randint(1, VAL_H),
                1,
                0,
                0,
            )
    if TYPE == 2:  # Archi - Helix
        if 4 <= TYPE_RESEAU <= 5:
            OBJET(NUMERO, 7, 1, 0, 0, 0, 0, 0, 0)


# --------------------------------------------------
# Action du deplacement
def DEPLACEMENT_ACTION(X, Y, Z, R):
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")
    SELECT = bpy.context.selected_objects
    SELECT = bpy.ops.transform.rotate(value=ROTATION[R], axis=(0, 0, 1))
    SELECT = bpy.ops.transform.translate(value=(X, Y, Z))


# --------------------------------------------------
# Appel du type de deplacement
def DEPLACEMENT(TYPE, Z):
    if TYPE == 0:  # Lien
        DEPLACEMENT_ACTION(0, -VAL_Y, Z, 0)
    elif TYPE == 1:  # Noeud / Ortho / Gauche
        DEPLACEMENT_ACTION(0, 0, Z, 9)
    elif TYPE == 2:  # Noeud / Ortho / Staight
        DEPLACEMENT_ACTION(0, -VAL_N, Z, 0)
    elif TYPE == 3:  # Noeud / Ortho / Droite
        DEPLACEMENT_ACTION(VAL_X, -VAL_N, Z, 3)
    elif TYPE == 4:  # Noeud / Diago / Gauche
        DEPLACEMENT_ACTION(0, 0, Z, 10)
    elif TYPE == 5:  # Noeud / Diago / Droite
        DEPLACEMENT_ACTION(VAL_X / 2, -VAL_N * 0.866, Z, 2)
    elif TYPE >= 6:  # Archi Speciale
        DEPLACEMENT_ACTION(0, 0, Z, 0)


# --------------------------------------------------
# Ordre d'appel
def ORDRE(
    NUMERO,
    TYPE_RESEAU,
    TYPE_ARCHI,
    RESEAU_TRUE,
    ARCHI_TRUE,
    MOVE_TRUE,
    TRIDIMENSION,
    REWIND,
):
    if RESEAU_TRUE == 1:
        OBJET(NUMERO, TYPE_RESEAU, 1, 0, 0, 0, 0, 0, 0)
    if ARCHI_TRUE == 1:
        ARCHI(NUMERO, TYPE_ARCHI, TYPE_RESEAU)
    if MOVE_TRUE == 1:
        DEPLACEMENT(TYPE_RESEAU, TRIDIMENSION)
    if REWIND != 0:
        if REWIND <= 5:
            L_COORD_REWIND = [(DEP_x, DEP_y, DEP_z), (DEP_x, DEP_y + 5, DEP_z)]
            L_VERTICE_REWIND = [(0, 1)]
            NOM = "Indice Rewind " + str(REWIND)
            MESH = bpy.data.meshes.new(NOM)
            LIEN = bpy.data.objects.new(NOM, MESH)
            SCENE.objects.link(LIEN)
            MESH.from_pydata(L_COORD_REWIND, [], L_VERTICE_REWIND)
        if REWIND >= 5:
            REWIND_ACTION(REWIND - 5)


# --------------------------------------------------
# Rewind Verification de le position
def REWIND_EN_POSITION(VERIF, R_REWIND, NUMERO):
    if VERIF <= 4.5:
        DEPLACEMENT_ACTION(0, 0, 0, 1)
    if R_REWIND == 11:
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects["Indice Rewind " + str(NUMERO)].select = True
        bpy.ops.object.delete()


# --------------------------------------------------
# Rewind
def REWIND_ACTION(NUMERO):
    for R_REWIND in range(12):
        bpy.data.objects["Indice Rewind " + str(NUMERO)].select = True
        O_REWIND = bpy.data.objects["Indice Rewind " + str(NUMERO)]
        bpy.ops.object.origin_set(type="ORIGIN_CURSOR")
        if R_REWIND == 0:
            COORD_REWIND = O_REWIND.matrix_world * O_REWIND.data.vertices[0].co
            DEPLACEMENT_ACTION(-COORD_REWIND[0], -COORD_REWIND[1], -COORD_REWIND[2], 0)
        if R_REWIND >= 1:
            COORD_REWIND = O_REWIND.matrix_world * O_REWIND.data.vertices[1].co
            REWIND_EN_POSITION(COORD_REWIND[1], R_REWIND, NUMERO)


"""                 ----------------------------------------------------------
                        G E N E R A T E U R S   D E S   F O R M A T I O N S
                    ----------------------------------------------------------"""

if C_FORMATION != 2:
    # Création du TIC-TAC
    L_TIC_TAC = [0]
    for TIC_TAC in range(200):
        L_TIC_TAC.extend([1])
        L_TIC_TAC.extend([0])
    # --------------------------------------------------
    # Création des Listes des Ordres
    L_REWIND = [0]
    L_TYPE_RESEAU = [0]
    L_TYPE_ARCHI = [0]
    L_RESEAU_TRUE = [0]
    L_ARCHI_TRUE = [0]
    L_MOVE_TRUE = [0]
    # Remove
    L_REWIND.remove(0)
    L_TYPE_RESEAU.remove(0)
    L_TYPE_ARCHI.remove(0)
    L_RESEAU_TRUE.remove(0)
    L_ARCHI_TRUE.remove(0)
    L_MOVE_TRUE.remove(0)

    # --------------------------------------------------
    # Definition de l'ajout au Ordre
    def TRANSLATE_TO_ORDRE(
        VALUE_REWIND,
        VALUE_TYPE_RESEAU,
        VALUE_TYPE_ARCHI,
        VALUE_RESEAU_TRUE,
        VALUE_ARCHI_TRUE,
        VALUE_MOVE_TRUE,
    ):
        L_REWIND.extend([VALUE_REWIND])
        L_TYPE_RESEAU.extend([VALUE_TYPE_RESEAU])
        L_TYPE_ARCHI.extend([VALUE_TYPE_ARCHI])
        L_RESEAU_TRUE.extend([VALUE_RESEAU_TRUE])
        L_ARCHI_TRUE.extend([VALUE_ARCHI_TRUE])
        L_MOVE_TRUE.extend([VALUE_MOVE_TRUE])

    # --------------------------------------------------
    # Def RYTHM
    def RYTHM(RESEAU, MASSE, NUMERO, H_A, H_B, H_C, H_D, H_E, H_F, H_G):
        if RESEAU == 1:  # Trame Ortho
            for R_TRAME in range(4):
                TRANSLATE_TO_ORDRE(0, 0, 0, 1, H_A, 1)
                TRANSLATE_TO_ORDRE(0, 1, 0, 1, 0, 1)
            for R_VALUE_A in range(H_A):
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                if R_VALUE_A == H_A - 1:
                    TRANSLATE_TO_ORDRE(0, H_B, 0, 0, 0, 1)
                else:
                    TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
        # --------------------------------------------------
        if RESEAU == 2:  # Dynamique Ortho
            TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
            TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
            for R_TRAME in range(3):
                if R_TRAME == 0:
                    TRANSLATE_TO_ORDRE(0, 1, 0, 1, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                    TRANSLATE_TO_ORDRE(0, 2, 1, 1, 1, 1)
                    if H_D == 1:
                        TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
                if R_TRAME == 1:
                    TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
                    TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
                    TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                    TRANSLATE_TO_ORDRE(0, 2, 1, 1, 1, 1)
                    if H_D == 2:
                        TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
                if R_TRAME == 2:
                    TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
                    TRANSLATE_TO_ORDRE(0, 3, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                    TRANSLATE_TO_ORDRE(0, 2, 1, 1, 1, 1)
                    if H_D == 3:
                        TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(7, 0, 0, 0, 0, 0)
        # --------------------------------------------------
        if RESEAU == 3:  # Linéaire Ortho
            for R_TRAME in range(4):
                TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                TRANSLATE_TO_ORDRE(0, 1, 0, 1, 0, 1)
            if L_TIC_TAC[NUMERO] == 0:
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
            else:
                for R_LINEAIRE in range(3):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 1, 0, 0, 0, 1)
                for R_LINEAIRE in range(3):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 3, 0, 0, 0, 1)
                if NUMERO >= H_E and NUMERO <= H_E + 1:
                    TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
                    for R_LINEAIRE in range(2):
                        TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                        TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
                    TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
            if NUMERO == H_E * 2:
                TRANSLATE_TO_ORDRE(7, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 5, 0, 1, 0, 1)
                TRANSLATE_TO_ORDRE(0, 0, 0, 1, 0, 1)
        # --------------------------------------------------
        if RESEAU == 4:  # Croissant Ortho
            for R_TRAME in range(3):
                if R_TRAME == 0:
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 0, 1)
                    TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
                elif R_TRAME == 1:
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 1, 1)
                else:
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 0, 1)
                TRANSLATE_TO_ORDRE(0, 1, 0, 1, 0, 1)
            TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(0, 3, 0, 0, 0, 1)
            for R_TRAME in range(H_F):
                TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
            TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(0, 1, 0, 0, 0, 1)
            TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
            TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
            for R_TRAME in range(H_G):
                TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
            TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
            TRANSLATE_TO_ORDRE(0, 2, 0, 0, 0, 1)
        # --------------------------------------------------
        # Diago
        if RESEAU == 11:  # Trame Diago
            for R_TRAME in range(6):
                TRANSLATE_TO_ORDRE(0, 0, 0, 1, 1, 1)
                TRANSLATE_TO_ORDRE(0, 4, 0, 1, 0, 1)
            for R_VALUE_A in range(H_A):
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
                if R_VALUE_A == H_A - 1:
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, H_C, 0, 0, 0, 1)
                else:
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
        # --------------------------------------------------
        if RESEAU == 12:  # Dynamique Diago
            TRANSLATE_TO_ORDRE(0, 0, 1, 1, 0, 1)
            TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
            for R_TRAME in range(2):
                if R_TRAME == 0:
                    TRANSLATE_TO_ORDRE(0, 4, 2, 1, 1, 1)
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 2, 0, 1, 0, 1)
                    if H_A == 1:
                        if NUMERO == H_E:
                            TRANSLATE_TO_ORDRE(3, 0, 0, 0, 0, 0)
                        TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
                if R_TRAME == 1:
                    TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
                    TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 2, 0, 1, 0, 1)
                    if H_A == 2:
                        if NUMERO == H_E:
                            TRANSLATE_TO_ORDRE(3, 0, 0, 0, 0, 0)
                        TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
            if NUMERO == (H_E * 2):
                TRANSLATE_TO_ORDRE(7, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(8, 0, 0, 0, 0, 0)
            else:
                TRANSLATE_TO_ORDRE(7, 0, 0, 0, 0, 0)
        # --------------------------------------------------
        if RESEAU == 13:  # Linéaire Diago
            if NUMERO == 1 + H_E * 2:
                TRANSLATE_TO_ORDRE(0, 0, 0, 1, 1, 1)
                TRANSLATE_TO_ORDRE(0, H_C, 0, 0, 0, 1)
            for R_TRAME in range(6):
                TRANSLATE_TO_ORDRE(0, 0, 0, 1, 1, 1)
                TRANSLATE_TO_ORDRE(0, 4, 0, 1, 0, 1)
            if L_TIC_TAC[NUMERO] == 0:
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 4, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
            else:
                for R_LINEAIRE in range(4):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 4, 0, 0, 0, 1)
                for R_LINEAIRE in range(5):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                    TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(0, 4, 0, 0, 0, 1)
        # --------------------------------------------------
        if RESEAU == 14:  # Croissant Diago
            if NUMERO <= 5:
                for R_CROISSANT in range(H_E):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 1, 1, 1)
                if NUMERO == H_E:
                    TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 4, 0, 1, 0, 1)
            elif NUMERO >= 6:
                TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 5, 0, 1, 0, 1)
                for R_CROISSANT in range(H_D):
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                TRANSLATE_TO_ORDRE(2, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 4, 0, 1, 0, 1)
                for R_CROISSANT in range(H_A):
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                TRANSLATE_TO_ORDRE(7, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 5, 0, 0, 0, 1)
                for R_CROISSANT in range(H_A):
                    TRANSLATE_TO_ORDRE(0, 0, 1, 1, 1, 1)
                TRANSLATE_TO_ORDRE(6, 0, 0, 0, 0, 0)
                TRANSLATE_TO_ORDRE(0, 4, 0, 0, 0, 1)
                for R_CROISSANT in range(H_E):
                    TRANSLATE_TO_ORDRE(0, 0, 0, 0, 0, 1)
                TRANSLATE_TO_ORDRE(1, 0, 0, 0, 0, 0)

    # --------------------------------------------------
    # Generateur des Reseau
    I_MASSE = random.randint(12, 18)
    HASARD = random.randint(2, 4)
    for R_RYTHM in range(I_MASSE):
        RYTHM(
            (H_RESEAU + 1) + (10 * H_NOEUD),
            I_MASSE,
            R_RYTHM,
            random.randint(1, 2),
            random.choice([1, 3]),
            random.choice([4, 5]),
            random.randint(1, 3),
            HASARD,
            random.randint(1, 5),
            random.randint(1, 5),
        )

"""                 ----------------------------------------------------------
                            D R A W
                    ----------------------------------------------------------"""

# New Indice de Masse
I_MASSE = L_MOVE_TRUE.count(0) + L_MOVE_TRUE.count(1)
# --------------------------------------------------
# Dessiner l'objet
if H_METABOLISME == 1:
    for R_DRAW in range(0, I_MASSE):
        ORDRE(
            R_DRAW,
            L_TYPE_RESEAU[R_DRAW],
            L_TYPE_ARCHI[R_DRAW],
            L_RESEAU_TRUE[R_DRAW],
            L_ARCHI_TRUE[R_DRAW],
            L_MOVE_TRUE[R_DRAW],
            0,
            L_REWIND[R_DRAW],
        )
        I_REEL = I_REEL + 1
# --------------------------------------------------
# Camera et Soleil
bpy.ops.object.lamp_add(
    type="SUN",
    view_align=False,
    location=(0, 0, 30),
    rotation=(PI / 6, 0, 0),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(0, 0, 100),
    rotation=(0, 0, 0),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
# --------------------------------------------------
# Center Object et Deselect All
bpy.ops.object.select_all(action="DESELECT")

"""                 ----------------------------------------------------------
                            P R I N T
                    ----------------------------------------------------------"""

# Prints
for REP in range(0, 100):
    print("")
print(" ************************************************")
print("")
print("              M E T A B O L I S M E")
print("")
print(" ************************************************")
print(" ------------------------------------------------")
print("")
print("      --------------------------------------     ")
print("      " + L_RESEAU[H_RESEAU])
print("      " + L_NOEUD[H_NOEUD])
print("      " + str((H_RESEAU + 1) + (10 * H_NOEUD)))
print("")
print(" ------------------------------------------------")
print(" ************************************************")
print("")
