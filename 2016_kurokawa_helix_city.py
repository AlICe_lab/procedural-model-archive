# SPDX-FileCopyrightText: 2016 Nicolas Jonard
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Helix/Floating City - Kurokawa
# Nicolas Jonard (000 376 583)
# AIM - Jury Final du 19/01/2017
# Blender 2.78

import bpy
import math
import random
from bpy import context
from math import *
from random import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

##################################################################################################################################
# Data
##################################################################################################################################

# Angles des choix de directions possibles

angle_1 = 0
angle_2 = 60
angle_3 = 120
angle_4 = 180
angle_5 = 240
angle_6 = 300

l_angles_impair = [angle_1, angle_3, angle_5]
l_angles_pair = [angle_2, angle_4, angle_6]
l_angles_total = [angle_1, angle_2, angle_3, angle_4, angle_5, angle_6]

##################################################################################################################################
# fonction CopyPaste du module d'une double boucle d'ADN
##################################################################################################################################


def CopyPaste(module_1, DNA):

    bpy.context.scene.objects.active = bpy.data.objects[module_1]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(DNA, obj.data)
    context.scene.objects.link(Copy)

    return Copy


##################################################################################################################################
# Création des listes de coordonnées et de modules
##################################################################################################################################

l_tower = []
l_DNA = []
l_loc = []

##################################################################################################################################
# Coefficient de "VITESSE" de croissance
##################################################################################################################################

k = 10  # Plus k diminue, plus l'intensité de croissance augmente (c'est l'inverse pour une décroissance)

##################################################################################################################################
# Coefficient de "SENS" de croissance
##################################################################################################################################

d = 1  # Rentrer 'd=-1' pour une décroissance ou 'd=1' pour une croissance

##################################################################################################################################
# Création de la fonction tower sur une trame hexagonale
##################################################################################################################################


def tower(location):
    if d == -1:
        d2 = 8
    else:
        d2 = 1
    x, y, z = location
    dist = ((hypot(x, y) / d2 + 1) / k) ** d
    etage = int(dist)

    ##################################################################################################################################
    # Point de base sur la terre en (0,0,0)
    ##################################################################################################################################

    if len(l_tower) == 0:
        x, y, z = location
        size = 1.81
        location = (x, y, size / 2 + etage)
        l_tower.append((location, size, choice(l_angles_impair)))

        # ajout du module 'DNA':
        for hauteur in range(etage):
            hauteur = hauteur * 1.81
            tower = CopyPaste("module_1", "DNA")
            tower.location = (x, y, 0 + hauteur)
            tower.rotation_euler = [0, 0, radians(60)]
            l_DNA.append(location)

    else:

        ##################################################################################################################################
        # Tour avec choix d'angles IMPAIR
        ##################################################################################################################################

        if len(l_tower) % 2 == 0:

            if len(l_tower) != 0:
                last_tower = l_tower[-1]
                location, size, angle = last_tower
                X, Y, Z = location
                size = 1.81
                distance = 1.79 * 2
                x = 0
                y = 0
                t = 0

                while (
                    x,
                    y,
                ) in l_loc:  # empecher un nouveau point de revenir sur un point existant

                    angle2 = choice(l_angles_impair)

                    x = X + ((cos(radians(angle2))) * distance)
                    y = Y + ((sin(radians(angle2))) * distance)

                    t = t + 1
                    if t > 6:
                        break

                dist = ((hypot(x, y) / d2 + 1) / k) ** d
                etage = int(dist)

                location = (x, y, size / 2 + etage)
                l_tower.append((location, size, angle2))

            # ajout du module 'DNA':
            for hauteur in range(etage):
                hauteur = hauteur * 1.81
                tower = CopyPaste("module_1", "DNA")
                tower.location = (X, Y, 0 + hauteur)
                tower.rotation_euler = [0, 0, radians(0)]
                l_DNA.append(location)

        ##################################################################################################################################
        # Tour avec choix d'angles PAIR
        ##################################################################################################################################

        if len(l_tower) % 2 != 0:
            if dist < 3:

                last_tower = l_tower[-1]
                location, size, angle = last_tower
                X, Y, Z = location
                size = 1.81
                distance = 1.79 * 2
                angle2 = 180

                x = X + ((cos(radians(angle2))) * distance)
                y = Y + ((sin(radians(angle2))) * distance)

                dist = ((hypot(x, y) / d2 + 1) / k) ** d
                etage = int(dist)
                location = (x, y, size / 2 + etage)
                l_tower.append((location, size, angle2))

            else:

                last_tower = l_tower[-1]
                location, size, angle = last_tower
                X, Y, Z = location
                size = 1.81
                distance = 1.79 * 2
                angle2 = choice(l_angles_pair)

                x = X + ((cos(radians(angle2))) * distance)
                y = Y + ((sin(radians(angle2))) * distance)

                dist = ((hypot(x, y) / d2 + 1) / k) ** d
                etage = int(dist)
                location = (x, y, size / 2 + etage)
                l_tower.append((location, size, angle2))

            # ajout du module 'DNA':
            for hauteur in range(etage):
                hauteur = hauteur * 1.81
                tower = CopyPaste("module_1", "DNA")
                tower.location = (X, Y, 0 + hauteur)
                tower.rotation_euler = [0, 0, radians(60)]
                l_DNA.append(location)

    l_loc.append((x, y))


##################################################################################################################################
# exécution de la fonction tower
##################################################################################################################################

for i in range(0, 20):
    tower((0, 0, 0))

##################################################################################################################################
# print
##################################################################################################################################

print("----------")
print("Cette configuration se compose de", len(l_DNA), "modules")
print("La ville peut donc habiter", len(l_DNA) * 74520, "personnes")
print('le coefficient de vitesse de croissance "k"=', k)
if d == 1:
    print("La ville se developpe en croissance")
if d == -1:
    print("La ville se developpe en decroissance")
print("----------")
