# SPDX-FileCopyrightText: 2018 Alba Llevadot Massanes & Gonzalo Auger Portillo
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Blender 2.79b f4dc9f9d68b
# Alba Llevadot & Gonzalo Auger _ 17 I 2019 _ AlICe

import bpy
from random import randint
from math import radians
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# COLORS
# We choose the colors that we are going to use and put name to them

mat_red = bpy.data.materials.new("red")
mat_red.diffuse_color = (0.8, 0, 0)
mat_black = bpy.data.materials.new("black")
mat_black.diffuse_color = (0, 0, 0)
mat_grey = bpy.data.materials.new("grey")
mat_grey.diffuse_color = (0.3, 0.3, 0.3)
mat_yellow = bpy.data.materials.new("yellow")
mat_yellow.diffuse_color = (1, 1, 0)

# FUNCTION
# We elaborate a function with three variables: the position, the size and the name of the object (cube)


def MaBoite(_position, _taille, _nom):
    bpy.ops.mesh.primitive_cube_add(radius=1, location=_position)
    bpy.ops.transform.resize(value=_taille)
    bpy.context.object.name = _nom


# THE 3 FACES
# Creation of the 3 faces of the model (Face A, Face B and Face C)

for j in range(0, 1):
    MaBoite((0, 0.05, -0.95), (11.95, 11.95, 0.05), "FACE XY")

for j in range(0, 1):
    MaBoite((-12, 0, 11), (0.05, 12, 12), "FACE YZ")

for j in range(0, 1):
    MaBoite((0, -11.95, 11), (11.95, 0.05, 12), "FACE XZ")


# FACE A (XZ)
# We start doing the extrusions of the Face A
## THICK EXTRUSIONS
### Level 1 - BLACK
# We create two thick and fixed extrusions in level 1

for j in range(0, 1):
    MaBoite((4, 0.05, 0), (4, 11.95, 1), "BLACK level 1 XZ 1")

    # We add the black color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_black)

for j in range(0, 1):
    MaBoite((-10, 0.05, 0), (2, 11.95, 1), "BLACK level XZ 2")

    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_black)

    ## THIN EXTRUSIONS

    ### Level 2 and 3 - GREY
    # We create three thin and non fixed extrusions

for j in range(-9, -6):
    MaBoite((j * 4 / 3, 0, 0), (1 / 3, 12, 1), "barres minces " + str(j * 5))
    bpy.ops.transform.translate(value=(1 / 3, 0, 0))

    # We add the grey color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_grey)

    # As we said they are not fixed, they may vary in z direction between levels 2 and 3
bpy.data.objects["barres minces -45"].location.z += randint(2, 4)
bpy.data.objects["barres minces -40"].location.z += randint(2, 4)
bpy.data.objects["barres minces -35"].location.z += randint(2, 4)

### Level 2 and 3 - RED
# We create three thin and non fixed extrusions

for j in range(6, 9):
    MaBoite((j * 4 / 3, 0, 0), (1 / 3, 12, 1), "barres minces " + str(j * 5))
    bpy.ops.transform.translate(value=(1 / 3, 0, 0))

    # We add the red color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_red)

    # They may vary in z direction between levels 2 and 3
bpy.data.objects["barres minces 30"].location.z += randint(2, 4)
bpy.data.objects["barres minces 35"].location.z += randint(2, 4)
bpy.data.objects["barres minces 40"].location.z += randint(2, 4)

### Levels 9 to 12 - BLACK
# We create three thin and non fixed extrusions

for j in range(-6, -3):
    MaBoite((j * 4 / 3, 0, 0), (1 / 3, 12, 1), "barres minces " + str(j * 5))
    bpy.ops.transform.translate(value=(1 / 3, 0, 0))

    # We add the black color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_black)

    # They may vary in z direction between levels 9 and 12
bpy.data.objects["barres minces -30"].location.z += randint(16, 22)
bpy.data.objects["barres minces -25"].location.z += randint(16, 22)
bpy.data.objects["barres minces -20"].location.z += randint(16, 22)

### Levels 9 to 12 - RED
# We create three thin and non fixed extrusions

for j in range(-3, 0):
    MaBoite((j * 4 / 3, 0, 0), (1 / 3, 12, 1), "barres minces " + str(j * 5))
    bpy.ops.transform.translate(value=(1 / 3, 0, 0))

    # We add the red color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_red)

    # They may vary in z direction between levels 9 and 12
bpy.data.objects["barres minces -15"].location.z += randint(16, 22)
bpy.data.objects["barres minces -10"].location.z += randint(16, 22)
bpy.data.objects["barres minces -5"].location.z += randint(16, 22)

### Levels 9 to 12 - GREY
# We create six thin and non fixed extrusions

for j in range(0, 6):
    MaBoite((j * 4 / 3, 0, 0), (1 / 3, 12, 1), "barres minces " + str(j * 5))
    bpy.ops.transform.translate(value=(1 / 3, 0, 0))

    # We add the grey color to the extrusions
    mesh = bpy.context.object.data
    mesh.materials.clear()
    mesh.materials.append(mat_grey)

    # They may vary in z direction between levels 9 and 12
bpy.data.objects["barres minces 0"].location.z += randint(16, 22)
bpy.data.objects["barres minces 5"].location.z += randint(16, 22)
bpy.data.objects["barres minces 10"].location.z += randint(16, 22)
bpy.data.objects["barres minces 15"].location.z += randint(16, 22)
bpy.data.objects["barres minces 20"].location.z += randint(16, 22)
bpy.data.objects["barres minces 25"].location.z += randint(16, 22)

# FACE B (YZ) - We continue with the extrusions of the Face B

## EXTRUSIONS 1x1

##FONCTION - We add a new function to make triangles from the extrusions in this face
#          - From every 1x1 extrustion we can make 4 squares divided in 2 triangles each one of them to make the diferent random possibilities
#          - We start doing the two triangles with different position

# TRIANGLE 1


def Triangle1():
    # we create some vertex to join them and define faces
    mesVertices = [(0, 0, 0), (0, 1, 0), (0, 1, 1), (24, 0, 0), (24, 1, 0), (24, 1, 1)]
    mesFaces = [(0, 1, 2), (1, 2, 5, 4), (0, 1, 4, 3), (0, 2, 5, 3), (3, 4, 5)]
    # we create the mesh and the object
    monMesh = bpy.data.meshes.new("triangleMesh")
    monObjet = bpy.data.objects.new("1", monMesh)
    # we link the object to the scene
    maScene = bpy.context.scene
    maScene.objects.link(monObjet)
    # "fill" the mesh with the information of the vertices and faces
    monMesh.from_pydata(mesVertices, [], mesFaces)

    return monObjet

    # TRIANGLE 2


def Triangle2():
    mesVertices2 = [(0, 0, 0), (0, 1, 1), (0, 0, 1), (24, 0, 0), (24, 1, 1), (24, 0, 1)]
    mesFaces2 = [(0, 1, 2), (1, 2, 5, 4), (0, 1, 4, 3), (0, 2, 5, 3), (3, 4, 5)]
    monMesh2 = bpy.data.meshes.new("triangleMesh2")
    monObjet2 = bpy.data.objects.new("2", monMesh2)
    maScene2 = bpy.context.scene
    maScene2.objects.link(monObjet2)
    monMesh2.from_pydata(mesVertices2, [], mesFaces2)

    return monObjet2

    # We create a function where we are going to have both triangles with two variables (the position and the name):


def TriangleCube(_position, _nom):
    bpy.ops.object.select_all(action="DESELECT")

    # We tell the program to do copies of the triangles together 4 times (when the angle is 0, 90, 180 and 270º)
    # We say too that just can appear one of the two triangles every time, and the program can choose it in a random way.
    for angle in range(0, 271, 90):
        object = random.choice([Triangle1, Triangle2])()
        object.select = True
        object.name = _nom
        object.rotation_euler[0] = radians(angle)

    # We add this parameter to change the position
    bpy.ops.transform.translate(value=_position)

    ### Level 4 to 8 - YELLOW
    #  We create the first yellow "TriangleCube"


for i in range(0, 1):
    TriangleCube((-12, -11, 0), "triangles")

    #  We choose the position which can vary between levels 4 and 8
    Yellow2H = randint(6, 14)
    bpy.data.objects["triangles"].location.z += Yellow2H

    #  We add the yellow color to every triangle
    bpy.data.objects["triangles"].active_material = mat_yellow
    bpy.data.objects["triangles.001"].location.z += Yellow2H
    bpy.data.objects["triangles.001"].active_material = mat_yellow
    bpy.data.objects["triangles.002"].location.z += Yellow2H
    bpy.data.objects["triangles.002"].active_material = mat_yellow
    bpy.data.objects["triangles.003"].location.z += Yellow2H
    bpy.data.objects["triangles.003"].active_material = mat_yellow

    ### Level 4 to 8 - BLACK
    #  We create the black "TriangleCube"

for i in range(1, 2):
    TriangleCube((-12, -7, 0), "blacktriangles")

    #  We choose the position which can vary between levels 4 and 8
    BlackH = randint(6, 14)
    bpy.data.objects["blacktriangles"].location.z += BlackH

    #  We add the black color to every triangle
    bpy.data.objects["blacktriangles"].active_material = mat_black
    bpy.data.objects["blacktriangles.001"].location.z += BlackH
    bpy.data.objects["blacktriangles.001"].active_material = mat_black
    bpy.data.objects["blacktriangles.002"].location.z += BlackH
    bpy.data.objects["blacktriangles.002"].active_material = mat_black
    bpy.data.objects["blacktriangles.003"].location.z += BlackH
    bpy.data.objects["blacktriangles.003"].active_material = mat_black

    ### Level 4 to 8 - GREY
    #  We create the grey "TriangleCube"
for i in range(2, 3):
    TriangleCube((-12, -3, 0), "greytriangles")

    #  We choose the position which can vary between levels 4 and 8
    GreyH = randint(6, 14)
    bpy.data.objects["greytriangles"].location.z += GreyH

    #  We add the grey color to every triangle
    bpy.data.objects["greytriangles"].active_material = mat_grey
    bpy.data.objects["greytriangles.001"].location.z += GreyH
    bpy.data.objects["greytriangles.001"].active_material = mat_grey
    bpy.data.objects["greytriangles.002"].location.z += GreyH
    bpy.data.objects["greytriangles.002"].active_material = mat_grey
    bpy.data.objects["greytriangles.003"].location.z += GreyH
    bpy.data.objects["greytriangles.003"].active_material = mat_grey

    ### Level 4 to 8 - RED
    #  We create the two red "TriangleCubes"

for i in range(3, 4):
    TriangleCube((-12, 1, 0), "redtriangles")

    #  We choose the position which can vary between levels 4 and 8
    Red1H = randint(6, 14)
    Red2H = randint(6, 14)
    bpy.data.objects["redtriangles"].location.z += Red1H

    #  We add the red color to every triangle
    bpy.data.objects["redtriangles"].active_material = mat_red
    bpy.data.objects["redtriangles.001"].location.z += Red1H
    bpy.data.objects["redtriangles.001"].active_material = mat_red
    bpy.data.objects["redtriangles.002"].location.z += Red1H
    bpy.data.objects["redtriangles.002"].active_material = mat_red
    bpy.data.objects["redtriangles.003"].location.z += Red1H
    bpy.data.objects["redtriangles.003"].active_material = mat_red

for i in range(4, 5):
    TriangleCube((-12, 5, 0), "redtriangles2")

    Red2H = randint(6, 14)
    bpy.data.objects["redtriangles2"].location.z += Red2H
    bpy.data.objects["redtriangles2"].active_material = mat_red
    bpy.data.objects["redtriangles2.001"].location.z += Red2H
    bpy.data.objects["redtriangles2.001"].active_material = mat_red
    bpy.data.objects["redtriangles2.002"].location.z += Red2H
    bpy.data.objects["redtriangles2.002"].active_material = mat_red
    bpy.data.objects["redtriangles2.003"].location.z += Red2H
    bpy.data.objects["redtriangles2.003"].active_material = mat_red

    ### Level 4 to 8 - YELLOW
    #  We create the second yellow "TriangleCube"
for i in range(5, 6):
    TriangleCube((-12, 9, 0), "yellowtriangles")

    #  We choose the position which can vary between levels 4 and 8
    YellowH = randint(6, 14)
    bpy.data.objects["yellowtriangles"].location.z += YellowH

    #  We add the yellow color to every triangle
    bpy.data.objects["yellowtriangles"].active_material = mat_yellow
    bpy.data.objects["yellowtriangles.001"].location.z += YellowH
    bpy.data.objects["yellowtriangles.001"].active_material = mat_yellow
    bpy.data.objects["yellowtriangles.002"].location.z += YellowH
    bpy.data.objects["yellowtriangles.002"].active_material = mat_yellow
    bpy.data.objects["yellowtriangles.003"].location.z += YellowH
    bpy.data.objects["yellowtriangles.003"].active_material = mat_yellow
