# SPDX-FileCopyrightText: 2014 Yani Bex
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

### Réalisé avec Blender v2.71, hash 9337574, sous Win8*64
#'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

### Faculte d'architecture Lacambre-Horta
### Labo AlICe, Option Aim 2014-2015
### Yani Bex étudiant en Ma2

# .........................................................

##=====================##
# - Modèles Procéduraux -#
##=====================##

###____Hopital de Venise, Le Corbusier + Guillermo Jullian____###


import bpy
import math
import random
from random import choice
from collections import Counter

# ___________________________________________________________________________________________________#


## Clear scene:
def clearAll():
    for obj in bpy.context.scene.objects:
        obj.select = True
    bpy.ops.object.delete()


clearAll()

print()
print(" Run Script + + +")
print()

# ___________________________________________________________________________________________________#


########################
####    MODULOR     ####
########################

print()
print(
    " * Initialisation du Modulor *"
)  # Génération des mesures du batiment selon la taille humaine
print()

# ---------------------------------------------------------------------------------------------------#
# Dimensions humaines
Toise = 1.828
Nombril = 1.13
# ---------------------------------------------------------------------------------------------------#


# .....................#
##__Série Rouge__##
#'''''''''''''''''''''#

# Multiples
Sr = []
a, b, c = Nombril, Toise, 1
while c < 10:
    Sr.append(a)
    a, b, c = b, a + b, c + 1

# Sous-multiples
Srinf = []
a, b, c = Nombril, Toise, 1
while c < 10:
    Srinf.append(b)
    b, a, c = a, b - a, c + 1

print(
    "-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^"
)
print(" Serie rouge:")
print()
print("Srinf[5]=", round(Srinf[5], 3))
print("Srinf[4]=", round(Srinf[4], 3))
print("Srinf[3]=", round(Srinf[3], 3))
print("Srinf[2]=", round(Srinf[2], 3))
print("  Sr[0]=", round(Sr[0], 3))
print("  Sr[1]=", round(Sr[1], 3))
print("  Sr[2]=", round(Sr[2], 3))
print("  Sr[3]=", round(Sr[3], 3))
print("  Sr[4]=", round(Sr[4], 3))
print("  Sr[5]=", round(Sr[5], 3))
print("  Sr[6]=", round(Sr[6], 3))
print("  Sr[7]=", round(Sr[7], 3))
print("  Sr[8]=", round(Sr[8], 3))
print()
print(
    "--------------------------------------------------------------------------------"
)
# ---------------------------------------------------------------------------------------------------#

# .....................#
##__Série Bleue__##
#'''''''''''''''''''''#

# Multiples
Sb = []
a, b, c = Sr[0] * 2, Sr[1] * 2, 1  # La série bleue est le double de la série rouge
while c < 10:
    Sb.append(a)
    a, b, c = b, a + b, c + 1

# Sous-multiples
Sbinf = []
a, b, c = Sr[0] * 2, Sr[1] * 2, 1
while c < 10:
    Sbinf.append(b)
    b, a, c = a, b - a, c + 1

print(" Serie bleue:")
print()
print("Sbinf[6]=", round(Sbinf[6], 3))
print("Sbinf[5]=", round(Sbinf[5], 3))
print("Sbinf[4]=", round(Sbinf[4], 3))
print("Sbinf[3]=", round(Sbinf[3], 3))
print("Sbinf[2]=", round(Sbinf[2], 3))
print("   Sb[0]=", round(Sb[0], 3))
print("   Sb[1]=", round(Sb[1], 3))
print("   Sb[2]=", round(Sb[2], 3))
print("   Sb[3]=", round(Sb[3], 3))
print("   Sb[4]=", round(Sb[4], 3))
print("   Sb[5]=", round(Sb[5], 3))
print("   Sb[6]=", round(Sb[6], 3))
print("   Sb[7]=", round(Sb[7], 3))
print()
print(
    "-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^"
)
# ---------------------------------------------------------------------------------------------------#


# ...................#
### Valeurs ###
#'''''''''''''''''''#

print(" * Calcul des donnees *")

Venise = bpy.context.scene

# Paramètres

TailleIlot = random.randrange(3, 6)
print()
print(" -La taille de l'ilot est de", TailleIlot, "unitees")
print()

CapaciteHop = random.randrange(30, 150)
print()
print(" -La capacite de l'hopital est de", CapaciteHop, "lits")
print()

LongueurDortoir = 7
print()
print(" -La longueur du dortoir est de", LongueurDortoir, "cellules")
print()


# Dimensions

Altitude = Sb[3]  # Hauteur des pilotis
Dalle = Sbinf[4]  # Coudée
Mur = Sbinf[6]  # Empan
HauPlafond = Sb[0] + Dalle
HauToit = Sb[1] + Dalle
DimCellule = Sr[2]  # Perche romaine
DimDortoir = (LongueurDortoir * DimCellule) + (LongueurDortoir * Mur)
Matrice = DimDortoir * 2 + Sr[5]  # Baton de Corde

# ---------------------------------------------------------------------------------------------------#


# ___________________________________________________________________________________________________#
################
###  OBJETS  ###
################

print()
print(" * Creation des objets *")

print()
print(" -Cube")


## Cube de base
# _Objet_mesh : Cube
def Cube(pos_x, pos_y, pos_z, dim_X, dim_Y, dim_Z, nom):
    # Vertices:
    Vertices = [
        (-dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, dim_Y / 2, dim_Z),
        (-dim_X / 2, dim_Y / 2, dim_Z),
    ]
    # Faces:
    Faces = [
        (3, 2, 1, 0),
        (4, 5, 6, 7),
        (1, 5, 4, 0),
        (3, 7, 6, 2),
        (0, 4, 7, 3),
        (2, 6, 5, 1),
    ]
    # Create a mesh
    Mesh = bpy.data.meshes.new(nom + "_mesh")
    # Create object linking mesh
    Objet = bpy.data.objects.new(nom, Mesh)
    # Compute mesh:
    Mesh.from_pydata(Vertices, [], Faces)
    # Translation:
    Objet.location = (pos_x, pos_y, pos_z)
    # Link to my scene:
    Venise.objects.link(Objet)
    Mesh.update()
    # Return object:
    return Objet


# ---------------------------------------------------------------------------------------------------#


# ...................#
### Cellule ###
#'''''''''''''''''''#
print()
print(" -Cellule")


def AddCell(pos_x, pos_y, pos_z, nom, piloti=True, miroir=True):
    AddCell.location = (pos_x, pos_y, pos_z)

    # Cellule = bpy.ops.object.add( type='EMPTY', location=(pos_x,pos_y,pos_z), rotation=(0.0, 0.0, 0.0))
    OrigCellule = bpy.data.objects.new(nom, None)
    OrigCellule.location = (pos_x, pos_y, pos_z)
    OrigCellule.select = True
    Venise.objects.link(OrigCellule)

    # Sol
    # obj1 = Cube(pos_x+0,pos_y,Altitude, DimCellule,DimCellule,Dalle,"Sol")
    # obj1.select = True

    # Mur1
    obj2 = Cube(
        pos_x + 0,
        -DimCellule / 2 + pos_y,
        Altitude + Dalle,
        DimCellule + Mur,
        Mur,
        Sb[0] + Mur,
        "Mur1",
    )
    obj2.select = True

    # Mur2
    obj3 = Cube(
        pos_x + Srinf[2],
        pos_y + (DimCellule / 2),
        Altitude + Dalle,
        Sr[1],
        Mur,
        Sb[0],
        "Mur2",
    )
    obj3.select = True

    # Mur3
    obj4 = Cube(
        pos_x + (Sr[2] / 2 + Mur / 2),
        pos_y + (DimCellule / 2 - Sr[1] / 2),
        Altitude + Dalle,
        Mur,
        Sr[1] + Mur / 2,
        Sb[0],
        "Mur3",
    )
    obj4.select = True

    # Lit
    obj5 = Cube(
        pos_x + Srinf[2],
        pos_y + (Sr[2] / 2 - Sr[1] / 2),
        Altitude + Dalle + Sbinf[3] - Sbinf[5],
        Sbinf[3],
        Sr[1],
        Sbinf[5],
        "Lit",
    )
    obj5.select = True

    if piloti:
        # Piloti
        obj6 = Cube(
            pos_x + (-Sr[2] / 2 + Sr[0] / 2),
            pos_y + (-DimCellule / 2),
            0,
            Sr[0],
            Srinf[4],
            Altitude,
            "Piloti",
        )
        obj6.select = True

    # Plafond
    obj7 = Cube(
        pos_x + 0,
        pos_y + (DimCellule / 2 - Sbinf[2] / 2 + Mur / 4),
        Altitude + Dalle + Sb[0],
        DimCellule + Mur,
        Sbinf[2] + Mur / 2,
        Mur,
        "Plafond",
    )
    obj7.select = True

    # Toit
    obj8 = Cube(
        pos_x + 0,
        pos_y + (DimCellule / 2 - Sr[0] / 2 + Mur / 2),
        Altitude + Dalle + Sb[1],
        DimCellule + Mur,
        Sr[0],
        Mur,
        "Toit",
    )
    obj8.select = True

    # Pente
    bpy.ops.object.select_all(action="DESELECT")

    if miroir:
        obj9 = Cube(
            pos_x + 0,
            pos_y + (-Sbinf[2] / 2 + Srinf[4]),
            Altitude + Sb[1] - Srinf[6],
            DimCellule + Mur,
            Sb[0] + Sbinf[6],
            Mur,
            "Pente",
        )
        obj9.select = True
        bpy.ops.transform.rotate(
            value=-10, axis=(1, 0, 0), constraint_axis=(True, False, False)
        )
    else:
        obj9 = Cube(
            pos_x + 0,
            pos_y + (-Sbinf[2] / 2 + Srinf[5]),
            Altitude + Sb[1] + Srinf[6] / 2,
            DimCellule + Mur,
            Sb[0] + Sbinf[6],
            Mur,
            "Pente",
        )
        obj9.select = True
        bpy.ops.transform.rotate(
            value=10, axis=(1, 0, 0), constraint_axis=(True, False, False)
        )

    bpy.ops.object.select_all(action="DESELECT")

    # Définir les éléments de la cellule
    CellElements = "Mur1", "Mur2", "Mur3", "Pente", "Toit", "Sol", "Lit", "Plafond"

    # Apparenter les éléments avec l'origine de la Cellule
    # obj1.select = True
    obj2.select = True
    obj3.select = True
    obj4.select = True
    obj5.select = True
    if piloti:
        obj6.select = True
    obj7.select = True
    obj8.select = True
    obj9.select = True
    if miroir:
        bpy.ops.transform.mirror(constraint_axis=(False, True, False))

    OrigCellule.select = True

    bpy.context.scene.objects.active = OrigCellule
    bpy.ops.object.parent_set()


print(".")
# ---------------------------------------------------------------------------------------------------#


# ...................#
### Dortoir ###
#'''''''''''''''''''#

print(" -Dortoir")
print()

Aj = Sr[5] / 2  # réajustement

# _Dalle
Cube(
    DimDortoir / 2 + Aj,
    DimDortoir / 2 + Aj,
    Altitude,
    DimDortoir,
    DimDortoir,
    Dalle,
    "DalleDortoir",
)

# _Murs de contour

Cube(
    DimDortoir / 2 + Aj,
    DimDortoir + Aj - Mur / 2,
    Altitude + Dalle,
    DimDortoir,
    Mur,
    Sb[0] + Mur,
    "MurDortoir1",
)  # Axe des x

Cube(
    DimDortoir + Aj - Mur / 2,
    DimDortoir / 2 + Aj,
    Altitude + Dalle,
    Mur,
    DimDortoir,
    Sb[0] + Mur,
    "MurDortoir2",
)  # Axe des y

### Remplir le dortoir
#'''''''''''''''''''''#
# Cellules Normales
for i in range(LongueurDortoir):
    for j in range(0, 3):

        if i % 2 == 0:
            AddCell(
                (Sr[2] + Mur) * i + DimCellule / 2 + Aj,
                (2 * Sr[2] + Sb[0]) * j + DimCellule / 2 + Mur / 2 + Aj,
                0,
                "Cell_",
                piloti=True,
                miroir=False,
            )  # Avec Piloti

        if i % 2 == 1:
            AddCell(
                (Sr[2] + Mur) * i + DimCellule / 2 + Aj,
                (2 * Sr[2] + Sb[0]) * j + DimCellule / 2 + Mur / 2 + Aj,
                0,
                "Cell_",
                piloti=False,
                miroir=False,
            )  # Sans Piloti

# Cellules Miroirs
for i in range(LongueurDortoir):
    for j in range(1, 2):
        AddCell(
            (Sr[2] + Mur) * i + DimCellule / 2 + Aj,
            (Sr[2] + Sb[0]) * j + DimCellule / 2 + Mur / 2 + Aj,
            0,
            "Cell_",
            piloti=False,
            miroir=True,
        )

for i in range(LongueurDortoir):
    for j in range(1, 2):
        AddCell(
            (Sr[2] + Mur) * i + DimCellule / 2 + Aj,
            (3 * Sr[2] + 2 * Sb[0]) * j + DimCellule / 2 + Mur / 2 + Aj,
            0,
            "Cell_",
            piloti=False,
            miroir=True,
        )


### Grouper les cellules
#'''''''''''''''''''''''''#


def GrouperDortoir(miroir=True):

    bpy.ops.group.create(name="Dortoir")

    DortoirElements = [obj.name for obj in bpy.data.objects]
    bpy.ops.object.select_all(action="DESELECT")

    for name in DortoirElements:
        Venise.objects.active = Venise.objects[name]
        bpy.ops.object.group_link(group="Dortoir")
        bpy.context.scene.objects[name].select = False

    return GrouperDortoir


GrouperDortoir()


AddDortoir = bpy.ops.object.group_instance_add

# Dortoir type B


def MiroirDortoir():
    bpy.ops.group.create(name="DortoirB")

    bpy.ops.object.select_all(action="DESELECT")
    AddDortoir(name="Dortoir", location=(Matrice, 0, 0), rotation=(0, 0, 0))
    # Venise.objects.active = Venise.objects[name]
    bpy.ops.transform.mirror(constraint_axis=(True, False, False))
    for obj in bpy.context.selected_objects:
        bpy.ops.object.group_link(group="DortoirB")
        bpy.ops.object.select_all(action="DESELECT")


MiroirDortoir()


print(".")
# ---------------------------------------------------------------------------------------------------#


# ---------------------------------------------------------------------------------------------------#
print()

# ___________________________________________________________________________________________________#
######################
###  Construction  ###
######################

print()
print(" + Debut de la construction +")
print()

# ....................#
### Colonnes ###
#''''''''''''''''''''#

print(" -Positionnement des distributions verticales")

for i in range(0, TailleIlot):
    for j in range(0, TailleIlot):
        Cube(i * Matrice, j * Matrice, 0, Sr[3], Sr[3], Altitude + 5, "Colonne")

print(".")
# ---------------------------------------------------------------------------------------------------#

# ...................#
### Halls ###
#'''''''''''''''''''#

print(" -Positionnement des halls centraux")

for i in range(0, TailleIlot):
    for j in range(0, TailleIlot):
        Cube(i * Matrice, j * Matrice, Altitude, Sr[5], Sr[5], HauToit, "Hall")

print(".")
# ---------------------------------------------------------------------------------------------------#

# ..................#
###_Rues_###
#''''''''''''''''''#
print(" -Positionnement des rues")

Ajus = Sr[5] / 2 + DimDortoir / 2

for i in range(0, TailleIlot):
    for j in range(0, TailleIlot):

        # Type A
        if (i % 2 == 0 and j % 2 == 0) or (i % 2 == 1 and j % 2 == 1):
            Cube(
                i * Matrice + Ajus,
                j * Matrice + Sr[5] / 2 - Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[3],
                HauToit,
                "Rue_A+X",
            )
            Cube(
                i * Matrice - Ajus,
                j * Matrice - Sr[5] / 2 + Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[3],
                HauToit,
                "Rue_A-X",
            )

            Cube(
                i * Matrice - Sr[5] / 2 + Sr[3] / 2,
                j * Matrice + Ajus,
                Altitude,
                Sr[3],
                DimDortoir,
                HauToit,
                "Rue_A+Y",
            )
            Cube(
                i * Matrice + Sr[5] / 2 - Sr[3] / 2,
                j * Matrice - Ajus,
                Altitude,
                Sr[3],
                DimDortoir,
                HauToit,
                "Rue_A-Y",
            )

        # Type B
        if (i % 2 == 0 and j % 2 == 1) or (i % 2 == 1 and j % 2 == 0):
            Cube(
                i * Matrice + Ajus,
                j * Matrice - Sr[5] / 2 + Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[3],
                HauToit,
                "Rue_B+X",
            )
            Cube(
                i * Matrice - Ajus,
                j * Matrice + Sr[5] / 2 - Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[3],
                HauToit,
                "Rue_B-X",
            )

            Cube(
                i * Matrice + Sr[5] / 2 - Sr[3] / 2,
                j * Matrice + Ajus,
                Altitude,
                Sr[3],
                DimDortoir,
                HauToit,
                "Rue_B+Y",
            )
            Cube(
                i * Matrice - Sr[5] / 2 + Sr[3] / 2,
                j * Matrice - Ajus,
                Altitude,
                Sr[3],
                DimDortoir,
                HauToit,
                "Rue_B-Y",
            )

print(".")
# ---------------------------------------------------------------------------------------------------#

# ....................#
###_Services_###
#''''''''''''''''''''#
print(" -Positionnement des services")

for i in range(0, TailleIlot):
    for j in range(0, TailleIlot):

        # Type A
        if (i % 2 == 0 and j % 2 == 0) or (i % 2 == 1 and j % 2 == 1):
            Cube(
                i * Matrice - Ajus,
                j * Matrice + Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[4],
                HauPlafond,
                "Service_A+X",
            )
            Cube(
                i * Matrice + Ajus,
                j * Matrice - Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[4],
                HauPlafond,
                "Service A-X",
            )

            Cube(
                i * Matrice + Sr[3] / 2,
                j * Matrice + Ajus,
                Altitude,
                Sr[4],
                DimDortoir,
                HauPlafond,
                "Service_A+Y",
            )
            Cube(
                i * Matrice - Sr[3] / 2,
                j * Matrice - Ajus,
                Altitude,
                Sr[4],
                DimDortoir,
                HauPlafond,
                "Service_A-Y",
            )

        # Type B
        if (i % 2 == 0 and j % 2 == 1) or (i % 2 == 1 and j % 2 == 0):
            Cube(
                i * Matrice - Ajus,
                j * Matrice - Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[4],
                HauPlafond,
                "Service_B+X",
            )
            Cube(
                i * Matrice + Ajus,
                j * Matrice + Sr[3] / 2,
                Altitude,
                DimDortoir,
                Sr[4],
                HauPlafond,
                "Service_B-X",
            )

            Cube(
                i * Matrice - Sr[3] / 2,
                j * Matrice + Ajus,
                Altitude,
                Sr[4],
                DimDortoir,
                HauPlafond,
                "Service_B+Y",
            )
            Cube(
                i * Matrice + Sr[3] / 2,
                j * Matrice - Ajus,
                Altitude,
                Sr[4],
                DimDortoir,
                HauPlafond,
                "Service_B-Y",
            )


print(".")
# ---------------------------------------------------------------------------------------------------#

# ...................#
###_Dortoir_###
#'''''''''''''''''''#
print(" -Positionnement des Dortoirs")

# Positionner les dortoirs par des rotations
dortoirs = []

for i in range(0, TailleIlot):
    for j in range(0, TailleIlot):

        # Type A
        if (i % 2 == 0 and j % 2 == 0) or (i % 2 == 1 and j % 2 == 1):
            AddDortoir(
                name="Dortoir",
                location=(i * Matrice, j * Matrice, 0),
                rotation=(0, 0, 0),
            )
            # print("selected object ",bpy.context.selected_objects)
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="Dortoir",
                location=(i * Matrice, j * Matrice, 0),
                rotation=(0, 0, math.pi / 2),
            )
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="Dortoir",
                location=(i * Matrice, j * Matrice, 0),
                rotation=(0, 0, math.pi),
            )
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="Dortoir",
                location=(i * Matrice, j * Matrice, 0),
                rotation=(0, 0, 3 * math.pi / 2),
            )
            dortoirs.append(bpy.context.selected_objects)

        # Type B
        if (i % 2 == 0 and j % 2 == 1) or (i % 2 == 1 and j % 2 == 0):
            AddDortoir(
                name="DortoirB",
                location=(i * Matrice - Matrice, j * Matrice, 0),
                rotation=(0, 0, 0),
            )
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="DortoirB",
                location=(i * Matrice, j * Matrice - Matrice, 0),
                rotation=(0, 0, math.pi / 2),
            )
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="DortoirB",
                location=(i * Matrice + Matrice, j * Matrice, 0),
                rotation=(0, 0, math.pi),
            )
            dortoirs.append(bpy.context.selected_objects)
            AddDortoir(
                name="DortoirB",
                location=(i * Matrice, j * Matrice + Matrice, 0),
                rotation=(0, 0, 3 * math.pi / 2),
            )
            dortoirs.append(bpy.context.selected_objects)

print(".")
# ---------------------------------------------------------------------------------------------------#


# ___________________________________________________________________________________________________#
########################
###  Actions Suppl.  ###
########################

print()
print(" + Actions supplémentaires +")
print()


bpy.ops.object.select_all(action="DESELECT")

# ...................#
###_Densité_###
#'''''''''''''''''''#

print()
print(" +  +")
Nombredelits = (TailleIlot * TailleIlot) * 4 * (LongueurDortoir * 5)


# while Nombredelits > CapaciteHop:

for i in range(0, 30):
    dortoirs[random.randint(0, len(dortoirs) - 1)][-1].select = True
    bpy.ops.object.delete()


print()
print(" Hopital realise")
print()
