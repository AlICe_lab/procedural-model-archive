# SPDX-FileCopyrightText: 2021 Johan Kouaho & Jacques Martens
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# KOUAHO_MARTENS_REICH_blenderV.2.93.5_Mac

# IMPORT & PURGE
import bpy

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

from random import randint


# DEFINITION OF LISTS


# define the list of voices
def get_objects(VoixI):
    res = []
    for obj in bpy.data.objects:
        if VoixI in obj.name:
            res.append(obj)
    return res

    # define the action of the boolean


def bool(obj1, obj2):
    bool = obj2.modifiers.new(name="booly", type="BOOLEAN")
    bool.object = obj1
    bool.operation = "INTERSECT"


def multi_boolean(VoixI, object):
    for t in VoixsI:
        bool(object, t)

    # define the action of applying the boolean
    # https://blenderartists.org/t/how-to-apply-all-the-modifiers-with-python/1314483/2


def apply_modifiers(obj):
    ctx = bpy.context.copy()
    ctx["object"] = obj
    for _, m in enumerate(obj.modifiers):
        try:
            ctx["modifier"] = m
            bpy.ops.object.modifier_apply(ctx, modifier=m.name)
        except RuntimeError:
            print(f"Error applying {m.name} to {obj.name}, removing it instead.")
            obj.modifiers.remove(m)
    for m in obj.modifiers:
        obj.modifiers.remove(m)


# CREATION OF OBJECTS

# Set a point in the partition to rotate & size
position = randint(-50, -10)
print(position)
rotation = randint(1, 2)
print(rotation)
taille = randint(5, 6)
print(taille)

# VoiceI
for i in range(0, 200):
    bpy.ops.mesh.primitive_cube_add(
        size=1,
        location=(3.75, round(i * 0.25 + position, 2), 5),
        scale=(taille, 0.1, 10),
    )
    bpy.context.object.name = "VoixI"

    # VoiceII
for i in range(0, 200):
    bpy.ops.mesh.primitive_cube_add(
        size=1,
        location=(1.25, round(i * 0.25 + position, 2), 5),
        scale=(taille, 0.1, 10),
    )
    bpy.context.object.rotation_euler[2] = round(-0.15708 * (rotation / 8) * i, 2)
    bpy.context.object.name = "VoixII"

    # VoiceIII
for i in range(0, 200):
    bpy.ops.mesh.primitive_cube_add(
        size=1,
        location=(-1.25, round(i * 0.25 + position, 2), 7),
        scale=(taille, 0.1, 6),
    )
    bpy.context.object.rotation_euler[2] = round(-0.15708 * (rotation / 4) * i, 2)
    bpy.context.object.name = "VoixIII"

    # VoiceIV
for i in range(0, 200):
    bpy.ops.mesh.primitive_cube_add(
        size=1,
        location=(-3.75, round(i * 0.25 + position, 2), 9),
        scale=(taille, 0.1, 3),
    )
    bpy.context.object.rotation_euler[2] = round(-0.15708 * (rotation / 2) * i, 2)
    bpy.context.object.name = "VoixIII"

    # Cube 10.10.10 for boolean
bpy.ops.mesh.primitive_cube_add(location=(0, 0, 5), scale=(5, 5, 5))

# Affection de noms
cube = bpy.context.scene.objects.get("Cube")
VoixsI = get_objects("VoixI")
multi_boolean(VoixsI, cube)

# APPLICATION & REMOVAL

# Application
for obj in VoixsI:
    apply_modifiers(obj)

    # Deleting the cube
bpy.ops.object.select_all(action="DESELECT")
cube.select_set(True)
bpy.ops.object.delete(use_global=False)


# END
