# SPDX-FileCopyrightText: 2022 Benjamin Tousch
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Wolfgang Amadeus Mozart: Musikalisches Würfelspiel

"""import bpy   #blender
import random #random

listPiecePossible = ['Peripherie Tempietto Sol','Peripherie Tempietto Middle','Peripherie Tempietto Toit','Angle Savoye Middle']   #liste fragments
bpy.ops.object.select_all(action='SELECT')   #selectionner tout
for piece in listPiecePossible:
    bpy.data.objects[piece].select_set(False)   #déselectionne les pièces qui sont dans la liste
bpy.ops.object.delete()   #supprime le reste
bpy.ops.outliner.orphans_purge()   #purge

listAdress = []

for z in range (3):  #coordonnées en z allant de 1 à 3
    for  y in range (3):   #coordonnées en y allant de 1 à 3
        for x in range (3):   #coordonnées en z allant de 1 à 3
            listAdress.append((x,y,z))   #ajouter valeur (x,y,z) à la liste

print(listAdress)


for adress in listAdress:   #on nomme la valeur 'adress' dans la liste 'listadress'
    bpy.ops.object.select_all(action='DESELECT')

    if adress[2] == 3:   #tous les valeur z=3
        pieceChoisi = 'Peripherie Tempietto Toit'   #on aura un toit
    elif adress
    elif adress[0] == 3 and adress[1] == 0:
        pieceChoisi = 'Peripherie Tempietto Middle'
    else:
        pieceChoisi = random.choice(listPiecePossible)  #reste choisi au hasard

    bpy.data.objects[pieceChoisi].select_set(True)  #les pièces choisis
    bpy.ops.object.duplicate(linked=True)  #sont duppliquées à partir d'une unité
    bpy.ops.transform.translate(value=adress)  #puis translater sur le maillage en fonction de la valeur 'adress'
"""

import bpy  # Importation de blender
import random  # Importation de random


# Pièces de coin pour chaque étage (chaque liste est un étage)
list_angle_toit = [
    "Angle Rotonda Toit",
    "Angle Savoye Toit",
    "Angle Pompidou Toit",
    "Angle Savoye Toit 2",
    "Angle Pompidou Toit 2",
]
list_angle_middle = [
    "Angle Rotonda Middle",
    "Angle Savoye Middle",
    "Angle Pompidou Middle",
    "Angle Savoye Middle 2",
    "Angle Pompidou Middle 2",
]
list_angle_sol = [
    "Angle Rotonda Sol",
    "Angle Savoye",
    "Angle Pompidou Sol",
    "Angle Pompidou Sol 2",
]


# Pièces de périphérie pour chaque étage (chaque liste est un étage)
list_peripherie_toit = [
    "Peripherie Rotonda Toit",
    "Peripherie Savoye Toit",
    "Peripherie Pompidou Toit",
    "Peripherie Savoye Toit 2",
    "Peripherie Pompidou Toit 2",
]
list_peripherie_middle = [
    "Peripherie Rotonda Middle",
    "Peripherie Savoye Middle",
    "Peripherie Pompidou Middle",
    "Peripherie Savoye Middle 2",
    "Peripherie Pompidou Middle 2",
]
list_peripherie_sol = [
    "Peripherie Rotonda Sol",
    "Peripherie Savoye Sol",
    "Peripherie Pompidou Sol",
    "Peripherie Savoye Sol 2",
    "Peripherie Pompidou Sol 2",
]


# Pièces intérieur pour chaque étage (chaque liste est un étage)
list_interieur_toit = (
    [
        "Interieur Rontonda Toit",
        "Interieur Savoye Toit",
        "Interieur Pompidou Toit",
        "Interieur Savoye Toit 2",
        "Interieur Pompidou Toit 2",
    ],
)
list_interieur_middle = (
    [
        "Interieur Rontonda Middle",
        "Interieur Savoye Middle",
        "Interieur Pompidou Middle",
        "Interieur Savoye Middle 2",
        "Interieur Pompidou Middle 2",
    ],
)
list_interieur_sol = [
    "Interieur Rontonda Toit",
    "Interieur Savoye Toit",
    "Interieur Pompidou Toit",
    "Interieur Savoye Toit 2",
    "Interieur Pompidou Toit 2",
]

bpy.ops.object.select_all(action="SELECT")  # Selectionner tout
for etage in list_angle_toit:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_angle_middle:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_angle_sol:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste


bpy.ops.object.select_all(action="SELECT")  # Selectionner tout
for etage in list_peripherie_toit:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_peripherie_middle:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_interieur_middle:
    for piece in list_interieur_toit:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

bpy.ops.object.select_all(action="SELECT")  # Selectionner tout
for etage in list_interieur_sol:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_peripherie_middle:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

for etage in list_peripherie_sol:
    for piece in etage:
        bpy.data.objects[piece].select_set(
            False
        )  # Déselectionne les pièces qui sont dans la liste

bpy.ops.object.delete()  # Supprime le reste
bpy.ops.outliner.orphans_purge()  # Purge

for z in range(3):  # Coordonnées en z allant de 1 à 3
    for y in range(3):  # Coordonnées en y allant de 1 à 3
        for x in range(3):  # Coordonnées en z allant de 1 à 3

            if (x == 0 and (y == 0 or y == 2)) or (
                x == 2 and (y == 0 or y == 2)
            ):  # Chaque coordonnée de coin
                piece = random.choice(
                    listCornerPieces[z]
                )  # On choisit une pièce au hasard dans les pièces de coin au bon étage

            if (y == 1 and (x == 0 or x == 2)) or (
                x == 1 and (y == 0 or y == 2)
            ):  # Chaque coordonnée de périphérie
                piece = random.choice(
                    listPeripheryPieces[z]
                )  # On choisit une pièce au hasard dans les pièces de périphérie au bon étage

            if x == 1 and y == 1:  # Chaque coordonnée intérieur
                piece = random.choice(
                    listInteriorPieces[z]
                )  # On choisit une pièce au hasard dans les pièces intérieures au bon étage

            bpy.data.objects[piece].select_set(True)  # Les pièces choisis
            bpy.ops.object.duplicate(
                linked=True
            )  # Sont duppliquées à partir d'une unité
            bpy.ops.transform.translate(
                value=(x, y, z)
            )  # Puis translater sur le maillage en fonction de la valeur 'adress'
