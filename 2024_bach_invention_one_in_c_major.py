# SPDX-FileCopyrightText: 2025 Kaede Otsubo & Nousseyba Darkaoui
# SPDX-FileCopyrightText: 2025 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy


def clean():
    bpy.ops.object.select_all(action="SELECT")
    bpy.data.objects["pattern"].select_set(False)
    bpy.ops.object.delete()
    bpy.ops.outliner.orphans_purge()


clean()


##################
# making patterns #
##################

for x in range(1, 3):
    for z in range(1, 9):
        bpy.data.objects["pattern"].select_set(True)
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": True, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={"value": (-6.5 * x, 0, z * 2)},
        )
        for obj in bpy.context.selected_objects:
            obj.name = "copy"

        if x == 1:
            if z == 1:
                bpy.ops.transform.resize(value=(1, 1, 1))
                bpy.ops.transform.translate(value=(0, 0, 0))
            if z == 2:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -1.155))
            if z == 3:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -2.31))
            if z == 4:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -1.2))
            if z == 5:
                bpy.ops.transform.resize(value=(1.7, 1, 1))
                bpy.ops.transform.translate(value=(-4, 0, -1.25))
            if z == 6:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -2.7))
            if z == 7:
                bpy.ops.transform.resize(value=(1.7, 1, 1))
                bpy.ops.transform.translate(value=(-4, 0, -2.75))
            if z == 8:
                bpy.ops.transform.resize(value=(1, 1, 1))
                bpy.ops.transform.translate(value=(0.15, 0, -2.5))

        if x == 2:
            if z == 1:
                bpy.ops.transform.resize(value=(1, 1, 1))
                bpy.ops.transform.translate(value=(0, 0, 2))
            if z == 2:
                bpy.ops.transform.resize(value=(1, 1, 1))
                bpy.ops.transform.translate(value=(0, 0, 0.845))
            if z == 3:
                bpy.ops.transform.resize(value=(1, 1, 1))
                bpy.ops.transform.translate(value=(0, 0, -0.31))
            if z == 4:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -1.2))
            if z == 6:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.5, 0, -2.7))
            if z == 5 or z == 7:
                bpy.ops.object.delete(use_global=False, confirm=False)
            if z == 8:
                bpy.ops.transform.resize(value=(-1, 1, -1))
                bpy.ops.transform.translate(value=(1.75, 0, -3.7))

        bpy.ops.object.select_all(action="DESELECT")


##################
# making pillars #
##################

for i, (y, r) in enumerate([(-7.5, 0.15), (-2.5, 0.2), (2.5, 0.15), (7.5, 0.3)]):
    bpy.ops.mesh.primitive_cylinder_add(
        radius=r, depth=2.25, location=(-9, 0.4 * y, 1.125)
    )
    if i == 0 or i == 2:
        bpy.ops.transform.translate(value=(0.25, 0, 0))
    if i == 1:
        bpy.ops.transform.translate(value=(0.1, 0, 0))

for j, (y, r) in enumerate([(-7.5, 0.2), (-2.5, 0.15), (2.5, 0.3), (7.5, 0.2)]):
    bpy.ops.mesh.primitive_cylinder_add(
        radius=r, depth=0.7, location=(-9, 0.4 * y, 2.735)
    )
    if j == 1:
        bpy.ops.transform.translate(value=(0.25, 0, 0))
    if j == 0 or j == 3:
        bpy.ops.transform.translate(value=(0.1, 0, 0))

for k, (y, r) in enumerate([(-7.5, 0.2), (-2.5, 0.15), (2.5, 0.3), (7.5, 0.2)]):
    bpy.ops.mesh.primitive_cylinder_add(radius=r, depth=2, location=(-9, 0.4 * y, 4.2))
    if k == 1:
        bpy.ops.transform.translate(value=(0.25, 0, 0))
    if k == 0 or k == 3:
        bpy.ops.transform.translate(value=(0.1, 0, 0))

for l, (y, r) in enumerate([(-7.5, 0.1), (-7.5, 0.1), (7.5, 0.1), (7.5, 0.1)]):
    bpy.ops.mesh.primitive_cylinder_add(
        radius=r, depth=4.5, location=(-8.75, 0.4 * y, 7.9)
    )
    if l == 0 or l == 3:
        bpy.ops.transform.translate(value=(1.7, 0, 0))
    if l == 1 or l == 2:
        bpy.ops.transform.translate(value=(-5, 0, 0.49))
        bpy.ops.transform.resize(value=(1, 1, 1.23))

for m, (y, r) in enumerate([(-7.5, 0.1), (-2.5, 0.15), (2.5, 0.3), (7.5, 0.2)]):
    bpy.ops.mesh.primitive_cylinder_add(radius=r, depth=2, location=(-9, 0.4 * y, 11.8))
    if m == 2:
        bpy.ops.transform.translate(value=(0.25, 0, 0))
    if m == 0:
        bpy.ops.transform.translate(value=(0.1, 0, 0))
    if m == 3:
        bpy.ops.transform.translate(value=(0.15, 0, 0))

from math import radians

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.duplicate_move(TRANSFORM_OT_translate={"value": (0, -8, 1.5)})
bpy.ops.transform.rotate(value=radians(180), orient_axis="X")
