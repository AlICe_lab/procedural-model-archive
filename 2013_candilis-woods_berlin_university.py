# SPDX-FileCopyrightText: 2013 Arthur Lachard
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# ///////////////////////////////////////////////////////////////////////////////////////////////
# variables
# ///////////////////////////////////////////////////////////////////////////////////////////////

tr = 1
n_tr_x = 50
n_tr_y = 50
hauteur = 1

# ///////////////////////////////////////////////////////////////////////////////////////////////
# objets
# ///////////////////////////////////////////////////////////////////////////////////////////////


class MODULE:
    """Création d'une classe d'object contenant les différents modules
    et leurs renseignements : nom,calque,type,coordonnées,taille,nombre"""


# module 3
l = [0, 1]
longueur_possible = [30, 2]
rand = random.choice(l)
long = longueur_possible[rand]
larg = longueur_possible[rand - 1]
MOD_1 = MODULE()
MOD_1.liste = []
MOD_1.longueur = long
MOD_1.largeur = larg
MOD_1.hauteur = hauteur
MOD_1.nombre = 2
MOD_1.nom = "MOD_3"
MOD_1.form = "carré"
MOD_1.active = True

# module 4
MOD_2 = MODULE()
MOD_2.liste = []
MOD_2.longueur = larg
MOD_2.largeur = long
MOD_2.hauteur = hauteur
MOD_2.nombre = 2
MOD_2.nom = "MOD_4"
MOD_2.form = "carré"
MOD_2.active = True

# module 2
MOD_3 = MODULE()
MOD_3.liste = []
MOD_3.longueur = 4
MOD_3.largeur = 4
MOD_3.hauteur = hauteur
MOD_3.nombre = 15
MOD_3.nom = "MOD_2"
MOD_3.form = "carré"
MOD_3.active = True

# module 5
MOD_4 = MODULE()
MOD_4.liste = []
MOD_4.longueur = 3
MOD_4.largeur = 1
MOD_4.hauteur = hauteur
MOD_4.nombre = 50
MOD_4.nom = "MOD_5"
MOD_4.form = "carré"
MOD_4.active = True

# module 1
MOD_5 = MODULE()
MOD_5.liste = []
MOD_5.longueur = 1
MOD_5.largeur = 1
MOD_5.hauteur = hauteur
MOD_5.nombre = 200
MOD_5.nom = "MOD_1"
MOD_5.form = "carré"
MOD_5.active = True

# module
MOD_6 = MODULE()
MOD_6.liste = []
MOD_6.longueur = 3
MOD_6.largeur = 3
MOD_6.hauteur = hauteur
MOD_6.nombre = 10
MOD_6.nom = "MOD_5"
MOD_6.form = "rond"
MOD_6.active = True

# module
MOD_7 = MODULE()
MOD_7.liste = []
MOD_7.longueur = 2
MOD_7.largeur = 2
MOD_7.hauteur = hauteur
MOD_7.nombre = 30
MOD_7.nom = "MOD_5"
MOD_7.form = "rond"
MOD_7.active = True


# liste totale des modules
l = [MOD_1, MOD_2, MOD_3, MOD_4, MOD_5, MOD_6, MOD_7]
l_total = []
for object in l:
    if object.active == True:
        l_total.append(object)
# liste des objects suivant leurs formes
cube = []
rond = []
for object in l_total:
    if object.form == "carré":
        cube.append(object)
    elif object.form == "rond":
        rond.append(object)

# ///////////////////////////////////////////////////////////////////////////////////////////////
# création des coordonnées
# ///////////////////////////////////////////////////////////////////////////////////////////////

for objet in l_total:
    for x in range(0, n_tr_x):
        for y in range(0, n_tr_y):
            long_x = n_tr_x - 1 - x
            larg_y = n_tr_y - 1 - y
            X = x * tr
            Y = y * tr
            if long_x >= objet.longueur:
                if larg_y >= objet.largeur:
                    objet.liste.append((X, Y))

# ///////////////////////////////////////////////////////////////////////////////////////////////
# fonctions
# ///////////////////////////////////////////////////////////////////////////////////////////////


# fonction de suppression des coordonnées
def suppression(obj, x, y):
    for objet in l_total:
        dbt_X = 1 - objet.longueur
        dbt_Y = 1 - objet.largeur
        fin_X = obj.longueur
        fin_Y = obj.largeur
        for X in range(dbt_X, fin_X):
            for Y in range(dbt_Y, fin_Y):
                x_1 = x + X * tr
                y_1 = y + Y * tr
                if objet.liste.count((x_1, y_1)) == 1:
                    objet.liste.remove((x_1, y_1))


# fonction volume
def volume(obj, x, y):
    location = (x + obj.longueur / 2, y + obj.largeur / 2, 0 + obj.hauteur / 2)
    resize = (obj.longueur * tr, obj.largeur * tr, obj.hauteur)
    nom = obj.nom
    if cube.count(obj) == 1:
        bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
        bpy.ops.transform.resize(value=(resize))
        bpy.context.object.name = nom
    elif rond.count(obj) == 1:
        bpy.ops.mesh.primitive_cylinder_add(radius=0.5, location=location)
        bpy.ops.transform.resize(value=(resize))
        bpy.context.object.name = nom

    suppression(obj, x, y)


# execution des modules

for objet in l_total:
    w = 0
    while w < objet.nombre:
        x, y = random.choice(objet.liste)
        volume(objet, x, y)
        w = w + 1
