# SPDX-FileCopyrightText: 2020 Bassam Boughaba & Eliott Steylemans
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Réinterpretation de Dimensions I & II, Manfred Mohr
# Bassam Boug & Eliott Steylemans
# date 17/12/2020
# Option AIM 2020-2021 module 1

# ========================
# import modules

import bpy
import bmesh
import numpy as np
import random
from random import randrange, uniform


# load custom helper module
# _ = bpy.data.texts["helper_module.py"].as_module()
# _.check_import()
# _.delete_all_objects()


def clean():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)
    bpy.ops.outliner.orphans_purge()


clean()


# ------------------------------------


# Run camera script + adding a key light for rendering

filename1 = "camera.py"
exec(compile(open(filename1).read(), filename1, "exec"))


# ------------------------------------


# Create system of coordinates

context = bpy.context

# Size of the system
size = uniform(1, 3)
position_inner_cube_x = uniform(-2, 2)
position_inner_cube_y = uniform(-2, 2)
position_inner_cube_z = uniform(-2, 2)

verts = (
    [
        -1 * size + position_inner_cube_x,
        -1 * size + position_inner_cube_y,
        -1.0 * size + position_inner_cube_z,
    ],
    [
        -1.0 * size + position_inner_cube_x,
        -1.0 * size + position_inner_cube_y,
        1.0 * size + position_inner_cube_z,
    ],
    [
        -1.0 * size + position_inner_cube_x,
        1.0 * size + position_inner_cube_y,
        -1.0 * size + position_inner_cube_z,
    ],
    [
        -1.0 * size + position_inner_cube_x,
        1.0 * size + position_inner_cube_y,
        1.0 * size + position_inner_cube_z,
    ],
    [
        1.0 * size + position_inner_cube_x,
        -1.0 * size + position_inner_cube_y,
        -1.0 * size + position_inner_cube_z,
    ],
    [
        1.0 * size + position_inner_cube_x,
        -1.0 * size + position_inner_cube_y,
        1.0 * size + position_inner_cube_z,
    ],
    [
        1.0 * size + position_inner_cube_x,
        1.0 * size + position_inner_cube_y,
        -1.0 * size + position_inner_cube_z,
    ],
    [
        1.0 * size + position_inner_cube_x,
        1.0 * size + position_inner_cube_y,
        1.0 * size + position_inner_cube_z,
    ],
    [-5, -5, -5],
    [-5, -5, 5],
    [-5, 5, -5],
    [-5, 5, 5],
    [5, -5, -5],
    [5, -5, 5],
    [5, 5, -5],
    [5, 5, 5],
)

# Index of vertices in console to help us code
print("==========")
print("System of coordonates ([Index] + [x, y, z]) :")
for i, item in enumerate(verts, start=1):
    print(i, item)


# Creation of the complete outer cube

# Edges of the base of the outer cube
edges = {"outer_cube_base": [(12, 14), (14, 10), (10, 8), (8, 12)]}
# Creation of the base of the outer cube
me = bpy.data.meshes.new("Outer_cube_base")
me.from_pydata(verts, edges["outer_cube_base"], [])
ob = bpy.data.objects.new("outer_cube_base", me)
context.collection.objects.link(ob)
context.view_layer.objects.active = ob

# Edges of the outer_cube
edges = {
    "outer_cube": [
        (14, 15),
        (15, 13),
        (12, 13),
        (15, 11),
        (11, 10),
        (8, 9),
        (9, 13),
        (11, 9),
    ]
}
# Creation of outer_cube
me = bpy.data.meshes.new("Outer_cube")
me.from_pydata(verts, edges["outer_cube"], [])
ob = bpy.data.objects.new("outer_cube", me)
context.collection.objects.link(ob)
context.view_layer.objects.active = ob

# Creation of the inner cube
# Choice between two variants of construction
variant_choice_list = [0, 1]
random_choice = random.choice(variant_choice_list)

print("==========")
print("If = 0 -> Left side, else -> right side: ")
print(random_choice)

# Creation of the edges we need
if random_choice == 0:
    edges = {
        "group_1": [(14, 6), (6, 4), (4, 5), (5, 1), (5, 13)],
        "group_2": [(10, 2), (2, 6), (6, 7), (7, 5), (7, 15)],
        "group_3": [(8, 0), (0, 2), (2, 3), (3, 7), (3, 11)],
        "group_4": [(12, 4), (4, 0), (0, 1), (1, 3), (1, 9)],
    }

else:
    edges = {
        "group_1": [(14, 6), (6, 2), (2, 3), (3, 1), (3, 11)],
        "group_2": [(10, 2), (2, 0), (0, 1), (1, 5), (1, 9)],
        "group_3": [(8, 0), (0, 4), (4, 5), (5, 7), (5, 13)],
        "group_4": [(12, 4), (4, 6), (6, 7), (7, 3), (7, 15)],
    }


"""
# Creation of the mesh to help us visualize while working
faces = ((0, 1, 3, 2),
         (2, 3, 7, 6),
         (6, 7, 5, 4),
         (4, 5, 1, 0),
         (2, 6, 4, 0),
         (7, 3, 1, 5))

me = bpy.data.meshes.new("Cube")
me.from_pydata(verts, [], faces)
ob = bpy.data.objects.new("Inner Cube", me)
context.collection.objects.link(ob)
context.view_layer.objects.active = ob
"""

# Creation of the different groups
print("==========")
print("Creation of groups")
for i in range(1, 5):
    me = bpy.data.meshes.new("Group_{}".format(i))
    me.from_pydata(verts, edges["group_{}".format(i)], [])
    ob = bpy.data.objects.new("group_{}".format(i), me)
    context.collection.objects.link(ob)
    context.view_layer.objects.active = ob
    print(me)


# Deformation for the object to be only visible in a certain point of view
def deformation(group, index1, index2):
    # Select vertices
    bpy.ops.object.select_all(action="DESELECT")
    objectToSelect = bpy.data.objects[group]
    objectToSelect.select_set(True)
    bpy.context.view_layer.objects.active = objectToSelect
    bpy.ops.object.mode_set(mode="EDIT")
    bpy.ops.mesh.select_mode(type="VERT")
    bpy.ops.mesh.select_all(action="DESELECT")
    # we need to switch from Edit mode to Object mode so the selection gets updated
    bpy.ops.object.mode_set(mode="OBJECT")

    # Selection of the vertices we need
    for i in range(index1, index2):
        objectToSelect.data.vertices[i].select = True

    # Enter edit mode
    bpy.ops.object.mode_set(mode="EDIT")
    # Translation
    facteur_deplacement = uniform(2, 4)
    bpy.ops.transform.translate(
        value=(
            0.339 * facteur_deplacement,
            -0.339 * facteur_deplacement,
            0.64 * facteur_deplacement,
        )
    )
    # Exit edit mode for next step
    bpy.ops.object.mode_set(mode="OBJECT")
    bpy.ops.object.select_all(action="DESELECT")


deformation("group_2", 0, 8)
deformation("group_4", 0, 8)

deformation("outer_cube", 10, 14)


# From edges to curve
def make_pipe(group, depth):
    # Select the edges
    bpy.ops.object.select_all(action="DESELECT")
    objectToSelect = bpy.data.objects[group]
    objectToSelect.select_set(True)
    bpy.context.view_layer.objects.active = objectToSelect

    # Convert edge to curve
    bpy.ops.object.convert(target="CURVE")
    bpy.context.object.data.bevel_depth = depth
    bpy.context.object.data.bevel_resolution = 8
    bpy.context.object.data.use_fill_caps = True


make_pipe("group_1", 0.2)
make_pipe("group_2", 0.2)
make_pipe("group_3", 0.2)
make_pipe("group_4", 0.2)
make_pipe("outer_cube", 0.2)
make_pipe("outer_cube_base", 0.2)

# Deselect
bpy.ops.object.select_all(action="DESELECT")
