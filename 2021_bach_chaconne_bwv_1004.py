# SPDX-FileCopyrightText: 2021 Leyloiu Boey & Alexandre Troc
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Blender 2.93.4
# CUBE CYCLE

# NETTOYAGE
import bpy
import bmesh
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()


# ON DEFINITION LA FORMATION CUBE
def cube_formation(verts):
    face = [(0, 1, 2, 3)]
    me = bpy.data.meshes.new("")
    me.from_pydata(verts, [], face)
    ob = bpy.data.objects.new("", me)
    scene = bpy.context.scene
    scene.collection.objects.link(ob)


# DONNEES VARIATION CUBE
variation = [-0.1, 0.1, -0.25, 0.25]

# LISTE DE DONNEES 1 (POSITION) DE CUBE GENERE
# POSITION HAUT/BAS X/Y
PB1y = []
PB2x = []
PB3y = []
PB4x = []

# LISTE DE DONNEES 2 (POSITION) DE CUBE GENERE
PH1y = []
PH2x = []
PH3y = []
PH4x = []

# ------------------------NAISSANCE------------------------------
# BOUCLE DE CREATION DES FACES


def construction(variation):
    s_1_x = 0
    s_1_y = 0
    s_2_x = 1
    s_2_y = 0
    s_3_x = 1
    s_3_y = -1
    s_4_x = 0
    s_4_y = -1

    for a in range(0, 6):
        cube_formation(
            [(s_1_x, s_1_y, 0), (s_2_x, s_2_y, 0), (s_3_x, s_3_y, 0), (s_4_x, s_4_y, 0)]
        )
        s_1_x += 1
        s_1_y += 0 + random.uniform(variation[0], variation[1])
        s_2_x += 1 + random.uniform(variation[0], variation[1])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 - random.uniform(variation[0], variation[1])
        s_4_x += 1 - random.uniform(variation[0], variation[1])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y - 1, 0),
                (s_2_x - 1, s_2_y - 1, 0),
                (s_3_x - 1, s_3_y - 1, 0),
                (s_4_x - 1, s_4_y - 1, 0),
            ]
        )
        s_1_x += -1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += -1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += -1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x, s_1_y - 2, 0),
                (s_2_x, s_2_y - 2, 0),
                (s_3_x, s_3_y - 2, 0),
                (s_4_x, s_4_y - 2, 0),
            ]
        )
        s_1_x += 1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += 1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += 1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y - 3, 0),
                (s_2_x - 1, s_2_y - 3, 0),
                (s_3_x - 1, s_3_y - 3, 0),
                (s_4_x - 1, s_4_y - 3, 0),
            ]
        )
        s_1_x += -1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += -1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += -1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x, s_1_y - 4, 0),
                (s_2_x, s_2_y - 4, 0),
                (s_3_x, s_3_y - 4, 0),
                (s_4_x, s_4_y - 4, 0),
            ]
        )
        s_1_x += 1
        s_1_y += 0 + random.uniform(variation[1], variation[3])
        s_2_x += 1 + random.uniform(variation[1], variation[3])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 - random.uniform(variation[1], variation[3])
        s_4_x += 1 - random.uniform(variation[1], variation[3])
        s_4_y += 0
        PB1y.append(s_1_y)
        PB2x.append(s_2_x)
        PB3y.append(s_3_y)
        PB4x.append(s_4_x)
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y - 5, 0),
                (s_2_x - 1, s_2_y - 5, 0),
                (s_3_x - 1, s_3_y - 5, 0),
                (s_4_x - 1, s_4_y - 5, 0),
            ]
        )
        s_1_x += -1
        s_1_y += 0 - ((PB1y[4]) / 4.5)
        s_2_x += -1 - ((PB2x[4] - 6) / 4.5)
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - ((PB3y[4] + 1) / 4.5)
        s_4_x += -1 - ((PB4x[4] - 5) / 4.5)
        s_4_y += 0


construction(variation)

# EDITION DES CUBES NAISSANCES

for x in range(0, 30):
    obj = bpy.context.window.scene.objects[x]
    bpy.context.view_layer.objects.active = obj

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.extrude_context_move(
        MESH_OT_extrude_context={
            "use_normal_flip": False,
            "use_dissolve_ortho_edges": False,
            "mirror": False,
        },
        TRANSFORM_OT_translate={
            "value": (0, 0, 1 + (x * 0.055)),
            "orient_type": "NORMAL",
            "orient_matrix": ((0, 1, 0), (1, 0, 0), (0, 0, 1)),
            "orient_matrix_type": "NORMAL",
            "constraint_axis": (False, False, True),
            "mirror": False,
            "use_proportional_edit": False,
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "use_proportional_connected": False,
            "use_proportional_projected": False,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "cursor_transform": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": True,
            "use_accurate": False,
            "use_automerge_and_split": False,
        },
    )
    bpy.ops.transform.rotate(
        value=(x * (-0.015)),
        orient_axis="Z",
        orient_type="GLOBAL",
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
        orient_matrix_type="GLOBAL",
        constraint_axis=(False, False, True),
        mirror=True,
        use_proportional_edit=False,
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        use_proportional_connected=False,
        use_proportional_projected=False,
        release_confirm=True,
    )
    bpy.ops.object.editmode_toggle()

for x in range(30, 36):
    obj = bpy.context.window.scene.objects[x]
    bpy.context.view_layer.objects.active = obj

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.extrude_context_move(
        MESH_OT_extrude_context={
            "use_normal_flip": False,
            "use_dissolve_ortho_edges": False,
            "mirror": False,
        },
        TRANSFORM_OT_translate={
            "value": (0, 0, 1 + (x * 0.055)),
            "orient_type": "NORMAL",
            "orient_matrix": ((0, 1, 0), (1, 0, 0), (0, 0, 1)),
            "orient_matrix_type": "NORMAL",
            "constraint_axis": (False, False, True),
            "mirror": False,
            "use_proportional_edit": False,
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "use_proportional_connected": False,
            "use_proportional_projected": False,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "cursor_transform": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": True,
            "use_accurate": False,
            "use_automerge_and_split": False,
        },
    )
    bpy.ops.transform.rotate(
        value=(-3.3 + (x * 0.095)),
        orient_axis="Z",
        orient_type="GLOBAL",
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
        orient_matrix_type="GLOBAL",
        constraint_axis=(False, False, True),
        mirror=True,
        use_proportional_edit=False,
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        use_proportional_connected=False,
        use_proportional_projected=False,
        release_confirm=True,
    )
    bpy.ops.object.editmode_toggle()


# ----------------------------------VIE + MORT---------------------------
# BOUCLE DE CREATION DES FACES


def construction(variation):
    s_1_x = 0
    s_1_y = -5 + random.uniform(variation[0], variation[1])
    s_2_x = 1 + random.uniform(variation[0], variation[1])
    s_2_y = -5
    s_3_x = 1
    s_3_y = -6 - random.uniform(variation[0], variation[1])
    s_4_x = 0 - random.uniform(variation[0], variation[1])
    s_4_y = -6
    for a in range(0, 6):
        cube_formation(
            [(s_1_x, s_1_y, 6), (s_2_x, s_2_y, 6), (s_3_x, s_3_y, 6), (s_4_x, s_4_y, 6)]
        )
        s_1_x += 1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += 1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += 1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y + 1, 6),
                (s_2_x - 1, s_2_y + 1, 6),
                (s_3_x - 1, s_3_y + 1, 6),
                (s_4_x - 1, s_4_y + 1, 6),
            ]
        )
        s_1_x += -1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += -1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += -1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x, s_1_y + 2, 6),
                (s_2_x, s_2_y + 2, 6),
                (s_3_x, s_3_y + 2, 6),
                (s_4_x, s_4_y + 2, 6),
            ]
        )
        s_1_x += 1
        s_1_y += 0 + random.uniform(variation[2], variation[3])
        s_2_x += 1 + random.uniform(variation[2], variation[3])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 - random.uniform(variation[2], variation[3])
        s_4_x += 1 - random.uniform(variation[2], variation[3])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y + 3, 6),
                (s_2_x - 1, s_2_y + 3, 6),
                (s_3_x - 1, s_3_y + 3, 6),
                (s_4_x - 1, s_4_y + 3, 6),
            ]
        )
        s_1_x += -1
        s_1_y += 0 + random.uniform(variation[1], variation[2])
        s_2_x += -1 + random.uniform(variation[1], variation[2])
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - random.uniform(variation[1], variation[2])
        s_4_x += -1 - random.uniform(variation[1], variation[2])
        s_4_y += 0
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x, s_1_y + 4, 6),
                (s_2_x, s_2_y + 4, 6),
                (s_3_x, s_3_y + 4, 6),
                (s_4_x, s_4_y + 4, 6),
            ]
        )
        s_1_x += 1
        s_1_y += 0 - random.uniform(variation[1], variation[3])
        s_2_x += 1 - random.uniform(variation[1], variation[3])
        s_2_y += 0
        s_3_x += 1
        s_3_y += 0 + random.uniform(variation[1], variation[3])
        s_4_x += 1 + random.uniform(variation[1], variation[3])
        s_4_y += 0
        PH1y.append(s_1_y)
        PH2x.append(s_2_x)
        PH3y.append(s_3_y)
        PH4x.append(s_4_x)
    for a in range(0, 6):
        cube_formation(
            [
                (s_1_x - 1, s_1_y + 5, 6),
                (s_2_x - 1, s_2_y + 5, 6),
                (s_3_x - 1, s_3_y + 5, 6),
                (s_4_x - 1, s_4_y + 5, 6),
            ]
        )
        s_1_x += -1
        s_1_y += 0 - ((PH1y[5] + 6) / 5)
        s_2_x += -1 - ((PH2x[5] - 6) / 5)
        s_2_y += 0
        s_3_x += -1
        s_3_y += 0 - ((PH3y[5] + 5) / 5)
        s_4_x += -1 - ((PH4x[5] - 7) / 5)
        s_4_y += 0


construction(variation)

# EDITION DES CUBES VIE MORT

for x in range(0, 24):
    obj = bpy.context.window.scene.objects[x + 36]
    bpy.context.view_layer.objects.active = obj

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.extrude_context_move(
        MESH_OT_extrude_context={
            "use_normal_flip": False,
            "use_dissolve_ortho_edges": False,
            "mirror": False,
        },
        TRANSFORM_OT_translate={
            "value": (0, 0, -3 + (x * 0.08)),
            "orient_type": "NORMAL",
            "orient_matrix": ((0, 1, 0), (1, 0, 0), (0, 0, 1)),
            "orient_matrix_type": "NORMAL",
            "constraint_axis": (False, False, True),
            "mirror": False,
            "use_proportional_edit": False,
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "use_proportional_connected": False,
            "use_proportional_projected": False,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "cursor_transform": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": True,
            "use_accurate": False,
            "use_automerge_and_split": False,
        },
    )
    bpy.ops.transform.rotate(
        value=(x * 0.05),
        orient_axis="Z",
        orient_type="GLOBAL",
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
        orient_matrix_type="GLOBAL",
        constraint_axis=(False, False, True),
        mirror=True,
        use_proportional_edit=False,
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        use_proportional_connected=False,
        use_proportional_projected=False,
        release_confirm=True,
    )
    bpy.ops.object.editmode_toggle()

for x in range(0, 12):
    obj = bpy.context.window.scene.objects[x + 60]
    bpy.context.view_layer.objects.active = obj

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.extrude_context_move(
        MESH_OT_extrude_context={
            "use_normal_flip": False,
            "use_dissolve_ortho_edges": False,
            "mirror": False,
        },
        TRANSFORM_OT_translate={
            "value": (0, 0, -1.3 - (x * 0.155)),
            "orient_type": "NORMAL",
            "orient_matrix": ((0, 1, 0), (1, 0, 0), (0, 0, 1)),
            "orient_matrix_type": "NORMAL",
            "constraint_axis": (False, False, True),
            "mirror": False,
            "use_proportional_edit": False,
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "use_proportional_connected": False,
            "use_proportional_projected": False,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "cursor_transform": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": True,
            "use_accurate": False,
            "use_automerge_and_split": False,
        },
    )
    bpy.ops.transform.rotate(
        value=(+0.7 + (x * (-0.06))),
        orient_axis="Z",
        orient_type="GLOBAL",
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
        orient_matrix_type="GLOBAL",
        constraint_axis=(False, False, True),
        mirror=True,
        use_proportional_edit=False,
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        use_proportional_connected=False,
        use_proportional_projected=False,
        release_confirm=True,
    )
    bpy.ops.object.editmode_toggle()


# ----------------------LIMITES-------------------
verts = [
    (0, 0, 0),
    (6, 0, 0),
    (6, -6, 0),
    (0, -6, 0),
    (0, 0, 6),
    (6, 0, 6),
    (6, -6, 6),
    (0, -6, 6),
]
edges = [
    (0, 1),
    (1, 2),
    (2, 3),
    (3, 0),
    (4, 5),
    (5, 6),
    (6, 7),
    (7, 4),
    (0, 4),
    (1, 5),
    (2, 6),
    (3, 7),
]

me = bpy.data.meshes.new("")
me.from_pydata(verts, edges, [])
ob = bpy.data.objects.new("", me)
scene = bpy.context.scene
scene.collection.objects.link(ob)
