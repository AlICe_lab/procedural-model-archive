# SPDX-FileCopyrightText: 2020 Camille Salignon & Spyridon Kellergis
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random as rand
from random import random


def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)
    bpy.ops.outliner.orphans_purge()


nettoyage()

for i in range(1):
    # initialised lists that will include vertices for
    list1 = []
    list2 = []

    # created initial vertex
    initial_point_0 = [random(), random(), 0]
    initial_point_1 = [random(), random(), 1]

    # created tuple object from list object
    vertex_0 = tuple(initial_point_0)
    vertex_1 = tuple(initial_point_1)
    list1.append(vertex_0)
    list2.append(vertex_1)

    # specify how much to add
    adding_x = 0.1
    adding_y = rand.uniform(0, 0.4)
    counter = 0

    print(initial_point_0)
    print("initial_point_0[1]")
    print(initial_point_0[2])

    for i in range(18):
        my_number = rand.choice([-1, 1])
        adding_y1 = rand.uniform(0, 0.4)
        adding_y2 = rand.uniform(0, 0.4)
        print("my number")
        print(my_number)
        if counter % 2 == 0:
            initial_point_0[0] = initial_point_0[0] + my_number * adding_x
            initial_point_1[0] = initial_point_1[0] + my_number * adding_x
            # check if point on plane 0 is out of bounds
            # out of bounds
            if (
                initial_point_0[0] > 1
                or initial_point_1[0] > 1
                or initial_point_0[0] < 0
                or initial_point_1[0] < 0
            ):
                initial_point_0[0] = initial_point_0[0] - 2 * my_number * adding_x
                initial_point_1[0] = initial_point_1[0] - 2 * my_number * adding_x
            # check if point on plane 1 is out of bounds
            # outof bounds
            """
            elif (initial_point_1[0]>1 or initial_point_1[0]<0):
                initial_point_1[0] = initial_point_1[0] - 2*my_number*adding_x
            """
            vertex_0 = tuple(initial_point_0)
            vertex_1 = tuple(initial_point_1)
            list1.append(vertex_0)
            list2.append(vertex_1)
        else:
            initial_point_0[1] = initial_point_0[1] + my_number * adding_y1
            initial_point_1[1] = initial_point_1[1] + my_number * adding_y2
            if (
                initial_point_0[1] > 1
                or initial_point_1[1] > 1
                or initial_point_0[1] < 0
                or initial_point_1[1] < 0
            ):
                initial_point_0[1] = initial_point_0[1] - 2 * my_number * adding_y1
                initial_point_1[1] = initial_point_1[1] - 2 * my_number * adding_y2
            # check if point on plane 1 is out of bounds
            # outof bounds
            """
            elif (initial_point_1[1]>1 or initial_point_1[1]<0):
                initial_point_1[1] = initial_point_1[1] - 2*my_number*adding_y
            """
            vertex_0 = tuple(initial_point_0)
            vertex_1 = tuple(initial_point_1)
            list1.append(vertex_0)
            list2.append(vertex_1)
        counter = counter + 1

    vertices = list1 + list2
    print("vertices")
    print(vertices)
    print(len(list1))
    print(len(list2))
    print(len(vertices))

    edges = []
    faces = []

    length = len(vertices) // 2
    print(length)
    for i in range(length - 1):
        face = (i, i + 1, i + length + 1, i + length)
        faces.append(face)

    move_vertex_by = 0
    for j in range(4):
        move_vertex_by = rand.uniform(0, 0.5)  # move_vertex_by*0.2
        # create vertex lists 3 more times

        vertices_list = []
        for k in range(len(vertices)):
            vertex_list = list(vertices[k])
            vertex_list[0] = vertex_list[0] + move_vertex_by
            vertex_list[1] = vertex_list[1] + move_vertex_by
            vertex_tuple = tuple(vertex_list)
            vertices_list.append(vertex_tuple)
        # draw
        new_mesh = bpy.data.meshes.new("new_mesh")
        new_mesh.from_pydata(vertices_list, edges, faces)
        new_mesh.update()

        # make object from mesh
        new_object = bpy.data.objects.new("new_object", new_mesh)

        # make collection
        new_collection = bpy.data.collections.new("new_collection")
        bpy.context.scene.collection.children.link(new_collection)

        # add object to scene collection
        new_collection.objects.link(new_object)

# draw
new_mesh = bpy.data.meshes.new("new_mesh")
new_mesh.from_pydata(vertices, edges, faces)
new_mesh.update()

# make object from mesh
new_object = bpy.data.objects.new("new_object", new_mesh)

# make collection
new_collection = bpy.data.collections.new("new_collection")
bpy.context.scene.collection.children.link(new_collection)

# add object to scene collection
new_collection.objects.link(new_object)
