# SPDX-FileCopyrightText: 2014 Thanh Thi Truong
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# PYTHON INTERACTIVE CONSOLE 3.4.0 (default, Apr 15 2014, 12:26:10)  [GCC 4.2.1 Compatible Apple LLVM 5.1 (clang-503.0.40)]

import bpy
import random


def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)


nettoyage()

# --------------------------------------------#
###----DEFINITION DES ÉLÉMENTS DE BASE-----###
# --------------------------------------------#


# DEFINITION DU MODULE DE BASE
def monModule(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    mesVertices = [
        (-dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, dim_Y / 2, dim_Z),
        (-dim_X / 2, dim_Y / 2, dim_Z),
    ]
    mesFaces = [
        (3, 2, 1, 0),
        (4, 5, 6, 7),
        (1, 5, 4, 0),
        (3, 7, 6, 2),
        (0, 4, 7, 3),
        (2, 6, 5, 1),
    ]
    monMesh = bpy.data.meshes.new("ModuleMesh")
    monObjet = bpy.data.objects.new("Module", monMesh)
    maScene = bpy.context.scene
    maScene.objects.link(monObjet)
    monMesh.from_pydata(mesVertices, [], mesFaces)
    monObjet.location = (pos_X, pos_Y, pos_Z)


# DIMENSIONS DES COULOIRS
LCoul = 1
lCoul = 1
hCoul = 1
LMur = 0.2

# DIMENSION DU MODULE CARRE
dim_ModX, dim_ModY, dim_ModZ = 3, 3, 1

# --------------------------------------------#
# --- ====> ENTRER LE NOMBRE DE RANGÉES ------#
# --------------------------------------------#

a = random.randint(1, 4)

_Rangée = a

# --------------------------------------------#
# --------------------------------------------#


# --------------------------------------------#
###--------DEFINITION DES 20 MURS----------###
# --------------------------------------------#

# MUR 1


def monMur1(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        3 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 1 a ÈtÈ exÈcutÈ")


# MUR 2


def monMur2(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        3 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 2 a ÈtÈ exÈcutÈ")


# MUR 3


def monMur3(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        3 + LMur,
        1,
        monModule,
    )
    print("mur 3 a ÈtÈ exÈcutÈ")


# MUR 4


def monMur4(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        3 + LMur,
        1,
        monModule,
    )
    print("mur 4 a ÈtÈ exÈcutÈ")


# MUR 5


def monMur5(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 5 a ÈtÈ exÈcutÈ")


# MUR 6


def monMur6(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 2,
        1,
        monModule,
    )
    print("mur 6 a ÈtÈ exÈcutÈ")


# MUR 7


def monMur7(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 7 a ÈtÈ exÈcutÈ")


# MUR 8


def monMur8(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    print("mur 8 a ÈtÈ exÈcutÈ")


# MUR 9


def monMur9(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 1,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 2,
        1,
        monModule,
    )
    print("mur 9 a ÈtÈ exÈcutÈ")


# MUR 10


def monMur10(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    print("mur 10 a ÈtÈ exÈcutÈ")


# MUR 11


def monMur11(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    print("mur 11 a ÈtÈ exÈcutÈ")


# MUR 12


def monMur12(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 3,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 12 a ÈtÈ exÈcutÈ")


# MUR 13


def monMur13(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    print("mur 13 a ÈtÈ exÈcutÈ")


# MUR 14


def monMur14(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 14 a ÈtÈ exÈcutÈ")


# MUR 15


def monMur15(
    m, b, p, o
):  # m et b son les coefficients de translation et f est le facteur pour le quadrant
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        3 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    print("mur 15 a ÈtÈ exÈcutÈ")


# MUR 16


def monMur16(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    print("mur 16 a ÈtÈ exÈcutÈ")


# MUR 17


def monMur17(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        3 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        2 + LMur,
        1,
        monModule,
    )
    print("mur 17 a ÈtÈ exÈcutÈ")


# MUR 19


def monMur19(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 1,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 1,
        1,
        monModule,
    )
    print("mur 19 a ÈtÈ exÈcutÈ")


# MUR 20


def monMur20(m, b, p, o):
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        1 + LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-1)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        1 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (-0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (0)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 1,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (0.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-0.5)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        2 + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * m) + LCoul / 2) + (1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + LCoul
            + (-1)
            + ((LCoul + dim_ModX) * (nb - 1))
        ),
        -1,
        LMur,
        LMur + 1,
        1,
        monModule,
    )
    print("mur 20 a ÈtÈ exÈcutÈ")


# --------------------------------------------#
###----------FONCTION ALÉATOIRE------------###
# --------------------------------------------#

nb = a


def murfind(m, b, p, o):
    lotterie = random.randint(0, 18)

    if lotterie is 0:
        monMur6(m, b, p, o)
    if lotterie is 1:
        monMur9(m, b, p, o)
    if lotterie is 2:
        monMur19(m, b, p, o)
    if lotterie is 3:
        monMur20(m, b, p, o)
    if lotterie is 4:
        monMur15(m, b, p, o)
    if lotterie is 5:
        monMur12(m, b, p, o)
    if lotterie is 6:
        monMur4(m, b, p, o)
    if lotterie is 7:
        monMur3(m, b, p, o)
    if lotterie is 8:
        monMur2(m, b, p, o)
    if lotterie is 9:
        monMur1(m, b, p, o)
    if lotterie is 10:
        monMur5(m, b, p, o)
    if lotterie is 11:
        monMur7(m, b, p, o)
    if lotterie is 12:
        monMur8(m, b, p, o)
    if lotterie is 13:
        monMur10(m, b, p, o)
    if lotterie is 14:
        monMur11(m, b, p, o)
    if lotterie is 15:
        monMur13(m, b, p, o)
    if lotterie is 16:
        monMur14(m, b, p, o)
    if lotterie is 17:
        monMur16(m, b, p, o)
    if lotterie is 18:
        monMur17(m, b, p, o)


# --------------------------------------------#
###-----ATTACHES ENTRES LES QUADRANTS------###
# --------------------------------------------#

# ------------------------#
# ATTACHE ENTRE Q4 et Q2 #
# ------------------------#
nb = a


def monAttache():

    monModule(
        (nb - 1) * (dim_ModX + LCoul) + (LCoul + dim_ModX) / 2 - LCoul / 2,
        -(LCoul + dim_ModX + LCoul / 2),
        -1,
        dim_ModX + LCoul,
        LMur,
        1,
        monModule,
    )
    monModule(
        (LCoul + dim_ModX) * (nb - 1) - LCoul / 2,
        -(dim_ModX) / 2 - LCoul / 2,
        -1,
        LMur,
        dim_ModX + LMur + 2 * LCoul,
        1,
        monModule,
    )
    monModule(
        (nb - 1) * (dim_ModX + LCoul)
        + (LCoul + dim_ModX) / 2
        + (dim_ModX + LCoul) / 2
        - LCoul / 2,
        +LCoul / 2,
        -1,
        2 * dim_ModX + 2 * LCoul,
        LMur,
        1,
        monModule,
    )
    monModule(
        (LCoul + dim_ModX) * (nb + 1) - LCoul / 2,
        (dim_ModX + LCoul) / 2 + LCoul / 2,
        -1,
        LMur,
        dim_ModX + LCoul + LMur,
        1,
        monModule,
    )


monAttache()


# ------------------------#
# ATTACHE ENTRE Q4 et Q3 #
# ------------------------#
def monAttache():

    monModule(
        -(LCoul / 2 + dim_ModX),
        -((nb - 1) * (dim_ModX + LCoul) + (LCoul + dim_ModX) / 2),
        -1,
        LMur,
        dim_ModX,
        1,
        monModule,
    )
    monModule(
        -(dim_ModX + LCoul) / 2,
        -((LCoul + dim_ModX) * (nb - 1) + LCoul / 2),
        -1,
        dim_ModX + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        -LCoul / 2,
        -(
            (nb - 1) * (dim_ModX + LCoul)
            + (LCoul + dim_ModX) / 2
            + (dim_ModX) / 2
            + (LCoul / 2)
        ),
        -1,
        LMur,
        2 * dim_ModX + LCoul + LMur,
        1,
        monModule,
    )
    monModule(
        0,
        -((LCoul + dim_ModX) * (nb + 1) - LCoul / 2),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        0,
        -((LCoul + dim_ModX) * (nb + 1) - LCoul / 2),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        -(LCoul + dim_ModX),
        -((LCoul + dim_ModX) * (nb) - LCoul / 2),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )


monAttache()

# ------------------------#
# ATTACHE ENTRE Q3 et Q1 #
# ------------------------#


def monAttache():

    monModule(
        -((LCoul + dim_ModX) * (nb - 1) - LCoul / 2),
        (LCoul + dim_ModX) / 2,
        -1,
        LMur,
        dim_ModX + 2 * LCoul + 2 * LMur,
        1,
        monModule,
    )
    monModule(
        -((nb - 1) * (dim_ModX + LCoul) + (LCoul) / 2 + (dim_ModX + LCoul) / 2 + LCoul),
        (LCoul / 2) - LMur / 2 + LCoul - (dim_ModX + LCoul) / 2,
        -1,
        2 * dim_ModX + 2 * LCoul,
        LMur,
        1,
        monModule,
    )
    monModule(
        -((nb - 1) * (dim_ModX + LCoul) + (dim_ModX + LCoul) / 2 - LCoul / 2),
        (LCoul / 2)
        + LMur / 2
        + LCoul
        + dim_ModX
        + LCoul
        + LCoul
        - (dim_ModX + LCoul) / 2,
        -1,
        dim_ModX + LCoul,
        LMur,
        1,
        monModule,
    )
    monModule(
        -((LCoul + dim_ModX) * (nb) - LCoul / 2 - LMur / 2),
        (LCoul + dim_ModX) * 2 + LCoul / 2 - (dim_ModX + LCoul) / 2,
        -1,
        LMur,
        LCoul + dim_ModX,
        1,
        monModule,
    )


monAttache()

# ------------------------#
# ATTACHE ENTRE Q4 et Q1 #
# ------------------------#


def monAttache():

    monModule(
        (LCoul / 2 + dim_ModX),
        ((nb) * (dim_ModX + LCoul) - (LCoul) / 2),
        -1,
        LMur,
        2 * dim_ModX + (dim_ModX + LCoul) / 2 + LMur,
        1,
        monModule,
    )
    monModule(
        (dim_ModX) / 2 + LCoul / 2,
        ((LCoul + dim_ModX) * (nb - 1) - LCoul / 2),
        -1,
        dim_ModX + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        LCoul / 2,
        ((nb) * (dim_ModX + LCoul) + (LCoul + dim_ModX) / 2 + LCoul / 2)
        - (LCoul)
        + LMur / 2,
        -1,
        LMur,
        3 * dim_ModX + LCoul + ((dim_ModX + LCoul) / 2),
        1,
        monModule,
    )
    monModule(
        dim_ModX + LCoul,
        (
            (LCoul + dim_ModX) * (nb + 1)
            - (dim_ModX + LCoul) / 2
            + LCoul / 2
            + LCoul
            + LMur
        ),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )
    monModule(
        0,
        ((LCoul + dim_ModX) * (nb + 2) - dim_ModX / 2 + LCoul / 2 + LCoul / 2 + LMur),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )


monAttache()

# ---------------------------------------------#
###----------FONCTION GENERATRICE-----------###
###------------LE BASEMENT PLAN-------------###
# ---------------------------------------------#

nb = a

for m in range(0, nb):
    b = -m
    murfind(m, b, 1, -1)
    m += 1
    b += 1

for m in range(0, nb):
    b = -m
    murfind(m + 1, b, 1, 1)
    m += 1
    b += 1

for m in range(0, nb):
    b = -m
    murfind(m + 1, b - 1, -1, -1)
    m += 1
    b += 1

for m in range(0, nb):
    b = -m
    murfind(m, b + 1, -1, 1)
    m += 1
    b += 1


# ---------------------------#
# ATTACHE ENTRE LES MODULES #
# ---------------------------#


def mon_Attache(m, b, p, o):
    print("mon_Attache a ÈtÈ exÈcutÈ")

    # ATTACHE
    monModule(
        p
        * ((1.5 + ((LCoul + dim_ModX) * (m)) + LCoul / 2 + dim_ModX + LCoul) + (-1.5)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) - LCoul / 2)
            + (1.5 + LCoul / 2)
            + LCoul
            + ((LCoul + dim_ModX) * (nb - 2))
        ),
        -1,
        LMur,
        LCoul + LMur,
        1,
        monModule,
    ),
    monModule(
        p * ((1.5 + ((LCoul + dim_ModX) * (m)) + LCoul / 2) + (1.5 + LCoul / 2)),
        o
        * (
            (1.5 + ((LCoul + dim_ModX) * b) + LCoul / 2)
            + dim_ModX
            + (-1.5)
            + LCoul
            + ((LCoul + dim_ModX) * (nb - 2))
        ),
        -1,
        LCoul + LMur,
        LMur,
        1,
        monModule,
    )


# QUADRANT X POSTIF & Y POSITIF

for m in range(0, nb - 1):
    b = -m
    mon_Attache((m + 1), (b + 1), 1, 1)
    m += 1
    b += 1

# QUADRANT X NÉGATIF & Y POSITIF

for m in range(0, nb - 1):
    b = -m
    mon_Attache((m), (b + 2), -1, 1)
    m += 1
    b += 1

# QUADRANT X NÉGATIF & Y NÉGATIF

for m in range(0, nb - 1):
    b = -m
    mon_Attache((m + 1), (b), -1, -1)
    m += 1
    b += 1

# QUADRANT X POSTIF & Y NÉGATIF

for m in range(0, nb - 1):
    b = -m
    mon_Attache((m), (b + 1), 1, -1)
    m += 1
    b += 1


# ---------------------------------------------#
###-------------LES QUADRANTS---------------###
# ---------------------------------------------#


# -------------------------------------#
# --------------QUADRANT I-------------#
# -------------------------------------#

# Le Quadrant 1 se positionne dans le quadrant des x négatifs et y positifs
# et a une rangée supplémentaire à compter.

t = (
    _Rangée - 1
)  # t est le facteur qui replace le quadrant à l'origine (0,0,0). On le soustrait de 1 car on rajoute au quadrant un rang supplémentaire
nb = _Rangée  # nb est le nombre de rang qu'on veut au départ


def monQuadrant(
    nb, k
):  # nb étant le nombre de rang entrer au départ et k la position du rang
    # (par exemple, si on veut un quadrant de rang 4, k indique si on est au 1e, 2e, 3e ou 4e rang)
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # Rangées de modules supplémentaires en x et en y
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                (k + 1) * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # LES COULOIRS

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX)),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    # Rangées de couloirs supplémentaires en x et en y

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX)),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX)),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                (k + 1) * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                    - (dim_ModY + LCoul)
                ),
                (k + 1) * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )


# BRANCHE DE L'HÉLICE
def monHeliceCentrale(h, k):
    monModule(
        (-1) * (0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
        (LCoul + dim_ModY) / 2,
        k * (LCoul + dim_ModZ),
        dim_ModX,
        dim_ModY + LMur,
        dim_ModZ + LMur,
        monModule,
    )

    # LES COULOIRS
    monModule(
        (-1) * (0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModY) / 2),
        (LCoul + dim_ModY) / 2,
        k * (LCoul + dim_ModZ),
        LCoul + LMur,
        5 * lCoul + LMur,
        hCoul,
        monModule,
    )
    monModule(
        (-1) * (0 + h * (LCoul + dim_ModX)),
        (LCoul + dim_ModY) / 2,
        k * (LCoul + dim_ModZ),
        LCoul + LMur,
        lCoul + LMur,
        hCoul,
        monModule,
    )


# Fonction de la branche d'hélice

for n in range(0, nb - 1):
    for k in range(0, n + 1):
        monHeliceCentrale(n, k)
        n -= 1
        k += 1


# FONCTION DU QUADRANT

a = 0

for a in range(0, nb):
    monQuadrant(nb, a)
    nb -= 1  # on soustrait le nombre de rangée de 1 à chaque nouvelle boucle
    a += 1  # on ajoute à a, 1 à chaque nouvelle boucle


# -------------------------------------#
# -------------QUADRANT II-------------#
# -------------------------------------#

# Le Quadrant 2 se positionne en x et y positifs et a,
# comme le Quadrant 1, une rangée supplémentaire

t = _Rangée - 1  # t est le facteur qui repositionne le quadrant à l'origine
nb = _Rangée  # nb est le nombre de rangs qu'on veut au départ


def monQuadrant(nb, k):
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # Rangées supplémentaire en x et en y

    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                (k + 1) * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # LES COULOIRS

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                lCoul + LMur,
                LCoul + LMur,
                hCoul,
                "Module",
            )

    # Rangées de couloirs en x et en y

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t - 1) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                k * (LCoul + dim_ModZ),
                lCoul + LMur,
                LCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                (k + 1) * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2 + (LCoul + dim_ModX),
                (-1)
                * (
                    (j) * (LCoul + dim_ModY)
                    + (LCoul + dim_ModX) / 2
                    + (k - t) * (LCoul + dim_ModY)
                    - (LCoul + dim_ModY) / 2
                ),
                (k + 1) * (LCoul + dim_ModZ),
                lCoul + LMur,
                LCoul + LMur,
                hCoul,
                "Module",
            )


##HÉLICE DU QUADRANT II
def monHeliceCentrale(h, k):
    monModule(
        (LCoul + dim_ModY) / 2,
        0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2,
        k * (LCoul + dim_ModZ),
        dim_ModX + LMur,
        dim_ModY + LMur,
        dim_ModZ,
        monModule,
    )

    # LES COULOIRS
    monModule(
        (LCoul + dim_ModY) / 2,
        0 + h * (LCoul + dim_ModX),
        k * (LCoul + dim_ModZ),
        LCoul + LMur,
        lCoul + LMur,
        hCoul,
        monModule,
    )
    monModule(
        (LCoul + dim_ModY) / 2,
        0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2,
        k * (LCoul + dim_ModZ),
        5 * LCoul + LMur,
        lCoul + LMur,
        hCoul,
        monModule,
    )


# Fonction de la branche d'hélice

for n in range(0, nb - 1):
    for k in range(0, n + 1):
        monHeliceCentrale(n, k)
        n -= 1
        k += 1

# Fonction du quadrant

a = 0

for a in range(0, nb):
    monQuadrant(nb, a)
    nb -= 1


# -------------------------------------#
# -------------QUADRANT III------------#
# -------------------------------------#

# Le Quadrant 3 se positionne dans le quadrant des x négatifs et y négatifs
# et n'a pas de spécificité particulière (=> évolution normale).

t = _Rangée  # t indique le facteur qui repositionne le quadrant à l'origine)
nb = _Rangée  # nb est le nombre de rangs qu'on veut au départ


def monHelice(nb, k):  # même logique que le Quadrant 1
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j

            monModule(
                (-1) * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2)
                - (dim_ModX + LCoul),
                (j) * (LCoul + dim_ModY)
                + (k - t) * (LCoul + dim_ModY)
                + (LCoul + dim_ModY) / 2,
                k * (LCoul + dim_ModZ),
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # LES COULOIRS

    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (-1)
                * ((i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) + (dim_ModX + LCoul)),
                (j) * (LCoul + dim_ModY)
                + (k - t) * (LCoul + dim_ModY)
                + (LCoul + dim_ModY) / 2,
                k * (LCoul + dim_ModZ),
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (-1)
                * (
                    (i) * (LCoul + dim_ModX)
                    + (LCoul + dim_ModX) / 2
                    + (dim_ModX + LCoul)
                ),
                (j) * (LCoul + dim_ModY)
                + (k - t) * (LCoul + dim_ModY)
                + LCoul
                + dim_ModY,
                k * (LCoul + dim_ModZ),
                lCoul + LMur,
                LCoul + LMur,
                hCoul,
                "Module",
            )


##HÉLICE DU QUADRANT III


def monHeliceCentrale(h, k):
    monModule(
        (-1) * ((LCoul + dim_ModY) / 2),
        (-1) * (0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
        k * (LCoul + dim_ModZ),
        dim_ModX + LMur,
        dim_ModY + LMur,
        dim_ModZ,
        monModule,
    )

    # LES COULOIRS

    monModule(
        (-1) * ((LCoul + dim_ModY) / 2),
        (-1) * (0 + h * (LCoul + dim_ModX)),
        k * (LCoul + dim_ModZ),
        LCoul + LMur,
        lCoul + LMur,
        hCoul,
        monModule,
    )
    monModule(
        (-1) * ((LCoul + dim_ModY) / 2),
        (-1) * (0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2),
        k * (LCoul + dim_ModZ),
        5 * LCoul + LMur,
        lCoul + LMur,
        hCoul,
        monModule,
    )


# Fonction de la branche d'hélice

for n in range(0, nb - 1):  # Hélice
    for k in range(0, n + 1):
        monHeliceCentrale(n, k)
        n -= 1
        k += 1

# Fonction du quadrant

a = 0


for a in range(0, nb):  # même raisonnement que le Quadrant 1
    monHelice(nb, a)
    nb -= 1
    a += 1

# -------------------------------------#
# -------------QUADRANT IV-------------#
# -------------------------------------#

# Le Quadrant 4 se positionne en x positif et y négatif
# Il n'évolue que sur un étage soit z=0

t = _Rangée  # t est le facteur qui replace le quadrant à l'origine (0,0,0)
nb = _Rangée


def monQuadrant(nb, k):  # même raisonnement que le Quadrant 1
    for i in range(0, nb):
        for j in range(i, nb):  # i car droite i=j

            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2,
                (j) * (LCoul + dim_ModY)
                + (k - t) * (LCoul + dim_ModY)
                - (LCoul + dim_ModY) / 2,
                0,  # pos_Z=0
                dim_ModX + LMur,
                dim_ModY + LMur,
                dim_ModZ,
                "Module",
            )

    # LES COULOIRS
    for i in range(0, nb):
        for j in range(i + 1, nb):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX),
                (j) * (LCoul + dim_ModY)
                + (k - t) * (LCoul + dim_ModY)
                - (LCoul + dim_ModY) / 2,
                0,  # pos_Z=0
                LCoul + LMur,
                lCoul + LMur,
                hCoul,
                "Module",
            )

    for i in range(0, nb):
        for j in range(i, nb - 1):
            monModule(
                (i) * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2,
                (j) * (LCoul + dim_ModY)
                + (LCoul + dim_ModX) / 2
                + (k - t) * (LCoul + dim_ModY)
                - (LCoul + dim_ModY) / 2,
                0,  # pos_Z=0
                lCoul + LMur,
                LCoul + LMur,
                hCoul,
                "Module",
            )


##HÉLICE DU QUADRANT IV
def monHeliceCentrale(h, k):
    monModule(
        0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModX) / 2,
        (-1) * (0 + (LCoul + dim_ModY) / 2),
        0,
        dim_ModX + LMur,
        dim_ModY + LMur,
        dim_ModZ,
        monModule,
    )

    # LES COULOIRS
    monModule(
        0 + h * (LCoul + dim_ModX) + (LCoul + dim_ModY) / 2,
        (-1) * ((LCoul + dim_ModY) / 2),
        0,
        LCoul,
        5 * lCoul,
        hCoul,
        monModule,
    )  # pos_Z=0
    monModule(
        0 + h * (LCoul + dim_ModX),
        (-1) * ((LCoul + dim_ModY) / 2),
        0,
        LCoul,
        lCoul,
        hCoul,
        monModule,
    )  # pos_Z=0


# Fonction de la branche d'Hélice

for n in range(0, nb - 1):
    for k in range(0, n + 1):
        monHeliceCentrale(n, k)
        n -= 1
        k += 1


# Fonction du quadrant


for a in range(0, nb):
    monQuadrant(nb, a)
    nb -= 1
    a += 1
