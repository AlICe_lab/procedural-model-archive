# SPDX-FileCopyrightText: 2016 Juliette Fuhs
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
from bpy import context

import random
from random import *

import math
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

############################
# définition des formes de base de la modélisation, tétraèdres copier/coller et cube aléatoire


def CopyPaste(Forme1, Tetra1):

    bpy.context.scene.objects.active = bpy.data.objects[Forme1]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(Tetra1, obj.data)
    context.scene.objects.link(Copy)

    return Copy


def Cube(position, dimension):
    bpy.ops.mesh.primitive_cube_add(radius=1, location=position)
    bpy.ops.transform.resize(value=dimension)


dimensionx = [1, 2, 3, 4, 5, 6, 7, 8]
dimensiony = [1, 2, 3, 4, 5, 6, 7, 8]
dimensionz = [1, 2, 3, 4, 5, 6]

Cube((0, 0, 9), (choice(dimensionx), choice(dimensiony), choice(dimensionz)))

###########################
# écriture des coordonnées extrêmes du cube aléatoire

LOC = bpy.data.objects["Cube"].location
SC = bpy.data.objects["Cube"].scale


Xdebut = LOC[0] - SC[0] - 1
Xfin = LOC[0] + SC[0] + 1
Ydebut = LOC[1] - SC[1] - 1
Yfin = LOC[1] + SC[1] + 1
Zdebut = LOC[2] - SC[2] - 1
Zfin = LOC[2] + SC[2]

print(LOC[0], SC[0], Xdebut, Xfin)
coordonnees = []

########################
# définition des 3 points de coordonnées des tétraèdres


def Tetra(location, forme):

    x, y, z = location

    if forme != "Forme2":
        P1 = [0.98476, -0.58355, 1.63288]
        P2 = [0, 1.15483, 1.63288]
        P3 = [-0.98476, -0.58355, 1.63288]

    tetra = CopyPaste(forme, "Tetra")
    tetra.location = location

    ######################
    # création de points de coordonnées selon des intervalles d'évitement du cube et d'éloignement maximum : détection de collision

    if not (x + P1[0], y + P1[1], z + P1[2]) in coordonnees:
        if (
            not Xdebut < x + P1[0] < Xfin
            or not Ydebut < y + P1[1] < Yfin
            or not Zdebut < z + P1[2] < Zfin
        ):
            if (Xdebut - 4) < x + P1[0] < (Xfin + 4) and (Ydebut - 4) < y + P1[1] < (
                Yfin + 4
            ):
                coordonnees.append((x + P1[0], y + P1[1], z + P1[2]))
    if not (x + P2[0], y + P2[1], z + P2[2]) in coordonnees:
        if (
            not Xdebut < x + P2[0] < Xfin
            or not Ydebut < y + P2[1] < Yfin
            or not Zdebut < z + P2[2] < Zfin
        ):
            if (Xdebut - 4) < x + P2[0] < (Xfin + 4) and (Ydebut - 4) < y + P2[1] < (
                Yfin + 4
            ):
                coordonnees.append((x + P2[0], y + P2[1], z + P2[2]))
    if not (x + P3[0], y + P3[1], z + P3[2]) in coordonnees:
        if (
            not Xdebut < x + P3[0] < Xfin
            or not Ydebut < y + P3[1] < Yfin
            or not Zdebut < z + P3[2] < Zfin
        ):
            if (Xdebut - 4) < x + P3[0] < (Xfin + 4) and (Ydebut - 4) < y + P3[1] < (
                Yfin + 4
            ):
                coordonnees.append((x + P3[0], y + P3[1], z + P3[2]))


tr1 = []
tr2 = []

for x in range(int(LOC[0]) - int(SC[0]) - 3, int(LOC[0]) + int(SC[0]) + 4):
    for y in range(int(LOC[1]) - int(SC[1]) - 3, int(LOC[1]) + int(SC[1]) + 4):
        tr1.append((x, y, 0))
for x in range(int(LOC[0]) - int(SC[0]), int(LOC[0]) + int(SC[0])):
    for y in range(int(LOC[1] - SC[1]), int(LOC[1] + SC[1])):
        tr2.append((x, y, 0))

trame = list(set(tr1) - set(tr2))

##################
# génération des tétraèdres pour les points de coordonnées donnés


for patte in range(5):
    coordonnees = []
    location = choice(trame)
    Tetra(location, "Forme1")
    for location in coordonnees:
        x, y, z = location

        #################
        # génération de différentes formes de tétraèdres selon la hauteur

        if LOC[2] - SC[2] - 1 < z < LOC[2] + SC[2]:
            forme = "Forme3"
        elif z > LOC[2] + SC[2]:
            forme = "Forme4"
        else:
            forme = "Forme1"
        if z > LOC[2] + SC[2] + 2:
            break
        Tetra(location, forme)
