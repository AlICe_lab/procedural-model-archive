# SPDX-FileCopyrightText: 2018 Alice Leclercq & Marie Willame
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Alice Leclercq - Marie Willame
# AIM - Module 1
# Variation compositionnelle par modèle procédural
# Diamond Houses - John Hejduk
# Préjury - 20 décembre 2018
# macOS 10.13.4 / blender 2.79 hash 5bd8ac9
# project

## BASE ##
import bpy
import random
import bmesh
import tools
import imp

imp.reload(tools)

bpy.ops.object.mode_set(mode="OBJECT")
bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["escalierColimaçon"].select = False
bpy.ops.object.delete(use_global=False)
# tools.delete_all()

"""
#dans la console
import imp
import project
imp.reload(project)
"""

"""
## DÉVELOPPEMENT PAR ÉTAPES ##
#sol
tools.add_sol((0,0,0), (10,10,.2), .785398, (0,0,1), "étage_0")
#périmètre
tools.add_perimetre()
#préparation boolean
tools.exclude_center()
tools.exclude_perimetre()
#grille base
tools.add_baseGrid()
#grille artiste
#tools.add_gridCW((1,1,1))
#tools.add_gridATP((1,1,1))
#tools.add_gridLPS((1,1,1))
#tools.add_gridTVD((1,1,1))
#cloisons+colonnes ou murs porteurs
tools.choice_structure()
tools.boolean("center")
#brise-soleil
tools.add_randomBS((1,1,1))
"""

## BOUCLE COMPLÈTE ##
# GÉNÉRER DIFFÉRENTES ÉCHELLES POUR LES DIFFÉRENTS NIVEAUX


def add_etage(dimEsc, dimGrand, value):
    tools.add_sol((0, 0, 0), (10, 10, 0.2), 0.785398, (0, 0, 1), "étage_0")
    tools.add_perimetre()
    tools.exclude_center()
    tools.exclude_perimetre()
    tools.add_baseGrid()
    tools.choice_structure()
    tools.boolean("center")
    tools.add_randomBS((1, 1, 1))
    bpy.ops.object.select_all(action="DESELECT")
    tools.select_object("center")
    tools.select_object("périmètre")
    bpy.ops.object.delete(use_global=False)
    tools.select_object("escalierColimaçon.001")
    tools.select_object("escalierDroit")
    tools.scale_object(dimEsc)
    tools.select_object("brise-soleil")
    tools.select_object("grilleBase")
    tools.select_object("étage_0")
    tools.select_object("choixStructure")
    bpy.data.objects["escalierColimaçon"].select = False
    tools.join_object()
    tools.rename_object("étage")
    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")
    tools.scale_object(dimGrand)
    tools.translate_object(value)


# liste des différentes échelles à utiliser (1/200, 1/100, 1/50 et 1/100 -> hauteur totale = 10)
echelle = [0.5, 1, 2, 1]
# liste pour mettre les niveaux à la bonne hauteur suivant l'échelle du niveau qui leur est inférieur
vEchelles = []
# boucle pour générer les niveaux
for h in range(0, 4):
    s = random.choice(echelle)
    echelle.remove(s)
    vEchelles.append(s)
    add_etage((s, s, 1), (1, 1, s), (0, 0, 0))

# hauteurs à prévoir selon l'échelle du niveau inférieur
h1 = vEchelles[0]
h2 = vEchelles[1]
h3 = vEchelles[2]
print(vEchelles)
print(h1, h2, h3)

# translation à la bonne hauteur
bpy.data.objects["étage.001"].location[2] = 2.25 * h1
bpy.data.objects["étage.002"].location[2] = 2.25 * (h1 + h2)
bpy.data.objects["étage.003"].location[2] = 2.25 * (h1 + h2 + h3)
