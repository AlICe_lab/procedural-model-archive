# SPDX-FileCopyrightText: 2014 Nikita Itenberg
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER 2.71 r9337574 / MS windows64 bit

import bpy
import random
import math
import os
import mathutils
import bmesh

########################################################################################################################

bpy.ops.object.select_all(action="SELECT")  # Nettoyage
bpy.ops.object.delete(use_global=False)

bpy.context.scene.cursor_location = mathutils.Vector(
    (0.0, 0.0, 0.0)
)  # Curseur à l'origine

########################################################################################################################
# GENERATEURS D OBJETS #
########################################################################################################################


def boite(x=0, y=0, z=0, dimX=1, dimY=1, dimZ=1):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z), radius=0.5)
    bpy.ops.transform.resize(value=(dimX, dimY, dimZ))


###


def cylindre(detail=100, x=0, y=0, z=0, dimX=1, dimY=1, dimZ=1):
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=detail, radius=0.5, depth=2, end_fill_type="NGON", location=(x, y, z)
    )
    bpy.ops.transform.resize(value=(dimX, dimY, dimZ))


###


def escalierY(
    OrigineX=0.0, OrigineY=0.0, OrigineZ=0.0, HTot=1.2, LTot=2.0, LargeurMarche=0.8
):

    # Generateur d'escalier a dimensions variables

    NbMarches = int(HTot * 10) // 2
    HMarche = HTot / NbMarches
    ProfMarche = LTot / NbMarches

    # Creation de la premiere marche (plate)
    boite(
        OrigineX,
        OrigineY + ProfMarche / 2,
        (OrigineZ + HMarche / 2),
        LargeurMarche,
        ProfMarche,
        HMarche,
    )
    name("Marche")

    # Création de la marche initiale trapezoidale
    # Les points sont des tuples de 3 flottants
    coords = [
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ + HMarche * 2),
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche, OrigineZ + HMarche * 2),
        (
            OrigineX - LargeurMarche / 2,
            OrigineY + ProfMarche * 2,
            OrigineZ + HMarche * 2,
        ),
        (
            OrigineX + LargeurMarche / 2,
            OrigineY + ProfMarche * 2,
            OrigineZ + HMarche * 2,
        ),
        (OrigineX + LargeurMarche / 2, OrigineY + ProfMarche * 2, OrigineZ + HMarche),
        (OrigineX - LargeurMarche / 2, OrigineY + ProfMarche * 2, OrigineZ + HMarche),
    ]

    # Faces définies par des tuples définissant les index des points (en partant de 0)

    faces = [
        (0, 1, 2, 3),
        (2, 5, 4, 3),
        (4, 5, 6, 7),
        (7, 6, 1, 0),
        (0, 3, 4, 7),
        (1, 2, 5, 6),
    ]

    me = bpy.data.meshes.new("MarcheTMesh")  # creation d'un nouveau mesh
    ob = bpy.data.objects.new("MarcheT", me)
    ob.location = bpy.context.scene.cursor_location
    bpy.context.scene.objects.link(ob)

    me.from_pydata(coords, [], faces)
    me.update(calc_edges=True)

    # Generation de l'escalier
    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["MarcheT"].select = True
    bpy.context.scene.objects.active = bpy.data.objects["MarcheT"]
    for i in range(0, NbMarches - 2):
        dupliquer(0, ProfMarche, HMarche)
    bpy.ops.object.select_pattern(pattern="Marche*")
    bpy.context.scene.objects.active = bpy.data.objects["Marche"]
    bpy.ops.object.join()


########################################################################################################################
# FONCTIONS #
########################################################################################################################


def effacer(obj):

    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects[obj].select = True
    bpy.ops.object.delete(use_global=False)


###


def joindre(i, j):

    bpy.context.scene.objects[j].select = True
    bpy.context.scene.objects[i].select = True
    bpy.context.scene.objects.active = bpy.data.objects[i]
    bpy.ops.object.join()


###


def joindreMulti(objName):

    bpy.ops.object.select_pattern(pattern=str(objName) + "*")
    bpy.context.scene.objects.active = bpy.data.objects[objName]
    bpy.ops.object.join()


###


def soustraction(i, j):

    bpy.context.scene.objects.active = bpy.data.objects[i]
    # if i is not 'Boolean':
    if True:
        bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[j]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects[j]
    bpy.ops.object.delete(use_global=False)


###


def union(i):

    bpy.context.scene.objects.active = bpy.data.objects[i]
    if i is not BOOLEAN:
        bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[j]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")


###


def dupliquer(x=0, y=0, z=0):

    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": (x, y, z)},
    )


###


def name(obj):

    bpy.context.object.name = obj
    bpy.context.object.data.name = obj


###


def place_obj(obj, x=0, y=0, z=0):

    # obj est de la forme 'obj'
    bpy.data.objects[obj].location = (x, y, z)


###


def rotation(obj):

    bpy.context.scene.objects["obj"].select = True
    bpy.ops.transform.rotate(value=math.pi / 4, axis=(0, 0, 1))


###


def rien(D):
    D = D


########################################################################################################################
# VARIABLES #
########################################################################################################################

CoteCube = 9
SubDivTrame = 3
EpMur = 0.2
NbEtages = 3

HCubeH = CoteCube + CoteCube / 3
CoteCubeH = CoteCube + CoteCube / 5

Dico = {
    "CoteX": CoteCube,
    "CoteY": CoteCube,
    "CoteZ": CoteCube,
    "Trame": SubDivTrame,
    "Etage": NbEtages,
    "Old": CoteCube,
    "AxeFort": 2,
    "EpMur": EpMur,
}


########################################################################################################################
# AXIOME #
########################################################################################################################


def axiome(D):

    boite(0, 0, 0, D["CoteX"], D["CoteY"], D["CoteZ"])
    name("Cube_Base")
    bpy.ops.transform.translate(
        value=((D["CoteX"] / 2), (D["CoteY"] / 2), (D["CoteZ"] / 2))
    )


# axiome(Dico)

########################################################################################################################
# GRAVES #
########################################################################################################################


def extEscGR(D):

    boite(
        D["CoteX"] - D["CoteX"] / (D["Trame"] * 3),
        D["CoteY"] - D["CoteY"] / (D["Trame"] * 3),
        3 / 2 * D["CoteZ"] / D["Etage"],
        D["CoteX"] / D["Trame"] + D["CoteX"] / (D["Trame"] * 3),
        D["CoteY"] / D["Trame"] + D["CoteY"] / (D["Trame"] * 3),
        D["CoteZ"] / D["Etage"],
    )
    name("ExtEscGR")
    boite(0, 0, 0, D["CoteX"] * 2, D["CoteY"], D["CoteZ"])
    name("Tmp")
    bpy.ops.transform.translate(
        value=((D["CoteX"] / 2), (D["CoteY"] / 2), (D["CoteZ"] / 2))
    )
    soustraction("ExtEscGR", "Tmp")

    # Les points sont des tuples de 3 flottants
    coords = [
        (
            D["CoteX"] + D["CoteX"] / (D["Trame"] * 3),
            D["CoteY"],
            2 / D["Etage"] * D["CoteZ"],
        ),
        (D["CoteX"], D["CoteY"], 2 / D["Etage"] * D["CoteZ"]),
        (D["CoteX"], D["CoteY"], 1 / D["Etage"] * D["CoteZ"]),
        (
            D["CoteX"] + D["CoteX"] / (D["Trame"] * 3),
            D["CoteY"],
            1 / D["Etage"] * D["CoteZ"],
        ),
        (D["CoteX"] + D["CoteX"] / (D["Trame"] * 3), 2 / 3 * D["CoteY"], 0),
        (D["CoteX"], 2 / 3 * D["CoteY"], 0),
        (D["CoteX"], 2 / 3 * D["CoteY"], 1 / D["Etage"] * D["CoteZ"]),
        (
            D["CoteX"] + D["CoteY"] / (D["Trame"] * 3),
            2 / 3 * D["CoteY"],
            1 / D["Etage"] * D["CoteZ"],
        ),
    ]

    # Faces définies par des tuples définissant les index des points (en partant de 0)

    faces = [
        (0, 1, 2, 3),
        (1, 2, 5, 6),
        (2, 3, 4, 5),
        (4, 7, 6, 5),
        (4, 7, 0, 3),
        (0, 1, 6, 7),
    ]

    me = bpy.data.meshes.new("GardeCorpsEscGRMesh")  # creation d'un nouveau mesh
    ob = bpy.data.objects.new("GardeCorpsEscGR", me)
    ob.location = bpy.context.scene.cursor_location
    bpy.context.scene.objects.link(ob)

    me.from_pydata(coords, [], faces)
    me.update(calc_edges=True)

    joindre("ExtEscGR", "GardeCorpsEscGR")

    #!!! Joindre correctement, et creuser les gardes-corps et l'escalier


########################################################################################################################


def extAvantGR(D):

    # Passerelle
    boite(
        D["CoteX"] / 2 + 1 / (D["Trame"] * 4) * D["CoteX"],
        -1 / D["Trame"] * D["CoteY"],
        3 / 2 * D["CoteZ"] / D["Etage"],
        1 / (D["Trame"] * 2) * D["CoteX"],
        2 / D["Trame"] * D["CoteY"],
        D["CoteZ"] / D["Etage"],
    )
    name("PasserelleGR")

    dupliquer(0, 0, D["EpMur"] + 1.2)
    name("T")
    soustraction("PasserelleGR", "T")

    # Bloc Avant
    boite(
        3 / 4 * D["CoteX"],
        -2 / D["Trame"] * D["CoteY"] - 1 / (D["Trame"] * 2) * D["CoteY"],
        (1 + D["Etage"]) / 2 * D["CoteZ"] / D["Etage"],
        D["CoteX"] / 2,
        D["CoteY"] / D["Trame"],
        (D["Etage"] - 1) * D["CoteZ"] / D["Etage"],
    )
    name("extAvantGR")

    joindre("extAvantGR", "PasserelleGR")

    # Colonnes
    cylindre(
        20,
        D["EpMur"] / 2 + D["CoteX"] / 2,
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    cylindre(
        20,
        D["CoteX"] / 2 + 1 / 2 / D["Trame"] * D["CoteX"],
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    cylindre(
        20,
        D["CoteX"] - D["EpMur"] / 2,
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    cylindre(
        20,
        D["EpMur"] / 2 + D["CoteX"] / 2,
        -2 / D["Trame"] * D["CoteY"] - D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    cylindre(
        20,
        D["CoteX"] / 2 + 1 / 2 / D["Trame"] * D["CoteX"],
        -2 / D["Trame"] * D["CoteY"] - D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    cylindre(
        20,
        D["CoteX"] - D["EpMur"] / 2,
        -2 / D["Trame"] * D["CoteY"] - D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColBlocGR")

    joindreMulti("ColBlocGR")

    joindre("extAvantGR", "ColBlocGR")

    # Extrusion de la circulation

    boite(
        D["CoteX"] / 2 + 1 / (D["Trame"] * 4) * D["CoteX"],
        -1 / D["Trame"] * D["CoteY"],
        3 / 2 * D["CoteZ"] / D["Etage"] + D["EpMur"],
        1 / (D["Trame"] * 2) * D["CoteX"] - D["EpMur"] * 2,
        5 * D["CoteY"],
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Circu")
    soustraction("extAvantGR", "Circu")


########################################################################################################################


def escAvantGR(D):

    escalierY(
        D["CoteX"] / 2 + 1 / (D["Trame"] * 4) * D["CoteX"],
        -5 * D["CoteY"] / D["Trame"],
        0,
        D["CoteZ"] / D["Etage"] + D["EpMur"],
        2 / D["Trame"] * D["CoteY"],
        1 / (D["Trame"] * 2) * D["CoteX"] - (2 * D["EpMur"]),
    )
    name("escalierAvantGR")


########################################################################################################################


def extFausseFacadeGR(D):

    boite(
        D["CoteX"] / 4,
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] - D["CoteZ"] / D["Etage"] / 2,
        D["CoteX"] / 2,
        D["EpMur"],
        D["CoteZ"] / D["Etage"],
    )
    name("FausseFacadeGR")

    boite(
        D["CoteX"] / 4,
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] - (D["CoteZ"] / D["Etage"]) / 3,
        D["CoteX"] / 2,
        5 * D["EpMur"],
        D["CoteZ"] / D["Etage"] / 3,
    )
    name("Bandeau")
    soustraction("FausseFacadeGR", "Bandeau")

    # Colonnes
    cylindre(
        20,
        D["EpMur"] / 2,
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / 2,
    )

    name("ColFacade")
    joindre("FausseFacadeGR", "ColFacade")

    cylindre(
        20,
        D["CoteX"] / D["Trame"],
        -3 / D["Trame"] * D["CoteY"] + D["EpMur"] / 2,
        (D["CoteZ"] - D["CoteZ"] / D["Etage"]) / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        (D["CoteZ"] - D["CoteZ"] / D["Etage"]) / 2,
    )

    name("ColFacade2")
    joindre("FausseFacadeGR", "ColFacade2")


########################################################################################################################


def mursMain(D):

    boite(
        D["CoteX"] / D["Trame"],
        2 / D["Trame"] * D["CoteY"] - 1 / (D["Trame"] * 3) * D["CoteY"],
        D["CoteZ"] / 2,
        2 / D["Trame"] * D["CoteX"],
        D["EpMur"],
        D["CoteZ"],
    )
    name("MurXGR")
    boite(
        D["CoteX"] * 2 / D["Trame"] + D["EpMur"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("MurYGR")


########################################################################################################################


def soustractionFaceGR(D):

    boite(
        1 / (D["Trame"] * 6) * D["CoteX"],
        (2 / D["Trame"] * D["CoteY"] - 1 / (D["Trame"] * 3) * D["CoteY"]) / 2
        - D["EpMur"] / 2,
        0,
        1 / (D["Trame"] * 3) * D["CoteX"],
        2 / D["Trame"] * D["CoteY"] - 1 / (D["Trame"] * 3) * D["CoteY"],
        2 * (D["CoteZ"] - D["CoteZ"] / D["Etage"] - EpMur),
    )

    name("TmpB")
    soustraction("Cube_Base", "TmpB")

    boite(
        1 / (D["Trame"] * 6) * D["CoteX"],
        1 / D["Trame"] * D["CoteY"] + 1 / (D["Trame"] * 3) * D["CoteY"],
        D["CoteZ"] / 2,
        1 / (D["Trame"] * 3) * D["CoteX"],
        2 / (D["Trame"] * 3) * D["CoteY"],
        D["CoteZ"] - D["EpMur"] * 2,
    )

    name("Tmp")
    soustraction("Cube_Base", "Tmp")

    cylindre(
        20,
        D["EpMur"] / 2,
        D["EpMur"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / 2,
    )
    name("ColFacadeGR")
    cylindre(
        20,
        D["EpMur"] / 2,
        1 / D["Trame"] * D["CoteY"] - D["EpMur"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / 2,
    )
    name("ColFacadeGR")
    joindreMulti("ColFacadeGR")


########################################################################################################################


def soustractionToitGR(D):

    # Cylindre
    cylindre(
        200,
        2 / D["Trame"] * D["CoteX"],
        -D["CoteY"] / 2,
        D["CoteZ"],
        D["CoteX"] + 2 / D["Trame"] * D["CoteX"],
        D["CoteX"] + 2 / D["Trame"] * D["CoteY"],
        D["CoteZ"] / D["Etage"] - D["EpMur"],
    )

    name("Cyl1")

    # Raccourcir a la facade
    boite(
        2 / D["Trame"] * D["CoteX"],
        -D["CoteY"],
        D["CoteZ"],
        5 * D["CoteX"],
        2 * D["CoteY"] + D["EpMur"] * 2,
        D["CoteZ"] / D["Etage"] * 2 - D["EpMur"],
    )

    name("Tmp2")
    soustraction("Cyl1", "Tmp2")

    # Limiter à un quartier
    boite(
        D["CoteX"],
        0,
        0,
        (D["CoteX"] - (D["CoteX"] * 2 / D["Trame"] + D["EpMur"] / 2)) * 2,
        D["CoteY"],
        4 * D["CoteZ"],
    )
    name("TmpC")
    soustraction("Cyl1", "TmpC")

    # Extruder la volumetrie
    bpy.context.scene.objects.active = bpy.data.objects["Cyl1"]
    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["Cyl1"].select = True
    bpy.ops.transform.resize(value=(1, 1, 1 / 2))
    bpy.ops.transform.translate(
        value=(0, 0, -(D["CoteZ"] / D["Etage"] - D["EpMur"]) / 2)
    )
    soustraction("Cube_Base", "Cyl1")

    # Troncon droit
    boite(
        1 / D["Trame"] * D["CoteX"],
        1 / (D["Trame"] * 4) * D["CoteY"] + D["EpMur"],
        D["CoteZ"],
        2 / D["Trame"] * D["CoteX"],
        1 / 2 / D["Trame"] * D["CoteY"] - D["EpMur"] * 2,
        2 * D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )

    name("Tmp2")
    soustraction("Cube_Base", "Tmp2")

    # Percee bandeau

    boite(
        D["CoteX"] / 4,
        0,
        D["CoteZ"] - (D["CoteZ"] / D["Etage"]) / 3,
        D["CoteX"] / 2,
        5 * D["EpMur"],
        D["CoteZ"] / D["Etage"] / 3,
    )
    name("Bandeau")
    soustraction("Cube_Base", "Bandeau")


########################################################################################################################


def extToitGR(D):

    boite(
        D["CoteX"] - D["CoteX"] / D["Trame"],
        D["CoteY"] - 1 / 2 / D["Trame"] * D["CoteY"],
        D["CoteZ"] + D["EpMur"] * 2,
        D["EpMur"] * 2,
        D["EpMur"] * 2,
        D["EpMur"] * 4,
    )
    name("ChemineeGR")

    cylindre(
        20,
        D["CoteX"] - 1 / 2 * D["CoteX"] / D["Trame"],
        D["CoteY"] - 1 / 2 / D["Trame"] * D["CoteY"],
        D["CoteZ"] + D["EpMur"] / 2,
        D["EpMur"] * 5,
        D["EpMur"] * 5,
        D["EpMur"] / 2,
    )
    name("ZenithalGR")


########################################################################################################################


def graves(D):

    axiome(D)
    extEscGR(D)
    extAvantGR(D)
    escAvantGR(D)
    extFausseFacadeGR(D)
    mursMain(D)
    soustractionFaceGR(D)
    soustractionToitGR(D)
    extToitGR(D)


########################################################################################################################
# MEIER #
########################################################################################################################


def newDicoM(D):

    D["CoteX"] = D["CoteX"] + D["CoteX"] / D["Trame"]
    D["CoteY"] = D["CoteY"] + 2 / (D["Trame"] * 3) * D["CoteY"]
    return D


########################################################################################################################


def extVolM(D):

    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["Cube_Base"].select = True

    bpy.ops.object.delete(use_global=False)
    axiome(D)


########################################################################################################################


def mursMainM(D):

    boite(
        D["CoteX"] / 2,
        (D["CoteY"] - D["Old"]) / 2 + D["Old"] * 2 / D["Trame"],
        D["CoteZ"] / 2,
        D["CoteX"],
        D["EpMur"],
        D["CoteZ"],
    )
    name("MurXM")
    boite(
        D["CoteX"],
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        (D["CoteX"] - D["Old"]) + 2 * D["Old"] / D["Trame"] + D["EpMur"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("Tmp")
    soustraction("MurXM", "Tmp")

    boite(
        (D["CoteX"] - D["Old"]) / 2 + D["Old"] * 2 / D["Trame"],
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("MurYM")
    boite(
        D["CoteX"] / 2,
        D["CoteY"],
        D["CoteZ"] / 2,
        D["CoteX"],
        (D["CoteX"] - D["Old"]) * 2 / 3 + 2 * D["Old"] / D["Trame"] - D["EpMur"],
        D["CoteZ"],
    )
    name("Tmp")
    soustraction("MurYM", "Tmp")


########################################################################################################################


def extEscM(D):

    boite(
        -D["Old"] / (D["Trame"] * D["Trame"]),
        D["CoteY"] - (D["CoteY"] - D["Old"]) / 4 - D["Old"] / D["Trame"] / 2,
        D["CoteZ"] / D["Etage"] * 3 / 2,
        2 * D["Old"] / (D["Trame"] * D["Trame"]),
        (D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"],
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp9")

    dupliquer(0, 0, 1.2 + D["EpMur"])
    soustraction("Tmp9", "Tmp9.001")
    bpy.context.scene.objects["Tmp9"].select = True

    dupliquer(D["EpMur"], D["EpMur"], D["EpMur"])
    soustraction("Tmp9", "Tmp9.001")

    cylindre(
        20,
        0,
        D["CoteY"],
        D["CoteZ"] / D["Etage"] * 3 / 2,
        D["Old"] * 4 / (D["Trame"] * D["Trame"]),
        D["Old"] * 4 / (D["Trame"] * D["Trame"]),
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ExtEscM")

    boite(
        -D["Old"] / (D["Trame"] * 6),
        D["CoteY"] - (D["CoteY"] - D["Old"]) / 4 - D["Old"] / (2 * D["Trame"]),
        D["CoteZ"] / 2,
        D["Old"],
        (D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"],
        D["CoteZ"],
    )

    name("Tmp2")
    soustraction("ExtEscM", "Tmp2")
    joindre("ExtEscM", "Tmp9")


########################################################################################################################


def extAvantM(D):

    # Passerelle
    boite(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -3 / D["Trame"] * D["Old"] / 2,
        D["CoteZ"] / D["Etage"] + 0.6,
        1 / (D["Trame"] * D["Trame"]) * D["Old"],
        3 / D["Trame"] * D["Old"],
        1.2,
    )
    name("PasserelleM")

    boite(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -3 / D["Trame"] * D["Old"] / 2,
        D["CoteZ"] / D["Etage"] + D["EpMur"] + 0.6,
        1 / (D["Trame"] * D["Trame"]) * D["Old"] - D["EpMur"],
        3 / D["Trame"] * D["Old"],
        1.2,
    )
    name("Tmp")
    soustraction("PasserelleM", "Tmp")

    # Colonnes
    cylindre(
        20,
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -D["Old"] / D["Trame"],
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColPasserelleM")
    joindre("PasserelleM", "ColPasserelleM")

    cylindre(
        20,
        D["CoteX"] - (1 / (D["Trame"] * 3) * D["Old"]) / 2,
        -2 * D["Old"] / D["Trame"],
        D["CoteZ"] / D["Etage"] / 2,
        D["EpMur"] * 2 / 3,
        D["EpMur"] * 2 / 3,
        D["CoteZ"] / D["Etage"] / 2,
    )
    name("ColPasserelleM")
    joindre("PasserelleM", "ColPasserelleM")

    # Volume avant
    boite(
        D["CoteX"] - (1 / D["Trame"] * D["Old"]) / 2,
        -4 / D["Trame"] * D["Old"],
        D["CoteZ"] / D["Etage"],
        D["Old"] / D["Trame"],
        2 * D["Old"] / D["Trame"],
        2 * D["CoteZ"] / D["Etage"],
    )
    name("ExtAvantM")

    boite(
        D["CoteX"] - (1 / D["Trame"] * D["Old"]) / 2,
        -29 / 6 / D["Trame"] * D["Old"],
        D["CoteZ"] / D["Etage"] / 2,
        D["Old"] / D["Trame"],
        D["Old"] / D["Trame"] / 3,
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp3")
    soustraction("ExtAvantM", "Tmp3")

    boite(
        D["CoteX"] - (1 / D["Trame"] * D["Old"]) / 2,
        -20 / 6 / D["Trame"] * D["Old"] - D["EpMur"],
        D["CoteZ"] / D["Etage"] / 2,
        D["Old"] / D["Trame"],
        2 * D["Old"] / D["Trame"] / 3 - D["EpMur"] * 2,
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp4")
    soustraction("ExtAvantM", "Tmp4")

    boite(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -3 / D["Trame"] * D["Old"] / 2,
        3 / 2 * D["CoteZ"] / D["Etage"],
        1 / (D["Trame"] * D["Trame"]) * D["Old"] - D["EpMur"],
        12 / D["Trame"] * D["Old"],
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Tmp5")
    soustraction("ExtAvantM", "Tmp5")

    boite(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -28 / 6 / D["Trame"] * D["Old"],
        D["CoteZ"] / 2,
        1 / (D["Trame"] * D["Trame"]) * D["Old"],
        2 / D["Trame"] * D["Old"],
        D["CoteZ"],
    )
    name("Tmp")
    soustraction("ExtAvantM", "Tmp")

    # Escalier avant

    escalierY(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -28 / 6 / D["Trame"] * D["Old"],
        0,
        D["CoteZ"] / D["Etage"] + D["EpMur"],
        1 / D["Trame"] * D["Old"],
        1 / (D["Trame"] * D["Trame"]) * D["Old"],
    )
    name("escalierM")
    joindre("ExtAvantM", "escalierM")

    joindre("ExtAvantM", "PasserelleM")

    # Creusement entree

    boite(
        D["CoteX"] - (1 / (D["Trame"] * D["Trame"]) * D["Old"]) / 2,
        -3 / D["Trame"] * D["Old"] / 2,
        3 / 2 * D["CoteZ"] / D["Etage"] + D["EpMur"] / 2,
        1 / (D["Trame"] * D["Trame"]) * D["Old"] - D["EpMur"],
        4 / D["Trame"] * D["Old"] + D["EpMur"],
        D["CoteZ"] / D["Etage"] - D["EpMur"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def supprArrondiM(D):

    boite(
        0,
        0,
        D["CoteZ"] / 2,
        math.sqrt(2 * (((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]) ** 2)),
        math.sqrt(2 * (((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]) ** 2)),
        D["CoteZ"],
    )
    name("Tmp2")
    bpy.ops.transform.rotate(value=math.pi / 4, axis=(0, 0, 1))
    cylindre(
        150,
        (D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"],
        (D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"],
        D["CoteZ"] / 2,
        2 * ((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]),
        2 * ((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]),
        D["CoteZ"] / 2,
    )
    name("Tmp")

    soustraction("Tmp2", "Tmp")
    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["Tmp2"].select = True
    soustraction("Cube_Base", "Tmp2")


########################################################################################################################


def supprVolRdcM(D):

    boite(
        0,
        0,
        D["CoteZ"] / 2 / D["Etage"],
        2 * ((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]),
        2 * ((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]),
        D["Old"] / D["Etage"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")

    boite(
        (D["CoteY"] - D["Old"]) + D["Old"] / D["Trame"],
        0,
        D["CoteZ"] / 2 / D["Etage"],
        2 * ((D["CoteY"] - D["Old"]) / 2 + D["Old"] / D["Trame"]),
        D["Old"] / D["Trame"] / D["Trame"] * 2,
        D["Old"] / D["Etage"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def extChemineeM(D):

    cylindre(
        15,
        D["CoteX"] / D["Trame"],
        D["CoteY"] / D["Trame"],
        (D["CoteZ"] + D["Etage"] / 2) / 2,
        D["EpMur"],
        D["EpMur"],
        (D["CoteZ"] + D["Etage"] / 2) / 2,
    )
    name("ChemineeM")
    cylindre(
        15,
        D["CoteX"] / D["Trame"],
        D["CoteY"] / D["Trame"],
        (D["CoteZ"] + D["Etage"] / 2) / 2,
        D["EpMur"],
        D["EpMur"],
        (D["CoteZ"] + D["Etage"] / 2) / 4,
    )
    name("Tmp")
    soustraction("ChemineeM", "Tmp")


########################################################################################################################


def soustractionToitureM(D):

    boite(
        (D["CoteX"] - D["Old"]) / 2 + D["Old"] / D["Trame"] * 3 / 2,
        D["CoteY"] - D["Old"] / D["Trame"] / 2,
        0,
        D["Old"] / D["Trame"],
        D["Old"] / D["Trame"],
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp")
    bpy.ops.transform.translate(value=(0, 0, D["CoteZ"] / D["Etage"] * 5 / 2))
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def meier(D):

    axiome(D)
    D = newDicoM(D)
    extVolM(D)
    mursMainM(D)
    extEscM(D)
    extAvantM(D)
    supprArrondiM(D)
    supprVolRdcM(D)
    soustractionToitureM(D)
    extChemineeM(D)


########################################################################################################################
# GWATHMEY #
########################################################################################################################


def AxeFortGW(D):

    D["AxeFort"] = int(D["Trame"] / 2) + 1
    return D


########################################################################################################################


def mursMainGW(D):

    boite(
        D["CoteX"] / 2,
        (((D["AxeFort"] - 1) / D["Trame"]) * D["CoteY"]),
        D["CoteZ"] / 2,
        D["CoteX"],
        D["EpMur"],
        D["CoteZ"],
    )
    name("MurX")
    boite(
        (((D["AxeFort"] - 1) / D["Trame"]) * D["CoteX"]),
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("MurY")


########################################################################################################################


def extTourelleGW(D):

    cylindre(
        detail=30,
        y=(((D["AxeFort"] - 1) / D["Trame"]) * D["CoteY"]),
        x=D["CoteX"],
        z=D["CoteZ"] / 2,
        dimX=((2 / D["Trame"]) * D["CoteX"]),
        dimY=((2 / D["Trame"]) * D["CoteY"]),
        dimZ=D["CoteZ"] / 2,
    )
    name("Demicylindre")
    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["CoteX"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("Tmp")
    soustraction("Demicylindre", "Tmp")


########################################################################################################################


def soustrVolGW(D):

    boite(
        D["CoteX"] / (D["Trame"] * 2),
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["CoteX"] / D["Trame"],
        D["CoteY"],
        D["CoteZ"],
    )
    name("Tmp")
    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def extEscMaineGW(D):

    # boite(D["CoteX"]/(D["Trame"]*2), D["CoteY"]/2, D["CoteZ"]/2, D["CoteX"]/D["Trame"], D["CoteY"], D["CoteZ"])
    # name("Tmp1")
    # bpy.context.scene.objects.active = bpy.data.objects["MurX"]
    # soustraction("MurX", "Tmp1")
    # !!! Test si MurX existe... sinon il n'y a rien

    cylindre(
        detail=50,
        x=((D["AxeFort"] / 2) / D["Trame"] * D["CoteX"]),
        y=D["CoteY"],
        z=D["CoteZ"] / D["Etage"],
        dimX=((D["AxeFort"] / D["Trame"]) * D["CoteX"]),
        dimY=((D["AxeFort"] / D["Trame"]) * D["CoteY"]),
        dimZ=D["CoteZ"] / D["Etage"],
    )
    name("Demicylindre2")
    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["CoteX"] * 2,
        D["CoteY"],
        D["CoteZ"],
    )
    name("Tmp")
    soustraction("Demicylindre2", "Tmp")

    boite(
        y=D["CoteY"] + D["CoteY"] / D["Trame"],
        x=(((D["AxeFort"] / 2) / D["Trame"]) * D["CoteX"]),
        z=D["CoteZ"] / D["Etage"] * 2,
        dimX=D["AxeFort"] / D["Trame"] * D["CoteX"],
        dimY=D["CoteY"],
        dimZ=(D["CoteZ"] / D["Etage"] - 1.2) * 2,
    )
    name("Tmp")
    soustraction("Demicylindre2", "Tmp")

    cylindre(
        detail=50,
        x=(((D["AxeFort"] / 2) / D["Trame"]) * D["CoteX"]),
        y=D["CoteY"],
        z=D["CoteZ"] / D["Etage"] * 2,
        dimX=((D["AxeFort"] / D["Trame"]) * D["CoteX"] - D["EpMur"] * 2),
        dimY=((D["AxeFort"] / D["Trame"]) * D["CoteY"] - D["EpMur"] * 2),
        dimZ=D["CoteZ"] / D["Etage"] - D["EpMur"],
    )
    name("Tmp")
    soustraction("Demicylindre2", "Tmp")

    # Les points sont des tuples de 3 flottants
    coords = [
        (0, (D["AxeFort"]) / D["Trame"] * D["CoteY"], 0),
        (D["EpMur"], (D["AxeFort"]) / D["Trame"] * D["CoteY"], 0),
        (D["EpMur"], (D["AxeFort"]) / D["Trame"] * D["CoteY"], 1.2),
        (0, (D["AxeFort"]) / D["Trame"] * D["CoteY"], 1.2),
        (0, D["CoteY"], 1.2 + D["CoteZ"] / D["Etage"]),
        (0, D["CoteY"], D["CoteZ"] / D["Etage"]),
        (D["EpMur"], D["CoteY"], D["CoteZ"] / D["Etage"]),
        (D["EpMur"], D["CoteY"], 1.2 + D["CoteZ"] / D["Etage"]),
    ]

    # Faces définies par des tuples définissant les index des points (en partant de 0)

    faces = [
        (0, 1, 2, 3),
        (1, 2, 7, 6),
        (2, 3, 4, 7),
        (4, 7, 6, 5),
        (5, 6, 1, 0),
        (4, 5, 0, 3),
    ]

    me = bpy.data.meshes.new("GardeCorpsEscMesh")  # creation d'un nouveau mesh

    ob = bpy.data.objects.new("GardeCorpsEsc", me)
    ob.location = bpy.context.scene.cursor_location
    bpy.context.scene.objects.link(ob)

    me.from_pydata(coords, [], faces)
    me.update(calc_edges=True)

    # ESCALIER
    escalierY(
        (D["CoteX"] / D["Trame"] + D["EpMur"]) / 2,
        (D["AxeFort"]) / D["Trame"] * D["CoteY"],
        0,
        D["CoteZ"] / D["Etage"],
        (D["AxeFort"] - 1) / D["Trame"] * D["CoteY"],
        D["CoteX"] / D["Trame"] - D["EpMur"],
    )


########################################################################################################################


def soustractionAccesGW(D):

    boite(
        y=(D["AxeFort"] - 1 / 2) / D["Trame"] * D["CoteY"] + D["EpMur"] / 4,
        x=D["CoteX"] / 2,
        z=(D["CoteZ"] / D["Etage"] - D["EpMur"]) / 2,
        dimX=2 * D["CoteX"],
        dimY=D["CoteY"] / D["Trame"] - D["EpMur"] / 2,
        dimZ=D["CoteZ"] / D["Etage"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def extTourelleGWbis(D):

    cylindre(
        detail=30,
        y=(((D["AxeFort"] - 1) / D["Trame"]) * D["CoteY"]),
        x=D["CoteX"],
        z=D["CoteZ"] + D["CoteX"] / D["Etage"] / 2,
        dimX=((2 / D["Trame"]) * D["CoteX"]),
        dimY=((2 / D["Trame"]) * D["CoteY"]),
        dimZ=D["CoteZ"] / D["Etage"] / 2,
    )
    name("3Quartscylindre")
    boite(
        D["CoteX"] / 2,
        D["CoteY"] * (D["AxeFort"] - 1) / (D["Trame"] * 2),
        D["CoteZ"] + D["CoteZ"] / D["Etage"] / 2,
        D["CoteX"],
        (D["AxeFort"] - 1) / D["Trame"] * D["CoteY"],
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp")
    soustraction("3Quartscylindre", "Tmp")


########################################################################################################################


def soustractionVolGW(D):

    boite(
        D["CoteX"],
        D["CoteY"],
        (D["CoteZ"] / D["Etage"] - D["EpMur"]) / 2,
        (D["Trame"] - 1) / D["Trame"] * D["CoteX"],
        (D["Trame"] - 1) / D["Trame"] * D["CoteY"],
        D["CoteZ"] / D["Etage"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def soustractionLoggiaGW(D):

    boite(
        D["CoteX"] - D["CoteX"] / D["Trame"] / 2,
        D["CoteY"] / D["Trame"] + D["EpMur"] / 4,
        (D["CoteZ"] / D["Etage"]) * 3 / 2 + D["EpMur"] / 2,
        D["CoteX"] / D["Trame"],
        2 * D["CoteY"] / D["Trame"] - D["EpMur"],
        D["CoteZ"] / D["Etage"] - D["EpMur"],
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def soustractionTerrasseGW(D):

    boite(
        D["CoteX"] / D["Trame"] * 2,
        D["CoteY"] - (D["AxeFort"]) / D["Trame"] * D["CoteY"] / 2 + D["EpMur"] / 4,
        (1 + D["Etage"]) / 2 * D["CoteZ"] / D["Etage"],
        D["CoteX"] / D["Trame"] * 2,
        (D["AxeFort"]) / D["Trame"] * D["CoteY"] - D["EpMur"] / 2,
        (D["Etage"] - 1) * D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")

    boite(
        D["CoteX"] / D["Trame"] + D["EpMur"] / 2,
        D["CoteY"] - D["EpMur"] / 2,
        (1 + D["Etage"]) / 2 * D["CoteZ"] / D["Etage"],
        D["EpMur"],
        D["EpMur"],
        (D["Etage"] - 1) * D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Tmp")
    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    joindre("Cube_Base", "Tmp")


########################################################################################################################


def extPrismeToitGW(D):

    # Test sur la taille de trame
    Axe = int(D["Trame"] / 2) + 1
    if D["Trame"] > 4:
        backUp = D["Trame"]
        D["Trame"] = 3
        Axe = int(D["Trame"] / 2) + 1

    # Les points sont des tuples de 3 flottants
    coords = [
        (D["CoteX"], 0, D["CoteZ"]),
        (D["CoteX"], 0, D["CoteZ"] + D["CoteZ"] / D["Etage"]),
        (D["CoteX"], D["CoteY"] * 2 / D["Trame"], D["CoteZ"] + D["CoteZ"] / D["Etage"]),
        (D["CoteX"], D["CoteY"] * 2 / D["Trame"], D["CoteZ"]),
        ((Axe - 1) / D["Trame"] * D["CoteX"], D["CoteY"] * 2 / D["Trame"], D["CoteZ"]),
        ((Axe - 1) / D["Trame"] * D["CoteX"], 0, D["CoteZ"]),
    ]

    # Faces définies par des tuples définissant les index des points (en partant de 0)

    faces = [(0, 1, 2, 3), (1, 2, 4, 5), (4, 5, 0, 3), (0, 1, 5), (3, 2, 4)]

    me = bpy.data.meshes.new("PrismeMesh")  # creation d'un nouveau mesh

    ob = bpy.data.objects.new("Prisme", me)
    ob.location = bpy.context.scene.cursor_location
    bpy.context.scene.objects.link(ob)

    me.from_pydata(coords, [], faces)
    me.update(calc_edges=True)

    # restitution eventuelle de la valeur de trame
    if D["Trame"] > 4:
        D["Trame"] = backUp


########################################################################################################################


def extChemineeGW(D):

    boite(
        2 / D["Trame"] * D["CoteX"],
        (D["Trame"] - 1) / D["Trame"] * D["CoteY"],
        D["CoteZ"] / 2 + D["CoteZ"] / D["Etage"] / 2,
        2 * D["EpMur"],
        2 * D["EpMur"],
        D["CoteZ"] + D["Etage"],
    )
    name("ChemineeGW")
    cylindre(
        15,
        2 / D["Trame"] * D["CoteX"],
        (D["Trame"] - 1) / D["Trame"] * D["CoteY"],
        D["CoteZ"] / 2 + D["CoteZ"] / D["Etage"] / 2,
        3 / 2 * D["EpMur"],
        3 / 2 * D["EpMur"],
        (D["CoteZ"] + D["Etage"]),
    )
    name("Tmp")
    soustraction("ChemineeGW", "Tmp")


########################################################################################################################


def gwathmey(D):

    axiome(D)
    Dico = AxeFortGW(D)
    mursMainGW(D)
    extTourelleGW(D)
    soustrVolGW(D)
    extEscMaineGW(D)
    extTourelleGWbis(D)
    soustractionVolGW(D)
    soustractionLoggiaGW(D)
    soustractionAccesGW(D)
    soustractionTerrasseGW(D)
    extPrismeToitGW(D)
    extChemineeGW(D)


########################################################################################################################
# HEJDUK #
########################################################################################################################


def manipVolH(D):

    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["Cube_Base"].select = True
    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    bpy.ops.transform.resize(
        value=(D["CoteX"] / D["Old"], D["CoteY"] / D["Old"], D["CoteZ"] / D["Old"])
    )
    bpy.ops.transform.translate(
        value=(
            (D["CoteX"] - D["Old"]) / 2,
            (D["CoteY"] - D["Old"]) / 2,
            (D["CoteZ"] - D["Old"]) / 2,
        )
    )


########################################################################################################################


def dicoH(D):

    D["CoteZ"] = D["CoteZ"] + D["CoteZ"] / D["Etage"]
    D["CoteX"] = D["CoteX"] + D["CoteX"] / 5
    D["CoteY"] = D["CoteY"] + D["CoteY"] / 5
    D["Etage"] = D["Etage"] + 1

    return D


########################################################################################################################


def decoupePlateauH(D):

    effacer("Cube_Base")

    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        -D["EpMur"] / 2,
        D["CoteX"],
        D["CoteY"],
        D["EpMur"],
    )
    name("Cube_Base")
    for i in range(0, D["Etage"]):
        dupliquer(0, 0, D["CoteZ"] / D["Etage"])
    joindreMulti("Cube_Base")

    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] - D["CoteX"] / D["Etage"] - D["EpMur"] * 3 / 2 + 0.6,
        D["CoteX"],
        D["CoteY"],
        1.2,
    )
    name("gardeCorpsH")
    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] - D["CoteX"] / D["Etage"] - D["EpMur"] * 3 / 2 + 0.6,
        D["CoteX"] - D["EpMur"] * 2,
        D["CoteY"] - D["EpMur"] * 2,
        1.2,
    )
    name("Tmp")

    soustraction("gardeCorpsH", "Tmp")

    joindre("Cube_Base", "gardeCorpsH")


########################################################################################################################


def rotVolH(D):

    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.transform.rotate(value=math.pi / 4, axis=(0, 0, 1))
    bpy.ops.object.select_all(action="DESELECT")


########################################################################################################################


def rotH(obj):

    bpy.context.scene.objects[obj].select = True
    bpy.ops.transform.rotate(value=math.pi / 4, axis=(0, 0, 1))


########################################################################################################################


def chemineeH(D):

    boite(
        D["CoteX"] - D["CoteX"] / D["Trame"],
        D["CoteY"] / D["Trame"] / 2,
        D["CoteZ"] / 2 + D["CoteZ"] / D["Etage"] / 4,
        D["CoteX"] / D["Trame"] / D["Trame"],
        D["CoteY"] / D["Trame"] / D["Trame"],
        D["CoteZ"] + D["CoteZ"] / D["Etage"] / 2,
    )
    name("chemineeH")
    rotH("chemineeH")


########################################################################################################################


def recoupeIntH(obj, D):

    boite(0, 0, 0, D["CoteX"], D["CoteY"], D["CoteZ"] + 1)
    name("cache")
    bpy.ops.transform.translate(
        value=((D["CoteX"] / 2), (D["CoteY"] / 2), (D["CoteZ"] / 2))
    )

    dupliquer(D["CoteX"], 0, 0)
    dupliquer(0, D["CoteY"], 0)
    dupliquer(-D["CoteX"], 0, 0)
    dupliquer(-D["CoteX"], 0, 0)
    dupliquer(0, -D["CoteY"], 0)
    dupliquer(0, -D["CoteY"], 0)
    dupliquer(D["CoteX"], 0, 0)
    dupliquer(D["CoteX"], 0, 0)

    effacer("cache")
    joindre("cache.001", "cache.002")
    joindre("cache.001", "cache.003")
    joindre("cache.001", "cache.004")
    joindre("cache.001", "cache.005")
    joindre("cache.001", "cache.006")
    joindre("cache.001", "cache.007")
    joindre("cache.001", "cache.008")

    soustraction(obj, "cache.001")


########################################################################################################################


def mursH(D):

    boite(
        D["CoteX"] / 2,
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"],
        2 * D["CoteY"],
        D["CoteZ"],
    )
    name("murH")
    rotH("murH")

    boite(
        D["CoteX"] / 2,
        D["CoteY"],
        D["CoteZ"] / 2,
        D["EpMur"],
        2 * D["CoteY"],
        D["CoteZ"],
    )
    name("murH")
    rotH("murH.001")

    joindre("murH", "murH.001")

    boite(D["CoteX"] / 2, 0, D["CoteZ"] / 2, D["EpMur"], 2 * D["CoteY"], D["CoteZ"])
    name("murH")
    rotH("murH.001")

    joindre("murH", "murH.001")

    recoupeIntH("murH", D)

    joindre("Cube_Base", "murH")


########################################################################################################################


def colonnesH(D):

    cylindre(
        20,
        D["CoteX"] / (D["Trame"] * 2),
        D["CoteX"] / (D["Trame"] * 2),
        D["CoteZ"] / 2,
        D["EpMur"],
        D["EpMur"],
        D["CoteZ"] / 2,
    )
    name("Colonne3H")
    for i in range(1, D["Trame"]):
        dupliquer(D["CoteX"] / D["Trame"], 0, 0)
    joindreMulti("Colonne3H")
    for i in range(1, D["Trame"]):
        dupliquer(0, D["CoteX"] / D["Trame"], 0)
    joindreMulti("Colonne3H")

    cylindre(
        20,
        D["CoteX"] / (D["Trame"]),
        D["CoteX"] / (D["Trame"]),
        D["CoteZ"] / 2,
        D["EpMur"],
        D["EpMur"],
        D["CoteZ"] / 2,
    )
    name("Colonne2H")
    for i in range(1, D["Trame"] - 1):
        dupliquer(D["CoteX"] / D["Trame"], 0, 0)
    joindreMulti("Colonne2H")
    for i in range(1, D["Trame"] - 1):
        dupliquer(0, D["CoteX"] / D["Trame"], 0)
    joindreMulti("Colonne2H")

    joindre("Colonne3H", "Colonne2H")

    joindre("Cube_Base", "Colonne3H")


########################################################################################################################


def hejduk(D):

    axiome(D)
    D = dicoH(D)
    manipVolH(D)
    decoupePlateauH(D)
    colonnesH(D)
    chemineeH(D)
    mursH(D)
    rotVolH(D)


########################################################################################################################
# EISENMAN #
########################################################################################################################


def manipVolInitE(D):
    """boite(0, 0, D["CoteZ"]*3/4, D["CoteX"], D["CoteY"], D["CoteZ"]/2)
    name("DemiCube2")
    bpy.ops.transform.translate(value=((D["CoteX"]/2), (D["CoteY"]/2), 0))
    boite(0,0,D["CoteZ"]*1/4,D["CoteX"],D["CoteY"],D["CoteZ"]/2)
    name("DemiCube1")
    bpy.ops.transform.translate(value=((D["CoteX"]/2), (D["CoteY"]/2), 0))
    """
    # Enlever l'axiome
    bpy.ops.object.select_all(action="DESELECT")
    bpy.context.scene.objects["Cube_Base"].select = True
    bpy.ops.object.delete(use_global=False)
    """
    # Translation
    bpy.ops.object.select_all(action='DESELECT')
    bpy.context.scene.objects["DemiCube2"].select=True
    bpy.ops.transform.translate(value=(D["CoteX"]/6, -D["CoteY"]/6, -D["CoteZ"]/6))

    # Replacer l'origine à 0,0,0
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.transform.translate(value=(0, D["CoteY"]/6, 0))"""

    # Nouveau Cube_Base
    boite(
        D["CoteX"] / 2 + D["CoteX"] / 12,
        D["CoteY"] / 2 + D["CoteY"] / 12,
        D["CoteZ"] / 2 - D["CoteZ"] / 12,
        D["CoteX"] + D["CoteX"] / 6,
        D["CoteY"] + D["CoteY"] / 6,
        D["CoteZ"] - D["CoteZ"] / 6,
    )
    name("Cube_Base")

    # joindre("Cube_Base", "DemiCube1")
    # joindre("Cube_Base", "DemiCube2")


########################################################################################################################


def dicoE(D):

    D["CoteX"] = D["CoteX"] + D["CoteX"] / 6
    D["CoteY"] = D["CoteY"] + D["CoteY"] / 6
    D["CoteZ"] = D["CoteZ"] - D["CoteZ"] / 6
    D["Etage"] = D["Etage"] - 1

    return D


########################################################################################################################


def trameRythmeE(D):

    # Rythme 1
    boite(
        4 / 20 * D["CoteX"],
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"] / 2,
        D["CoteY"],
        D["CoteZ"],
    )
    name("RythmeE1")
    dupliquer(2 / 20 * D["CoteX"], 0, 0)
    dupliquer(6 / 20 * D["CoteX"], 0, 0)
    dupliquer(2.5 / 20 * D["CoteX"], 0, 0)
    dupliquer(3 / 20 * D["CoteX"], 0, 0)
    joindreMulti("RythmeE1")

    # joindre("Cube_Base", "RythmeE1")

    # Rythme 2
    boite(
        3 / 20 * D["CoteX"],
        D["CoteY"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"] / 2,
        D["CoteY"],
        D["CoteZ"],
    )
    name("RythmeE2")
    dupliquer(3 / 20 * D["CoteX"], 0, 0)
    dupliquer(2 / 20 * D["CoteX"], 0, 0)
    dupliquer(6 / 20 * D["CoteX"], 0, 0)
    dupliquer(2 / 20 * D["CoteX"], 0, 0)
    joindreMulti("RythmeE2")

    # joindre("Cube_Base", "RythmeE2")
    joindre("RythmeE1", "RythmeE2")


########################################################################################################################


def defVolMainE(D):

    trameRythmeE(D)

    boite(
        (12 / 20 * D["CoteX"]) / 2,
        (12 / 20 * D["CoteY"]) / 2 + D["EpMur"],
        D["CoteZ"] / D["Etage"] * 3 / 2,
        12 / 20 * D["CoteX"] - D["EpMur"] * 3,
        12 / 20 * D["CoteY"] - D["EpMur"] * 2,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("VolMainE")
    bpy.ops.transform.translate(value=(4 / 20 * D["CoteX"], 0, -3 / 2 * D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE1"]
    soustraction("RythmeE1", "VolMainE")

    boite(
        (12 / 20 * D["CoteX"]) / 2,
        (12 / 20 * D["CoteY"]) / 2 + D["EpMur"],
        D["CoteZ"] / D["Etage"] * 1 / 2,
        12 / 20 * D["CoteX"] - D["EpMur"] * 3,
        12 / 20 * D["CoteY"] - D["EpMur"] * 2,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("VolMainE")
    bpy.ops.transform.translate(value=(4 / 20 * D["CoteX"], 0, -3 / 2 * D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE1"]
    soustraction("RythmeE1", "VolMainE")

    # joindre("Cube_Base", "RythmeE1")

    """boite((17.5/20*D["CoteX"]-4/20*D["CoteX"])/2, (17.5/20*D["CoteY"]-4/20*D["CoteY"])/2, D["CoteZ"]/2, \
    17.5/20*D["CoteX"] - 4/20*D["CoteX"] -D["EpMur"]*3, 17.5/20*D["CoteY"]-4/20*D["CoteY"] -D["EpMur"]*3, D["CoteZ"]-D["EpMur"]*3)
    name("VolMainE")
    bpy.ops.transform.translate(value=(4/20*D["CoteX"], 0, -3/2*D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE2"]
    soustraction("RythmeE2", "VolMainE")"""

    """boite((17.5/20*D["CoteX"]-4/20*D["CoteX"])/2, (17.5/20*D["CoteY"]-4/20*D["CoteY"])/2, D["CoteZ"]/2, \
    17.5/20*D["CoteX"] - 4/20*D["CoteX"] -D["EpMur"]*3, 17.5/20*D["CoteY"]-4/20*D["CoteY"] -D["EpMur"]*3, D["CoteZ"]-D["EpMur"]*3)
    name("VolMainE")
    bpy.ops.transform.translate(value=(4/20*D["CoteX"], 0, -3/2*D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "VolMainE")"""


########################################################################################################################


def defCircuE(D):

    trameRythmeE(D)

    boite(
        0,
        17.5 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"] * 3 / 2,
        2 * D["CoteX"] - D["EpMur"] * 3,
        5 / 20 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE1"]
    soustraction("RythmeE1", "Tmp")

    boite(
        (18 / 20 * D["CoteX"]),
        0,
        D["CoteZ"] / D["Etage"] * 3 / 2,
        4 / 20 * D["CoteX"] - D["EpMur"] * 3,
        2 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE1"]
    soustraction("RythmeE1", "Tmp")

    boite(
        0,
        17.5 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"] * 3 / 2,
        2 * D["CoteX"] - D["EpMur"] * 3,
        5 / 20 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp")

    boite(
        (18 / 20 * D["CoteX"]),
        0,
        D["CoteZ"] / D["Etage"] * 3 / 2,
        4 / 20 * D["CoteX"] - D["EpMur"] * 3,
        2 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp")

    # RDC

    boite(
        0,
        17.5 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"] * 1 / 2,
        2 * D["CoteX"] - D["EpMur"] * 3,
        5 / 20 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")
    bpy.ops.transform.translate(value=(4 / 20 * D["CoteX"], 0, -3 / 2 * D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["RythmeE1"]
    soustraction("RythmeE1", "Tmp")

    boite(
        0,
        17.5 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"] * 1 / 2,
        2 * D["CoteX"] - D["EpMur"] * 3,
        5 / 20 * D["CoteY"] - D["EpMur"] * 3,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 3,
    )
    name("Tmp")
    bpy.ops.transform.translate(value=(4 / 20 * D["CoteX"], 0, -3 / 2 * D["EpMur"]))

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def toitureE(D):

    boite(
        D["CoteX"] / 2,
        17.5 / 20 * D["CoteY"],
        D["CoteZ"],
        D["CoteX"] - D["EpMur"] * 3,
        5 / 20 * D["CoteY"] - D["EpMur"] * 4,
        D["EpMur"] * 2,
    )
    name("Tmp1")

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp1")

    boite(
        (18 / 20 * D["CoteX"]),
        D["CoteY"] / 2,
        D["CoteZ"],
        4 / 20 * D["CoteX"] - D["EpMur"] * 3,
        D["CoteY"] - D["EpMur"] * 3,
        D["EpMur"] * 2,
    )
    name("Tmp2")

    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp2")


########################################################################################################################


def supprVolEntreeE(D):

    # joindre("Cube_Base", "RythmeE1")

    # Creusement principal
    boite(
        y=D["CoteY"] / 2,
        x=0,
        z=(D["CoteZ"] - D["EpMur"]) / 2,
        dimY=D["CoteY"],
        dimX=6 / 20 * D["CoteX"] - D["EpMur"],
        dimZ=D["CoteZ"] * 2,
    )
    name("Tmp")
    bpy.context.scene.objects.active = bpy.data.objects["Cube_Base"]
    soustraction("Cube_Base", "Tmp")

    # Entree
    boite(
        y=0,
        x=0,
        z=0,
        dimY=12 / 20 * D["CoteY"],
        dimX=12 / 20 * D["CoteX"] - D["EpMur"],
        dimZ=D["CoteZ"] * 2 - D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")

    boite(
        y=0,
        x=0,
        z=D["CoteZ"] / 2,
        dimY=6 / 20 * D["CoteY"],
        dimX=12 / 20 * D["CoteX"] - D["EpMur"],
        dimZ=D["CoteZ"] * 2,
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")

    # Volume central

    boite(
        y=9 / 20 * D["CoteY"],
        x=5 / 20 * D["CoteX"] / 2,
        z=3 / 2 * D["CoteZ"] / D["Etage"] - D["EpMur"],
        dimY=6 / 20 * D["CoteY"],
        dimX=1 / 20 * D["CoteX"],
        dimZ=D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Tmp")
    joindre("Cube_Base", "Tmp")

    # Structure de la facade

    # Vertical
    boite(
        D["EpMur"],
        3 / 20 * D["CoteY"] + D["EpMur"] / 2,
        (D["CoteZ"] - D["EpMur"]) / 2,
        D["EpMur"] * 2,
        D["EpMur"],
        (D["CoteZ"] - D["EpMur"]),
    )
    name("FacadeE")
    dupliquer(0, 2 / 20 * D["CoteY"] + 3 / 2 * D["EpMur"], 0)
    dupliquer(0, 15 / 20 * D["CoteY"] - 5 / 2 * D["EpMur"], 0)

    joindreMulti("FacadeE")

    # Horizontal
    boite(
        1.5 / 20 * D["CoteX"],
        13 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"],
        3 / 20 * D["CoteX"],
        14 / 20 * D["CoteY"],
        D["EpMur"],
    )
    name("Terrasse")

    boite(
        1.5 / 20 * D["CoteX"] + D["EpMur"],
        18.5 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"],
        3 / 20 * D["CoteX"] - D["EpMur"] * 2,
        3 / 20 * D["CoteY"] - D["EpMur"] * 2,
        D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Terrasse", "Tmp")

    boite(
        2.5 / 20 * D["CoteX"],
        13.5 / 20 * D["CoteY"] + D["EpMur"] / 2,
        D["CoteZ"] / D["Etage"],
        1 / 20 * D["CoteX"],
        4 / 20 * D["CoteY"] - D["EpMur"] * 3 / 2,
        D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Terrasse", "Tmp")

    boite(
        1.5 / 20 * D["CoteX"],
        9 / 20 * D["CoteY"],
        D["CoteZ"] / D["Etage"],
        1 / 20 * D["CoteX"],
        6 / 20 * D["CoteY"],
        D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Terrasse", "Tmp")

    boite(
        3 / 20 * D["CoteX"] + D["EpMur"],
        3 / 20 * D["CoteY"] + D["EpMur"] / 2,
        (D["CoteZ"] + D["CoteZ"] / 6 - D["EpMur"]) / 2,
        6 / 20 * D["CoteX"] - D["EpMur"] * 2,
        D["EpMur"] / 2,
        D["EpMur"],
    )
    name("Tmp")
    joindre("Terrasse", "Tmp")

    boite(
        3 / 20 * D["CoteX"] + D["EpMur"],
        6 / 20 * D["CoteY"] - D["EpMur"] / 2,
        (D["CoteZ"] + D["CoteZ"] / 6 - D["EpMur"]) / 2,
        6 / 20 * D["CoteX"] - D["EpMur"] * 2,
        D["EpMur"] / 2,
        D["EpMur"],
    )
    name("Tmp")
    joindre("Terrasse", "Tmp")

    boite(
        3 / 2 * D["EpMur"] + D["EpMur"] / 4,
        4.5 / 20 * D["CoteY"],
        (D["CoteZ"] + D["CoteZ"] / 6 - D["EpMur"]) / 2,
        D["EpMur"] / 2,
        3 / 20 * D["CoteY"] - D["EpMur"] * 2,
        D["EpMur"],
    )
    name("Tmp")
    joindre("Terrasse", "Tmp")

    joindre("FacadeE", "Terrasse")


########################################################################################################################


def fenteE(D):

    boite(
        8 / 20 * D["CoteX"],
        0,
        D["CoteZ"] / D["Etage"] * 3 / 2,
        D["EpMur"],
        D["EpMur"] * 4,
        D["CoteZ"] / D["Etage"] - D["EpMur"] * 2,
    )
    name("Tmp")
    soustraction("Cube_Base", "Tmp")


########################################################################################################################


def eisenman(D):

    axiome(D)
    manipVolInitE(D)

    D = dicoE(D)

    # trameRythmeE(D)
    defVolMainE(D)
    defCircuE(D)
    toitureE(D)
    # joindre("Cube_Base", "RythmeE1")

    supprVolEntreeE(D)
    fenteE(D)


########################################################################################################################
########################################################################################################################
########################################################################################################################


def colAll(D):

    cylindre(
        20,
        D["EpMur"] / 2,
        D["EpMur"] / 2,
        D["CoteZ"] / 2,
        D["EpMur"],
        D["EpMur"],
        D["CoteZ"] / 2,
    )
    name("ColonneAll")
    for i in range(1, D["Trame"]):
        dupliquer(D["CoteX"] / D["Trame"] - (D["EpMur"] / D["Trame"]), 0, 0)
    joindreMulti("ColonneAll")
    for i in range(1, D["Trame"]):
        dupliquer(0, D["CoteX"] / D["Trame"], 0)


########################################################################################################################
# LISTES DES FONCTIONS #
########################################################################################################################

listVolume = [manipVolH, manipVolInitE, extVolM, rien, rien]

listExtension = [
    rien,
    rien,
    extAvantGR,
    extEscM,
    extAvantM,
    extTourelleGW,
    extEscMaineGW,
]

listSoustrVol = [
    soustractionVolGW,
    soustractionTerrasseGW,
    soustractionAccesGW,
    soustractionFaceGR,
    soustractionToitGR,
    supprVolRdcM,
    supprVolEntreeE,
    supprArrondiM,
    soustractionToitureM,
    soustrVolGW,
]

listAddDetailToit = [
    extChemineeGW,
    extPrismeToitGW,
    extChemineeM,
    chemineeH,
    fenteE,
    toitureE,
    rien,
]

listAddDetailFacade = [fenteE, soustractionToitureM, soustractionLoggiaGW, rien, rien]

listMursMain = [mursMainM, defVolMainE, mursMainGW, mursH, mursMain, defCircuE, rien]

listColonnes = [colonnesH, rien, rien, colAll, rien]

listDivers = [decoupePlateauH, rien, rien, rien, rien]

listFinalisation = [rotVolH, rien, rien, rien, rien]


########################################################################################################################
# GENERATOR #
########################################################################################################################


def generatorNY5(D):

    bpy.ops.object.select_all(action="SELECT")  # Nettoyage
    bpy.ops.object.delete(use_global=False)

    # BASE
    axiome(D)

    # TRAME
    NB = random.randint(0, 6)
    if NB > 4:
        D["Trame"] = 7
    elif NB == 0:
        D["Trame"] = 5
    else:
        D["Trame"] = 3

    print(NB)

    # Ajout d'Axe Fort
    D = AxeFortGW(D)

    # MANIPULATION VOLUMETRIQUE

    NB = random.randint(0, 4)
    print(NB)
    if NB == 2:
        D = newDicoM(D)
    elif NB == 0:
        D = dicoH(D)

    listVolume[NB](D)

    print(NB)

    if NB == 1:
        D = dicoE(D)
        # D["DFct"]["manipVolInitE"] = ["DemiCube1", "DemiCube2"]

    # MURS MAIN
    NB1 = random.randint(0, 6)
    print(NB1)
    listMursMain[NB1](D)
    if NB1 == 1:
        if random.randint(0, 2) == 0:
            defCircuE(D)

    # EXTENSIONS
    NB2 = random.randint(0, 6)
    print(NB2)
    listExtension[NB2](D)

    if NB2 == 2 or NB2 == 4:
        del listExtension[2]
        del listExtension[3]

    NB2bis = random.randint(0, len(listExtension) - 1)
    listExtension[NB2bis](D)
    print(NB2bis)

    # DIVERS
    NB3 = random.randint(0, 4)
    print(NB3)
    listDivers[NB3](D)

    # COLONNES

    NB4 = random.randint(0, 4)
    print(NB4)
    listColonnes[NB4](D)

    # SOUSTRACTION VOLUMETRIQUE

    NB5 = random.randint(0, len(listSoustrVol) - 1)
    print(NB5)
    listSoustrVol[NB5](D)

    del listSoustrVol[NB5]

    NB5bis = random.randint(0, len(listSoustrVol) - 1)
    listSoustrVol[NB5bis](D)
    print(NB5bis)

    # AJOUT DETAIL TOITURE

    NB6 = random.randint(0, len(listAddDetailToit) - 1)
    print(NB6)
    listAddDetailToit[NB6](D)

    del listAddDetailToit[NB6]

    NB6bis = random.randint(0, len(listAddDetailToit) - 1)
    listAddDetailToit[NB6bis](D)
    print(NB6bis)

    # AJOUT DETAIL FACADE

    NB7 = random.randint(0, len(listAddDetailFacade) - 1)
    print(NB7)
    listAddDetailFacade[NB7](D)

    # FINALISATION

    NB8 = random.randint(0, len(listFinalisation) - 1)
    print(NB8)
    listFinalisation[NB8](D)

    bpy.ops.object.select_all(action="DESELECT")


########################################################################################################################
# INITIALISATION DU DICTIONNAIRE #
########################################################################################################################

Dico = {
    "CoteX": 9,
    "CoteY": 9,
    "CoteZ": 9,
    "Trame": 3,
    "Etage": 3,
    "Old": 9,
    "AxeFort": 2,
    "DFct": {"axiome": "CubeBase"},
    "EpMur": 0.2,
}


########################################################################################################################

generatorNY5(Dico)

########################################################################################################################

# gwathmey(Dico)
# meier(Dico)
# eisenman(Dico)
# graves(Dico)
# hejduk(Dico)

########################################################################################################################
