# SPDX-FileCopyrightText: 2015 Jean Trottet
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Richard Meier-Jean TROTTET
# Script réalisé sous Windows 7 avec Blender v2.76b-2015/11/03 -10:56- Hash f337fea

import bpy
import random
from random import *


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# variable générées aléatoire


dim_carre = choice([1])

# fonction de dimensionnement des differentes longueurs d'or:


def dor(x):
    return x - x / 1.618


dim1 = dor(dim_carre)


def dor2(x):
    return x - x / 1.618


dim_carre2 = dim_carre + dim1

dim2 = dor2(dim_carre2)


def dor3(x):
    return x - x / 1.618


dim_carre3 = dim_carre - dim1

dim3 = dor3(dim_carre3)


def dor4(x):  # Division Tshape interieure
    return x - x / 1.618


dim_carre4 = dim_carre3 - dim3

dim4 = dor4(dim_carre4)


# dimensionnement Cube complementaire

dim_xcomp = dim_carre + dim1

dim_ycomp = dim1


# creation fonction cube


def cube(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


# cube principal

(dim_carre / 2, dim_carre / 2, dim_carre / 2, dim_carre, dim_carre, dim_carre)
# cube de depart
(dim_carre - dim1 / 2, dim_carre / 2, dim_carre / 2, dim1, dim_carre, dim_carre)
# volume capable facade
cube(
    (dim_carre - dim1) / 2,
    dim_carre / 2,
    dim_carre / 2,
    dim_carre - dim1,
    dim_carre,
    dim_carre,
)

# Volume d'or1:

(dim_carre3 / 2, -dim1 / 2, dim_carre / 2, dim_carre - dim1, dim1, dim_carre)
# volumecapable
(dim_carre3 / 2, -dim1 / 2, dim_carre3 / 2, dim_carre - dim1, dim1, dim_carre3)
# à diviser
Tshape_1 = cube(dim1 / 2, -dim1 / 2, dim1 / 2, dim1, dim1, dim1)
Tshape_2 = (dim1 + dim3 / 2, -dim1 / 2, dim_carre3 / 2, dim3, dim1, dim_carre3)
Tshape_3 = (dim1 / 2, -dim1 / 2, dim1 + dim3 / 2, dim1, dim1, dim3)

Tshape_4 = cube(dim3 / 2, -dim1 / 2, dim_carre3 + dim1 / 2, dim3, dim1, dim1)
Tshape_5 = cube(dim3 + dim1 / 2, -dim1 / 2, dim_carre3 + dim1 / 2, dim1, dim1, dim1)


Tshape_6 = (dim_carre - dim1 / 2, -dim1 / 2, dim1 / 2, dim1, dim1, dim_carre4)
Tshape_7 = cube(
    dim_carre - dim1 / 2, -dim3 / 2, dim1 + dim_carre3 / 2, dim1, dim3, dim_carre3
)
Tshape_8 = (
    dim_carre - dim1 / 2,
    -dim1 / 2 - dim3 / 2,
    dim1 + dim_carre3 / 2,
    dim1,
    dim1 - dim3,
    dim_carre3,
)


# Division TshapeFacade cube:

(dim_carre - dim1 / 2, dim_carre - dim1 / 2, dim_carre / 2, dim1, dim1, dim_carre)
(dim_carre - dim1 / 2, dim_carre3 / 2, dim_carre3 / 2, dim1, dim_carre3, dim_carre3)
(dim_carre - dim1 / 2, dim_carre3 / 2, dim_carre3 + dim1 / 2, dim1, dim_carre3, dim1)
(dim_carre - dim1 / 2, dim_carre3 / 2, dim_carre3 / 2, dim1, dim_carre3, dim_carre4)

# division Tshape Facade cube,CARRE:

# position1:

Tshape_9 = (dim_carre - dim1 / 2, dim_carre3 / 2, dim3 / 2, dim1, dim_carre3, dim3)
Tshape_10 = (dim_carre - dim1 / 2, dim3 / 2, dim3 + dim1 / 2, dim1, dim3, dim1)
Tshape_11 = (dim_carre - dim1 / 2, dim3 + dim1 / 2, dim3 + dim1 / 2, dim1, dim1, dim1)

# position2:
(dim_carre - dim1 / 2, dim_carre3 / 2, dim3 / 2, dim1, dim_carre3, dim3)
(dim_carre - dim1 / 2, dim1 + dim3 / 2, dim3 + dim1 / 2, dim1, dim3, dim1)
(dim_carre - dim1 / 2, dim1 / 2, dim3 + dim1 / 2, dim1, dim1, dim1)

# position3:
(dim_carre - dim1 / 2, dim_carre3 / 2, dim3 / 2, dim1, dim_carre3, dim3)
(dim_carre - dim1 / 2, dim1 + dim3 / 2, dim3 + dim1 / 2, dim1, dim3, dim1)
(dim_carre - dim1 / 2, dim1 / 2, dim3 + dim1 / 2, dim1, dim1, dim1)
# position4:

# division Tshape Facade cube,Rectangle supérieur:

Tshape_12 = cube(
    dim_carre - dim1 / 2,
    dim_carre3 / 2,
    dim_carre - dim1 / 2 + dim3 / 2,
    dim1,
    dim_carre3,
    dim1 - dim3,
)
Tshape_13 = (
    dim_carre - dim1 / 2,
    dim3 + dim_carre / 2 - dim3,
    dim_carre3 + dim3 / 2,
    dim1,
    dim3,
    dim3,
)
Tshape_14 = (dim_carre - dim1 / 2, dim1 / 2, dim_carre3 + dim3 / 2, dim1, dim1, dim3)

# division Tshape Facade cube, Rectangle exterieur:

Tshape_15 = (
    dim_carre - dim1 / 2,
    dim_carre - dim1 / 2,
    dim_carre - dim1 / 2,
    dim1,
    dim1,
    dim1,
)
Tshape_16 = (
    dim_carre - dim1 / 2,
    dim_carre3 + dim4 / 2,
    dim_carre3 / 2,
    dim1,
    dim4,
    dim_carre3,
)
Tshape_17 = (
    dim_carre - dim1 / 2,
    dim_carre - dim3 / 2,
    dim_carre3 / 2,
    dim1,
    dim3,
    dim_carre3,
)

liste_Tshape = (
    Tshape_2,
    Tshape_3,
    Tshape_6,
    Tshape_8,
    Tshape_9,
    Tshape_10,
    Tshape_11,
    Tshape_13,
    Tshape_14,
    Tshape_15,
    Tshape_16,
    Tshape_17,
)

# condition
sample_1 = sample(liste_Tshape, randint(1, len(liste_Tshape)))
print(sample_1)

liste = []
for Tshape in sample_1:
    a, b, c, d, e, f = Tshape
    cube(a, b, c, d, e, f)
    index = liste_Tshape.index(Tshape)
    liste.append(index)
    liste.sort()

if liste.count(9) == 0 and liste.count(10) == 0 and liste.count(11) == 0:
    objet = ["E:\\Jean AIM\\Objet3.obj", "E:\\Jean AIM\\Arretebiso.obj"]
    objet3 = choice(objet)
    bpy.ops.import_scene.obj(filepath=objet3)
    print("1 arrete chanfrein cree ")
    print("__________________________________________________")

if liste.count(9) == 1 and liste.count(10) == 0 and liste.count(2) == 0:
    balcon = ["E:\\Jean AIM\\balcon.obj", "E:\\Jean AIM\\balcon2.obj"]
    balconchoix = choice(balcon)
    bpy.ops.import_scene.obj(filepath=balconchoix)
    print("1 balcon cree ")
    print("__________________________________________________")


if liste.count(9) == 1 and liste.count(10) == 1 and liste.count(2) == 0:
    gallerie3 = "E:\\Jean AIM\\gallerie3.obj"
    bpy.ops.import_scene.obj(filepath=gallerie3)
    print("1 gallerie cree ")
    print("__________________________________________________")


if liste.count(9) == 1 and liste.count(10) == 1:
    gallerie2 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//gallerie2.obj"
    bpy.ops.import_scene.obj(filepath=gallerie2)
    print("1 gallerie cree ")
    print("__________________________________________________")

if liste.count(0) == 0 and liste.count(2) == 0:
    escalier5 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//escalier5.obj"
    bpy.ops.import_scene.obj(filepath=escalier5)
    print("1 grand escalier cree ")
    print("__________________________________________________")

elif liste.count(2) == 0:
    gallerie = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//gallerie.obj"
    bpy.ops.import_scene.obj(filepath=gallerie)
    print("1 gallerie cree ")
    print("__________________________________________________")

elif liste.count(0) == 0:
    escalier2 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//escalier4.obj"
    bpy.ops.import_scene.obj(filepath=escalier2)
    print("1 escalier cree ")
    print("__________________________________________________")

elif liste.count(4) == 0 and liste.count(6) == 0:
    objet2 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//escaliertest.obj"
    bpy.ops.import_scene.obj(filepath=objet2)
    print("1 escalier cree ")
    print("__________________________________________________")

if liste.count(4) == 1 and liste.count(5) == 1 and liste.count(8) == 1:
    cheminée1 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//Objet4.obj"
    bpy.ops.import_scene.obj(filepath=cheminée1)
    print("1 cheminee cree ")
    print("__________________________________________________")

if liste.count(2) == 1 and liste.count(4) == 0:
    cheminée2 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//cheminé2.obj"
    bpy.ops.import_scene.obj(filepath=cheminée2)
    print("1 cheminee cree ")
    print("__________________________________________________")

if liste.count(9) == 0 and liste.count(10) == 1 and liste.count(11) == 0:
    cheminée4 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//cheminé4.obj"
    bpy.ops.import_scene.obj(filepath=cheminée4)
    print("1 cheminee cree ")
    print("__________________________________________________")

if liste.count(3) == 0:
    acrotere = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof.obj"
    bpy.ops.import_scene.obj(filepath=acrotere)
    bpy.ops.object.material_slot_remove()
    print("1 acces toit cree ")
    print("__________________________________________________")

if liste.count(3) == 0 and liste.count(9) == 1:
    acrotere2 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof2.obj"
    bpy.ops.import_scene.obj(filepath=acrotere2)
    print("1 acces toit cree ")
    print("__________________________________________________")

if liste.count(3) == 0 and liste.count(9) == 0 and liste.count(10) == 1:
    acrotere3 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof3.obj"
    bpy.ops.import_scene.obj(filepath=acrotere3)
    print("acrotere type_1 cree ")
    print("__________________________________________________")

if liste.count(3) == 0 and liste.count(9) == 0 and liste.count(11) == 1:
    acrotere3 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof3.obj"
    bpy.ops.import_scene.obj(filepath=acrotere3)
    print("acrotere type_2 cree ")
    print("__________________________________________________")

if liste.count(3) == 1 and liste.count(9) == 0 and liste.count(11) == 1:
    acrotere4 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof4.obj"
    bpy.ops.import_scene.obj(filepath=acrotere4)
    print("acrotere type_3 cree ")
    print("__________________________________________________")

if liste.count(3) == 1 and liste.count(9) == 0 and liste.count(10) == 1:
    acrotere4 = (
        "Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof4.obj"
    )
    bpy.ops.import_scene.obj(filepath=acrotere4)
    print("acrotere type_4 cree ")
    print("__________________________________________________")

if liste.count(3) == 1 and liste.count(9) == 1:
    acrotere5 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof5.obj"
    bpy.ops.import_scene.obj(filepath=acrotere5)
    print("acrotere type_4 cree ")
    print("__________________________________________________")

if liste.count(3) == 1:
    acrotere6 = "Ramen//Desktop//AIM module I-remise160114//Jean TROTTET//CODE//Objets//roof6.obj"
    bpy.ops.import_scene.obj(filepath=acrotere6)
    obj_objects = bpy.context.selected_objects[:]
    print("acrotere type_5 cree ")
    print("__________________________________________________")

print("******************************************************")

# RAndom Acces toit:


Actoit_1 = (dim3 / 2, -dim1 / 2, dim_carre + dim1 / 2, dim3, dim1, dim1)
Actoit_2 = (dim3 / 2, -dim1 / 2, dim_carre + dim3 / 2, dim3, dim1, dim3)
Actoit_3 = (dim3 / 2, dim1, dim_carre + dim1 / 2, dim3, dim1, dim1)
Actoit_4 = (dim3 / 2, dim1, dim_carre + dim3 / 2, dim3, dim1, dim3)
Actoit_5 = (dim3 / 2, dim_carre - dim1 / 2, dim_carre + dim1 / 2, dim3, dim1, dim1)
Actoit_6 = (dim3 / 2, dim_carre - dim1 / 2, dim_carre + dim3 / 2, dim3, dim1, dim3)
Actoit_7 = (
    dim3 / 2,
    (dim_carre / 2 + dim3 / 2) / 2,
    dim_carre + dim3 / 2,
    dim3,
    dim_carre + dim1,
    dim3,
)

liste_Actoit = (Actoit_1, Actoit_2, Actoit_3, Actoit_4, Actoit_5, Actoit_6, Actoit_7)

randomACtoit = choice(liste_Actoit)
print(randomACtoit)
a, b, c, d, e, f = randomACtoit
cube(a, b, c, d, e, f)


bpy.ops.object.select_all(action="DESELECT")


"""
#Division Tshapefacade:


#Division T-shape Carré:

Tshape2_1=(dim_carre-dim_carre3/2,-dim1/2,dim1+dim3/2,dim_carre3,dim1,dim3)
Tshape2_2=(dim1+dim3/2,-dim1/2,dim_carre3+dim_carre4/2,dim3,dim1,dim_carre4)
Tshape2_3=(dim_carre-dim_carre4/2,-dim1/2,dim_carre3+dim_carre4/2,dim_carre4,dim1,dim_carre4)

liste_Tshape2= (Tshape2_1,Tshape2_2,Tshape2_3)

#Division T-shape Petit Rectangle d'or:

Tshape1_1=(dim1/2,-dim1/2,dim_carre-dim3/2,dim1,dim1,dim3)
Tshape1_2=(dim3/2,-dim1/2,dim1+dim_carre4/2,dim3,dim1,dim_carre4)
Tshape1_3=(dim3+dim4/2,-dim1/2,dim1+dim_carre4/2,dim4,dim1,dim_carre4)

liste_Tshape1= (Tshape1_1,Tshape1_2,Tshape1_3)

#Division T-shape Grand rectangle d'or:

Tshape3_1=(dim1/2,-dim1/2,dim1-dim1/2,dim_carre4,dim1,dim1)
Tshape3_2=(dim_carre-dim_carre3/2,-dim1/2,dim3/2,dim_carre3,dim1,dim3)
Tshape3_3=(dim_carre-dim_carre3/2,-dim1/2,dim3+dim4/2,dim_carre3,dim1,dim4)

liste_Tshape3= (Tshape3_1,Tshape3_2,Tshape3_3)





sample_1=sample (liste_Tshape1, randint (1,len (liste_Tshape1)))

l_tshape = []
for sample in sample_1:
    Tshape = choice (sample)
    l_tshape.append (Tshape)



print (sample_1)"""
