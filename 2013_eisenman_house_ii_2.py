# SPDX-FileCopyrightText: 2013 Liesbet Neesen & Evelien Ral
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# CUBE


def Cube(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X, pos_Y, pos_Z + dim_Z / 2)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


# ---------------------------------------------------------------
# COLUMNS


def Column(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X, pos_Y, pos_Z + dim_Z / 2)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


def Columns():
    for i in range(0, 4):
        for j in range(0, 4):
            Column(
                (i * 0.33) - ((4 / 2) * 2) + 3.5,
                (j * 0.33) - ((4 / 2) * 2) + 3.5,
                0,
                0.01,
                0.01,
                1,
            )
            bpy.context.object.name = "COLUMNS"
            bpy.data.objects["COLUMNS"].select = True
            bpy.context.object.name = "COLUMNS.001"
            bpy.data.objects["COLUMNS.001"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "COLUMNS"
            bpy.data.objects["COLUMNS"].select = True
            bpy.context.object.name = "COLUMNS.001"
            bpy.data.objects["COLUMNS.001"].select = True
            bpy.ops.object.join()


def ColumnsforRotation():
    for i in range(0, 4):
        for j in range(0, 4):
            Column(
                (i * 0.33) - ((4 / 2) * 2) + 3.5,
                (j * 0.33) - ((4 / 2) * 2) + 3.5,
                0,
                0.01,
                0.01,
                1,
            )
            bpy.context.object.name = "COLUMNS-R"
            bpy.data.objects["COLUMNS-R"].select = True
            bpy.context.object.name = "COLUMNS.001-R"
            bpy.data.objects["COLUMNS.001-R"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "COLUMNS-R"
            bpy.data.objects["COLUMNS-R"].select = True
            bpy.context.object.name = "COLUMNS.001-R"
            bpy.data.objects["COLUMNS.001-R"].select = True
            bpy.ops.object.join()


def ColumnsforSlice():
    for i in range(0, 4):
        for j in range(0, 4):
            Column(
                (i * 0.33) - ((4 / 2) * 2) + 3.5,
                (j * 0.33) - ((4 / 2) * 2) + 3.5,
                0,
                0.01,
                0.01,
                1,
            )
            bpy.context.object.name = "COLUMNS-S"
            bpy.data.objects["COLUMNS-S"].select = True
            bpy.context.object.name = "COLUMNS.001-S"
            bpy.data.objects["COLUMNS.001-S"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "COLUMNS-S"
            bpy.data.objects["COLUMNS-S"].select = True
            bpy.context.object.name = "COLUMNS.001-S"
            bpy.data.objects["COLUMNS.001-S"].select = True
            bpy.ops.object.join()


# ---------------------------------------------------------------

# SURFACES


def Surface(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X, pos_Y, pos_Z + dim_Z / 2)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


def Surfaces():
    for j in range(0, 4):
        Surface(
            (1 * 0.33) - ((4 / 2) * 2) + 3.66,
            (j * 0.33) - ((4 / 2) * 2) + 3.5,
            0,
            1,
            0.01,
            1,
        )
        bpy.context.object.name = "SURFACES"
        bpy.data.objects["SURFACES"].select = True
        bpy.ops.object.join()
        bpy.context.object.name = "SURFACES"
        bpy.data.objects["SURFACES"].select = True
        bpy.ops.object.join()


def SurfacesforRotation():
    for j in range(0, 4):
        Surface(
            (1 * 0.33) - ((4 / 2) * 2) + 3.66,
            (j * 0.33) - ((4 / 2) * 2) + 3.5,
            0,
            1,
            0.01,
            1,
        )
        bpy.context.object.name = "SURFACES-R"
        bpy.data.objects["SURFACES-R"].select = True
        bpy.ops.object.join()
        bpy.context.object.name = "SURFACES-R"
        bpy.data.objects["SURFACES-R"].select = True
        bpy.ops.object.join()


def SurfacesforSlice():
    for j in range(0, 4):
        Surface(
            (1 * 0.33) - ((4 / 2) * 2) + 3.66,
            (j * 0.33) - ((4 / 2) * 2) + 3.5,
            0,
            1,
            0.01,
            1,
        )
        bpy.context.object.name = "SURFACES-S"
        bpy.data.objects["SURFACES-S"].select = True
        bpy.ops.object.join()
        bpy.context.object.name = "SURFACES-S"
        bpy.data.objects["SURFACES-S"].select = True
        bpy.ops.object.join()


# ---------------------------------------------------------------

# VOLUMES


def Volume(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X, pos_Y, pos_Z + dim_Z / 2)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


def Volumes():
    for j in range(0, 2):
        for k in range(0, 2):
            Volume(
                (1 * 0.66) - ((2 / 2) * 2) + 1.33,
                (j * 0.66) - ((2 / 2) * 2) + 1.66,
                (k * 0.66) - ((2 / 2) * 2) + 2,
                1,
                0.33,
                0.33,
            )
            bpy.context.object.name = "VOLUMES"
            bpy.data.objects["VOLUMES"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "VOLUMES"
            bpy.data.objects["VOLUMES"].select = True
            bpy.ops.object.join()


def VolumesforRotation():
    for j in range(0, 2):
        for k in range(0, 2):
            Volume(
                (1 * 0.66) - ((2 / 2) * 2) + 1.33,
                (j * 0.66) - ((2 / 2) * 2) + 1.66,
                (k * 0.66) - ((2 / 2) * 2) + 2,
                1,
                0.33,
                0.33,
            )
            bpy.context.object.name = "VOLUMES-R"
            bpy.data.objects["VOLUMES-R"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "VOLUMES-R"
            bpy.data.objects["VOLUMES-R"].select = True
            bpy.ops.object.join()


def VolumesforSlice():
    for j in range(0, 2):
        for k in range(0, 2):
            Volume(
                (1 * 0.66) - ((2 / 2) * 2) + 1.33,
                (j * 0.66) - ((2 / 2) * 2) + 1.66,
                (k * 0.66) - ((2 / 2) * 2) + 2,
                1,
                0.33,
                0.33,
            )
            bpy.context.object.name = "VOLUMES-S"
            bpy.data.objects["VOLUMES-S"].select = True
            bpy.ops.object.join()
            bpy.context.object.name = "VOLUMES-S"
            bpy.data.objects["VOLUMES-S"].select = True
            bpy.ops.object.join()


#

# ---------------------------------------------------------------

# CUBE


def Box():
    bpy.ops.mesh.primitive_cube_add(
        location=(0, 0, 0.5)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.context.object.name = "BOX"


# ---------------------------------------------------------------
def RBox():
    bpy.ops.mesh.primitive_cube_add(
        location=(0, 0, 0.5)
    )  # remettre le cube sur la base
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.context.object.name = "RBOX"


# ---------------------------------------------------------------
def Camera_Axono():
    bpy.ops.object.camera_add(
        location=(-2, -3.31452, 3.96331), rotation=(1.047, 0.0, -5.235)
    )
    ob = bpy.context.object
    ob.data.type = "ORTHO"
    ob.data.ortho_scale = 20
    bpy.context.scene.camera = ob

    for area in bpy.context.screen.areas:
        if area.type == "VIEW_3D":
            for region in area.regions:
                if region.type == "WINDOW":
                    override = {
                        "window": bpy.context.window,
                        "area": area,
                        "region": region,
                        "scene": bpy.context.scene,
                        "active_object": ob,
                    }
                    bpy.ops.view3d.object_as_camera(override)


Camera_Axono()


# ---------------------------------------------------------------
def light():
    bpy.ops.object.lamp_add(type="AREA", location=(5, -5, 30), rotation=(0, 0.785, 0))
    bpy.context.object.name = "lamp"


light()

print("-----------------------------------------------")

numberofcubes = random.randint(1, 2)
print("numberofcubes=", numberofcubes)


def ConfigArchi():
    for i in range(0, numberofcubes):
        Cube(0, (i * 2) - ((numberofcubes / 2) * 2) + 1, 0, 1, 1, 1)

        if numberofcubes == 1:
            bpy.data.objects["Cube"].location.x += -12
            bpy.context.object.name = "NOC1"

            divideoptions = [1, 2, 3, 4]
            print("divideoptions=", divideoptions)
            chosenoption = random.choice(divideoptions)
            print("chosenoption=", chosenoption)

            if chosenoption == 1:
                Columns()
                bpy.data.objects["COLUMNS.001"].location.x += -9
                ColumnsforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["COLUMNS.001-R"].location.x += -6

                if rotation == 1.57:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                if rotation == 0:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                elif rotation == 0.785:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

            if chosenoption == 2:
                Surfaces()
                bpy.data.objects["SURFACES"].location.x += -9
                SurfacesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["SURFACES-R"].location.x += -6
                if rotation == 1.57:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                if rotation == 0:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                elif rotation == 0.785:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

            if chosenoption == 3:
                Volumes()
                bpy.data.objects["VOLUMES"].location.x += -9
                VolumesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["VOLUMES-R"].location.x += -6
                if rotation == 1.57:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                if rotation == 0:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                elif rotation == 0.785:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    bpy.ops.transform.translate(value=(3, 0, 0))

            if chosenoption == 4:
                Box()
                bpy.data.objects["BOX"].location.x += -9
                Box()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["BOX"].location.x += -6
                if rotation == 1.57:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["BOX"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                if rotation == 0:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["BOX"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    bpy.ops.transform.translate(value=(3, 0, 0))

                elif rotation == 0.785:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["BOX"].location.x += -3
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    bpy.ops.transform.translate(value=(3, 0, 0))

            bpy.ops.mesh.primitive_cube_add(location=(-15, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE"

            bpy.ops.mesh.primitive_cube_add(location=(-12, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE1"

            bpy.ops.mesh.primitive_cube_add(location=(-9, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE2"

            bpy.ops.mesh.primitive_cube_add(location=(-6, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE3"

            bpy.ops.mesh.primitive_cube_add(location=(-3, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE4"

            bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE4"

            bpy.data.objects["CUBE"].select = True
            bpy.data.objects["CUBE1"].select = True
            bpy.data.objects["CUBE2"].select = True
            bpy.data.objects["CUBE3"].select = True
            bpy.data.objects["CUBE4"].select = True
            bpy.ops.object.join()

        else:
            bpy.ops.transform.translate(value=(-12, 0, 0))
            bpy.context.object.name = "NOC2"
            bpy.data.objects["NOC2"].select = True
            bpy.ops.object.join()
            bpy.data.objects["NOC2"].location.x += 0

            divideoptions = [5, 6, 7, 8, 9, 10]
            print("divideoptions=", divideoptions)
            chosenoption = random.choice(divideoptions)
            print("chosenoption=", chosenoption)

            if chosenoption == 5:
                Columns()
                bpy.data.objects["COLUMNS.001"].location.x += -9
                bpy.data.objects["COLUMNS.001"].location.y += -1
                ColumnsforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["COLUMNS.001-R"].location.x += -6
                bpy.data.objects["COLUMNS.001-R"].location.y += -1

                if rotation == 1.57:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                Surfaces()
                bpy.data.objects["SURFACES"].location.x += -9
                bpy.data.objects["SURFACES"].location.y += 1
                SurfacesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["SURFACES-R"].location.x += -6
                bpy.data.objects["SURFACES-R"].location.y += 1
                if rotation == 1.57:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["SURFACES-S"].location.x += -2
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

            if chosenoption == 6:
                Columns()
                bpy.data.objects["COLUMNS.001"].location.x += -9
                bpy.data.objects["COLUMNS.001"].location.y += -1
                ColumnsforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["COLUMNS.001-R"].location.x += -6
                bpy.data.objects["COLUMNS.001-R"].location.y += -1
                if rotation == 1.57:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                Box()
                bpy.data.objects["BOX"].location.x += -9
                bpy.data.objects["BOX"].location.y += 1
                Box()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["BOX"].location.x += -6
                bpy.data.objects["BOX"].location.y += 1
                if rotation == 1.57:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

            if chosenoption == 7:
                Columns()
                bpy.data.objects["COLUMNS.001"].location.x += -9
                bpy.data.objects["COLUMNS.001"].location.y += -1
                ColumnsforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["COLUMNS.001-R"].location.x += -6
                bpy.data.objects["COLUMNS.001-R"].location.y += -1

                if rotation == 1.57:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    ColumnsforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["COLUMNS.001-S"].location.x += -3
                    bpy.data.objects["COLUMNS.001-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["COLUMNS.001-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "COLUMNS.001-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                Volumes()
                bpy.data.objects["VOLUMES"].location.x += -9
                bpy.data.objects["VOLUMES"].location.y += 1
                VolumesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["VOLUMES-R"].location.x += -6
                bpy.data.objects["VOLUMES-R"].location.y += 1

                if rotation == 1.57:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

            if chosenoption == 8:
                Volumes()
                bpy.data.objects["VOLUMES"].location.x += -9
                bpy.data.objects["VOLUMES"].location.y += -1
                VolumesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["VOLUMES-R"].location.x += -6
                bpy.data.objects["VOLUMES-R"].location.y += -1

                if rotation == 1.57:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 5, 6, 7, 8]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                Box()
                bpy.data.objects["BOX"].location.x += -9
                bpy.data.objects["BOX"].location.y += 1
                Box()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["BOX"].location.x += -6
                bpy.data.objects["BOX"].location.y += 1

                if rotation == 1.57:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

            if chosenoption == 9:
                Volumes()
                bpy.data.objects["VOLUMES"].location.x += -9
                bpy.data.objects["VOLUMES"].location.y += 1
                VolumesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["VOLUMES-R"].location.x += -6
                bpy.data.objects["VOLUMES-R"].location.y += 1

                if rotation == 1.57:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    VolumesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["VOLUMES-S"].location.x += -3
                    bpy.data.objects["VOLUMES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["VOLUMES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "VOLUMES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                Surfaces()
                Surfaces()
                bpy.data.objects["SURFACES"].location.x += -9
                bpy.data.objects["SURFACES"].location.y += -1
                SurfacesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["SURFACES-R"].location.x += -6
                bpy.data.objects["SURFACES-R"].location.y += -1

                if rotation == 1.57:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

            if chosenoption == 10:
                Box()
                bpy.data.objects["BOX"].location.x += -9
                bpy.data.objects["BOX"].location.y += -1
                Box()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["BOX"].location.x += -6
                bpy.data.objects["BOX"].location.y += -1

                if rotation == 1.57:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                if rotation == 0:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                elif rotation == 0.785:
                    Box()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["BOX"].location.x += -3
                    bpy.data.objects["BOX"].location.y += -1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["BOX"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "BOX"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, 1, 0))

                Surfaces()
                Surfaces()
                bpy.data.objects["SURFACES"].location.x += -9
                bpy.data.objects["SURFACES"].location.y += 1
                SurfacesforRotation()
                rotation = random.choice([0, 0.785, 1.57])
                bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                bpy.context.object.rotation_euler[2] = 1 * rotation
                bpy.data.objects["SURFACES-R"].location.x += -6
                bpy.data.objects["SURFACES-R"].location.y += 1

                if rotation == 1.57:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 1.57
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                if rotation == 0:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                elif rotation == 0.785:
                    SurfacesforSlice()
                    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
                    bpy.context.object.rotation_euler[2] = 0.785
                    bpy.data.objects["SURFACES-S"].location.x += -3
                    bpy.data.objects["SURFACES-S"].location.y += 1
                    scaleoptions = [1, 2, 3, 4, 5, 6, 7]
                    print("scaleoptions=", scaleoptions)
                    chosenscaleoption = random.choice(scaleoptions)
                    print("chosenscaleoption=", chosenscaleoption)
                    if chosenscaleoption == 1:
                        bpy.ops.transform.resize(value=(1, 1, 1))
                    if chosenscaleoption == 2:
                        bpy.ops.transform.resize(value=(1, 1, 0.33))
                    if chosenscaleoption == 3:
                        bpy.ops.transform.resize(value=(1, 1, 0.66))
                    if chosenscaleoption == 4:
                        bpy.ops.transform.resize(value=(1, 0.33, 1))
                    if chosenscaleoption == 5:
                        bpy.ops.transform.resize(value=(1, 0.66, 1))
                    if chosenscaleoption == 6:
                        bpy.ops.transform.resize(value=(0.33, 1, 1))
                    if chosenscaleoption == 7:
                        bpy.ops.transform.resize(value=(0.66, 1, 1))
                    bpy.data.objects["SURFACES-S"].select = True
                    bpy.ops.object.duplicate_move()
                    new_obj = bpy.context.scene.objects.active
                    new_obj.name = "SURFACES-S"
                    move = random.choice([0, 0.33, 0.66])
                    bpy.ops.transform.translate(value=(1 * move, 1 * move, 0))
                    bpy.ops.transform.translate(value=(3, -1, 0))

                    # RED BOXES

            bpy.ops.mesh.primitive_cube_add(location=(-15, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE"

            bpy.ops.mesh.primitive_cube_add(location=(-12, -1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE1"

            bpy.ops.mesh.primitive_cube_add(location=(-9, -1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE2"

            bpy.ops.mesh.primitive_cube_add(location=(-6, -1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE3"

            bpy.ops.mesh.primitive_cube_add(location=(-3, -1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE4"

            bpy.ops.mesh.primitive_cube_add(location=(-12, 1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE5"

            bpy.ops.mesh.primitive_cube_add(location=(-9, 1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE6"

            bpy.ops.mesh.primitive_cube_add(location=(-6, 1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE7"

            bpy.ops.mesh.primitive_cube_add(location=(-3, 1, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE8"

            bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0.5))
            bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
            bpy.context.object.name = "CUBE9"

            bpy.data.objects["CUBE"].select = True
            bpy.data.objects["CUBE1"].select = True
            bpy.data.objects["CUBE2"].select = True
            bpy.data.objects["CUBE3"].select = True
            bpy.data.objects["CUBE4"].select = True
            bpy.data.objects["CUBE5"].select = True
            bpy.data.objects["CUBE6"].select = True
            bpy.data.objects["CUBE7"].select = True
            bpy.data.objects["CUBE8"].select = True
            bpy.data.objects["CUBE9"].select = True
            bpy.ops.object.join()


ConfigArchi()
