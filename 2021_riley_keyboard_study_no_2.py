# SPDX-FileCopyrightText: 2021 Omar Asif & Loïc Gras
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# KEYBOARD STUDY No. 2 - Terry Riley
# GRAS Loïc - ASIF Omar
# AlIce 2021-2022


# ____________________________________________________________________________________ #

#                                        PRESETS                                       #

# ____________________________________________________________________________________ #


# import modules
import bpy
import math
from random import randint


# import patterns
def get_objects(name):
    res = []
    for obj in bpy.data.objects:
        if name in obj.name:
            res.append(obj)
    return res


# boolean function
def apply_modifiers(obj):
    ctx = bpy.context.copy()
    ctx["object"] = obj
    for _, m in enumerate(obj.modifiers):
        try:
            ctx["modifier"] = m
            bpy.ops.object.modifier_apply(ctx, modifier=m.name)
        except RuntimeError:
            print(f"Error applying {m.name} to {obj.name}, removing it instead.")
            obj.modifiers.remove(m)
    for m in obj.modifiers:
        obj.modifiers.remove(m)


# clean everything outside the frame
def bool(obj1, obj2, type="INTERSECT"):
    bool = obj2.modifiers.new(name="booly", type="BOOLEAN")
    bool.object = obj1
    bool.operation = type
    bool.solver = "FAST"


def multi_boolean(collection, object, type):
    for element in collection:
        bool(object, element, type)


# frame cube 10*10*10
def Frame(position, dimension, name):
    bpy.ops.mesh.primitive_cube_add(location=(0, 5, 5))
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.display_type = "WIRE"


# import pattern
def duplicate(object_name, location):
    bpy.ops.object.select_all(action="DESELECT")
    for obj in bpy.data.objects:
        if obj.name == object_name:
            obj.select_set(True)
            break
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": location,
            "orient_type": "GLOBAL",
            "orient_matrix": ((1, 0, 0), (0, 1, 0), (0, 0, 1)),
            "orient_matrix_type": "GLOBAL",
            "constraint_axis": (False, False, False),
            "mirror": True,
            "use_proportional_edit": False,
            "proportional_edit_falloff": "RANDOM",
            "proportional_size": 1,
            "use_proportional_connected": False,
            "use_proportional_projected": False,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "gpencil_strokes": False,
            "cursor_transform": False,
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
            "use_accurate": False,
            "use_automerge_and_split": False,
        },
    )


# __________________________________________________________________________________ #

#                               DEFINING THE ELEMENTS                                #

# __________________________________________________________________________________ #


# patterns definition

dictionnaire = (("P1", 3), ("P2", 3), ("P3", 4), ("P4", 4), ("P5", 6), ("P6", 6))

bpy.ops.object.select_all(action="SELECT")
for element in dictionnaire:
    bpy.data.objects[element[0]].select_set(False)
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()


# horizontals patterns = left hand
for i in range(5):
    current_y = 0
    z = i * 2.2
    print(z)
    # position in the cube
    while current_y < 8.5:
        current_x = -4.5
        y = current_y
        # patterns generate in z
        while current_x < 1.75:
            x = current_x
            nom, larg = dictionnaire[randint(0, len(dictionnaire) - 4)]
            current_x = current_x + 0.75 * larg  # space between the patterns in y
            duplicate(nom, (x, y, z))
        # range duplicate in z
        current_y = current_y + 3  # space between the patterns in z


# verticals patterns = right hand
for i in range(2):
    current_y = 3.5
    z = i
    print(z)
    # position in the cube
    while current_y < 10:
        current_x = -4.5
        y = current_y
        while current_x < 5:
            x = current_x
            nom, larg = dictionnaire[randint(3, len(dictionnaire) - 1)]
            current_x = current_x + 0.5 * larg  # space between the patterns in x
            duplicate(nom, (x, y, z * larg))
        current_y = current_y + 2.75  # space between the patterns in y


# Boolean : intersect
bpy.ops.object.select_all(action="SELECT")
for element in dictionnaire:
    bpy.data.objects[element[0]].select_set(False)
tousMesObjets = bpy.context.selected_objects[:]
print("tous mes objets cest", tousMesObjets)
bpy.ops.object.select_all(action="DESELECT")


# creation mesh
# selection all of "spag"
for spag in tousMesObjets:
    spag.select_set(True)
    # activation "spag"
    bpy.context.view_layer.objects.active = spag
    # convertion in mesh
    bpy.ops.object.convert(target="MESH")
    bpy.ops.object.select_all(action="DESELECT")

# cube location
bpy.ops.mesh.primitive_cube_add(location=(0, 5, 5), scale=(5, 5, 5))
Cube = get_objects("Cube")[0]
bpy.ops.object.select_all(action="DESELECT")
print("mon dictionnaire cest", dictionnaire)

# Make and apply intersection and difference
multi_boolean(tousMesObjets, Cube, "INTERSECT")


for item in tousMesObjets:
    apply_modifiers(item)


bpy.ops.object.select_all(action="DESELECT")
Cube.select_set(True)
bpy.ops.object.delete(use_global=False)
