# SPDX-FileCopyrightText: 2013 Elzbieta Liguz
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# blender 2.68a / MS windows
# projet pour 23/01/2014

import bpy
import random

# Scene
Scene = bpy.context.scene


# Couleur - ROUGE
ROUGE = bpy.data.materials.new("Rouge")
ROUGE.diffuse_color = (1, 0, 0)
ROUGE.diffuse_intensity = 0.2
ROUGE.specular_color = (1, 1, 1)
ROUGE.specular_intensity = 0.5
ROUGE.alpha = 1
ROUGE.ambient = 1

# --------------------------------------------------
# Couleur - BLANC
BLANC = bpy.data.materials.new("Blanc")
BLANC.diffuse_color = (1, 1, 1)
BLANC.diffuse_intensity = 1
BLANC.specular_color = (1, 1, 1)
BLANC.specular_intensity = 1
BLANC.alpha = 1
BLANC.ambient = 1

# --------------------------------------------------
# Couleur - BLANC_1
BLANC_1 = bpy.data.materials.new("Blanc_1")
BLANC_1.diffuse_color = (1, 1, 1)
BLANC_1.diffuse_intensity = 0.5
BLANC_1.specular_color = (1, 1, 1)
BLANC_1.specular_intensity = 0.5
BLANC_1.alpha = 0.5
BLANC_1.ambient = 0.5


# -----------------------------------------------------------------------
# Couleur - BLUE
BLUE = bpy.data.materials.new("Blue")
BLUE.diffuse_color = (0, 0, 0.9)
BLUE.diffuse_intensity = 1
BLUE.specular_color = (1, 1, 1)
BLUE.specular_intensity = 0.5
BLUE.alpha = 1
BLUE.ambient = 1
# -----------------------------------------------------------------------

# Couleur - VERT
VERT = bpy.data.materials.new("Vert")
VERT.diffuse_color = (0, 1, 0)
VERT.diffuse_intensity = 0.3
VERT.specular_color = (1, 1, 1)
VERT.specular_intensity = 0.5
VERT.alpha = 1
VERT.ambient = 1
# -----------------------------------------------------------------------


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

Batiment = random.choice(["musee", "logement"])
print("La Batiment est un", Batiment)


L_Toute_Proportion = [(4, 2), (4, 3), (4, 4)]
L_Proportion = L_Toute_Proportion[random.randint(0, 2)]

L_Toute_Taille = [8, 10, 12, 20]


L_Toute_Circulation = [(1, 0), (1, 2), (2, 0)]
L_Circulation = L_Toute_Circulation[random.randint(0, 2)]

L_Toute_Interieur = [2, 4, 3]
L_Interieur = L_Toute_Interieur[random.randint(0, 2)]

L_Toute_Hauteur = [3, 8]

if Batiment == "musee":
    L_Taille = L_Toute_Taille[3]
    H = L_Toute_Hauteur[1]
    Etage = 2
    Z_AXIS = [0, 50, 100]
if Batiment == "logement":
    L_Taille = L_Toute_Taille[random.randint(0, 2)]
    H = L_Toute_Hauteur[0]
    Etage = random.randint(2, 4)
    Z_AXIS = [0, 20, 40, 60, 80, 100]


for NumberOfFloor in range(0, Etage):
    pos_x, pos_y, pos_z = 0, 0, Z_AXIS[NumberOfFloor]

    p_x, p_y, p_z = (
        L_Proportion[0] * L_Taille / 2 + pos_x,
        L_Proportion[1] * L_Taille / 2 + pos_y,
        H + pos_z,
    )

    L_Coordonne = (
        (-p_x, -p_y, pos_z),
        (-p_x, p_y, pos_z),
        (p_x, p_y, pos_z),
        (p_x, -p_y, pos_z),
    )

    Vertices_1 = [(0, 1, 2, 3)]

    Surface = bpy.data.meshes.new("Niveau 0 mesh")
    Niveau = bpy.data.objects.new("Niveau 0", Surface)
    Scene.objects.link(Niveau)
    Surface.from_pydata(L_Coordonne, [], Vertices_1)
    Surface.materials.append(BLANC_1)

    def MaSurface(p_x, p_y, p_z, Colour):

        Coordonne_principal = (
            (0, 0, 0),
            (-p_x, -p_y, pos_z),
            (-p_x, p_y, pos_z),
            (p_x, p_y, pos_z),
            (p_x, -p_y, pos_z),
            (-p_x, -p_y, p_z),
            (-p_x, p_y, p_z),
            (p_x, p_y, p_z),
            (p_x, -p_y, p_z),
        )
        Vertices = (1, 2, 6, 5), (2, 3, 7, 6), (3, 4, 8, 7), (1, 4, 8, 5)
        Surface = bpy.data.meshes.new("Murs mesh")
        Niveau = bpy.data.objects.new("Murs", Surface)
        Scene.objects.link(Niveau)
        Surface.from_pydata(Coordonne_principal, [], Vertices)
        Surface.materials.append(Colour)

    MaSurface(
        L_Proportion[0] * L_Taille / 2 + pos_x,
        L_Proportion[1] * L_Taille / 2 + pos_y,
        H + pos_z,
        BLANC,
    )
    MaSurface(
        L_Proportion[0] * L_Taille / 8, L_Proportion[1] * L_Taille / 8, H + pos_z, ROUGE
    )

    def MaCirculation(p_x, p_y, p_z, Colour):
        Coordonne_principal = (
            (0, 0, 0),
            (-p_x, -p_y, pos_z),
            (-p_x * 5 / 6, -p_y, pos_z),
            (-p_x, -p_y / 4, pos_z),
            (-p_x * 5 / 6, -p_y / 4, pos_z),
            (-p_x, -p_y, p_z),
            (-p_x * 5 / 6, -p_y, p_z),
            (-p_x, -p_y / 4, p_z),
            (-p_x * 5 / 6, -p_y / 4, p_z),
        )
        Vertices = (4, 3, 7, 8), (2, 4, 8, 6)
        Surface = bpy.data.meshes.new("Murs mesh")
        Niveau = bpy.data.objects.new("Murs", Surface)
        Scene.objects.link(Niveau)
        Surface.from_pydata(Coordonne_principal, [], Vertices)
        Surface.materials.append(Colour)

    def MaInterieur(p_x, p_y, p_z, v_1, v_2, v_3, v_4, Colour):
        Coordonne_interieur = (
            (0, 0, 0),
            (-p_x, -p_y, pos_z),
            (-p_x, p_y, pos_z),
            (p_x, p_y, pos_z),
            (p_x, -p_y, pos_z),
            (-p_x, -p_y, p_z),
            (-p_x, p_y, p_z),
            (p_x, p_y, p_z),
            (p_x, -p_y, p_z),
            (-p_x, -p_y * 4, pos_z),
            (-p_x * 4, -p_y, pos_z),
            (-p_x * 4, p_y, pos_z),
            (-p_x, p_y * 4, pos_z),
            (p_x, p_y * 4, pos_z),
            (p_x * 4, p_y, pos_z),
            (p_x * 4, -p_y, pos_z),
            (p_x, -p_y * 4, pos_z),
            (-p_x, -p_y * 4, p_z),
            (-p_x * 4, -p_y, p_z),
            (-p_x * 4, p_y, p_z),
            (-p_x, p_y * 4, p_z),
            (p_x, p_y * 4, p_z),
            (p_x * 4, p_y, p_z),
            (p_x * 4, -p_y, p_z),
            (p_x, -p_y * 4, p_z),
            (-p_x, 0, pos_z),
            (0, p_y, pos_z),
            (p_x, 0, pos_z),
            (0, -p_y, pos_z),
            (-p_x, 0, p_z),
            (0, p_y, p_z),
            (p_x, 0, p_z),
            (0, -p_y, p_z),
        )
        Vertices = ((v_1, v_2, v_3, v_4),)
        Surface = bpy.data.meshes.new("Murs mesh")
        Niveau = bpy.data.objects.new("Murs", Surface)
        Scene.objects.link(Niveau)
        Surface.from_pydata(Coordonne_interieur, [], Vertices)
        Surface.materials.append(Colour)

    MaInterieur(
        L_Proportion[0] * L_Taille / 8,
        L_Proportion[1] * L_Taille / 8,
        H + pos_z,
        2,
        6,
        20,
        12,
        VERT,
    )
    MaInterieur(
        L_Proportion[0] * L_Taille / 8,
        L_Proportion[1] * L_Taille / 8,
        H + pos_z,
        16,
        24,
        8,
        4,
        VERT,
    )

    def MaDivision(
        p_x, p_y, p_z, p_x_1, p_y_1, p_z_1, v_1, v_2, v_3, v_4, x_a, y_a, Colour
    ):
        Coordonne_division = (
            (0, 0, 0),
            (-p_x, -p_y, pos_z),
            (-p_x, p_y, pos_z),
            (p_x, p_y, pos_z),
            (p_x, -p_y, pos_z),
            (-p_x, -p_y, p_z),
            (-p_x, p_y, p_z),
            (p_x, p_y, p_z),
            (p_x, -p_y, p_z),  # 5,6,7,8
            (-p_x, 0, pos_z),
            (0, p_y, pos_z),
            (p_x, 0, pos_z),
            (0, -p_y, pos_z),  # 9,10,11,12
            (-p_x, 0, p_z),
            (0, p_y, p_z),
            (p_x, 0, p_z),
            (0, -p_y, p_z),  # 13,14,15,16
            (-p_x, y_a, pos_z),
            (x_a, p_y, pos_z),
            (p_x, y_a, pos_z),
            (x_a, -p_y, pos_z),  # 17,18,19,20
            (-p_x, y_a, p_z),
            (x_a, p_y, p_z),
            (p_x, y_a, p_z),
            (x_a, -p_y, p_z),  # 21,22,23,24
            (-p_x_1, -p_y_1, pos_z),
            (-p_x_1, p_y_1, pos_z),
            (p_x_1, p_y_1, pos_z),
            (p_x_1, -p_y_1, pos_z),  # 25,26,27,28
            (-p_x_1, -p_y_1, p_z_1),
            (-p_x_1, p_y_1, p_z_1),
            (p_x_1, p_y_1, p_z_1),
            (p_x_1, -p_y_1, p_z_1),  # 29,30,31,32
            (-p_x_1, 0, pos_z),
            (0, p_y_1, pos_z),
            (p_x_1, 0, pos_z),
            (0, -p_y_1, pos_z),  # 33,34,35,36
            (-p_x_1, 0, p_z_1),
            (0, p_y_1, p_z_1),
            (p_x_1, 0, p_z_1),
            (0, -p_y_1, p_z_1),  # 37,38,39,40
            (-p_x_1, -p_y_1 + y_a, pos_z),
            (-p_x_1 + x_a, p_y_1, pos_z),
            (p_x_1, p_y_1 + y_a, pos_z),
            (p_x_1 + x_a, -p_y_1, pos_z),
            (-p_x_1, -p_y_1 + y_a, p_z_1),
            (-p_x_1 + x_a, p_y_1, p_z_1),
            (p_x_1, p_y_1 + y_a, p_z_1),
            (p_x_1 + x_a, -p_y_1, p_z_1),  # 45,46,47,48,
            (-p_x + x_a, y_a, pos_z),
            (x_a, p_y + y_a, pos_z),
            (p_x + x_a, y_a, pos_z),
            (x_a, -p_y + y_a, pos_z),  # 49,50,51,52
            (-p_x + x_a, y_a, p_z),
            (x_a, p_y + y_a, p_z),
            (p_x + x_a, y_a, p_z),
            (x_a, -p_y + y_a, p_z),
        )  # 53,54,55,56
        Vertices = ((v_1, v_2, v_3, v_4),)
        Surface = bpy.data.meshes.new("Murs mesh")
        Niveau = bpy.data.objects.new("Murs", Surface)
        Scene.objects.link(Niveau)
        Surface.from_pydata(Coordonne_division, [], Vertices)
        Surface.materials.append(Colour)

    if L_Interieur == 2:

        L_Toute_Division = [
            (1, 1),
            (2, 1),
            (1, 2),
            (2, 2),
            (2, 3),
            (3, 2),
            (3, 1),
            (1, 3),
            (3, 3),
        ]
        L_Division = L_Toute_Division[random.randint(0, 8)]

        if L_Division[0] == 1:

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                33,
                37,
                13,
                9,
                0,
                0,
                BLUE,
            )

        if L_Division[1] == 1:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                34,
                38,
                31,
                27,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                27,
                31,
                39,
                35,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                35,
                39,
                15,
                11,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                34,
                38,
                14,
                10,
                0,
                0,
                BLUE,
            )

        if L_Division[0] == 2:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4 - L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            # les murs principales verticales
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

            # les murs principales horizontales

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                33,
                37,
                13,
                9,
                0,
                0,
                BLUE,
            )

            # le mur horizontal long
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 2 - L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 3,
                BLUE,
            )

        if L_Division[1] == 2:

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4 + L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            # les murs principales verticales
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

            # les murs principales horizontales

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                33,
                37,
                13,
                9,
                0,
                0,
                BLUE,
            )

            # le mur horizontal long
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 2 + L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / H,
                BLUE,
            )

        if L_Division[0] == 3:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4 - L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            # les murs principales verticales
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

            # les murs principales horizontales

            # le mur horizontal long
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 2 - L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 3,
                BLUE,
            )

            # Les murs aditional
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 6,
                BLUE,
            )

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

        if L_Division[1] == 3:
            # Duxieme partie du batiment
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                +L_Proportion[1] * L_Taille / 4 + L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 2 + L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 3,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                +L_Proportion[1] * L_Taille / 6,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                +L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

    if L_Interieur == 4:

        L_Toute_Division = [
            (0, 0, 0, 0),
            (1, 1, 1, 1),
            (1, 1, 1, 2),
            (1, 1, 2, 1),
            (1, 2, 1, 2),
            (2, 2, 1, 1),
            (2, 2, 2, 1),
            (2, 2, 2, 2),
        ]
        L_Division = L_Toute_Division[random.randint(0, 6)]

        MaInterieur(
            L_Proportion[0] * L_Taille / 8,
            L_Proportion[1] * L_Taille / 8,
            H + pos_z,
            10,
            18,
            5,
            1,
            VERT,
        )
        MaInterieur(
            L_Proportion[0] * L_Taille / 8,
            L_Proportion[1] * L_Taille / 8,
            H + pos_z,
            3,
            7,
            22,
            14,
            VERT,
        )

        if L_Circulation[1] == 2:
            MaCirculation(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                ROUGE,
            )
            MaCirculation(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                ROUGE,
            )

        if L_Division[0] == 1:
            MaSurface(
                L_Proportion[0] * L_Taille / 4 + pos_x,
                L_Proportion[1] * L_Taille / 4 + pos_y,
                H + pos_z,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
        if L_Division[1] == 1:
            MaSurface(
                L_Proportion[0] * L_Taille / 4 + pos_x,
                L_Proportion[1] * L_Taille / 4 + pos_y,
                H + pos_z,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
        if L_Division[2] == 1:
            MaSurface(
                L_Proportion[0] * L_Taille / 4 + pos_x,
                L_Proportion[1] * L_Taille / 4 + pos_y,
                H + pos_z,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
        if L_Division[3] == 1:
            MaSurface(
                L_Proportion[0] * L_Taille / 4 + pos_x,
                L_Proportion[1] * L_Taille / 4 + pos_y,
                H + pos_z,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                +L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

        if L_Division[0] == 2:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 16,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

        if L_Division[1] == 2:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 16,
                BLUE,
            )

        if L_Division[2] == 2:
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 16,
                BLUE,
            )
        if L_Division[3] == 2:
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                +L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                +L_Proportion[0] * L_Taille / 16,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

    if L_Interieur == 3:

        L_Toute_Division = [(2, 2, 1), (2, 2, 2), (2, 2, 3)]
        L_Division = L_Toute_Division[random.randint(0, 2)]

        MaInterieur(
            L_Proportion[0] * L_Taille / 8,
            L_Proportion[1] * L_Taille / 8,
            H + pos_z,
            10,
            18,
            5,
            1,
            VERT,
        )
        if L_Division[0] == 1:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 4,
                ROUGE,
            )

        if L_Division[0] == 2:

            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                -L_Proportion[0] * L_Taille / 6,
                L_Proportion[1] * L_Taille / 4,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 2 - L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 3,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

        if L_Division[1] == 1:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
        if L_Division[1] == 2:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 16,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4 - L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )

        if L_Division[2] == 1:
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                34,
                38,
                31,
                27,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                27,
                31,
                39,
                35,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                35,
                39,
                15,
                11,
                0,
                0,
                BLUE,
            )
            MaDivision(
                L_Proportion[0] * L_Taille / 2,
                L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                L_Proportion[0] * L_Taille / 8,
                L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                34,
                38,
                14,
                10,
                0,
                0,
                BLUE,
            )

        if L_Division[2] == 2:

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                L_Proportion[1] * L_Taille / 4 + L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                33,
                37,
                13,
                9,
                0,
                0,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 2 + L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 3,
                BLUE,
            )

        if L_Division[2] == 3:

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                36,
                40,
                29,
                25,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                +L_Proportion[1] * L_Taille / 4 + L_Proportion[1] * L_Taille / 12,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 4,
                H + pos_z,
                25,
                29,
                37,
                33,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 8,
                H + pos_z,
                36,
                40,
                16,
                12,
                0,
                0,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 2 + L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 3,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                -L_Proportion[1] * L_Taille / 12,
                BLUE,
            )
            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 8,
                -L_Proportion[0] * L_Taille / 2,
                H + pos_z,
                49,
                53,
                21,
                17,
                -L_Proportion[0] * L_Taille / 4,
                +L_Proportion[1] * L_Taille / 6,
                BLUE,
            )

            MaDivision(
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[1] * L_Taille / 2,
                H + pos_z,
                -L_Proportion[0] * L_Taille / 2,
                -L_Proportion[0] * L_Taille / 8,
                H + pos_z,
                52,
                56,
                24,
                20,
                +L_Proportion[0] * L_Taille / 8,
                -L_Proportion[1] * L_Taille / 4,
                BLUE,
            )

    print("******************")
    print(L_Proportion)
    print(L_Taille)
    print(H)
    print(L_Circulation)
    print(L_Interieur)
    print(L_Division)

print(Etage)
print(
    "******************************************************************************************************************************************"
)
