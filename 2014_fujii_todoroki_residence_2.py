# SPDX-FileCopyrightText: 2014 David Facho Santos
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Blender 2.71 - MACOS - 9337574
# Script David Facho Santos-22/01/2015
# HIROMI FUHII

# ----------------------------------------------------------------------------------------

### DEFAULT ###

import bpy
import random
from math import pi

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

scene = bpy.context.scene

# ----------------------------------------------------------------------------------------

### ARBRE DES DECISIONS ###

# Longueur, largeur, hauteur d'un cube

VAL_X = 1  # Largeur d'un Cube
VAL_Y = 1  # Profondeur d'un Cube
VAL_Z = 1  # Hauteur d'un Cube

# Réduction des cubes

RED_L = 1.00
RED_M = 0.80
RED_S = 0.60

# Epaisseurs des cloisons

EPA_CLO = 0.05

# Quantité de cubes

CON_HAU = 2  # en hauteur
CON_LON = 2  # en longueur
CON_LAR = 2  # en largeur

# ----------------------------------------------------------------------------------------

# Réduction des cubes

VAL_X_L, VAL_Y_L, VAL_Z_L = (
    VAL_X * RED_L * 0.99,
    VAL_Y * RED_L * 0.99,
    VAL_Z * RED_L * 0.99,
)  # Réduction du cube L
VAL_X_M, VAL_Y_M, VAL_Z_M = (
    VAL_X * RED_M * 0.99,
    VAL_Y * RED_M * 0.99,
    VAL_Z * RED_M * 0.99,
)  # Réduction du cube M
VAL_X_S, VAL_Y_S, VAL_Z_S = (
    VAL_X * RED_S * 0.99,
    VAL_Y * RED_S * 0.99,
    VAL_Z * RED_S * 0.99,
)  # Réduction du cube S


# Verification si contraint hauteur

if CON_HAU == 0:
    HAS_HAU = random.randint(1, 4)
else:
    HAS_HAU = CON_HAU  # CONTRAINTE

# Verification si contraint longueur

if CON_LON == 0:
    HAS_LON = random.randint(2, 3)
else:
    HAS_LON = CON_LON  # CONTRAINTE

# Verification si contraint largeur

if CON_LAR == 0:
    HAS_LAR = random.randint(1, 2)
else:
    HAS_LAR = CON_LAR  # CONTRAINTE


I_REEL = 0
I_REEL = I_REEL + 1

# ----------------------------------------------------------------------------------------

### DEFINITION DES OBJETS ###


def CubeL(a, b, c):
    bpy.ops.mesh.primitive_cube_add(location=(0, EPA_CLO / 2, c / 2))
    bpy.ops.transform.translate(value=((a - EPA_CLO) / 2, 0, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(0, EPA_CLO / 2, c / 2))
    bpy.ops.transform.translate(value=(a - EPA_CLO / 2, b / 2 - EPA_CLO, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(0, -EPA_CLO / 2, c / 2))
    bpy.ops.transform.translate(value=((a + EPA_CLO) / 2, b, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(0, EPA_CLO / 2, c / 2))
    bpy.ops.transform.translate(value=(EPA_CLO / 2, b / 2, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(0, EPA_CLO / 2, c / 2))
    bpy.ops.transform.translate(value=(a / 2, (b - EPA_CLO) / 2, (c - EPA_CLO) / 2))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(1, 0, 0), mirror=False)
    bpy.context.object.scale[0] = (a) / 2 - EPA_CLO
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2 - EPA_CLO
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    for ob in bpy.context.scene.objects:
        ob.select = ob.name.startswith(
            "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)
        )
    bpy.ops.object.join()


def A():
    CubeL(VAL_X_L, VAL_Y_L, VAL_Z_L)


def CubeM(a, b, c, d, e, f):
    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=((a - EPA_CLO) / 2, 0, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(a - EPA_CLO / 2, b / 2 - EPA_CLO, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e - EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=((a + EPA_CLO) / 2, b, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(EPA_CLO / 2, b / 2, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(a / 2, (b - EPA_CLO) / 2, (c - EPA_CLO) / 2))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(1, 0, 0), mirror=False)
    bpy.context.object.scale[0] = (a) / 2 - EPA_CLO
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2 - EPA_CLO
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    for ob in bpy.context.scene.objects:
        ob.select = ob.name.startswith(
            "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)
        )
    bpy.ops.object.join()


def B():
    if x == 1:  # 1. QUADRANT CUBE S
        CubeM(VAL_X_M, VAL_Y_M, VAL_Z_M, 0, 0, 0)
    elif x == 2:  # 2. QUADRANT CUBE S
        CubeM(VAL_X_M, VAL_Y_M, VAL_Z_M, VAL_X_L - VAL_X_M, 0, 0)
    elif x == 3:  # 3. QUADRANT CUBE S
        CubeM(VAL_X_M, VAL_Y_M, VAL_Z_M, VAL_X_L - VAL_X_M, VAL_Y_L - VAL_Y_M, 0)
    elif x == 4:  # 4. QUADRANT CUBE S
        CubeM(VAL_X_M, VAL_Y_M, VAL_Z_M, 0, VAL_X_L - VAL_Y_M, 0)


def CubeS(a, b, c, d, e, f):
    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=((a - EPA_CLO) / 2, 0, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(a - EPA_CLO / 2, b / 2 - EPA_CLO, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e - EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=((a + EPA_CLO) / 2, b, 0))
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(EPA_CLO / 2, b / 2, 0))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(0, 0, 1), mirror=False)
    bpy.context.object.scale[0] = (a - EPA_CLO) / 2
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    bpy.ops.mesh.primitive_cube_add(location=(d + 0, e + EPA_CLO / 2, f + c / 2))
    bpy.ops.transform.translate(value=(a / 2, (b - EPA_CLO) / 2, (c - EPA_CLO) / 2))
    bpy.ops.transform.rotate(value=-pi / 2, axis=(1, 0, 0), mirror=False)
    bpy.context.object.scale[0] = (a) / 2 - EPA_CLO
    bpy.context.object.scale[1] = EPA_CLO / 2
    bpy.context.object.scale[2] = c / 2 - EPA_CLO
    bpy.context.object.name = "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)

    for ob in bpy.context.scene.objects:
        ob.select = ob.name.startswith(
            "Boite_x" + str(i) + "_y" + str(j) + "_z" + str(k)
        )
    bpy.ops.object.join()


def C():
    if x == 1:  # 1. QUADRANT CUBE S
        if y == 1:  # 1. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, 0, 0, 0)
        elif y == 2:  # 2. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_M - VAL_X_S, 0, 0)
        elif y == 3:  # 3. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_M - VAL_X_S, VAL_Y_M - VAL_Y_S, 0)
        elif y == 4:  # 4. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, 0, VAL_X_M - VAL_Y_S, 0)

    elif x == 2:  # 2. QUADRANT CUBE S
        if y == 1:  # 1. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_M, 0, 0)
        elif y == 2:  # 2. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_S, 0, 0)
        elif y == 3:  # 3. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_S, VAL_Y_M - VAL_Y_S, 0)
        elif y == 4:  # 4. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_M, VAL_X_M - VAL_Y_S, 0)

    elif x == 3:  # 3. QUADRANT CUBE S
        if y == 1:  # 1. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_M, VAL_Y_L - VAL_Y_M, 0)
        elif y == 2:  # 2. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_S, VAL_Y_L - VAL_Y_M, 0)
        elif y == 3:  # 3. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_S, VAL_Y_L - VAL_Y_S, 0)
        elif y == 4:  # 4. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_L - VAL_X_M, VAL_X_L - VAL_Y_S, 0)

    elif x == 4:  # 4. QUADRANT CUBE S
        if y == 1:  # 1. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, 0, VAL_X_L - VAL_Y_M, 0)
        elif y == 2:  # 2. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_M - VAL_X_S, VAL_Y_L - VAL_Y_M, 0)
        elif y == 3:  # 3. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, VAL_X_M - VAL_X_S, VAL_Y_L - VAL_Y_S, 0)
        elif y == 4:  # 4. QUADRANT CUBE S
            CubeS(VAL_X_S, VAL_Y_S, VAL_Z_S, 0, VAL_Y_L - VAL_Y_S, 0)


def UNITE(jj, ii, kk):
    A()
    B()
    C()
    bpy.ops.transform.translate(value=(jj * RED_L, ii * RED_L, kk * RED_L))


def Boolean(x):
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[x]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")


def Delete(x):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects[x].select = True
    bpy.ops.object.delete(use_global=False)


def Cube(a, b, c, d, e, f):
    bpy.ops.mesh.primitive_cube_add(radius=0.5)
    bpy.ops.transform.translate(value=(a, b, c))
    bpy.ops.transform.resize(value=(d, e, f))


def Fenetre(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    Cube(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z)
    bpy.context.object.name = "Négatif"


def Ouverture(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    Cube(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z)
    bpy.context.object.name = "Négatif"


def Camera():
    bpy.ops.object.camera_add(
        view_align=True, location=(-25, -25, -35), rotation=(2.3562, 0, -0.785398)
    )
    bpy.context.object.data.ortho_scale = 6.6
    bpy.context.object.data.type = "ORTHO"
    bpy.context.scene.world.ambient_color = (1, 1, 1)
    bpy.context.scene.render.resolution_x = 4000
    bpy.context.scene.render.resolution_y = 3000
    # bpy.context.scene.select_contour = True
    bpy.context.scene.render.use_freestyle = True
    bpy.context.scene.render.line_thickness = 1.2


# ----------------------------------------------------------------------------------------

### GENERATEUR DES FORMATIONS ###

for k in range(0, CON_HAU):
    for j in range(0, CON_LON):
        for i in range(0, CON_LAR):
            x = random.randint(0, 4)
            if x == 1 or x == 2 or x == 3 or x == 4:
                x = random.randint(0, 4)
            y = random.randint(0, 4)
            if y == 1 or y == 2 or y == 3 or y == 4:
                y = random.randint(0, 4)
                if y == 1 or y == 2 or y == 3 or y == 4:
                    y = random.randint(0, 4)
                    if y == 1 or y == 2 or y == 3 or y == 4:
                        y = random.randint(0, 4)
            # x = 1
            # y = 1
            I_REEL = I_REEL + 1

            ### FENETRES ###

            if i == 0:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2,
                        EPA_CLO * 0.5,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        j + VAL_X_L / 2,
                        EPA_CLO * 0.5,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        j + VAL_X_L / 2,
                        EPA_CLO * 0.5,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )

            elif i == CON_LAR - 1:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2,
                        CON_LAR * VAL_Y_L,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        j + VAL_X_L / 2,
                        CON_LAR * VAL_Y_L,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        j + VAL_X_L / 2,
                        CON_LAR * VAL_Y_L,
                        k + VAL_Z_L / 2,
                        0.6,
                        EPA_CLO * 2.2,
                        0.55,
                    )

            elif j == 0:
                if x == 0:
                    Fenetre(
                        EPA_CLO * 1.2 / 2,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        EPA_CLO * 1.2 / 2,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        EPA_CLO * 1.2 / 2,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )

            elif j == CON_LON - 1:
                if x == 0:
                    Fenetre(
                        CON_LON * VAL_X_L,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        CON_LON * VAL_X_L,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        CON_LON * VAL_X_L,
                        i + VAL_Y_L / 2,
                        k + VAL_Z_L / 2,
                        EPA_CLO * 2.2,
                        0.6,
                        0.55,
                    )

            elif i < 1:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )

            elif i == CON_LAR - 1:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )

            elif i >= 1 and i < CON_LAR - 1:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + VAL_Y_L - EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        j + VAL_X_L / 2,
                        i + EPA_CLO / 2,
                        k + 0.6 / 2.2,
                        0.48,
                        EPA_CLO * 1.2,
                        0.55,
                    )

            elif j < 1:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        j + VAL_X_L - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        j + VAL_X_L - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )

            elif j == CON_LON - 1:
                if x == 0:
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )

            elif j >= 1 and j < CON_LON - 2:
                if x == 0:
                    Fenetre(
                        j + VAL_X_L / 2 - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.7 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 1:
                    Fenetre(
                        j + VAL_X_L - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 2:
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 3:
                    Fenetre(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )
                if x == 4:
                    Fenetre(
                        j + VAL_X_L - EPA_CLO / 2,
                        i + VAL_Y_L / 2,
                        k + 0.6 / 2.2,
                        EPA_CLO * 1.2,
                        0.48,
                        0.55,
                    )

            ### OUVERTURES A L'INTERIEUR ###

            if i < 1:
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_M + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )

            if i >= 1 and i < CON_LAR - 1:
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )

                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2.05,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.44 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.44 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_M + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )

            if i == CON_LAR - 1:
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.45,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )

                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_M - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.4,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 + 0.25 - 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_M + VAL_Y_S - EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S / 2 - 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L / 2 - 0.25 + 0.2,
                            i + VAL_Y_L - VAL_Y_M + EPA_CLO / 2,
                            k + 0.48 / 2.2,
                            0.40,
                            EPA_CLO * 1.2,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S / 2 + 0.03,
                            i + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            k + 0.45 / 2.2,
                            0.38,
                            EPA_CLO * 1.2,
                            0.42,
                        )

            if j < 1:
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_Y_L + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_Y_M + VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )

            if j >= 1 and j < CON_LON - 2:
                if y == 0:
                    Ouverture(
                        j + EPA_CLO / 2,
                        i + VAL_Y_L / 2 - 0.25 + 0.2,
                        k + 0.48 / 2.2,
                        EPA_CLO * 1.2,
                        0.45,
                        0.45,
                    )
                    Ouverture(
                        j + VAL_X_M - EPA_CLO / 2,
                        i + VAL_Y_L / 2 - 0.25 + 0.2,
                        k + 0.48 / 2.2,
                        EPA_CLO * 1.2,
                        0.45,
                        0.45,
                    )
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.04,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_S - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_Y_L - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_Y_M + VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                    elif y == 4:  # 4. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )

            if j == CON_LON - 1:
                if x == 1:  # 1. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_Y_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.04,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - VAL_X_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 4:
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_S - EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 2:  # 2. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_S / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 - 0.25 + 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 3:  # 3. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + VAL_X_L - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_L - VAL_X_M + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_L - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                elif x == 4:  # 4. QUADRANT CUBE M
                    if y == 0:
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.45,
                            0.45,
                        )
                    elif y == 1:  # 1. QUADRANT CUBE S
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_Y_M + VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 2:  # 2. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_M - VAL_Y_S + EPA_CLO / 2,
                            i + VAL_X_L / 2 + 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )
                    elif y == 3:  # 3. QUADRANT CUBE S
                        Ouverture(
                            j + EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                    elif y == 4:
                        Ouverture(
                            j + VAL_X_M - EPA_CLO / 2,
                            i + VAL_Y_L / 2 + 0.25 - 0.2,
                            k + 0.48 / 2.2,
                            EPA_CLO * 1.2,
                            0.40,
                            0.45,
                        )
                        Ouverture(
                            j + VAL_Y_S - EPA_CLO / 2,
                            i + VAL_X_L - VAL_X_S / 2 - 0.03,
                            k + 0.45 / 2.2,
                            EPA_CLO * 1.2,
                            0.38,
                            0.42,
                        )

            for ob in scene.objects:
                if ob.type == "MESH" and ob.name.startswith("Négatif"):
                    ob.select = True
                bpy.ops.object.join()

            UNITE(j, i, k)

for ob in scene.objects:
    if ob.type == "MESH" and ob.name.startswith("Boite"):
        ob.select = True

bpy.ops.object.join()
bpy.context.object.name = "Unité"


### EFFACEMENT DES OUVERTURES ###

for ob in scene.objects:
    if ob.name == ("Négatif"):
        Boolean("Négatif")
        Delete("Négatif")
    if ob.name.startswith("Négatif.001"):
        Boolean("Négatif.001")
        Delete("Négatif.001")
    if ob.name.startswith("Négatif.002"):
        Boolean("Négatif.002")
        Delete("Négatif.002")
    if ob.name.startswith("Négatif.003"):
        Boolean("Négatif.003")
        Delete("Négatif.003")
    if ob.name.startswith("Négatif.004"):
        Boolean("Négatif.004")
        Delete("Négatif.004")
    if ob.name.startswith("Négatif.005"):
        Boolean("Négatif.005")
        Delete("Négatif.005")
    if ob.name.startswith("Négatif.006"):
        Boolean("Négatif.006")
        Delete("Négatif.006")
    if ob.name.startswith("Négatif.007"):
        Boolean("Négatif.007")
        Delete("Négatif.007")
    if ob.name.startswith("Négatif.008"):
        Boolean("Négatif.008")
        Delete("Négatif.008")


# Camera()
