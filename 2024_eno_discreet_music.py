# SPDX-FileCopyrightText: 2025 Takashi Miyase & Julien Rialland
# SPDX-FileCopyrightText: 2025 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# python Blender exercise for 21.11.2024 course
# developed under Blender 4.1.0 / Windows11

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["L1"].select_set(False)
bpy.data.objects["L2"].select_set(False)
bpy.data.objects["L3"].select_set(False)
bpy.data.objects["L4"].select_set(False)
bpy.data.objects["R1"].select_set(False)
bpy.data.objects["R2"].select_set(False)
bpy.data.objects["R3"].select_set(False)
bpy.data.objects["R4"].select_set(False)
bpy.data.objects["LL1"].select_set(False)
bpy.data.objects["LL2"].select_set(False)
bpy.data.objects["LL3"].select_set(False)
bpy.data.objects["LL4"].select_set(False)
bpy.data.objects["RR1"].select_set(False)
bpy.data.objects["RR2"].select_set(False)
bpy.data.objects["RR3"].select_set(False)
bpy.data.objects["RR4"].select_set(False)
bpy.data.objects["Camera"].select_set(False)
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()


# defining code that copys cube and change the scale
def decay(position, scale):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"mode": "TRANSLATION"},
        TRANSFORM_OT_translate={"value": position},
    )
    bpy.ops.transform.resize(value=scale)


# Represent Original Sounds--------------------------------------------------------------------------------------------------------
# L1
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["L1"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 9)
    decay((36 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"L1.{y*12+1:03d}"].select_set(True)

# L2
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["L2"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 9)
    decay((36 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"L2.{y*12+1:03d}"].select_set(True)

# L3
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["L3"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 9)
    decay((36 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"L3.{y*12+1:03d}"].select_set(True)

# L4
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["L4"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 9)
    decay((36 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"L4.{y*12+1:03d}"].select_set(True)

# R1
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["R1"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 8)
    decay((39 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"R1.{y*13+1:03d}"].select_set(True)

# R2
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["R2"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 8)
    decay((39 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"R2.{y*13+1:03d}"].select_set(True)

# R3
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["R3"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 8)
    decay((39 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"R3.{y*13+1:03d}"].select_set(True)

# R4
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["R4"].select_set(True)
    Nx = random.randint(1, 4)
    Ny = random.randint(2, 5)
    Nz = random.randint(2, 5)
    Nd = random.randint(1, 8)
    decay((39 * y, 0, 0), (1, 0.5 * Ny, 0.5 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"R4.{y*13+1:03d}"].select_set(True)

# Represent Echo----------------------------------------------------------------------------------------------------------------------
# L1
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["LL1"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((36 * y, N1 * -2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"LL1.{y*12+1:03d}"].select_set(True)

# L2
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["LL2"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((36 * y, N1 * -2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"LL2.{y*12+1:03d}"].select_set(True)

# L3
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["LL3"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((36 * y, N1 * -2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"LL3.{y*12+1:03d}"].select_set(True)

# L4
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["LL4"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((36 * y, N1 * -2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 12):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"LL4.{y*12+1:03d}"].select_set(True)

# R1
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["RR1"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((39 * y, N1 * 2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"RR1.{y*13+1:03d}"].select_set(True)

# R2
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["RR2"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((39 * y, N1 * 2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"RR2.{y*13+1:03d}"].select_set(True)

# R3
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["RR3"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((39 * y, N1 * 2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"RR3.{y*13+1:03d}"].select_set(True)

# R4
for y in range(0, 10):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects["RR4"].select_set(True)
    N1 = random.randint(0, 3)
    Nx = random.randint(3, 6)
    Nz = random.randint(3, 6)
    Ny = random.randint(2, 4)
    Nd = random.randint(1, 9)
    decay((39 * y, N1 * 2, 0), (1 * Nx, 1 * Ny, 1 * Nz))
    for x in range(1, 13):
        decay((x * 3, 0, 0), (1 - x * Nd * 0.01, 1 - x * Nd * 0.01, 1 - x * Nd * 0.01))
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects[f"RR4.{y*13+1:03d}"].select_set(True)

bpy.ops.object.select_all(action="DESELECT")
