# SPDX-FileCopyrightText: 2013 Julien Rippinger
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# 23 I 2014
# MacOS 10.6.8 // Blender r58536

import bpy
import random
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# ----------------------------------------------------------------------------------------

# OPERATIONS


def Boolean(x):
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[x]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")


def Delete(x):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects[x].select = True
    bpy.ops.object.delete(use_global=False)


def Name(x):
    bpy.context.object.name = x


def Rotation_X(x):
    bpy.context.object.rotation_euler[0] = x


def Rotation_Y(x):
    bpy.context.object.rotation_euler[1] = x


def Cube(x, a, b, c, d, e, f):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=x)
    bpy.ops.transform.translate(value=(a, b, c))
    bpy.ops.transform.resize(value=(d, e, f))


def Cylinder(x, y, z, a, b, c):
    bpy.ops.mesh.primitive_cylinder_add(vertices=32, depth=z, radius=y, location=x)
    bpy.ops.transform.translate(value=(a, b, c))


def Sphere(x, y, a, b, c):
    bpy.ops.mesh.primitive_uv_sphere_add(segments=32, ring_count=16, size=y, location=x)
    bpy.ops.transform.translate(value=(a, b, c))


def Plane(x, y, a, b, c):
    bpy.ops.mesh.primitive_plane_add(radius=y, location=x)
    bpy.ops.transform.translate(value=(a, b, c))


def Cone(x, y, z, a, b, c):
    bpy.ops.mesh.primitive_cone_add(
        vertices=4, radius1=z, radius2=0, depth=y, location=x, rotation=(0, 0, 0.785398)
    )
    bpy.ops.transform.translate(value=(a, b, c))


def Center():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.transform.translate(value=(-(axes - 1) * 11 / 7, -(axes - 1) * 11 / 7, 0))


def Grid():
    bpy.ops.mesh.primitive_grid_add(
        x_subdivisions=(axes * 7 + (axes - 1) * 15) + 1,
        y_subdivisions=(axes * 7 + (axes - 1) * 15) + 1,
        radius=(axes * 7 + (axes - 1) * 15) / 14,
        location=(0, 0, 0),
    )


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# JEAN-NICOLAS LOUIS DURAND : MARCHE À SUIVRE DANS LA COMPOSITION D'UN PROJET QUELCONQUE
# à partir de la division d'un carré...
# ----------------------------------------------------------------------------------------


# VARIABLES

axes = random.randint(2, 5)  # nombre d'axes


# Chances

cS = random.randint(0, 1)  # type de colonnade pour les salles
cV = random.randint(0, 3)  # type de vestibules -> influence la pièce centrale


# Compteurs
# pour la console & les noms des éléments
nSalles = []
nVestibules = []
nPiecesCentrales = []
nCours = []
nColonnes = []

# ----------------------------------------------------------------------------------------

# LISTE AVEC LES COORDONNEES
# En fonction du nombre d'axes
listeSalles = []
for x in range(0, axes):
    for y in range(0, axes):
        listeSalles.append(
            (x * 22 / 7, y * 22 / 7, 0)
        )  # Toutes les coordonnées des petites salles

listeVestibules_X = []
for x in range(0, axes - 1):
    for y in range(0, axes):
        listeVestibules_X.append(
            (x * 22 / 7 + 11 / 7, y * 22 / 7, 0)
        )  # Toutes les coordonnées des vestibules de sur l'axe X

listeVestibules_Y = []
for x in range(0, axes):
    for y in range(0, axes - 1):
        listeVestibules_Y.append(
            (x * 22 / 7, y * 22 / 7 + 11 / 7, 0)
        )  # Toutes les coordonnées des vestibules de sur l'axe Y

listePiecesCentrales = []
for x in range(0, axes - 1):
    for y in range(0, axes - 1):
        listePiecesCentrales.append(
            (x * 22 / 7 + 11 / 7, y * 22 / 7 + 11 / 7, 0)
        )  # Toutes les coordonnées des éléments centraux

# ----------------------------------------------------------------------------------------

# FONCTION PAR ELEMENT


def Salle(x):

    nSalles.append(x)  # Compteur

    for k in range(-1, 2, 2):
        Cube(x, k * (0.5 - 0.5 / 28), 0, 2.5 / 7, 1 / 28, 1 - 2 / 28, 1 / 7)
        Name("Salle " + str(len(nSalles)) + ": mur")
        Cube(x, 0, k * (0.5 - 0.5 / 28), 2.5 / 7, 1, 1 / 28, 1 / 7)
        Name("Salle " + str(len(nSalles)) + ": mur")
        for l in range(-1, 2, 2):
            Cube(
                x,
                l * (0.5 - 0.5 / 28),
                k * (2 / 7 - 0.75 / 28),
                1 / 7,
                1 / 28,
                3 / 7 - 0.5 / 28,
                2 / 7,
            )
            Name("Salle " + str(len(nSalles)) + ": mur")
            Cube(
                x,
                k * (2 / 7 - 0.25 / 28),
                l * (0.5 - 0.5 / 28),
                1 / 7,
                3 / 7 + 0.5 / 28,
                1 / 28,
                2 / 7,
            )
            Name("Salle " + str(len(nSalles)) + ": mur")
            Cylinder(x, 0.5 / 28, 3 / 7, l * (2.5 / 7), k * (2.5 / 7), 1.5 / 7)
            Name("Salle " + str(len(nSalles)) + ": colonne")
            nColonnes.append(x)
    Cube(x, 0, 0, 3.5 / 7, 5 / 7 - 1 / 28, 5 / 7 - 1 / 28, 1 / 7)
    Name("Négatif")
    Cube(x, 0, 0, 3 / 7 + 1 / 21, 1, 1, 2 / 21)
    Name("Salle " + str(len(nSalles)) + ": plafond")
    Boolean("Négatif")
    Cube(x, 0, 0, 4 / 7 - 0.5 / 21, 1 + 1 / 21, 1 + 1 / 21, 1 / 21)
    Name("Salle " + str(len(nSalles)) + ": plafond")
    Boolean("Négatif")
    Delete("Négatif")

    if cS == 1:
        for l in range(-1, 2, 2):
            for m in range(-1, 2, 2):
                Cube(x, m * 1 / 7, l * (0.5 - 0.5 / 28), 4.5 / 7, 1 / 7, 1 / 28, 1 / 7)
                Name("Salle " + str(len(nSalles)) + ": mur")
            for n in range(-3, 4, 6):
                Cube(
                    x,
                    n * (1 / 7 - 0.25 / 28),
                    l * (0.5 - 0.5 / 28),
                    4.5 / 7,
                    1 / 7 - 0.5 / 28,
                    1 / 28,
                    1 / 7,
                )
                Name("Salle " + str(len(nSalles)) + ": mur")
            for k in range(-3, 4, 2):
                Cylinder(x, 0.5 / 28, 3 / 7, l * (2.5 / 7), k * (0.5 / 7), 1.5 / 7)
                Name("Salle " + str(len(nSalles)) + ": colonne")
                nColonnes.append(x)
                Cylinder(x, 0.5 / 28, 3 / 7, k * (0.5 / 7), l * (2.5 / 7), 1.5 / 7)
                Name("Salle " + str(len(nSalles)) + ": colonne")
                nColonnes.append(x)
                Cube(x, l * (0.5 - 0.5 / 28), k * 1 / 7, 4.5 / 7, 1 / 28, 1 / 7, 1 / 7)
                Name("Salle " + str(len(nSalles)) + ": mur")
        Cube(x, 0, 0, 5 / 7 + 0.5 / 28, 1 - 2 / 28, 1 - 2 / 28, 1 / 28)
        Name("Négatif")
        Cube(x, 0, 0, 5 / 7 + 0.5 / 28, 1 + 1 / 21, 1 + 1 / 21, 1 / 28)
        Name("Salle " + str(len(nSalles)) + ": mur")
        Boolean("Négatif")
        Delete("Négatif")
        Cone(x, 2 / 7 - 1 / 28, 0.75, 0, 0, 6 / 7 + 0.5 / 28)
        Name("Salle " + str(len(nSalles)) + ": toit")

    else:
        Cylinder(x, 2.5 / 7 - 0.5 / 28, 2, 0, 0, 4 / 7)
        Rotation_X(1.5708)
        Cylinder(x, 2.5 / 7 - 0.5 / 28, 2, 0, 0, 4 / 7)
        Rotation_Y(1.5708)
        Cube(x, 0, 0, 5 / 7 + 1 / 28, 1, 1, 3 / 7 - 0.5 / 7)
        Name("Salle " + str(len(nSalles)) + ": plafond")
        Boolean("Cylinder")
        Boolean("Cylinder.001")
        Delete("Cylinder")
        Delete("Cylinder.001")
        Cube(x, 0, 0, 1 - 1 / 21, 1 + 1 / 21, 1 + 1 / 21, 1 / 21)
        Name("Salle " + str(len(nSalles)) + ": toit")


def Vestibules_X(x):

    nVestibules.append(x)

    Cube(x, 0, 0, 3.5 / 7 + 1 / 21, 15 / 7 - 1 / 21, 1 + 1 / 21, 1 / 21)
    Name("Vestibule " + str(len(nVestibules)) + ": toit")
    for k in range(-1, 2, 2):
        Cube(
            x,
            0,
            k * (3.5 / 7 - 0.5 / 28),
            3 / 7 - 0.5 / 21,
            15 / 7,
            1 / 28,
            1 / 7 + 2 / 21,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": mur")
        Cube(
            x,
            0,
            k * (3 / 7 - 1.25 / 28),
            3 / 7 - 0.5 / 21 + 0.5 / 28,
            15 / 7,
            1 / 7 + 0.5 / 28,
            1 / 7 + 1 / 28 + 0.5 / 21,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")
        Cube(
            x,
            0,
            k * (3 / 7 - 0.75 / 28),
            2 / 7 + 0.5 / 28,
            15 / 7,
            1 / 7 - 0.5 / 28,
            1 / 28,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")
        Cube(x, 0, 0, 3.5 / 7 - 0.5 / 21, 15 / 7, 5 / 7 - 3 / 28, 2 / 21)
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")

    if cV <= 1:
        for k in range(-1, 2, 2):
            for l in range(-1, 2, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, k * 2.5 / 7, l * 2.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            if cV == 0:
                for l in range(-1, 2, 2):
                    Cube(
                        x,
                        k * 2.5 / 7,
                        l * (3.5 / 7 - 0.5 / 28),
                        1 / 7,
                        4 / 7 + 1 / 28,
                        1 / 28,
                        2 / 7,
                    )
                    Name("Vestibule " + str(len(nVestibules)) + ": mur")
                for m in range(-13, 14, 26):
                    Cube(
                        x,
                        m * (0.5 / 7 - 0.25 / 28 / 13),
                        k * (3.5 / 7 - 0.5 / 28),
                        1 / 7,
                        2 / 7 + 0.5 / 28,
                        1 / 28,
                        2 / 7,
                    )
                    Name("Vestibule " + str(len(nVestibules)) + ": mur")

    if cV == 1:
        for k in range(-1, 2, 2):
            for l in range(7, 14, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, -0.5 / 7 * l, k * 2.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
                Cylinder(x, 0.5 / 28, 2 / 7, 0.5 / 7 * l, -k * 2.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            for m in range(-3, 4, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, 0.5 / 7 * m, -k * 2.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            for n in range(-7, 8, 14):
                Cube(
                    x,
                    n * (1 / 7 - 0.25 / 28 / 7),
                    k * (3.5 / 7 - 0.5 / 28),
                    1.5 / 7 - 0.5 / 28,
                    1 / 7 + 0.5 / 28,
                    1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
            for o in range(-5, 6, 2):
                Cube(
                    x,
                    o * 1 / 7,
                    k * (3.5 / 7 - 0.5 / 28),
                    1.5 / 7 - 0.5 / 28,
                    1 / 7 + 1 / 28,
                    1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
            for p in range(-1, 2, 2):
                Cube(
                    x,
                    k * (3.5 / 7 + 1.75 / 28),
                    p * (3.5 / 7 - 0.5 / 28),
                    1.5 / 28,
                    1 + 0.5 / 28,
                    1 / 28,
                    3 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")

    if cV >= 2:
        for k in range(-1, 2, 2):
            for l in range(-1, 2, 2):
                Cube(
                    x,
                    k * 2.5 / 7,
                    l * (2.5 / 7 + 1.25 / 28),
                    1 / 7,
                    1 / 28,
                    1 / 7 - 0.5 / 28,
                    2 / 7,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
                Cube(
                    x,
                    k * (3.5 / 7 + 1.75 / 28),
                    l * (3.5 / 7 - 0.5 / 28),
                    1.5 / 28,
                    1 + 0.5 / 28,
                    1 / 28,
                    3 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
                Cube(
                    x,
                    k * (2.5 / 7),
                    l * (3.5 / 7 - 0.5 / 28),
                    1.5 / 7 - 0.5 / 28,
                    4 / 7 + 1 / 28,
                    1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
                Cube(
                    x,
                    k * (6.5 / 7 - 0.25 / 28),
                    l * (3.5 / 7 - 0.5 / 28),
                    1.5 / 7 - 0.5 / 28,
                    2 / 7 + 0.5 / 28,
                    1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")

            if cV == 3:
                for m in range(7, 14, 2):
                    Cylinder(x, 0.5 / 28, 2 / 7, -0.5 / 7 * m, k * 2.5 / 7, 1 / 7)
                    Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                    nColonnes.append(x)
                    Cylinder(x, 0.5 / 28, 2 / 7, 0.5 / 7 * m, -k * 2.5 / 7, 1 / 7)
                    Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                    nColonnes.append(x)


def Vestibules_Y(x):

    nVestibules.append(x)

    Cube(x, 0, 0, 3.5 / 7 + 1 / 21, 1 + 1 / 21, 15 / 7 - 1 / 21, 1 / 21)
    Name("Vestibule " + str(len(nVestibules)) + ": toit")
    for k in range(-1, 2, 2):
        Cube(
            x,
            k * (3.5 / 7 - 0.5 / 28),
            0,
            3 / 7 - 0.5 / 21,
            1 / 28,
            15 / 7,
            1 / 7 + 2 / 21,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": mur")
        Cube(
            x,
            k * (3 / 7 - 1.25 / 28),
            0,
            3 / 7 - 0.5 / 21 + 0.5 / 28,
            1 / 7 + 0.5 / 28,
            15 / 7,
            1 / 7 + 1 / 28 + 0.5 / 21,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")
        Cube(
            x,
            k * (3 / 7 - 0.75 / 28),
            0,
            2 / 7 + 0.5 / 28,
            1 / 7 - 0.5 / 28,
            15 / 7,
            1 / 28,
        )
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")
        Cube(x, 0, 0, 3.5 / 7 - 0.5 / 21, 5 / 7 - 3 / 28, 15 / 7, 2 / 21)
        Name("Vestibule " + str(len(nVestibules)) + ": plafond")

    if cV <= 1:
        for k in range(-1, 2, 2):
            for l in range(-1, 2, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, k * 2.5 / 7, l * 2.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            if cV == 0:
                for l in range(-1, 2, 2):
                    Cube(
                        x,
                        k * (3.5 / 7 - 0.5 / 28),
                        l * 2.5 / 7,
                        1 / 7,
                        1 / 28,
                        4 / 7 + 1 / 28,
                        2 / 7,
                    )
                    Name("Vestibule " + str(len(nVestibules)) + ": mur")
                for m in range(-13, 14, 26):
                    Cube(
                        x,
                        k * (3.5 / 7 - 0.5 / 28),
                        m * (0.5 / 7 - 0.25 / 28 / 13),
                        1 / 7,
                        1 / 28,
                        2 / 7 + 0.5 / 28,
                        2 / 7,
                    )
                    Name("Vestibule " + str(len(nVestibules)) + ": mur")

    if cV == 1:
        for k in range(-1, 2, 2):
            for l in range(7, 14, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, k * 2.5 / 7, -l * 0.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
                Cylinder(x, 0.5 / 28, 2 / 7, -k * 2.5 / 7, l * 0.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            for m in range(-3, 4, 2):
                Cylinder(x, 0.5 / 28, 2 / 7, -k * 2.5 / 7, m * 0.5 / 7, 1 / 7)
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
            for n in range(-7, 8, 14):
                Cube(
                    x,
                    k * (3.5 / 7 - 0.5 / 28),
                    n * (1 / 7 - 0.25 / 28 / 7),
                    1.5 / 7 - 0.5 / 28,
                    1 / 28,
                    1 / 7 + 0.5 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
            for o in range(-5, 6, 2):
                Cube(
                    x,
                    k * (3.5 / 7 - 0.5 / 28),
                    o * 1 / 7,
                    1.5 / 7 - 0.5 / 28,
                    1 / 28,
                    1 / 7 + 1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
            for p in range(-1, 2, 2):
                Cube(
                    x,
                    p * (3.5 / 7 - 0.5 / 28),
                    k * (3.5 / 7 + 1.75 / 28),
                    1.5 / 28,
                    1 / 28,
                    1 + 0.5 / 28,
                    3 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")

    if cV >= 2:
        for k in range(-1, 2, 2):
            for l in range(-1, 2, 2):
                Cube(
                    x,
                    l * (2.5 / 7 + 1.25 / 28),
                    k * 2.5 / 7,
                    1 / 7,
                    1 / 7 - 0.5 / 28,
                    1 / 28,
                    2 / 7,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                nColonnes.append(x)
                Cube(
                    x,
                    l * (3.5 / 7 - 0.5 / 28),
                    k * (3.5 / 7 + 1.75 / 28),
                    1.5 / 28,
                    1 / 28,
                    1 + 0.5 / 28,
                    3 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
                Cube(
                    x,
                    l * (3.5 / 7 - 0.5 / 28),
                    k * 2.5 / 7,
                    1.5 / 7 - 0.5 / 28,
                    1 / 28,
                    4 / 7 + 1 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")
                Cube(
                    x,
                    l * (3.5 / 7 - 0.5 / 28),
                    k * (6.5 / 7 - 0.25 / 28),
                    1.5 / 7 - 0.5 / 28,
                    1 / 28,
                    2 / 7 + 0.5 / 28,
                    1 / 7 + 1 / 28,
                )
                Name("Vestibule " + str(len(nVestibules)) + ": mur")

            if cV == 3:
                for m in range(7, 14, 2):
                    Cylinder(x, 0.5 / 28, 2 / 7, k * 2.5 / 7, -m * 0.5 / 7, 1 / 7)
                    Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                    nColonnes.append(x)
                    Cylinder(x, 0.5 / 28, 2 / 7, -k * 2.5 / 7, m * 0.5 / 7, 1 / 7)
                    Name("Vestibule " + str(len(nVestibules)) + ": colonne")
                    nColonnes.append(x)


def PieceCentrale(x):

    nPiecesCentrales.append(x)

    if cV == 0 or cV >= 2:
        for k in range(-1, 2, 2):
            Cube(
                x,
                k * (7.5 / 7 - 0.5 / 28),
                0,
                3 / 7 - 0.5 / 21,
                1 / 28,
                15 / 7,
                1 / 7 + 2 / 28 + 0.5 / 21,
            )
            Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
            Cube(
                x,
                0,
                k * (7.5 / 7 - 0.5 / 28),
                3 / 7 - 0.5 / 21,
                15 / 7 - 2 / 28,
                1 / 28,
                1 / 7 + 2 / 28 + 0.5 / 21,
            )
            Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
            for l in range(-1, 2, 2):
                Cube(
                    x,
                    k * (7.5 / 7 - 0.5 / 28),
                    l * 2.5 / 7,
                    1 / 7,
                    1 / 28,
                    4 / 7 + 1 / 28,
                    2 / 7,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                Cube(
                    x,
                    k * 2.5 / 7,
                    l * (7.5 / 7 - 0.5 / 28),
                    1 / 7,
                    4 / 7 + 1 / 28,
                    1 / 28,
                    2 / 7,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                Cube(
                    x,
                    k * (7.5 / 7 - 0.5 / 28),
                    l * (6.5 / 7 - 0.75 / 28),
                    1 / 7,
                    1 / 28,
                    2 / 7 - 0.5 / 28,
                    2 / 7,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                Cube(
                    x,
                    k * (6.5 / 7 - 0.25 / 28),
                    l * (7.5 / 7 - 0.5 / 28),
                    1 / 7,
                    2 / 7 + 0.5 / 28,
                    1 / 28,
                    2 / 7,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                if cV >= 2:
                    Cube(
                        x,
                        k * 5 / 7,
                        l * (7.5 / 7 - 0.5 / 28),
                        1.5 / 28,
                        3 / 28,
                        1 / 28,
                        3 / 28,
                    )
                    Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                    Cube(
                        x,
                        k * (7.5 / 7 - 0.5 / 28),
                        l * 5 / 7,
                        1.5 / 28,
                        1 / 28,
                        3 / 28,
                        3 / 28,
                    )
                    Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")

    if cV == 1:
        for k in range(-1, 2, 2):
            Cube(
                x,
                k * (7.5 / 7 - 0.5 / 28),
                0,
                3 / 7 - 0.5 / 21,
                1 / 28,
                1 / 7 - 1 / 28,
                1 / 7 + 2 / 28 + 0.5 / 21,
            )
            Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
            Cube(
                x,
                0,
                k * (7.5 / 7 - 0.5 / 28),
                3 / 7 - 0.5 / 21,
                1 / 7 - 1 / 28,
                1 / 28,
                1 / 7 + 2 / 28 + 0.5 / 21,
            )
            Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
            for l in range(-1, 2, 2):
                Cube(
                    x,
                    k * (7.5 / 7 - 0.5 / 28),
                    l * (7.5 / 7 / 2 + 0.75 / 28),
                    2 / 7 - 0.5 / 21,
                    1 / 28,
                    1 + 0.5 / 28,
                    4 / 7 - 1 / 21,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")
                Cube(
                    x,
                    k * (3.25 / 7 + 2.25 / 28),
                    l * (7.5 / 7 - 0.5 / 28),
                    2 / 7 - 0.5 / 21,
                    1 - 0.5 / 28,
                    1 / 28,
                    4 / 7 - 1 / 21,
                )
                Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": mur")

    for k in range(1, 49, 2):
        Cylinder(
            x,
            1 / 28,
            4 / 7 - 1 / 21,
            sin(k * pi / 24) * 5 / 7,
            cos(k * pi / 24) * 5 / 7,
            2 / 7 - 0.5 / 21,
        )
        Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": colonne")
        nColonnes.append(x)
    Cylinder(x, 4.5 / 7 + 1 / 28, 1, 0, 0, 0.5)
    Name("Négatif")

    Cube(x, 0, 0, 4 / 7 + 1 / 21, 15 / 7 + 1 / 21, 15 / 7 + 1 / 21, 1 / 7 + 1 / 21)
    Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": plafond et corniche")
    Boolean("Négatif")
    Delete("Négatif")

    Plane(x, 7.5 / 7, 0, 0, 4 / 7)
    Name("Négatif_plan")
    Cylinder(x, 2 / 7, 5, 0, 0, 0)
    Name("Négatif_oculus")
    Sphere(x, 5 / 7 - 1 / 28, 0, 0, 5 / 7)
    Name("Négatif_coupole")
    Cylinder(x, 4.5 / 7 + 3 / 28, 1 / 7, 0, 0, 4.5 / 7)
    Name("Négatif_corniche")

    Sphere(x, 7.5 / 7, 0, 0, 3.8 / 7)
    Name("Pièce centrale " + str(len(nPiecesCentrales)) + ": dome")
    Boolean("Négatif_coupole")
    Boolean("Négatif_plan")
    Boolean("Négatif_corniche")
    Boolean("Négatif_oculus")
    Delete("Négatif_coupole")
    Delete("Négatif_plan")
    Delete("Négatif_corniche")
    Delete("Négatif_oculus")


def Cour(x):

    nCours.append(x)

    for k in range(-1, 2, 2):
        for l in range(-11, 12, 2):
            Cylinder(x, 0.5 / 28, 3 / 7, l * (0.5 / 7), k * (5.5 / 7), 1.5 / 7)
            Name("Cour " + str(len(nCours)) + ": colonne")
            nColonnes.append(x)
        for l in range(-9, 10, 2):
            Cylinder(x, 0.5 / 28, 3 / 7, k * (5.5 / 7), l * (0.5 / 7), 1.5 / 7)
            Name("Cour " + str(len(nCours)) + ": colonne")
            nColonnes.append(x)
    Cube(x, 0, 0, 3 / 7 + 0.5 / 21, 11 / 7 - 1 / 21, 11 / 7 - 1 / 21, 1 / 21)
    Name("Négatif")
    Cube(x, 0, 0, 3 / 7 + 0.5 / 21, 15 / 7, 15 / 7, 1 / 21)
    Name("Cour " + str(len(nCours)) + ": plafond")
    Boolean("Négatif")
    Delete("Négatif")


# ----------------------------------------------------------------------------------------

# COMPOSITION

# 2 Axes = carré
if axes == 2:
    for x in range(0, axes * 2):
        Salle(listeSalles[x])  # les quatres salles sur les coins
    b = random.choice(range(3))  # choix aléatoire entre 3 possibilités
    c = random.randint(
        0, 1
    )  # choix aléatoire entre 2 possibilités: élément central oui/non
    if b >= 0:  # deux vestibules selon l'axe Y (constante)
        for x in range(0, axes):
            Vestibules_Y(listeVestibules_Y[x])
    if b >= 1:  # un vestibule en plus sur un des deux axes Y
        if c == 0:
            Vestibules_X(listeVestibules_X[0])
        else:
            Vestibules_X(listeVestibules_X[1])
    if b == 2:  # un vestibule en plus sur l'axe opposée
        if c == 0:
            Vestibules_X(listeVestibules_X[1])
        else:
            Vestibules_X(listeVestibules_X[0])
        if c == 0:  # Un élément central
            PieceCentrale(listePiecesCentrales[0])
        if c == 1:
            Cour(listePiecesCentrales[0])

    Center()
    Grid()

# 3 Axes = carré divisé par 3 axes
if axes == 3:
    b = random.choice(range(5))  # 5 compositions différentes
    c = random.choice(range(2))  # Chance
    Salle(listeSalles[4])
    if b == 0 or b == 3 or b == 4:  # premier cas de figure
        for x in range(1, axes + 2):
            Salle(
                listeSalles[x + (x - 1)]
            )  # appel à la 1er, 3iem, 5iem et 7iem donnée (coordonnée) de la liste "listeCubes"
        if b == 3 or b == 4:
            for x in range(
                0, 3, 2
            ):  # appel à la 0 et 2iem donnée (coordonnée) de la liste "listeCubes" et "listeVestibules_X"
                Salle(listeSalles[x])
                Vestibules_X(listeVestibules_X[x])
            for x in range((len(listeSalles) - 3), len(listeSalles), 2):
                Salle(listeSalles[x])
            for x in range(0, 6):
                Vestibules_Y(listeVestibules_Y[x])
            for x in range(3, 6, 2):
                Vestibules_X(listeVestibules_X[x])
            if b == 4:
                for x in range(1, 5, 3):
                    Vestibules_X(listeVestibules_X[x])
                if c == 1:
                    for x in range(0, 4):
                        PieceCentrale(listePiecesCentrales[x])
                else:
                    for x in range(0, 4):
                        Cour(listePiecesCentrales[x])
        else:
            for x in range(1, 5, 3):
                Vestibules_X(listeVestibules_X[x])
            for x in range(2, 4):
                Vestibules_Y(listeVestibules_Y[x])
            if c == 1:
                for x in range(0, 4):
                    PieceCentrale(listePiecesCentrales[x])
    if b == 1 or b == 2:  # deuxième cas de figure
        for x in range(0, 3):
            Salle(listeSalles[x])
        for x in range(0, 2):
            Vestibules_Y(listeVestibules_Y[x])
        for x in range(1, 5, 3):
            Vestibules_X(listeVestibules_X[x])
        for x in range(6, 9):
            Salle(listeSalles[x])
        for x in range(4, 6):
            Vestibules_Y(listeVestibules_Y[x])
        if b == 2:
            for x in range(3, 6, 2):
                Salle(listeSalles[x])
            for x in range(2, 4):
                Vestibules_Y(listeVestibules_Y[x])
            if c == 1:
                for x in range(0, 4):
                    PieceCentrale(listePiecesCentrales[x])

    Center()
    Grid()

# 4 Axes

if axes == 4:
    b = random.choice(range(6))  # 6 Compositions différentes

    if b == 0:
        for x in range(0, 13, 12):
            for y in range(0, 4, 3):
                Salle(listeSalles[x + y])
        for x in range(1, 14, 4):
            for y in range(0, 2):
                Salle(listeSalles[x + y])
        for x in range(1, 10, 4):
            for y in range(0, 2):
                Vestibules_X(listeVestibules_X[x + y])
        for x in range(0, 10, 9):
            for y in range(0, 3, 2):
                Vestibules_Y(listeVestibules_Y[x + y])
        for x in range(1, 11, 3):
            Vestibules_Y(listeVestibules_Y[x])

        c = random.randint(0, 1)
        if c == 0:
            PieceCentrale(listePiecesCentrales[4])
        else:
            Cour(listePiecesCentrales[4])
        c = random.randint(0, 1)
        for x in range(1, 8, 6):
            if c == 0:
                PieceCentrale(listePiecesCentrales[x])
            else:
                Cour(listePiecesCentrales[x])

        c = random.randint(0, 1)
        if c == 0:
            for x in range(4, 8, 3):
                for y in range(0, 5, 4):
                    Salle(listeSalles[x + y])
            for x in range(3, 6, 2):
                for y in range(0, 4, 3):
                    Vestibules_Y(listeVestibules_Y[x + y])

    if b == 1:
        for x in range(1, 11, 3):
            Vestibules_Y(listeVestibules_Y[x])
        for x in range(4, 8):
            Vestibules_X(listeVestibules_X[x])
        for x in range(4, 9, 4):
            for y in range(0, 4, 3):
                Salle(listeSalles[x + y])
        for x in range(1, 14, 4):
            for y in range(0, 2):
                Salle(listeSalles[x + y])

        c = random.randint(0, 1)
        if c == 0:
            for x in range(1, 10, 8):
                for y in range(0, 2):
                    Vestibules_X(listeVestibules_X[x + y])
            for x in range(3, 6, 2):
                for y in range(0, 4, 3):
                    Vestibules_Y(listeVestibules_Y[x + y])
            c = random.randint(0, 1)
            if c == 0:
                for x in range(0, 7, 6):
                    for y in range(0, 3, 2):
                        PieceCentrale(listePiecesCentrales[x + y])
            c = random.randint(0, 1)
            for x in range(1, 8, 2):
                if c == 0:
                    PieceCentrale(listePiecesCentrales[x])
                else:
                    Cour(listePiecesCentrales[x])
            c = random.randint(0, 1)
            if c == 0:
                PieceCentrale(listePiecesCentrales[4])
            else:
                Cour(listePiecesCentrales[4])
        if c == 1:
            for x in range(0, 13, 12):
                for y in range(0, 4, 3):
                    Salle(listeSalles[x + y])
            for x in range(0, 9, 8):
                for y in range(0, 4, 3):
                    Vestibules_X(listeVestibules_X[x + y])
            for x in range(0, 10, 9):
                for y in range(0, 3, 2):
                    Vestibules_Y(listeVestibules_Y[x + y])
            for x in range(1, 8, 2):
                PieceCentrale(listePiecesCentrales[x])
            c = random.randint(0, 1)
            if c == 0:
                PieceCentrale(listePiecesCentrales[4])
            else:
                Cour(listePiecesCentrales[4])

    if b >= 2:  # Composition & Base pour les compositions 3, 4 & 5
        for x in range(0, 16):
            Salle(listeSalles[x])
        for x in range(0, 9, 4):
            Vestibules_X(listeVestibules_X[x])
        for x in range(3, 12, 4):
            Vestibules_X(listeVestibules_X[x])
        for x in range(0, 12):
            Vestibules_Y(listeVestibules_Y[x])
        if b == 3:
            for x in range(5, 7):
                Vestibules_X(listeVestibules_X[x])
            c = random.randint(0, 1)
            if c == 0:
                for x in range(3, 6, 2):
                    PieceCentrale(listePiecesCentrales[x])
            else:
                for x in range(3, 6, 2):
                    Cour(listePiecesCentrales[x])
            c = random.randint(0, 1)
            if c == 0:
                PieceCentrale(listePiecesCentrales[4])
            else:
                Cour(listePiecesCentrales[4])
        if b == 4:
            for x in range(1, 10, 8):
                for y in range(0, 2):
                    Vestibules_X(listeVestibules_X[x + y])
            c = random.randint(0, 1)
            for x in range(0, 7, 6):
                for y in range(0, 3, 2):
                    if c == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
            c = random.randint(0, 1)
            if c == 0:
                for x in range(1, 8, 6):
                    PieceCentrale(listePiecesCentrales[x])
            else:
                for x in range(1, 8, 6):
                    Cour(listePiecesCentrales[x])
        if b == 5:
            for x in range(1, 10, 4):
                for y in range(0, 2):
                    Vestibules_X(listeVestibules_X[x + y])
            c = random.randint(0, 1)
            for x in range(0, 7, 6):
                for y in range(0, 3, 2):
                    if c == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
            c = random.randint(0, 1)
            for x in range(1, 8, 2):
                if c == 0:
                    PieceCentrale(listePiecesCentrales[x])
                else:
                    Cour(listePiecesCentrales[x])
            c = random.randint(0, 1)
            if c == 0:
                PieceCentrale(listePiecesCentrales[4])
            else:
                Cour(listePiecesCentrales[4])

    Center()
    Grid()

# 5 Axes

if axes == 5:
    b = random.choice(range(3))  # 3 BASES de composition différentes

    if b == 0:  # 1er Base avec deux développements aléatoires possibles
        c = random.randint(0, 1)
        if c == 0:
            for x in range(1, 22, 5):
                for y in range(0, 3, 2):
                    Salle(listeSalles[x + y])
            for x in range(5, 10, 2):
                for y in range(0, 11, 10):
                    Salle(listeSalles[x + y])
            for x in range(1, 17, 5):
                for y in range(0, 3, 2):
                    Vestibules_X(listeVestibules_X[x + y])
            for x in range(4, 8):
                for y in range(0, 9, 8):
                    Vestibules_Y(listeVestibules_Y[x + y])

            cc = random.randint(0, 1)
            if cc == 0:
                for x in range(2, 23, 10):
                    Salle(listeSalles[x])
                for x in range(10, 15, 4):
                    Salle(listeSalles[x])
                for x in range(2, 18, 5):
                    Vestibules_X(listeVestibules_X[x])
                for x in range(8, 12):
                    Vestibules_Y(listeVestibules_Y[x])
                ccc = random.randint(0, 1)
                for x in range(5, 7):
                    for y in range(0, 5, 4):
                        if ccc == 0:
                            PieceCentrale(listePiecesCentrales[x + y])
                        else:
                            Cour(listePiecesCentrales[x + y])
        if c == 1:
            for x in range(0, 2):
                Salle(listeSalles[x])
                Salle(listeSalles[x + 8])
                Salle(listeSalles[x + 15])
                Salle(listeSalles[x + 23])
                Vestibules_X(listeVestibules_X[x])
                Vestibules_X(listeVestibules_X[x + 3])
                Vestibules_X(listeVestibules_X[x + 15])
                Vestibules_X(listeVestibules_X[x + 18])
            for x in range(3, 7):
                Salle(listeSalles[x])
                Salle(listeSalles[x + 15])
            for x in range(0, 4, 3):
                Vestibules_Y(listeVestibules_Y[x])
                Vestibules_Y(listeVestibules_Y[x + 4])
                Vestibules_Y(listeVestibules_Y[x + 12])
                Vestibules_Y(listeVestibules_Y[x + 16])
            cc = random.randint(0, 1)
            for x in range(0, 4, 3):
                for y in range(0, 13, 12):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])

            cc = random.randint(0, 1)
            if cc == 0:
                for x in range(2, 23, 20):
                    Salle(listeSalles[x])
                for x in range(10, 15, 4):
                    Salle(listeSalles[x])
                for x in range(5, 10, 4):
                    for y in range(0, 6, 5):
                        Vestibules_X(listeVestibules_X[x + y])
                for x in range(1, 3):
                    for y in range(0, 17, 16):
                        Vestibules_Y(listeVestibules_Y[x + y])

    if b == 1:  # 2ième Base avec développement aléatoire

        for x in range(1, 22, 5):
            for y in range(0, 3, 2):
                Salle(listeSalles[x + y])
        for x in range(5, 10, 2):
            for y in range(0, 11, 10):
                Salle(listeSalles[x + y])
        for x in range(1, 17, 5):
            for y in range(0, 3, 2):
                Vestibules_X(listeVestibules_X[x + y])
        for x in range(4, 8):
            for y in range(0, 9, 8):
                Vestibules_Y(listeVestibules_Y[x + y])

        c = random.randint(0, 1)
        if c == 0:
            for x in range(0, 5, 4):
                for y in range(0, 6, 5):
                    Salle(listeSalles[x + 4 * y])
                    Vestibules_X(listeVestibules_X[x + 3 * y])
            for x in range(0, 4, 3):
                for y in range(0, 17, 16):
                    Vestibules_Y(listeVestibules_Y[x + y])
            cc = random.randint(0, 1)
            for x in range(0, 4, 3):
                for y in range(0, 13, 12):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
            cc = random.randint(0, 1)
            if cc >= 0:
                for x in range(2, 11, 8):
                    Salle(listeSalles[x])
                for x in range(14, 23, 8):
                    Salle(listeSalles[x])
                for x in range(8, 12, 3):
                    Vestibules_Y(listeVestibules_Y[x])
                for x in range(2, 18, 15):
                    Vestibules_X(listeVestibules_X[x])
            if cc == 1:
                Salle(listeSalles[12])
                for x in range(7, 13, 5):
                    Vestibules_X(listeVestibules_X[x])
                for x in range(9, 11):
                    Vestibules_Y(listeVestibules_Y[x])
                cc = random.randint(0, 1)
                for x in range(5, 7):
                    for y in range(0, 5, 4):
                        if cc == 0:
                            PieceCentrale(listePiecesCentrales[x + y])
                        else:
                            Cour(listePiecesCentrales[x + y])

        if c == 1:
            for x in range(2, 11, 8):
                Salle(listeSalles[x])
            for x in range(14, 23, 8):
                Salle(listeSalles[x])
            for x in range(5, 10, 4):
                for y in range(0, 6, 5):
                    Vestibules_X(listeVestibules_X[x + y])
            for x in range(1, 3):
                for y in range(0, 17, 16):
                    Vestibules_Y(listeVestibules_Y[x + y])
            cc = random.randint(0, 1)
            if cc >= 0:
                for x in range(2, 18, 15):
                    Vestibules_X(listeVestibules_X[x])
                for x in range(8, 12, 3):
                    Vestibules_Y(listeVestibules_Y[x])
                ccc = random.randint(0, 1)
                for x in range(1, 3):
                    for y in range(0, 13, 12):
                        if ccc == 0:
                            PieceCentrale(listePiecesCentrales[x + y])
                        else:
                            Cour(listePiecesCentrales[x + y])
                for x in range(4, 8, 3):
                    for y in range(0, 5, 4):
                        if ccc == 0:
                            PieceCentrale(listePiecesCentrales[x + y])
                        else:
                            Cour(listePiecesCentrales[x + y])
            if cc == 1:
                Salle(listeSalles[12])
                for x in range(7, 13, 5):
                    Vestibules_X(listeVestibules_X[x])
                for x in range(9, 11):
                    Vestibules_Y(listeVestibules_Y[x])
                cc = random.randint(0, 1)
                for x in range(5, 7):
                    for y in range(0, 5, 4):
                        if cc == 0:
                            PieceCentrale(listePiecesCentrales[x + y])
                        else:
                            Cour(listePiecesCentrales[x + y])

    if b == 2:
        for x in range(0, 11):
            for y in range(0, 15, 14):
                Salle(listeSalles[x + y])
        for x in range(0, 16, 5):
            for y in range(0, 5, 4):
                Vestibules_X(listeVestibules_X[x + y])
        for x in range(0, 8):
            for y in range(0, 13, 12):
                Vestibules_Y(listeVestibules_Y[x + y])
        c = random.randint(0, 3)
        if c >= 0:
            for x in range(11, 14, 2):
                Salle(listeSalles[x])
            for x in range(1, 4, 2):
                for y in range(0, 17, 5):
                    Vestibules_X(listeVestibules_X[x + y])
            cc = random.randint(0, 1)
            for x in range(0, 4, 3):
                for y in range(0, 13, 12):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
        if c >= 1:
            for x in range(2, 18, 15):
                Vestibules_X(listeVestibules_X[x])
            cc = random.randint(0, 1)
            for x in range(1, 3):
                for y in range(0, 13, 12):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
        if c >= 2:
            for x in range(8, 12, 3):
                Vestibules_Y(listeVestibules_Y[x])
            cc = random.randint(0, 1)
            for x in range(4, 8, 3):
                for y in range(0, 5, 4):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])
        if c >= 3:
            Salle(listeSalles[12])
            for x in range(7, 13, 5):
                Vestibules_X(listeVestibules_X[x])
            for x in range(9, 11):
                Vestibules_Y(listeVestibules_Y[x])
            cc = random.randint(0, 1)
            for x in range(5, 7):
                for y in range(0, 5, 4):
                    if cc == 0:
                        PieceCentrale(listePiecesCentrales[x + y])
                    else:
                        Cour(listePiecesCentrales[x + y])

    Center()
    Grid()


# ----------------------------------------------------------------------------------------

# RENDU

bpy.ops.object.select_all(action="TOGGLE")

# Console

print()
print(
    "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
)
print("    - nombre d'axes :", axes)
print()
print("    - nombre de salles :", len(nSalles))
print()
print("    - nombre de vestibules :", len(nVestibules))
print()
if len(nCours) == 0:
    print("    - aucun patio")
if len(nCours) == 1:
    print("    - un patio")
if len(nCours) > 1:
    print("    - nombre de patios :", len(nCours))
print()
if len(nPiecesCentrales) == 0:
    print("    - aucune pièce centrale")
if len(nPiecesCentrales) == 1:
    print("    - une pièce centrale")
if len(nPiecesCentrales) > 1:
    print("    - nombre de pièces centrales :", len(nPiecesCentrales))
print()
print("    -", len(nColonnes), "colonnes")
print()
print(
    "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
)
