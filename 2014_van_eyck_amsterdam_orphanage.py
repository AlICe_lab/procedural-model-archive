# SPDX-FileCopyrightText: 2014 Geoffrey Minne
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER r60995
# MANIPULATIONS D OBJETS SOUS MAC OSX


import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# _____________________________________________________________________________________________________________________#
# _____________________________________________________________________________________________________________________#
#                                                                                                                     #
#                                                                                                                     #                                                                                                                     #
#           _____       ____      ____              ____                           _____   ________                   #
#          I     I     I    I    I    I   I    I   I       I       I    I     I   I     I     I                       #
#          I     I     I   I     I    I   I    I   I       I       I    I I   I   I     I     I                       #
#          I     I     I I       I____I   I____I   I____   I       I    I  I  I   I_____I     I                       #
#          I     I     I   I     I        I    I   I       I       I    I   I I   I     I     I                       #
#          I_____I     I    I    I        I    I   I____   I_____  I    I     I   I     I     I                       #
#                                                                                                                     #                                                                                                                     #
#                                                                                                                     #
# ________________________________________________________________________________________________PAR ALDO VAN EYCK____#
# _____________________________________________________________________________________________________________________#


# ----------------------------------#
# GENERER UN MODULE DE CIRCULATION #
# ----------------------------------#


def Circulation(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))


brasX = random.randint(3, 4)
brasY = random.randint(4, 5)

for j in range(1, brasX):
    Circulation(j, 0, 0.5, 0.5, 0.5, 0.5)

for i in range(0, brasY):
    Circulation(0, i, 0.5, 0.5, 0.5, 0.5)


# Coupoles


def Coupoles(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_z):
    bpy.ops.mesh.primitive_uv_sphere_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(-0.488145, -0.488145, -0.488145))


for j in range(1, brasX):
    Coupoles(j, 0, 0.9, 0.5, 0.5, 0.5)

for i in range(0, brasY):
    Coupoles(0, i, 0.9, 0.5, 0.5, 0.5)


# _______________________________________________________________________________________________________________________#


# ---------------------#
# GENERER LE MODULE 1 #
# ---------------------#


def Module1(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))


def Coupoles2(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_z):
    bpy.ops.mesh.primitive_uv_sphere_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(-0.488145, -0.488145, -0.488145))


## Reseau du bas ##

rdc = random.randint(5, 7)

for m in range(1, random.randint(5, 7)):
    Module1(m, 1, 0.5, 0.5, 0.5, 0.5)
    Module1(m, 2, 0.5, 0.5, 0.5, 0.5)
    Coupoles2(m, 1, 0.9, 0.5, 0.5, 0.5)
    Coupoles2(m, 2, 0.9, 0.5, 0.5, 0.5)


## Reseau du haut ##

niveau1 = random.randint(2, 3)
niveau1bis = random.randint(3, 5)

for n in range(1, niveau1):
    for q in range(1, niveau1bis):
        Module1(q, n, 1.5, 0.5, 0.5, 0.5)
    # Coupoles2(q,n,1.9,0.5,0.5,0.5)


# _______________________________________________________________________________________________________________________#


# -----------------------------------------#
# REPETER LE MODULE 1 AVEC SA CIRCULATION #
# -----------------------------------------#


bpy.ops.object.select_all(action="SELECT")

MaxDuplication = random.randint(4, 5)

for nombreDuplication in range(1, MaxDuplication):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (brasX, -brasY + 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#


# ------------------------------------#
# GENERER UN MODULE DE CIRCULATION 2 #
# ------------------------------------#


def Circulation2(pos_X, pos_Y, pos_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.context.object.name = nom


brasX2 = 4
brasY2 = random.choice([5, 6])


listBoitesBras = []

# brasX2

for j in range(0, brasX2):
    Circulation2(-j, 0, 0.5, "brasx" + str(j))
    listBoitesBras.append("brasx" + str(j))

# brasY2

for i in range(0, brasY2):
    Circulation2(-3, -i, 0.5, "brasy" + str(i))
    listBoitesBras.append("brasy" + str(i))

# COUPOLES


def Coupoles3(pos_X, pos_Y, pos_Z, nom2):
    bpy.ops.mesh.primitive_uv_sphere_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(-0.488145, -0.488145, -0.488145))
    bpy.context.object.name = nom2


listBoitesBras2 = []

for j in range(0, brasX2):
    Coupoles3(-j, 0, 0.9, "brasx2" + str(j))
    listBoitesBras2.append("brasx2" + str(j))

for i in range(0, brasY2):
    Coupoles3(-3, -i, 0.9, "brasy2" + str(i))
    listBoitesBras2.append("brasy2" + str(i))


# --------------------------------------#
# DUPLIQUER LE MODULE DE CIRCULATION 2 #
# --------------------------------------#

# selection des cubes comprenant les bras

bpy.ops.object.select_all(action="DESELECT")

for x in range(0, len(listBoitesBras)):
    bpy.data.objects[listBoitesBras[x]].select = True

for x in range(0, len(listBoitesBras2)):
    bpy.data.objects[listBoitesBras2[x]].select = True


# duplication positive

MaxDuplication2 = random.randint(3, 5)

for nombreDuplication2 in range(1, MaxDuplication2):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (-brasX2 + 1, -brasY2 + 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# duplication negative

bpy.ops.object.select_all(action="DESELECT")

for x in range(0, len(listBoitesBras)):
    bpy.data.objects[listBoitesBras[x]].select = True

for x in range(0, len(listBoitesBras2)):
    bpy.data.objects[listBoitesBras2[x]].select = True


MaxDuplication4 = random.randint(1, 2)

for nombreDuplication2 in range(0, MaxDuplication4):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (brasX2 - 1, brasY2 - 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# _______________________________________________________________________________________________________________________#


# ---------------------#
# GENERER LE MODULE 2 #
# ---------------------#


def Module2(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))


Lou = random.randint(3, 5)

Module2(-Lou - 0.5, 1.5, 0.5)
bpy.ops.transform.resize(value=(Lou * 2, 2, 1))


# EXTRUSION

depla1 = random.randint(-2, 2)
taille1 = random.randint(2, 3)
taille2 = random.choice([1.5, 2])
taille3 = random.choice([0.5, 1])

if taille2 > 1.5:
    taille3 = 1
else:
    taille3 = 0.5

bpy.ops.object.editmode_toggle()

bpy.ops.mesh.extrude_region_move(
    MESH_OT_extrude_region={"mirror": False},
    TRANSFORM_OT_translate={
        "value": (0, 2, 0),
        "constraint_axis": (False, False, True),
        "constraint_orientation": "NORMAL",
        "mirror": False,
        "proportional": "DISABLED",
        "proportional_edit_falloff": "SMOOTH",
        "proportional_size": 1,
        "snap": False,
        "snap_target": "CLOSEST",
        "snap_point": (0, 0, 0),
        "snap_align": False,
        "snap_normal": (0, 0, 0),
        "texture_space": False,
        "remove_on_cancel": False,
        "release_confirm": False,
    },
)

bpy.ops.transform.translate(
    value=(depla1, taille2, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="VIEW",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SPHERE",
    proportional_size=1.1,
)

bpy.ops.transform.resize(
    value=(taille1 / 6, taille3, 1),
    constraint_axis=(False, False, False),
    constraint_orientation="VIEW",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SPHERE",
    proportional_size=1.1,
)

bpy.ops.object.editmode_toggle()


# -----------------------#
# DUPLIQUER LE MODULE 2 #
# -----------------------#

# (Selon le nombre Maximum de duplication du module de circulation 2 !!!)


# duplication positive

for nombreDuplication5 in range(1, MaxDuplication2 + 1):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (-brasX2 + 1, -brasY2 + 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# duplication negative

bpy.ops.object.duplicate_move(
    OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
    TRANSFORM_OT_translate={
        "value": (
            (brasX2 - 1) * (MaxDuplication2 + 1),
            (brasY2 - 1) * (MaxDuplication2 + 1),
            0,
        ),
        "constraint_axis": (False, False, False),
        "constraint_orientation": "GLOBAL",
        "mirror": False,
        "proportional": "DISABLED",
        "proportional_edit_falloff": "SMOOTH",
        "proportional_size": 1,
        "snap": False,
        "snap_target": "CLOSEST",
        "snap_point": (0, 0, 0),
        "snap_align": False,
        "snap_normal": (0, 0, 0),
        "texture_space": False,
        "remove_on_cancel": False,
        "release_confirm": False,
    },
)


for nombreDuplication6 in range(0, MaxDuplication4 - 1):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (brasX2 - 1, brasY2 - 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# -----------------------#
# DEUXIEME EXCROISSANCE #
# -----------------------#


if brasY2 is 6:
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(0, 0, 0.5),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(1, 0.5, 0.5),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.transform.translate(
        value=(-5, 0, 0),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    for nombreDuplication6 in range(1, MaxDuplication2 + 1):
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={
                "value": (-brasX2 + 1, -brasY2 + 1, 0),
                "constraint_axis": (False, False, False),
                "constraint_orientation": "GLOBAL",
                "mirror": False,
                "proportional": "DISABLED",
                "proportional_edit_falloff": "SMOOTH",
                "proportional_size": 1,
                "snap": False,
                "snap_target": "CLOSEST",
                "snap_point": (0, 0, 0),
                "snap_align": False,
                "snap_normal": (0, 0, 0),
                "texture_space": False,
                "remove_on_cancel": False,
                "release_confirm": False,
            },
        )

    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (
                (brasX2 - 1) * (MaxDuplication2 + 1),
                (brasY2 - 1) * (MaxDuplication2 + 1),
                0,
            ),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )
    for nombreDuplication6 in range(0, MaxDuplication4 - 1):
        bpy.ops.object.duplicate_move(
            OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
            TRANSFORM_OT_translate={
                "value": (brasX2 - 1, brasY2 - 1, 0),
                "constraint_axis": (False, False, False),
                "constraint_orientation": "GLOBAL",
                "mirror": False,
                "proportional": "DISABLED",
                "proportional_edit_falloff": "SMOOTH",
                "proportional_size": 1,
                "snap": False,
                "snap_target": "CLOSEST",
                "snap_point": (0, 0, 0),
                "snap_align": False,
                "snap_normal": (0, 0, 0),
                "texture_space": False,
                "remove_on_cancel": False,
                "release_confirm": False,
            },
        )


# ----------#
# COUPOLES #
# ----------#


def Coupoles4(pos_X, pos_Y, pos_Z, nom4):
    bpy.ops.mesh.primitive_uv_sphere_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(-0.488145, -0.488145, -0.488145))
    bpy.context.object.name = nom4


listBoitesBras3 = []
listBoitesBras4 = []

for w in range(1, Lou * 2 + 1):
    Coupoles4(-w, 2, 0.9, "brasx3" + str(w))
    listBoitesBras3.append("brasx3" + str(w))

for v in range(1, Lou * 2 + 1):
    Coupoles4(-v, 1, 0.9, "brasx4" + str(v))
    listBoitesBras4.append("brasx4" + str(v))


# dupliquer les coupoles

for x in range(0, len(listBoitesBras3)):
    bpy.data.objects[listBoitesBras3[x]].select = True

for x in range(0, len(listBoitesBras4)):
    bpy.data.objects[listBoitesBras4[x]].select = True

# positif

for nombreDuplication5 in range(1, MaxDuplication2 + 1):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (-brasX2 + 1, -brasY2 + 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )

# negatif


bpy.ops.object.duplicate_move(
    OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
    TRANSFORM_OT_translate={
        "value": (
            (brasX2 - 1) * (MaxDuplication2 + 1),
            (brasY2 - 1) * (MaxDuplication2 + 1),
            0,
        ),
        "constraint_axis": (False, False, False),
        "constraint_orientation": "GLOBAL",
        "mirror": False,
        "proportional": "DISABLED",
        "proportional_edit_falloff": "SMOOTH",
        "proportional_size": 1,
        "snap": False,
        "snap_target": "CLOSEST",
        "snap_point": (0, 0, 0),
        "snap_align": False,
        "snap_normal": (0, 0, 0),
        "texture_space": False,
        "remove_on_cancel": False,
        "release_confirm": False,
    },
)


for nombreDuplication6 in range(0, MaxDuplication4 - 1):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (brasX2 - 1, brasY2 - 1, 0),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "remove_on_cancel": False,
            "release_confirm": False,
        },
    )


# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#


# -----------------------------#
# CREATION DES ZONES COMMUNES #
# -----------------------------#


# Barrette gauche 1

bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(1, 4, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=(brasX + 0.5, -brasY - 2.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 3):
    for q in range(1, 8):
        Coupoles2(n + (brasX + 0.5) - 1.5, q + (-brasY / 2 - 2.5) - 6.5, 0.9, 1, 1, 1)

# __________________________________________________________________________________________#


# Barrette gauche 2

bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(1, 2, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((brasX * 2) + 0.5, (-brasY * 2) - 0.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 3):
    for q in range(1, 5):
        Coupoles2(n + (brasX * 2) - 1, q + (-brasY * 2) - 3, 0.9, 1, 1, 1)


# __________________________________________________________________________________________#


# Barrette gauche 3


test2 = random.choice([0.5, 1])


if brasX > 3:
    test2 is 1
else:
    test2 is 0.5


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(test2, 1, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((brasX * 1.5) + 0.5, (-brasY * 1.5) - 1.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# __________________________________________________________________________________________#


# Barrette gauche 4


test3 = random.choice([3, 4, 5, 6])


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(test3, 1, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((brasX * 2), (-brasY * 2.5) - 1, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

LouMarine = random.choice([9, 10])

if test3 > 5:
    LouMarine = 11

# Coupoles

for n in range(1, LouMarine):
    for q in range(1, 3):
        Coupoles2(n + (brasX * 2) - 5, q + (-brasY * 2.5) - 2.5, 0.9, 1, 1, 1)


# ________________________________________________________________________________________________________#


# Barette droite 1


test = random.choice([1, 2.5])


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(1, 4, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((-brasX2 * 1) - 0.5, (-brasY2 * 1) - 3.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 3):
    for q in range(1, 10):
        Coupoles2(n + (-brasX2 * 2) + 2, q + (-brasY2 * 2) - 3, 0.9, 1, 1, 1)


# ________________________________________________________________________________________________________#


# Barette droite 2


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(0.5, 3, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((-brasX2 * 2), (-brasY2 * 2) - 1.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 2):
    for q in range(1, 7):
        Coupoles2(n + (-brasX2 * 2) - 1, q + (-brasY2 * 2) - 5, 0.9, 1, 1, 1)


# ________________________________________________________________________________________________________#


# Barette droite 3


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(3, 1, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((-brasX2 * 1) - 0.5, (-brasY2 * 1.5) - 6, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 7):
    for q in range(1, 3):
        Coupoles2(n + (-brasX2 * 1) - 4, q + (-brasY2 * 1.5) - 7.5, 0.9, 1, 1, 1)


# ________________________________________________________________________________________________________#


# Barrete bas 1


taille1 = random.choice([1, 2])

bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(2, 2, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((brasX2 * 2) - 3.5, (brasY2 * 2) - 4.5, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


# Coupoles

for n in range(1, 5):
    for q in range(1, 5):
        Coupoles2(n + (brasX2 * 2) - 6, q + (brasY2 * 2) - 7, 0.9, 1, 1, 1)


# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#
# _______________________________________________________________________________________________________________________#


# ---------------------------------#
# CREATION DE LA GALETTE NIVEAU 1 #
# ---------------------------------#


epaisseur = random.choice([0.5, 1])

positionY = random.choice([0, 1, 2, 3, 4])

positionX = random.choice([3, 4, 5])

longueur = random.choice([5, 6])


bpy.ops.mesh.primitive_cube_add(
    view_align=False,
    enter_editmode=False,
    location=(0, 0, 0.5),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)

bpy.ops.transform.resize(
    value=(longueur, epaisseur, 0.5),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

bpy.ops.transform.translate(
    value=((-brasX2) + positionX, (-brasY2) - positionY, 1),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)


bpy.ops.object.select_all(action="DESELECT")


### FIN ###
