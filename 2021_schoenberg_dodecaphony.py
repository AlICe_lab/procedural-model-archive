# SPDX-FileCopyrightText: 2021 Matthieu Desruelles & Guillaume Pirard
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

### Dodecaphonie, Schoenberg Arnold
### DESRUELLES Matthieu, PIRARD Guillaume
### AlIce 2021-2022
### Final Jury, Python code


import bpy
import random
import math


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()


def rectangle(position, dimension):
    bpy.ops.mesh.primitive_cube_add(size=1, location=position, scale=dimension)


def sphere(position, dimension):
    bpy.ops.mesh.primitive_ico_sphere_add(
        subdivisions=0.5, location=position, scale=dimension
    )


def cone(position, dimension):
    bpy.ops.mesh.primitive_cone_add(vertices=3, location=position, scale=dimension)


# Creation of the different mesh and their transformations
for rangeX in range(0, 12):
    for rangeY in range(0, 12):
        for rangeZ in range(0, 1):
            hauteur = 5
            rectangle((rangeX * 5, rangeY * 5, rangeZ * 5), (5, 5, hauteur))


for rangeZ in range(1, 12):
    for rangeY in range(0, 12):
        for rangeX in range(0, 12):
            if random.choice([0, 0, 1]):
                rectangle((rangeX * 5, rangeY * 5, rangeZ * 5), (5, 5, hauteur))
                if rangeZ >= 2:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-10, 10)), orient_axis="X"
                    )
                if rangeZ >= 3:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-15, 15)), orient_axis="X"
                    )
                if rangeZ >= 4:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-20, 20)), orient_axis="X"
                    )
                if rangeZ >= 5:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-25, 25)), orient_axis="Y"
                    )
                if rangeZ >= 6:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-30, 30)), orient_axis="Y"
                    )
                if rangeZ >= 7:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-35, 35)), orient_axis="Y"
                    )
                if rangeZ >= 8:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-40, 40)), orient_axis="Z"
                    )
                if rangeZ >= 11:
                    hauteur = random.randint(5, 12)
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-45, 45)), orient_axis="Z"
                    )


for rangeZ in range(5, 12):
    for rangeY in range(0, 12):
        for rangeX in range(0, 12):
            if random.choice([0, 0, 0, 0, 0, 0, 0, 1]):
                sphere((rangeX * 5, rangeY * 5, rangeZ * 5), (4, 4, 4))
                if rangeZ >= 3:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-15, 15)), orient_axis="X"
                    )
                if rangeZ >= 4:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-20, 20)), orient_axis="X"
                    )
                if rangeZ >= 5:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-30, 30)), orient_axis="Y"
                    )
                if rangeZ >= 6:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-40, 40)), orient_axis="Y"
                    )
                if rangeZ >= 7:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-45, 45)), orient_axis="Z"
                    )
                if rangeZ >= 8:
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-90, 90)), orient_axis="Z"
                    )


for rangeZ in range(8, 12):
    for rangeY in range(0, 12):
        for rangeX in range(0, 12):
            if random.choice([0, 0, 0, 0, 0, 0, 0, 1]):
                cone((rangeX * 5, rangeY * 5, rangeZ * 5), (4, 4, hauteur))
                if rangeZ >= 6:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-40, 40)), orient_axis="X"
                    )
                if rangeZ >= 7:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-45, 45)), orient_axis="Y"
                    )
                if rangeZ >= 8:
                    hauteur = 5
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-90, 90)), orient_axis="Z"
                    )
                if rangeZ >= 10:
                    hauteur = random.randint(5, 6)
                    bpy.ops.transform.rotate(
                        value=math.radians(random.randint(-90, 90)), orient_axis="Z"
                    )


"""
# Put the object in a 100x100 box
bpy.context.view_layer.objects.active = bpy.context.scene.objects["Cube"]
item = 'MESH'
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.select_by_type(type=item)
bpy.ops.object.join()
bpy.data.objects["Cube"].dimensions = (100, 100, 100)
"""


# Deselect all
bpy.ops.object.select_all(action="DESELECT")
