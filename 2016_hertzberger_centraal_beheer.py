# SPDX-FileCopyrightText: 2016 Michaël Vignano
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# Import des différentes pyramides


def CopyPaste(name, newname):

    bpy.context.scene.objects.active = bpy.data.objects[name]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(newname, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


pyramideun = CopyPaste("pyramide1", "NewName")

pyramidedeux = CopyPaste("pyramide2", "NewName2")

pyramidetrois = CopyPaste("pyramide3", "NewName3")

pyramidequatre = CopyPaste("pyramide4", "NewName4")

# agregation des pyramides avec la pyramideun au centre


def agregation1(position):

    x, y, z = position

    pyramideun.location = [0, 0, 0]
    pyramideun.scale = [10, 10, 40]
    pyramideun.rotation_euler = [radians(0), radians(0), radians(0)]

    l_agregation12 = [
        (-288, +60, 0),
        (-72, +276, 0),
        (-288, -84, 0),
        (-300, -72, 0),
        (+288, +60, 0),
        (+72, +276, 0),
        (+288, -84, 0),
        (+72, -300, 0),
    ]

    l_agregation41 = [
        (-72, +420, 0),
        (-432, +600, 0),
        (-432, -84, 0),
        (-72, -444, 0),
        (+72, -444, 0),
        (+432, -84, 0),
        (+432, +60, 0),
        (+72, +420, 0),
    ]

    l_agregation31 = [
        (+72, +348, 0),
        (+360, +60, 0),
        (+360, -84, 0),
        (+72, -372, 0),
        (-72, -372, 0),
        (+360, -84, 0),
        (-360, +60, 0),
        (-72, +348, 0),
    ]

    pospyramide2 = (0, 0, 0)
    for i in range(0, 5):
        pyramidedeux.location = pospyramide2
        pyramidedeux.scale = [10, 10, 60]

        pospyramide2 = choice(l_agregation12)

        l_agregation12.remove(pospyramide2)

        print(l_agregation12)

    pospyramide3 = (0, 0, 0)
    for i in range(0, 5):
        pyramidetrois.location = pospyramide3
        pyramidetrois.scale = [10, 10, 60]

        pospyramide3 = choice(l_agregation31)

        l_agregation31.remove(pospyramide3)

        print(l_agregation31)

    pospyramide4 = (0, 0, 0)
    for i in range(0, 5):
        pyramidequatre.location = pospyramide4
        pyramidequatre.scale = [10, 10, 80]

        pospyramide4 = choice(l_agregation41)

        l_agregation41.remove(pospyramide4)

        print(l_agregation41)


# agregation1 ((0,0,0))


# agregation des pyramides avec la pyramidedeux au centre


def agregation2(position):

    x, y, z = position

    pyramidedeux.location = [0, 0, 0]
    pyramidedeux.scale = [10, 10, 60]
    pyramidedeux.rotation_euler = [radians(0), radians(0), radians(0)]

    l_agregation12 = [
        (-288, +60, 0),
        (-72, +276, 0),
        (-288, -84, 0),
        (-300, -72, 0),
        (+288, +60, 0),
        (+72, +276, 0),
        (+288, -84, 0),
        (+72, -300, 0),
    ]

    l_agregation42 = [
        (+72, +504, 0),
        (+72, -504, 0),
        (-72, +504, 0),
        (-72, -504, 0),
        (+504, +72, 0),
        (+504, -72, 0),
        (-504, +72, 0),
        (-504, -72, 0),
        (-144, +432, 0),
        (-216, +360, 0),
        (-288, +288, 0),
        (-360, +216, 0),
        (-432, +144, 0),
        (+144, -432, 0),
        (+216, -360, 0),
        (+288, -288, 0),
        (+360, -216, 0),
        (+432, -144, 0),
    ]

    l_agregation32 = [
        (-72, -432, 0),
        (-432, -722, 0),
        (-432, +72, 0),
        (-72, +432, 0),
        (+72, +432, 0),
        (+432, +72, 0),
        (+432, -72, 0),
        (+72, -432, 0),
    ]

    pospyramide = (0, 0, 0)

    for i in range(0, 2):
        pyramideun.location = pospyramide
        pyramideun.scale = [10, 10, 40]

        pospyramide = choice(l_agregation12)

        l_agregation12.remove(pospyramide)

        print(l_agregation12)

    pospyramide3 = (0, 0, 0)

    for i in range(0, 5):
        pyramidetrois.location = pospyramide3
        pyramidetrois.scale = [10, 10, 60]

        pospyramide3 = choice(l_agregation32)

        l_agregation32.remove(pospyramide3)

        print(l_agregation32)

    pospyramide4 = (0, 0, 0)
    for i in range(0, 5):
        pyramidequatre.location = pospyramide4
        pyramidequatre.scale = [10, 10, 80]

        pospyramide4 = choice(l_agregation42)

        l_agregation42.remove(pospyramide4)

        print(l_agregation42)


# agregation2((0,0,0))


# agregation des pyramides avec la pyramidetrois au centre


def agregation3(position):

    x, y, z = position

    pyramidetrois.location = [0, 0, 0]
    pyramidetrois.scale = [10, 10, 80]
    pyramidetrois.rotation_euler = [radians(0), radians(0), radians(0)]

    l_agregation43 = [
        (+72, +576, 0),
        (+576, +72, 0),
        (+576, -72, 0),
        (+72, -576, 0),
        (-72, -576, 0),
        (-576, -72, 0),
        (-576, +72, 0),
        (-72, +576, 0),
        (-504, -144, 0),
        (-432, -216, 0),
        (-360, -288, 0),
        (-288, -360, 0),
        (+504, +144, 0),
        (+432, +216, 0),
        (+360, +288, 0),
        (+288, +360, 0),
    ]

    l_agregation32 = [
        (-72, -432, 0),
        (-432, -722, 0),
        (-432, +72, 0),
        (-72, +432, 0),
        (+72, +432, 0),
        (+432, +72, 0),
        (+432, -72, 0),
        (+72, -432, 0),
    ]

    l_agregation31 = [
        (+72, +348, 0),
        (+360, +60, 0),
        (+360, -84, 0),
        (+72, -372, 0),
        (-72, -372, 0),
        (+360, -84, 0),
        (-360, +60, 0),
        (-72, +348, 0),
    ]

    pospyramide = (0, 0, 0)

    for i in range(0, 5):
        pyramideun.location = pospyramide
        pyramideun.scale = [10, 10, 40]

        pospyramide = choice(l_agregation31)

        l_agregation31.remove(pospyramide)

        print(l_agregation31)

    pospyramide2 = (0, 0, 0)
    for i in range(0, 5):
        pyramidedeux.location = pospyramide2
        pyramidedeux.scale = [10, 10, 60]

        pospyramide2 = choice(l_agregation32)

        l_agregation32.remove(pospyramide2)

        print(l_agregation32)

    pospyramide4 = (0, 0, 0)
    for i in range(0, 5):
        pyramidequatre.location = pospyramide4
        pyramidequatre.scale = [10, 10, 80]

        pospyramide4 = choice(l_agregation43)

        l_agregation43.remove(pospyramide4)

        print(l_agregation43)


# agregation3 ((0,0,0))

# agregation des pyramides avec la pyramidequatre au centre


def agregation4(position):

    x, y, z = position

    pyramidequatre.location = [0, 0, 0]
    pyramidequatre.scale = [10, 10, 100]
    pyramidequatre.rotation_euler = [radians(0), radians(0), radians(0)]

    l_agregation41 = [
        (-72, +420, 0),
        (-432, +600, 0),
        (-432, -84, 0),
        (-72, -444, 0),
        (+72, -444, 0),
        (+432, -84, 0),
        (+432, +60, 0),
        (+72, +420, 0),
    ]

    l_agregation42 = [
        (+72, +504, 0),
        (+72, -504, 0),
        (-72, +504, 0),
        (-72, -504, 0),
        (+504, +72, 0),
        (+504, -72, 0),
        (-504, +72, 0),
        (-504, -72, 0),
        (-144, +432, 0),
        (-216, +360, 0),
        (-288, +288, 0),
        (-360, +216, 0),
        (-432, +144, 0),
        (+144, -432, 0),
        (+216, -360, 0),
        (+288, -288, 0),
        (+360, -216, 0),
        (+432, -144, 0),
    ]

    l_agregation43 = [
        (+72, +576, 0),
        (+576, +72, 0),
        (+576, -72, 0),
        (+72, -576, 0),
        (-72, -576, 0),
        (-576, -72, 0),
        (-576, +72, 0),
        (-72, +576, 0),
        (-504, -144, 0),
        (-432, -216, 0),
        (-360, -288, 0),
        (-288, -360, 0),
        (+504, +144, 0),
        (+432, +216, 0),
        (+360, +288, 0),
        (+288, +360, 0),
    ]

    pospyramide = (0, 0, 0)

    for i in range(0, 5):
        pyramideun.location = pospyramide
        pyramideun.scale = [10, 10, 40]

        pospyramide = choice(l_agregation41)

        l_agregation41.remove(pospyramide)

        print(l_agregation41)

    pospyramide2 = (0, 0, 0)
    for i in range(0, 5):
        pyramidedeux.location = pospyramide2
        pyramidedeux.scale = [10, 10, 60]

        pospyramide2 = choice(l_agregation42)

        l_agregation42.remove(pospyramide2)

        print(l_agregation42)

    pospyramide3 = (0, 0, 0)
    for i in range(0, 5):
        pyramidetrois.location = pospyramide3
        pyramidetrois.scale = [10, 10, 60]

        pospyramide3 = choice(l_agregation43)

        l_agregation43.remove(pospyramide3)

        print(l_agregation43)

    """pospyramide4 = (0,0,0)
    for i in range (0,5):
        pyramidequatre.location = (pospyramide4)
        pyramidequatre.scale = [10,10,80]

        pospyramide4 = choice (l_agregation43)

        l_agregation43.remove(pospyramide4)

        print (l_agregation43)"""


# agregation4 ((0,0,0))


# choix d'un des différents type d'agregation des pyramide


def random(position):

    x, y, z = position

    l_pyramide = [agregation1, agregation2, agregation3, agregation4]

    geo = choice(l_pyramide)

    if geo == agregation1:
        agregation1((0, 0, 0))

    if geo == agregation2:
        agregation2((0, 0, 0))

    if geo == agregation3:
        agregation3((0, 0, 0))

    if geo == agregation4:
        agregation4((0, 0, 0))


random((0, 0, 0))


"""
    l_agregation11 = [(x+144,y+144,z),(x-144,y-144,z),(x+144,y-144,z),(x-144,y+144,z),(x+216,y+72,z),(x+216,y-72,z),(x-216,y+72,z),(x-216,y-72,z),(x+72,y+216,z),(x+72,y-216,z),(x-72,y+216,z),(x-72,y-216,z)]
    l_agregation12 = [(x-288,y+60),(x-72,y+276),(x-288,y-84),(x-300,y-72),(x+288,y+60),(x+72,y+276), (x+288,y-84), (x+72,y-300)]
    l_agregation13 =[(x+72,y+348),(x+360,y+60),(x+360,y-84),(x+72,y-372),(x-72,y-372),(x+360,y-84),(x-360,y+60),(x-72,y+348)]
    l_agregation14 = [(x-72,y+420),(x-432,y+600),(x-432,y-84),(x-72,y-444),(x+72,y-444),(x+432,y-84),(x+432,y+60),(x+72,y+420)]

    l_agregation21 = [(x-288,y+60),(x-72,y+276),(x-288,y-84),(x-300,y-72),(x+288,y+60),(x+72,y+276), (x+288,y-84), (x+72,y-300)]
    l_agregation22 = [(x+360,y+72),(x+360,y-72),(x-360,y+72),(x-360,y-72),(x+72,y+360),(x+72,y-360),(x-72,y+360),(x-72,y-360)]
    l_agregation23 = [(x-72,y-432),(x-432,y-722),(x-432,y+72),(x-72,y+432),(x+72,y+432),(x+432,y+72),(x+432,y-72),(x+72,y-432)]
    l_agregation24 = [(x+72,y+504),(x+72,y-504),(x-72,y+504),(x-72,y-504),(x+504,y+72),(x+504,y-72),(x-504,y+72),(x-504,y-72)]

    l_agregation31 = [(x+72,y+348),(x+360,y+60),(x+360,y-84),(x+72,y-372),(x-72,y-372),(x+360,y-84),(x-360,y+60),(x-72,y+348)]
    l_agregation32 = [(x-72,y-432),(x-432,y-722),(x-432,y+72),(x-72,y+432),(x+72,y+432),(x+432,y+72),(x+432,y-72),(x+72,y-432)]
    l_agregation33 = [(x+504,y+72),(x+504,y-72),(x-504,y+72),(x-504,y-72),(x+72,y+504),(x+72,y-504),(x-72,y+504),(x-72,y-504)]
    l_agregation34 = [(x+72,y+576),(x+576,y+72),(x+576,y-72),(x+72,y-576),(x-72,y-576),(x-576,y-72),(x-576,y+72),(x-72,y+576)]

    l_agregation41 = [(x+72,y+576),(x+576,y+72),(x+576,y-72),(x+72,y-576),(x-72,y-576),(x-576,y-72),(x-576,y+72),(x-72,y+576),]
    l_agregation42 = [(x+72,y+504),(x+72,y-504),(x-72,y+504),(x-72,y-504),(x+504,y+72),(x+504,y-72),(x-504,y+72),(x-504,y-72)]
    l_agregation43 = [(x+72,y+576),(x+576,y+72),(x+576,y-72),(x+72,y-576),(x-72,y-576),(x-576,y-72),(x-576,y+72),(x-72,y+576)]
    l_agregation44 = [(x+648,y+72),(x+648,y-72),(x-648,y+72),(x-648,y-72),(x+72,y+648),(x+72,y-648),(x-72,y+648),(x-72,y-648)]"""
