# SPDX-FileCopyrightText: 2022 Elias Chabbi & Etienne Poinas
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Ianis Xenakis: Achorripsis

import bpy
import random
import math


def clean():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete()
    bpy.ops.outliner.orphans_purge()


clean()


############################# 1ere loi de poisson ##############

import bpy

import numpy as np


def make_poisson(lam1, lam2, lam3):

    # how many cubes you want to add
    count = 10

    # wrap next part in a loop to generate more than 1 sample
    Tabbivariate1 = []
    Tabbivariate2 = []
    for i in range(100):
        a = np.random.poisson(lam1)
        b = np.random.poisson(lam2)
        c = np.random.poisson(lam3)
        Tabbivariate1.append(a + c)  # follows Poi(lam1+lam3)
        Tabbivariate2.append(b + c)  # follows Poi(lam2+lam3)

        # Tableaux
    Tabbivariate1 = np.array(Tabbivariate1)  # elements
    Tabbivariate2 = np.array(Tabbivariate2)  # elements

    hist, xedges, yedges = np.histogram2d(
        Tabbivariate1, Tabbivariate2, bins=10, range=[[0, 10], [0, 10]]
    )

    xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25, indexing="ij")
    xpos = xpos.ravel()
    ypos = ypos.ravel()
    zpos = 0

    # Construct arrays with the dimensions for the 100 bars.
    dx = dy = 0.8 * np.ones_like(zpos)
    dz = hist.ravel()

    dz = dz.reshape((10, 10))
    dz = dz / 6  # To have them smaller in depth

    return dz


dz = make_poisson(1.3, 0.6, 2.2)
count = 10


#################### FACE 1 : ARCO = NAPPE DU BAS ###################

bpy.ops.mesh.primitive_grid_add(
    enter_editmode=False, align="WORLD", location=(4.5, 4.5, 0.15), scale=(1, 1, 1)
)

glissandi = bpy.data.objects["Grid"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 5, 5))


bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")


bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


####################### FACE 2 : PERCUSSIONS = Spheres  ##################################

Percussions = bpy.data.collections.new("Percussions")


for c in range(0, count):
    for d in range(0, count):
        x = c
        y = d
        z = 9

        if dz[c][d] > 0:

            bpy.ops.mesh.primitive_uv_sphere_add(
                radius=d / (c + 1),
                enter_editmode=False,
                align="WORLD",
                location=(x, y, 1.666 + dz[c][d]),
                scale=(1, 1, 1),
            )
            bpy.ops.object.shade_smooth()


######################### FACE 3 : PIZZICATI = CONES ######

for c in range(0, count):
    for d in range(0, count):
        x = c
        y = d
        z = 9

        if dz[c][d] > 0:
            bpy.ops.mesh.primitive_cone_add(
                radius1=0.6 + (dz[c][d] / 1.2),
                radius2=0,
                depth=(dz[c][d]),
                enter_editmode=True,
                location=(x, y, 3.333 - dz[c][d] / 2),
                rotation=(0, math.pi, 0),
                scale=(0.6, 0.6, dz[c][d] / 4),
            )

            # subdivise
            bpy.ops.mesh.subdivide(
                number_cuts=20, smoothness=0, fractal=0, fractal_along_normal=0
            )
            # sharp
            bpy.context.scene.tool_settings.use_proportional_edit = True
            bpy.context.scene.tool_settings.proportional_edit_falloff = "SHARP"
            bpy.ops.object.editmode_toggle()

            bpy.ops.object.modifier_add(type="WAVE")
            bpy.context.object.modifiers["Wave"].falloff_radius = 2
            bpy.context.object.modifiers["Wave"].height = 2 + dz[c][d] * 3
            bpy.context.object.modifiers["Wave"].width = 100
            bpy.ops.object.modifier_apply(modifier="Wave")


######################### FACE 4 : CUIVRES = RUBANS #################

############################ Premiere bande ##########################
# loi de poisson
dz = make_poisson(1, 2, 3)


# grille #

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=10,
    y_subdivisions=4,
    enter_editmode=False,
    align="WORLD",
    location=(4.5, 4.5, 5),
    scale=(1, 1, 1),
)

glissandi = bpy.data.objects["Grid.001"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 1, 5))

bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")

# Epaissseur #
bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


################################### deuxieme bande ################
# loi de poisson
dz = make_poisson(3, 0.3, 3)

# grille #

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=10,
    y_subdivisions=4,
    enter_editmode=False,
    align="WORLD",
    location=(4.5, 6.5, 5),
    rotation=(0, math.pi, 0),
    scale=(1, 1, 1),
)

glissandi = bpy.data.objects["Grid.002"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 1, 5))

bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")

# Epaissseur #
bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


################################### Troisème bande ############
# loi de poisson
dz = make_poisson(0.7, 0.2, 7.2)

# grille #

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=5,
    y_subdivisions=8,
    enter_editmode=False,
    align="WORLD",
    location=(4.5, 8.5, 5),
    scale=(1, 1, 1),
)

glissandi = bpy.data.objects["Grid.003"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 1, 5))

bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")

# Epaissseur #
bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


################################### quatrieme bande ################
# loi de poisson
dz = make_poisson(1.8, 0.9, 2.9)

# Grille #

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=5,
    y_subdivisions=10,
    enter_editmode=False,
    align="WORLD",
    location=(4.5, 2.5, 5),
    rotation=(0, math.pi, 0),
    scale=(1, 1, 1),
)

glissandi = bpy.data.objects["Grid.004"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 1, 5))

bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")

# Epaissseur #
bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


################################### cinquième bande ################
# loi de poisson
dz = make_poisson(0.6, 1, 3)

# Grille #

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=7,
    y_subdivisions=4,
    enter_editmode=False,
    align="WORLD",
    location=(4.5, 0.5, 5),
    scale=(1, 1, 1),
)

glissandi = bpy.data.objects["Grid.005"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 1, 5))

bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")

# Epaissseur #
bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")


###################### FACE 5 : GLISSANDI = SPIRALES   #############

for c in range(0, count):
    for d in range(0, count):
        x = c
        y = d
        z = 9
        if dz[c][d] > 0:
            #################### Créer Spiral Archimède
            bpy.ops.curve.spirals(
                spiral_type="ARCH",
                turns=2,
                steps=22,
                edit_mode=False,
                radius=0.1,
                dif_radius=0.5,
                dif_z=(dz[c][d]) * 10,
                startlocation=(x, y, 6.666 - dz[c][d] * 13),
                rotation_euler=(0, 0, 0),
            )
            curve_object = bpy.data.objects["Spiral"]
            bpy.context.object.data.offset = 0.15
            bpy.context.object.data.extrude = 0.15
            ############# Epaisseur Spiral#######
            bpy.data.objects["Spiral"].select_set(True)
            bpy.ops.object.convert(target="MESH")
            bpy.ops.object.modifier_add(type="SOLIDIFY")
            bpy.context.object.modifiers["Solidify"].thickness = (dz[c][d]) / 2.2
            bpy.context.object.modifiers["Solidify"].offset = (dz[c][d]) / 6
            bpy.ops.object.modifier_apply(modifier="Solidify")


################### FACE 6 : HAUTBOIS = FORMES CLAP ############

for c in range(0, count):
    for d in range(0, count):
        x = c
        y = d
        z = 9

        if dz[c][d] > 0:
            bpy.ops.mesh.primitive_uv_sphere_add(
                segments=50,
                ring_count=30,
                enter_editmode=False,
                location=(x, y, 8.333 - dz[c][d] * 3),
                rotation=(0, 0, d),
                scale=(0.5, 0.5, 1),
            )

            ################# Add modifier Subdivison ##########
            bpy.ops.object.modifier_add(type="SUBSURF")
            bpy.context.object.modifiers["Subdivision"].levels = 3
            bpy.context.object.modifiers["Subdivision"].render_levels = 3
            bpy.ops.object.modifier_apply(modifier="Subdivision")

            ############# Add modifier Wave #############
            bpy.ops.object.modifier_add(type="WAVE")
            bpy.context.object.modifiers["Wave"].use_x = False

            bpy.context.object.modifiers["Wave"].falloff_radius = 0
            bpy.context.object.modifiers["Wave"].height = 1
            bpy.context.object.modifiers["Wave"].width = 4
            bpy.context.object.modifiers["Wave"].narrowness = 4
            bpy.ops.object.modifier_apply(modifier="Wave")


###################### FACE 7 : FLUTES = NAPPE DU DESSUS #########


# loi de poisson #
dz = make_poisson(1.6, 1.1, 0.5)


# grille #

bpy.ops.mesh.primitive_grid_add(
    enter_editmode=False, align="WORLD", location=(4.3, 4.5, 9.85), scale=(1, 1, 1)
)

glissandi = bpy.data.objects["Grid.006"].data

vals = []
for line in dz:
    for v in line:
        vals.append(v)

for point, z_val in zip(glissandi.vertices, vals):
    point.co.z = z_val

bpy.ops.transform.resize(value=(5, 5, 5))


bpy.ops.object.modifier_add(type="SUBSURF")
bpy.context.object.modifiers["Subdivision"].levels = 3
bpy.ops.object.modifier_apply(modifier="Subdivision")


bpy.ops.object.modifier_add(type="SOLIDIFY")
bpy.context.object.modifiers["Solidify"].thickness = 0.04
bpy.ops.object.modifier_apply(modifier="Solidify")

bpy.ops.transform.rotate(
    value=3.14159,
    orient_axis="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    constraint_axis=(True, False, False),
    proportional_size=1,
)
