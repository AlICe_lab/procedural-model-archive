# SPDX-FileCopyrightText: 2013 Johnny Leya
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER 2.68a r 58537 / MS windows64 bit


# Important activer dans Userpreferences/Addons/Mesh/Add Mesh: ANT Landscape
# Curseur au centre
# Cube parasite à effacer

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


#####################################################################################


print(
    "-----------------------------------------------Random----------------------------------------------"
)
# AbriMunition C
X = random.randint(4, 9)
Y = random.randint(4, 9)
Z = random.randint(9, 50)

XB = random.randint(6, 30)
YB = random.randint(6, 30)
ZB = random.randint(4, 8)

XC = random.randint(10, 20)
YC = random.randint(10, 20)
ZC = random.randint(6, 8)

XD = random.randint(5, 15)
YD = random.randint(5, 15)
ZD = random.randint(5, 25)

XE = random.randint(9, 30)
YE = random.randint(9, 30)
ZE = random.randint(19, 50)

HS = random.randint(0, 30)
XS = random.randint(0, 10)
YS = random.randint(0, 10)

pos_X = 1
pos_Y = 1
pos_Z = 1

d = random.randint(13, 20)
Haz = random.randint(0, 15)
za = random.randint(0, 11)
Terra = random.randint(5, 11)
DDay = random.randint(2, 8)
WallWest = random.randint(0, 19)
Doll = random.randint(0, 10)
Doll2 = random.randint(0, 10)


Prob_Ann = random.randint(0, 10)
Prob_Mort = random.randint(0, 10)

# E = random.randint(0,1) angles des ailes A
Euler = [
    -0.610865,
    -0.610865,
    -0.610865,
    -0.645772,
    -0.663225,
    -0.680679,
    -0.698132,
    -0.715585,
    -0.733038,
    -0.750492,
    -0.767945,
    -0.785398,
    -0.802852,
    -0.820305,
    -0.837758,
]
HasardE = random.randint(0, 14)
E = Euler[HasardE]
# U = random.randint(0,1) angles des ailes B
HasardU = random.randint(0, 14)
U = Euler[HasardU]

####################################################################################################################


print(
    "-----------------------------------------------Base Volumétrique----------------------------------------------"
)


def Cylindre(d, l, m, n, loclx, locly, loclz):
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=d,
        radius=1,
        depth=2,
        end_fill_type="NGON",
        view_align=False,
        enter_editmode=False,
        location=(loclx, locly, loclz),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(l, m, n),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


def Boite(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(o, p, q),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(r, s, t),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


def Sphere(d, l1, m1, n1, loclx1, locly1, loclz1):
    bpy.ops.mesh.primitive_uv_sphere_add(
        segments=d,
        ring_count=16,
        size=1,
        view_align=False,
        enter_editmode=False,
        location=(loclx1, locly1, loclz1),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(l1, m1, n1),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


print(
    "-----------------------------------------------Modificateurs----------------------------------------------"
)


def Chanfrein(c):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.subdivide(
        number_cuts=10,
        smoothness=0,
        quadtri=False,
        quadcorner="STRAIGHT_CUT",
        fractal=0,
        fractal_along_normal=0,
        seed=0,
    )
    # Bevel pas encore au point
    bpy.ops.object.modifier_add(type="BEVEL")
    bpy.context.object.modifiers["Bevel"].limit_method = "ANGLE"
    bpy.context.object.modifiers["Bevel"].angle_limit = 0.391986
    bpy.ops.mesh.vertices_smooth(repeat=2, xaxis=True, yaxis=True, zaxis=True)
    bpy.context.object.modifiers["Bevel"].width = c
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")


def ChanfreinVariante(cc):
    bpy.ops.object.modifier_add(type="BEVEL")
    bpy.context.object.modifiers["Bevel"].width = cc
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")


def Smooth(de):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.subdivide(
        number_cuts=4,
        smoothness=0,
        quadtri=False,
        quadcorner="STRAIGHT_CUT",
        fractal=0,
        fractal_along_normal=0,
        seed=0,
    )
    bpy.ops.mesh.vertices_smooth(repeat=1, xaxis=True, yaxis=True, zaxis=True)
    bpy.ops.object.editmode_toggle()


def Multiplication(g, h):
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].count = g
    bpy.context.object.modifiers["Array"].use_relative_offset = False
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].constant_offset_displace[2] = h
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")


def Soustraction(i, j):
    bpy.context.scene.objects.active = bpy.data.objects[i]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[j]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects[j]
    bpy.ops.object.delete(use_global=False)


def Union(ii):
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[ii]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")


def Joindre(jjj):
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.join()


def Deplacer(v, w, x):
    bpy.ops.transform.translate(
        value=(v, w, x),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )


def Rotation(Z1, Z2):
    bpy.context.object.rotation_euler[Z1] = Z2


def Dupliquer(m12, m13, m14):
    bpy.ops.object.duplicate_move(
        OBJECT_OT_duplicate={"linked": False, "mode": "TRANSLATION"},
        TRANSFORM_OT_translate={
            "value": (m12, m13, m14),
            "constraint_axis": (False, False, False),
            "constraint_orientation": "GLOBAL",
            "mirror": False,
            "proportional": "DISABLED",
            "proportional_edit_falloff": "SMOOTH",
            "proportional_size": 1,
            "snap": False,
            "snap_target": "CLOSEST",
            "snap_point": (0, 0, 0),
            "snap_align": False,
            "snap_normal": (0, 0, 0),
            "texture_space": False,
            "release_confirm": False,
        },
    )


def Resize(m15, m16, m17):
    bpy.ops.transform.resize(
        value=(m15, m16, m17),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )


print(
    "-----------------------------------------------Nom----------------------------------------------"
)


def Name(k):
    bpy.context.object.name = k
    bpy.context.object.data.name = k


def SelectObject(kk):
    bpy.context.scene.objects.active = bpy.data.objects[kk]


def SelectObjectContext(erd):
    bpy.context.scene.objects[erd].select = True


def Supprimer(kkk):
    bpy.ops.object.delete(use_global=False)


def Clean(pl):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.paths_clear()


#############################################################################################

print(
    "---------------------------------------------FIRE CONTROL BAS OBSERVATOIRE UNIQUE C----------------------------------------------------"
)


def FcuBase(p1):
    Boite(pos_X, pos_Y, pos_Z + (ZC), XC, YC, ZC)
    Name("FCUBASE")


def FcuAngle(p3):
    Boite(pos_X + (XC), pos_Y + (YC), pos_Z + (ZC), XC, YC / 2, ZC * 2)
    Rotation(2, -0.785398)
    Name("FcuAngleNegatif1")
    Soustraction("FCUBASE", "FcuAngleNegatif1")
    Boite(pos_X - (XC), pos_Y + (YC), pos_Z + (ZC), XC, YC / 2, ZC * 2)
    Rotation(2, 0.785398)
    Name("FcuAngleNegatif2")
    Soustraction("FCUBASE", "FcuAngleNegatif2")
    SelectObject("FCUBASE")
    ChanfreinVariante(0.1)
    Clean(1)


def FcuRetrait(p4):
    Boite(pos_X + (XC) - (Doll / 2), pos_Y - (YC), pos_Z + (ZC), XC, YC / 2.915, ZC * 2)
    Name("FcuRetraitN1")
    Soustraction("FCUBASE", "FcuRetraitN1")
    # SelectObject("FCUBASE")
    # ChanfreinVariante(0.1)


def FcuPoste(p6):
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2), XC / 2, YC / 2, ZC / 2.125)
    ChanfreinVariante(0.1)
    Name("FcuPoste")
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2) + ((ZC / 2.125) * 2), XC, YC, ZC / 2.125)
    Name("FcuPosteNegatif1")
    Rotation(1, 0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif1")
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2) + ((ZC / 2.125) * 2), XC, YC, ZC / 2.125)
    Name("FcuPosteNegatif2")
    Rotation(1, -0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif2")
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2) + ((ZC / 2.125) * 2), XC, YC, ZC / 2.125)
    Name("FcuPosteNegatif3")
    Rotation(0, 0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif3")
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2) + ((ZC / 2.125) * 2), XC, YC, ZC / 2.125)
    Name("FcuPosteNegatif4")
    Rotation(0, -0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif4")
    # Ouverture
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2) + (ZC / 8.5) + (ZC / 77), XC, YC, ZC / 8.5)
    Name("FcuPosteNegatif5")
    Soustraction("FcuPoste", "FcuPosteNegatif5")
    SelectObjectContext("FcuPoste")
    SelectObject("FcuPoste")
    Union("FCUBASE1")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUBASE1")
    Supprimer(1)
    # ouverture2
    Boite(pos_X, pos_Y, pos_Z + (ZC * 2), XC / 3, YC / 3, (ZC / 4) + (ZC / 10))
    Name("FcuPosteOuverture")
    Soustraction("FcuPoste", "FcuPosteOuverture")
    # Appuis1
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZC * 2) + (ZC / 8.5) + (ZC / 77),
        XC / 3 + (XC / 54),
        YC / 3 + (YC / 54),
        ZC / 8,
    )
    Name("FcuPosteOuverture1")
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZC * 2) + (ZC / 8.5) + (ZC / 77),
        XC / 3 + (XC / 30),
        YC / 3 + (YC / 30),
        ZC / 8,
    )
    Name("FcuPosteOuverture2")
    Union("FcuPosteOuverture1")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteOuverture1")
    Supprimer(1)
    # EvidementOuvetures
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZC * 2) + (ZC / 8.5) + (ZC / 77),
        XC,
        YC / 3 + (YC / 54),
        ZC / 2,
    )
    Name("FcuPosteVide1")
    Soustraction("FcuPosteOuverture2", "FcuPosteVide1")
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZC * 2) + (ZC / 8.5) + (ZC / 77),
        XC / 3 + (XC / 54),
        YC,
        ZC / 2,
    )
    Name("FcuPosteVide2")
    Soustraction("FcuPosteOuverture2", "FcuPosteVide2")
    SelectObjectContext("FcuPosteOuverture2")
    SelectObject("FcuPosteOuverture2")
    Union("FcuPoste")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPoste")
    Supprimer(1)
    # accès
    Boite(
        pos_X + (XC / 2),
        pos_Y - (YC / 2),
        pos_Z + (ZC) + (ZC / 9) + (ZC / 2) + 1,
        0.8,
        YC / 3 + (YC / 30),
        ZC / 2.56,
    )
    Name("FcuPostePorte1")
    Soustraction("FcuPosteOuverture2", "FcuPostePorte1")


def FcuBec(p2):
    Sphere(
        d * 10,
        XC / 2.3,
        YC / 2.3,
        ZC - 2,
        pos_X,
        pos_Y + (YC),
        pos_Z + (ZC * 2) - (ZC / 2),
    )
    Name("Chapeau")
    Cylindre(
        d * 10, XC / 2.3, YC / 2.3, ZC, pos_X, pos_Y + (YC), pos_Z + (ZC) - (ZC / 2)
    )
    Name("ChapeauBase")
    Union("Chapeau")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("Chapeau")
    Supprimer(1)
    Boite(pos_X, pos_Y, pos_Z + (ZC * 3), XC * 2, YC * 2, ZC)
    # ChanfreinVariante(0.1)
    Name("Tour Palier2")
    Soustraction("ChapeauBase", "Tour Palier2")
    # Cylindre(d*3,XC/2.3,YC/2.3, ZC/15,pos_X, pos_Y+(YC), pos_Z+(ZC*2)-ZC/1.15)
    # Cylindre(d*3,XC,YC, ZC/15,pos_X, pos_Y+(YC), pos_Z+(ZC*2)-ZC/1.15)
    Cylindre(
        d * 10, XC, YC, ZC, pos_X, pos_Y + (YC), pos_Z + (ZC) - ZC / 1.15 + (ZC / 15)
    )
    Name("OuvertureBec")
    Soustraction("ChapeauBase", "OuvertureBec")
    # Cylindre(d*3,XC/2.3,YC/2.3, ZC,pos_X, pos_Y+(YC), pos_Z+(ZC)-ZC/1.15-(ZC/15))
    Cylindre(
        d * 10,
        XC / 1.78,
        YC / 1.78,
        ZC,
        pos_X,
        pos_Y + (YC),
        pos_Z + (ZC) - ZC / 1.15 - (ZC / 15),
    )
    Name("GrosBec")
    # Cylindre(d*3,XC/2.3,YC/2.3, ZC,pos_X, pos_Y+(YC), pos_Z+(ZC)-ZC/1.15+(ZC/15))
    # Cylindre(d*3,XC/2.3,YC/2.3, ZC/2.7,pos_X, pos_Y+(YC), pos_Z+(ZC)-ZC/1.15-(ZC/15)+(ZC/2.7))
    # Cylindre(d*3,XC/2.3,YC/2.3, ZC,pos_X, pos_Y+(YC), pos_Z+(ZC)-ZC/1.15-(ZC/15))
    Boite(pos_X, pos_Y, pos_Z - (ZC) + (ZC / 3.34), XC * 2, YC * 2, ZC)
    Name("BasBec")
    Soustraction("GrosBec", "BasBec")
    SelectObjectContext("GrosBec")
    SelectObject("GrosBec")
    Union("ChapeauBase")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("ChapeauBase")
    Supprimer(1)
    Name("BecBrut")
    # Ouverture
    Cylindre(
        10,
        XC / 2.8,
        YC / 2.8,
        ZC / 9,
        pos_X,
        pos_Y + (YC),
        pos_Z + (ZC * 2) - ZC / 1.15,
    )
    Name("BecDent1")
    Cylindre(10, XC, YC, ZC / 22, pos_X, pos_Y + (YC), pos_Z + (ZC * 2) - ZC / 1.15)
    Name("BecDent1Negatif")
    Soustraction("BecDent1", "BecDent1Negatif")
    SelectObjectContext("BecDent1")
    SelectObject("BecDent1")
    Union("BecBrut")
    SelectObjectContext("BecDent1")
    SelectObject("BecDent1")
    Name("BecBrut1")
    SelectObject("BecBrut")
    Cylindre(
        10, XC / 3, YC / 3, ZC / 9, pos_X, pos_Y + (YC), pos_Z + (ZC * 2) - ZC / 1.15
    )
    Name("BecDent2")
    Cylindre(10, XC, YC, ZC / 32, pos_X, pos_Y + (YC), pos_Z + (ZC * 2) - ZC / 1.15)
    Name("BecDent1Negati2")
    Soustraction("BecDent2", "BecDent1Negati2")
    SelectObjectContext("BecDent2")
    SelectObject("BecDent2")
    Union("BecBrut1")
    SelectObjectContext("BecDent2")
    SelectObject("BecDent2")
    Name("BecBrut2")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("BecBrut1")
    SelectObjectContext("BecBrut1")
    Supprimer(1)
    SelectObject("BecBrut")
    SelectObjectContext("BecBrut")
    Supprimer(1)
    # Evidement
    Cylindre(
        10,
        XC / 3.5,
        YC / 3.5,
        ZC / 6,
        pos_X,
        pos_Y + (YC),
        pos_Z + (ZC * 2) - ZC / 1.15,
    )
    Name("BecBrutNegatif")
    Soustraction("BecBrut2", "BecBrutNegatif")
    SelectObjectContext("BecBrut2")
    SelectObject("BecBrut2")
    Union("FCUBASE")
    Name("FCUBASE1")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("FCUBASE")
    SelectObjectContext("FCUBASE")
    Supprimer(1)


print(
    "---------------------------------------------DEFINITION FIRE CONTROL BAS OBSERVATOIRE UNIQUE C----------------------------------------------------"
)


def FcbC(p5):
    FcuBase(1)
    FcuRetrait(1)
    if XC >= 12:
        FcuAngle(1)
    else:
        SelectObject("FCUBASE")
        ChanfreinVariante(0.1)

    FcuBec(1)
    FcuPoste(1)


# FcbC(1)

#############################################################################################

print(
    "---------------------------------------------FIRE CONTROL BAS OBSERVATOIRE MULTIPLE ----------------------------------------------------"
)


def FcuBaseM(p8):
    Boite(pos_X, pos_Y, pos_Z + (ZD), XD, YD, ZD)
    Name("FCUBASEM")


###########################VARIANTEPOSITIONPOSTEDOBSERVATION#######################################################


def FcuMPoste(
    LOCX,
    LOCY,
    LOCZ,
    LOCZ1,
    LOCZ2,
    DIMX1,
    DIMY1,
    DIMZ1,
    DIMX2,
    DIMY2,
    DIMZ2,
    DIMZ3,
    DIMX4,
    DIMY4,
    DIMZ4,
    NomBase,
    LOCZ3,
    DIMX5,
    DIMY5,
    DIMZ5,
    DIMX6,
    DIMY6,
    DIMZ6,
):
    Boite(LOCX, LOCY, LOCZ, DIMX1, DIMY1, DIMZ1)
    ChanfreinVariante(0.1)
    Name("FcuPoste")
    Boite(LOCX, LOCY, LOCZ + LOCZ1, DIMX2, DIMY2, DIMZ2)
    Name("FcuPosteNegatif1")
    Rotation(1, 0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif1")
    Boite(LOCX, LOCY, LOCZ + LOCZ1, DIMX2, DIMY2, DIMZ2)
    Name("FcuPosteNegatif2")
    Rotation(1, -0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif2")
    Boite(LOCX, LOCY, LOCZ + LOCZ1, DIMX2, DIMY2, DIMZ2)
    Name("FcuPosteNegatif3")
    Rotation(0, 0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif3")
    Boite(LOCX, LOCY, LOCZ + LOCZ1, DIMX2, DIMY2, DIMZ2)
    Name("FcuPosteNegatif4")
    Rotation(0, -0.0698132)
    Soustraction("FcuPoste", "FcuPosteNegatif4")
    Boite(LOCX, LOCY, LOCZ + LOCZ2, DIMX2, DIMY2, DIMZ3)
    Name("FcuPosteNegatif5")
    Soustraction("FcuPoste", "FcuPosteNegatif5")
    SelectObjectContext("FcuPoste")
    SelectObject("FcuPoste")
    Union(NomBase)
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext(NomBase)
    Supprimer(1)
    # ouverture2
    Boite(LOCX, LOCY, LOCZ, DIMX4, DIMY4, DIMZ4)
    Name("FcuPosteOuverture")
    Soustraction("FcuPoste", "FcuPosteOuverture")
    # ICIAppuis1
    Boite(LOCX, LOCY, LOCZ + LOCZ3, DIMX5, DIMY5, DIMZ5)
    Name("FcuPosteOuverture1")
    Boite(LOCX, LOCY, LOCZ + LOCZ3, DIMX5, DIMY6, DIMZ5)
    Name("FcuPosteOuverture2")
    Union("FcuPosteOuverture1")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteOuverture1")
    Supprimer(1)
    # EvidementOuvetures
    Boite(LOCX, LOCY, LOCZ + LOCZ3, DIMX1, DIMY5, DIMZ6)
    Name("FcuPosteVide1")
    Soustraction("FcuPosteOuverture2", "FcuPosteVide1")
    Boite(LOCX, LOCY, LOCZ + LOCZ3, DIMX6, DIMY1 * 2, DIMZ6)
    Name("FcuPosteVide2")
    Soustraction("FcuPosteOuverture2", "FcuPosteVide2")
    SelectObjectContext("FcuPosteOuverture2")
    SelectObject("FcuPosteOuverture2")
    Union("FcuPoste")
    Clean(1)
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPoste")
    Supprimer(1)


def FcuPosteMB(p6):
    Boite(pos_X, pos_Y, pos_Z + (ZD * 2) + (ZD / 6), XD / 2, YD / 2, ZD / 6)
    ChanfreinVariante(0.1)
    Name("FcuPoste")
    Boite(pos_X, pos_Y, pos_Z + (ZD * 2), XD, YD, ZD / 6)
    Name("FcuPosteNegatif1")
    Soustraction("FcuPoste", "FcuPosteNegatif1")
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZD * 2),
        (XD / 2) / 1.38,
        (YD / 2) / 1.38,
        (ZD / 6) + (ZD / 12),
    )
    Name("FcuPosteDetail")
    Union("FcuPoste")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPoste")
    Supprimer(1)
    Boite(pos_X, pos_Y, pos_Z + (ZD * 2) + (ZD / 12), XD * 2, YD, ZD / 18)
    Name("FcuPosteNegatif2")
    Soustraction("FcuPosteDetail", "FcuPosteNegatif2")
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZD * 2),
        (XD / 2) / 2.173,
        (YD / 2) / 2.173,
        (ZD / 6) + (ZD / 16),
    )
    Name("FcuPosteVide")
    Soustraction("FcuPosteDetail", "FcuPosteVide")
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZD * 2),
        (XD / 2) / 2.6,
        (YD / 2) / 2.6,
        (ZD / 6) + (ZD / 32),
    )
    Name("FcuPosteVide2")
    Soustraction("FCUBASEM", "FcuPosteVide2")
    # Appuis
    Boite(
        pos_X,
        pos_Y,
        pos_Z + (ZD * 2),
        (XD / 2) / 1.59,
        (YD / 2) / 1.59,
        (ZD / 6) + (ZD / 14),
    )
    Name("FcuPosteAppuiExt")
    # AppuisNegatifs
    Boite(pos_X, pos_Y, pos_Z + (ZD * 2), XD * 2, (YD / 2) / 1.87, ZD)
    Name("FcuPosteAppuiNegatif")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif")
    Boite(pos_X, pos_Y, pos_Z + (ZD * 2), (XD / 2) / 1.87, YD * 2, ZD)
    Name("FcuPosteAppuiNegatif2")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif2")
    SelectObject("FcuPosteDetail")
    Union("FcuPosteAppuiExt")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteAppuiExt")
    Supprimer(1)
    SelectObject("FCUBASEM")
    Clean(1)
    Name("FCUTourMB")
    Union("FcuPosteDetail")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteDetail")
    Supprimer(1)


def FcuPosteMC(p6):
    Boite(pos_X, pos_Y - (YD / 2), pos_Z + (ZD * 2) + (ZD / 6), XD, YD / 2, ZD / 6)
    ChanfreinVariante(0.1)
    Name("FcuPoste")
    Boite(pos_X, pos_Y - (YD / 2), pos_Z + (ZD * 2), XD * 2, YD, ZD / 6)
    Name("FcuPosteNegatif1")
    Soustraction("FcuPoste", "FcuPosteNegatif1")
    Boite(
        pos_X,
        pos_Y - (YD / 2),
        pos_Z + (ZD * 2),
        XD / 1.38,
        (YD / 2) / 1.38,
        (ZD / 6) + (ZD / 12),
    )
    Name("FcuPosteDetail")
    Union("FcuPoste")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPoste")
    Supprimer(1)
    Boite(pos_X, pos_Y - (YD / 2), pos_Z + (ZD * 2) + (ZD / 12), XD * 2, YD, ZD / 18)
    Name("FcuPosteNegatif2")
    Soustraction("FcuPosteDetail", "FcuPosteNegatif2")
    Boite(
        pos_X,
        pos_Y - (YD / 2),
        pos_Z + (ZD * 2),
        XD / 2.173,
        (YD / 2) / 2.173,
        (ZD / 6) + (ZD / 16),
    )
    Name("FcuPosteVide")
    Soustraction("FcuPosteDetail", "FcuPosteVide")
    Boite(
        pos_X,
        pos_Y - (YD / 2),
        pos_Z + (ZD * 2),
        XD / 2.6,
        (YD / 2) / 2.6,
        (ZD / 6) + (ZD / 32),
    )
    Name("FcuPosteVide2")
    Clean(1)
    Soustraction("FCUBASEM", "FcuPosteVide2")
    # Appuis
    Boite(
        pos_X,
        pos_Y - (YD / 2),
        pos_Z + (ZD * 2),
        XD / 1.59,
        (YD / 2) / 1.59,
        (ZD / 6) + (ZD / 14),
    )
    Name("FcuPosteAppuiExt")
    # AppuisNegatifs
    Boite(pos_X, pos_Y - (YD / 2), pos_Z + (ZD * 2), XD * 2, (YD / 2) / 1.87, ZD)
    Name("FcuPosteAppuiNegatif")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif")
    Boite(pos_X, pos_Y - (YD / 2), pos_Z + (ZD * 2), XD / 1.87, YD * 2, ZD)
    Name("FcuPosteAppuiNegatif2")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif2")
    SelectObject("FcuPosteDetail")
    Union("FcuPosteAppuiExt")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteAppuiExt")
    Supprimer(1)
    SelectObject("FCUBASEM")
    Clean(1)
    Name("FCUTourMC")
    Union("FcuPosteDetail")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteDetail")
    Supprimer(1)


def FcuPosteMD(p6):
    # Boite(pos_X, pos_Y-(YD/4), pos_Z+(ZD*3),XD,YD-(YD/4),ZD)
    Boite(
        pos_X, pos_Y - (YD / 4), pos_Z + (ZD * 2) + (ZD / 6), XD, YD - (YD / 4), ZD / 6
    )
    ChanfreinVariante(0.1)
    Name("FcuPoste")
    Boite(pos_X, pos_Y - (YD / 4), pos_Z + (ZD * 2), XD * 2, YD * 2, ZD / 6)
    Name("FcuPosteNegatif1")
    Soustraction("FcuPoste", "FcuPosteNegatif1")
    Boite(
        pos_X,
        pos_Y - (YD / 4),
        pos_Z + (ZD * 2),
        XD / 1.38,
        (YD - (YD / 4)) / 1.38,
        (ZD / 6) + (ZD / 12),
    )
    Name("FcuPosteDetail")
    Union("FcuPoste")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPoste")
    Supprimer(1)
    Boite(pos_X, pos_Y - (YD / 4), pos_Z + (ZD * 2) + (ZD / 12), XD * 2, YD, ZD / 18)
    Name("FcuPosteNegatif2")
    Soustraction("FcuPosteDetail", "FcuPosteNegatif2")
    Boite(
        pos_X,
        pos_Y - (YD / 4),
        pos_Z + (ZD * 2),
        XD / 2.173,
        (YD - (YD / 4)) / 2.173,
        (ZD / 6) + (ZD / 16),
    )
    Name("FcuPosteVide")
    Soustraction("FcuPosteDetail", "FcuPosteVide")
    Boite(
        pos_X,
        pos_Y - (YD / 4),
        pos_Z + (ZD * 2),
        XD / 2.6,
        (YD - (YD / 4)) / 2.6,
        (ZD / 6) + (ZD / 32),
    )
    Name("FcuPosteVide2")
    Soustraction("FCUBASEM", "FcuPosteVide2")
    # Appuis
    Boite(
        pos_X,
        pos_Y - (YD / 4),
        pos_Z + (ZD * 2),
        XD / 1.59,
        (YD - (YD / 4)) / 1.59,
        (ZD / 6) + (ZD / 14),
    )
    Name("FcuPosteAppuiExt")
    # AppuisNegatifs
    Boite(pos_X, pos_Y - (YD / 4), pos_Z + (ZD * 2), XD * 2, (YD - (YD / 4)) / 1.87, ZD)
    Name("FcuPosteAppuiNegatif")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif")
    Boite(pos_X, pos_Y - (YD / 4), pos_Z + (ZD * 2), XD / 1.87, YD * 2, ZD)
    Name("FcuPosteAppuiNegatif2")
    Soustraction("FcuPosteAppuiExt", "FcuPosteAppuiNegatif2")
    SelectObject("FcuPosteDetail")
    Union("FcuPosteAppuiExt")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteAppuiExt")
    Supprimer(1)
    SelectObject("FCUBASEM")
    Clean(1)
    Name("FCUTourMD")
    Union("FcuPosteDetail")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FcuPosteDetail")
    Supprimer(1)


###########################VARIANTEPOSITIONPOSTEDOBSERVATIONMC#######################################################


def FcuExtensionM(p10):
    Boite(
        pos_X, pos_Y + YD + (YD / 2.72), pos_Z + (ZD / 1.32), XD, YD / 2.72, ZD / 1.32
    )
    Name("FCUEXTENSION1")
    Union("FCUTourMC")
    Clean(1)
    Name("FCUTourMCE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUTourMC")
    Supprimer(1)
    # Rajout
    Boite(
        pos_X,
        pos_Y + YD + ((YD / 2.72)) * 2 + (YD / 10.41),
        pos_Z + (ZD / 1.32),
        XD,
        YD / 10.41,
        ZD / 1.32,
    )
    Name("FCUEXTENSION2")
    Union("FCUTourMCE")
    Clean(1)
    Name("FCUTourMCE1")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUTourMCE")
    Supprimer(1)


def FcuExtensionDentM(p11):
    Boite(
        pos_X,
        pos_Y + YD,
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD * 2,
        YD / 1.36,
        ZD / 8.33,
    )
    Name("FCUDOUVERTURENENEGATIF1")
    Soustraction("FCUTourMCE1", "FCUDOUVERTURENENEGATIF1")
    # DEUXIEMEOUVERTURE
    Boite(
        pos_X,
        pos_Y + YD + ((YD / 2.72) * 2),
        pos_Z + (ZD) + (ZD / 3.29),
        XD * 2,
        (YD / 4.03) * 2,
        ZD / 11.03,
    )
    Name("FCUDOUVERTURENENEGATIF2")
    Soustraction("FCUTourMCE1", "FCUDOUVERTURENENEGATIF2")
    # Dents de ces ouvertures
    # Boite(pos_X, pos_Y+YD/2-(YD/9.6), pos_Z+((ZD/1.32)*2)+(ZD/8.33),XD/2.21,YD/2,ZD/7)
    Boite(
        pos_X,
        pos_Y + YD / 2 - (YD / 25),
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD / 1.5,
        YD / 2,
        ZD / 7,
    )
    Name("FCUDDENT1")
    Boite(
        pos_X,
        pos_Y + YD / 2 - (YD / 25),
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD,
        YD,
        ZD / 8.33 - (ZD / 113.64),
    )
    Name("FCUDDENT1Negatif")
    Soustraction("FCUDDENT1", "FCUDDENT1Negatif")
    Boite(
        pos_X,
        pos_Y + YD / 2 - (YD / 14.11),
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD / 1.8 - (XD / 20.86),
        YD / 2,
        ZD / 7,
    )
    Name("FCUDDENT2")
    Boite(
        pos_X,
        pos_Y + YD / 2 - (YD / 14.11),
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD,
        YD,
        ZD / 8.33 - (ZD / 56.82),
    )
    Name("FCUDDENT2Negatif")
    Soustraction("FCUDDENT2", "FCUDDENT2Negatif")
    SelectObject("FCUDDENT2")
    Union("FCUDDENT1")
    Clean(1)
    SelectObjectContext("FCUDDENT1")
    Supprimer(1)
    SelectObject("FCUTourMCE1")
    Union("FCUDDENT2")
    Clean(1)
    SelectObjectContext("FCUDDENT2")
    Supprimer(1)
    Boite(
        pos_X,
        pos_Y + YD / 2 - (YD / 9.6),
        pos_Z + ((ZD / 1.32) * 2) + (ZD / 8.33),
        XD / 2.21,
        YD / 2,
        ZD / 5,
    )
    Name("FCUDDENT3")
    Soustraction("FCUTourMCE1", "FCUDDENT3")


def FcuExtensionDentM2(p11):
    # Boite(pos_X, pos_Y+YD/2-(YD/25), pos_Z+((ZD/1.32)*2)+(ZD/8.33),XD,YD,ZD/8.33-(ZD/113.64))
    Boite(
        pos_X,
        pos_Y + YD + (YD / 2.72) - (YD / 25),
        pos_Z + (ZD) + (ZD / 3.29),
        XD / 2.15,
        YD / 2.72,
        ZD / 9,
    )
    Name("FCUDDENT1")
    Boite(
        pos_X,
        pos_Y + YD + (YD / 2.72) - (YD / 25),
        pos_Z + (ZD) + (ZD / 3.29),
        XD,
        YD,
        ZD / 11.03 - (ZD / 113.64),
    )
    Name("FCUDDENT1Negatif")
    Soustraction("FCUDDENT1", "FCUDDENT1Negatif")
    Boite(
        pos_X,
        pos_Y + YD + (YD / 2.72) - (YD / 14.11),
        pos_Z + (ZD) + (ZD / 3.29),
        XD / 2.15 - (XD / 20.86),
        YD / 2.72,
        ZD / 9,
    )
    Name("FCUDDENT2")
    Boite(
        pos_X,
        pos_Y + YD + (YD / 2.72) - (YD / 14.11),
        pos_Z + (ZD) + (ZD / 3.29),
        XD,
        YD,
        ZD / 11.03 - (ZD / 56.82),
    )
    Name("FCUDDENT2Negatif")
    Soustraction("FCUDDENT2", "FCUDDENT2Negatif")
    SelectObject("FCUDDENT2")
    Union("FCUDDENT1")
    Clean(1)
    SelectObjectContext("FCUDDENT1")
    Supprimer(1)
    SelectObject("FCUTourMCE1")
    Union("FCUDDENT2")
    Clean(1)
    SelectObjectContext("FCUDDENT2")
    Supprimer(1)
    Boite(
        pos_X,
        pos_Y + YD + (YD / 2.72) - (YD / 9.6),
        pos_Z + (ZD) + (ZD / 3.29),
        XD / 4.05,
        YD / 2.72,
        ZD / 8,
    )
    Name("FCUDDENT3")
    Soustraction("FCUTourMCE1", "FCUDDENT3")


###########################VARIANTESFACEMC#######################################################


def FcuObliqueM(p8):
    # HAUT
    Boite(
        pos_X + XD + (XD / 2) - (XD / 6),
        pos_Y + (YD),
        pos_Z + (ZD * 2),
        XD / 2,
        YD * 2,
        ZD / 3,
    )
    Name("FCUObliqueM1")
    Rotation(2, 0.20944)
    Soustraction("FCUTourMCE1", "FCUObliqueM1")
    # Gauche
    Boite(
        pos_X - XD - (XD / 2) + (XD / 6),
        pos_Y + (YD),
        pos_Z + (ZD * 2),
        XD / 2,
        YD * 2,
        ZD / 3,
    )
    Name("FCUObliqueM2")
    Rotation(2, -0.20944)
    Soustraction("FCUTourMCE1", "FCUObliqueM2")
    # BAS
    # Reprendre pour l'autre
    Boite(
        pos_X + XD + (XD / 2) - (XD / 6),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 4.03) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2Droit")
    Soustraction("FCUTourMCE1", "FCUObliqueM2Droit")
    Boite(
        pos_X - XD - (XD / 2) + (XD / 6),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 4.03) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2GAUCHE")
    Soustraction("FCUTourMCE1", "FCUObliqueM2GAUCHE")
    # HAUT
    Boite(
        pos_X,
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD,
        YD / 5.205,
        ZD / 7,
    )
    Name("FCUObliqueDevant")
    Soustraction("FCUTourMCE1", "FCUObliqueDevant")
    ####Fin reprise
    Boite(
        pos_X + XD + (XD / 2) - (XD / 3),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 6) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2Droit")
    Rotation(2, 0.20944)
    Soustraction("FCUTourMCE1", "FCUObliqueM2Droit")
    # Gauche
    Boite(
        pos_X - XD - (XD / 2) + (XD / 3),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 6) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2Gauche")
    Rotation(2, -0.20944)
    Soustraction("FCUTourMCE1", "FCUObliqueM2Gauche")
    # Nettoyage
    SelectObject("FCUTourMCE1")
    SelectObjectContext("FCUTourMCE1")
    Clean(1)


def FcuDroitM(p8):
    Boite(
        pos_X + XD + (XD / 2) - (XD / 6),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 4.03) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2Droit")
    Soustraction("FCUTourMCE1", "FCUObliqueM2Droit")
    Boite(
        pos_X - XD - (XD / 2) + (XD / 6),
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD / 2,
        (YD / 4.03) * 2 + ((YD / 10.41) * 2),
        ZD / 7,
    )
    Name("FCUObliqueM2GAUCHE")
    Soustraction("FCUTourMCE1", "FCUObliqueM2GAUCHE")
    # HAUT
    Boite(
        pos_X,
        pos_Y + YD + ((YD / 2.72)) * 2 + ((YD / 10.41) * 2),
        pos_Z + ((ZD / 1.32) * 2),
        XD,
        YD / 5.205,
        ZD / 7,
    )
    Name("FCUObliqueDevant")
    Soustraction("FCUTourMCE1", "FCUObliqueDevant")
    SelectObject("FCUTourMCE1")
    SelectObjectContext("FCUTourMCE1")
    Clean(1)


###########################VARIANTESFACEMC2#######################################################


def FcuExtensionBas(d):
    Boite(
        pos_X + XD + (XD / 5.705),
        pos_Y
        + (YD / 2.72)
        + (YD / 10.41)
        - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        pos_Z + (ZD / 1.32),
        (XD / 5.705),
        YD + (YD / 2.72) + (YD / 10.41) - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        ZD / 1.32,
    )
    Name("FCUEXTDroiteM")
    # Gauche
    Boite(
        pos_X - XD - (XD / 5.705),
        pos_Y
        + (YD / 2.72)
        + (YD / 10.41)
        - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        pos_Z + (ZD / 1.32),
        (XD / 5.705),
        YD + (YD / 2.72) + (YD / 10.41) - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        ZD / 1.32,
    )
    Name("FCUEXTGaucheM")
    # arriere
    Boite(
        pos_X,
        pos_Y - YD - (YD / 2),
        pos_Z + (ZD / 1.32),
        XD + ((XD / 5.705) * 2),
        YD / 2,
        ZD / 1.32,
    )
    Name("FCUEXTArriereM")
    Union("FCUEXTDroiteM")
    Clean(1)
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUEXTDroiteM")
    Supprimer(1)
    SelectObject("FCUEXTArriereM")
    Union("FCUEXTGaucheM")
    Clean(1)
    SelectObjectContext("FCUEXTGaucheM")
    Supprimer(1)
    # angles avant
    Boite(
        pos_X + XD + ((XD / 5.705) * 2),
        pos_Y
        + (YD / 2.72)
        + (YD / 10.41)
        - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2)
        + (
            YD
            + (YD / 2.72)
            + (YD / 10.41)
            - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2)
        ),
        pos_Z + (ZD / 1.32),
        (XD / 5.705),
        YD + (YD / 2.72) + (YD / 10.41) - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        ZD,
    )
    Name("FCUEXTAvantDM")
    Rotation(2, 0.785398)
    Soustraction("FCUEXTArriereM", "FCUEXTAvantDM")
    # GAuche
    Boite(
        pos_X - XD - ((XD / 5.705) * 2),
        pos_Y
        + (YD / 2.72)
        + (YD / 10.41)
        - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2)
        + (
            YD
            + (YD / 2.72)
            + (YD / 10.41)
            - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2)
        ),
        pos_Z + (ZD / 1.32),
        (XD / 5.705),
        YD + (YD / 2.72) + (YD / 10.41) - (((YD / 4.03) * 2 + ((YD / 10.41) * 2)) / 2),
        ZD,
    )
    Name("FCUEXTAvantGM")
    Rotation(2, -0.785398)
    Soustraction("FCUEXTArriereM", "FCUEXTAvantGM")
    SelectObject("FCUTourMCE1")
    Union("FCUEXTArriereM")
    Clean(1)
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUEXTArriereM")
    SelectObject("FCUEXTArriereM")
    Supprimer(1)
    SelectObjectContext("FCUTourMCE1")
    SelectObject("FCUTourMCE1")
    Name("Regelbau S414")
    # METTTTTTRE SMOOTH SI ENVIEDE
    # Smooth(1)


def FcuExtensionBasB(d):
    Boite(pos_X, pos_Y - YD, pos_Z + (ZD / 1.32), XD + (XD / 2), YD, ZD / 1.32)
    Name("Regelbau M157")
    ChanfreinVariante(0.1)
    Union("FCUTourMCE1")
    Clean(1)
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("FCUTourMCE1")
    SelectObject("FCUTourMCE1")
    Supprimer(1)


###########################VARIANTEPOSITIONPOSTEDOBSERVATIONMB#######################################################


def FcuArriereMB(p24):
    Boite(pos_X + XD, pos_Y - YD - 0.5, pos_Z + (ZD), XD, YD / 2, ZD * 2)
    Name("FCUARRNegatif1")
    Soustraction("FCUTourMA", "FCUARRNegatif1")
    # oblique
    Boite(pos_X + 1, pos_Y - YD - 0.5 - (YD / 3), pos_Z + (ZD), YD / 2, YD / 4, ZD * 2)
    Name("FCUARRNegatif2")
    bpy.context.object.rotation_euler[2] = -0.994838
    Soustraction("FCUTourMA", "FCUARRNegatif2")


def PortearriereMB(p26):
    Boite(pos_X + (XD / 2), pos_Y - (YD / 2) - 1, pos_Z + (ZD) + (ZD / 2), 1, 2, 2)
    Name("FCUPorteNegatif")
    Soustraction("FCUTourMA", "FCUPorteNegatif")


def EscalierTubeMB(p25):
    Cylindre(
        20,
        2.3,
        1.3,
        ZD - (ZD / 5),
        pos_X + XD,
        pos_Y + (YD / 2) + (YD / 4),
        pos_Z + ZD + (ZD / 5),
    )
    Name("FCUTubeMB")
    Cylindre(
        20,
        2,
        1,
        ZD - (ZD / 5),
        pos_X + XD,
        pos_Y + (YD / 2) + (YD / 4),
        pos_Z + ZD + (ZD / 5) + 1,
    )
    Name("FCUTubeNegatifMB")
    Soustraction("FCUTubeMB", "FCUTubeNegatifMB")
    SelectObject("FCUTourMA")
    Clean(1)
    Union("FCUTubeMB")
    Clean(1)
    SelectObject("FCUTubeMB")
    SelectObjectContext("FCUTubeMB")
    Supprimer(1)


def FcuOrc(p27):
    Boite(pos_X, pos_Y + YD + (YD / 4) - 1, pos_Z + (ZD), XD / 2, YD / 4 + 1, ZD)
    Name("FCUOrc")
    bpy.ops.object.modifier_add(type="BEVEL")
    bpy.context.object.modifiers["Bevel"].width = 0.26
    bpy.context.object.modifiers["Bevel"].segments = 6
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")
    # Negatiforc
    Boite(pos_X, pos_Y + YD + (YD / 4) - 1, pos_Z + (ZD * 2) - (ZD / 1.35), XD, YD, 1)
    Name("FCUOrcNegatif1")
    # Boite(pos_X, pos_Y+YD+(YD/4)-1, pos_Z+(ZD*2)-(ZD/1.35),XD/2,YD/4+1,1)
    Soustraction("FCUOrc", "FCUOrcNegatif1")
    # DentitionOrc
    Boite(
        pos_X,
        pos_Y + YD + (YD / 4) - 1,
        pos_Z + (ZD * 2) - (ZD / 1.35),
        XD / 2 - ((XD / 2) / 4.5),
        YD / 4 + 1 - ((XD / 2) / 4.5),
        2,
    )
    Name("FCUOrcDents2")
    # DentitionOrcNegatif2
    Boite(pos_X, pos_Y + YD + (YD / 4) - 1, pos_Z + (ZD * 2) - (ZD / 1.35), XD, YD, 0.8)
    Name("FCUOrcDents2Negatif")
    Soustraction("FCUOrcDents2", "FCUOrcDents2Negatif")
    SelectObject("FCUOrc")
    Clean(1)
    Union("FCUOrcDents2")
    Clean(1)
    SelectObject("FCUOrcDents2")
    SelectObjectContext("FCUOrcDents2")
    Supprimer(1)
    # DentitionOrc3
    Boite(
        pos_X,
        pos_Y + YD + (YD / 4) - 1,
        pos_Z + (ZD * 2) - (ZD / 1.35),
        XD / 2 - ((XD / 2) / 6),
        YD / 4 + 1 - ((XD / 2) / 6),
        2,
    )
    Name("FCUOrcDents3")
    # DentitionOrcNegatif3
    Boite(pos_X, pos_Y + YD + (YD / 4) - 1, pos_Z + (ZD * 2) - (ZD / 1.35), XD, YD, 0.9)
    Name("FCUOrcDents3Negatif")
    Soustraction("FCUOrcDents3", "FCUOrcDents3Negatif")
    SelectObject("FCUOrc")
    Clean(1)
    Union("FCUOrcDents3")
    Clean(1)
    SelectObject("FCUOrcDents3")
    SelectObjectContext("FCUOrcDents3")
    Supprimer(1)
    # Union et trou
    SelectObject("FCUTourMA")
    Name("RegelbauM162")
    Clean(1)
    Union("FCUOrc")
    Clean(1)
    SelectObject("FCUOrc")
    SelectObjectContext("FCUOrc")
    Supprimer(1)
    # Derniertrou
    Boite(
        pos_X,
        pos_Y + YD + (YD / 4) - 1,
        pos_Z + (ZD * 2) - (ZD / 1.35),
        XD / 2 - ((XD / 2) / 3),
        YD / 4 + 1 - ((XD / 2) / 3),
        2,
    )
    Name("FCUOrcDents1")
    Soustraction("RegelbauM162", "FCUOrcDents1")


# Appel fonction pour test#######################################################

"""FcuBaseM(1)
FcuMPoste(pos_X, pos_Y, pos_Z+(ZD*2),+((ZD/2.125)*2),+(ZD/8.5)+(ZD/77),XD,YD/2,ZD/2.125,XD*2,YD,ZD/2.125,ZD/8.5,XD-(XD/5),YD/3,(ZD/4)+(ZD/10),"FCUBASEM",+(ZD/8.5)+(ZD/77),(XD-(XD/5))+(XD/54),YD/3+(YD/54),ZD/8,(XD-(XD/5))-(XD/54),YD/3+(YD/30),ZD/2)
SelectObjectContext("FcuPosteOuverture2")
SelectObject("FcuPosteOuverture2")
Name("FCUTourMA")
#Nouvel fonction
FcuArriereMB(1)
PortearriereMB(1)
EscalierTubeMB(1)
FcuOrc(1)"""
###########################VARIANTEPOSITIONPOSTEDOBSERVATIONMX#######################################################


def FcuWilly(kf):
    # NegatifWilly
    Boite(pos_X, pos_Y + YD + 0.5, pos_Z + (ZD), XD * 2, YD / 2, ZD * 2)
    Name("FCUWillyN")
    Soustraction("FCUTourMB", "FCUWillyN")
    # Willy
    Boite(pos_X, pos_Y + YD + 0.5, pos_Z + (ZD), XD, YD / 2, ZD)
    Name("FCUWillyN")
    # WillyAngles
    Boite(pos_X + XD * 2, pos_Y + YD / 2 + 0.5, pos_Z + (ZD), XD, YD * 2, ZD * 2)
    Name("FCUWillyN1")
    Rotation(2, 0.2618)
    Soustraction("FCUWillyN", "FCUWillyN1")
    # WillyAngles2
    Boite(pos_X - XD * 2, pos_Y + YD / 2 + 0.5, pos_Z + (ZD), XD, YD * 2, ZD * 2)
    Name("FCUWillyN2")
    Rotation(2, -0.2618)
    Soustraction("FCUWillyN", "FCUWillyN2")
    # WillyChapeau
    SelectObjectContext("FCUWillyN")
    SelectObject("FCUWillyN")
    Dupliquer(0, 0, ZD * 2 - ((ZD * 2)) / 2)
    Name("FCUWillyN3Haut")
    Resize(2, 2, 1)
    Soustraction("FCUWillyN", "FCUWillyN3Haut")
    # WillychapeauHaut
    Boite(pos_X, pos_Y + YD + 0.5, pos_Z + (ZD) + (ZD / 2), XD, YD / 2, ZD - (ZD / 2))
    Name("FCUWillyTete")
    Boite(pos_X + XD * 2, pos_Y + YD / 2 + 0.5, pos_Z + (ZD), XD, YD * 2, ZD * 2)
    Name("FCUWillyNH1")
    Rotation(2, 0.401426)
    Soustraction("FCUWillyTete", "FCUWillyNH1")
    # Anglegauche
    Boite(pos_X - XD * 2, pos_Y + YD / 2 + 0.5, pos_Z + (ZD), XD, YD * 2, ZD * 2)
    Name("FCUWillyNH2")
    Rotation(2, -0.401426)
    Soustraction("FCUWillyTete", "FCUWillyNH2")
    # adoucir
    SelectObjectContext("FCUWillyTete")
    SelectObject("FCUWillyTete")
    bpy.ops.object.modifier_add(type="BEVEL")
    bpy.context.object.modifiers["Bevel"].width = 0.26
    bpy.context.object.modifiers["Bevel"].segments = 6
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")
    Deplacer(0, -1, 0)
    # WillyOuvertures
    Boite(pos_X, pos_Y + YD + 0.5, pos_Z + (ZD), XD, YD, 2)
    Name("FCUWillyOuvertureN")
    Soustraction("FCUWillyTete", "FCUWillyOuvertureN")
    SelectObject("FCUWillyTete")
    Clean(1)
    Union("FCUWillyN")
    Clean(1)
    SelectObject("FCUWillyN")
    SelectObjectContext("FCUWillyN")
    Resize(0.7, 1, 1)
    Deplacer(0, -((YD / 2) / 1.5), 3)
    Deplacer(0, 0, -2.7)
    bpy.context.object.dimensions[2] = 1
    Dupliquer(0, 0, -0.2)
    Name("FCUWillyVide3N")
    Dupliquer(0, 0, -0.2)
    Name("FCUWillyOuvertureDent3N")
    Resize(1.1, 1.2, 1)
    Union("FCUWillyN")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("FCUWillyN")
    SelectObjectContext("FCUWillyN")
    Supprimer(1)
    SelectObject("FCUWillyTete")
    Union("FCUWillyOuvertureDent3N")
    Clean(1)
    SelectObject("FCUWillyOuvertureDent3N")
    SelectObjectContext("FCUWillyOuvertureDent3N")
    Supprimer(1)
    if XD > 12:
        SelectObject("FCUWillyTete")
        SelectObjectContext("FCUWillyTete")
        Resize(0.5, 1, 1)
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObject("FCUTourMB")
        SelectObjectContext("FCUTourMB")
        Union("FCUWillyTete")
        Clean(1)
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObject("FCUWillyTete")
        SelectObjectContext("FCUWillyTete")
        Supprimer(1)
        SelectObject("FCUWillyVide3N")
        SelectObjectContext("FCUWillyVide3N")
        Resize(0.5, 1, 1)
        Resize(0.8, 1, (ZD))
        Deplacer(0, -1, (ZD / 2))
        Soustraction("FCUTourMB", "FCUWillyVide3N")
        SelectObject("FCUTourMB")
        Clean(1)
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.subdivide(
            number_cuts=5,
            smoothness=0,
            quadtri=False,
            quadcorner="STRAIGHT_CUT",
            fractal=0,
            fractal_along_normal=0,
            seed=0,
        )
        bpy.ops.mesh.vertices_smooth(repeat=1, xaxis=True, yaxis=True, zaxis=True)
        bpy.ops.object.editmode_toggle()
        Name("RegelbauM362")

    if XD <= 12:
        SelectObject("FCUWillyTete")
        SelectObjectContext("FCUWillyTete")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObject("FCUTourMB")
        SelectObjectContext("FCUTourMB")
        Union("FCUWillyTete")
        Clean(1)
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObject("FCUWillyTete")
        SelectObjectContext("FCUWillyTete")
        Supprimer(1)
        SelectObject("FCUWillyVide3N")
        SelectObjectContext("FCUWillyVide3N")
        Resize(0.8, 1, (ZD / 3))
        Deplacer(0, -1, (ZD / 4))
        Soustraction("FCUTourMB", "FCUWillyVide3N")
        SelectObject("FCUTourMB")
        Name("RegelbauM262")

    # Negatif
    # Deplacer(0 ,0, -2.7)
    # Name("FCUWillyOuvertureDent1N")
    # bpy.context.object.dimensions[2] = 1

    # Dupliquer(0 ,0, 0)


###########################PreDEFINITION FONCTION#######################################################


def RegelbauS414(KZ):
    FcuObliqueM(1)
    FcuExtensionBas(1)


def RegelbauM157(kl):
    FcuDroitM(1)
    FcuExtensionBasB(1)


def RegelbauM162(ki):
    FcuMPoste(
        pos_X,
        pos_Y,
        pos_Z + (ZD * 2),
        +((ZD / 2.125) * 2),
        +(ZD / 8.5) + (ZD / 77),
        XD,
        YD / 2,
        ZD / 2.125,
        XD * 2,
        YD,
        ZD / 2.125,
        ZD / 8.5,
        XD - (XD / 5),
        YD / 3,
        (ZD / 4) + (ZD / 10),
        "FCUBASEM",
        +(ZD / 8.5) + (ZD / 77),
        (XD - (XD / 5)) + (XD / 54),
        YD / 3 + (YD / 54),
        ZD / 8,
        (XD - (XD / 5)) - (XD / 54),
        YD / 3 + (YD / 30),
        ZD / 2,
    )
    SelectObjectContext("FcuPosteOuverture2")
    SelectObject("FcuPosteOuverture2")
    Name("FCUTourMA")
    # Nouvel fonction
    FcuArriereMB(1)
    PortearriereMB(1)
    EscalierTubeMB(1)
    FcuOrc(1)


def RegelbauM362262(hj):
    FcuPosteMB(1)
    FcuWilly(1)


# FcuBaseM(1)
# RegelbauM362262(1)


# FcuBaseM(1)
# RegelbauM162(1)


###########################DEFINITION FONCTION#######################################################


def FcuTour(p9):
    if ZD <= 20:
        if XD < 10:
            FcuPosteMC(1)
            FcuExtensionM(1)
            FcuExtensionDentM(1)
            FcuExtensionDentM2(1)
            if Doll2 <= 5:
                RegelbauS414(1)

            if Doll2 > 5:
                RegelbauM157(1)

        if XD > 10:
            RegelbauM362262(1)

        if XD == 10:
            RegelbauM162(1)

    if ZD > 20:
        FcuPosteMD(1)


# Appel fonction final
# FcuBaseM(1)
# FcuTour(1)

print(
    "-----------------------------------------------DEFINITION OBSERVATOIRE MULTIPLE----------------------------------------------"
)


def AObservationsMultiples(hj):
    FcuBaseM(1)
    FcuTour(1)


#############################################################################################


print(
    "-----------------------------------------------Construcution Volumetrique TOUR  CIRCULAIRES----------------------------------------------"
)


def TourCirculaire(a):
    Cylindre(d, XE, XE, ZE, pos_X, pos_Y, pos_Z + (ZE))
    Chanfrein(1)
    Name("Tour circulaire")


def FireControl(b):
    Boite(1, XE, (ZE * 2) - (ZE / 2), XE, XE, 1)
    if ZE >= 24:
        Multiplication(ZE / 9, -ZE / 3)
    Name("NegatifOuverture")
    Soustraction("Tour circulaire", "NegatifOuverture")


def TourCirculaireVide(a):
    Cylindre(
        d,
        XE / 2 + (XE / 4),
        XE / 2 + (XE / 4),
        ZE - (ZE / 11),
        pos_X,
        pos_Y,
        pos_Z + (ZE),
    )
    Chanfrein(1)
    Name("Vide")
    Soustraction("Tour circulaire", "Vide")


######################################################################################


def TourCirculaireB(a):
    Cylindre(d, XE, XE, ZE, pos_X, pos_Y, pos_Z + (ZE))
    Chanfrein(1)
    Name("Tour circulaireB")
    Boite(1, pos_Y, pos_Z, 2 * XE, XE * 2, ZE / 2 + (ZE / 4))
    Name("NegatifOuvertureB")
    Soustraction("Tour circulaireB", "NegatifOuvertureB")


def FireControlB(b):
    Boite(1, XE, (ZE * 2) - (ZE / 2), XE, XE, 1)
    if ZE >= 24:
        Multiplication((ZE / 9) / 2, -ZE / 3)
    Name("NegatifOuvertureB")
    Soustraction("Tour circulaireB", "NegatifOuvertureB")


def TourCirculaireVideB(a):
    Cylindre(
        d, XE / 2 + (XE / 4), XE / 2 + (XE / 4), ZE / 4, pos_X, pos_Y, pos_Z + (ZE)
    )
    Chanfrein(1)
    Name("VideB")
    Soustraction("Tour circulaireB", "VideB")


########################################################################################
def TourExtension(u):
    Boite(pos_X, pos_Y, ZE - (ZE / 2), XE - 1, XE, (ZE / 9) + 5)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Name("TourExtension")


def ExtensionDetail(z):
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(XE + (XE / 4), -XE - (XE / 3), 0)
    Name("ExtensionNegatif1")
    Soustraction("TourExtension", "ExtensionNegatif1")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE - (XE / 2.5), (ZE / 4) - 2)
    Deplacer(2 + (XE / 3), -XE - (XE / 3), 0)
    Rotation(2, -0.680679)
    Name("ExtensionNegatif2")
    Soustraction("TourExtension", "ExtensionNegatif2")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(-XE - (XE / 2), -XE - (XE / 3), 0)
    Rotation(2, 0.715585)
    Name("ExtensionNegatif3")
    Soustraction("TourExtension", "ExtensionNegatif3")
    # SelectObject("TourExtension")
    # Chanfrein(1)
    # Test detail coupure
    Boite(pos_X, -pos_Y, ZE + 1, XE - 4, XE - 1, (ZE / 9) + 8)
    Deplacer(0, 0, (ZE / 4) - 1)
    Deplacer(0, -(XE / 4), 0)
    Name("Rebord")
    Soustraction("TourExtension", "Rebord")
    SelectObject("TourExtension")
    Chanfrein(1)


def Porte(aa):
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), 1, 6, 2)
    Deplacer((XE / 2) + 1, -XE - (XE / 3), (ZE / 4) - (ZE / 6))
    Name("Porte")
    Soustraction("TourExtension", "Porte")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), 1, 0.1, 0.3)
    Deplacer((XE / 2) + 1, -XE - (XE / 3), (ZE / 4) - (ZE / 6) + 2)
    Name("Linteau")
    Union("TourExtension")
    Name("TourExtensionComplet")


##############################################################################"
def TourExtensionB(ra):
    Boite(pos_X, pos_Y, pos_Z, XE - 1, XE, (ZE / 9) + 5)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Name("TourExtensionB")


def ExtensionDetailB(z):
    Boite(pos_X, -pos_Y, (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(XE + (XE / 4), -XE - (XE / 3), 0)
    Name("ExtensionNegatif1B")
    Soustraction("TourExtensionB", "ExtensionNegatif1B")
    Boite(pos_X, -pos_Y, (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE - (XE / 2.5), (ZE / 4) - 2)
    Deplacer(2 + (XE / 3), -XE - (XE / 3), 0)
    Rotation(2, -0.680679)
    Name("ExtensionNegatif2B")
    Soustraction("TourExtensionB", "ExtensionNegatif2B")
    Boite(pos_X, -pos_Y, (ZE / 2), XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(-XE - (XE / 2), -XE - (XE / 3), 0)
    Rotation(2, 0.715585)
    Name("ExtensionNegatif3B")
    Soustraction("TourExtensionB", "ExtensionNegatif3B")
    Boite(pos_X, -pos_Y, ZE + 1, XE - 4, XE - 1, (ZE / 9) + 8)
    Deplacer(0, 0, (ZE / 4) - 1)
    Deplacer(0, -(XE / 4), 0)
    Name("RebordB")
    Soustraction("TourExtensionB", "RebordB")
    SelectObject("TourExtensionB")
    Chanfrein(1)


###########################################################################################""


def TourExtensionC(ra):
    Boite(pos_X, pos_Y, pos_Z, XE - 1, XE, (ZE / 9) + 5)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Name("TourExtensionC")


def ExtensionDetailC(z):
    Boite(pos_X, -pos_Y, pos_Z, XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(XE + (XE / 4), -XE - (XE / 3), 0)
    Name("ExtensionNegatif1C")
    Soustraction("TourExtensionC", "ExtensionNegatif1C")
    Boite(pos_X, -pos_Y, pos_Z, XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE - (XE / 2.5), (ZE / 4) - 2)
    Deplacer(2 + (XE / 3), -XE - (XE / 3), 0)
    Rotation(2, -0.680679)
    Name("ExtensionNegatif2C")
    Soustraction("TourExtensionC", "ExtensionNegatif2C")
    Boite(pos_X, -pos_Y, pos_Z, XE - 1, XE, (ZE / 9) + 8)
    Deplacer(0, -XE, (ZE / 4) - 2)
    Deplacer(-XE - (XE / 2), -XE - (XE / 3), 0)
    Rotation(2, 0.715585)
    Name("ExtensionNegatif3C")
    Soustraction("TourExtensionC", "ExtensionNegatif3C")
    Boite(pos_X, -pos_Y, (ZE / 2) + 1, XE - 4, XE - 1, (ZE / 9) + 8)
    Deplacer(0, 0, (ZE / 4) - 1)
    Deplacer(0, -(XE / 4), 0)
    Name("RebordC")
    Soustraction("TourExtensionC", "RebordC")
    SelectObject("TourExtensionC")
    Chanfrein(1)


def PorteC(aa):
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), 1, 6, 2)
    Deplacer((XE / 2) + 1, -XE - (XE / 3), (ZE / 4) - (ZE / 6))
    Name("PorteC")
    Soustraction("TourExtensionC", "PorteC")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), 1, 0.1, 0.3)
    Deplacer((XE / 2) + 1, -XE - (XE / 3), (ZE / 4) - (ZE / 6) + 2)
    Name("LinteauC")
    Union("TourExtensionC")


###########################################################################################
# ECHEC
def TrouVue(aaa):
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE / 8, 0.1, 0.4)
    Deplacer(XE / 6, -XE - (XE / 6), (ZE / 4) - (ZE / 6) + 0.5)
    Deplacer(0, -(XE / 3), 0)
    Name("Meurtriere1")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE / 8, 0.7, 0.8)
    Deplacer((XE / 6) + 1, -XE - (XE / 6), (ZE / 4) - (ZE / 6) + 0.5)
    Deplacer(0, -(XE / 3), 0)
    Name("Meurtriere2")
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Meurtriere1"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    Name("Meurtriere2")
    Boite(pos_X, -pos_Y, ZE - (ZE / 2), XE / 8, 1, 0.9)
    Deplacer((XE / 6) + 1.5, -XE - (XE / 6), (ZE / 4) - (ZE / 6) + 0.5)
    Deplacer(0, -(XE / 3), 0)
    Name("Meurtriere3")
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Meurtriere2"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    Name("Meurtriere")
    Rotation(2, -0.680679)
    Soustraction("TourExtension", "Meurtriere")
    SelectObjectContext("Meurtriere2")
    SelectObject("Meurtriere2")
    Supprimer(1)
    SelectObjectContext("Meurtriere1")
    SelectObject("Meurtriere1")
    Supprimer(1)


# TrouVue(1)


########################################################################################################
def GardeTour(ooo):
    Cylindre(d, XE - 3, XE - 3, ZE / 19, pos_X, pos_Y, pos_Z + (ZE * 2) - 0.3)
    Name("GardeTour")
    Cylindre(d, XE - 4, XE - 4, 3, pos_X, pos_Y, pos_Z + (ZE * 2) - 0.3)
    Name("NegatifGardeTour")
    Soustraction("GardeTour", "NegatifGardeTour")
    Boite(
        pos_X,
        pos_Y - (XE / 2) + 2,
        pos_Z + (ZE) + 5,
        (XE / 3) - 1,
        (XE / 2),
        ZE + (ZE / 38) - 2.7,
    )
    Name("NegatifTubeTour")
    Soustraction("GardeTour", "NegatifTubeTour")


def TubeTour(lll):
    Boite(
        pos_X, pos_Y - (XE / 2), pos_Z + (ZE) + 3, XE / 3, XE / 2, ZE + (ZE / 38) - 2.7
    )
    Name("TubeTour")
    Boite(
        pos_X,
        pos_Y - (XE / 2) + 2,
        pos_Z + (ZE) + 5,
        (XE / 3) - 1,
        (XE / 2),
        ZE + (ZE / 38) - 2.7,
    )
    Name("NegatifTubeTour")
    Soustraction("TubeTour", "NegatifTubeTour")
    Cylindre(d, XE - 4, XE - 4, 3, pos_X, pos_Y, pos_Z + (ZE * 2) - 0.3)
    Name("NegatifGardeTour")
    Soustraction("TubeTour", "NegatifGardeTour")
    Boite(
        pos_X,
        pos_Y - (XE / 2) - (XE / 4) + 1,
        pos_Z + (ZE) + 5,
        (XE / 3) - 1 - (XE / 6),
        (XE / 7),
        ZE + (ZE / 38) - 2.7,
    )
    Name("TrouHautTour")
    Soustraction("Tour circulaire", "TrouHautTour")


def GardeTourB(ooo):
    Cylindre(d, XE - 3, XE - 3, ZE / 19, pos_X, pos_Y, pos_Z + (ZE * 2) - 1)
    Name("GardeTourB")
    Soustraction("Tour circulaire", "GardeTourB")


def ExtensionLong(ger):
    Boite(pos_X, -pos_Y, pos_Z, XE + 3, XE / 3, (ZE / 5))
    Deplacer(0, -XE, (ZE / 2) + (ZE / 6))
    Deplacer(-XE - (XE / 2), -XE - (XE / 3), 0)
    Rotation(2, 0.715585)
    Name("ExtensionHautB")
    Boite(pos_X, -pos_Y, pos_Z, XE + 9, (XE / 3) + (XE / 2), (ZE / 5))
    Deplacer(0, -XE + (XE / 6), (ZE / 2) + (ZE / 6))
    Deplacer(-XE - XE, -XE - (XE / 3), -(ZE / 5))
    Rotation(2, 0.715585)
    Rotation(1, 0.279253)
    Name("NegatifExtensionHautB")
    Soustraction("ExtensionHautB", "NegatifExtensionHautB")
    SelectObject("ExtensionHautB")
    Chanfrein(1)


############################################################################ENSEMBLES#############################################################
def ExtensionBasse(zz):
    TourExtension(1)
    ExtensionDetail(1)
    TrouVue(1)
    Porte(1)


def ExtensionBasseB(zz):
    TourExtension(1)
    ExtensionDetail(1)
    TrouVue(1)
    ExtensionLong(1)
    Porte(1)


# ExtensionBasseB(1)

# ExtensionBasse(1)


def GardeCorpsHaut(uuu):
    GardeTour(1)
    TubeTour(1)


def ExtensionSol(sze):
    TourExtensionB(1)
    ExtensionDetailB(1)


def ExtensionRaz(zse):
    TourExtensionC(1)
    ExtensionDetailC(1)
    PorteC(1)


def GardeCorpsBas(mpo):
    GardeTourB(1)


####################################################################Appeller une tour##############
def PosteObservationAlpha(ert):
    TourCirculaire(1)
    FireControl(1)
    TourCirculaireVide(1)


def PosteObservationBeta(hyj):
    TourCirculaireB(1)
    FireControlB(1)
    TourCirculaireVideB(1)


#####################################################################################


def ExtensionAvant(zio):
    Boite(pos_X, pos_Y + XE, pos_Z, XE, 2 * XE + za, (ZE / 9) + 5)
    # Chanfrein(1)
    Name("ExtensionAvant")
    Boite(
        pos_X + XE,
        pos_Y + XE + (2 * XE + za) - (XE / 2),
        pos_Z,
        (XE / 2) - 1,
        ((2 * XE + za) / 2),
        (ZE / 9) + 15,
    )
    Name("NegatifExtensionAvant")
    Soustraction("ExtensionAvant", "NegatifExtensionAvant")
    SelectObject("ExtensionAvant")
    bpy.context.scene.objects["ExtensionAvant"].select = True
    Chanfrein(1)


def PosteObservationBas(a):
    ExtensionAvant(1)
    PosteObservationBeta(1)
    SelectObject("Tour circulaireB")
    Name("PosteObservationBas")
    SelectObject("PosteObservationBas")
    bpy.context.scene.objects["PosteObservationBas"].select = True
    Deplacer(
        0, pos_Y + XE + (2 * XE + za), -ZE - pos_Z - (ZE / 2) - (((ZE / 9) + 5) / 2)
    )


########################################################################################


def PosteObservationHautEtBas(bez):
    # PosteObservationHaut(1)
    PosteObservationBas(1)
    Joindre(1)
    Name("PosteObservationHautEtBAS")


# PosteObservationHautEtBas(1)
##########################################################################"

print(
    "-----------------------------------------------Bunker Type Poste d observation-----------------------------------------------"
)


########################################Formule de fin Tour ciruclaire #######################################
def PosteObservationHaut(a):
    PosteObservationAlpha(1)
    print("ALors alors ZE vaut")
    print(ZE)
    if ZE <= 35:
        GardeCorpsBas(1)
    else:
        GardeCorpsHaut(1)
    print("ALors alors ZE/9 vaut")
    print(ZE / 9)
    print("Les dés nous donnent pour les extensions")
    print(Haz)
    if ZE / 9 >= 3:
        if Haz > 3:
            ExtensionBasse(1)
            Joindre(1)
        if Haz < 3:
            ExtensionRaz(1)
            Joindre(1)
        if Haz == 3:
            ExtensionBasse(1)
            ExtensionSol(1)
            Joindre(1)
        if Doll >= 4:
            ExtensionBasseB(1)
            Joindre(1)

    if Terra >= 8:
        PosteObservationHautEtBas(1)
        Joindre(1)
        if WallWest >= 10:
            bpy.ops.object.modifier_add(type="ARRAY")
            bpy.context.object.modifiers["Array"].count = DDay
            bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 0
            bpy.context.object.modifiers["Array"].relative_offset_displace[1] = 0.6
            bpy.context.object.modifiers["Array"].relative_offset_displace[2] = -0.9
            bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    Name("PosteObservationHaut")


# PosteObservationHaut(1)
# print(ZE,ZE/9,Haz,Doll,Terra,WallWest)
#############################################################################################


print(
    "-----------------------------------------------Construcution Volumetrique Tour S 448----------------------------------------------"
)


def TourPalier(a):
    Boite(pos_X, pos_Y, pos_Z + (Z), X, Y, Z)
    ChanfreinVariante(0.1)
    Name("Tour Palier")
    print(X, Y, Z)
    Boite(pos_X, pos_Y, (pos_Z + (Z)) - (Z / 3), X + 4, Y + 4, Z - (Z / 3))
    ChanfreinVariante(0.3)
    Name("Tour PalierBas")
    Union("Tour Palier")
    Name("Tour PalierBas")
    # SelectObject("Tour Palier")
    # SelectObjectContext("Tour Palier")
    # Supprimer(1)
    Boite(pos_X, pos_Y, pos_Z, X + 10, Y + 10, Z / 4)
    ChanfreinVariante(0.5)
    Name("Tour PalierSocle")
    Union("Tour PalierBas")
    Name("Tour PalierOk")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObjectContext("Tour PalierBas")
    SelectObject("Tour PalierBas")
    Supprimer(1)
    SelectObjectContext("Tour Palier")
    SelectObject("Tour Palier")
    Supprimer(1)


def DetailToit1(a1):
    Boite(pos_X, pos_Y, (Z * 2) + 1, X * 2, Y * 2, 4)
    Name("Detail ToitNegatif")
    Soustraction("Tour PalierOk", "Detail ToitNegatif")
    if X >= Y:
        Boite(pos_X - (X - (X / 2)), pos_Y, (Z * 2) - 9, X, Y - 1, 4)
        ChanfreinVariante(0.5)
        Name("Detail ToitNegatif3")
        Union("Tour PalierOk")
        Name("Tour PalierOkOk")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour PalierOk")
        SelectObject("Tour PalierOk")
        Supprimer(1)
        Boite(pos_X - (X + X / 2), pos_Y, (Z * 2) - 2, X, Y * 2, 4)
        Name("Detail ToitNegatif2")
    else:
        Boite(pos_X, pos_Y - (X - (X / 2)), (Z * 2) - 9, X - 1, Y, 4)
        ChanfreinVariante(0.5)
        Name("Detail ToitNegatif3")
        Union("Tour PalierOk")
        Name("Tour PalierOkOk")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour PalierOk")
        SelectObject("Tour PalierOk")
        Supprimer(1)
        Boite(pos_X, pos_Y - (Y + Y / 2), (Z * 2) - 2, X * 2, Y, 4)
        Name("Detail ToitNegatif2")
    Soustraction("Tour PalierOkOk", "Detail ToitNegatif2")


def DetailTrouToit(a2):
    if X >= Y:
        Cylindre(32, X / 2, X / 2, 0.5, pos_X + 1, pos_Y, (Z * 2) - 3)
        # Boite(pos_X, pos_Y, (Z*2)-3,X*2,Y*2, 0.3)
        Name("Detail TrouToitNegatif")
        Union("Tour PalierOkOk")
        Name("Tour PalierOkOkOk")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour PalierOkOk")
        SelectObject("Tour PalierOkOk")
        Supprimer(1)
        Cylindre(32, 1, 1, Z / 2, pos_X + 1, pos_Y, (Z * 2) - 3)
        Name("TrouToitNegatif")
    else:
        Cylindre(32, Y / 2, Y / 2, 0.5, pos_X, pos_Y + 1, (Z * 2) - 3)
        # Boite(pos_X, pos_Y, (Z*2)-3,X*2,Y*2, 0.3)
        Name("Detail TrouToitNegatif")
        Union("Tour PalierOkOk")
        Name("Tour PalierOkOkOk")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour PalierOkOk")
        SelectObject("Tour PalierOkOk")
        Supprimer(1)
        Cylindre(32, 1, 1, Z / 2, pos_X, pos_Y + 1, (Z * 2) - 3)
        Name("TrouToitNegatif")
    Soustraction("Tour PalierOkOkOk", "TrouToitNegatif")


def DetailTrouToit2(a3):
    if X >= Y:
        Cylindre(32, (X / 3) + 1, X / 3, 2, pos_X - (X / 2), pos_Y, (Z * 2) - 6)
        Name("Trou")
        # Boite(pos_X-(X+X/2), pos_Y, (Z*2)-2,X,Y, 4)
    else:
        Cylindre(32, Y / 3, (Y / 3) + 1, 2, pos_X, pos_Y - (Y / 2), (Z * 2) - 6)
        Name("Trou")
    Soustraction("Tour PalierOkOkOk", "Trou")


def TourS448(n1):
    TourPalier(1)
    DetailToit1(1)
    DetailTrouToit(1)
    DetailTrouToit2(1)
    SelectObject("Tour PalierOkOkOk")
    Name("Tour S448")


# TourS448(1)

print(
    "-----------------------------------------------Construcution Volumetrique Tour FL354----------------------------------------------"
)


def TourRectangle(m1):
    Boite(pos_X, pos_Y, pos_Z + (Z), X, Y, Z)
    ChanfreinVariante(0.1)
    Name("Tour Rectangle")


def TourRectangleIndus(m2):
    # Y/2
    Boite(pos_X, pos_Y + (Y * 2) - 1, pos_Z + (Z) - (Z / 3), X - 2, Y, Z - (Z / 3))
    Name("EvidementY/2")
    Boite(pos_X, pos_Y - 0.2, pos_Z + (Z) - (Z / 3), X + 2, Y, 1)
    Name("LinteauY/2")
    Soustraction("EvidementY/2", "LinteauY/2")
    Soustraction("Tour Rectangle", "EvidementY/2")
    SelectObjectContext("EvidementY/2")
    Supprimer(1)
    # -Y/2
    Boite(pos_X, pos_Y - (Y * 2) + 1, pos_Z + (Z) - (Z / 3), X - 2, Y, Z - (Z / 3))
    Name("Evidement-Y/2")
    Boite(pos_X, pos_Y + 0.2, pos_Z + (Z) - (Z / 3), X + 2, Y, 1)
    Name("Linteau-Y/2")
    Soustraction("Evidement-Y/2", "Linteau-Y/2")
    Soustraction("Tour Rectangle", "Evidement-Y/2")
    SelectObjectContext("Evidement-Y/2")
    Supprimer(1)
    # -X/2
    Boite(pos_X + (X * 2) - 1, pos_Y, pos_Z + (Z) - (Z / 3), X, Y - 2, Z - (Z / 3))
    Name("Evidement-X/2")
    Boite(pos_X - 0.2, pos_Y, pos_Z + (Z) - (Z / 3), X, Y + 2, 1)
    Name("Linteau-X/2")
    Soustraction("Evidement-X/2", "Linteau-X/2")
    Soustraction("Tour Rectangle", "Evidement-X/2")
    SelectObjectContext("Evidement-X/2")
    Supprimer(1)
    # X/2
    Boite(pos_X - (X * 2) + 1, pos_Y, pos_Z + (Z) - (Z / 3), X, Y - 2, Z - (Z / 3))
    Name("EvidementX/2")
    Boite(pos_X + 0.2, pos_Y, pos_Z + (Z) - (Z / 3), X, Y + 2, 1)
    Name("LinteauX/2")
    Soustraction("EvidementX/2", "LinteauX/2")
    Soustraction("Tour Rectangle", "EvidementX/2")
    SelectObjectContext("EvidementX/2")
    Supprimer(1)


def Escaliers(loc5, loc6, loc7, cm):
    Boite(loc5, loc6, loc7, 3, 1, 0.2)
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].use_relative_offset = False
    bpy.context.object.modifiers["Array"].constant_offset_displace[0] = 0.2
    bpy.context.object.modifiers["Array"].constant_offset_displace[2] = 1.5
    bpy.context.object.modifiers["Array"].count = cm
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    Name("Escaliers")


def DetailPorteTour(m4):
    if Y >= X:
        Boite(pos_X - X + 0.3, pos_Y - (Y / 3), pos_Z + 3, 3, 1, 2)
        Name("PorteTourY")
        Soustraction("Tour Rectangle", "PorteTourY")
        Escaliers(pos_X - X + 2, pos_Y - (Y / 3), pos_Z + 0.2, 4)
        Union("Tour Rectangle")
        Name("PorteTourY")
        Name("TourFL354")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour Rectangle")
        Supprimer(1)
        Boite(pos_X + X - 0.3, pos_Y - (Y / 3), pos_Z + (Z) - (Z / 3) - 2, 3, 0.6, 0.6)
        Name("Ouverture1")
        Soustraction("TourFL354", "Ouverture1")
        Boite(pos_X + X - 0.3, pos_Y - (Y / 3), pos_Z + (Z) + (Z / 3) - 2, 3, 0.6, 0.6)
        Name("Ouverture2")
        Soustraction("TourFL354", "Ouverture2")

    else:
        Boite(pos_X - (X / 3), pos_Y - Y + 0.3, pos_Z + 3, 1, 3, 2)
        Name("PorteTourX")
        Soustraction("Tour Rectangle", "PorteTourX")
        Escaliers(pos_X - (X / 3), pos_Y - Y + 2, pos_Z + 0.2, 4)
        bpy.context.object.rotation_euler[2] = 1.5708
        Union("Tour Rectangle")
        Name("PorteTourX")
        Name("TourFL354")
        bpy.ops.object.select_all(action="TOGGLE")
        SelectObjectContext("Tour Rectangle")
        Supprimer(1)
        Boite(pos_X - (X / 3), pos_Y + (Y / 3), pos_Z + (Z) - (Z / 3) - 2, 0.6, 3, 0.6)
        Name("Ouverture1")
        Boite(pos_X - (X / 3), pos_Y - Y + 0.3, pos_Z + 3, 1, 3, 2)
        Soustraction("TourFL354", "Ouverture1")
        Boite(pos_X - (X / 3), pos_Y + (Y / 3), pos_Z + (Z) + (Z / 3) - 2, 0.6, 3, 0.6)
        Name("Ouverture2")
        Soustraction("TourFL354", "Ouverture2")
        SelectObjectContext("Ouverture1")
        Supprimer(1)


def CreuxTour(m7):
    Boite(pos_X, pos_Y, pos_Z + (Z) - (Z / 2), X - 3, Y - 3, Z - (Z / 3))
    Name("Tour Rectangle Vide")
    Soustraction("TourFL354", "Tour Rectangle Vide")


# TourRectangle(1)
# TourRectangleIndus(1)
# DetailPorteTour(1)
# CreuxTour(1)


def TourFL354(m3):
    TourRectangle(1)
    TourRectangleIndus(1)
    DetailPorteTour(1)
    CreuxTour(1)


# TourFL354(1)


print(
    "-----------------------------------------------Construcution Volumetrique Tour FL354----------------------------------------------"
)


def TourDogon(m8):
    Boite(pos_X, pos_Y, pos_Z + (Z), X, Y, Z)
    # ChanfreinVariante(0.1)
    Name("Tour Dogon")
    Boite(pos_X + X + 1, pos_Y, (Z * 2) + 1, X, Y * 2, Z / 3)
    Name("Detail ToitNegatif42")
    Soustraction("Tour Dogon", "Detail ToitNegatif42")
    Boite(pos_X, pos_Y, (Z * 2) - 1, X * 2, Y * 2, 0.6)
    Name("Detail ToitNegatif423")
    Soustraction("Tour Dogon", "Detail ToitNegatif423")
    SelectObject("Tour Dogon")
    ChanfreinVariante(0.2)
    Boite(pos_X - (X / 2) + 0.5, pos_Y, pos_Z + (Z), X / 3, Y - 1, Z)
    Name("Appuis")
    Boite(pos_X - (X / 2) + 0.5, pos_Y, pos_Z + (Z), X / 3 - 0.3, Y - 1.3, Z * 2)
    Name("AppuisVide")
    Soustraction("Appuis", "AppuisVide")
    Boite(pos_X - (X / 2) + 0.5, pos_Y, pos_Z + (Z), X / 3 - 0.3, Y * 2, Z * 2)
    Name("AppuisVideColonne")
    Soustraction("Appuis", "AppuisVideColonne")
    Boite(pos_X - (X / 2) + 0.5, pos_Y, pos_Z + (Z), X, Y - 1.3, Z * 2)
    Name("AppuisVideColonne2")
    Soustraction("Appuis", "AppuisVideColonne2")
    # Vide
    Boite(pos_X - (X / 2) + 0.5, pos_Y, pos_Z + (Z) - 2, X / 3 - 1, Y - 2, Z)
    Name("Vide")
    Soustraction("Tour Dogon", "Vide")


def OuvertureDents(m21, m22, m23, m24):
    # Boite(pos_X+X-1+0.41, pos_Y, Z*2-(Z/3)-3,0.82,3.5, 0.68)
    Boite(pos_X + X - 1 + m21, pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 3.5, m24)
    Name("Detail ouverture1")
    # Boite(pos_X+X-1-0.41, pos_Y, Z*2-(Z/3)-3,0.82,3.4, 0.6)
    Boite(pos_X + X - 1 + m22, pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 3.4, m24 - 0.1)
    Name("Detail ouverture2")
    Union("Detail ouverture1")
    Name("Detail ouverture3")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Detail ouverture1")
    SelectObjectContext("Detail ouverture1")
    Supprimer(1)
    # Dernier module
    # Boite(pos_X+X-1-1.23, pos_Y, Z*2-(Z/3)-3,0.82,3.3, 0.5)
    Boite(pos_X + X - 1 + m23, pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 3.3, m24 - 0.2)
    Name("Detail ouverture4")
    Union("Detail ouverture3")
    Name("Detail ouverture5")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Detail ouverture3")
    SelectObjectContext("Detail ouverture3")
    Supprimer(1)
    Soustraction("Tour Dogon", "Detail ouverture5")
    SelectObject("Detail ouverture5")
    SelectObjectContext("Detail ouverture5")
    Supprimer(1)


def OuvertureDents2(m27):
    Boite(-(pos_X + X - 1 + 0.41), pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 1, 0.68)
    # Boite(pos_X+X-1+m21, pos_Y, Z*2-(Z/3)-3,0.82,3.5, m24)
    Name("Detail ouverture1")
    Boite(-(pos_X + X - 1 - 0.41), pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 0.9, 0.6)
    # Boite(pos_X+X-1+m22, pos_Y, Z*2-(Z/3)-3,0.82,3.4, m24-0.1)
    Name("Detail ouverture2")
    Union("Detail ouverture1")
    Name("Detail ouverture3")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Detail ouverture1")
    SelectObjectContext("Detail ouverture1")
    Supprimer(1)
    # Dernier module
    Boite(-(pos_X + X - 1 - 1.23), pos_Y, Z * 2 - (Z / 3) - 3, 0.82, 0.8, 0.5)
    # Boite(pos_X+X-1+m23, pos_Y, Z*2-(Z/3)-3,0.82,3.3, m24-0.2)
    Name("Detail ouverture4")
    Union("Detail ouverture3")
    Name("Detail ouverture5")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Detail ouverture3")
    SelectObjectContext("Detail ouverture3")
    Supprimer(1)
    Soustraction("Tour Dogon", "Detail ouverture5")
    SelectObject("Detail ouverture5")
    SelectObjectContext("Detail ouverture5")
    Supprimer(1)


def Poutre(m9):
    Boite(pos_X, pos_Y, pos_Z + (Z / 2), 0.17, Y * 2, 0.22)
    Name("Poutre")
    Boite(pos_X + 0.19, pos_Y, pos_Z + (Z / 2), 0.17, Y * 2, 0.12)
    Name("Poutre-X")
    Soustraction("Poutre", "Poutre-X")
    Boite(pos_X - 0.19, pos_Y, pos_Z + (Z / 2), 0.17, Y * 2, 0.12)
    Name("PoutreX")
    Soustraction("Poutre", "PoutreX")
    SelectObject("Poutre")
    SelectObjectContext("Poutre")
    Dupliquer(-X + 1, 0, Z + (Z / 4))
    Name("Poutre12")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Poutre12")
    SelectObjectContext("Poutre12")
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].count = 3
    bpy.context.object.modifiers["Array"].use_relative_offset = False
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].constant_offset_displace[0] = X * 2
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Poutre")
    SelectObjectContext("Poutre")
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].count = 3
    bpy.context.object.modifiers["Array"].use_relative_offset = False
    bpy.context.object.modifiers["Array"].use_constant_offset = True
    bpy.context.object.modifiers["Array"].constant_offset_displace[2] = Z * 2
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    Dupliquer(X / 2 + (X / 4), 0, 0)
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Poutre")
    SelectObjectContext("Poutre")
    Dupliquer(-X / 2 - (X / 4), 0, 0)

    # Dupliquer(X/2+(X/5),0,0)
    # Name("Poutre1")
    # SelectObject("Poutre")
    # SelectObjectContext("Poutre")
    # Dupliquer(X/2+(X/5),0,0)
    # Name("Poutre2")


# TourDogon(1)
# OuvertureDents(0.41,-0.41,-1.23,0.68)
# OuvertureDents2(1)
# Poutre(1)


def TourS487(m41):
    TourDogon(1)
    OuvertureDents(0.41, -0.41, -1.23, 0.68)
    OuvertureDents2(1)
    Poutre(1)
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.join()
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.object.editmode_toggle()
    Name("TourS487")


# TourS487(1)

print(
    "-----------------------------------------------Construcution Volumetrique Tour FL241----------------------------------------------"
)


def TourBatman(m41):
    Boite(pos_X, pos_Y, pos_Z + (Z), X, Y, Z)
    # ChanfreinVariante(0.1)
    Name("Tour Batman")


def DentsY(m42):
    # dents Y/2
    Boite(
        pos_X + X + (X / 2) - 1,
        pos_Y - Y + (Y / 5) + (Y / 16),
        pos_Z + (Z * 2) - (Z / 10),
        X / 2,
        Y / 8,
        Z / 10 - (0.5),
    )
    Name("DentsBase")
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].fit_type = "FIT_LENGTH"
    bpy.context.object.modifiers["Array"].fit_length = Y * 2
    bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 0
    bpy.context.object.modifiers["Array"].relative_offset_displace[1] = 2
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    # Negatif
    Boite(
        pos_X + X + (X / 2) - 1,
        pos_Y - Y + (Y / 5) + (Y / 16),
        pos_Z + (Z * 2) + (Z / 10) + 1,
        X * 2,
        Y * 2,
        1 + Z / 10 - (0.5),
    )
    Name("DentsBaseNegatif")
    Rotation(1, 0.785398)
    Soustraction("DentsBase", "DentsBaseNegatif")
    SelectObject("DentsBase")
    SelectObjectContext("DentsBase")
    Dupliquer(-X * 2 - (X / 2) - 0.5, 0, 0)

    bpy.context.object.scale[0] = -X / 2
    Name("DentsBase-Y")
    Union("DentsBase")
    Name("DentsBaseY-Y")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("DentsBase")
    SelectObjectContext("DentsBase")
    Supprimer(1)

    # Rotation(1,0.785398)


def DentsX(m42):
    # dents Y/2
    Boite(
        pos_X - X + (X / 5) + (X / 16),
        pos_Y + Y + (Y / 2) - 1,
        pos_Z + (Z * 2) - (Z / 10),
        X / 8,
        Y / 2,
        Z / 10,
    )
    Name("DentsBaseX")
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].fit_type = "FIT_LENGTH"
    bpy.context.object.modifiers["Array"].fit_length = X * 2
    bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 2
    bpy.context.object.modifiers["Array"].relative_offset_displace[1] = 0
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    # Negatif
    Boite(
        0, pos_Y + Y + Y / 4, pos_Z + (Z * 2) + (Z / 10) + 1, X * 2, Y * 2, 1 + Z / 10
    )
    Name("DentsBaseNegatifX")
    Deplacer(0, 0, 0)
    Rotation(0, -0.785398)
    Soustraction("DentsBaseX", "DentsBaseNegatifX")
    SelectObject("DentsBaseX")
    SelectObjectContext("DentsBaseX")
    Dupliquer(0, -Y * 2 - (Y / 2) - 0.5, 0)
    bpy.context.object.scale[1] = -Y / 2
    Name("DentsBase-X")


def DentUnion(m80):
    Union("Tour Batman")
    Name("Tour BatmanOk")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Tour Batman")
    SelectObjectContext("Tour Batman")
    Supprimer(1)
    SelectObject("DentsBaseX")
    SelectObjectContext("DentsBaseX")
    Union("Tour BatmanOk")


def Finition1(m78):
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.join()
    Name("TourDent")


def Finition2(m79):
    Boite(pos_X, pos_Y, pos_Z + (Z * 2), X - 1, Y - 1, 3)
    Name("FinitionNegatifX")
    # Soustraction("TourDent","FinitionNegatifX")


def EscaliersBatman(m28):
    # aussi cheminée
    Boite(pos_X + (X / 2), pos_Y - Y, pos_Z + (Z * 2) - (Z / 3), 0.6, 0.6, Z / 3)
    Name("EscalierBatman")
    Boite(pos_X + (X / 2), pos_Y - Y, pos_Z + (Z * 2) - (Z / 3), 0.5, 0.5, Z)
    Name("EscalierBatmanVide")
    Soustraction("EscalierBatman", "EscalierBatmanVide")
    # cassure
    Boite(pos_X + (X / 2), pos_Y - Y, pos_Z + (Z * 2) - 0.5, 1, 1, 0.4)
    Name("EscalierBatmanNegatif")
    Multiplication(50, -2.4)
    Soustraction("EscalierBatman", "EscalierBatmanNegatif")


def UnionTotal(m57):
    SelectObject("TourDent")
    SelectObjectContext("TourDent")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.join()
    Name("Tour FL241")


# TourBatman(1)
# DentsY(1)
# DentsX(1)
# DentUnion(1)
# Finition1(1)
# EscaliersBatman(1)


def TourFL241(m30):
    TourBatman(1)
    DentsY(1)
    DentsX(1)
    Finition1(1)
    EscaliersBatman(1)
    UnionTotal(1)


# TourFL241(1)

print(
    "-----------------------------------------------Construcution Volumetrique Tour S21----------------------------------------------"
)


def TourSilo(s1):
    Cylindre(40, X, X, Z, pos_X, pos_Y, pos_Z + (Z))
    Name("Tour Silo")
    Cylindre(40, X / 8, X / 8, Z * 2, pos_X, pos_Y, pos_Z + (Z))
    Name("Tour SiloVide")
    Soustraction("Tour Silo", "Tour SiloVide")
    Boite(pos_X, pos_Y + X, pos_Z + (Z * 2), X * 2, X - (X / 3), 2)
    Rotation(2, 0.191986)
    Name("Tour Silo Negatif")
    Soustraction("Tour Silo", "Tour Silo Negatif")
    Boite(pos_X, pos_Y + X, pos_Z + (Z * 2), X * 2, X - (X / 3), 2)
    Rotation(2, -0.191986)
    Name("Tour Silo Negatif2")
    Soustraction("Tour Silo", "Tour Silo Negatif2")
    Cylindre(40, X / 3.2, X / 3.2, 4, pos_X, pos_Y + X / 2, pos_Z + (Z * 2))
    Name("Tour SiloVide2")
    Soustraction("Tour Silo", "Tour SiloVide2")


def OuvertureSiloY(s2):
    Boite(pos_X, pos_Y - X, pos_Z + (Z * 2) - 1.5, 0.8, 1, 1)
    Name("Ouvertures Y")
    Multiplication(50, -5)
    Soustraction("Tour Silo", "Ouvertures Y")
    Boite(pos_X, pos_Y + X, pos_Z + (Z * 2) - 6, 0.8, 1, 1)
    Name("Ouvertures X")
    Multiplication(50, -5)
    Soustraction("Tour Silo", "Ouvertures X")
    Boite(pos_X + X, pos_Y, pos_Z + (Z * 2) - 6, 0.8, 1, 1)
    Name("Ouvertures YX")
    Multiplication(50, -5)
    Soustraction("Tour Silo", "Ouvertures YX")
    Boite(pos_X - X, pos_Y, pos_Z + (Z * 2) - 6, 0.8, 1, 1)
    Name("Ouvertures XY")
    Multiplication(50, -5)
    Soustraction("Tour Silo", "Ouvertures XY")


def TourS21(s58):
    TourSilo(1)
    OuvertureSiloY(1)


print(
    "-----------------------------------------------Construcution Volumetrique Base 1----------------------------------------------"
)


def BaseTour(l4):
    Boite(pos_X, pos_Y, pos_Z - (ZB), XB, YB, ZB)
    Name("Base1")
    ChanfreinVariante(0.1)


def Cheminee(l5):
    Cylindre(40, 2, 2, ZB / DDay, pos_X - XB, pos_Y + X / 2, pos_Z - ZB / 2)
    Name("Cheminee")
    Cylindre(40, 2 - 0.12, 2 - 0.12, ZB, pos_X - XB, pos_Y + X / 2, pos_Z - ZB / 2)
    Name("ChemineeVide")
    Soustraction("Cheminee", "ChemineeVide")


def UnionTotal(m57):
    SelectObject("Base1")
    SelectObjectContext("Base1")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.join()
    Name("Basok")


def Base1(ter1):
    BaseTour(1)
    Cheminee(1)
    UnionTotal(1)
    if Doll >= 5:
        Deplacer(XB / 2, YB / 3, 0)


print(
    "-----------------------------------------------Fonctions des Tours fire Control Haut E---------------------------------------------"
)


def TourEtBaseE(l1):

    if X >= 7 and Y > 7:
        TourS448(1)

    if X <= 7 and Y <= 7:
        TourFL354(1)
    if X == 7 and Y == 7:
        TourS487(1)

    if X < 7 and Y > 7:
        TourFL241(1)
    if X > 7 and Y < 7:
        TourS21(1)


# TourS448(1)
# TourFL354(1)
# TourS487(1)
###Soucis
# TourFL241(1)
# TourS21(1)

print(
    "-----------------------------------------------Appel des Tour B et E----------------------------------------------"
)

# Base1(1)
# TourEtBaseE(1)
# print(X,Y,Z)


def Assemble(ref):
    Base1(1)
    TourEtBaseE(1)
    # if Doll >=5 :
    # SelectObject("DentsBaseX")
    # SelectObjectContext("DentsBaseX")
    bpy.ops.object.select_all(action="TOGGLE")
    SelectObject("Basok")
    SelectObjectContext("Basok")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.join()
    Name("Volcan")

    print(X, Y, Z)


# Assemble(1)
#####################################################POSTECLINIQUEBUNKER BAS#############################################################################
print(
    "-----------------------------------------------Construcution Volumetrique----------------------------------------------"
)


def PosteCliniqueA(a):
    Boite(pos_X, pos_Y, Z / 3, X, Y, Z)
    Chanfrein(1)
    Name("PCBA")


def Portes2A(a):
    # Porte une
    Boite(-X + 1.5, Y / 2, (Z / 2) + (Z / 5), 1, 1, 2)
    Name("Porte")
    Soustraction("PCBA", "Porte")
    Boite(-X + 1.5, -Y / 2, (Z / 2) + (Z / 5), 1, 1, 2)
    Name("Porte")
    Soustraction("PCBA", "Porte")


def Portes3A(a):
    # Porte une
    Boite(-X + 1.5, 0, (Z / 2) + (Z / 4), 1, 1, 2)
    Name("Porte")
    Soustraction("PCBA", "Porte")
    #####################################################################""Porte de
    Boite(-X + 1.5, (Y / 2) + (Y / 4), (Z / 2) + (Z / 4), 1, 1, 2)
    Name("Porte")
    Soustraction("PCBA", "Porte")
    ################################################################Porte trois
    Boite(-X + 1.5, -(Y / 2) - (Y / 4), (Z / 2) + (Z / 4), 1, 1, 2)
    Name("Porte")
    Soustraction("PCBA", "Porte")


def AnnexeA(a):
    # Annexe une
    Boite(-X - 1.5, -Y + (Y / 3), Z / 3, 3, Y / 3, Z)
    Chanfrein(1)
    Name("AnnexeA")
    Boite(-X + 0.5, -Y + (Y / 2), (Z / 2) + (Z / 4), 1, Y / 4, 2)
    Name("PorteA")
    Soustraction("AnnexeA", "PorteA")


def AnnexeB(a):
    # Annexe deux
    Boite(-X - 1.5, -(-Y + (Y / 3)), Z / 3, 3, Y / 3, Z)
    Chanfrein(1)
    Name("AnnexeA")
    Boite(-X + 0.5, -(-Y + (Y / 2)), (Z / 2) + (Z / 4), 1, Y / 4, 2)
    Name("PorteA")
    Soustraction("AnnexeA", "PorteA")


def BlocA(a):
    # Aile A
    Boite(-X / 2 + 1, -Y - 1, Z / 3, X / 2, Y / 4, Z)
    Name("AA")
    Dupliquer(X, 0, 0)
    Resize(1, 3, 1)
    # bpy.ops.transform.resize(value=(1, 3, 1), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=False)
    Rotation(2, E)
    Name("NantiAA")
    Soustraction("AA", "NantiAA")
    # JOINDRE
    SelectObject("AA")
    # bpy.context.scene.objects.active = bpy.data.objects["AA"]
    Chanfrein(1)
    Union("PCBA")
    Joindre(1)
    Name("PCBA")
    # Aile B
    Boite(-X / 2 + 1, Y + 1, Z / 3, X / 2, Y / 4, Z)
    Name("AB")
    Dupliquer(X, 0, 0)
    bpy.ops.transform.resize(
        value=(-1, -3, 1),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    Rotation(2, -U)
    Name("NantiAB")
    Soustraction("AB", "NantiAB")
    SelectObject("AB")
    Chanfrein(1)
    Union("PCBA")
    Joindre(1)
    Name("PCBA")


def MortierA(a):
    # Cylindre(d,1,1,2,-X/2+1, -Y-1, Z/3)
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=32,
        radius=2,
        depth=Z,
        end_fill_type="NGON",
        view_align=False,
        enter_editmode=False,
        location=(-X / 2 + 1, -Y - 1, Z / 3),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(1, 1, 2),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.ops.transform.translate(
        value=(0, 0, Z + (Z / 4)),
        constraint_axis=(False, False, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    Name("TrouM")
    Soustraction("PCBA", "TrouM")
    # Mettre le Torus
    bpy.ops.mesh.primitive_torus_add(
        location=(-X / 2 + 1, -Y - 1, Z),
        rotation=(0, 0, 0),
        view_align=False,
        major_radius=2,
        minor_radius=0.5,
        major_segments=28,
        minor_segments=6,
        use_abso=False,
        abso_major_rad=5,
        abso_minor_rad=2.5,
    )
    bpy.ops.transform.translate(
        value=(0, 0, Z / 3),
        constraint_axis=(False, False, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    Name("TrouT")
    # bpy.context.object.name = "TrouT"
    # Joindre Torus
    bpy.context.scene.objects.active = bpy.data.objects["TrouT"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["PCBA"]
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.join()
    bpy.context.object.name = "PCBA"
    bpy.ops.object.select_all(action="TOGGLE")
    # Rajout d'une porte
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(-X + 1.5, -Y - 2, (Z / 2) + (Z / 5)),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(X / 2, 0.8, 2),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.context.object.name = "Porte"
    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Porte"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects["Porte"]
    bpy.ops.object.delete(use_global=False)


def MortierB(a):
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=32,
        radius=2,
        depth=Z,
        end_fill_type="NGON",
        view_align=False,
        enter_editmode=False,
        location=(-X / 2 + 1, Y + 1, Z / 3),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(1, 1, 2),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.ops.transform.translate(
        value=(0, 0, Z + (Z / 4)),
        constraint_axis=(False, False, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.object.name = "TrouM"
    # soustraire le cylindre
    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["TrouM"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects["TrouM"]
    bpy.ops.object.delete(use_global=False)
    # Mettre le Torus
    bpy.ops.mesh.primitive_torus_add(
        location=(-X / 2 + 1, Y + 1, Z),
        rotation=(0, 0, 0),
        view_align=False,
        major_radius=2,
        minor_radius=0.5,
        major_segments=28,
        minor_segments=6,
        use_abso=False,
        abso_major_rad=5,
        abso_minor_rad=2.5,
    )
    bpy.ops.transform.translate(
        value=(0, 0, Z / 3),
        constraint_axis=(False, False, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.object.name = "TrouT"
    # Joindre Torus
    bpy.context.scene.objects.active = bpy.data.objects["TrouT"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "UNION"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["PCBA"]
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.join()
    bpy.context.object.name = "PCBA"
    bpy.ops.object.select_all(action="TOGGLE")
    # Rajout d'une porte
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(-X + 1.5, Y + 2, (Z / 2) + (Z / 5)),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(X / 2, 0.8, 2),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.context.object.name = "Porte"
    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Porte"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects["Porte"]
    bpy.ops.object.delete(use_global=False)


def AngleA(a):
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(X * 1.8, pos_Y, Z / 3),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(X, Y, Z),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.ops.transform.translate(
        value=(-X * 2, Y, 0),
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.object.rotation_euler[2] = 0.785398
    bpy.context.object.name = "AngleA"
    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["AngleA"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects["AngleA"]
    bpy.ops.object.delete(use_global=False)


def AngleB(a):
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(X * 1.8, pos_Y, Z / 3),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    bpy.ops.transform.resize(
        value=(X, Y, Z),
        constraint_axis=(True, True, True),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    bpy.ops.transform.translate(
        value=(-X * 2, -Y, 0),
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.object.rotation_euler[2] = -0.785398
    bpy.context.object.name = "AngleB"
    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["AngleB"]
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
    bpy.context.scene.objects.active = bpy.data.objects["AngleB"]
    bpy.ops.object.delete(use_global=False)


# definition du bunker bas clinique###########################################""
def BunkerBasclinique(jh):
    PosteCliniqueA(1)
    if Prob_Ann >= 9:
        AngleA(1)
        AngleB(1)
    if Prob_Ann == 5:
        AngleA(1)
    if Prob_Ann == 2:
        AngleB(1)
    if Y <= 25:
        Portes2A(1)
    if Y > 25:
        Portes3A(1)
        if Prob_Mort == 4:
            AnnexeA(1)
        else:
            if Prob_Mort == 4:
                AnnexeB(1)

    if Prob_Mort >= 7:
        BlocA(1)
        MortierA(1)
    if Prob_Mort < 3:
        BlocA(1)
        MortierB(1)
    if Prob_Mort == 5:
        BlocA(1)
        MortierA(1)
        MortierB(1)

    bpy.context.scene.objects.active = bpy.data.objects["PCBA"]
    bpy.context.object.name = "PosteDeSecours"
    bpy.context.object.data.name = "PosteDeSecours"
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.select_all(action="TOGGLE")


# BunkerBasclinique(1)


##############################################################################################################################################

print(
    "-----------------------------------------------Creation du sol----------------------------------------------"
)


def Paysage(v1):
    bpy.ops.mesh.landscape_add(
        AutoUpdate=True,
        SphereMesh=False,
        SmoothMesh=True,
        Subdivision=64,
        MeshSize=2,
        RandomSeed=0,
        NoiseSize=1,
        NoiseType=v2,
        BasisType="0",
        VLBasisType="0",
        Distortion=1,
        HardNoise=True,
        NoiseDepth=6,
        mDimension=1,
        mLacunarity=2,
        mOffset=1,
        mGain=1,
        MarbleBias="0",
        MarbleSharp="0",
        MarbleShape="0",
        Invert=False,
        Height=HS,
        Offset=0,
        Falloff="1",
        Sealevel=-4,
        Plateaulevel=HS * 2,
        Strata=3,
        StrataType="0",
    )
    Name("Paysage")
    bpy.ops.transform.resize(
        value=(100, 60, 1),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=False,
    )
    Rotation(2, 1.5708)


def BunkerHaut(v3):
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(0, 0, HS - 3),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    Name("Volcan")
    SelectObject("Volcan")
    SelectObjectContext("Volcan")
    bpy.ops.transform.translate(
        value=(-5.36458, 3.86447, 13.3745),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.scene.tool_settings.use_snap = True
    bpy.context.scene.tool_settings.snap_element = "VERTEX"
    bpy.context.scene.tool_settings.snap_target = "ACTIVE"
    bpy.ops.transform.translate(
        value=(0, 0, -HS / 4),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    SelectObject("Paysage")
    # bpy.context.scene.tool_settings.use_snap = True
    # bpy.context.scene.tool_settings.snap_target = 'ACTIVE'
    bpy.ops.transform.translate(
        value=(0, 0, -8),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.scene.tool_settings.use_snap = False
    bpy.ops.object.select_all(action="TOGGLE")


def BunkerBas(v4):
    # bpy.ops.mesh.primitive_cube_add(radius=1, view_align=False, enter_editmode=False, location=(0, 0, HS-3), rotation=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
    # Name("Volcan")
    Assemble(1)
    """SelectObject("Volcan")
    SelectObjectContext("Volcan")
    bpy.ops.transform.translate(value=(-8, 3.86447, HS), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=True)
    bpy.context.scene.tool_settings.use_snap = True
    bpy.context.scene.tool_settings.snap_element = 'VERTEX'
    bpy.context.scene.tool_settings.snap_target = 'ACTIVE'
    bpy.ops.transform.translate(value=(0, 0, -HS/3), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=True)
    SelectObject("Paysage")
    #bpy.context.scene.tool_settings.use_snap = True
    #bpy.context.scene.tool_settings.snap_target = 'ACTIVE'
    bpy.ops.transform.translate(value=(0, 0, -2), constraint_axis=(False, False, False), constraint_orientation='GLOBAL', mirror=False, proportional='DISABLED', proportional_edit_falloff='SMOOTH', proportional_size=1, snap=False, snap_target='CLOSEST', snap_point=(0, 0, 0), snap_align=False, snap_normal=(0, 0, 0), texture_space=False, release_confirm=True)
    bpy.context.scene.tool_settings.use_snap = False
    bpy.ops.object.select_all(action='TOGGLE')"""


def BunkerSol(v5):
    bpy.ops.mesh.primitive_cube_add(
        radius=1,
        view_align=False,
        enter_editmode=False,
        location=(0, 0, 0),
        rotation=(0, 0, 0),
        layers=(
            True,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
            False,
        ),
    )
    Name("Volcan")
    SelectObject("Volcan")
    SelectObjectContext("Volcan")
    bpy.ops.transform.translate(
        value=(-8, 3.86447, HS),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.scene.tool_settings.use_snap = True
    bpy.context.scene.tool_settings.snap_element = "VERTEX"
    bpy.context.scene.tool_settings.snap_target = "ACTIVE"
    bpy.ops.transform.translate(
        value=(0, 0, +HS / 3),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    SelectObject("Paysage")
    # bpy.context.scene.tool_settings.use_snap = True
    # bpy.context.scene.tool_settings.snap_target = 'ACTIVE'
    bpy.ops.transform.translate(
        value=(0, 0, -HS / 3),
        constraint_axis=(False, False, False),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        snap=False,
        snap_target="CLOSEST",
        snap_point=(0, 0, 0),
        snap_align=False,
        snap_normal=(0, 0, 0),
        texture_space=False,
        release_confirm=True,
    )
    bpy.context.scene.tool_settings.use_snap = False
    bpy.ops.object.select_all(action="TOGGLE")


print(
    "-----------------------------------------------Bunkerland----------------------------------------------"
)


if HS <= 20 and HS > 5:
    v2 = "2"
    Paysage(1)
    if Doll < 5:
        BunkerBas(1)
    if Doll >= 5:
        AObservationsMultiples(1)


if HS <= 5:
    v2 = "2"
    Paysage(1)
    if Doll <= 3:
        BunkerBasclinique(1)
    if Doll > 3 and Doll <= 6:
        FcbC(1)
    if Doll > 6:
        AObservationsMultiples(1)


if HS > 20:
    v2 = "0"
    Paysage(1)
    if Doll < 5:
        PosteObservationHaut(1)
    if Doll >= 5:
        FcbC(1)


print("la hauteur du site vaut", HS, "le type de terrain", v2, "les dés donnent", Doll)

# BunkerLand(1)

# Paysage(1)
# BoitePositionné(1)
