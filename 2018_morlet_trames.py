# SPDX-FileCopyrightText: 2018 Charles-Antoine Grignard & Pierre Zeytoune
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Script réalisé sous Blender Hash: f4dc9f9
# 17-01-2019 Auteurs: z3yZey & Ch4rly

### ### Interprétation formelle par modèle procédural de travaux par trames de François Morellet ### ###

import bpy
import random
from bpy import data as D

#### NETOYAGE PRELIMINAIRE ###


def delete_all():
    for item in D.objects:
        D.objects.remove(item)
    for item in D.meshes:
        D.meshes.remove(item)


delete_all()

### CREATION DU CADRE 10x10 ###


def Cube(position, dimension, nom):
    bpy.ops.mesh.primitive_cube_add(location=position)
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.name = nom
    bpy.context.object.show_wire = False
    bpy.context.object.draw_type = "WIRE"


Cube((0, 0, 5), (5, 5, 5), "Cadre")

### CREATION DES LAMES ###

hauteur = [0.8333, 1, 1.25, 1.6665]
hauteurRandom = random.choice(hauteur)


def Lame(_Nom, _Position):
    bpy.ops.mesh.primitive_cube_add()
    bpy.context.object.scale[0] = 10
    bpy.context.object.scale[1] = hauteurRandom
    bpy.context.object.scale[2] = 0.05
    bpy.ops.object.modifier_add(type="ARRAY")
    bpy.context.object.modifiers["Array"].count = 20
    bpy.context.object.modifiers["Array"].relative_offset_displace[0] = 0
    bpy.context.object.modifiers["Array"].relative_offset_displace[1] = 0
    bpy.context.object.modifiers["Array"].relative_offset_displace[2] = random.randint(
        8, 15
    )
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Array")
    bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_VOLUME")
    bpy.context.object.location[2] = _Position
    bpy.context.object.rotation_euler[0] = 1.5708
    bpy.context.object.rotation_euler[2] = random.randint(0, 1000)
    bpy.context.object.name = _Nom


if hauteurRandom == 0.8333:
    Lame("Plan 1", 0.83), Lame("Plan 2", 2.49), Lame("Plan 3", 4.16), Lame(
        "Plan 4", 5.83
    ), Lame("Plan 5", 7.49), Lame("Plan 6", 9.16)

elif hauteurRandom == 1:
    Lame("Plan 1", 1), Lame("Plan 2", 3), Lame("Plan 3", 5), Lame("Plan 4", 7), Lame(
        "Plan 5", 9
    )

elif hauteurRandom == 1.25:
    Lame("Plan 1", 1.25), Lame("Plan 2", 3.75), Lame("Plan 3", 6.25), Lame(
        "Plan 4", 8.75
    )

elif hauteurRandom == 1.6665:
    Lame("Plan 1", 1.66), Lame("Plan 2", 4.99), Lame("Plan 3", 8.33)

print(hauteurRandom)

### ROGNAGE BOLLYWOODIEN DES LAMES ###

bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["Cadre"].select = False
bpy.ops.object.join()
bpy.ops.object.modifier_add(type="BOOLEAN")
bpy.context.object.modifiers["Boolean"].solver = "CARVE"
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Cadre"]
bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Boolean")
bpy.ops.object.select_all(action="DESELECT")

### SUPPRESSION DU CADRE ###

bpy.data.objects["Cadre"].select = True
bpy.ops.object.delete(use_global=False)

### ROTATION DU VOLUME FINAL ###

bpy.ops.object.select_all(action="SELECT")
bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0))
bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_VOLUME")
bpy.context.object.location[0] = 0
bpy.context.object.location[1] = 0
bpy.context.object.location[2] = 5
bpy.ops.object.select_all(action="DESELECT")

### CAMERA ISOMETRIQUE ###

CAM_Focale = 40


def CameraISO(_Rot, _Loc, _Foc, _Name):
    bpy.ops.object.camera_add(location=_Loc, rotation=_Rot)
    bpy.context.object.data.type = "ORTHO"
    bpy.context.object.data.ortho_scale = _Foc
    bpy.context.object.name = _Name


CameraISO((0.785395, 0, -2.35619), (-15, 15, 25), CAM_Focale, "Caméra Iso")

### ### FIN DU CODE ### ###
