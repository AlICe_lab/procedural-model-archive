# SPDX-FileCopyrightText: 2014 Simone Peluso
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Examen Python Blender 22/01/2015
# Développé sous Blender 2.70 hash 19e627c / Mac OS

import bpy
import random


def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)


nettoyage()


# Définition des éléments de construction
def Sol(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4, 4, 0.3))


def Muro(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 4, 3))


def Muro_oriz(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4, 0.3, 3))


def Muro_vert(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(4, 0.3, 3))


def Muro_tet(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(12.3, 0.3, 0.8))


def Muro_tet_or(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 12.3, 0.8))


def Pil(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cylinder_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 0.3, 3))


def Pil_haut(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cylinder_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 0.3, 9))


def Pil_cube(pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.2, 0.2, 3))


def Boite(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(dim_X, dim_Y, dim_Z))


# -----CONDITIONS FONDAMENTALES-------------------------------------------------------------
a = random.randrange(0, 9, 8)  # Position du sol au première etage
b = random.randrange(0, 9, 8)  # Position du sol au deuxière etage
c = random.randrange(-10, 1, 10)  # Position des escaliers

# Creation du sol avec les coordonnées dans lequel doit se déplacer
Boite(a, -4, 3.15, 4, 12, 0.3)
Boite(b, -4, 6.15, 4, 12, 0.3)

# Première recomposition--------Saltzman House---------------------------------------------------
if a == 0 and b == 0 and c == -10:
    # Géométrie de la maison
    for i in range(-8, -2, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, 0, 6.15)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 4.8)
        Muro(9.85, i, 1.8)
        Muro(9.85, i, 4.8)
        Muro(9.85, i, 7.8)
    for i in range(8, 10, 4):
        Muro_vert(i, -9.85, 1.8)
        Muro_vert(i, -9.85, 4.8)
        Muro_vert(i, -9.85, 7.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    # Piliers
    for i in range(0, 10, 3):
        Pil_haut(i, -8, 4.5)
    for i in range(2, 10, 3):
        Pil_haut(i, -5, 4.5)
    for i in range(5, 10, 3):
        Pil_haut(i, -2, 4.5)
    for i in range(8, 10, 3):
        Pil_haut(i, 1, 4.5)
    # Creation rebord de fenetre du toit
    for i in range(-10, 3, 12):
        Muro_tet(4, i, 9.5)
    for i in range(-2, 11, 12):
        Muro_tet_or(i, -4, 9.5)
    # Deuxieme maison avec passerelle
    Boite(23, 0, 3, 6, 3, 5)
    Boite(15, 1, 3.3, 10, 1, 0.5)
    for i in range(12, 19, 3):  # Piliers de la passerelle
        Pil(i, 1, 1.8)
        # Creation du rebord de fenetre à Gauche
        bpy.ops.mesh.primitive_cube_add(location=(-1.85, -4, 6.7))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(0.3, 12, 0.8))
        # Creation du rebord de fenetre en Face
        bpy.ops.mesh.primitive_cube_add(location=(0, -9.85, 6.7))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(4, 0.3, 0.8))
        # Creation Petit Sol
        bpy.ops.mesh.primitive_cube_add(location=(6, -9, 3.15))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(8, 2, 0.3))
        # Creation  du sol du toit
        Boite(4, -4, 9.14, 12, 12, 0.3)
        # Creation grand mur vertical
        Boite(1.85, -6, 4.8, 0.3, 8, 9)
        # Sol
        Boite(0, 0, 0.15, 30, 30, 0.3)
        Boite(30, 0, 0.15, 30, 30, 0.3)
        # Importation du module des Escaliers
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/escalier.3ds"
        )
        bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
        bpy.ops.transform.translate(value=(-4.12, c, 1))
        bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
        # Importation du module du Chaminée
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/camino.3ds"
        )
        bpy.ops.transform.resize(value=(1, 1, 1))
        bpy.ops.transform.translate(value=(8, -7, 0.1))
        bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
        # Importation des modules de la Fenetre
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 1.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 4.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 7.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(0.1, -9.85, 1.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(0.1, -9.85, 4.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        # Importation du module du Balcon
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/balcone.3ds"
        )
        bpy.ops.transform.resize(value=(100, 65, 100))
        bpy.ops.transform.translate(value=(0, -10.90, 3.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))

# Deuxième recomposition-------------------------------------------------------------------------------
if a == 0 and b == 0 and c == 0:
    # Géométrie de la maison
    for i in range(-8, -2, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, 0, 6.15)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(9.85, i, 1.8)
        Muro(9.85, i, 4.8)
        Muro(9.85, i, 7.8)
    for i in range(-8, -3, 4):
        Muro(-1.85, i, 4.8)
    for i in range(8, 10, 4):
        Muro_vert(i, -9.85, 1.8)
        Muro_vert(i, -9.85, 4.8)
        Muro_vert(i, -9.85, 7.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    # Piliers
    for i in range(0, 10, 3):
        Pil_haut(i, -8, 4.5)
    for i in range(2, 10, 3):
        Pil_haut(i, -5, 4.5)
    for i in range(5, 10, 3):
        Pil_haut(i, -2, 4.5)
    for i in range(8, 10, 3):
        Pil_haut(i, 1, 4.5)
    # Creation rebord de fenetre du toit
    for i in range(-10, 3, 12):
        Muro_tet(4, i, 9.5)
    for i in range(-2, 11, 12):
        Muro_tet_or(i, -4, 9.5)
        # Creation du rebord de fenetre à Gauche
        bpy.ops.mesh.primitive_cube_add(location=(-1.85, -4, 6.7))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(0.3, 12, 0.8))
        # Creation du rebord de fenetre en Face
        bpy.ops.mesh.primitive_cube_add(location=(0, -9.85, 6.7))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(4, 0.3, 0.8))
        # Creation Petit Sol
        bpy.ops.mesh.primitive_cube_add(location=(6, -9, 3.15))
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.resize(value=(8, 2, 0.3))
        # Creation  du sol du toit
        Boite(4, -4, 9.14, 12, 12, 0.3)
        # Creation grand mur vertical
        Boite(1.85, -6, 4.8, 0.3, 8, 9)
        # Sol
        Boite(0, 0, 0.15, 30, 30, 0.3)
        # Importation du module des Escaliers
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/escalier.3ds"
        )
        bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
        bpy.ops.transform.translate(value=(-4.12, c - 0.75, 1))
        bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
        # Importation du module du Chaminée
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/camino.3ds"
        )
        bpy.ops.transform.resize(value=(1, 1, 1))
        bpy.ops.transform.translate(value=(8, -7, 0.1))
        bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
        # Importation des modules de la Fenetre
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 1.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 4.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(4, -9.85, 7.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(0.1, -9.85, 1.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(0.1, -9.85, 4.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
        # Importation des modules de la Fenetre
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/vetri.3ds"
        )
        bpy.ops.transform.resize(value=(10, 10, 10))
        bpy.ops.transform.translate(value=(-1.85, -0.1, 4.5))
        bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
        # Importation du module du Balcon
        bpy.ops.import_scene.autodesk_3ds(
            filepath="/Users/simone/Desktop/Blender/balcone.3ds"
        )
        bpy.ops.transform.resize(value=(100, 65, 100))
        bpy.ops.transform.translate(value=(0, -10.90, 3.5))
        bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))

# Troisième recomposition--------Giovannitti House---------------------------------------------------
if a == 8 and b == 0 and c == -10:
    # Géométrie de la maison
    for i in range(-8, 1, 4):
        Sol(-i, 0, 0.15)
        Sol(0, i, 0.15)
        Sol(4, i, 0.15)
        Sol(8, i, 0.15)
        Sol(8, i, 3.15)
        Sol(-i, 0, 3.15)
        Sol(0, i, 6.15)
        Sol(-i, 0, 6.15)
        Sol(0, i, 9.15)
        Sol(-i, 0, 9.15)
    for i in range(0, 12, 3):
        Pil_cube(i, -1, 1.8)
        Pil_cube(i, -1, 4.8)
        Pil_cube(i, -1, 7.8)
    # Creation Pilo Haut
    bpy.ops.mesh.primitive_cylinder_add(location=(1.8, -9.8, 4.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 0.3, 9))
    # Creation Petit Sol
    bpy.ops.mesh.primitive_cube_add(location=(4.25, -5, 3.15))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(3.5, 6, 0.3))
    # Creation Petit Colonne
    bpy.ops.mesh.primitive_cube_add(location=(9, 1, 4.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.2, 0.2, 9))
    # Piliers
    for i in range(0, 5, 3):
        Pil_cube(i, -6, 1.8)
        Pil_cube(i, -6, 4.8)
        Pil_cube(i, -6, 7.2)
    for i in range(3, 12, 3):
        Pil_cube(i, -7.8, 1.8)
        Pil_cube(i, -7.8, 4.8)
        Pil_cube(i, -7.8, 7.2)
    # Creation Petit Toit
    bpy.ops.mesh.primitive_cube_add(location=(6, -5, 8.6))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(8, 6, 0.3))
    Boite(6, -7.85, 9, 8, 0.3, 0.8)
    Boite(6, -1.85, 9, 8, 0.3, 0.8)
    Boite(2.15, -4.85, 9, 0.3, 6, 0.8)
    Boite(9.85, -4.85, 9, 0.3, 6, 0.8)
    Boite(-1.85, -4, 9.6, 0.3, 12, 0.8)
    Boite(0, -9.85, 9.6, 4, 0.3, 0.8)
    Boite(1.85, -5.7, 9.6, 0.3, 8, 0.8)
    Boite(6, -1.85, 9.6, 8, 0.3, 0.8)
    Boite(4, 1.85, 9.6, 12, 0.3, 0.8)
    Boite(9.85, 0, 9.6, 0.3, 4, 0.8)
    Boite(9.85, -1.5, 7.8, 0.3, 6.5, 3)
    Boite(8, -9.85, 1.8, 4, 0.3, 3)
    Boite(6, -8, 1.8, 0.3, 4, 3)
    Boite(9.85, 0, 3.6, 0.3, 4, 0.8)
    Boite(7.85, 1.85, 3.6, 4, 0.3, 0.8)

    for i in range(-8, -2, 4):
        Muro(9.85, i, 1.8)
    for i in range(-4, -1, 4):
        Muro(9.85, i, 4.8)
    for i in range(0, 6, 4):
        Muro_vert(i, 1.85, 1.8)
        Muro_vert(i, 1.85, 4.8)
    for i in range(0, 10, 4):
        Muro_vert(i, 1.85, 7.8)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 4.8)
        Muro(-1.85, i, 7.8)
    Boite(0, -9.85, 7.8, 4, 0.3, 3)
    Boite(1.85, -8, 7.8, 0.3, 4, 3)
    # Creation du garage à cote de la maison
    for i in range(-8, 1, 4):
        Sol(-4, i - 2, 0.15)
        Sol(-8, i - 2, 0.15)
        Sol(-12, i - 2, 0.15)
        Sol(-4, i - 2, 3.15)
        Sol(-8, i - 2, 3.15)
        Sol(-12, i - 2, 3.15)
    Boite(-8, -12, 0.15, 12, 2, 0.3)
    Boite(-8, -12, 3.15, 12, 2, 0.3)
    Boite(-3, -0.15, 1.8, 2, 0.3, 3)
    Boite(-13, -0.15, 1.8, 2, 0.3, 3)
    Boite(-8, -0.15, 2.85, 8, 0.3, 0.8)
    Boite(-2.15, -10.85, 1.8, 0.3, 4, 3)
    Boite(-13.85, -10.85, 1.8, 0.3, 4, 3)
    for i in range(-12, 0, 4):
        Muro_vert(i, -12.85, 1.8)
    for i in range(-10, 2, 4):
        Muro(-13.85, i, 1.8)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)
    # Importation du module de le Fenetre en face
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.4, -7.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4.4, -7.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.4, -7.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, 1.9, 1.7))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(9.9, -0.1, 1.7))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module du Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(10.3, -5.6, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(-4.12, c, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
    bpy.ops.object.delete(use_global=False)

# Quatrième recomposition----------------------------------------------------------------------------
if a == 8 and b == 0 and c == 0:
    # Géométrie de la maison
    for i in range(-8, 1, 4):
        Sol(-i, 0, 0.15)
        Sol(0, i, 0.15)
        Sol(4, i, 0.15)
        Sol(8, i, 0.15)
        Sol(8, i, 3.15)
        Sol(-i, 0, 3.15)
        Sol(0, i, 6.15)
        Sol(-i, 0, 6.15)
        Sol(0, i, 9.15)
        Sol(-i, 0, 9.15)
    for i in range(0, 12, 3):
        Pil_cube(i, -1, 1.8)
        Pil_cube(i, -1, 4.8)
        Pil_cube(i, -1, 7.8)
    # Creation Pilo Haut
    bpy.ops.mesh.primitive_cylinder_add(location=(1.8, -9.8, 4.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.3, 0.3, 9))
    # Creation Petit Sol
    bpy.ops.mesh.primitive_cube_add(location=(4.25, -5, 3.15))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(3.5, 6, 0.3))
    # Creation Petit Colonne
    bpy.ops.mesh.primitive_cube_add(location=(9, 1, 4.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(0.2, 0.2, 9))
    for i in range(0, 5, 3):
        Pil_cube(i, -6, 1.8)
        Pil_cube(i, -6, 4.8)
        Pil_cube(i, -6, 7.2)
    for i in range(3, 12, 3):
        Pil_cube(i, -7.8, 1.8)
        Pil_cube(i, -7.8, 4.8)
        Pil_cube(i, -7.8, 7.2)
    for i in range(-8, -2, 4):
        Muro(9.85, i, 1.8)
    for i in range(-4, -1, 4):
        Muro(9.85, i, 4.8)
    Boite(9.85, -1.5, 7.8, 0.3, 6.5, 3)
    Boite(8, -9.85, 1.8, 4, 0.3, 3)
    Boite(6, -8, 1.8, 0.3, 4, 3)
    Boite(9.85, 0, 3.6, 0.3, 4, 0.8)
    Boite(7.85, 1.85, 3.6, 4, 0.3, 0.8)

    for i in range(0, 6, 4):
        Muro_vert(i, 1.85, 1.8)
        Muro_vert(i, 1.85, 4.8)
    for i in range(0, 10, 4):
        Muro_vert(i, 1.85, 7.8)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 7.8)
    for i in range(-8, -3, 4):
        Muro(-1.85, i, 4.8)
    # Creation Petit Toit
    bpy.ops.mesh.primitive_cube_add(location=(6, -5, 8.6))
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    bpy.ops.transform.resize(value=(8, 6, 0.3))
    Boite(6, -7.85, 9, 8, 0.3, 0.8)
    Boite(6, -1.85, 9, 8, 0.3, 0.8)
    Boite(2.15, -4.85, 9, 0.3, 6, 0.8)
    Boite(9.85, -4.85, 9, 0.3, 6, 0.8)
    Boite(-1.85, -4, 9.6, 0.3, 12, 0.8)
    Boite(0, -9.85, 9.6, 4, 0.3, 0.8)
    Boite(1.85, -5.7, 9.6, 0.3, 8, 0.8)
    Boite(6, -1.85, 9.6, 8, 0.3, 0.8)
    Boite(4, 1.85, 9.6, 12, 0.3, 0.8)
    Boite(9.85, 0, 9.6, 0.3, 4, 0.8)
    Boite(0, -9.85, 7.8, 4, 0.3, 3)
    Boite(1.85, -8, 7.8, 0.3, 4, 3)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)
    # Importation du module de la fenetres en face
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.4, -7.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4.4, -7.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.4, -7.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, 1.9, 1.7))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(9.9, -0.1, 1.7))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -0.1, 4.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    # Importation du module du Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(10.3, -5.6, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(-4.12, c - 0.75, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))

# Cinquième recomposition--------------------------------------------------------------------------
if a == 8 and b == 8 and c == 0:
    # Géométrie de la maison
    for i in range(-8, 1, 4):
        Sol(8, i, 3.15)
        Sol(4, i, 6.15)
        Sol(8, i, 6.15)
    for i in range(-8, -3, 4):
        Sol(4, i, 6.15)
    for i in range(-4, 1, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, 0, 6.15)
    for i in range(0, 1, 4):
        Muro(-1.85, i, 1.8)  # Murs a gouche
        Muro(-1.85, i, 4.8)
        Muro(-1.85, i, 7.8)
    for i in range(-8, 1, 4):
        Muro(9.85, i, 1.8)  # Murs a droit
        Muro(9.85, i, 4.8)
        Muro(9.85, i, 7.8)
    for i in range(8, 10, 4):
        Muro_vert(i, -9.85, 1.8)
        Muro_vert(i, -9.85, 4.8)
        Muro_vert(i, -9.85, 7.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    # Piliers
    for i in range(0, 10, 3):
        Pil_haut(i, -8, 4.5)
    for i in range(0, 8, 3):
        Pil_haut(i, -5, 4.5)
    for i in range(0, 5, 3):
        Pil_haut(i, -2, 4.5)
    for i in range(0, 2, 3):
        Pil_haut(i, 1, 4.5)
    for i in range(-2, 0, 3):
        Pil_haut(i + 0.15, -10, 4.5)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(-4.4, c - 0.75, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
    # Importation du module du Toit
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/tettoblond.3ds"
    )
    bpy.ops.transform.resize(value=(10.5, 10.5, 10))
    bpy.ops.transform.translate(value=(3.7, -4.4, 9.4))
    bpy.ops.transform.rotate(value=-1.57, axis=(0, 0, 1))
    # Importation du module de la Fenetres en face
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(1, -9.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(1, -9.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(1, -9.9, 7.4))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.9, 7.4))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    # Importation du module de la Fenetres à gouche
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -7.9, 1.6))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -3.9, 1.6))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -7.9, 4.5))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -3.9, 4.5))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -7.9, 7.4))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1, -3.9, 7.4))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module du  Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(0, -7, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))

# Sixième recomposition--------------------------------------------------------------------------
if a == 8 and b == 8 and c == -10:
    # Géométrie de la maison
    for i in range(-8, 1, 4):
        Sol(8, i, 3.15)
        Sol(8, i, 6.15)
    for i in range(-4, 1, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, -8, 3.15)
        Sol(-i, 0, 6.15)
        Sol(-i, -8, 6.15)
    for i in range(-8, 1, 4):
        Muro(9.85, i, 1.8)
        Muro(9.85, i, 4.8)
        Muro(9.85, i, 7.8)
    for i in range(0, 2, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 4.8)
        Muro(-1.85, i, 7.8)
    for i in range(-8, -6, 4):
        Muro(-1.85, i, 7.8)
    for i in range(8, 10, 4):
        Muro_vert(i, -9.85, 1.8)
        Muro_vert(i, -9.85, 4.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    for i in range(0, 10, 4):
        Muro_vert(i, -9.85, 7.8)
    # Piliers: les piliers sont present où il n'y a pas des etages, mais sont present où il y a des escaliers ou encastrées dans les murs
    for i in range(0, 8, 3):
        Pil_haut(i, -6, 4.5)
    for i in range(0, 8, 3):
        Pil_haut(i, -2, 4.5)
    for i in range(-2, 0, 3):
        Pil_haut(i + 0.15, -10, 4.5)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)

    # Importation du module du toit
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/tettoblond.3ds"
    )
    bpy.ops.transform.resize(value=(10.5, 10.5, 10))
    bpy.ops.transform.translate(value=(3.7, -4.4, 9.4))
    bpy.ops.transform.rotate(value=-1.57, axis=(0, 0, 1))
    # Importation des modules de les fenetres en face
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0, -9.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.9, 1.6))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0, -9.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.9, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    # Importation du module de la Fenetres à gouche
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.9, 1.6))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.9, 4.5))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -3.9, 7.4))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -3.9, 4.5))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -3.9, 1.6))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(-4.4, c + 1.44, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
    # Importation du module du Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(0, -4, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))

# Septième recomposition--------------------------------------------------------------------------
if a == 0 and b == 8 and c == -10:
    # Géométrie de la maison
    for i in range(-8, -2, 4):
        Sol(0, i, 6.15)
        Sol(8, i, 3.15)
    for i in range(-8, 1, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, 0, 6.15)
    for i in range(-4, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 4.8)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 7.8)
        Muro(1.85, i, 7.8)
        Muro(6, i, 4.8)
        Muro(6, i, 7.8)
        Muro(9.85, i, 1.8)
        Muro(9.85, i, 7.8)
    for i in range(-4, 1, 4):
        Muro(9.85, i, 4.8)
    for i in range(0, 9, 8):
        Muro_vert(i, -9.85, 7.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    # Piliers
    for i in range(-8, 1, 3):
        Pil_haut(1.8, i, 4.5)
        Pil_haut(6, i, 4.5)
    for i in range(-9, -7, 3):
        Pil_haut(1.8, i - 0.80, 4.5)
        Pil_haut(-1.84, i - 0.80, 4.5)
    # Creation rebord de fenetre du toit
    for i in range(-10, 3, 12):
        Muro_tet(4, i, 9.5)
    for i in range(-2, 11, 12):
        Muro_tet_or(i, -4, 9.5)
    # Deuxieme maison avec passerelle
    Boite(23, 0, 3, 6, 3, 5)
    Boite(15, 1, 3.3, 10, 1, 0.5)
    for i in range(12, 19, 3):  # Piliers de la passerelle
        Pil(i, 1, 1.8)
    # Creation  du sol du toit
    Boite(4, -4, 9.14, 12, 12, 0.3)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)
    Boite(30, 0, 0.15, 30, 30, 0.3)
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(12.4, -10, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
    bpy.ops.transform.mirror(
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    # Importation du module du Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(-2.3, -7, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation des modules de la Fenetre
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.1, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 7.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.1, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.8, 4.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(9.88, -7.8, 4.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.8, 1.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    # Importation du module du Balcon
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/balcone.3ds"
    )
    bpy.ops.transform.resize(value=(100, 65, 100))
    bpy.ops.transform.translate(value=(7.9, -10.90, 3.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))

# Octave recomposition----------------------------------------------------------------------------
if a == 0 and b == 8 and c == 0:
    # Géométrie de la maison
    for i in range(-8, -2, 4):
        Sol(0, i, 6.15)
        Sol(8, i, 3.15)
    for i in range(-8, 1, 4):
        Sol(-i, 0, 3.15)
        Sol(-i, 0, 6.15)
    for i in range(-4, 1, 4):
        Muro(-1.85, i, 1.8)
        Muro(-1.85, i, 4.8)
    for i in range(-8, 1, 4):
        Muro(-1.85, i, 7.8)
        Muro(1.85, i, 7.8)
        Muro(6, i, 4.8)
        Muro(6, i, 7.8)
        Muro(9.85, i, 1.8)
        Muro(9.85, i, 7.8)
    for i in range(-8, -3, 4):
        Muro(9.85, i, 4.8)
    for i in range(0, 9, 8):
        Muro_vert(i, -9.85, 7.8)
    for i in range(-8, 1, 4):
        Muro_oriz(-i, 1.85, 1.8)
        Muro_oriz(-i, 1.85, 4.8)
        Muro_oriz(-i, 1.85, 7.8)
    # Piliers
    for i in range(-8, 1, 3):
        Pil_haut(1.8, i, 4.5)
        Pil_haut(6, i, 4.5)
    for i in range(-9, -7, 3):
        Pil_haut(1.8, i - 0.80, 4.5)
        Pil_haut(-1.84, i - 0.80, 4.5)
    # Creation rebord de fenetre du toit
    for i in range(-10, 3, 12):
        Muro_tet(4, i, 9.5)
    for i in range(-2, 11, 12):
        Muro_tet_or(i, -4, 9.5)
    # Creation  du sol du toit
    Boite(4, -4, 9.14, 12, 12, 0.3)
    # Sol
    Boite(0, 0, 0.15, 30, 30, 0.3)
    # Importation du module des Escaliers
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/escalier.3ds"
    )
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))
    bpy.ops.transform.translate(value=(12.4, c + 0.6, 1))
    bpy.ops.transform.rotate(value=3.12, axis=(0, 0, 1))
    bpy.ops.transform.mirror(
        constraint_axis=(False, True, False),
        constraint_orientation="GLOBAL",
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    # Importation du module du Chaminée
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/camino.3ds"
    )
    bpy.ops.transform.resize(value=(1, 1, 1))
    bpy.ops.transform.translate(value=(-2.3, -7, 0.1))
    bpy.ops.transform.rotate(value=4.70, axis=(0, 0, 1))
    # Importation du module de la Fenetre
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.1, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(4, -9.85, 7.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(7.9, -9.85, 1.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(0.1, -9.85, 4.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.8, 4.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(9.88, 0, 4.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/vetri.3ds"
    )
    bpy.ops.transform.resize(value=(10, 10, 10))
    bpy.ops.transform.translate(value=(-1.9, -7.8, 1.5))
    bpy.ops.transform.rotate(value=4.7, axis=(0, 0, 1))
    # Importation du module du Balcon
    bpy.ops.import_scene.autodesk_3ds(
        filepath="/Users/simone/Desktop/Blender/balcone.3ds"
    )
    bpy.ops.transform.resize(value=(100, 65, 100))
    bpy.ops.transform.translate(value=(7.9, -10.90, 3.5))
    bpy.ops.transform.rotate(value=0, axis=(0, 0, 1))
