# SPDX-FileCopyrightText: 2016 Vasco Da Re
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Developpe sous Blender 2.78
# Script basé sur le projet de "City in the air" de Arata Isozaki

import bpy
import random
from random import *
import math
from math import radians

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

#########################################################################
# Definition de la fonction de création de camera en projection parallele#
#########################################################################

"""
def axoCam (projection,canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = 'ORTHO'
    monAxoCam.data.ortho_scale = 1000
    if projection == 'axonometrique':
        if canon == 'dimetrie': #OK
            monAxoCam.name = 'axoDimetrie'
            monAxoCam.rotation_euler = (radians(60), 0.0, radians(23))
            monAxoCam.location = (5.53*120, -13.04*120, 8.18*120)
            maScene.pixel_aspect_x = 1
            bpy.context.object.data.clip_end = 3000

axoCam ('axonometrique','dimetrie')
"""

######################
# Ensemble des Mesures#
######################

# Mesures Principales des Piliers

Diametre_Pilier = 15
Hauteur_Base_Pilier = 28
Hauteur_Sommet_Pilier = 10
Hauteur_Pilier_1 = 50.6
Hauteur_Pilier_2 = 63.2
Hauteur_Pilier_3 = 75.8
Hauteur_Pilier_4 = 88.4
Hauteur_Pilier_5 = 101
Hauteur_Pilier_6 = 113.6
Hauteur_Pilier_7 = 126.2

Taille_Piliers = [
    Hauteur_Pilier_1,
    Hauteur_Pilier_2,
    Hauteur_Pilier_3,
    Hauteur_Pilier_4,
    Hauteur_Pilier_5,
    Hauteur_Pilier_6,
    Hauteur_Pilier_7,
]

print(Taille_Piliers)


# Angles des directions possibles

Angle_1 = 0
Angle_2 = 60
Angle_3 = 120
Angle_4 = 180
Angle_5 = 240
Angle_6 = 300

Liste_Angles_Impair = [Angle_1, Angle_3, Angle_5]
Liste_Angles_Pair = [Angle_2, Angle_4, Angle_6]

Liste_Angles_Totale = [Angle_1, Angle_2, Angle_3, Angle_4, Angle_5, Angle_6]


# Distance entre piliers

Distance_Min = 50.6707
Distance_Moy = 88.6644
Distance_Max = 126.6581

Distance_Pilier = [Distance_Min, Distance_Moy, Distance_Max]


##################
# Suite de Piliers#
##################

# Definition de la fonction copier/coller pour les modules


def CopyPaste(Module, GM):

    bpy.context.scene.objects.active = bpy.data.objects[Module]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(GM, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


# Definition des piliers
l_piliers = []


def PilierX(Position):

    if len(l_piliers) == 0:  # 1er Pilier

        px, py, pz = Position

        Hauteur = choice(Taille_Piliers)

        Position = (px, py, pz + Hauteur / 2)

        bpy.ops.mesh.primitive_cylinder_add(
            radius=Diametre_Pilier / 2, depth=Hauteur, location=Position
        )

        l_piliers.append((Position, Hauteur, choice(Liste_Angles_Totale)))

    else:  # Autres Piliers

        last_pilier = l_piliers[-1]

        Position, Hauteur, Angle = last_pilier

        X, Y, Z = Position

        Hauteur = choice(Taille_Piliers)  # Choix aleatoire de la hauteur du pilier
        Pos = choice(
            Distance_Pilier
        )  # Choix aleatoire de la distance entre les piliers

        if Angle == 0:  # Choix aleatoire des angles
            Angle2 = choice((300, 300, 0, 60, 60))
        if Angle == 60:
            Angle2 = choice((0, 0, 60, 120, 120))
        if Angle == 120:
            Angle2 = choice((60, 60, 120, 180, 180))
        if Angle == 180:
            Angle2 = choice((120, 120, 180, 240, 240))
        if Angle == 240:
            Angle2 = choice((180, 180, 240, 300, 300))
        if Angle == 300:
            Angle2 = choice((240, 240, 300, 0, 0))

        x = X + ((math.cos(math.radians(Angle2))) * Pos)  # Calcul des coordonnées
        y = Y + ((math.sin(math.radians(Angle2))) * Pos)

        Position = (x, y, 0 + Hauteur / 2)

        bpy.ops.mesh.primitive_cylinder_add(
            radius=Diametre_Pilier / 2, depth=Hauteur, location=Position
        )

        l_piliers.append((Position, Hauteur, Angle2))

        # Apparition des modules

        dernier = l_piliers[-1]
        Position, Hauteur, Angle = dernier
        X, Y, Z = Position

        avant_dernier = l_piliers[-2]
        Position2, Hauteur2, Angle2 = avant_dernier
        x, y, z = Position2

        Hau_Max = min(
            Hauteur, Hauteur2
        )  # Choix des hauteurs des modules et de leur nombre
        if Hau_Max == Hauteur_Pilier_7:
            Hau_Mod = choice((40.6, 53.2, 65.8, 78.4))
            nbModule = choice((1, 2, 2, 2, 3, 3, 4))
        if Hau_Max == Hauteur_Pilier_6:
            Hau_Mod = choice((40.6, 53.2, 65.8, 78.4))
            nbModule = choice((1, 2, 2, 2, 3, 4))
        if Hau_Max == Hauteur_Pilier_5:
            Hau_Mod = choice((40.6, 53.2, 65.8))
            nbModule = choice((1, 2, 2, 2, 3))
        if Hau_Max == Hauteur_Pilier_4:
            Hau_Mod = choice((40.6, 53.2))
            nbModule = choice((1, 2, 2, 3))
        if Hau_Max == Hauteur_Pilier_3:
            Hau_Mod = choice((28, 40.6))
            nbModule = choice((1, 1, 2))
        if Hau_Max == Hauteur_Pilier_2:
            Hau_Mod = choice((28, 40.6))
            nbModule = 1
        if Hau_Max == Hauteur_Pilier_1:
            Hau_Mod = 28
            nbModule = 1

        # Choix des tailles de modules en fonction de la distance entre les piliers
        if Pos == Distance_Min:
            Module = "PetitModule"
        if Pos == Distance_Moy:
            Module = "MoyenModule"
        if Pos == Distance_Max:
            Module = "GrandModule"

        # Apparition des modules en fonction du nombre choisi
        if nbModule == 1:

            Coupure = choice((1, 1, 1, 1, 1, 1, 1, 1, 0, 0))  # Choix d'une coupure
            if Coupure == 1:
                pilier = CopyPaste(Module, "0")
                pilier.location = (x, y, Hau_Mod)
                pilier.rotation_euler = [0, 0, radians(Angle)]

            if Coupure == 0:
                pilier = 0

        if nbModule == 2:
            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod + 12.6)
            pilier.rotation_euler = [0, 0, radians(Angle)]

        if nbModule == 3:
            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod + 12.6)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod - 12.6)
            pilier.rotation_euler = [0, 0, radians(Angle)]

        if nbModule == 4:
            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod + 12.6)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod - 12.6)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            pilier = CopyPaste(Module, "0")
            pilier.location = (x, y, Hau_Mod + 25.2)
            pilier.rotation_euler = [0, 0, radians(Angle)]

            print(nbModule)


# Génération du tout
for i in range(15):
    PilierX((0, 0, 0))


print(l_piliers)
