# SPDX-FileCopyrightText: 2025 Tetiana Chernysh & Alina Matiienko
# SPDX-FileCopyrightText: 2025 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Laurie Spiegel - Drums
# Blender 4.2.1
# Final Cube Generation

import bpy
import random
import math


####################################
#            Clean up              #
####################################


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
bpy.ops.outliner.orphans_purge()
bpy.context.scene.cursor.location[0] = 0
bpy.context.scene.cursor.location[1] = 0
bpy.context.scene.cursor.location[2] = 0


# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
#################################################################################################
###                                       CUBE INSIDE                                         ###
#################################################################################################
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#


# --- Functions


def make_boolean(base_shape, negative_shape):
    """
    make a Boolean between two given objects.
    """
    base_shape = base_shape.name
    negative_shape = negative_shape.name
    # Create the cube for boolean

    # Select All
    bpy.ops.object.select_all(action="SELECT")

    # Deselect Cube
    bpy.data.objects[negative_shape].select_set(False)

    # Plane active objects
    bpy.context.view_layer.objects.active = bpy.data.objects[base_shape]

    # Boolean action with to differentiate the Cube with the Plane
    bpy.ops.object.modifier_add(type="BOOLEAN")
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[negative_shape]
    bpy.context.object.modifiers["Boolean"].use_self = True
    bpy.ops.object.modifier_apply(modifier="Boolean")

    # Select All
    bpy.ops.object.select_all(action="DESELECT")

    # Deselect Cube
    bpy.data.objects[negative_shape].select_set(True)
    bpy.ops.object.delete(use_global=False)


def vertical_plane(scale, thickness, value):
    # Add the plane
    bpy.ops.mesh.primitive_plane_add(location=(0, -0.045, 0), scale=(1, 1, 1))
    plane = bpy.context.object

    # Scale
    plane.scale = (
        scale[0] / 2,
        scale[1] / 2,
        1,
    )  # Divide by 2 to account for the size of the sides
    plane.scale = (plane.scale[0], 0.1, plane.scale[2])

    # Extrude the plane
    bpy.ops.object.mode_set(mode="EDIT")  # Switching to edit mode
    bpy.ops.mesh.extrude_region_move(
        TRANSFORM_OT_translate={"value": (0, 0, thickness)}
    )
    bpy.ops.object.mode_set(mode="OBJECT")  # Return to object mode

    # Make a rotation
    plane.rotation_euler[0] = (
        1.5708  # Rotation by 90 degrees (π/2 radians) around the X axis
    )

    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.loopcut_slide(
        MESH_OT_loopcut={
            "number_cuts": 1,
            "smoothness": 0,
            "falloff": "INVERSE_SQUARE",
            "object_index": 0,
            "edge_index": 4,
            "mesh_select_mode_init": (False, True, False),
        }
    )
    bpy.ops.transform.translate(value=(0, value, 0))
    bpy.ops.mesh.loopcut_slide(
        MESH_OT_loopcut={
            "number_cuts": 1,
            "smoothness": 0,
            "falloff": "INVERSE_SQUARE",
            "object_index": 0,
            "edge_index": 15,
            "mesh_select_mode_init": (False, True, False),
        }
    )
    bpy.ops.transform.translate(value=(0, -0.03, 0))
    bpy.ops.mesh.loopcut_slide(
        MESH_OT_loopcut={
            "number_cuts": 1,
            "smoothness": 0,
            "falloff": "INVERSE_SQUARE",
            "object_index": 0,
            "edge_index": 6,
            "mesh_select_mode_init": (False, True, False),
        }
    )
    bpy.ops.transform.translate(value=(0, -0.03, 0))
    bpy.ops.object.editmode_toggle()

    return plane


####################################
#            Waves|Planes          #
####################################


# Creating the initial plane
base_plane = vertical_plane(scale=(0.09, 0.09), thickness=0.002, value=0.005)

# Duplicate and scale the planes
num_planes = 26
distance = 0.005  # Distance between planes (5 mm)
scale_decrement = 0.005

for i in range(1, num_planes + 1):
    new_plane = base_plane.copy()
    new_plane.location.y += distance * i  # * random.choice([1,2,3,4])
    new_scale = max(0.05 - scale_decrement * i, random.uniform(0.046, 0.05))
    # new_scale = max(0.05 - scale_decrement *i, 0.05)
    new_plane.scale = (new_scale * random.choice([1.1, 1.2, 1.3, 1.4, 1.5]), 0.05, 1)
    new_plane.scale = (new_plane.scale[0], 0.1, new_plane.scale[2])
    bpy.context.collection.objects.link(new_plane)

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.join()
my_shape = bpy.data.objects[0]
bpy.ops.object.select_all(action="DESELECT")

# ###################################
#            Boolean               #
# ###################################


# make a cube for boolean opertation
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.mesh.primitive_cube_add()
# Size the cube for the boolean
cube = bpy.context.object
cube.scale = (0.05, 1, 1)

# apply boolean
make_boolean(my_shape, cube)

# put origin in median center
bpy.ops.object.select_all(action="DESELECT")
my_shape.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")

# bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="MEDIAN")
# rotate 45
bpy.ops.transform.rotate(value=0.5, orient_axis="X")

# make a cube for boolean opertation 1
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.mesh.primitive_cube_add(scale=(0.1, 0.1, 0.1), location=(0, -0.15, 0))
# Size the cube for the boolean
cube = bpy.context.object
make_boolean(my_shape, cube)

# make a cube for boolean opertation 2
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.mesh.primitive_cube_add(scale=(0.1, 0.1, 0.1), location=(0, 0.15, 0))
# Size the cube for the boolean
cube = bpy.context.object
make_boolean(my_shape, cube)

# make a cube for boolean opertation 3
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.mesh.primitive_cube_add(scale=(0.1, 0.1, 0.1), location=(0, 0, 0.15))
# Size the cube for the boolean
cube = bpy.context.object
make_boolean(my_shape, cube)

# make a cube for boolean opertation 4
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.mesh.primitive_cube_add(scale=(0.1, 0.1, 0.1), location=(0, 0, -0.15))
# Size the cube for the boolean
cube = bpy.context.object
make_boolean(my_shape, cube)


# ####################################
# #            Duplicate             #
# ####################################


# Selecting an object to duplicate
plane = bpy.context.active_object  # the active object in the scene

# Checking that the object exists
if plane:
    # First duplication
    duplicate = plane.copy()  # Copy
    duplicate.data = plane.data.copy()
    bpy.context.collection.objects.link(duplicate)  # Add copy to the collection

    # Rotate the duplicated object 90 degrees around the Z axis
    duplicate.rotation_euler[2] = 1.5708

    # Second duplication
    duplicate2 = plane.copy()  # Copy the object again
    duplicate2.data = plane.data.copy()
    bpy.context.collection.objects.link(duplicate2)  # Add second copy to the collection

    # Rotate the second duplicate 90 degrees around the Y axis
    duplicate2.rotation_euler[1] = 1.5708  # 90 degrees in radians

else:
    print("Active object not selected!")


# Creating of the Base for the cube
def create_Base(pos, dim):
    bpy.ops.mesh.primitive_cube_add(location=pos, scale=dim)


create_Base((0, 0, 0), (0.05, 0.05, 0.05))


bpy.ops.object.select_all(action="SELECT")
# for object in bpy.data.objects:
#    if 'Plane' in object.name:
#        object.select_set(True)
bpy.ops.object.join()
bpy.ops.transform.resize(value=(62, 62, 62))
bpy.ops.object.select_all(action="DESELECT")


# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
#################################################################################################
###                                      CUBE OUTSIDE                                         ###
#################################################################################################
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#
# -----------------------------------------------------------------------------------------------#


# Create a cube with size 1 (default)
# bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))

# cube = bpy.context.object

# Change the dimensions of the cube to 9.7 x 9.7 x 9.7
# cube.scale = (9.4 / 2, 9.4 / 2, 9.7 / 2)

# Apply transformations to fix the dimensions
# bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)


####################################
#    Coordinates of all parts      #
####################################


# ---For the 1st side

coord_cube_side_1_lower_left = (4.85, -3.5, -2.85)
coord_cylinder_side_1_lower_left = (4.85, -2, -2.85)

coord_cube_side_1_middle_left = (4.85, -3.5, 1.15)
coord_cylinder_side_1_middle_left = (4.85, -2, 1.15)

coord_cube_side_1_highest_left = (4.85, -3.5, 4.15)
coord_cylinder_side_1_highest_left = (4.85, -2, 4.15)

###

coord_cube_side_1_lower_right = (4.85, 4, -2.85)
coord_cylinder_side_1_lower_right = (4.85, 5, -2.85)

coord_cube_side_1_middle_right = (4.85, 4, 0.15)
coord_cylinder_side_1_middle_right = (4.85, 5, 0.15)

coord_cube_side_1_highest_right = (4.85, 4, 3.15)
coord_cylinder_side_1_highest_right = (4.85, 5, 3.15)


# ---For the 2nd side

coord_cube_side_2_lower_left = (-3.2, -4.85, -2.85)
coord_cylinder_side_2_lower_left = (-3.2, -3.35, -2.85)

coord_cube_side_2_middle_left = (-3.2, -4.85, 0.65)
coord_cylinder_side_2_middle_left = (-3.2, -3.35, 0.65)

coord_cube_side_2_highest_left = (-3.2, -4.85, 3.65)
coord_cylinder_side_2_highest_left = (-3.2, -3.35, 3.65)

###

coord_cube_side_2_lower_right = (3.2, -4.85, -2.85)
coord_cylinder_side_2_lower_right = (3.2, -2.95, -2.85)

coord_cube_side_2_middle_right = (3.2, -4.85, 0.65)
coord_cylinder_side_2_middle_right = (3.2, -3.35, 0.65)

coord_cube_side_2_highest_right = (3.2, -4.85, 3.65)
coord_cylinder_side_2_highest_right = (3.2, -3.35, 3.65)


# ---For the 3rd side

coord_cube_side_3_lower_left = (-4.85, 3.5, -3.35)
coord_cylinder_side_3_lower_left = (-4.85, 5, -3.35)

coord_cube_side_3_middle_left = (-4.85, 3.5, 0.15)
coord_cylinder_side_3_middle_left = (-4.85, 2, 0.15)

coord_cube_side_3_highest_left = (-4.85, 3.5, 3.65)
coord_cylinder_side_3_highest_left = (-4.85, 5, 3.65)

###

coord_cube_side_3_lower_right = (-4.85, -4, -2.85)
coord_cylinder_side_3_lower_right = (-4.85, -3, -2.85)

coord_cube_side_3_middle_right = (-4.85, -4, 0.65)
coord_cylinder_side_3_middle_right = (-4.85, -3, 0.65)

coord_cube_side_3_highest_right = (-4.85, -4, 3.65)
coord_cylinder_side_3_highest_right = (-4.85, -3, 3.65)


# ---For the 4th side

coord_cube_side_4_lower_left = (3.7, 4.85, -2.85)
coord_cylinder_side_4_lower_left = (3.7, 5.85, -2.85)

coord_cube_side_4_middle_left = (3.7, 4.85, 0.65)
coord_cylinder_side_4_middle_left = (3.7, 5.85, 0.65)

coord_cube_side_4_highest_left = (3.7, 4.85, 3.65)
coord_cylinder_side_4_highest_left = (3.7, 5.85, 3.65)

###

coord_cube_side_4_lower_right = (-2.7, 4.85, -2.85)
coord_cylinder_side_4_lower_right = (-2.7, 6.85, -2.85)

coord_cube_side_4_middle_right = (-2.7, 4.85, 0.15)
coord_cylinder_side_4_middle_right = (-2.7, 6.85, 0.15)

coord_cube_side_4_highest_right = (-2.7, 4.85, 3.15)
coord_cylinder_side_4_highest_right = (-2.7, 6.85, 3.15)

# ---For the top

coord_cube_top_lower_left = (-3.5, 4, 5)
coord_cylinder_top_lower_left = (-3.5, 5, 5)

coord_cube_top_middle_left = (-0.5, 4, 5)
coord_cylinder_top_middle_left = (-0.5, 5, 5)

coord_cube_top_highest_left = (3, 4, 5)
coord_cylinder_top_highest_left = (3, 5, 5)

###

coord_cube_top_lower_right = (-4, -3.5, 5)
coord_cylinder_top_lower_right = (-4, -2, 5)

coord_cube_top_middle_right = (-1, -3.5, 5)
coord_cylinder_top_middle_right = (-1, -2, 5)

coord_cube_top_highest_right = (3, -3.5, 5)
coord_cylinder_top_highest_right = (3, -2, 5)


###################################################
#    Coordinates & radius of all connections      #
###################################################

# ---For the 1st side

coord_connection_side_1 = (4.85, random.uniform(0, 1.5), random.uniform(-2.5, 2.5))
radius_connection_out_side_1 = random.choice([1.7, 1.8, 1.9, 2])
radius_connection_in_side_1 = random.choice([1.2, 1.3, 1.4, 1.5])

# ---For the 2nd side

coord_connection_side_2 = (random.uniform(-0.5, 0.5), -4.85, random.uniform(-2.5, 2.5))
radius_connection_out_side_2 = random.choice([1.5, 1.6, 1.7, 1.8])
radius_connection_in_side_2 = random.choice([1, 1.1, 1.2, 1.3])

# ---For the 3rd side

coord_connection_side_3 = (-4.85, random.uniform(0, -1.5), random.uniform(-2.5, 2.5))
radius_connection_out_side_3 = random.choice([1.7, 1.8, 1.9, 2])
radius_connection_in_side_3 = random.choice([1.2, 1.3, 1.4, 1.5])

# ---For the 4th side

coord_connection_side_4 = (random.uniform(0, 1), 4.85, random.uniform(-2.5, 2.5))
radius_connection_out_side_4 = random.choice([1.7, 1.8, 1.9, 2])
radius_connection_in_side_4 = random.choice([1.2, 1.3, 1.4, 1.5])

# ---For the top

coord_connection_top = (random.uniform(-2.5, 2.5), random.uniform(0, 0.8), 5)
radius_connection_out_top = random.choice([1.8, 1.9, 2, 2.1])
radius_connection_in_top = random.choice([1.3, 1.4, 1.5, 1.6])


#####################################
###  Creating a cube SIDE 1 LEFT  ###
#####################################


# LOWER Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_lower_left)
cube = bpy.context.object
cube.name = "A1.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_lower_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


# MIDDLE Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_middle_left)
cube = bpy.context.object
cube.name = "A1.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_middle_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


# HIGHEST Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_highest_left)
cube = bpy.context.object
cube.name = "A1.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(2, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_highest_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


#####################################
###  Creating a cube SIDE 1 Right ###
#####################################


# LOWER Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_lower_right)
cube = bpy.context.object
cube.name = "A2.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_lower_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -3.14159  # -180 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# MIDDLE Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_middle_right)
cube = bpy.context.object
cube.name = "A2.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(2, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1,  # Radius of cylinder = half the width of the cube
    depth=0.35,  # Cylinder depth = cube thickness (3mm)
    location=(0, -1, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_middle_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -3.14159  # -180 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# HIGHEST Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_1_highest_right)
cube = bpy.context.object
cube.name = "A2.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_1_highest_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -3.14159  # -180 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


#####################################
###  Creating a cube SIDE 2 LEFT  ###
#####################################


# LOWER Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_lower_left)
cube = bpy.context.object
cube.name = "B1.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_lower_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


# MIDDLE Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_middle_left)
cube = bpy.context.object
cube.name = "B1.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_middle_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


# HIGHEST Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_highest_left)
cube = bpy.context.object
cube.name = "B1.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_highest_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


#####################################
###  Creating a cube SIDE 2 RIGHT ###
#####################################


# LOWER Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_lower_right)
cube = bpy.context.object
cube.name = "B2.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_lower_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# MIDDLE Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_middle_right)
cube = bpy.context.object
cube.name = "B2.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_middle_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# HIGHEST Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_2_highest_right)
cube = bpy.context.object
cube.name = "B2.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_2_highest_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


#####################################
###  Creating a cube SIDE 3 LEFT  ###
#####################################


# LOWER Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_lower_left)
cube = bpy.context.object
cube.name = "C1.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_lower_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -3.14159  # -180 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


# MIDDLE Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_middle_left)
cube = bpy.context.object
cube.name = "C1.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_middle_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


# HIGHEST Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_highest_left)
cube = bpy.context.object
cube.name = "C1.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_highest_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -3.14159  # -180 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


#####################################
###  Creating a cube SIDE 3 Right ###
#####################################


# LOWER Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_lower_right)
cube = bpy.context.object
cube.name = "C2.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_lower_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


# MIDDLE Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_middle_right)
cube = bpy.context.object
cube.name = "C2.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_middle_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


# HIGHEST Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_3_highest_right)
cube = bpy.context.object
cube.name = "C2.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_3_highest_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[1] = 1.5708  # 90 degrees


#####################################
###  Creating a cube SIDE 4 Left  ###
#####################################


# LOWER Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_lower_left)
cube = bpy.context.object
cube.name = "D1.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_lower_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# MIDDLE Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_middle_left)
cube = bpy.context.object
cube.name = "D1.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_middle_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


# HIGHEST Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_highest_left)
cube = bpy.context.object
cube.name = "D1.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_highest_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = -4.71239  # - 270 degrees
cube.rotation_euler[1] = 4.71239  # 270 degrees
cube.rotation_euler[2] = 0  # 0 degrees


#####################################
###  Creating a cube SIDE 4 Right ###
#####################################


# LOWER Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_lower_right)
cube = bpy.context.object
cube.name = "D2.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 4, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_lower_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


# MIDDLE Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_middle_right)
cube = bpy.context.object
cube.name = "D2.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(2, 4, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_middle_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


# HIGHEST Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_side_4_highest_right)
cube = bpy.context.object
cube.name = "D2.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 4, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_side_4_highest_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[0] = 1.5708  # 90 degrees
cube.rotation_euler[1] = 1.5708  # 90 degrees


#############################
###  Creating a TOP Left  ###
#############################


# LOWER Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_lower_left)
cube = bpy.context.object
cube.name = "E1.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_lower_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[2] = 3.14159  # 180 degrees


# MIDDLE Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_middle_left)
cube = bpy.context.object
cube.name = "E1.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(3, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1.5,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_middle_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[2] = 3.14159  # 180 degrees


# HIGHEST Left PART 1

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_highest_left)
cube = bpy.context.object
cube.name = "E1.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 2, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_highest_left  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)

# Rotate an object
cube.rotation_euler[2] = 3.14159  # 180 degrees


##############################
###  Creating a TOP Right  ###
##############################


# LOWER Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_lower_right)
cube = bpy.context.object
cube.name = "E2.1"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(2, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=1,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_lower_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)


# MIDDLE Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_middle_right)
cube = bpy.context.object
cube.name = "E2.2"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_middle_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "UNION"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)


# HIGHEST Right PART 2

bpy.ops.mesh.primitive_cube_add(size=1, location=coord_cube_top_highest_right)
cube = bpy.context.object
cube.name = "E2.3"

# Reducing the cube thickness to 3 mm
bpy.ops.object.mode_set(mode="EDIT")
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.transform.resize(value=(4, 3, 0.3))  # 3mm = 0.05 Blender units (height)
bpy.ops.object.mode_set(mode="OBJECT")

# Creating a Cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=2,  # Radius of cylinder = half the width of the cube
    depth=0.3,  # Cylinder depth = cube thickness (3mm)
    location=(0, 0, 0),
)
cylinder = bpy.context.object
cylinder.name = "Cylinder"

# Setting the cylinder position
cylinder.location = (
    coord_cylinder_top_highest_right  # Offset the cylinder to the side of the cube
)

# Applying the Boolean modifier
bpy.context.view_layer.objects.active = cube
bool_mod = cube.modifiers.new(name="Boolean", type="BOOLEAN")
bool_mod.operation = "DIFFERENCE"
bool_mod.object = cylinder

bpy.ops.object.modifier_apply(modifier=bool_mod.name)

# Removing a cylinder
bpy.data.objects.remove(cylinder, do_unlink=True)


######################################################
###              Creating Connections              ###
######################################################


# Connection for the 1st side

# Create an outer cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_out_side_1, depth=0.3, location=coord_connection_side_1
)
outer_cylinder = bpy.context.object
outer_cylinder.name = "OuterCylinder"

# Create an inner cylinder for the cutout
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_in_side_1, depth=0.3, location=coord_connection_side_1
)
inner_cylinder = bpy.context.object
inner_cylinder.name = "InnerCylinder"

# Boolean
mod_bool = outer_cylinder.modifiers.new(name="Boolean", type="BOOLEAN")
mod_bool.operation = "DIFFERENCE"
mod_bool.object = inner_cylinder

bpy.context.view_layer.objects.active = outer_cylinder
bpy.ops.object.modifier_apply(modifier="Boolean")
bpy.data.objects.remove(inner_cylinder, do_unlink=True)

outer_cylinder.rotation_euler[1] = 1.5708
print("Cylinder has been created!")

# Connection for the 2nd side

# Create an outer cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_out_side_2, depth=0.3, location=coord_connection_side_2
)
outer_cylinder = bpy.context.object
outer_cylinder.name = "OuterCylinder"

# Create an inner cylinder for the cutout
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_in_side_2, depth=0.3, location=coord_connection_side_2
)
inner_cylinder = bpy.context.object
inner_cylinder.name = "InnerCylinder"

# Boolean
mod_bool = outer_cylinder.modifiers.new(name="Boolean", type="BOOLEAN")
mod_bool.operation = "DIFFERENCE"
mod_bool.object = inner_cylinder

bpy.context.view_layer.objects.active = outer_cylinder
bpy.ops.object.modifier_apply(modifier="Boolean")
bpy.data.objects.remove(inner_cylinder, do_unlink=True)

outer_cylinder.rotation_euler[0] = 1.5708
outer_cylinder.rotation_euler[1] = 1.5708
print("Cylinder has been created!")


# Connection for the 3rd side

# Create an outer cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_out_side_3, depth=0.3, location=coord_connection_side_3
)
outer_cylinder = bpy.context.object
outer_cylinder.name = "OuterCylinder"

# Create an inner cylinder for the cutout
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_in_side_3, depth=0.3, location=coord_connection_side_3
)
inner_cylinder = bpy.context.object
inner_cylinder.name = "InnerCylinder"

# Boolean
mod_bool = outer_cylinder.modifiers.new(name="Boolean", type="BOOLEAN")
mod_bool.operation = "DIFFERENCE"
mod_bool.object = inner_cylinder

bpy.context.view_layer.objects.active = outer_cylinder
bpy.ops.object.modifier_apply(modifier="Boolean")
bpy.data.objects.remove(inner_cylinder, do_unlink=True)

outer_cylinder.rotation_euler[1] = 1.5708
print("Cylinder has been created!")

# Connection for the 4th side

# Create an outer cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_out_side_4, depth=0.3, location=coord_connection_side_4
)
outer_cylinder = bpy.context.object
outer_cylinder.name = "OuterCylinder"

# Create an inner cylinder for the cutout
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_in_side_4, depth=0.3, location=coord_connection_side_4
)
inner_cylinder = bpy.context.object
inner_cylinder.name = "InnerCylinder"

# Boolean
mod_bool = outer_cylinder.modifiers.new(name="Boolean", type="BOOLEAN")
mod_bool.operation = "DIFFERENCE"
mod_bool.object = inner_cylinder

bpy.context.view_layer.objects.active = outer_cylinder
bpy.ops.object.modifier_apply(modifier="Boolean")
bpy.data.objects.remove(inner_cylinder, do_unlink=True)

outer_cylinder.rotation_euler[0] = 1.5708
outer_cylinder.rotation_euler[1] = 1.5708
print("Cylinder has been created!")

# Connection for the top

# Create an outer cylinder
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_out_top, depth=0.3, location=coord_connection_top
)
outer_cylinder = bpy.context.object
outer_cylinder.name = "OuterCylinder"

# Create an inner cylinder for the cutout
bpy.ops.mesh.primitive_cylinder_add(
    radius=radius_connection_in_top, depth=0.3, location=coord_connection_top
)
inner_cylinder = bpy.context.object
inner_cylinder.name = "InnerCylinder"

# Boolean
mod_bool = outer_cylinder.modifiers.new(name="Boolean", type="BOOLEAN")
mod_bool.operation = "DIFFERENCE"
mod_bool.object = inner_cylinder

bpy.context.view_layer.objects.active = outer_cylinder
bpy.ops.object.modifier_apply(modifier="Boolean")
bpy.data.objects.remove(inner_cylinder, do_unlink=True)

outer_cylinder.rotation_euler[2] = 3.14159
print("Cylinder has been created!")


################################################
###              Creating Holes              ###
################################################

hole_locations = {}

# Select the objects by name
for name in [
    "A1.2",
    "A2.1",
    "A2.3",
    "B1.1",
    "B1.3",
    "B2.2",
    "C1.2",
    "C2.1",
    "C2.3",
    "D1.1",
    "D1.3",
    "D2.2",
    "E1.1",
    "E1.3",
    "E2.2",
]:
    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects[name].select_set(True)
    obj = bpy.context.selected_objects[0]

    # Generate a random radius for the hole
    random_radius = random.uniform(0.7, 1)
    print(f"Creating hole with radius {random_radius} in object '{obj.name}'")

    # Create the cylinder for the hole (with random radius)
    bpy.ops.mesh.primitive_cylinder_add(
        radius=random_radius,  # Random radius for the hole
        depth=1,  # Hole thickness (3 mm)
        location=obj.location,  # Cylinder positioned at the object's location
    )

    hole_locations[name] = bpy.context.object.location

    if "A" in name or "C" in name:
        bpy.ops.transform.rotate(value=math.pi / 2, orient_axis="Y")
        if "A1.2" in name or "C2.3" in name or "C2.1" in name:
            bpy.ops.transform.translate(value=(0, random.uniform(0.5, 1.5), 0))
        if "A2.3" in name or "A2.1" in name or "C1.2" in name:
            bpy.ops.transform.translate(value=(0, random.uniform(-0.1, -1.5), 0))

    elif "B" in name or "D" in name:
        bpy.ops.transform.rotate(value=math.pi / 2, orient_axis="X")
        if "B1.1" in name or "B1.3" in name:
            bpy.ops.transform.translate(value=(random.uniform(0.5, 1.5), 0, 0))
        if "B2.2" in name or "D1.1" in name or "D1.3" in name:
            bpy.ops.transform.translate(value=(random.uniform(-0.5, -1), 0, 0))
        if "D2.2" in name:
            bpy.ops.transform.translate(value=(random.uniform(-1, 1), 0, 0))

    elif "E" in name:
        bpy.ops.transform.rotate(value=math.pi, orient_axis="Z")
        if "E1.1" in name or "E1.3" in name:
            bpy.ops.transform.translate(value=(0, random.uniform(-0.5, -1), 0))
        if "E2.2" in name:
            bpy.ops.transform.translate(value=(0, random.uniform(0.5, 1.5), 0))

    hole_cylinder = bpy.context.object
    hole_cylinder.name = "HoleCylinder"

    # Apply the Boolean modifier to subtract the hole from the object
    bpy.context.view_layer.objects.active = obj
    bool_mod = obj.modifiers.new(name="Boolean", type="BOOLEAN")
    bool_mod.operation = "DIFFERENCE"  # Subtract the cylinder from the object
    bool_mod.object = hole_cylinder

    bpy.ops.object.modifier_apply(modifier=bool_mod.name)

    # Remove the cylinder after applying the modifier
    # bpy.data.objects.remove(hole_cylinder, do_unlink=True)


################################################
###             Creating Spheres             ###
################################################


delete_me = []
sphere_positions = []
for object in bpy.data.objects:
    if "HoleCylinder" in object.name:
        sphere_positions.append(object.location)
        delete_me.append(object)
    elif "OuterCylinder" in object.name:
        sphere_positions.append(object.location)

for cylinder in delete_me:
    bpy.data.objects.remove(cylinder, do_unlink=True)

print("Holes created in the selected objects.")
bpy.data.objects["OuterCylinder.003"]


for coord in sphere_positions:
    if random.choice([True, False, False]):
        bpy.ops.mesh.primitive_uv_sphere_add(
            radius=random.uniform(0.4, 0.6),  # Random radius for variety
            segments=32,  # Number of segments
            ring_count=16,  # Number of rings
            location=coord,  # Location from the list\
        )


bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["Cube"].select_set(False)
bpy.ops.object.join()
