# SPDX-FileCopyrightText: 2016 Antoine Moulin
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.data.objects["Chapeau"].select = False
bpy.data.objects["Capsule"].select = False
bpy.data.objects["Capsules1"].select = False
bpy.data.objects["Capsules2"].select = False
bpy.data.objects["Capsules3"].select = False
bpy.data.objects["socle"].select = False
bpy.data.objects["Camera"].select = False
bpy.ops.object.delete()


nbr_Clusters = randint(2, 5)


def CopyPaste(name, newname):

    bpy.context.scene.objects.active = bpy.data.objects[name]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(newname, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy

    ###################
    ### DEFINITIONS ###
    ###################


def noyau(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension

    location = (x + sx, y + sy, z + sz)
    bpy.ops.mesh.primitive_cube_add(radius=1, location=location)
    bpy.ops.transform.resize(value=dimension)


def fluide(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension

    location = (x + sx, y + sy, z + sz)
    bpy.ops.mesh.primitive_cube_add(radius=1, location=location)
    bpy.ops.transform.resize(value=dimension)


def socle(position):
    x, y, z = position
    socle = CopyPaste("socle", "socle")
    socle.location = [x, y, z]


def Colonnes(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension

    location = (x + sx, y + sy, z + sz)
    bpy.ops.mesh.primitive_cube_add(radius=1, location=location)
    bpy.ops.transform.resize(value=dimension)


def noyau_socle(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension

    location = (x + sx, y + sy, z + sz)
    bpy.ops.mesh.primitive_cube_add(radius=1, location=location)
    bpy.ops.transform.resize(value=dimension)


def Capsule(position, rotate):
    x, y, z = position
    Capsule = CopyPaste("Capsule", "Capsule")
    Capsule.location = [x, y, z]
    Capsule.rotation_euler = [radians(0), radians(0), radians(rotate)]


def Capsules1(position, rotate):
    x, y, z = position
    Capsules1 = CopyPaste("Capsules1", "Capsules1")
    Capsules1.location = [x, y, z]
    Capsules1.rotation_euler = [radians(0), radians(0), radians(rotate)]


def Capsules2(position, rotate):
    x, y, z = position
    Capsules2 = CopyPaste("Capsules2", "Capsules2")
    Capsules2.location = [x, y, z]
    Capsules2.rotation_euler = [radians(0), radians(0), radians(rotate)]


def Capsules3(position, rotate):
    x, y, z = position
    Capsules3 = CopyPaste("Capsules3", "Capsules3")
    Capsules3.location = [x, y, z]
    Capsules3.rotation_euler = [radians(0), radians(0), radians(rotate)]


ListeCaps = []

ListeCaps.append(Capsules1)
ListeCaps.append(Capsules2)
ListeCaps.append(Capsules3)


def Capsules(position, rotate):
    x, y, z = position
    Capsules = choice(ListeCaps)
    Capsules(position, rotate)


def Cluster(position):
    etages = randint(12, 25)
    tours = [1] * 10 + [2] * 8 + [3] * 2 + [4] * 1
    toursX = choice(tours)
    toursY = choice(tours)
    pont = [3, 6, 9, 12, 15, 18, 21, 24]
    Socle = choice([0, 1.6])
    x, y, z = position

    #############
    ### AXE X ###
    #############

    for X in range(0, toursX):

        for Z in range(0, etages - (2 * X)):

            ##### TOUR #####

            noyau((x + X * 2.28, y, z + Z * 0.56 + Socle), (0.57, 0.57, 0.28))

            # fluide ((-0.4+x*2.28,0.57,(z+2)*0.5),(0.2,0.02,0.25))
            # fluide ((1.14+x*2.28,0.57,(z+2)*0.5),(0.2,0.02,0.25))
            # fluide ((0.57+x*2.28,-0.4,(z+2)*0.5),(0.02,0.2,0.25))
            # fluide ((0.57+x*2.28,1.14,(z+2)*0.5),(0.02,0.2,0.25))

            chapeau = CopyPaste("Chapeau", "Chapeau")
            chapeau.location = [
                x + X * 2.28 + 0.57,
                y + 0.57,
                z + (etages - (2 * X) + Socle) * 0.56,
            ]

            ##### CAPSULES #####

            ## capsules angle 0 ##
            if X == 0:
                Capsules(
                    (x + X * 2.28 - 0.02, y + 1.15, z + (Z + 0.33) * 0.54 + Socle), 0
                )

            ## capsules angle bout ##
            if X == toursX - 1:
                Capsules(
                    (x + X * 2.28 + 1.15, y + 1.17, z + (Z + X * 0.33) * 0.54 + Socle),
                    -90,
                )
                if toursX - 1 == 0:
                    pass
                else:
                    Capsules(
                        (
                            x + X * 2.28 + 1.17,
                            y,
                            z + (Z + (X + 1) * 0.33) * 0.54 + Socle,
                        ),
                        180,
                    )

            ## capsules entre-tours ##
            else:
                Capsules2(
                    (x + X * 2.28 + 1.16, y + 1.16, z + (Z + X * 0.33) * 0.54 + Socle),
                    -90,
                )
                Capsules3(
                    (x + X * 2.28 + 2.26, y + 1.17, z + (Z + X * 0.33) * 0.54 + Socle),
                    0,
                )
                if (
                    Z == 2
                    or Z == 5
                    or Z == 8
                    or Z == 11
                    or Z == 14
                    or Z == 17
                    or Z == 20
                    or Z == 23
                ):
                    Capsule(
                        (x + X * 2.28 + 2.83, y, z + (Z + X * 0.33) * 0.54 + Socle), 90
                    )
                    if X == 0 and toursY - 1 == 0:
                        Capsule(
                            (x + X * 2.28 + 1.12, y, z + (Z + X * 0.33) * 0.54 + Socle),
                            90,
                        )
                    if X == 0:
                        pass
                    else:
                        Capsule(
                            (x + X * 2.28 + 1.12, y, z + (Z + X * 0.33) * 0.54 + Socle),
                            90,
                        )
                else:
                    Capsules2(
                        (x + X * 2.28 + 2.26, y, z + (Z + X * 0.33) * 0.54 + Socle), 90
                    )
                    if X == 0 and toursY - 1 == 0:
                        Capsules3(
                            (x + X * 2.28 + 1.16, y, z + (Z + X * 0.33) * 0.54 + Socle),
                            180,
                        )
                    if X == 0:
                        pass
                    else:
                        Capsules3(
                            (x + X * 2.28 + 1.16, y, z + (Z + X * 0.33) * 0.54 + Socle),
                            180,
                        )

        ##### SOCLE #####

        if Socle == 1.6:
            socle((x + 0.54 + X * 2.28, y + 0.54, z + 1.15))

            noyau_socle((x + X * 2.28, y, z), (0.57, 0.57, 0.5))

            Colonnes((x - 0.7, y + 1.6, z), (0.1, 0.1, 0.5))
            Colonnes((x + (X + 1) * 2.28 - 0.7, y + 1.6, z), (0.1, 0.1, 0.5))
            Colonnes((x + X * 2.28 + 0.45, y + 1.6, z), (0.1, 0.1, 0.5))
            Colonnes((x + (X + 1) * 2.28 - 0.7, y - 0.7, z), (0.1, 0.1, 0.5))
            Colonnes((x + X * 2.28 + 0.45, y - 0.7, z), (0.1, 0.1, 0.5))

    #############
    ### AXE Y ###
    #############

    for Y in range(0, toursY):

        for Z in range(0, etages - (2 * Y)):

            ##### TOUR #####

            noyau((x, y - Y * 2.28, z + Z * 0.56 + Socle), (0.57, 0.57, 0.28))

            # fluide ((0.57,-0.4-y*2.28,(z+2)*0.5),(0.02,0.2,0.25))
            # fluide ((0.57,1.14-y*2.28,(z+2)*0.5),(0.02,0.2,0.25))
            # fluide ((-0.4,0.57-y*2.28,(z+2)*0.5),(0.2,0.02,0.25))
            # fluide ((1.14,0.57-y*2.28,(z+2)*0.5),(0.2,0.02,0.25))

            chapeau = CopyPaste("Chapeau", "Chapeau")
            chapeau.location = [
                x + 0.57,
                y - Y * 2.28 + 0.57,
                z + (etages - (2 * Y) + Socle) * 0.56,
            ]

            ##### CAPSULES #####

            ## capsules angle 0 ##
            if Y == 0:
                pass

            ## capsules angle bout ##
            if Y == toursY - 1:
                Capsules(
                    (x, y - Y * 2.28 - 0.02, z + (Z + Y * 0.33) * 0.54 + Socle), 90
                )
                if toursY - 1 == 0:
                    pass
                else:
                    Capsules(
                        (
                            x + 1.17,
                            y - Y * 2.28 - 0.02,
                            z + (Z + (Y + 1) * 0.33) * 0.54 + Socle,
                        ),
                        180,
                    )

            ## capsules entre-tours ##
            else:
                Capsules2(
                    (x, y - Y * 2.28 - 1.12, z + (Z + Y * 0.33) * 0.54 + Socle), 0
                )
                Capsules3(
                    (x - 0.02, y - Y * 2.28 - 0.02, z + (Z + Y * 0.33) * 0.54 + Socle),
                    90,
                )
                if (
                    Z == 2
                    or Z == 5
                    or Z == 8
                    or Z == 11
                    or Z == 14
                    or Z == 17
                    or Z == 20
                    or Z == 23
                ):
                    Capsule(
                        (
                            x + 1.17,
                            y - Y * 2.28 - 1.15,
                            z + (Z + Y * 0.33) * 0.54 + Socle,
                        ),
                        180,
                    )
                    if Y == 0 and toursX - 1 == 0:
                        Capsule(
                            (
                                x + 1.17,
                                y - Y * 2.28 + 0.56,
                                z + (Z + Y * 0.33) * 0.54 + Socle,
                            ),
                            180,
                        )
                    if Y == 0:
                        pass
                    else:
                        Capsule(
                            (
                                x + 1.17,
                                y - Y * 2.28 + 0.56,
                                z + (Z + Y * 0.33) * 0.54 + Socle,
                            ),
                            180,
                        )
                else:
                    Capsules3(
                        (
                            x + 1.17,
                            y - Y * 2.28 - 1.12,
                            z + (Z + Y * 0.33) * 0.54 + Socle,
                        ),
                        -90,
                    )
                    if Y == 0 and toursX - 1 == 0:
                        Capsules2(
                            (
                                x + 1.15,
                                y - Y * 2.28 - 0.02,
                                z + (Z + Y * 0.33) * 0.54 + Socle,
                            ),
                            180,
                        )
                    if Y == 0:
                        pass
                    else:
                        Capsules2(
                            (
                                x + 1.15,
                                y - Y * 2.28 - 0.02,
                                z + (Z + Y * 0.33) * 0.54 + Socle,
                            ),
                            180,
                        )

            ## capsules angle interieur ##
            if toursY - 1 == 0 and toursX - 1 == 0:
                Capsules(
                    (x + 1.16, y - 0.01, z + (Z + (Y + 1) * 0.33) * 0.54 + Socle), 180
                )

        ##### SOCLE #####

        if Socle == 1.6:
            socle((x + 0.54, y + 0.54 - Y * 2.28, z + 1.15))

            noyau_socle((x, y - Y * 2.28, z), (0.57, 0.57, 0.5))

            Colonnes((x - 0.7, y - (Y + 1) * 2.28 + 1.6, z), (0.1, 0.1, 0.5))
            Colonnes((x - 0.7, y - (Y + 1) * 2.28 + 2.75, z), (0.1, 0.1, 0.5))
            Colonnes((x + 1.6, y - (Y + 1) * 2.28 + 1.6, z), (0.1, 0.1, 0.5))
            Colonnes((x + 1.6, y - (Y + 1) * 2.28 + 2.75, z), (0.1, 0.1, 0.5))

    ###############
    ### CLUSTER ###
    ###############


# Cluster((0,0,0))

"""for i in range (0,nbr_Clusters):
    PositionX = randint (-15,15)
    PositionY = randint (-15,15)
    Cluster ((PositionX,PositionY,0))"""

for i in range(0, nbr_Clusters):
    PositionX = randint(-15, 15)
    Cluster((PositionX, i * 7 - 15, 0))
