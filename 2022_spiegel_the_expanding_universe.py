# SPDX-FileCopyrightText: 2022 Araprima Aji & Francesco Piloni
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# PYTHON BASICS - 16/01/2023
# Script written by Araprima AJI & Francesco PILLONI under blender 3.3.0 hash 0759f671celf

import bpy
import random
import math

##################
#    CLEANING    #
##################

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


#################################
#   CUBE BASE / Cylinder base   #
#################################
for i in range(1, 28):

    # coordinates
    Stacking = (0, 0, i * -3 + 37)
    Resize = (4, 4, i * 0.2)

    # Primitve Cylinders
    bpy.ops.mesh.primitive_cylinder_add(
        vertices=4, radius=15, depth=0.5, location=Stacking, scale=Resize
    )

    # Solidify the Cylinders
    bpy.ops.object.modifier_add(type="SOLIDIFY")

    # Thickness parameter with solidifier
    bpy.context.object.modifiers["Solidify"].thickness = -0.5

# Select Cylinders to join them

# Select All
bpy.ops.object.select_all(action="SELECT")

# Join Cylinders
bpy.ops.object.join()

##################################################
#   Balls : To create the Void Inside The Cube   #
##################################################

for i in range(1, 30):
    posX = random.uniform(-30, 15)
    posY = random.uniform(-30, 30)
    posZ = random.uniform(-35, 30)
    size = random.uniform(1, 30)
    bpy.ops.mesh.primitive_uv_sphere_add(
        segments=35,
        ring_count=35,
        radius=size,
        location=(posX, posY, posZ),
        scale=(1, 1, 1),
    )

################
#  Join Balls  #
################

# Select Cylinder

# Select All
bpy.ops.object.select_all(action="SELECT")

# Deselect Sphere
bpy.data.objects["Cylinder.026"].select_set(False)

# Cylinder active objects
bpy.context.view_layer.objects.active = bpy.data.objects["Sphere"]

# Join Balls
bpy.ops.object.join()

####################################
#   Boolean :  Sphere - Cylinders  #
####################################

# Select Cylinder

# Select All
bpy.ops.object.select_all(action="SELECT")

# Deselect Sphere
bpy.data.objects["Sphere"].select_set(False)

# Cylinder active objects
bpy.context.view_layer.objects.active = bpy.data.objects["Cylinder.026"]

# Boolean action with to differentiate the Spheres with the Cylinders
bpy.ops.object.modifier_add(type="BOOLEAN")
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Sphere"]
bpy.context.object.modifiers["Boolean"].use_self = True
bpy.ops.object.modifier_apply(modifier="Boolean")


###################################################
#   Sphere : To create the Void Inside The Cube   #
###################################################

for i in range(1, 30):

    # coordinates
    posX = random.uniform(-30, 30)
    posY = random.uniform(-30, 30)
    posZ = random.uniform(-35, 30)

    # Size parameters for the Spheres
    size = random.uniform(0.5, 6)

    # Primitive Spheres
    bpy.ops.mesh.primitive_uv_sphere_add(
        segments=35,
        ring_count=35,
        radius=size,
        location=(posX, posY, posZ),
        scale=(1, 1, 1),
    )


##############################################################
#   Cylinders : Sticks for crescendo and decrescendo lines   #
##############################################################

for i in range(1, 20):

    # coordinates
    posX1 = random.uniform(-20, 20)
    posY1 = random.uniform(-20, 20)
    posZ1 = random.uniform(-20, 10)
    Angle = i * math.radians(10)

    # Primitive Cylinders
    bpy.ops.mesh.primitive_cylinder_add(
        radius=0.7, depth=100, location=(posX1, posY1, posZ1), scale=(1, 1, 1)
    )

    # Rotation on the x and Y axis
    bpy.context.object.rotation_euler[0] = i * math.radians(15)

    # Rotation on the Z axis
    bpy.ops.transform.rotate(value=Angle, orient_axis="Z")

######################################
#   Cylinders : Structural columns   #
######################################

for i in range(1, 30):

    # coordinates
    posX2 = random.uniform(-50, 50)
    posY2 = random.uniform(-50, 50)
    posZ2 = random.uniform(-20, 10)

    # Primitive Cylinders
    bpy.ops.mesh.primitive_cylinder_add(
        radius=1, depth=100, location=(posX2, posY2, posZ2), scale=(1, 1, 1)
    )

    # Rotation angle for vertical columns
    bpy.context.object.rotation_euler[0] = i * math.radians(180)
