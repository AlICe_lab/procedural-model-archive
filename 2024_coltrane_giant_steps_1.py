# SPDX-FileCopyrightText: 2025 Metin Özden
# SPDX-FileCopyrightText: 2025 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER4.3/Ozden.Metin.14/01/25


import bpy
import random
import math


def clean():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)
    bpy.ops.outliner.orphans_purge()


clean()


# pain du bas

# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 0))
bpy.context.object.name = "portee.1"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.1"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
b.z = b.z + 0.5
d.z = d.z + 2.5
g.z = g.z + 0.5
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
a.z = a.z + 1
d.z = d.z + 2.5


# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 0.5))
bpy.context.object.name = "portee.2"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.2"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
g.z = g.z + 0.5
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
gb.z = gb.z + 2.5
b.z = b.z + 0.5
f.z = f.z + 1
BbEb.z = BbEb.z + 2.5


# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 1))
bpy.context.object.name = "portee.3"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.3"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
eb.z = eb.z + 0.5
a.z = a.z + 1
d.z = d.z + 2.5
g.z = g.z + 0.5
db.z = db.z + 1
gb.z = gb.z + 2.5

# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 1.5))
bpy.context.object.name = "portee.4"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.4"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
b.z = b.z + 0.5
f.z = f.z + 1
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
db.z = db.z + 1
gb.z = gb.z + 2.5


# pain du haut

# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 6))
bpy.context.object.name = "portee.5"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.5"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
b.z = b.z + 0.5
d.z = d.z + 2.5
g.z = g.z + 0.5
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
a.z = a.z + 1
d.z = d.z + 2.5


# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 6.5))
bpy.context.object.name = "portee.6"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.6"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
g.z = g.z + 0.5
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
gb.z = gb.z + 2.5
b.z = b.z + 0.5
f.z = f.z + 1
BbEb.z = BbEb.z + 2.5


# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 7))
bpy.context.object.name = "portee.7"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.7"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
eb.z = eb.z + 0.5
a.z = a.z + 1
d.z = d.z + 2.5
g.z = g.z + 0.5
db.z = db.z + 1
gb.z = gb.z + 2.5

# generer un plan
bpy.ops.mesh.primitive_plane_add(location=(0, 0, 7.5))
bpy.context.object.name = "portee.8"

# entre dans editmode pour subdiviser et redimensionner
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.subdivide(number_cuts=2)
bpy.ops.transform.resize(value=(5, 5, 5))
# sort edit mode
bpy.ops.object.editmode_toggle()

obj = bpy.data.objects["portee.8"]
me = obj.data

# identifier les vertex et le nommer selon accord
c = me.vertices[0].co
g = me.vertices[5].co
d = me.vertices[4].co
gb = me.vertices[3].co
a = me.vertices[2].co
eb = me.vertices[1].co
f = me.vertices[6].co
bb = me.vertices[7].co
ab = me.vertices[8].co
db = me.vertices[9].co
b = me.vertices[10].co
e = me.vertices[11].co
DE = me.vertices[12].co
BDb = me.vertices[13].co
FG = me.vertices[14].co
BbEb = me.vertices[15].co


# un petit outil pour repere les vertex par leur index
# me.vertices[5].co.z = 5

# deplacer le vertex en z
b.z = b.z + 0.5
f.z = f.z + 1
bb.z = bb.z + 2.5
eb.z = eb.z + 0.5
db.z = db.z + 1
gb.z = gb.z + 2.5


# ajouter des modifier

for obj in bpy.data.objects:
    bpy.ops.object.select_all(action="DESELECT")
    if "portee" in obj.name:
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.modifier_add(type="BEVEL")
        bpy.context.object.modifiers["Bevel"].width = 1
        bpy.context.object.modifiers["Bevel"].segments = 6

        mod = bpy.context.selected_objects[0].modifiers.new(
            name="solidify", type="SOLIDIFY"
        )
        mod.thickness = 0.2 + random.random() * 0.3

# impro

# mise en place des steps

listPos = []
hauteur = 4
ratio = 1.4285714286
valeurEscalier = 0.2

# step horizontaux
for i in range(0, 7):
    for j in range(0, 7):
        hauteur = hauteur + random.uniform(-valeurEscalier, valeurEscalier)
        listPos.append((i * ratio - 3 * ratio, j * ratio - 3 * ratio, hauteur))

# décalage 1

print(listPos)

for pos in listPos:
    decalage = random.random() * 0.7 * random.choice([-1, 1])
    bpy.ops.mesh.primitive_cube_add(size=1, location=pos)
    # taille des cubes impro (x,y,z)
    bpy.ops.transform.resize(value=(ratio, ratio, 0.1))
    # bpy.ops.transform.resize(value=(ratio, ratio, valeurEscalier*10+random.uniform(-.5,.5)))"""

    bpy.ops.mesh.primitive_cube_add(
        size=1, location=(pos[0] + decalage, pos[1] + decalage, pos[2] + decalage)
    )
    # taille des cubes impro (x,y,z)
    bpy.ops.transform.resize(value=(ratio, ratio, 0.4))

# décalage2

print(listPos)

for pos in listPos:
    decalage = random.random() * 0.7 * random.choice([-1, 1])
    bpy.ops.mesh.primitive_cube_add(size=1, location=pos)
    # taille des cubes impro (x,y,z)
    bpy.ops.transform.resize(value=(ratio, ratio, 0.7))
    # bpy.ops.transform.resize(value=(ratio, ratio, valeurEscalier*10+random.uniform(-.5,.5)))"""

    bpy.ops.mesh.primitive_cube_add(
        size=1, location=(pos[0] + decalage, pos[1] + decalage, pos[2] + decalage)
    )
    # taille des cubes impro (x,y,z)
    bpy.ops.transform.resize(value=(ratio, ratio, 1.2))


# creation des baguettes représentnat la batterie


side = int(math.sqrt(len(listPos)))

for n in range(side):
    bpy.ops.mesh.primitive_cylinder_add(
        radius=0.1,
        location=(listPos[n + 3 * side][0], listPos[n + 3 * side][1], 5),
        scale=(1, 1, random.randint(8, 9) / 2),
    )
    bpy.ops.mesh.primitive_cylinder_add(
        radius=0.1,
        location=(listPos[3 + n * side][0], listPos[3 + n * side][1], 5),
        scale=(1, 1, random.randint(8, 9) / 2),
    )

# Création des cubes et des connexions
prev_pos = None  # Pour stocker la position précédente

for pos in listPos:
    # Ajouter un cube
    bpy.ops.mesh.primitive_cube_add(size=1, location=pos)
    bpy.ops.transform.resize(value=(ratio, ratio, 0.5))

    # Si une position précédente existe, ajouter une connexion
    if prev_pos:
        # Calculer le milieu entre les deux cubes
        mid_pos = (
            (prev_pos[0] + pos[0]) / 2,
            (prev_pos[1] + pos[1]) / 2,
            (prev_pos[2] + pos[2]) / 2,
        )

        # Calculer la distance entre les deux cubes
        dist_x = abs(prev_pos[0] - pos[0])
        dist_y = abs(prev_pos[1] - pos[1])
        dist_z = abs(prev_pos[2] - pos[2])

        # Ajouter un cylindre pour relier les cubes
        bpy.ops.mesh.primitive_cylinder_add(
            radius=0.1,
            depth=(dist_x**2 + dist_y**2 + dist_z**2) ** 0.5,
            location=mid_pos,
        )

        # Ajuster la rotation du cylindre pour qu'il suive l'alignement
        bpy.context.object.rotation_euler[0] = random.uniform(
            0, 0.1
        )  # Exemple de rotation

    # Mise à jour de la position précédente
    prev_pos = pos


# essai de rajout horizontaux
