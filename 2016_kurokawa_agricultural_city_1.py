# SPDX-FileCopyrightText: 2016 Hugo Cormerais
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *


print("__________________")
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()


# DEFINITION GEOMETRIES #####################################


def micro_cel(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension
    position = (x + sx / 2, y + sy / 2, z + sz / 2)
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def circu(position, dimension):
    x, y, z = position
    sx, sy, sz = dimension
    position = (x + sx / 2, y + sy / 2, z + sz / 2)
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)


def infrastructure(position):
    x, y, z = position
    circu((-0.8 + x, -0.4 + y, -0.07 + z), (5.6, 0.4, 0.13))
    circu((-0.8 + x, 4 + y, -0.07 + z), (5.6, 0.4, 0.13))
    circu((-0.4 + x, -0.8 + y, -0.07 + z), (0.4, 5.6, 0.13))
    circu((4 + x, -0.8 + y, -0.07 + z), (0.4, 5.6, 0.13))


# 1 DEFINITION CELLULES [100.100] #####################################

l_population = []


def cel(position):
    x, y, z = position
    l_coord = []
    l_maison = []
    infrastructure((0 + x, 0 + y, 0))

    """#Trame micro cellules 4x4
    for ix in range (0,4):
        for iy in range (0,4):
            l_coord.append ((ix+x,iy+y,0))

    #Suppression aléatoire micro cellules
    for i in range (0,randint(1,16)):
        position1 = choice (l_coord)
        l_coord.remove (position1)

    for coord_cel in l_coord:
        micro_cel (coord_cel,(1,1,0.06))

    #Importation maisons
    position2 = sample (l_coord,randint (0,len(l_coord)))
    l_maison.append (position2)
    for i in position2:
        micro_cel (i,(0.2,0.2,0.2))
        l_population.append (i)"""

    # Importation circulation secondaire
    l_circu = []
    for ix in range(1, randint(1, 4)):
        if len(l_coord) > 0:
            circu((ix + x, 0 + y, 0), (0.1, 4, 0.06))
            l_circu.append(circu)
    for iy in range(1, randint(1, 4)):
        if len(l_coord) > 0:
            circu((0 + x, iy + y, 0), (4, 0.1, 0.06))
            l_circu.append(circu)


# 2 DEFINITION GROUPE CELLULES AUTONOME - BASE [500.500] #####################################

l_cel_globale = []


def groupement_cel(position_group):
    x, y, z = position_group
    l_coord2 = []

    # Trame cellules 5x5
    for ix in range(0, 5):
        for iy in range(0, 5):
            l_coord2.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
            l_cel_globale.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))

    # Suppression aléatoire cellules
    for i in range(0, randint(1, 3)):
        position2 = choice(l_coord2)
        l_coord2.remove(position2)

    for coord_group in l_coord2:
        cel(coord_group)


# 3 GROUPE CELLULES AUTONOME - MODIFIER POINT D'ANCRAGE ####################

# Inferieur gauche


def groupement_cel_0(position_group0):
    x, y, z = position_group0
    l_coord2 = []

    for ix in range(0, 5):
        for iy in range(0, 5):
            l_coord2.append((ix * 4.4 + x, iy * 4.4 + y, 0))
            l_cel_globale.append((ix * 4.4 + x, iy * 4.4 + y, 0))

    # supression aleatoire cellules (densité forte)
    for i in range(0, randint(1, 3)):
        position2 = choice(l_coord2)
        l_coord2.remove(position2)

    for coord_group in l_coord2:
        cel(coord_group)

    # croissance bras nord
    ix = randint(0, 4)
    for iy in range(5, randint(7, 9)):
        l_nord.append((ix * 4.4 + x, iy * 4.4 + y, 0))
        l_cel_globale.append((ix * 4.4 + x, iy * 4.4 + y, 0))
    print("nord----->", len(l_nord), l_nord)
    for point in l_nord:
        cel(point)

    # croissance bras est
    iy = randint(1, 5)
    for ix in range(5, randint(7, 9)):
        l_est.append((ix * 4.4 + x, iy * 4.4 + y, 0))
    print("est----->", len(l_est), l_est)
    for point in l_est:
        cel(point)

    # croissance ouest
    iy = randint(0, 4)
    for ix in range(1, randint(1, 4)):
        l_ouest.append((-4.4 * ix + x, iy * 4.4 + y, 0))
    print("ouest----->", len(l_ouest), l_ouest)
    for point in l_ouest:
        cel(point)


###########################################################

# Superieur gauche


def groupement_cel_1(position_group):
    x, y, z = position_group
    l_coord2 = []
    for ix in range(0, 5):
        for iy in range(1, 6):
            l_coord2.append((ix * 4.4 + x, iy * -4.4 + y, 0))
            l_cel_globale.append((ix * 4.4 + x, iy * -4.4 + y, 0))

    # supression aleatoire cellules (densité forte)
    for i in range(0, randint(1, 3)):
        position2 = choice(l_coord2)
        l_coord2.remove(position2)

    for coord_group in l_coord2:
        cel(coord_group)
    # croissance bras est
    iy = randint(1, 5)
    for ix in range(5, randint(7, 9)):
        l_est_1.append((ix * 4.4 + x, iy * -4.4 + y, 0))
    print("est----->", len(l_est), l_est)
    for point in l_est_1:
        cel(point)

    # croissance bras nord
    ix = randint(0, 4)
    for iy in range(5, randint(7, 9)):
        l_nord_1.append((ix * 4.4 + x, iy * -4.4 + y, 0))
        l_cel_globale.append((ix * 4.4 + x, iy * -4.4 + y, 0))
    print("nord_1----->", len(l_nord_1), l_nord_1)
    for point in l_nord_1:
        cel(point)


###########################################################

# Superieur droit


def groupement_cel_2(position_group):
    x, y, z = position_group
    l_coord2 = []
    l_coord3 = []
    for ix in range(1, 6):
        for iy in range(1, 6):
            l_coord2.append((ix * -4.4 + x, iy * -4.4 + y, 0))
            l_coord3.append((ix * 4.4 + x, iy * 4.4 + y, 0))

    # supression aleatoire cellules (densité forte)
    for i in range(0, randint(1, 3)):
        position2 = choice(l_coord2)
        l_coord2.remove(position2)

    for coord_group in l_coord2:
        cel(coord_group)


###########################################################

# Inferieur droit


def groupement_cel_3(position_group):
    x, y, z = position_group
    l_coord2 = []
    l_coord3 = []
    for ix in range(1, 6):
        for iy in range(0, 5):
            l_coord2.append((ix * -4.4 + x, iy * 4.4 + y, 0))
            l_coord3.append((ix * 4.4 + x, iy * 4.4 + y, 0))

    # supression aleatoire cellules (densité forte)
    for i in range(0, randint(1, 3)):
        position2 = choice(l_coord2)
        l_coord2.remove(position2)

    for coord_group in l_coord2:
        cel(coord_group)

    # croissance ouest
    iy = randint(0, 4)
    for ix in range(5, randint(7, 9)):
        l_ouest_3.append((-4.4 * ix + x, iy * 4.4 + y, 0))
    print("ouest_3----->", len(l_ouest_3), l_ouest_3)
    for point in l_ouest_3:
        cel(point)

    # croissance bras nord
    ix = randint(0, 4)
    for iy in range(1, randint(1, 4)):
        l_nord_3.append((ix * -4.4 + x, iy * -4.4 + y, 0))
        l_cel_globale.append((ix * -4.4 + x, iy * -4.4 + y, 0))
    print("nord_3----->", len(l_nord_3), l_nord_3)
    for point in l_nord_3:
        cel(point)


# 4 DEFINITION CROISSANCE BRAS- 4 DIRECTIONS###########################

l_nord = []
l_est = []
l_sud = []
l_ouest = []

l_nord_1 = []
l_est_1 = []
l_ouest_1 = []

l_nord_3 = []
l_est_3 = []
l_ouest_3 = []


def croissance_nord(position_croissance_nord):
    x, y, z = position_croissance_nord

    # Extension nord

    ix = randint(0, 4)
    for iy in range(5, randint(7, 9)):
        l_nord.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
        l_cel_globale.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
    print("nord----->", len(l_nord), l_nord)
    for point in l_nord:
        cel(point)

    # Extension est


def croissance_est(position_croissance_est):
    x, y, z = position_croissance_est

    iy = randint(0, 4)
    for ix in range(5, randint(7, 9)):
        l_est.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
        l_cel_globale.append((ix * 4.4 + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
    print("est----->", len(l_est), l_est)
    for point in l_est:
        cel(point)

    # Extension sud


def croissance_sud(position_croissance_sud):
    x, y, z = position_croissance_sud

    ix = randint(0, 4)
    for iy in range(1, randint(1, 4)):
        l_sud.append((ix * 4.4 + 0.4 + x, -4.4 * iy + 0.4 + y, 0))
        l_cel_globale.append((ix * 4.4 + 0.4 + x, -4.4 * iy + 0.4 + y, 0))
    print("sud----->", len(l_sud), l_sud)
    for point in l_sud:
        cel(point)

    # Extension ouest


def croissance_ouest(position_croissance_ouest):
    x, y, z = position_croissance_ouest

    iy = randint(0, 4)
    for ix in range(1, randint(1, 4)):
        l_ouest.append((-4.4 * ix + 0.4 + x, iy * 4.4 + 0.4 + y, 0))
    print("ouest----->", len(l_ouest), l_ouest)
    for point in l_ouest:
        cel(point)


# 5 DEFINITION ENSEMBLE ARBORESCENT###########################


def ensemble(position_ensemble):
    x, y, z = position_ensemble
    l_croissance = [0, 1, 2, 3]

    groupement_cel(position_ensemble)
    croissance_nord(position_ensemble)

    l_cel_globale.remove(l_nord[-1])
    for i in range(0, 2):
        if len(l_nord) > 0:
            groupement_cel_0(l_nord[-1])

        if len(l_est) > 0:
            groupement_cel_1(l_est[-1])
        if len(l_est_1) > 0:
            groupement_cel_1(l_est_1[-1])

        if len(l_ouest) > 0:
            groupement_cel_3(l_ouest[-1])
        if len(l_ouest_3) > 0:
            groupement_cel_3(l_ouest_3[-1])
        print("============================")


ensemble((0, 0, 0))


print("il y a", len(l_population), "habitations")
print("il y a", len(l_cel_globale), "cellules au total")
