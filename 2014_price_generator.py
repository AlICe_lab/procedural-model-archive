# SPDX-FileCopyrightText: 2014 Manuel Leon Fanjul
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# BLENDER 2.71 Hash:9337574
# Windows 8

import bpy
import random
import math
import mathutils
from mathutils import Vector

# bpy.ops.object.select_all(action='SELECT')
# bpy.ops.object.delete(use_global=False)


##################################################################################################
# -------------------------------------------------------------------------------------------------
#                                       CATALOGUE DE CUBE
# -------------------------------------------------------------------------------------------------
##################################################################################################

"DEFINITIONS NECESSAIRES A LA CREATION DES CUBES"


def Boite(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))


def Name(k):
    bpy.context.object.name = k
    bpy.context.object.data.name = k


def Join(i, j):
    SelectObjectContext(i)
    bpy.context.scene.objects[i].select = True
    SelectObjectContext(j)
    bpy.context.scene.objects[j].select = True
    bpy.ops.object.join()


def Place_obj(obj, x=0, y=0, z=0):
    bpy.data.objects[obj].location = (x, y, z)


def SelectObjectContext(erd):
    bpy.context.scene.objects[erd].select = True


##################################################################################################################
# ---------------------------------------------------Fonctions Cubes----------------------------------------------
##################################################################################################################

print(
    "-----------------------------------------------Cub_Arete: 0 /6 ----------------------------------------------"
)


def Cub_0(x=0, y=0, z=0):

    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boite1")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boite2")
    Join("Boite1", "Boite2")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boite3")
    Join("Boite2", "Boite3")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite4")
    Join("Boite3", "Boite4")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite5")
    Join("Boite4", "Boite5")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite6")
    Join("Boite5", "Boite6")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite7")
    Join("Boite6", "Boite7")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boite8")
    Join("Boite7", "Boite8")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boite9")
    Join("Boite8", "Boite9")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boite10")
    Join("Boite9", "Boite10")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boite11")
    Join("Boite10", "Boite11")
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boite12")
    Join("Boite11", "Boite12")
    Name("Cub_0")
    # Place_obj('Cub_0',x,1.65+y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_Toit: 1 /6 -------------------------------------------"
)


def Cub_1(x=0, y=0, z=0):

    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Boite13")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boite14")
    Join("Boite13", "Boite14")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boite15")
    Join("Boite14", "Boite15")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boite16")
    Join("Boite15", "Boite16")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite17")
    Join("Boite16", "Boite17")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite18")
    Join("Boite17", "Boite18")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite19")
    Join("Boite18", "Boite19")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boite20")
    Join("Boite19", "Boite20")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boite21")
    Join("Boite20", "Boite21")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boite22")
    Join("Boite21", "Boite22")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boite23")
    Join("Boite22", "Boite23")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boite24")
    Join("Boite23", "Boite24")
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boite25")
    Join("Boite24", "Boite25")
    # Name('Cub_1')
    # Place_obj('Cub_1',x,1.65+y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_Sol: 1 /6 ----------------------------------------------"
)


def Cub_2(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boi17")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boi18")
    Join("Boi17", "Boi18")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boi19")
    Join("Boi18", "Boi19")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boi20")
    Join("Boi19", "Boi20")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi21")
    Join("Boi20", "Boi21")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi22")
    Join("Boi21", "Boi22")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi23")
    Join("Boi22", "Boi23")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi24")
    Join("Boi23", "Boi24")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boi25")
    Join("Boi24", "Boi25")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boi26")
    Join("Boi25", "Boi26")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boi27")
    Join("Boi26", "Boi27")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boi28")
    Join("Boi27", "Boi28")
    Boite(x + 0, y + 0, z + 0.15, 1.5, 1.5, 0.15)
    Name("Boi29")
    Join("Boi28", "Boi29")
    # Name('Cub_2')
    # Place_obj('Cub_2',x,y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_ToFa: 2 /6 ----------------------------------------------"
)


def Cub_3(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, 1.8, 1.8, 0.15, 1.8)
    Name("Bot1")
    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Bot2")
    Join("Bot1", "Bot2")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Bot3")
    Join("Bot2", "Bot3")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Bot4")
    Join("Bot3", "Bot4")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Bot5")
    Join("Bot4", "Bot5")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot6")
    Join("Bot5", "Bot6")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot7")
    Join("Bot6", "Bot7")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot8")
    Join("Bot7", "Bot8")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot9")
    Join("Bot8", "Bot9")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Bot10")
    Join("Bot9", "Bot10")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Bot11")
    Join("Bot10", "Bot11")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Bot12")
    Join("Bot11", "Bot12")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Bot13")
    Join("Bot12", "Bot13")
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Bot14")
    Join("Bot13", "Bot14")
    # Name('Cub_3')
    # Place_obj('Cub_3',x,1.65+y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_SolToit: 2 /6 ----------------------------------------------"
)


def Cub_4(x=0, y=0, z=0):

    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boix1")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boix2")
    Join("Boix1", "Boix2")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boix3")
    Join("Boix2", "Boix3")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boix4")
    Join("Boix3", "Boix4")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix5")
    Join("Boix4", "Boix5")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix6")
    Join("Boix5", "Boix6")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix7")
    Join("Boix6", "Boix7")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix8")
    Join("Boix7", "Boix8")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boix9")
    Join("Boix8", "Boix9")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boix10")
    Join("Boix9", "Boix10")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boix11")
    Join("Boix10", "Boix11")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boix12")
    Join("Boix11", "Boix12")
    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Boix13")
    Join("Boix12", "Boix13")
    Boite(x + 0, y + 0, z + 0.15, 1.5, 1.5, 0.15)
    Name("Boix14")
    Join("Boix13", "Boix14")
    # Name('Cub_4')
    # Place_obj('Cub_4',x,y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_ToFaFa: 3 /6 ----------------------------------------------"
)


def Cub_5(x=0, y=0, z=0):

    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Bot14")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Bot15")
    Join("Bot14", "Bot15")
    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Bot16")
    Join("Bot15", "Bot16")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Bot17")
    Join("Bot16", "Bot17")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Bot18")
    Join("Bot17", "Bot18")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Bot19")
    Join("Bot18", "Bot19")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot20")
    Join("Bot19", "Bot20")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot21")
    Join("Bot20", "Bot21")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot22")
    Join("Bot21", "Bot22")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Bot23")
    Join("Bot22", "Bot23")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Bot24")
    Join("Bot23", "Bot24")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Bot25")
    Join("Bot24", "Bot25")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Bot26")
    Join("Bot25", "Bot26")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Bot27")
    Join("Bot26", "Bot27")
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Bot28")
    Join("Bot27", "Bot28")

    Name("CubeV")


#################################################################################################################
print(
    "-----------------------------------------------Cub_3Face: 3 /6 ----------------------------------------------"
)


def Cub_6(x=0, y=0, z=0):

    Boite(x + 0, y + 1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0i1")
    Boite(x + 1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0i2")
    Join("B0i1", "B0i2")
    Boite(x + 0, y + -1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0i3")
    Join("B0i2", "B0i3")
    Boite(x + -1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0i4")
    Join("B0i3", "B0i4")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i5")
    Join("B0i4", "B0i5")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i6")
    Join("B0i5", "B0i6")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i7")
    Join("B0i6", "B0i7")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i8")
    Join("B0i7", "B0i8")
    Boite(x + 0, y + 1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0i9")
    Join("B0i8", "B0i9")
    Boite(x + 1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0i10")
    Join("B0i9", "B0i10")
    Boite(x + 0, y + -1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0i11")
    Join("B0i10", "B0i11")
    Boite(x + -1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0i12")
    Join("B0i11", "B0i12")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("B0i13")
    Join("B0i12", "B0i13")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("B0i14")
    Join("B0i13", "B0i14")
    Boite(x + 0, y + 0, z + 0.1, 1.5, 1.5, 0.15)
    Name("B0i15")
    Join("B0i14", "B0i15")

    bpy.ops.transform.resize(value=(1, 1, 3.6 / 3.7))
    Name("CubeV")


#################################################################################################################
print(
    "-----------------------------------------------FaceaFace: 3 /6 ----------------------------------------------"
)


def Cub_7(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("Box1")
    Boite(x + 1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("Box2")
    Join("Box1", "Box2")
    Boite(x + 0, y + -1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("Box3")
    Join("Box2", "Box3")
    Boite(x + -1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("Box4")
    Join("Box3", "Box4")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box5")
    Join("Box4", "Box5")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box6")
    Join("Box5", "Box6")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box7")
    Join("Box6", "Box7")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box8")
    Join("Box7", "Box8")
    Boite(x + 0, y + 1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("Box9")
    Join("Box8", "Box9")
    Boite(x + 1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("Box10")
    Join("Box9", "Box10")
    Boite(x + 0, y + -1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("Box11")
    Join("Box10", "Box11")
    Boite(x + -1.65, y + 0, 3.5, 0.15, 1.8, 0.15)
    Name("Box12")
    Join("Box11", "Box12")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Box13")
    Join("Box12", "Box13")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Box14")
    Join("Box13", "Box14")
    Boite(x + 0, y + 0, z + 0.1, 1.5, 1.5, 0.15)
    Name("Box15")
    Join("Box14", "Box15")

    bpy.ops.transform.resize(value=(1, 1, 3.6 / 3.7))
    Name("CubeV")


#################################################################################################################

print(
    "-----------------------------------------------Cube_SoFaTo: 3 /6 ----------------------------------------------"
)


def Cub_8(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Box16")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Box17")
    Join("Box16", "Box17")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Box18")
    Join("Box17", "Box18")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Box19")
    Join("Box18", "Box19")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box20")
    Join("Box19", "Box20")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box21")
    Join("Box20", "Box21")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box22")
    Join("Box21", "Box22")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Box23")
    Join("Box22", "Box23")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Box24")
    Join("Box23", "Box24")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Box25")
    Join("Box24", "Box25")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Box26")
    Join("Box25", "Box26")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Box27")
    Join("Box26", "Box27")
    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Box28")
    Join("Box27", "Box28")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("Box29")
    Join("Box28", "Box29")
    Boite(x + 0, y + 0, z + 0.15, 1.5, 1.5, 0.15)
    Name("Box30")
    Join("Box29", "Box30")
    # Name('Cub_8')
    # Place_obj('Cub_8',x,y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor(use_offset=False)


#################################################################################################################
print(
    "-----------------------------------------------Cub_4Face: 4 /6 ----------------------------------------------"
)


def Cub_9(x=0, y=0, z=0):

    Boite(x + 0, y + 1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0i16")
    Boite(x + 1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0i17")
    Join("B0i16", "B0i17")
    Boite(x + 0, y + -1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0i18")
    Join("B0i17", "B0i18")
    Boite(x + -1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0i19")
    Join("B0i18", "B0i19")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i20")
    Join("B0i19", "B0i20")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i21")
    Join("B0i20", "B0i21")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i22")
    Join("B0i21", "B0i22")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0i23")
    Join("B0i22", "B0i23")
    Boite(x + 0, y + 1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0i24")
    Join("B0i23", "B0i24")
    Boite(x + 1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0i25")
    Join("B0i24", "B0i25")
    Boite(x + 0, y + -1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0i26")
    Join("B0i25", "B0i26")
    Boite(x + -1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0i27")
    Join("B0i26", "B0i27")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("B0i28")
    Join("B0i27", "B0i28")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("B0i29")
    Join("B0i28", "B0i29")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("B0i30")
    Join("B0i29", "B0i30")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("B0i31")
    Join("B0i30", "B0i31")
    Boite(x + 0, y + 0, z + 0.1, 1.5, 1.5, 0.15)
    Name("B0i32")
    Join("B0i31", "B0i32")

    bpy.ops.transform.resize(value=(1, 1, 3.6 / 3.7))
    Name("CubeV")


#################################################################################################################
print(
    "-----------------------------------------------Cub_To3Fa: 4 /6 ----------------------------------------------"
)


def Cub_10(x=0, y=0, z=0):

    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("Boi1")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Boi2")
    Join("Boi1", "Boi2")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Boi3")
    Join("Boi2", "Boi3")
    Boite(x + 0, y + 0, z + 3.45, 1.5, 1.5, 0.15)
    Name("Boi4")
    Join("Boi3", "Boi4")
    Boite(x + 1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boi5")
    Join("Boi4", "Boi5")
    Boite(x + 0, y + -1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boi6")
    Join("Boi5", "Boi6")
    Boite(x + -1.65, y + 0, z + 0.15, 0.15, 1.8, 0.15)
    Name("Boi7")
    Join("Boi6", "Boi7")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi8")
    Join("Boi7", "Boi8")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi9")
    Join("Boi8", "Boi9")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi10")
    Join("Boi9", "Boi10")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boi11")
    Join("Boi10", "Boi11")
    Boite(x + 0, y + 1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boi12")
    Join("Boi11", "Boi12")
    Boite(x + 1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boi13")
    Join("Boi12", "Boi13")
    Boite(x + 0, y + -1.65, z + 3.45, 1.8, 0.15, 0.15)
    Name("Boi14")
    Join("Boi13", "Boi14")
    Boite(x + -1.65, y + 0, z + 3.45, 0.15, 1.8, 0.15)
    Name("Boi15")
    Join("Boi14", "Boi15")
    Boite(x + 0, y + 1.65, z + 0.15, 1.8, 0.15, 0.15)
    Name("Boi16")
    Join("Boi15", "Boi16")
    # Name('Cub_10')
    # Place_obj('Cub_10',x,1.65+y,z)
    Name("CubeV")
    # bpy.context.scene.cursor_location = mathutils.Vector((x, y, z))
    # bpy.ops.view3d.snap_selected_to_cursor()


#################################################################################################################
print(
    "-----------------------------------------------Cub_5Face: 5 /6 ----------------------------------------------"
)


def Cub_11(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0it1")
    Boite(x + 1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0it2")
    Join("B0it1", "B0it2")
    Boite(x + 0, y + -1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("B0it3")
    Join("B0it2", "B0it3")
    Boite(x + -1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("B0it4")
    Join("B0it3", "B0it4")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0it5")
    Join("B0it4", "B0it5")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0it6")
    Join("B0it5", "B0it6")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0it7")
    Join("B0it6", "B0it7")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("B0it8")
    Join("B0it8", "B0it7")
    Boite(x + 0, y + 1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0it9")
    Join("B0it8", "B0it9")
    Boite(x + 1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0it10")
    Join("B0it9", "B0it10")
    Boite(x + 0, y + -1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("B0it11")
    Join("B0it10", "B0it11")
    Boite(x + -1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("B0it12")
    Join("B0it11", "B0it12")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("B0it13")
    Join("B0it12", "B0it13")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("B0it14")
    Join("B0it13", "B0it14")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("B0it15")
    Join("B0it14", "B0it15")
    Boite(x + 0, y + -1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("B0it16")
    Join("B0it15", "B0it16")
    Boite(x + 0, y + 0, z + 0.1, 1.5, 1.5, 0.15)
    Name("B0it17")
    Join("B0it16", "B0it17")

    bpy.ops.transform.resize(value=(1, 1, 3.6 / 3.7))
    Name("CubeV")


#################################################################################################################
print(
    "-----------------------------------------------Cub_Plein: 6 /6 ----------------------------------------------"
)


def Cub_12(x=0, y=0, z=0):
    Boite(x + 0, y + 1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("Boix15")
    Boite(x + 1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("Boix16")
    Join("Boix15", "Boix16")
    Boite(x + 0, y + -1.65, z + 0.1, 1.8, 0.15, 0.15)
    Name("Boix17")
    Join("Boix16", "Boix17")
    Boite(x + -1.65, y + 0, z + 0.1, 0.15, 1.8, 0.15)
    Name("Boix18")
    Join("Boix17", "Boix18")
    Boite(x + 1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix19")
    Join("Boix18", "Boix19")
    Boite(x + -1.65, y + 1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix20")
    Join("Boix19", "Boix20")
    Boite(x + 1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix21")
    Join("Boix20", "Boix21")
    Boite(x + -1.65, y + -1.65, z + 1.8, 0.15, 0.15, 1.8)
    Name("Boix22")
    Join("Boix21", "Boix22")
    Boite(x + 0, y + 1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("Boix23")
    Join("Boix22", "Boix23")
    Boite(x + 1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("Boix24")
    Join("Boix23", "Boix24")
    Boite(x + 0, y + -1.65, z + 3.5, 1.8, 0.15, 0.15)
    Name("Boix25")
    Join("Boix24", "Boix25")
    Boite(x + -1.65, y + 0, z + 3.5, 0.15, 1.8, 0.15)
    Name("Boix26")
    Join("Boix25", "Boix26")
    Boite(x + 1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Boix27")
    Join("Boix26", "Boix27")
    Boite(x + -1.65, y + 0, z + 1.8, 0.15, 1.8, 1.8)
    Name("Boix28")
    Join("Boix27", "Boix28")
    Boite(x + 0, y + 1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("Boix29")
    Join("Boix28", "Boix29")
    Boite(x + 0, y + -1.65, z + 1.8, 1.8, 0.15, 1.8)
    Name("Boix30")
    Join("Boix29", "Boix30")
    Boite(x + 0, y + 0, z + 3.5, 1.5, 1.5, 0.15)
    Name("Boix31")
    Join("Boix30", "Boix31")
    Boite(x + 0, y + 0, z + 0.1, 1.5, 1.5, 0.15)
    Name("Boix32")
    Join("Boix31", "Boix32")

    bpy.ops.transform.resize(value=(1, 1, 3.6 / 3.7))
    Name("CubeV")


##################################################################################################################
# ---------------------------------------------------Fonctions Cubes-Circulation----------------------------------
##################################################################################################################


#####################################################################################################################
# EscPass1()
#####################################################################################################################
def Pan_Inc1(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 7, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Pan_Inc2(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 3.3, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Rampe1(x=0, y=0, z=0):
    Pan_Inc1(x, y + 2.3, z + 0.95, 2, 0.5, 0.05)


def Rampe2(x=0, y=0, z=0):
    Pan_Inc2(x - 2.3, y + 0.5, z + 2.7, 0.5, 0.05, 1.65)


def Palier1(x=0, y=0, z=0):
    Boite(-2.3, 2.3, 1.8, 0.5, 0.5, 0.05)


def Palier2(x=0, y=0, z=0):
    Boite(-2.3, -1.3, 3.65, 0.5, 0.5, 0.05)


# Passerelle


def Pass90(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))


def Pass45(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / (-4), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass1(x=0, y=0, z=0):
    Pass90(0, -1.3, 3.65, 1.8, 0.5, 0.05)


def Pass1b(x=0, y=0, z=0):
    Pass90(6.65, -4.5, 3.65, 1.8, 0.5, 0.05)


def Pass1c(x=0, y=0, z=0):
    Pass90(13.3, -7.68, 3.65, 1.8, 0.5, 0.05)


def Pass2(x=0, y=0, z=0):
    Pass45(3.325, -2.89, 3.65, 2.55, 0.4, 0.05)


def Pass2b(x=0, y=0, z=0):
    Pass45(9.97, -6.09, 3.65, 2.55, 0.4, 0.05)


# Definition


def EscPass1(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu1")
    Rampe1(0, 0, 0)
    Name("Ram1")
    Join("Cu1", "Ram1")
    Rampe2(0, 0, 0)
    Name("Ram2")
    Join("Ram1", "Ram2")
    Palier1()
    Name("Pa1")
    Join("Ram2", "Pa1")
    Palier2()
    Name("Pa2")
    Join("Pa1", "Pa2")
    Pass1()
    Name("Pas1")
    Join("Pa2", "Pas1")
    Pass1b()
    Name("Pas1b")
    Join("Pas1", "Pas1b")
    Pass1c()
    Name("Pas1c")
    Join("Pas1b", "Pas1c")
    Pass2()
    Name("Pas2")
    Join("Pas1c", "Pas2")
    Pass2b()
    Name("Pas2b")
    Join("Pas2", "Pas2b")
    Name("Cube")


#####################################################################################################################
# EscPass2()
#####################################################################################################################


def Pass45c(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 4, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass2c(x=0, y=0, z=0):
    Pass45(-0.28, -2.89, 3.65, 2.55, 0.4, 0.05)


def Pass2d(x=0, y=0, z=0):
    Pass45(6.36, -6.06, 3.65, 2.55, 0.4, 0.05)


def Pass2e(x=0, y=0, z=0):
    Pass45c(6.35, -12.7, 3.65, 2.55, 0.4, 0.05)


def Pass1d(x=0, y=0, z=0):
    Pass90(3.045, -4.475, 3.65, 1.8, 0.5, 0.05)


def Pass1e(x=0, y=0, z=0):
    Pass90(7.94, -9.37, 3.65, 0.5, 1.8, 0.05)


def Pass1f(x=0, y=0, z=0):
    Pass90(4.755, -16.02, 3.65, 0.5, 1.8, 0.05)


#####################################################################################################################
def EscPass2(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu2")
    Rampe1(0, 0, 0)
    Name("Ra1")
    Join("Cu2", "Ra1")
    Rampe2(0, 0, 0)
    Name("Ra2")
    Join("Ra1", "Ra2")
    Palier1()
    Name("Pal1")
    Join("Ra2", "Pal1")
    Palier2()
    Name("Pal2")
    Join("Pal1", "Pal2")
    Pass2c()
    Name("Pass2c")
    Join("Pal2", "Pass2c")
    Pass1d()
    Name("Pass1d")
    Join("Pass2c", "Pass1d")
    Pass2d()
    Name("Pass2d")
    Join("Pass1d", "Pass2d")
    Pass1e()
    Name("Pass1e")
    Join("Pass2d", "Pass1e")
    Pass2e()
    Name("Pass2e")
    Join("Pass1e", "Pass2e")
    Pass1f()
    Name("Pass1f")
    Join("Pass2e", "Pass1f")
    Name("Cube")


#####################################################################################################################
# EscPass3()
#####################################################################################################################
def Pan_Inc3(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / (-3.3), axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Pan_Inc4(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 3.3, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Pass90b(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))


def Pass45b(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 4, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass45(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / (-4), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


# Rampe


def Rampe3(x=0, y=0, z=0):
    Pan_Inc3(x - 0.5, y - 2.3, z + 2.7, 0.05, 0.5, 1.65)


def Rampe4(x=0, y=0, z=0):
    Pan_Inc4(x - 0.4, y - 3.3, z + 0.94, 0.05, 0.5, 1.5)


# Paliers


def Palier3(x=0, y=0, z=0):
    Boite(-2.3, -1.8, 3.65, 0.5, 1, 0.05)


def Palier4(x=0, y=0, z=0):
    Boite(1.3, -2.8, 1.8, 0.5, 1, 0.05)


def Pass3(x=0, y=0, z=0):
    Pass45b(-0.3, 0.3, 3.65, 2.55, 0.4, 0.05)


def Pass3b(x=0, y=0, z=0):
    Pass45b(6.32, 3.46, 3.65, 2.55, 0.4, 0.05)


def Pass3c(x=0, y=0, z=0):
    Pass45b(9.5, 10.1, 3.65, 2.55, 0.4, 0.05)


def Pass3d(x=0, y=0, z=0):
    Pass45(16.14, 10.1, 3.65, 2.55, 0.4, 0.05)


def Pass4(x=0, y=0, z=0):
    Pass90b(3, 1.87, 3.65, 1.8, 0.5, 0.05)


def Pass4b(x=0, y=0, z=0):
    Pass90b(12.82, 11.69, 3.65, 1.8, 0.5, 0.05)


def Pass5(x=0, y=0, z=0):
    Pass90b(7.91, 6.78, 3.65, 0.5, 1.8, 0.05)


#####################################################################################################################
def EscPass3(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu3")
    Palier3()
    Name("Pl3")
    Join("Cu3", "Pl3")
    Palier4()
    Name("Pl4")
    Join("Pl3", "Pl4")
    Rampe3()
    Name("Rm3")
    Join("Pl4", "Rm3")
    Rampe4()
    Name("Rm4")
    Join("Rm3", "Rm4")
    Pass3()
    Name("Ps3")
    Join("Rm4", "Ps3")
    Pass4()
    Name("Ps4")
    Join("Ps3", "Ps4")
    Pass4b()
    Name("Ps4b")
    Join("Ps4", "Ps4b")
    Pass3b()
    Name("Ps3b")
    Join("Ps4b", "Ps3b")
    Pass3c()
    Name("Ps3c")
    Join("Ps3b", "Ps3c")
    Pass3d()
    Name("Ps3d")
    Join("Ps3c", "Ps3d")
    Pass5()
    Name("Ps5")
    Join("Ps3d", "Ps5")
    Name("Cube")


#####################################################################################################################
# EscPass4()
#####################################################################################################################


def Pass6(x=0, y=0, z=0):
    Pass90(-1.3, 0, 3.65, 0.5, 1.8, 0.05)


def Pass3e(x=0, y=0, z=0):
    Pass45(-2.89, 3.32, 3.65, 2.55, 0.4, 0.05)


def Pass3f(x=0, y=0, z=0):
    Pass45(-9.52, 6.49, 3.65, 2.55, 0.4, 0.05)


def Pass3g(x=0, y=0, z=0):
    Pass45(-16.17, 9.7, 3.65, 2.55, 0.4, 0.05)


def Pass7(x=0, y=0, z=0):
    Pass90(-6.2, 4.9, 3.65, 1.8, 0.5, 0.05)


def Pass8(x=0, y=0, z=0):
    Pass90(-12.85, 8.1, 3.65, 1.8, 0.5, 0.05)


#####################################################################################################################


def EscPass4(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu4")
    Palier3()
    Name("Pli3")
    Join("Cu4", "Pli3")
    Palier4()
    Name("Pli4")
    Join("Pli3", "Pli4")
    Rampe3()
    Name("Rp3")
    Join("Pli4", "Rp3")
    Rampe4()
    Name("Rp4")
    Join("Rp3", "Rp4")
    Pass6()
    Name("Pss6")
    Join("Rp4", "Pss6")
    Pass3e()
    Name("Pss3e")
    Join("Pss6", "Pss3e")
    Pass7()
    Name("Pss7")
    Join("Pss3e", "Pss7")
    Pass3f()
    Name("Pss3f")
    Join("Pss7", "Pss3f")
    Pass8()
    Name("Pss8")
    Join("Pss3f", "Pss8")
    Pass3g()
    Name("Pss3g")
    Join("Pss8", "Pss3g")
    Name("Cube")


#####################################################################################################################
# EscPass5()
#####################################################################################################################
def Pass45c(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 4, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass9(x=0, y=0, z=0):
    Pass90(-12.73, 1.7, 3.65, 1.8, 0.5, 0.05)


def Pass3h(x=0, y=0, z=0):
    Pass45c(-9.4, 3.3, 3.65, 2.55, 0.4, 0.05)


####################################################################################################################
def EscPass5(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu5")
    Palier3()
    Name("Plr3")
    Join("Cu5", "Plr3")
    Palier4()
    Name("Plr4")
    Join("Plr3", "Plr4")
    Rampe3()
    Name("Rmp3")
    Join("Plr4", "Rmp3")
    Rampe4()
    Name("Rmp4")
    Join("Rmp3", "Rmp4")
    Pass6()
    Name("Passe6")
    Join("Rmp4", "Passe6")
    Pass3e()
    Name("Passe3e")
    Join("Passe6", "Passe3e")
    Pass7()
    Name("Passe7")
    Join("Passe3e", "Passe7")
    Pass3f()
    Name("Passe3f")
    Join("Passe7", "Passe3f")
    Pass8()
    Name("Passe8")
    Join("Passe3f", "Passe8")
    Pass3g()
    Name("Passe3g")
    Join("Passe8", "Passe3g")
    Pass3h()
    Name("Passe3h")
    Join("Passe3g", "Passe3h")
    Pass9()
    Name("Passe9")
    Join("Passe3h", "Passe9")
    Name("Cube")


#####################################################################################################################
# EscPass6()
#####################################################################################################################


def EscPass6(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu6")
    Palier3()
    Name("Prl3")
    Join("Cu6", "Prl3")
    Palier4()
    Name("Prl4")
    Join("Prl3", "Prl4")
    Rampe3()
    Name("Rpm3")
    Join("Prl4", "Rpm3")
    Rampe4()
    Name("Rpm4")
    Join("Rpm3", "Rpm4")
    Pass6()
    Name("Psa6")
    Join("Rpm4", "Psa6")
    Pass3e()
    Name("Psa3e")
    Join("Psa6", "Psa3e")
    Pass7()
    Name("Psa7")
    Join("Psa3e", "Psa7")
    Pass3h()
    Name("Psa3h")
    Join("Psa7", "Psa3h")
    Pass9()
    Name("Psa9")
    Join("Psa3h", "Psa9")
    Name("Pass")


#####################################################################################################################
# EscPass7()
#####################################################################################################################
def Pass45d(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 4, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass1g(x=0, y=0, z=0):
    Pass90(4.92, -6.2, 3.65, 0.5, 1.8, 0.05)


def Pass1h(x=0, y=0, z=0):
    Pass90(-0.01, -11.11, 3.65, 1.8, 0.5, 0.05)


def Pass1j(x=0, y=0, z=0):
    Pass90(-3.61, -11.11, 3.65, 1.8, 0.5, 0.05)


def Pass3i(x=0, y=0, z=0):
    Pass45d(3.32, -9.52, 3.65, 2.55, 0.4, 0.05)


def Pass3j(x=0, y=0, z=0):
    Pass45d(-6.9, -12.7, 3.65, 2.55, 0.4, 0.05)


#################################################################################################################


def EscPass7(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu7")
    Rampe1()
    Name("Rma1")
    Join("Cu7", "Rma1")
    Rampe2()
    Name("Rma2")
    Join("Rma1", "Rma2")
    Palier1()
    Name("Pla1")
    Join("Rma2", "Pla1")
    Palier2()
    Name("Pla2")
    Join("Pla1", "Pla2")
    Pass1()
    Name("Pssa1")
    Join("Pla2", "Pssa1")
    Pass1g()
    Name("Pssa1g")
    Join("Pssa1", "Pssa1g")
    Pass2()
    Name("Pssa2")
    Join("Pssa1g", "Pssa2")
    Pass3i()
    Name("Pssa3i")
    Join("Pssa2", "Pssa3i")
    Pass1h()
    Name("Pssa1h")
    Join("Pssa3i", "Pssa1h")
    Pass1j()
    Name("Pssa1j")
    Join("Pssa1h", "Pssa1j")
    Pass3j()
    Name("Pssa3j")
    Join("Pssa1j", "Pssa3j")
    Name("Cube")


#####################################################################################################################
# EscPass8()
#####################################################################################################################


def Pass45e(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / (-4), axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Pass3k(x=0, y=0, z=0):
    Pass45e(6.6, -9.48, 3.65, 2.55, 0.4, 0.05)


#################################################################################################################


def EscPass8(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu8")
    Rampe1()
    Name("Rem1")
    Join("Cu8", "Rem1")
    Rampe2()
    Name("Rem2")
    Join("Rem1", "Rem2")
    Palier1()
    Name("Ple1")
    Join("Rem2", "Ple1")
    Palier2()
    Name("Ple2")
    Join("Ple1", "Ple2")
    Pass1()
    Name("Pes1")
    Join("Ple2", "Pes1")
    Pass1g()
    Name("Pes1g")
    Join("Pes1", "Pes1g")
    Pass2()
    Name("Pes2")
    Join("Pes1g", "Pes2")
    Pass3i()
    Name("Pes3i")
    Join("Pes2", "Pes3i")
    Pass1h()
    Name("Pes1h")
    Join("Pes3i", "Pes1h")
    Pass1j()
    Name("Pes1j")
    Join("Pes1h", "Pes1j")
    Pass3j()
    Name("Pes3j")
    Join("Pes1j", "Pes3j")
    Pass3k()
    Name("Pes3k")
    Join("Pes3j", "Pes3k")
    Name("Cube")


#####################################################################################################################
# EscPass9()
#####################################################################################################################


def Pass1i(x=0, y=0, z=0):
    Pass90(8.2, -12.8, 3.65, 0.5, 1.8, 0.05)


def EscPass9(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu9")
    Rampe1()
    Name("Rae1")
    Join("Cu9", "Rae1")
    Rampe2()
    Name("Rae2")
    Join("Rae1", "Rae2")
    Palier1()
    Name("Pae1")
    Join("Rae2", "Pae1")
    Palier2()
    Name("Pae2")
    Join("Pae1", "Pae2")
    Pass1()
    Name("Pase1")
    Join("Pae2", "Pase1")
    Pass1g()
    Name("Pase1g")
    Join("Pase1", "Pase1g")
    Pass2()
    Name("Pase2")
    Join("Pase1g", "Pase2")
    Pass3i()
    Name("Pase3i")
    Join("Pase2", "Pase3i")
    Pass3k()
    Name("Pase3k")
    Join("Pase3i", "Pase3k")
    Pass1i()
    Name("Pase1i")
    Join("Pase3k", "Pase1i")
    Name("Cube")


#####################################################################################################################
# EscPass10()
#####################################################################################################################


def Pass1k(x=0, y=0, z=0):
    Pass90(9.9, -11.07, 3.65, 1.8, 0.5, 0.05)


def Pass1l(x=0, y=0, z=0):
    Pass90(1.75, -12.8, 3.65, 0.5, 1.8, 0.05)


def EscPass10(x=0, y=0, z=0):

    Boite(0, 0, 1.8, 1.8, 1.8, 1.8)
    Name("Cu10")
    Rampe1()
    Name("Rape1")
    Join("Cu10", "Rape1")
    Palier1()
    Name("Pler1")
    Join("Rape1", "Pler1")
    Rampe2()
    Name("Rape2")
    Join("Pler1", "Rape2")
    Palier2()
    Name("Pler2")
    Join("Rape2", "Pler2")
    Pass1()
    Name("Psl1")
    Join("Pler2", "Psl1")
    Pass1g()
    Name("Psl1g")
    Join("Psl1", "Psl1g")
    Pass2()
    Name("Psl2")
    Join("Psl1g", "Psl2")
    Pass3i()
    Name("Psl3i")
    Join("Psl2", "Psl3i")
    Pass3k()
    Name("Psl3k")
    Join("Psl3i", "Psl3k")
    Pass1k()
    Name("Psl1k")
    Join("Psl3k", "Psl1k")
    Pass1l()
    Name("Psl1l")
    Join("Psl1k", "Psl1l")
    Name("Cube")


##################################################################################################################
# LISTE_CUBE
"""
NOMBRE DE FACE

Cub_0= 0/6
Cub_1= 1/6
Cub_2= 1/6
Cub_3= 2/6
Cub_4= 2/6
Cub_5= 3/6
Cub_6= 3/6
Cub_7= 3/6
Cub_8= 3/6
Cub_9= 4/6
Cub_10= 4/6
Cub_11= 5/6
Cub_12= 6/6
"""

L_CUB = [
    Cub_0,
    Cub_1,
    Cub_2,
    Cub_3,
    Cub_4,
    Cub_5,
    Cub_6,
    Cub_7,
    Cub_8,
    Cub_9,
    Cub_10,
    Cub_11,
    Cub_12,
]

# L_Cub [11]() APPELE D'UNE LISTE

# QUANTITE DE CUBES PAR TYPE

P0 = 10
P1 = 10
P2 = 10
P3 = 10
P4 = 10
P5 = 10
P6 = 5
P7 = 5
P8 = 5
P9 = 5
P10 = 5
P11 = 5
P12 = 10


# POURCENTAGE DE CUBE PAR TYPE

L_PERC = [P0, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12]

# DEFINTION


def MIX_CUB(x=0, y=0, z=0):
    NB = random.randint(0, 99)
    Somme = 0
    for i in range(0, 13):
        Somme = Somme + L_PERC[i]
        if NB <= Somme:
            L_CUB[i](x, y, z)
            break


##################################################################################################
# -------------------------------------------------------------------------------------------------
#                               CATALOGUE D'ARBRES ARTIFICIELS
# -------------------------------------------------------------------------------------------------
##################################################################################################

"DEFINITIONS NECESSAIRE A LA CREATION DES ARBRES"

print(
    "-----------------------------------------------Modificateurs----------------------------------------------"
)


def Join(i, j):
    SelectObjectContext(i)
    bpy.context.scene.objects[i].select = True
    SelectObjectContext(j)
    bpy.context.scene.objects[j].select = True
    bpy.ops.object.join()


def Name(k):
    bpy.context.object.name = k
    bpy.context.object.data.name = k


def Place_obj(obj, x=0, y=0, z=0):
    bpy.data.objects[obj].location = (x, y, z)


def SelectObjectContext(erd):
    bpy.context.scene.objects[erd].select = True


####################################################################################################################

print(
    "-----------------------------------------------Base Volumétrique----------------------------------------------"
)


def Boite(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Cone(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cone_add(
        radius1=1.5,
        radius2=0,
        depth=1.5,
        view_align=False,
        enter_editmode=False,
        location=(x, y, z),
    )
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Conex(x, y, z, r, d, dim_x, dim_y, dim_z, n):
    bpy.ops.mesh.primitive_cone_add(
        radius1=r,
        radius2=0,
        depth=d,
        view_align=False,
        enter_editmode=False,
        location=(x, y, z),
    )
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / n, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Cylindre(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Cylindre1(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Sphere(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_uv_sphere_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Boite2(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Boite3(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Boite4(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Boite5(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Boite6(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Arete_sol1():
    Boite1(0, 1.75, 0, 1.8, 0.05, 0.05)
    Name("Boit1")
    Boite1(1.75, 0, 0, 0.05, 1.8, 0.05)
    Name("Boit2")
    Join("Boit1", "Boit2")
    Boite1(0, -1.75, 0, 1.8, 0.05, 0.05)
    Name("Boit3")
    Join("Boit2", "Boit3")
    Boite1(-1.75, 0, 0, 0.05, 1.8, 0.05)
    Name("Boit4")
    Join("Boit3", "Boit4")


##################################################################################################################
# DEFINITION DES ARBRES

print(
    "-----------------------------------------------Tree_ant()----------------------------------------------"
)


def Socle(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def StrucTub(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Ant_Princ(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 2, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Ant_(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 3, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Ant_Sec():
    Ant_(2.125, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_1")
    Ant_(-2.125, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_2")
    Join("Ant_1", "Ant_2")
    Ant_(1.625, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_3")
    Join("Ant_2", "Ant_3")
    Ant_(-1.625, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_4")
    Join("Ant_3", "Ant_4")
    Ant_(1.125, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_5")
    Join("Ant_4", "Ant_5")
    Ant_(-1.125, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_6")
    Join("Ant_5", "Ant_6")
    Ant_(0.625, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_7")
    Join("Ant_6", "Ant_7")
    Ant_(-0.625, 0, 9.5, 0.03, 0.03, 1.5)
    Name("Ant_8")
    Join("Ant_7", "Ant_8")

    StrucTub(0, 0, 5, 0.05, 0.05, 5)
    Name("StrucTub")
    Join("Ant_8", "StrucTub")
    Ant_Princ(0, 0, 9.5, 0.05, 0.05, 2.5)
    Name("Ant_Princ")
    Join("StrucTub", "Ant_Princ")
    Name("An_Sec")


##################################################################################################################

print(
    "-----------------------------------------------Tree_scout()----------------------------------------------"
)


def Chanfrein(c):
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.subdivide(
        number_cuts=10,
        smoothness=0,
        quadtri=False,
        quadcorner="STRAIGHT_CUT",
        fractal=0,
        fractal_along_normal=0,
        seed=0,
    )
    # Bevel pas encore au point
    bpy.ops.object.modifier_add(type="BEVEL")
    bpy.context.object.modifiers["Bevel"].limit_method = "ANGLE"
    bpy.context.object.modifiers["Bevel"].angle_limit = 0.391986
    bpy.ops.mesh.vertices_smooth(repeat=2, xaxis=True, yaxis=True, zaxis=True)
    bpy.context.object.modifiers["Bevel"].width = c
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")


def Fondation(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Chanfrein(1)


def Cab(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Chanfrein(1)


def Cabine():
    Cab(0, 0, 14, 0.8, 1.1, 0.05)
    Name("Ca1")
    Cab(0.75, 0, 14.7, 0.05, 1.1, 0.7)
    Name("Ca2")
    Join("Ca1", "Ca2")
    Cab(-0.75, 0, 14.7, 0.05, 1.1, 0.7)
    Name("Ca3")
    Join("Ca2", "Ca3")
    Cab(0, 1.1, 14.7, 0.75, 0.05, 0.3)
    Name("Ca4")
    Join("Ca3", "Ca4")
    Cab(0, -1.1, 14.7, 0.75, 0.05, 0.3)
    Name("Ca5")
    Join("Ca4", "Ca5")
    Name("Cabine")


##################################################################################################################

print(
    "-----------------------------------------------Tree_scope()----------------------------------------------"
)


def Arete_sol():
    Boite5(0, 1.75, 0, 1.8, 0.05, 0.05)
    Name("Bwt1")
    Boite5(1.75, 0, 0, 0.05, 1.8, 0.05)
    Name("Bwt2")
    Join("Bwt1", "Bwt2")
    Boite5(0, -1.75, 0, 1.8, 0.05, 0.05)
    Name("Bwt3")
    Join("Bwt2", "Bwt3")
    Boite5(-1.75, 0, 0, 0.05, 1.8, 0.05)
    Name("Bwt4")
    Join("Bwt3", "Bwt4")
    Name("Arete_sol1")


def Poid_sol(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 2, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Hypoth(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 6, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Opp(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 3, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Contr(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 1.333, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Ball(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_uv_sphere_add(
        view_align=False, enter_editmode=False, location=(x, y, z)
    )
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Cone(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cone_add(
        radius1=1.5,
        radius2=0,
        depth=1.5,
        view_align=False,
        enter_editmode=False,
        location=(x, y, z),
    )
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Cam(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Obj(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 2, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Charp():
    Boite5(0, 0, 2.925, 0.03, 0.03, 3)
    Name("Bte5")
    Hypoth(0, -2.8, 4.8, 0.03, 0.03, 5.5)
    Name("Hyp")
    Join("Bte5", "Hyp")
    Opp(0, -2.65, 7.5, 0.03, 0.03, 3.1)
    Name("Opp")
    Join("Hyp", "Opp")
    Contr(0, -1.1, 4.8, 0.03, 0.03, 1.55)
    Name("Contr")
    Join("Opp", "Contr")
    Name("Charp")


##################################################################################################################

print(
    "-----------------------------------------------Tree_pass()----------------------------------------------"
)


def Pied(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Structure1():
    Boite3(0, 0.325, 1.65, 0.35, 0.025, 0.025)
    Name("Bt1")
    Boite3(0, -0.325, 1.65, 0.35, 0.025, 0.025)
    Name("Bt2")
    Join("Bt1", "Bt2")
    Boite3(0.325, 0, 1.65, 0.025, 0.35, 0.025)
    Name("Bt3")
    Join("Bt2", "Bt3")
    Boite3(-0.325, 0, 1.65, 0.025, 0.35, 0.025)
    Name("Bt4")
    Join("Bt3", "Bt4")


def Structure2():
    Boite3(0, 0.325, 3.65, 0.35, 0.025, 0.025)
    Name("Bt5")
    Boite3(0, -0.325, 3.65, 0.35, 0.025, 0.025)
    Name("Bt6")
    Join("Bt5", "Bt6")
    Boite3(0.325, 0, 3.65, 0.025, 0.35, 0.025)
    Name("Bt7")
    Join("Bt6", "Bt7")
    Boite3(-0.325, 0, 3.65, 0.025, 0.35, 0.025)
    Name("Bt8")
    Join("Bt7", "Bt8")


def Structure3():
    Boite3(0, 0.325, 5.65, 0.35, 0.025, 0.025)
    Name("Bt9")
    Boite3(0, -0.325, 5.65, 0.35, 0.025, 0.025)
    Name("Bt10")
    Join("Bt9", "Bt10")
    Boite3(0.325, 0, 5.65, 0.025, 0.35, 0.025)
    Name("Bt11")
    Join("Bt10", "Bt11")
    Boite3(-0.325, 0, 5.65, 0.025, 0.35, 0.025)
    Name("Bt12")
    Join("Bt11", "Bt12")


def Structure4():
    Boite3(0, 0.325, 7.65, 0.35, 0.025, 0.025)
    Name("Bt13")
    Boite3(0, -0.325, 7.65, 0.35, 0.025, 0.025)
    Name("Bt14")
    Join("Bt13", "Bt14")
    Boite3(0.325, 0, 7.65, 0.025, 0.35, 0.025)
    Name("Bt15")
    Join("Bt14", "Bt15")
    Boite3(-0.325, 0, 7.65, 0.025, 0.35, 0.025)
    Name("Bt16")
    Join("Bt15", "Bt16")


def Structure5():
    Boite3(0, 0.325, 9.65, 0.35, 0.025, 0.025)
    Name("Bt17")
    Boite3(0, -0.325, 9.65, 0.35, 0.025, 0.025)
    Name("Bt18")
    Join("Bt17", "Bt18")
    Boite3(0.325, 0, 9.65, 0.025, 0.35, 0.025)
    Name("Bt19")
    Join("Bt18", "Bt19")
    Boite3(-0.325, 0, 9.65, 0.025, 0.35, 0.025)
    Name("Bt20")
    Join("Bt19", "Bt20")


def Tub(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    Name("Tuuub")


def Pieds():
    Pied(1.8, 0, 0, 0.1, 0.1, 0.05)
    Name("Pie1")
    Pied(-1.8, 0, 0, 0.1, 0.1, 0.05)
    Name("Pie2")
    Join("Pie1", "Pie2")
    Pied(0, 1.8, 0, 0.1, 0.1, 0.05)
    Name("Pie3")
    Join("Pie2", "Pie3")
    Pied(0, -1.8, 0, 0.1, 0.1, 0.05)
    Name("Pie4")
    Join("Pie3", "Pie4")
    Boite3(1.8, 0, 0.35, 0.05, 0.05, 0.35)
    Name("Bt21")
    Join("Pie4", "Bt21")
    Boite3(-1.8, 0, 0.35, 0.05, 0.05, 0.35)
    Name("Bt22")
    Join("Bt21", "Bt22")
    Boite3(0, 1.8, 0.35, 0.05, 0.05, 0.35)
    Name("Bt23")
    Join("Bt22", "Bt23")
    Boite3(0, -1.8, 0.35, 0.05, 0.05, 0.35)
    Name("Bt24")
    Join("Bt23", "Bt24")
    Boite3(1.1, 0, 0.65, 0.75, 0.05, 0.05)
    Name("Bt25")
    Join("Bt24", "Bt25")
    Boite3(-1.1, 0, 0.65, 0.75, 0.05, 0.05)
    Name("Bt26")
    Join("Bt25", "Bt26")
    Boite3(0, 1.1, 0.65, 0.05, 0.75, 0.05)
    Name("Bt27")
    Join("Bt26", "Bt27")
    Boite3(0, -1.1, 0.65, 0.05, 0.75, 0.05)
    Name("Bt28")
    Join("Bt27", "Bt28")
    Boite3(0, 0, 0.65, 0.35, 0.35, 0.05)
    Name("Bt29")
    Join("Bt28", "Bt29")


def StrucVerNass():
    Boite3(0.325, 0.325, 5.6, 0.025, 0.025, 5)
    Name("Bt30")
    Boite3(-0.325, 0.325, 5.6, 0.025, 0.025, 5)
    Name("Bt31")
    Join("Bt30", "Bt31")
    Boite3(0.325, -0.325, 5.6, 0.025, 0.025, 5)
    Name("Bt32")
    Join("Bt31", "Bt32")
    Boite3(-0.325, -0.325, 5.6, 0.025, 0.025, 5)
    Name("Bt33")
    Join("Bt32", "Bt33")
    Structure1()
    Name("Strc1")
    Join("Bt33", "Strc1")
    Structure2()
    Name("Strc2")
    Join("Strc1", "Strc2")
    Structure3()
    Name("Strc3")
    Join("Strc2", "Strc3")
    Structure4()
    Name("Strc4")
    Join("Strc3", "Strc4")
    Structure5()
    Name("Strc5")
    Join("Strc4", "Strc5")
    Boite3(0, 0, 10.6, 0.9, 3.5, 0.05)
    Name("Bt34")
    Join("Strc5", "Bt34")
    Boite3(0.875, 3.475, 11.1, 0.025, 0.025, 0.5)
    Name("Bt35")
    Join("Bt34", "Bt35")
    Boite3(-0.875, 3.475, 11.1, 0.025, 0.025, 0.5)
    Name("Bt36")
    Join("Bt35", "Bt36")
    Boite3(0.875, -3.475, 11.1, 0.025, 0.025, 0.5)
    Name("Bt37")
    Join("Bt36", "Bt37")
    Boite3(-0.875, -3.475, 11.1, 0.025, 0.025, 0.5)
    Name("Bt38")
    Join("Bt37", "Bt38")
    Boite3(0.875, 0, 11.6, 0.025, 3.5, 0.025)
    Name("Bt39")
    Join("Bt38", "Bt39")
    Boite3(-0.875, 0, 11.6, 0.025, 3.5, 0.025)
    Name("Bt40")
    Join("Bt39", "Bt40")
    Boite3(0.875, 0, 11.6, 0.025, 3.5, 0.025)
    Name("Bt41")
    Join("Bt40", "Bt41")
    Boite3(0, 3.475, 11.6, 0.9, 0.025, 0.025)
    Name("Bt42")
    Join("Bt41", "Bt42")
    Boite3(0, -3.475, 11.6, 0.9, 0.025, 0.025)
    Name("Bt43")
    Join("Bt42", "Bt43")


##################################################################################################################

print(
    "-----------------------------------------------Tree_flower()----------------------------------------------"
)


def Cercle(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Tige(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Tube(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Pistil():
    Tube(0, 0, 10, 0.1, 0.1, 0.4)
    Name("Tub1")
    Tube(0, 0, 9.7, 0.12, 0.12, 0.4)
    Name("Tub2")
    Join("Tub1", "Tub2")
    Tube(0, 0, 9.2, 0.14, 0.14, 0.4)
    Name("Tub3")
    Join("Tub2", "Tub3")
    Tube(0, 0, 8.7, 0.16, 0.16, 0.4)
    Name("Tub4")
    Join("Tub3", "Tub4")
    Tube(0, 0, 8.2, 0.18, 0.18, 0.4)
    Name("Tub5")
    Join("Tub4", "Tub5")
    Tube(0, 0, 7.7, 0.2, 0.2, 0.4)
    Name("Tub6")
    Join("Tub5", "Tub6")
    Tube(0, 0, 7.2, 0.22, 0.22, 0.4)
    Name("Tub7")
    Join("Tub6", "Tub7")
    Tube(0, 0, 6.7, 0.24, 0.24, 0.4)
    Name("Tub8")
    Join("Tub7", "Tub8")
    Tube(0, 0, 6.2, 0.26, 0.26, 0.4)
    Name("Tub9")
    Join("Tub8", "Tub9")


##################################################################################################################

print(
    "-----------------------------------------------Tree_Psych()----------------------------------------------"
)


def Base2(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


def Branche(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 3, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Branche2(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 6, axis=(1, 0, 0), constraint_axis=(True, False, False)
    )


def Branche3(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 2.7, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Branche4(x, y, z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cylinder_add(location=(x, y, z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))
    bpy.ops.transform.rotate(
        value=math.pi / 1.3, axis=(0, 1, 0), constraint_axis=(False, True, False)
    )


def Parlor1():
    Conex(-0.6, 0, 9.25, 1.5, 1.5, 0.2, 0.2, 0.2, 2.3)
    Name("Parlor1")


def Parlor2():
    Conex(-0.1, 0, 10, 1.5, 1.5, 0.4, 0.4, 0.4, 1.2)
    Name("Parlor2")


##################################################################################################################

print(
    "-----------------------------------------------Tree_root()----------------------------------------------"
)


def Pal():
    Cylindre1(0, 0, 5, 0.05, 0.05, 5)
    Name("Cyl1")
    Cylindre(0.5, 0.025, 3, 0.05, 0.05, 3)
    Name("Cyl2")
    Join("Cyl1", "Cyl2")
    Cylindre1(0.025, 0.5, 3.5, 0.02, 0.02, 3.5)
    Name("Cyl3")
    Join("Cyl2", "Cyl3")
    Cylindre1(0.075, 0.075, 4.5, 0.06, 0.06, 4.5)
    Name("Cyl4")
    Join("Cyl3", "Cyl4")
    Cylindre1(0.6, 0.6, 3, 0.04, 0.04, 3)
    Name("Cyl5")
    Join("Cyl4", "Cyl5")
    Cylindre1(-0.8, 0.08, 4.5, 0.05, 0.05, 4.5)
    Name("Cyl6")
    Join("Cyl5", "Cyl6")
    Cylindre1(-0.4, -0.5, 3.75, 0.035, 0.035, 3.75)
    Name("Cyl7")
    Join("Cyl6", "Cyl7")
    Cylindre1(0, -0.3, 3, 0.02, 0.02, 3)
    Name("Cyl8")
    Join("Cyl7", "Cyl8")
    Cylindre1(-0.4, -0.35, 5.5, 0.05, 0.05, 5.5)
    Name("Cyl9")
    Join("Cyl8", "Cyl9")
    Cylindre1(-0.375, -0.25, 5, 0.08, 0.08, 3.5)
    Name("Cyl10")
    Join("Cyl9", "Cyl10")
    Cylindre1(-0.35, -0.25, 2.75, 0.1, 0.1, 2.75)
    Name("Cyl11")
    Join("Cyl10", "Cyl11")
    Cylindre1(-0.5, -0.5, 2, 0.15, 0.15, 2)
    Name("Cyl12")
    Join("Cyl11", "Cyl12")
    Cylindre1(-0.5, 0.5, 1, 0.2, 0.2, 1)
    Name("Cyl13")
    Join("Cyl12", "Cyl13")
    Cylindre1(0.3, 0.35, 1.5, 0.18, 0.18, 1.5)
    Name("Cyl14")
    Join("Cyl13", "Cyl14")


##################################################################################################################
# ---------------------------------------------------Fonctions Arbres----------------------------------------------
##################################################################################################################


def Tree_ant(x=0, y=0, z=0):
    Ant_Sec()
    Name("Ant_Sec")
    Socle(0, 0, 0, 1.8, 1.8, 0.1)
    Name("Socle")
    Join("Ant_Sec", "Socle")
    Name("Tree_ant")
    Place_obj("Tree_ant", x, y, z)


def Tree_scout(x=0, y=0, z=0):
    Fondation(0, 0, 0.6, 0.75, 0.75, 0.6)
    Name("Fond")
    Boite6(0, 0, 7, 0.05, 0.1, 7)
    Name("Bwit6")
    Join("Fond", "Bwit6")
    Cabine()
    Name("Cabine")
    Join("Bwit6", "Cabine")
    Boite6(0, 0, 0, 1.8, 1.8, 0.01)
    Name("Bwit7")
    Join("Cabine", "Bwit7")
    Name("Tree_scout")
    Place_obj("Tree_scout", x, y, z)


def Tree_scope(x=0, y=0, z=0):
    Arete_sol()
    Name("AretSol")
    Charp()
    Name("Charp")
    Join("AretSol", "Charp")
    Ball(0, -5.7, 10, 0.75, 0.75, 0.75)
    Name("Ball")
    Join("Charp", "Ball")
    Cone(0, -5.7, 9.165, 0.25, 0.25, 0.25)
    Name("Cone")
    Join("Ball", "Cone")
    Cam(0, -5.8, 8.95, 0.2, 0.35, 0.125)
    Name("Cam")
    Join("Cone", "Cam")
    Obj(0, -6.15, 8.95, 0.07, 0.07, 0.1)
    Name("Obj")
    Join("Cam", "Obj")
    Poid_sol(0, 0, 0, 0.1, 0.1, 1)
    Name("PoidSol")
    Join("Obj", "PoidSol")
    Name("Tree_scope")
    Place_obj("Tree_scope", x, y, z)


def Tree_pass(x=0, y=0, z=0):
    StrucVerNass()
    Name("StrVrNass")
    Tub(0, 0, 11.8, 0.5, 0.5, 1.25)
    Name("Tuub")
    Join("StrVrNass", "Tuub")
    Pieds()
    Name("Pieds")
    Join("Tuub", "Pieds")
    Name("Tree_pass")
    Place_obj("Tree_pass", x, y, z + 0.7)


def Tree_flower(x=0, y=0, z=0):
    Cercle(0, 0, 0.05, 1.6, 1.6, 0.1)
    Name("Cerc")
    Tige(0, 0, 5, 0.05, 0.05, 5)
    Name("Tig")
    Join("Cerc", "Tig")
    Pistil()
    Name("Pist")
    Join("Tig", "Pist")
    Boite2(0, 0, 0, 1.8, 1.8, 0.05)
    Name("Boit2")
    Join("Pist", "Boit2")
    Name("Tree_flower")
    Place_obj("Tree_flower", x, y, z)


def Tree_psych(x=0, y=0, z=0):
    Tube(0, 0, 5, 0.03, 0.03, 5)
    Name("Tube")
    Branche(0.1, 0, 7, 0.02, 0.02, 0.3)
    Name("Branch1")
    Join("Tube", "Branch1")
    Branche2(0, -0.2, 8, 0.02, 0.02, 0.5)
    Name("Branch2")
    Join("Branch1", "Branch2")
    Branche3(0.15, 0, 9.5, 0.02, 0.02, 0.2)
    Name("Branch3")
    Join("Branch2", "Branch3")
    Branche4(-0.25, 0, 9, 0.02, 0.02, 0.4)
    Name("Branch4")
    Join("Branch3", "Branch4")
    Parlor1()
    Name("Parlor1")
    Join("Branch4", "Parlor1")
    Parlor2()
    Name("Parlor2")
    Join("Parlor1", "Parlor2")
    Sphere(0.1, -0.55, 8.3, 0.12, 0.12, 0.12)
    Name("Spher1")
    Join("Parlor2", "Spher1")
    Sphere(-0.1, -0.5, 8.2, 0.05, 0.05, 0.05)
    Name("Spher2")
    Join("Spher1", "Spher2")
    Sphere(0.5, 0, 9.5, 0.2, 0.2, 0.2)
    Name("Spher3")
    Join("Spher2", "Spher3")
    Base2(0, 0, 0, 1.8, 1.8, 0.05)
    Name("Base")
    Join("Spher3", "Base")
    Name("Tree_psych")
    Place_obj("Tree_psych", x, y, z)


def Tree_root(x=0, y=0, z=0):
    Pal()
    Name("Pal")
    Boite4(0, 0, 0, 1.8, 1.8, 0.05)
    Name("Boite4")
    Join("Pal", "Boite4")
    Name("Tree_root")
    Place_obj("Tree_root", x, y, z)


###############################################################

###############################################################

# LISTE ARBRES


L_TREE = [
    Tree_ant,
    Tree_scout,
    Tree_scope,
    Tree_pass,
    Tree_flower,
    Tree_psych,
    Tree_root,
]


def MIX_TREE(x=0, y=0, z=0, num=0):

    NB = random.randint(0, 6)
    L_TREE[NB]()
    Name(name_tree(num))
    Place_obj(name_tree(num), x, y, z)


##############################################################################################################################


def PLACE_TREE(board_to_redraw):
    i = 0
    while i < len(board_to_redraw):
        if board[i] == ".":
            if int(board_to_redraw[i]) == 0:

                select_cube(name_cube(i))
                bpy.ops.view3d.snap_cursor_to_selected()

                MIX_TREE(x=0, y=0, z=0)
                bpy.ops.view3d.snap_selected_to_cursor(use_offset == False)


##
# Parois


def Cloison(o, p, q, r, s, t):
    bpy.ops.mesh.primitive_cube_add(location=(o, p, q))
    bpy.ops.transform.resize(value=(r, s, t))
    bpy.ops.transform.rotate(
        value=math.pi / 4, axis=(0, 0, 1), constraint_axis=(False, False, True)
    )


def Paroi(x=0, y=0, z=0):
    Cloison(x, y, z + 1.8, 2.55, 0.05, 1.8)
    Name("Paroi0")


# Paroi(0,0,0)

####################################################################################################################################
####################################################################################################################################
# GAME OF LIFE-------------------GAME OF LIFE-------------------GAME OF LIFE-------------------GAME OF LIFE--------------------------
####################################################################################################################################
####################################################################################################################################

# ---------------------------------------------------------------------------
#                       0/ VALEUR GAME OF LIFE
# ---------------------------------------------------------------------------

# board = []
# keyframe = 0
# move_length = 5


# ---------------------------------------------------------------------------
#                       1/ PARCELLE GAME OF LIFE
# ---------------------------------------------------------------------------
def create_board(board_width, board_height, size_cube):
    i = 0
    j = 1
    while i < board_height * board_width:
        if (
            j == 1
            or j == board_height
            or i % board_width == 0
            or i % board_width == board_width - 1
        ):
            bpy.ops.mesh.primitive_cube_add(
                radius=size_cube / 2000,
                location=((i % board_width) * size_cube, j * size_cube, 100),
            )
            # bpy.ops.transform.resize(value=(0.1,0.1,1.5))
            bpy.ops.mesh.primitive_cube_add(
                radius=size_cube / 2000,
                location=((i % board_width) * size_cube, j * size_cube, 0),
            )
            Name("CubeV")
        else:
            if random.randint(1, 3) == 1:
                MIX_CUB((i % board_width) * size_cube, j * size_cube, 0)
                bpy.ops.mesh.primitive_cube_add(
                    radius=size_cube / 2,
                    location=((i % board_width) * size_cube, j * size_cube, 100),
                )

            else:
                bpy.ops.mesh.primitive_cube_add(
                    radius=size_cube / 200,
                    location=((i % board_width) * size_cube, j * size_cube, 100),
                )
                MIX_CUB((i % board_width) * size_cube, j * size_cube, 0)
                bpy.ops.transform.resize(value=(1 / 100, 1 / 100, 1 / 100))
        if i % board_width == board_width - 1:
            j += 1
        i += 1


# -------------------------Nomination des Cubes-----------------------------------------


def name_tree(j):
    if j == 0:
        name_of_cube = "Tree"
    elif j < 10:
        name_of_cube = "Tree.00" + str(j)
    elif j < 100:
        name_of_cube = "Tree.0" + str(j)
    elif j < 1000:
        name_of_cube = "Tree." + str(j)
    return name_of_cube


def name_cube(j):
    if j == 0:
        name_of_cube = "Cube"
    elif j < 10:
        name_of_cube = "Cube.00" + str(j)
    elif j < 100:
        name_of_cube = "Cube.0" + str(j)
    elif j < 1000:
        name_of_cube = "Cube." + str(j)
    return name_of_cube


def name_cubeV(j):
    if j == 0:
        name_of_cube = "CubeV"
    elif j < 10:
        name_of_cube = "CubeV.00" + str(j)
    elif j < 100:
        name_of_cube = "CubeV.0" + str(j)
    elif j < 1000:
        name_of_cube = "CubeV." + str(j)
    return name_of_cube


# -------------------------Selection des Cubes-----------------------------------------


def select_cube(cube_name):
    bpy.ops.object.select_all(action="DESELECT")
    bpy.ops.object.select_pattern(pattern=cube_name)


# -------------------------Retrecissement-----------------------------------------


def shrink(number_to_shrink):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_shrink))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cube(number_to_shrink))
    scene.objects.active = bpy.data.objects[name_cube(number_to_shrink)]
    bpy.ops.transform.resize(value=(0.01, 0.01, 0.01))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def shrinkV(number_to_shrink):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_shrink))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cubeV(number_to_shrink))
    scene.objects.active = bpy.data.objects[name_cubeV(number_to_shrink)]
    bpy.ops.transform.resize(value=(0.01, 0.01, 0.01))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def shrinkT(number_to_shrink):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_shrink))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cube(number_to_shrink))
    scene.objects.active = bpy.data.objects[name_cube(number_to_shrink)]
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def shrinkTV(number_to_shrink):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_shrink))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cubeV(number_to_shrink))
    scene.objects.active = bpy.data.objects[name_cubeV(number_to_shrink)]
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


# -------------------------Agrandissement-----------------------------------------


def grow(number_to_grow):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_grow))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cube(number_to_grow))
    scene.objects.active = bpy.data.objects[name_cube(number_to_grow)]
    bpy.ops.transform.resize(value=(100, 100, 100))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def growV(number_to_grow):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_grow))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cubeV(number_to_grow))
    scene.objects.active = bpy.data.objects[name_cubeV(number_to_grow)]
    bpy.ops.transform.resize(value=(100, 100, 100))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def growT(number_to_grow):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_grow))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cube(number_to_grow))
    scene.objects.active = bpy.data.objects[name_cube(number_to_grow)]
    bpy.ops.transform.resize(value=(2, 2, 2))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


def growTV(number_to_grow):
    # bpy.context.scene.frame_current = move_number * move_length
    # select_cube(name_cube(number_to_grow))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = (move_number + 1) * move_length
    select_cube(name_cubeV(number_to_grow))
    scene.objects.active = bpy.data.objects[name_cubeV(number_to_grow)]
    bpy.ops.transform.resize(value=(2, 2, 2))
    # bpy.ops.anim.keyframe_insert_menu(type = "Scaling")
    # bpy.context.scene.frame_current = move_number


# -------------------------Verification des voisins-----------------------------------------

scene = bpy.context.scene

###


def neighboursNB(num, width, size):

    total = (
        count(num + 1, size)
        + count(num - 1, size)
        + count(num + width, size)
        + count(num - width, size)
        + count(num - width + 1, size)
        + count(num - width - 1, size)
        + count(num + width - 1, size)
        + count(num + width + 1, size)
    )

    return total


###


def neighboursNBT(num, width, size):

    total = (
        count(num + 1, size)
        + count(num - 1, size)
        + count(num + width, size)
        + count(num - width, size)
        + count(num - width + 1, size)
        + count(num - width - 1, size)
        + count(num + width - 1, size)
        + count(num + width + 1, size)
        + count_Tree(num + 1, size)
        + count_Tree(num - 1, size)
        + count_Tree(num + width, size)
        + count_Tree(num - width, size)
        + count_Tree(num - width + 1, size)
        + count_Tree(num - width - 1, size)
        + count_Tree(num + width - 1, size)
        + count_Tree(num + width + 1, size)
    )
    return total


###


def count(j, size):

    scene.objects.active = bpy.data.objects[name_cube(j)]
    if bpy.context.active_object.dimensions == Vector([size, size, size]):
        return 1
    else:
        return 0


###


def count_Tree(j, size):

    scene.objects.active = bpy.data.objects[name_cube(j)]
    if bpy.context.active_object.dimensions == Vector(
        [size / 200, size / 200, size / 200]
    ):
        return 1
    else:
        return 0


###


def gameOfLifeWoodstock(board_width, board_height, size):

    i = 0

    while i < (board_width * board_height):

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector([size, size, size]):

            if (neighboursNBT(i, board_width, size) < 2) or (
                neighboursNBT(i, board_width, size) > 3
            ):

                shrink(i)
                shrinkV(i)

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector(
            [size / 100, size / 100, size / 100]
        ):

            if neighboursNBT(i, board_width, size) == 3:

                grow(i)
                growV(i)

        i = i + 1


###


def gameOfLifeWallStreet(board_width, board_height, size):

    i = 0

    while i < (board_width * board_height):

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector([size, size, size]):

            if (neighboursNB(i, board_width, size) < 2) or (
                neighboursNB(i, board_width, size) > 4
            ):

                shrink(i)
                shrinkV(i)

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector(
            [size / 100, size / 100, size / 100]
        ):

            if neighboursNB(i, board_width, size) == 3:

                grow(i)
                growV(i)

                # Passerelle
                scene.objects.active = bpy.data.objects[name_cube(i)]
                tempo = bpy.context.active_object.location
                EscPass6(x=0, y=0, z=0)
                bpy.context.active_object.location.x = tempo.x
                bpy.context.active_object.location.y = tempo.y

        i = i + 1


###


def makeTrees(board_width, board_height, size):

    i = 0

    while i < (board_width * board_height):

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector(
            [size / 100, size / 100, size / 100]
        ):

            if neighboursNB(i, board_width, size) < 2:
                shrinkTV(i)
                shrinkT(i)
                scene.objects.active = bpy.data.objects[name_cube(i)]
                tmp = scene.objects.active.location
                MIX_TREE(tmp[0], tmp[1], tmp[2] - 100, i)

        i = i + 1


###


def destroyTrees(board_width, board_height, size):

    i = 0

    while i < (board_width * board_height):

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector(
            [size / 200, size / 200, size / 200]
        ):

            print("CACA")
            print(i)

            if neighboursNBT(i, board_width, size) > 3:
                growTV(i)
                growT(i)
                bpy.ops.object.select_all(action="DESELECT")
                scene.objects[name_tree(i)].select = True
                bpy.ops.object.delete(use_global=False)

                # Remplacement par une paroie

                scene.objects.active = bpy.data.objects[name_cube(i)]
                Paroi(
                    bpy.context.active_object.location.x,
                    bpy.context.active_object.location.y,
                    0,
                )

        i = i + 1


###


def deadTrees(board_width, board_height, size):

    i = 0

    while i < (board_width * board_height):

        scene.objects.active = bpy.data.objects[name_cube(i)]
        if bpy.context.active_object.dimensions == Vector(
            [size / 200, size / 200, size / 200]
        ):

            if neighboursNBT(i, board_width, size) > 6:
                growTV(i)
                growT(i)
                bpy.ops.object.select_all(action="DESELECT")
                scene.objects[name_tree(i)].select = True
                bpy.ops.object.delete(use_global=False)

        i = i + 1


###############################################################
# Price_Generator
###############################################################


def price_generator(board_width=15, board_height=15, size=3.6, NbMoves=5):

    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)

    create_board(board_width, board_height, size)
    move_number = 0

    while move_number < NbMoves:
        bpy.context.scene.frame_current = move_number

        if move_number % 2 == 0:

            makeTrees(board_width, board_height, size)
            gameOfLifeWoodstock(board_width, board_height, size)

        else:

            destroyTrees(board_width, board_height, size)
            gameOfLifeWallStreet(board_width, board_height, size)

        deadTrees(board_width, board_height, size)

        move_number += 1


##################################################################

##################################################################

price_generator(30, 20, 3.6, 1)

# makeTrees(15, 15, 3.6)
# gameOfLifeWoodstock(15, 15, 3.6)
# destroyTrees(15, 15, 3.6)
# gameOfLifeWallStreet(15, 15, 3.6)
# deadTrees(15,15,3.6)
