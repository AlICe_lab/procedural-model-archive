# SPDX-FileCopyrightText: 2021 Oriel Malka & Sascha Ciudad Hidalgo
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sript ecrit par sascha et oriel pour AIM: johanna beyer, suite for clariette n2.


import bpy
import mathutils
import random
import math

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()

# cube avec une base
variation = random.uniform(0, 1)  ##variation pour la translation des cube dans l'axe X


def Cube(position, taille):
    bpy.ops.mesh.primitive_cube_add(size=1, location=(position))
    # bpy.ops.mesh.primitive_cylinder_add(vertices=1.5, radius=0.5, depth=0.05, location=position, scale=taille)

    bpy.ops.object.editmode_toggle()
    x, y, z = taille
    bpy.ops.transform.translate(
        value=(variation, 0, z / 2)
    )  # translation des cubes + variation dans l'axe X
    bpy.ops.transform.resize(value=(taille))  # evolution des tailles
    bpy.ops.object.editmode_toggle()
    bpy.ops.transform.rotate()


for i in range(0, 10):  # premiere boucle volume du bas

    for j in range(0, 10):
        value = 1  # valeur indicative pour les rotations
        x = 0.5 + i * 1.6  # espacement entre les rangees direction X
        y = 1 + j * 1.6  # espacement entre les rangees direction y
        z = 0  # position sur l'axe Z de la grille

        Cube(
            (x, y, z),
            (
                1.6,
                1.6,
                random.randint(1, 2) / 3.4
                + i**2 / 3**2 * random.randint(1, 2) / 3.4
                + j**2 / 3**2,
            ),
        )  # formule de la parabole hyperbolique, changement de la courbure
        bpy.ops.transform.rotate(
            value=random.uniform(0, 1),
            orient_axis="Z",
            orient_type="VIEW",
            orient_matrix=(
                (0.387515, -0.921863, -8.67992e-07),
                (0.121921, 0.05125, 0.991216),
                (-0.913766, -0.384111, 0.132255),
            ),
            orient_matrix_type="VIEW",
            mirror=True,
            use_proportional_edit=True,
            proportional_edit_falloff="SMOOTH",
            proportional_size=1,
            use_proportional_connected=False,
            use_proportional_projected=False,
        )

        bpy.ops.object.modifier_add(type="SIMPLE_DEFORM")  # deformation/torsion
        bpy.context.object.modifiers["SimpleDeform"].deform_axis = (
            "Z"  # deformation/torsion
        )
        bpy.context.object.modifiers["SimpleDeform"].angle = random.randint(
            -1, 1
        )  # deformation/torsion


for i in range(0, 10):  # deuxieme boucle volume du haut
    for j in range(0, 10):
        value = 1  # valeur indicative pour les rotations
        x = 0.5 - i * 1.6 + z  # suivre la position du volume principale en X
        y = 0.5 - j * 1.6 + z  # suivre la position du volume principale en y
        z = 14.45  # suivre la position du volume principale en Z

        Cube(
            (x, y, z),
            (
                1.6,
                1.6,
                random.randint(1, 2) / 3.4
                - i**2 / 3**2 * random.randint(1, 2) / 3.4
                - j**2 / 3**2,
            ),
        )  # formule de la parabole hyperbolique, changement de la courbure
        bpy.ops.transform.rotate(
            value=random.uniform(0, 1),
            orient_axis="Z",
            orient_type="VIEW",
            orient_matrix=(
                (0.5, 2, -8.67992e-07),
                (0.121921, 0.05125, 0.991216),
                (-0.913766, -0.384111, 0.132255),
            ),
            orient_matrix_type="VIEW",
            mirror=True,
            use_proportional_edit=True,
            proportional_edit_falloff="SMOOTH",
            proportional_size=2,
            use_proportional_connected=False,
            use_proportional_projected=False,
        )  # rotation sur l'axe Z
        bpy.ops.object.modifier_add(type="SIMPLE_DEFORM")  # deformation/torsion
        bpy.context.object.modifiers["SimpleDeform"].deform_axis = (
            "Z"  # deformation/torsion
        )
        bpy.context.object.modifiers["SimpleDeform"].angle = random.randint(
            -1, 1
        )  # deformation/torsion
