# SPDX-FileCopyrightText: 2014 Odair Nelson Rodrigues
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

print(" ")
print(" ")
print(" ******************************************************************************")
print(" ")
print(
    "   Reinterpretation of Hermann Hertzeberger, Centraal Beheer. "
)  # By Rodrigues Fortes Odair Nelson - Ma 1 - AlIce Lab - Ulb - 2014-2015
print(" ")
print(" ******************************************************************************")

#   Professeurs     :   Denis Derycke, Michel Lefèvre
#   Site internet   :   https://alicelab.be/
#   Developped in   :   Blender 2.72b
#   Platform        :   Windows

#   Email           :   fortes_odair@hotmail.com

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#       Informations:
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#   The model is generated so that every floor is on a seperate layer. So Layer 0-9 are for the Floors (meaning that everything above the tenth floor is not layered.)
#   Befor you run the script make sure that the System Console is switched on otherwise Blender will stop working.
#   It is preferable to choose only odd numbers to have the 4 cubes in the center like Hermann Herzeberger did, nevertheless it will still work.
#   The maximum Height (second number you put in the console) will highly depend on the number first number (that number is the lenght for the building) you will put in.
#   As result of that the number you give is the maximum height it will reach.
#   It may take some time, because the model is created as whole and afterwards it selects and deletes the objects whil encountering conditions.
#
#   Pls send me a mail for any furhter questions or informations that might help getting better results.


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#       CODE:
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# _______________________________________________________________________________________________________________________
#
# Import from Python
# _______________________________________________________________________________________________________________________
#

import bpy
import math
import random
from random import choice
import time
import sys

# _______________________________________________________________________________________________________________________
#
# Print Input Informations
# _______________________________________________________________________________________________________________________
#


# Information

print(" ")
print("-" * 78)
print(" ")
name = input(" Your Name :  ")
print(" ")
print("-" * 78)
print("-" * 78)
print(" ")
print(" Hello, " + name + " nice to meet you.")
print(" ")
print("-" * 78)
print(" ")
print(" -----------------------------------------------------------------------------")
print()
Number1 = int(
    input(
        "   Tell me %s, what number is on your mind,(at least 4, 0 is Random): " % name
    )
)
if Number1 == 0:
    Number1 = random.randrange(4, 20, 2)
print(" ")
print(" -----------------------------------------------------------------------------")
print(" ")
print(" -----------------------------------------------------------------------------")
print(" ")
Height1 = int(input("   And a second one : "))
if Height1 == 0:
    Height1 = random.randrange(4, 18, 2)
print()
print(" -----------------------------------------------------------------------------")
print(" ")
print(" ")
print(" _____________________________________________________________________________")
print()
print(
    "    The Network to create the step Pyramid in X:",
    Number1,
    ", in Y:",
    Number1,
    "and in Z:",
    Height1,
    ".",
)
print(" _____________________________________________________________________________")
print(" ")
print(" ")


# _______________________________________________________________________________________________________________________
#
# Layers
# _______________________________________________________________________________________________________________________
#

"""From http://blenderscripting.blogspot.com/2011/05/blender-25-python-selecting-layer.html"""


def selectLayer(layer_nr):
    return tuple(i == layer_nr for i in range(0, 20))


# _______________________________________________________________________________________________________________________
#
# Cleaning of the Model
# _______________________________________________________________________________________________________________________
#


def Cleanit():
    bpy.ops.object.select_all(action="SELECT")
    bpy.data.objects["BodyModel"].select = False
    bpy.data.objects["RoofModel"].select = False
    bpy.context.scene.layers[0] = True
    bpy.context.scene.layers[1] = True
    bpy.context.scene.layers[2] = True
    bpy.context.scene.layers[3] = True
    bpy.context.scene.layers[4] = True
    bpy.context.scene.layers[5] = True
    bpy.context.scene.layers[6] = True
    bpy.context.scene.layers[7] = True
    bpy.context.scene.layers[8] = True
    bpy.context.scene.layers[9] = True
    bpy.context.scene.layers[10] = True
    bpy.context.scene.layers[11] = True
    bpy.context.scene.layers[12] = True
    bpy.context.scene.layers[13] = True
    bpy.context.scene.layers[14] = True
    bpy.ops.object.delete(use_global=False)


Cleanit()

# _______________________________________________________________________________________________________________________
#
# Scene
# _______________________________________________________________________________________________________________________
#

scene = bpy.context.scene
scn = bpy.context.scene

# _______________________________________________________________________________________________________________________
#
# Number of Modules in the Lenght, Width and Height
# _______________________________________________________________________________________________________________________
#


Number = Number1

Height = Height1

# Number = random.randrange(4,18,2)
# Height = random.randint(4,10)

while Height > Number:
    Height = Height - 2

# _______________________________________________________________________________________________________________________
#
# The Module
# _______________________________________________________________________________________________________________________
#


def Module1(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, name):
    bpy.ops.object.add_named(linked=False, name="Bodymodel")


def Module(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, name):
    Vertices = [
        (-dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, -dim_Y / 2, 0),
        (dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, dim_Y / 2, 0),
        (-dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, -dim_Y / 2, dim_Z),
        (dim_X / 2, dim_Y / 2, dim_Z),
        (-dim_X / 2, dim_Y / 2, dim_Z),
    ]

    Faces = [
        (3, 2, 1, 0),
        (4, 5, 6, 7),
        (1, 5, 4, 0),
        (3, 7, 6, 2),
        (0, 4, 7, 3),
        (2, 6, 5, 1),
    ]

    Mesh = bpy.data.meshes.new(name + "ModuleMesh")
    Object = bpy.data.objects.new(name, Mesh)
    Scene = bpy.context.scene
    Scene.objects.link(Object)
    Mesh.from_pydata(Vertices, [], Faces)
    Object.location = (pos_X, pos_Y, pos_Z)


# _______________________________________________________________________________________________________________________
#
# Dimension of the Module
# _______________________________________________________________________________________________________________________
#

""" X for Lenght , Y for Width and Z for Height """

dim_ModX = 0.99
dim_ModY = 0.99
dim_ModZ = 0.33

# _______________________________________________________________________________________________________________________
#
# Dimensions of the Corridor
# _______________________________________________________________________________________________________________________
#

""" L for Lenght , W for Width and H for Height  """

LCorridor = 0.3358
WCorridor = 0.21
HCorridor = 0.33

# _______________________________________________________________________________________________________________________
#
# Dimensions of the Roof
# _______________________________________________________________________________________________________________________
#

""" L for Lenght , W for Width and H for Height  """
# Roof number 1

LRoof = 0.99
WRoof = 0.99
HRoof = 0.18

# Roof number 2

LRoof2 = 0.90
WRoof2 = 0.90
HRoof2 = 0.1090


# _______________________________________________________________________________________________________________________
#
# Loop for the Layers
# _______________________________________________________________________________________________________________________
#

# To form a square on the bottom:

resX = Number
resY = resX
resZ = Height


# To form a the pyramid going to the top:

for k in range(0, resZ):
    if k >= 2:
        resX -= 2
        resY -= 2
    for j in range(0, resY):
        for i in range(0, resX):
            if k == Height - 1:
                Module(
                    -((resX - 1) * (LCorridor + dim_ModX) / 2)
                    + (i * (LCorridor + dim_ModX)),
                    -((resY - 1) * (LCorridor + dim_ModY) / 2)
                    + (j * (LCorridor + dim_ModY)),
                    k * (dim_ModZ),
                    LRoof,
                    WRoof,
                    HRoof,
                    "Roof in x: " + str(i) + ", in y: " + str(j) + ", in z: " + str(k),
                )
            else:
                Module(
                    -((resX - 1) * (LCorridor + dim_ModX) / 2)
                    + (i * (LCorridor + dim_ModX)),
                    -((resY - 1) * (LCorridor + dim_ModY) / 2)
                    + (j * (LCorridor + dim_ModY)),
                    k * (dim_ModZ),
                    dim_ModX,
                    dim_ModY,
                    dim_ModZ,
                    "Kubus in x " + str(i) + ", in y: " + str(j) + ", in z: " + str(k),
                )
                if i < resX - 1:
                    Module(
                        -((resX - 1) * (LCorridor + dim_ModX) / 2)
                        + (i * (LCorridor + dim_ModX))
                        + dim_ModX / 2
                        + LCorridor / 2,
                        -((resY - 1) * (LCorridor + dim_ModY) / 2)
                        + (j * (LCorridor + dim_ModY)),
                        k * (dim_ModZ),
                        LCorridor,
                        WCorridor,
                        HCorridor,
                        "Corridor in x: "
                        + str(i)
                        + ", in: y "
                        + str(j)
                        + ", in z: "
                        + str(k),
                    )
                if j < resY - 1:
                    Module(
                        -((resX - 1) * (LCorridor + dim_ModX) / 2)
                        + (i * (LCorridor + dim_ModX)),
                        -((resY - 1) * (LCorridor + dim_ModY) / 2)
                        + (j * (LCorridor + dim_ModY))
                        + dim_ModX / 2
                        + LCorridor / 2,
                        k * (dim_ModZ),
                        WCorridor,
                        LCorridor,
                        HCorridor,
                        "Corridor in x: "
                        + str(i)
                        + ", in: y "
                        + str(j)
                        + ", in z: "
                        + str(k),
                    )

# _______________________________________________________________________________________________________________________
#
# Grouping and Layering Elements
# _______________________________________________________________________________________________________________________
#


# The Corridors:

for ob in scene.objects:
    if ob.name.startswith("Corridor"):
        ob.select = True

    else:
        ob.select = False
bpy.ops.group.create(name="Corridors")
bpy.ops.object.move_to_layer(layers=(selectLayer(12)))

bpy.ops.object.select_all(action="DESELECT")


# The Cubes:

for ob in scene.objects:
    if ob.name.startswith("Kubus"):
        ob.select = True
    else:
        ob.select = False

bpy.ops.group.create(name="Cubes")
bpy.ops.object.move_to_layer(layers=(selectLayer(11)))

bpy.ops.object.select_all(action="DESELECT")


# The Roofs:

for ob in scene.objects:
    if ob.name.startswith("Roof"):
        ob.select = True
    else:
        ob.select = False

bpy.ops.group.create(name="Roofs")
bpy.ops.object.move_to_layer(layers=(selectLayer(10)))


# First Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 0.00) < 0.01  # < 0.01 is the tolerance value

bpy.ops.object.move_to_layer(layers=(selectLayer(0)))

# Second Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 0.33) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(1)))

# Third Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 0.66) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(2)))

# Forth Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 0.99) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(3)))

# Fifth Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 1.32) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(4)))

# Sixt Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 1.65) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(5)))

# Seventh Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 1.98) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(6)))

# Eigth Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 2.31) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(7)))

# Ninth Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 2.64) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(8)))

# Tenth Floor

for obj in bpy.context.scene.objects:
    obj.select = abs(obj.location[2] - 2.97) < 0.01

bpy.ops.object.move_to_layer(layers=(selectLayer(9)))


# _______________________________________________________________________________________________________________________
#
# Delete Randomly the Corridors and Kubus
# _______________________________________________________________________________________________________________________
#


# Counting the Modules Created

SelKubus = []
for ob in scene.objects:
    if ob.type == "MESH" and ob.name.startswith("Kubus"):
        SelKubus.append(ob)
        random.choice(SelKubus).select = True
    else:
        ob.select = False

NumberofModulesCreated = len(bpy.context.selected_objects)


# The Modules on the ground floor 1/6 is deleted

for ob in scn.objects:
    ob.select = False

Kubus = [
    ob
    for ob in scn.objects
    if ob.name.startswith("Kubus") and abs(ob.location[2] - 0.00) < 0.01
]
Corridors = [ob for ob in scn.objects if ob.name.startswith("Corridor")]

Rounded = round(
    (Number1**2) * (1 / 6)
)  # 1/6 Because of Hermann Herzberger deleted modules, delete it if you think he went out from a square or rectangle

for i in range(Rounded):
    Ku = random.choice(Kubus)
    Ku.select = True

    for ob in Corridors:
        delta = (ob.location - Ku.location).length
        sel = abs(delta - 0.6629) < 0.01
        if sel and ob.select == False:
            ob.select = sel

sel = bpy.context.selected_objects
for act in sel:
    for obj in bpy.context.scene.objects:
        if obj.select == False:
            loc = obj.location
            obj.select = (
                loc[2] > act.location[2]
                and round(loc[0], 4) == round(act.location[0], 4)
                and round(loc[1], 4) == round(act.location[1], 4)
            )

bpy.ops.object.delete()

# The Modules after Groundfloor random

for ob in scn.objects:
    ob.select = False

Kubus = [
    ob
    for ob in scn.objects
    if ob.name.startswith("Kubus") and abs(ob.location[2] - 0.33) < 0.01
]
Corridors = [ob for ob in scn.objects if ob.name.startswith("Corridor")]

Randomly = random.randint(50, 100)

for i in range(Randomly):
    Ku = random.choice(Kubus)
    Ku.select = True

    for ob in Corridors:
        delta = (ob.location - Ku.location).length
        sel = abs(delta - 0.6629) < 0.01
        if sel and ob.select == False:
            ob.select = sel

sel = bpy.context.selected_objects
for act in sel:
    for obj in bpy.context.scene.objects:
        if obj.select == False:
            loc = obj.location
            obj.select = (
                loc[2] > act.location[2]
                and round(loc[0], 4) == round(act.location[0], 4)
                and round(loc[1], 4) == round(act.location[1], 4)
            )

for obj in bpy.context.scene.objects:
    if Number1 <= 10:
        bpy.ops.object.select_all(action="TOGGLE")

bpy.ops.object.delete()


# The Corridors

SelCorrid = []
for ob in scene.objects:
    if ob.type == "MESH" and ob.name.startswith("Corridor"):
        SelCorrid.append(ob)
        random.choice(SelCorrid).select = True
    else:
        ob.select = False

sel = bpy.context.selected_objects
for act in sel:
    for obj in bpy.context.scene.objects:
        if obj.select == False:
            loc = obj.location
            obj.select = (
                loc[2] > act.location[2]
                and round(loc[0], 4) == round(act.location[0], 4)
                and round(loc[1], 4) == round(act.location[1], 4)
            )

NumberofCorridorsdeleted = len(bpy.context.selected_objects)

bpy.ops.object.delete()


# Counting the remaining Corridors

SelCorrid2 = []
for ob in scene.objects:
    if ob.type == "MESH" and ob.name.startswith("Corridor"):
        SelCorrid2.append(ob)
        random.choice(SelCorrid2).select = True
    else:
        ob.select = False

NumberofremaingingCorridors = len(bpy.context.selected_objects)

bpy.ops.object.select_all(action="TOGGLE")


# If no Corridors around it will be deleted.

for ob in scn.objects:
    ob.select = False
Kubus = [ob for ob in scn.objects if ob.name.startswith("Kubus")]
Corridors = [ob for ob in scn.objects if ob.name.startswith("Corridor")]
for Ku in Kubus:
    Ku.select = True
    for ob in Corridors:
        delta = (ob.location - Ku.location).length
        if abs(delta - 0.6629) < 0.01:
            Ku.select = False
            break

Fixed = len(bpy.context.selected_objects)

bpy.ops.object.delete()

# _______________________________________________________________________________________________________________________
#
# Camera
# _______________________________________________________________________________________________________________________
#


def Camera():
    bpy.ops.object.camera_add(
        view_align=True,
        enter_editmode=False,
        location=(10, -10, 10),
        rotation=(45, 0, 45),
        layers=(selectLayer(18)),
    )
    bpy.context.object.rotation_euler[0] = 1.0472
    bpy.context.object.rotation_euler[2] = 0.785398
    bpy.context.object.data.type = "ORTHO"
    bpy.context.object.data.ortho_scale = 38


Camera()

# _______________________________________________________________________________________________________________________
#
# Sun
# _______________________________________________________________________________________________________________________
#


def Sun():
    bpy.ops.object.lamp_add(
        type="SUN",
        radius=1,
        view_align=False,
        location=(10, -20, 20),
        layers=(selectLayer(18)),
    )
    bpy.context.object.rotation_euler[1] = 0.785398
    bpy.context.object.rotation_euler[0] = 0.785398
    bpy.context.object.data.energy = 3
    bpy.context.object.data.shadow_method = "RAY_SHADOW"
    bpy.context.object.data.shadow_color = (0.288, 0.288, 0.288)


Sun()

# _______________________________________________________________________________________________________________________
#
# Print Informations
# _______________________________________________________________________________________________________________________
#

print(" _____________________________________________________________________________ ")
print()
print(
    "    Number of Modules Created according to a normal step pyramid :",
    NumberofModulesCreated,
    ".",
)
print(" _____________________________________________________________________________ ")
print(" ")
print(" ")
print(" _____________________________________________________________________________")
print()
print(
    "    Number of Modules deleted :",
    (Rounded + Randomly + Fixed),
    ", Number of actual Modules:",
    NumberofModulesCreated - (Rounded + Randomly),
    ".",
)
print(" _____________________________________________________________________________")
print(" ")
print(" ")
print(" _____________________________________________________________________________ ")
print()
print(
    "    Number of Corridors deleted :",
    NumberofCorridorsdeleted,
    ", Number of actual Corridors:",
    NumberofremaingingCorridors,
    ".",
)
print(" _____________________________________________________________________________ ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" -----------------------------------------------------------------------------")
print()
Again = input("   Done. Again? 'Yes' to change Numbers or 'No' to quit to the Model:  ")
print()
print(" -----------------------------------------------------------------------------")

if Again.lower() == "yes":
    bpy.ops.text.run_script()

print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
print(" ")
