# SPDX-FileCopyrightText: 2016 Quincy Jones Deldaele
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Script jury final 19/01/16
# Ecrit sous blender hash e8299c8 - windows 10

import bpy
import random
import math
from math import *


bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


#################################################################################################
#  definition poutre
#################################################################################################


def poutre(position, dimension, nom):

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)
    bpy.context.object.name = nom
    print("pos=", position, "dim=", dimension, "nom=", nom)


l_colonne = []

#################################################################################################
#  Mise en place mur Poutre / Colonne (gauche)
#################################################################################################


def colonne(position, dimension, nom):

    if len(l_colonne) == 0:
        bpy.ops.mesh.primitive_cube_add(
            radius=0.5,
            location=(position[0], position[1], position[2] + dimension[2] / 2),
        )
        bpy.ops.transform.resize(value=dimension)
        bpy.context.object.name = nom
        # print('pos=',position,'dim=',dimension,'nom=',nom)

        l_colonne.append(dimension[2])

    else:

        precedent = l_colonne[-1]
        suivante = dimension[2]

        hauteur = max([precedent, suivante])

        bpy.ops.mesh.primitive_cube_add(
            radius=0.5, location=(position[0], position[1], position[2] + hauteur / 2)
        )
        bpy.ops.transform.resize(value=(dimension[0], dimension[1], hauteur))
        bpy.context.object.name = nom
        # print('pos=',position,'dim=',dimension,'nom=',nom)

        l_colonne.append(dimension[2])


nbCelY = random.randint(2, 10)

length_y_right = 0
for y in range(0, nbCelY):
    nbCelZ = random.randint(3, 11)
    if y == nbCelY - 1:
        HtLastCol = nbCelZ * 83
    colonne((0, 0 + y * 226 - 18.5, 0), (10, 10, nbCelZ * 83), "colonne")
    length_y_right += 0 + y * 226 - 18.5
    for z in range(0, nbCelZ):
        poutre((0, y * 226 + 94.5, z * 83 + 83), (10, 216, 10), "poutre")

print(l_colonne)

l_trame = []

for i in range(0, len(l_colonne)):

    l = []

    for y in range(i * 216, (i + 1) * 216):
        for z in range(1, l_colonne[i] + 1):

            if z % 83 == 0 and y % 27 == 0:
                l.append((0, y, z))

    l_trame.append(l)

print(l_trame[0])


y = len(l_colonne)
colonne((0, 0 + y * 226 - 18.5, 0), (10, 10, HtLastCol), "colonne")

#################################################################################################
#  Trame de droite poutre et collone ( décaler de 300 en X)
#################################################################################################

l_colonne = []

for y in range(0, nbCelY):
    nbCelZ = random.randint(3, 11)
    if y == nbCelY - 1:
        HtLastCol = nbCelZ * 83
    colonne((300, 0 + y * 226 - 18.5, 0), (10, 10, nbCelZ * 83), "colonne")

    for z in range(0, nbCelZ):
        poutre((300, y * 226 + 94.5, z * 83 + 83), (10, 216, 10), "poutre")


l_trame_droite = []

for i in range(0, len(l_colonne)):

    l = []

    for y in range(i * 216, (i + 1) * 216):
        for z in range(1, l_colonne[i] + 1):

            if z % 83 == 0 and y % 27 == 0:
                l.append((0, y, z))

    l_trame_droite.append(l)
y = len(l_colonne)
colonne((300, 0 + y * 226 - 18.5, 0), (10, 10, HtLastCol), "colonne")

#################################################################################################
#  modules / fonction copyPaste des différents module
#################################################################################################


def CopyPaste(name, newname):

    bpy.context.scene.objects.active = bpy.data.objects[name]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(newname, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


l_module = []
l_module_simple = [
    "module simple A",
    "module simple B",
    "module simple C",
    "module simple D",
    "module simple E",
    "module simple F",
    "module simple G",
    "module simple H",
    "module simple I",
]

l_module_double = [
    "module double B",
    "module double C",
    "module double D",
    "module double E",
    "module double F",
    "module double H",
]


l_module_base = [
    "module base A",
    "module base B",
    "module base C",
    "module base bas",
    "module base haut",
]


l_type_module = ["vide", "simple", "double"]

#################################################################################################
# boucle de modules par travé (Gauche)
#################################################################################################

l_type_module_gauche = []
for index in range(len(l_trame)):
    if index == 0:
        shift1 = 0
    else:
        shifl1 = 13.5
    shift = shift1 + 10 * index
    l_module_trame = []
    for coordonnees in l_trame[index]:
        moduleChoisi = random.choice(l_type_module)
        print("ce module sera", moduleChoisi)
        if moduleChoisi == "vide":
            print("rien")
        if moduleChoisi == "simple":
            moduleSimpleChoisi = random.choice(l_module_simple)
            l_module_trame.append(moduleChoisi)
            l_module.append(CopyPaste(moduleSimpleChoisi, "ModuleSimple"))
            x, y, z = coordonnees
            l_module[-1].location = [x, y + shift, z]
            l_module[-1].scale = [1, 1, 1]
            l_module[-1].rotation_euler = [radians(0), radians(0), radians(180)]
        if moduleChoisi == "double":
            moduleDoubleChoisi = random.choice(l_module_double)
            l_module_trame.append(moduleChoisi)
            l_module.append(CopyPaste(moduleDoubleChoisi, "ModuleDouble"))
            x, y, z = coordonnees
            l_module[-1].location = [x, y + shift, z]
            l_module[-1].scale = [1, 1, 1]
            l_module[-1].rotation_euler = [radians(0), radians(0), radians(180)]
            print(len("ModuleDouble"))
    l_type_module_gauche.append(l_module_trame)

##################################################################################################
# boucle de modules par travé (Droite)
##################################################################################################

l_type_module_droit = []
for index in range(len(l_trame_droite)):
    if index == 0:
        shift1 = 0
    else:
        shifl1 = 13.5
    shift = shift1 + 10 * index
    l_module_trame = []
    for coordonnees in l_trame_droite[index]:
        moduleChoisi = random.choice(l_type_module)
        print("ce module sera", moduleChoisi)
        if moduleChoisi == "vide":
            print("rien")
        if moduleChoisi == "simple":
            moduleSimpleChoisi = random.choice(l_module_simple)
            l_module_trame.append(moduleChoisi)
            l_module.append(CopyPaste(moduleSimpleChoisi, "ModuleSimple"))
            x, y, z = coordonnees
            l_module[-1].location = [x + 300, y + shift, z]
            l_module[-1].scale = [1, 1, 1]
            l_module[-1].rotation_euler = [radians(0), radians(0), radians(0)]
        if moduleChoisi == "double":
            moduleDoubleChoisi = random.choice(l_module_double)
            l_module_trame.append(moduleChoisi)
            l_module.append(CopyPaste(moduleDoubleChoisi, "ModuleDouble"))
            x, y, z = coordonnees
            l_module[-1].location = [x + 300, y + shift, z]
            l_module[-1].scale = [1, 1, 1]
            l_module[-1].rotation_euler = [radians(0), radians(0), radians(0)]

    l_type_module_droit.append(l_module_trame)

#################################################################################################
# print le nombre total de modules et de personnes par configuration
#################################################################################################

perso_par_module = 15
print(l_type_module_gauche)
nbr_totale_module = 0
nbr_total_perso = 0

for trame in l_type_module_gauche:
    numero_trame = l_type_module_gauche.index(trame)
    nbr_module_simple = trame.count("simple")
    nbr_module_double = trame.count("double")

    print(
        "dans la trame gauche ",
        numero_trame,
        " il y a ",
        nbr_module_simple,
        " modules simple",
    )
    print(
        "dans la trame gauche ",
        numero_trame,
        " il y a ",
        nbr_module_double,
        " modules double",
    )

    nbr_totale_module = nbr_totale_module + nbr_module_simple + nbr_module_double
    nbr_total_perso = (
        nbr_total_perso
        + nbr_module_simple * perso_par_module
        + nbr_module_double * 2 * perso_par_module
    )

for trame in l_type_module_droit:
    numero_trame = l_type_module_droit.index(trame)
    nbr_module_simple = trame.count("simple")
    nbr_module_double = trame.count("double")

    print(
        "dans la trame droite ",
        numero_trame,
        " il y a ",
        nbr_module_simple,
        " modules simple",
    )
    print(
        "dans la trame droite ",
        numero_trame,
        " il y a ",
        nbr_module_double,
        " modules double",
    )

    nbr_totale_module = nbr_totale_module + nbr_module_simple + nbr_module_double
    nbr_total_perso = (
        nbr_total_perso
        + nbr_module_simple * perso_par_module
        + nbr_module_double * 2 * perso_par_module
    )

print("nombre totale de modules =", nbr_totale_module)
print("nombre totale de personnes =", nbr_total_perso)
