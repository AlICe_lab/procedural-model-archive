# SPDX-FileCopyrightText: 2013 Krystian Polanski & Javi Garcia
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy  # bpy : est une bibliotheque d'opération #ops : opération que l'on souhaite dans la bibliot
import random

# -------------------------------------------------------------------------------------

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# -------------------------------------------------------------------------------------------------------
#                                            P L A N S
# -------------------------------------------------------------------------------------------------------

Plan = 0  # 0 - simple, 1 - medium
Store = 1  # 0 - without store, 1 - with store

# -----------------------------------------------------------------------------------------------------


def Doma(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_grid_add(
        location=(pos_X + dim_X / 2, pos_Y + dim_Y / 2, pos_Z)
    )
    bpy.context.object.dimensions = (1, 1, 0)


def Room(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X + dim_X / 2, pos_Y + dim_Y / 2, pos_Z + dim_Z / 2)
    )
    bpy.context.object.dimensions = (1, 1, 0.4)


def RoomHigh(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X + dim_X / 2, pos_Y + dim_Y / 2, pos_Z + dim_Z / 2)
    )
    bpy.context.object.dimensions = (1, 1, 0.4)


# DOMA
# -------------------------------------------------------------------------------------------------------

Doma_X = random.randint(6.0, 7.0)
Doma_Y = random.randint(11.0, 13.0)

if Plan == 0:
    Doma_Y = random.randint(8, 10)

if Doma_X > Doma_Y:
    Doma_X = Doma_X - 3
else:
    Doma_X = Doma_X


def DomaArch():
    for k in range(0, 1):
        for j in range(0, Doma_Y):
            for i in range(0, Doma_X):
                Doma(
                    i,
                    j,
                    k,
                    1,
                    1,
                    1,
                    "maChambreDsFonction x" + str(i) + "y" + str(j) + "z" + str(k),
                )


DomaArch()

# ---------------------
print("")
print("------------------------")
print("Reseau de boites :", Doma_X, "suivant X", Doma_Y, "suivant_Y")
print("------------------------")

# Store
Store_X = random.randint(6.0, 10.0)
Store_Y = random.randint(5.0, 7.0)
Store2_X = random.randint(4.0, 6.0)
Store2_Y = random.randint(0, 2)
if Store == 1:

    def Store1Arch():
        for sz in range(0, 1):
            for sy in range(Store_Y, Doma_Y):
                for sx in range(-Store_X, 0):
                    Doma(
                        sx,
                        sy,
                        sz,
                        1,
                        1,
                        1,
                        "maChambreDsFonction x"
                        + str(sx)
                        + "y"
                        + str(sy)
                        + "z"
                        + str(sz),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (float(1.5), 0.0, 0.0)
                    mat.specular_color = (200.0, 1.0, 100.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    Store1Arch()

    def Store2Arch():
        for s2z in range(0, 1):
            for s2y in range(Store2_Y, Store_Y):
                for s2x in range(-Store2_X, 0):
                    Doma(
                        s2x,
                        s2y,
                        s2z,
                        1,
                        1,
                        1,
                        "maChambreDsFonction x"
                        + str(s2x)
                        + "y"
                        + str(s2y)
                        + "z"
                        + str(s2z),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (float(1.0), 0.0, 5.0)
                    mat.specular_color = (200.0, 1.0, 100.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    Store2Arch()

# KATTE
# -------------------------------------------------------------------------------------------------------
# range from 4 to 5 squares
Katte_X = random.randint(Doma_X + 6, Doma_X + 8)
# range from 5 to 5 sq
Katte_Y = random.randint(Doma_Y - 1, Doma_Y)

#    _2
#  3|_|1
#    0

Katte_0 = Katte_Y - 6

if Plan == 0:
    Katte_X = random.randint(Doma_X + 4, Doma_X + 5)
    Katte_0 = Katte_Y - 5

# Hasard = random.randint(0,1)


# Constans Katte 4x5 squares
def KatteArch():
    for l in range(0, 1):
        # dimension de Katte_Y - difference de 5 squres
        for h in range(Katte_0, Katte_Y):
            # dimension de Katte_X
            for g in range(Doma_X, Katte_X):

                Room(
                    g,
                    h,
                    l,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(g) + "y" + str(h) + "z" + str(l),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (float(1.5), 0.0, 0.0)
                mat.specular_color = (200.0, 1.0, 100.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


KatteArch()


# DAIDOKO - Chambre verte
# -------------------------------------------------------------------------------------------------------

Daidoko_X = random.randint(Katte_X + 6, Katte_X + 8)
Daidoko_Y2 = random.randint(Katte_Y - 1, Katte_Y)
Daidoko_Y1 = random.randint(Katte_Y - 8, Katte_Y - 6)

if Plan == 0:
    Daidoko_X = random.randint(Katte_X + 4, Katte_X + 5)
    Daidoko_Y1 = random.randint(Katte_Y - 6, Katte_Y - 4)


def DaidokoArch():
    for p in range(0, 1):
        # dimension de Daidoko_Y
        for n in range(Daidoko_Y1, Daidoko_Y2):
            # dimension de Daidoko_X
            for m in range(Katte_X, Daidoko_X):
                # Ech = random.randint (1,1)
                RoomHigh(
                    m,
                    n,
                    p,
                    1,
                    1,
                    0.8,
                    "maChambreDsFonction x" + str(m) + "y" + str(n) + "z" + str(p),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (0.0, 1.0, 0.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


# Appeler la fonction:
DaidokoArch()


# Zashiki - Chambre bleu
# -------------------------------------------------------------------------------------------------------
Zashiki_X = random.randint(Daidoko_X + 4, Daidoko_X + 5)
Zashiki_Y = random.randint(Daidoko_Y2 - 1, Daidoko_Y2)
Zashiki_Y1 = Daidoko_Y2 - 5


# Genkan- subZashiki - small space related with Zashiki

Genkan_X = random.randint(Daidoko_X + 4, Daidoko_X + 5)
if Genkan_X > Zashiki_X:
    Genkan_X == Zashiki_X
Genkan_Y = random.randint(Daidoko_Y2, Daidoko_Y2)
Genkan_Y1 = Daidoko_Y2 - 5


def GenkanArch():
    for gz in range(0, 1):
        for gy in range(Zashiki_Y, Genkan_Y):
            for gx in range(Daidoko_X, Zashiki_X):
                Room(
                    gx,
                    gy,
                    gz,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(gx) + "y" + str(gy) + "z" + str(gz),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (5.0, 0.0, 1.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


ZashikiRandom = random.randint(0, 1)
if Plan == 0:
    ZashikiRandom == 0

if ZashikiRandom == 1:
    Zashiki_X = random.randint(Daidoko_X + 4, Daidoko_X + 5)
    Zashiki_Y = random.randint(Daidoko_Y2 - 2, Daidoko_Y2 - 2)
    Zashiki_Y1 = Daidoko_Y2 - 6

    def ZashikiArch2():
        for q in range(0, 1):
            # dimension de Zashiki_Y
            for l in range(Zashiki_Y1, Zashiki_Y):
                # dimension de Zashiki_X
                for n in range(Daidoko_X, Zashiki_X):
                    # Ech = random.randint (1,1)
                    Room(
                        n,
                        l,
                        q,
                        1,
                        1,
                        0.4,
                        "maChambreDsFonction x" + str(n) + "y" + str(l) + "z" + str(q),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (0.0, 0.0, 1.0)
                    mat.specular_color = (0.0, 0.0, 0.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    # Appeler la fonction:
    ZashikiArch2()
    GenkanArch()


def ZashikiArch():
    for q in range(0, 1):
        # dimension de Zashiki_Y
        for l in range(Zashiki_Y1, Zashiki_Y):
            # dimension de Zashiki_X
            for n in range(Daidoko_X, Zashiki_X):
                # Ech = random.randint (1,1)
                Room(
                    n,
                    l,
                    q,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(n) + "y" + str(l) + "z" + str(q),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (0.0, 0.0, 1.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


# Appeler la fonction:
ZashikiArch()


# Heya
# -------------------------------------------------------------------------------------------------------
Heya_X = random.randint(Zashiki_X - 1, Zashiki_X)
Heya_Y = random.randint(Daidoko_Y2 - 10, Daidoko_Y2 - 9)
# Heya_Y1 = random.randint(Katte_Y-6,Katte_Y-5)

if Plan == 0:
    Heya_X = random.randint(Zashiki_X - 1, Zashiki_X)
    Heya_Y = random.randint(Zashiki_Y1 - 4, Zashiki_Y1 - 3)


def HeyaArch():
    for z5 in range(0, 1):
        # dimension de Heya_Y
        for y5 in range(Heya_Y, Daidoko_Y2 - 5):
            # dimension de Heya_X
            for x5 in range(Daidoko_X, Heya_X):
                # Ech = random.randint (1,1)
                Room(
                    x5,
                    y5,
                    z5,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(x5) + "y" + str(y5) + "z" + str(z5),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (1.0, 0.0, 1.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


# Appeler la fonction:
HeyaArch()


# Kami
# -------------------------------------------------------------------------------------------------------

Kami_X = random.randint(Daidoko_X, Daidoko_X)
Kami_Y = random.randint(Daidoko_Y1 - 5, Daidoko_Y1 - 4)
# Kami_Y1 = random.randint(Katte_Y-6,Katte_Y-5)


def KamiArch():
    for z6 in range(0, 1):
        # dimension de Kami_Y
        for y6 in range(Kami_Y, Daidoko_Y1):
            # dimension de Kami_X
            for x6 in range(Katte_X, Kami_X):
                # Ech = random.randint (1,1)
                Room(
                    x6,
                    y6,
                    z6,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(x6) + "y" + str(y6) + "z" + str(z6),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (0.0, 1.0, 1.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


if Daidoko_X > Katte_X + 7:
    Kami_X = Katte_X + 4  # random.randint(Daidoko_X,Daidoko_X)
    Kami_Y = random.randint(Daidoko_Y1 - 5, Daidoko_Y1 - 4)

    def KamiArch():
        for z6 in range(0, 1):
            # dimension de Joi_Y
            for y6 in range(Kami_Y, Daidoko_Y1):
                # dimension de Joi_X
                for x6 in range(Katte_X, Kami_X):
                    # Ech = random.randint (1,1)
                    Room(
                        x6,
                        y6,
                        z6,
                        1,
                        1,
                        0.4,
                        "maChambreDsFonction x"
                        + str(x6)
                        + "y"
                        + str(y6)
                        + "z"
                        + str(z6),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (5.0, 5.0, 2.0)
                    mat.specular_color = (0.0, 10.0, 0.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    def Kami2Arch():
        for z6 in range(0, 1):
            # dimension de Joi_Y
            for y6 in range(Kami_Y, Daidoko_Y1):
                # dimension de Joi_X
                for x6 in range(Kami_X, Daidoko_X):
                    # Ech = random.randint (1,1)
                    Room(
                        x6,
                        y6,
                        z6,
                        1,
                        1,
                        0.4,
                        "maChambreDsFonction x"
                        + str(x6)
                        + "y"
                        + str(y6)
                        + "z"
                        + str(z6),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (0.0, 1.0, 2.0)
                    mat.specular_color = (0.0, 0.0, 0.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    Kami2Arch()
KamiArch()

# Joi
# -------------------------------------------------------------------------------------------------------

Joi_X = Katte_X  # random.randint(Daidoko_X,Daidoko_X)
Joi_Y = random.randint(Katte_0 - 5, Katte_0 - 3)
# Joi_Y1 = random.randint(Katte_Y-6,Katte_Y-5)


def JoiArch():
    for z7 in range(0, 1):
        # dimension de Joi_Y
        for y7 in range(Joi_Y, Katte_Y - 4):
            # dimension de Joi_X
            for x7 in range(Doma_X, Joi_X):
                # Ech = random.randint (1,1)
                Room(
                    x7,
                    y7,
                    z7,
                    1,
                    1,
                    0.4,
                    "maChambreDsFonction x" + str(x7) + "y" + str(y7) + "z" + str(z7),
                )
                mat = bpy.data.materials.new("PKHG")
                mat.diffuse_color = (1.0, 1.0, 2.0)
                mat.specular_color = (0.0, 0.0, 0.0)
                o = bpy.context.selected_objects[0]
                o.active_material = mat


if Katte_X >= Doma_X + 7:
    Joi_X = Doma_X + 4  # random.randint(Daidoko_X,Daidoko_X)
    Joi_Y = random.randint(Katte_0 - 5, Katte_0 - 3)

    def JoiArch():
        for z7 in range(0, 1):
            # dimension de Joi_Y
            for y7 in range(Joi_Y, Katte_Y - 4):
                # dimension de Joi_X
                for x7 in range(Doma_X, Joi_X):
                    # Ech = random.randint (1,1)
                    Room(
                        x7,
                        y7,
                        z7,
                        1,
                        1,
                        0.4,
                        "maChambreDsFonction x"
                        + str(x7)
                        + "y"
                        + str(y7)
                        + "z"
                        + str(z7),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (1.0, 1.0, 2.0)
                    mat.specular_color = (0.0, 0.0, 0.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    def Joi2Arch():
        for z7 in range(0, 1):
            # dimension de Joi_Y
            for y7 in range(Joi_Y, Katte_Y - 4):
                # dimension de Joi_X
                for x7 in range(Joi_X, Katte_X):
                    # Ech = random.randint (1,1)
                    Room(
                        x7,
                        y7,
                        z7,
                        1,
                        1,
                        0.4,
                        "maChambreDsFonction x"
                        + str(x7)
                        + "y"
                        + str(y7)
                        + "z"
                        + str(z7),
                    )
                    mat = bpy.data.materials.new("PKHG")
                    mat.diffuse_color = (1.0, 5.0, 2.0)
                    mat.specular_color = (0.0, 0.0, 0.0)
                    o = bpy.context.selected_objects[0]
                    o.active_material = mat

    Joi2Arch()
JoiArch()


# -------------------------------------------------------------------------------------------------------
#                                            P A R T I T I O N S
# -------------------------------------------------------------------------------------------------------

# -------------------------------------------------------------------------------------------------------
#                                                   LEGERES
# -------------------------------------------------------------------------------------------------------


# Shoji_Doma (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Shoji(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (0.1, 0.1, 3.0)

    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 10.0, 2.0)
    mat.specular_color = (0.0, 0.0, 0.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat


def Shoji_DomaArch():
    for z8 in range(0, 1):

        for y8 in range(Joi_Y, Katte_Y + 1):

            for x8 in range(Doma_X, Doma_X + 1):

                Shoji(
                    x8,
                    y8,
                    z8,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(x8) + "y" + str(y8) + "z" + str(z8),
                )


Shoji_DomaArch()


# Shoji_Katte_Joi (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Shoji_Katte_JoiArch():
    for z9 in range(0, 1):
        for y9 in range(Katte_0, Katte_0 + 1):
            for x9 in range(Doma_X, Katte_X):
                Shoji(
                    x9,
                    y9,
                    z9,
                    0.1,
                    0.1,
                    3.0,
                    "maChambreDsFonction x" + str(x9) + "y" + str(y9) + "z" + str(z9),
                )


Shoji_Katte_JoiArch()


# Shoji_Joi_Kami (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------
def Shoji_KatteArch():
    for z9 in range(0, 1):
        for y9 in range(Katte_Y, Katte_Y + 1):
            for x9 in range(Doma_X, Katte_X + 1):
                Shoji(
                    x9,
                    y9,
                    z9,
                    0.1,
                    0.1,
                    3.0,
                    "maChambreDsFonction x" + str(x9) + "y" + str(y9) + "z" + str(z9),
                )


Shoji_KatteArch()

# ------------------------------------------------------------------------


def Shoji_Joi_KamiArch():
    for z8 in range(0, 1):
        for y8 in range(Joi_Y, Daidoko_Y1 + 1):
            for x8 in range(Joi_X, Joi_X + 1):
                Shoji(
                    x8,
                    y8,
                    z8,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(x8) + "y" + str(y8) + "z" + str(z8),
                )


Shoji_Joi_KamiArch()


# external
def Shoji_Joi_ExtArch():
    for z8 in range(0, 1):
        for y8 in range(Joi_Y, Joi_Y + 1):
            for x8 in range(Doma_X, Katte_X):
                Shoji(
                    x8,
                    y8,
                    z8,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(x8) + "y" + str(y8) + "z" + str(z8),
                )


Shoji_Joi_ExtArch()

if Katte_X >= Doma_X + 7:

    def Shoji_Joi_Joi2Arch():
        for z8 in range(0, 1):
            for y8 in range(Joi_Y, Katte_0 + 1):
                for x8 in range(Joi_X, Joi_X + 1):
                    Shoji(
                        x8,
                        y8,
                        z8,
                        0.1,
                        0.1,
                        3,
                        "maChambreDsFonction x"
                        + str(x8)
                        + "y"
                        + str(y8)
                        + "z"
                        + str(z8),
                    )

    Shoji_Joi_Joi2Arch()

    def Shoji_Joi_KamiArch():
        for z8 in range(0, 1):
            for y8 in range(Kami_Y, Daidoko_Y1 + 1):
                for x8 in range(Katte_X, Katte_X + 1):
                    Shoji(
                        x8,
                        y8,
                        z8,
                        0.1,
                        0.1,
                        3,
                        "maChambreDsFonction x"
                        + str(x8)
                        + "y"
                        + str(y8)
                        + "z"
                        + str(z8),
                    )

    Shoji_Joi_KamiArch()


# external
def Shoji_Kami_ExtArch():
    for z8 in range(0, 1):
        for y8 in range(Kami_Y, Kami_Y + 1):
            for x8 in range(Katte_X, Daidoko_X):
                Shoji(
                    x8,
                    y8,
                    z8,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(x8) + "y" + str(y8) + "z" + str(z8),
                )


Shoji_Kami_ExtArch()

# Shoji_Kami_Heya (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Shoji_Kami_HeyaArch():
    for z10 in range(0, 1):
        for y10 in range(Kami_Y, Daidoko_Y1 + 1):
            for x10 in range(Daidoko_X, Daidoko_X + 1):
                Shoji(
                    x10,
                    y10,
                    z10,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x"
                    + str(x10)
                    + "y"
                    + str(y10)
                    + "z"
                    + str(z10),
                )


Shoji_Kami_HeyaArch()

if Daidoko_X > Katte_X + 7:
    # moyen shoji kami
    def Shoji_Kami_Kami2Arch():
        for skkz in range(0, 1):
            for skky in range(Kami_Y, Daidoko_Y1 + 1):
                for skkx in range(Katte_X + 4, Katte_X + 5):
                    Shoji(
                        skkx,
                        skky,
                        skkz,
                        0.1,
                        0.1,
                        3,
                        "maChambreDsFonction x"
                        + str(skkx)
                        + "y"
                        + str(skky)
                        + "z"
                        + str(skkz),
                    )

    Shoji_Kami_Kami2Arch()

    def Shoji_Kami_HeyaArch():
        for z10 in range(0, 1):
            for y10 in range(Kami_Y, Daidoko_Y1 + 1):
                for x10 in range(Daidoko_X, Daidoko_X + 1):
                    Shoji(
                        x10,
                        y10,
                        z10,
                        0.1,
                        0.1,
                        3,
                        "maChambreDsFonction x"
                        + str(x10)
                        + "y"
                        + str(y10)
                        + "z"
                        + str(z10),
                    )


Shoji_Kami_HeyaArch()


# Shoji_Zashiki_Heya (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Shoji_Zashiki_HeyaArch():
    for z11 in range(0, 1):
        for y11 in range(Zashiki_Y1, Zashiki_Y1 + 1):
            for x11 in range(Daidoko_X, Zashiki_X + 1):
                Shoji(
                    x11,
                    y11,
                    z11,
                    0.1,
                    0.1,
                    3.0,
                    "maChambreDsFonction x"
                    + str(x11)
                    + "y"
                    + str(y11)
                    + "z"
                    + str(z11),
                )


Shoji_Zashiki_HeyaArch()


# external
def Shoji_Heya1_ExtArch():
    for zg in range(0, 1):
        for yg in range(Heya_Y, Heya_Y + 1):
            for xg in range(Daidoko_X, Heya_X + 1):
                Shoji(
                    xg,
                    yg,
                    zg,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(xg) + "y" + str(yg) + "z" + str(zg),
                )


Shoji_Heya1_ExtArch()


# external2
def Shoji_Heya2_ExtArch():
    for zg in range(0, 1):
        for yg in range(Heya_Y, Zashiki_Y1):
            for xg in range(Heya_X, Heya_X + 1):
                Shoji(
                    xg,
                    yg,
                    zg,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(xg) + "y" + str(yg) + "z" + str(zg),
                )


Shoji_Heya2_ExtArch()


def Shoji_Zashiki_ExtArch():
    for z11 in range(0, 1):
        for y11 in range(Zashiki_Y, Zashiki_Y + 1):
            for x11 in range(Daidoko_X, Zashiki_X + 1):
                Shoji(
                    x11,
                    y11,
                    z11,
                    0.1,
                    0.1,
                    3.0,
                    "maChambreDsFonction x"
                    + str(x11)
                    + "y"
                    + str(y11)
                    + "z"
                    + str(z11),
                )


Shoji_Zashiki_ExtArch()

if ZashikiRandom == 1:

    def Shoji_Genkan_ExtArch():
        for z11 in range(0, 1):
            for y11 in range(Genkan_Y, Genkan_Y + 1):
                for x11 in range(Daidoko_X, Zashiki_X + 1):
                    Shoji(
                        x11,
                        y11,
                        z11,
                        0.1,
                        0.1,
                        3.0,
                        "maChambreDsFonction x"
                        + str(x11)
                        + "y"
                        + str(y11)
                        + "z"
                        + str(z11),
                    )

    Shoji_Genkan_ExtArch()

    # external
    def Shoji_GGuest_ExtArch():
        for zg in range(0, 1):
            for yg in range(Zashiki_Y, Genkan_Y):
                for xg in range(Genkan_X, Genkan_X + 1):
                    Shoji(
                        xg,
                        yg,
                        zg,
                        0.1,
                        0.1,
                        3,
                        "maChambreDsFonction x"
                        + str(xg)
                        + "y"
                        + str(yg)
                        + "z"
                        + str(zg),
                    )

    Shoji_GGuest_ExtArch()


# external
def Shoji_Guest_ExtArch():
    for zg in range(0, 1):
        for yg in range(Zashiki_Y1, Zashiki_Y):
            for xg in range(Zashiki_X, Zashiki_X + 1):
                Shoji(
                    xg,
                    yg,
                    zg,
                    0.1,
                    0.1,
                    3,
                    "maChambreDsFonction x" + str(xg) + "y" + str(yg) + "z" + str(zg),
                )


Shoji_Guest_ExtArch()

# -------------------------------------------------------------------------------------------------------
#                                                   SOLIDE
# -------------------------------------------------------------------------------------------------------

# Wall_Katte_Daidoko (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Wall_Katte_Daidoko(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (0.1, Daidoko_Y2 - Daidoko_Y1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Katte_DaidokoArch():
    for z12 in range(0, 1):
        y12 = Daidoko_Y1 + (Daidoko_Y2 - Daidoko_Y1) / 2
        # in range(Daidoko_Y1+2, Daidoko_Y1+3):
        for x12 in range(Katte_X, Katte_X + 1):
            Wall_Katte_Daidoko(
                x12,
                y12,
                z12,
                3,
                "maChambreDsFonction x" + str(x12) + "y" + str(y12) + "z" + str(z12),
            )


Wall_Katte_DaidokoArch()

# Wall_Daidoko_Kamii (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Wall_Daidoko_Kami(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (Daidoko_X - Katte_X, 0.1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Daidoko_KamiArch():
    for z13 in range(0, 1):
        for y13 in range(Daidoko_Y1, Daidoko_Y1 + 1):
            x13 = Daidoko_X - (Daidoko_X - Katte_X) / 2
            Wall_Daidoko_Kami(
                x13,
                y13,
                z13,
                3,
                "maChambreDsFonction x" + str(x13) + "y" + str(y13) + "z" + str(z13),
            )


Wall_Daidoko_KamiArch()

# Wall_Daidoko_Zashiki (la partitions du papier et bois)
# -------------------------------------------------------------------------------------------------------


def Wall_Daidoko_Zashiki(pos_X, pos_Y, pos_Z, dim_X, dim_Y, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (0.1, Daidoko_Y2 - Daidoko_Y1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Daidoko_ZashikiArch():
    for z14 in range(0, 1):
        y14 = (
            Daidoko_Y1 + (Daidoko_Y2 - Daidoko_Y1) / 2
        )  # in range(Daidoko_Y1+2, Daidoko_Y1+3):
        for x14 in range(Daidoko_X, Daidoko_X + 1):
            Wall_Daidoko_Zashiki(
                x14,
                y14,
                z14,
                0.1,
                0.1,
                3,
                "maChambreDsFonction x" + str(x14) + "y" + str(y14) + "z" + str(z14),
            )


Wall_Daidoko_ZashikiArch()


# EXTERNAL
def Wall_Daidoko(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (Daidoko_X - Katte_X, 0.1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_DaidokoArch():
    for z13 in range(0, 1):
        for y13 in range(Daidoko_Y2, Daidoko_Y2 + 1):
            x13 = Daidoko_X - (Daidoko_X - Katte_X) / 2
            Wall_Daidoko_Kami(
                x13,
                y13,
                z13,
                3,
                "maChambreDsFonction x" + str(x13) + "y" + str(y13) + "z" + str(z13),
            )


Wall_DaidokoArch()

# Doma rear
# -------------------------------------------


def Wall_Doma_Rear(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (Doma_X, 0.1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Doma_RearArch():
    for dsz in range(0, 1):
        for dsy in range(Doma_Y, Doma_Y + 1):
            dsx = Doma_X / 2
            Wall_Doma_Rear(
                dsx,
                dsy,
                dsz,
                3,
                "maChambreDsFonction x" + str(dsx) + "y" + str(dsy) + "z" + str(dsz),
            )


Wall_Doma_RearArch()

# Doma Front
# -------------------------------------


def Wall_Doma_Front(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (Doma_X, 0.1, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Doma_FrontArch():
    for dsz in range(0, 1):
        for dsy in range(0, 1):
            dsx = Doma_X / 2
            Wall_Doma_Front(
                dsx,
                dsy,
                dsz,
                3,
                "maChambreDsFonction x" + str(dsx) + "y" + str(dsy) + "z" + str(dsz),
            )


Wall_Doma_FrontArch()

# Doma Right
# -------------------------------------


def Wall_Doma_Right(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (0.1, Joi_Y, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Doma_RightArch():
    for dsz in range(0, 1):
        dsy = Joi_Y / 2
        for dsx in range(Doma_X, Doma_X + 1):
            Wall_Doma_Right(
                dsx,
                dsy,
                dsz,
                3,
                "maChambreDsFonction x" + str(dsx) + "y" + str(dsy) + "z" + str(dsz),
            )


Wall_Doma_RightArch()


# Doma Left
# --------------------
def Wall_Doma_Store(pos_X, pos_Y, pos_Z, dim_Z, nom):
    bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
    bpy.context.object.dimensions = (0.1, Doma_Y, 3.0)
    mat = bpy.data.materials.new("PKHG")
    mat.diffuse_color = (1.0, 1.0, 1.0)
    mat.specular_color = (200.0, 1.0, 100.0)
    o = bpy.context.selected_objects[0]
    o.active_material = mat
    bpy.context.object.active_material.use_transparency = True
    bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5


def Wall_Doma_StoreArch():
    for dsz in range(0, 1):
        dsy = 0 + Doma_Y / 2
        for dsx in range(0, 1):
            Wall_Doma_Store(
                dsx,
                dsy,
                dsz,
                3,
                "maChambreDsFonction x" + str(dsx) + "y" + str(dsy) + "z" + str(dsz),
            )


Wall_Doma_StoreArch()

if Store == 1:
    # Wall_Store_Doma (la partitions du papier et bois)
    # -------------------------------------------------------------------------------------------------------
    def Wall_Store(pos_X, pos_Y, pos_Z, dim_Z, nom):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
        bpy.context.object.dimensions = (Store_X, 0.1, 3.0)
        mat = bpy.data.materials.new("PKHG")
        mat.diffuse_color = (1.0, 1.0, 1.0)
        mat.specular_color = (200.0, 1.0, 100.0)
        o = bpy.context.selected_objects[0]
        o.active_material = mat
        bpy.context.object.active_material.use_transparency = True
        bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5

    def Wall_StoreArch():
        for dsz in range(0, 1):
            for dsy in range(Store_Y, Store_Y + 1):
                dsx = 0 - (Store_X) / 2
                Wall_Store(
                    dsx,
                    dsy,
                    dsz,
                    3,
                    "maChambreDsFonction x"
                    + str(dsx)
                    + "y"
                    + str(dsy)
                    + "z"
                    + str(dsz),
                )

    Wall_StoreArch()

    # Store 2
    # -------------
    def Wall_Store3(pos_X, pos_Y, pos_Z, dim_Z, nom):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
        bpy.context.object.dimensions = (Store_X, 0.1, 3.0)
        mat = bpy.data.materials.new("PKHG")
        mat.diffuse_color = (1.0, 1.0, 1.0)
        mat.specular_color = (200.0, 1.0, 100.0)
        o = bpy.context.selected_objects[0]
        o.active_material = mat
        bpy.context.object.active_material.use_transparency = True
        bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5

    def Wall_Store3Arch():
        for dsz in range(0, 1):
            for dsy in range(Doma_Y, Doma_Y + 1):
                dsx = 0 - (Store_X) / 2
                Wall_Store3(
                    dsx,
                    dsy,
                    dsz,
                    3,
                    "maChambreDsFonction x"
                    + str(dsx)
                    + "y"
                    + str(dsy)
                    + "z"
                    + str(dsz),
                )

    Wall_Store3Arch()

    # Store 0
    # -------------
    def Wall_Store0(pos_X, pos_Y, pos_Z, dim_Z, nom):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
        bpy.context.object.dimensions = (Store2_X, 0.1, 3.0)
        mat = bpy.data.materials.new("PKHG")
        mat.diffuse_color = (1.0, 1.0, 1.0)
        mat.specular_color = (200.0, 1.0, 100.0)
        o = bpy.context.selected_objects[0]
        o.active_material = mat
        bpy.context.object.active_material.use_transparency = True
        bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5

    def Wall_Store0Arch():
        for dsz in range(0, 1):
            for dsy in range(Store2_Y, Store2_Y + 1):
                dsx = 0 - (Store2_X) / 2
                Wall_Store0(
                    dsx,
                    dsy,
                    dsz,
                    3,
                    "maChambreDsFonction x"
                    + str(dsx)
                    + "y"
                    + str(dsy)
                    + "z"
                    + str(dsz),
                )

    Wall_Store0Arch()

    # Store_X = random.randint(6.0,10.0)
    # Store_Y = random.randint(5.0,7.0)
    # Store2_X = random.randint(4.0,6.0)
    # Store2_Y = random.randint(0,2)

    # Store 3
    # -----------------
    def Wall_Store3(pos_X, pos_Y, pos_Z, dim_Z, nom):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
        bpy.context.object.dimensions = (0.1, Doma_Y - Store_Y, 3.0)
        mat = bpy.data.materials.new("PKHG")
        mat.diffuse_color = (1.0, 1.0, 1.0)
        mat.specular_color = (200.0, 1.0, 100.0)
        o = bpy.context.selected_objects[0]
        o.active_material = mat
        bpy.context.object.active_material.use_transparency = True
        bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5

    def Wall_Store3Arch():
        for dsz in range(0, 1):
            dsy = Store_Y + (Doma_Y - Store_Y) / 2
            for dsx in range(-Store_X, -Store_X + 1):
                Wall_Store3(
                    dsx,
                    dsy,
                    dsz,
                    3,
                    "maChambreDsFonction x"
                    + str(dsx)
                    + "y"
                    + str(dsy)
                    + "z"
                    + str(dsz),
                )

    Wall_Store3Arch()

    # Store 4
    # -----------------
    def Wall_Store4(pos_X, pos_Y, pos_Z, dim_Z, nom):
        bpy.ops.mesh.primitive_cube_add(location=(pos_X, pos_Y, pos_Z + dim_Z / 2))
        bpy.context.object.dimensions = (0.1, Store2_Y - Store_Y, 3.0)
        mat = bpy.data.materials.new("PKHG")
        mat.diffuse_color = (1.0, 1.0, 1.0)
        mat.specular_color = (200.0, 1.0, 100.0)
        o = bpy.context.selected_objects[0]
        o.active_material = mat
        bpy.context.object.active_material.use_transparency = True
        bpy.context.object.active_material.raytrace_transparency.fresnel = 2.5

    def Wall_Store4Arch():
        for wsz in range(0, 1):
            wsy = Store2_Y + (Store_Y - Store2_Y) / 2
            for wsx in range(-Store2_X, -Store2_X + 1):
                Wall_Store4(
                    wsx,
                    wsy,
                    wsz,
                    3,
                    "maChambreDsFonction x"
                    + str(wsx)
                    + "y"
                    + str(wsy)
                    + "z"
                    + str(wsz),
                )

    Wall_Store4Arch()

"""
#-------------------------------------------------------------------------------------------------------
#                                                   TATAMI
#-------------------------------------------------------------------------------------------------------

#Doma
#-------------------------------------------------------------------------------------------------------
def Tatami(Tpos_X, Tpos_Y, Tpos_Z,Tdim_X, Tdim_Y,Tdim_Z, nom):
    bpy.ops.mesh.primitive_grid_add(location=(Tpos_X + Tdim_X, Tpos_Y + Tdim_Y/2, Tpos_Z + Tdim_Z))
    bpy.context.object.dimensions=(2,1,1)



def TatamiArch():
    for tk in range(0, 1):
        for tj in range(0, Doma_Y):
            for ti in range((0, Doma_X)+1):
                    Tatami(ti, tj, tk,1,1,1,'maChambreDsFonction x'+str(ti)+'y'+ str(tj)+'z'+ str(tk))

TatamiArch()
"""
