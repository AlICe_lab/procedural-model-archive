# SPDX-FileCopyrightText: 2018 Pierre Quinet & Rodrigo Oliveira Rodrigues
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

######################################
########### The Zajec Code ###########
######################################

### Importation des listes
import bpy
import random
from random import randint

### Nettoyage préliminaire
from bpy import data as D


def delete_all():
    for item in D.objects:
        D.objects.remove(item)
    for item in D.meshes:
        D.meshes.remove(item)


delete_all()

##############################################################
##############################################################
"""#--- VARIABLES : Manipulation des variables de matrice -#"""
##############################################################
##############################################################


### 0_créer une matrice de point pour faire 5X5 Colonne
coord = []
for x in range(0, 9):
    for y in range(0, 9):
        for z in range(0, 10):
            coord.append([x, z, y])

##############################################################
##############################################################
"""#- VARIABLES : Manipulation des options de base ------#"""
##############################################################
##############################################################


####VARIABLE Tableau
### A_choix de tableau option1/2/3 VARIABLE
# Tableau 1 (8X8) == 1X(5x5) + 5X(3X3)
# Tableau 2 (8X8) == 2X(5x5) + 2X(3x3)
# Tableau 3 (8X5) == 1X(5x5) + 2X(3x3)

opChoix = [1, 2, 3]
op = 1

### B_face vierge des matrices visible (2) ou non visible (1) VARIABLE

face = 2

### C_Epaisseur des parrois VARIABLE

TY = 0.12

### D_Mirroirs en X Y et Z VARIABLE

MX = False
MY = False
MZ = False

### E_Duplicate and rotation True or False: VARIABLE

Duplicate = True

### F_Variables Camera
CAM_Focale = 50  # Definit la focale
# La focale est inversée, la valeur minimale(1) = la plus grande longueur focale

#### VARIABLE Hauteur des Matrices et des faces dans les Zajecs

### G_hauteur de chaque matrice (entre 0,1,2,3,4,5,6,7,8) VARIABLE
# faire attention de pas tomber dans des valeurs impossibles avec l'accumulation
ihChoix = [0, 1, 2]

ih = random.choice(ihChoix)  # Hauteur matrice A
jh = random.choice(ihChoix)  # Hauteur matrice B
kh = random.choice(ihChoix)  # Hauteur matrice C
lh = random.choice(ihChoix)  # Hauteur matrice D
mh = random.choice(ihChoix)  # Hauteur matrice E
nh = random.choice(ihChoix)  # Hauteur matrice F

### D_hauteur de chaque face dans zajec VARIABLE
# faire attention de pas tomber dans des valeurs impossibles si on met des negatifs dans i,j,k ne pas dépacer le minimum de -(ihChoix)
# si ihChoix > 2 alors on peut mettre des negatifs jusque -2
# si ihChoix > 0 alors pas de negatif

i = 0
j = 1
k = 2

### E_Hauteur du rectangle en intéraction avec les diagonales  VARIABLE
# faire attention de pas tomber dans des valeurs impossibles si on met des negatifs dans i,j,k ne pas dépacer le minimum de -(ihChoix)
# si ihChoix > 2 alors on peut mettre des negatifs jusque -2
# si ihChoix > 0 alors pas de negatif

H = k
Hh = H  # Diagonale A hauteur
Hi = H  # Diagonale B hauteur
Hj = H  # Diagonale C hauteur
Hk = H  # Diagonale D hauteur
Hl = H  # Diagonale E hauteur
Hm = H  # Diagonale F hauteur

##############################################################
##############################################################
"""#-------------- Etape 1 : Base de chaque tableau ------#"""
##############################################################
##############################################################


### A_matrice_a 5x5 de base
Aa = 0 + ih
Ab = Aa + (5 * 10)
Ac = Aa + (5 * 90) + (5 * 10)
Ad = Aa + (5 * 90)
maMatrice_a = [(Aa, Ab, Ac, Ad)]
print(maMatrice_a)
if face == 1:
    print("pasdeface")
else:
    # Voir la matrice_a
    # créer le mesh et l'objet
    monMesh_a = bpy.data.meshes.new("triangleMesh")
    monObjet_a = bpy.data.objects.new("maMatrice_a", monMesh_a)
    # lier l'objet à la scène
    maScene_a = bpy.context.scene
    maScene_a.objects.link(monObjet_a)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_a.from_pydata(coord, [], maMatrice_a)

##############################################################
# -------------- Debut CODE : Matrice Zajec 5x5 --------------#
##############################################################

### 1_ intro
### 1_ definition des points de la matrice 5x5 en fct d'une origine
Aaai = Aa
Aabi = Aaai + (10 * 1)
Aaci = Aaai + (10 * 2)
Aadi = Aaai + (10 * 3)
Aaei = Aaai + (10 * 4)
Aafi = Aaai + (10 * 5)

Abai = Aa + (90 * 1)
Abbi = Abai + (10 * 1)
Abci = Abai + (10 * 2)
Abdi = Abai + (10 * 3)
Abei = Abai + (10 * 4)
Abfi = Abai + (10 * 5)

Acai = Aa + (90 * 2)
Acbi = Acai + (10 * 1)
Acci = Acai + (10 * 2)
Acdi = Acai + (10 * 3)
Acei = Acai + (10 * 4)
Acfi = Acai + (10 * 5)

Adai = Aa + (90 * 3)
Adbi = Adai + (10 * 1)
Adci = Adai + (10 * 2)
Addi = Adai + (10 * 3)
Adei = Adai + (10 * 4)
Adfi = Adai + (10 * 5)

Aeai = Aa + (90 * 4)
Aebi = Aeai + (10 * 1)
Aeci = Aeai + (10 * 2)
Aedi = Aeai + (10 * 3)
Aeei = Aeai + (10 * 4)
Aefi = Aeai + (10 * 5)

Afai = Aa + (90 * 5)
Afbi = Afai + (10 * 1)
Afci = Afai + (10 * 2)
Afdi = Afai + (10 * 3)
Afei = Afai + (10 * 4)
Affi = Afai + (10 * 5)

### 2_ Création de chaque face composant la matrice mais à plat
### 2.1_ Definir les points de la Face directrice avec diagonale
# Aaa
AaazChoix = [
    Afai,
    Aeai,
    Adai,
    Acai,
    Abai,
    Afbi,
    Aebi,
    Adbi,
    Acbi,
    Abbi,
    Afci,
    Aeci,
    Adci,
    Acci,
    Abci,
    Afdi,
    Aedi,
    Addi,
    Acdi,
    Abdi,
    Afei,
    Aeei,
    Aedi,
    Aeci,
    Aebi,
]
Aaa = random.choice(AaazChoix)

# Aab
AibChoix = [10, 20]
if Aaa in [Afbi, Aebi, Adbi, Acbi, Abbi, Afdi, Aedi, Addi, Acdi, Abdi]:
    Aib = 20
elif Aaa in [Afei, Aeei, Adei, Acei, Abei]:
    Aib = 10
else:
    Aib = random.choice(AibChoix)

Aab = Aaa + Aib

# Aac
AicChoix = [90, 180]
if Aaa in [Aeai, Aebi, Aeci, Aedi, Aeei, Aefi, Acai, Acbi, Acci, Acdi, Acei, Acfi]:
    Aic = 180
elif Aaa in [Abai, Abbi, Abci, Abdi, Abei, Abfi]:
    Aic = 90
else:
    Aic = random.choice(AicChoix)
Aac = Aab - Aic

# Aad
if Aaa in [Abai, Abbi, Abci, Abdi, Abei]:
    Aad = Abfi
elif Aaa in [Acai, Acbi, Acci, Acdi, Acei]:
    Aad = Acfi
elif Aaa in [Adai, Adbi, Adci, Addi, Adei]:
    Aad = Adfi
elif Aaa in [Aeai, Aebi, Aeci, Aedi, Aeei]:
    Aad = Aefi
else:
    Aad = Affi

# Aae
Aae = Aad - (Aic)

### 2.2_Definir les points de la Face directrice avec diagonale mais vide
# Aaf

if Aaa in [Afai, Aeai, Adai, Acai, Abai]:
    Aaf = Aaai
elif Aaa in [Afbi, Aebi, Adbi, Acbi, Abbi]:
    Aaf = Aabi
elif Aaa in [Afci, Aeci, Adci, Acci, Abci]:
    Aaf = Aaci
elif Aaa in [Afdi, Aedi, Addi, Acdi, Abdi]:
    Aaf = Aadi
else:
    Aaf = Aaei

# Aag
Aag = Aaf + Aib

### 3_Ajouter une hauteur aux faces
### 3.1_faire un random des 3 hauteur possibles au préalabe
Ahachoix = [i, j, k]
Aha = random.choice(Ahachoix)
Ahbchoix = [i, j]
Ahcchoix = [j, k]
Ahdchoix = [i, k]
if Aha == k:
    Ahb = random.choice(Ahbchoix)
elif Aha == i:
    Ahb = random.choice(Ahcchoix)
else:
    Ahb = random.choice(Ahdchoix)
if Aha == j and Ahb == i:
    Ahc = k
elif Aha == i and Ahb == i:
    Ahc = k
elif Aha == k and Ahb == i:
    Ahc = j
elif Aha == i and Ahb == k:
    Ahc = j
else:
    Ahc = i

### 2.3_Créer un rectangle à subidiviser en 2-2-1 quider par les diagonales directrice sur y Ary

if Aad == Affi:
    Aah = Afai
    Aan = Aaa
    print("Aad=Affi")
else:
    Aah = Aad - (5 * 10)

    # 2.3.1_Subidiviser en 3 rectangles en respectant 2-2-1
    # 2.3.2_premier rectangle Ary1
    AaxChoix = [10, 20]
    Aax = random.choice(AaxChoix)
    Aaj = Aah + Aax
    Aak = Afai + Aax
    Aal = Afai

    # 2.3.3_deuxieme rectangle Ary2
    AayChoix = [10, 20]
    if Aax == 10:
        Aay = 20
    else:
        Aay = random.choice(AayChoix)
    Aan = Aaj + Aay
    Aao = Aak + Aay
    Aap = Affi

    # 3.1_Ajouter une Hauteur a chaque face Ary
    # 3.1.1.1_les points de Aryh1
    Aryh1a = Aah + Aha
    Aryh1b = Aaj + Aha
    Aryh1c = Aak + Aha
    Aryh1d = Aal + Aha

    # 3.1.1.2_Face Aryh1
    mesFaces_Aryh1 = [(Aryh1a, Aryh1b, Aryh1c, Aryh1d)]
    print("Aryh1=", mesFaces_Aryh1)

    if Aha == 0:
        print("ilyapasAry1")
    else:
        # créer le mesh et l'objet
        monMesh_Aryh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Aryh1 = bpy.data.objects.new("Aryh1", monMesh_Aryh1)
        # lier l'objet à la scène
        maScene_Aryh1 = bpy.context.scene
        maScene_Aryh1.objects.link(monObjet_Aryh1)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Aryh1.from_pydata(coord, [], mesFaces_Aryh1)

    # 3.1.2.1_les points deAryh2
    Aryh2a = Aan + Ahb
    Aryh2b = Aad + Ahb
    Aryh2c = Aap + Ahb
    Aryh2d = Aao + Ahb

    # 3.1.2.2_Face Aryh2
    mesFaces_Aryh2 = [(Aryh2a, Aryh2b, Aryh2c, Aryh2d)]
    print("Aryh2=", mesFaces_Aryh2)
    if Ahb == 0:
        print("ilyapasAry2")
    else:
        # créer le mesh et l'objet
        monMesh_Aryh2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Aryh2 = bpy.data.objects.new("Aryh2", monMesh_Aryh2)
        # lier l'objet à la scène
        maScene_Aryh2 = bpy.context.scene
        maScene_Aryh2.objects.link(monObjet_Aryh2)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Aryh2.from_pydata(coord, [], mesFaces_Aryh2)

    # 3.1.3.1_les points de Aryh3
    Aryh3a = Aan + Ahc
    Aryh3b = Aaj + Ahc
    Aryh3c = Aak + Ahc
    Aryh3d = Aao + Ahc

    # 3.1.3.2_Face Aryh3
    mesFaces_Aryh3 = [(Aryh3a, Aryh3b, Aryh3c, Aryh3d)]
    print("Aryh3=", mesFaces_Aryh3)
    if Ahc == 0:
        print("ilyapasAry3")
    else:
        # créer le mesh et l'objet
        monMesh_Aryh3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Aryh3 = bpy.data.objects.new("Aryh3", monMesh_Aryh3)
        # lier l'objet à la scène
        maScene_Aryh3 = bpy.context.scene
        maScene_Aryh3.objects.link(monObjet_Aryh3)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Aryh3.from_pydata(coord, [], mesFaces_Aryh3)

    # 3.2_créer les 2 faces de connection
    # 3.2.1_Face  entre Aryh3 et Aryh2 ==Aryhv1
    mesFaces_Aryhv1 = [(Aryh3d, Aryh2d, Aryh2a, Aryh3a)]
    print("face Aryhv1=", mesFaces_Aryhv1)
    # créer le mesh et l'objet
    monMesh_Aryhv1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Aryhv1 = bpy.data.objects.new("Aryhv1", monMesh_Aryhv1)
    # lier l'objet à la scène
    maScene_Aryhv1 = bpy.context.scene
    maScene_Aryhv1.objects.link(monObjet_Aryhv1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Aryhv1.from_pydata(coord, [], mesFaces_Aryhv1)

    # 3.2.2_Face  entre Aryh1 et Aryh3 == Aryhv2
    mesFaces_Aryhv2 = [(Aryh1b, Aryh1c, Aryh3c, Aryh3b)]
    print("face Aryhv2=", mesFaces_Aryhv2)
    # créer le mesh et l'objet
    monMesh_Aryhv2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Aryhv2 = bpy.data.objects.new("Aryhv2", monMesh_Aryhv2)
    # lier l'objet à la scène
    maScene_Aryhv2 = bpy.context.scene
    maScene_Aryhv2.objects.link(monObjet_Aryhv2)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Aryhv2.from_pydata(coord, [], mesFaces_Aryhv2)

    # 3.2.3_Face  arrete Aryh2 == Aryhv3
    mesFaces_Aryhv3 = [(Aryh2b, Aryh2c, Affi, Aad)]
    print("face Aryhv3=", mesFaces_Aryhv3)
    # créer le mesh et l'objet
    monMesh_Aryhv3 = bpy.data.meshes.new("triangleMesh")
    monObjet_Aryhv3 = bpy.data.objects.new("Aryhv3", monMesh_Aryhv3)
    # lier l'objet à la scène
    maScene_Aryhv3 = bpy.context.scene
    maScene_Aryhv3.objects.link(monObjet_Aryhv3)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Aryhv3.from_pydata(coord, [], mesFaces_Aryhv3)

    # 3.2.4_Face  arrete Aryh1 == Aryhv4
    mesFaces_Aryhv4 = [(Aryh1a, Aryh1d, Afai, Aah)]
    print("face Aryhv4=", mesFaces_Aryhv4)
    # créer le mesh et l'objet
    monMesh_Aryhv4 = bpy.data.meshes.new("triangleMesh")
    monObjet_Aryhv4 = bpy.data.objects.new("Aryhv4", monMesh_Aryhv4)
    # lier l'objet à la scène
    maScene_Aryhv4 = bpy.context.scene
    maScene_Aryhv4.objects.link(monObjet_Aryhv4)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Aryhv4.from_pydata(coord, [], mesFaces_Aryhv4)


### 2.4_ Créer un rectangle à subidiviser en 2-2-1
Aal = Afai
if Aaf == Aaai:
    print("Aaf = Aaai")
else:
    # 2.4.1_Subidiviser en 3 rectangles en respectant 2-2-1
    # 2.4.1_point des rectangles en Arx
    Aav = Aal - Aah
    AawChoix = [90, 180]
    Aaw = random.choice(AawChoix)
    if Aav == 90:
        Aaw = 180
    else:
        Aaw = random.choice(AawChoix)
    Aaq = Aaf + Aaw
    Aar = Aaai
    Aap = Aar + Aaw

    # 3.4.1.1_point de Arxh1
    Arxh1a = Aah + Ahb
    Arxh1b = Aap + Ahb
    Arxh1c = Aaq + Ahb
    Arxh1d = Aaa + Ahb

    # 3.4.1.2_Face Arxh1
    mesFaces_Arxh1 = [(Arxh1a, Arxh1b, Arxh1c, Arxh1d)]
    print("Arxh1=", mesFaces_Arxh1)
    if Ahb == 0:
        print("Ahb=0")
    else:
        # créer le mesh et l'objet
        monMesh_Arxh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Arxh1 = bpy.data.objects.new("Arxh1", monMesh_Arxh1)
        # lier l'objet à la scène
        maScene_Arxh1 = bpy.context.scene
        maScene_Arxh1.objects.link(monObjet_Arxh1)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Arxh1.from_pydata(coord, [], mesFaces_Arxh1)

    # 3.4.2.1_point de Arxh2
    Arxh2a = Aar + Aha
    Arxh2b = Aap + Aha
    Arxh2c = Aaq + Aha
    Arxh2d = Aaf + Aha

    # 3.4.2.2_Face Arxh2
    mesFaces_Arxh2 = [(Arxh2a, Arxh2b, Arxh2c, Arxh2d)]
    print("Arxh2=", mesFaces_Arxh2)
    if Aha == 0:
        print("Aha = 0")
    else:
        # créer le mesh et l'objet
        monMesh_Arxh2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Arxh2 = bpy.data.objects.new("Arxh2", monMesh_Arxh2)
        # lier l'objet à la scène
        maScene_Arxh2 = bpy.context.scene
        maScene_Arxh2.objects.link(monObjet_Arxh2)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Arxh2.from_pydata(coord, [], mesFaces_Arxh2)

    # 3.4.3_Face  entre Arxh1 et Arxh2 == Arxhv1
    mesFaces_Arxhv1 = [(Arxh1b, Arxh1c, Arxh2c, Arxh2b)]
    print("face Arxhv1=", mesFaces_Arxhv1)
    # créer le mesh et l'objet
    monMesh_Arxhv1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhv1 = bpy.data.objects.new("Arxhv1", monMesh_Arxhv1)
    # lier l'objet à la scène
    maScene_Arxhv1 = bpy.context.scene
    maScene_Arxhv1.objects.link(monObjet_Arxhv1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhv1.from_pydata(coord, [], mesFaces_Arxhv1)

    # 3.4.4_Face  entre Arxh2 sol == Arxhv11
    mesFaces_Arxhv11 = [(Arxh1a, Arxh1b, Aap, Aah)]
    print("Arxhv11=", mesFaces_Arxhv11)
    # créer le mesh et l'objet
    monMesh_Arxhv11 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhv11 = bpy.data.objects.new("Arxhv11", monMesh_Arxhv11)
    # lier l'objet à la scène
    maScene_Arxhv11 = bpy.context.scene
    maScene_Arxhv11.objects.link(monObjet_Arxhv11)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhv11.from_pydata(coord, [], mesFaces_Arxhv11)

    # 3.4.5_Face  entre Arxh2 sol == Arxhv2
    mesFaces_Arxhv2 = [(Arxh2a, Arxh2b, Aap, Aar)]
    print("Arxhv2=", mesFaces_Arxhv2)
    # créer le mesh et l'objet
    monMesh_Arxhv2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhv2 = bpy.data.objects.new("Arxhv2", monMesh_Arxhv2)
    # lier l'objet à la scène
    maScene_Arxhv2 = bpy.context.scene
    maScene_Arxhv2.objects.link(monObjet_Arxhv2)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhv2.from_pydata(coord, [], mesFaces_Arxhv2)

    # 3.4.6_Face  entre Arxh1 et Aryh1 == Arxhv3
    if Aad == Affi:
        Aryh1b = Aaa
        Aryh1a = Aah
        if Aah == Afai:
            Aryh3b = Aaa
            Aryh3a = Aaa
        else:
            Aryh3b = Aan
            Aryh3a = Aan
    else:
        Aryh1a = Aah + Aha
        Aryh1b = Aaj + Aha

    mesFaces_Arxhv3 = [(Arxh1a, Arxh1d, Aaa, Aan, Aryh3a, Aryh3b, Aryh1b, Aryh1a)]
    print("Arxhv3=", mesFaces_Arxhv3)
    # créer le mesh et l'objet
    monMesh_Arxhv3 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhv3 = bpy.data.objects.new("Arxhv3", monMesh_Arxhv3)
    # lier l'objet à la scène
    maScene_Arxhv3 = bpy.context.scene
    maScene_Arxhv3.objects.link(monObjet_Arxhv3)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhv3.from_pydata(coord, [], mesFaces_Arxhv3)

    # 3.4.7_face sur la tranche de Arxh1 == Arxhvh1
    mesFaces_Arxhvh1 = [(Arxh1d, Arxh1c, Aaq, Aaa)]
    print("Arxhvh1=", mesFaces_Arxhvh1)
    # créer le mesh et l'objet
    monMesh_Arxhvh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhvh1 = bpy.data.objects.new("Arxhvh1", monMesh_Arxhvh1)
    # lier l'objet à la scène
    maScene_Arxhvh1 = bpy.context.scene
    maScene_Arxhvh1.objects.link(monObjet_Arxhvh1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhvh1.from_pydata(coord, [], mesFaces_Arxhvh1)

    # 3.4.8_face sur la tranche de Arxh2 == Arxhvh2
    mesFaces_Arxhvh2 = [(Arxh2c, Arxh2d, Aaf, Aaq)]
    print("Arxhvh2=", mesFaces_Arxhvh2)
    # créer le mesh et l'objet
    monMesh_Arxhvh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arxhvh2 = bpy.data.objects.new("Arxhvh2", monMesh_Arxhvh2)
    # lier l'objet à la scène
    maScene_Arxhvh2 = bpy.context.scene
    maScene_Arxhvh2.objects.link(monObjet_Arxhvh2)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arxhvh2.from_pydata(coord, [], mesFaces_Arxhvh2)

### 3.5_Créer un rectangle entre les diagonales directrice
Arrha = Aae + Hh
Arrhb = Aac + Hh
Arrhc = Aag + Hh
Arrhd = Aafi + Hh

if Aag == Aafi:
    print("Aag==Aafi")
else:
    # Arr
    # 3.5.1_Face Arrh
    mesFaces_Arrh = [(Arrha, Arrhb, Arrhc, Arrhd)]
    print("Arrh=", mesFaces_Arrh)
    # créer le mesh et l'objet
    monMesh_Arrh = bpy.data.meshes.new("triangleMesh")
    monObjet_Arrh = bpy.data.objects.new("Arrh", monMesh_Arrh)
    # lier l'objet à la scène
    maScene_Arrh = bpy.context.scene
    maScene_Arrh.objects.link(monObjet_Arrh)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arrh.from_pydata(coord, [], mesFaces_Arrh)

    # 3.5.2_face Arhh 1
    mesFaces_Arrh1 = [(Arrhc, Arrhb, Aac, Aag)]
    print("Arrh1=", mesFaces_Arrh1)
    # créer le mesh et l'objet
    monMesh_Arrh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Arrh1 = bpy.data.objects.new("Arrh1", monMesh_Arrh1)
    # lier l'objet à la scène
    maScene_Arrh1 = bpy.context.scene
    maScene_Arrh1.objects.link(monObjet_Arrh1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Arrh1.from_pydata(coord, [], mesFaces_Arrh1)

### 3.6_créer la face de la diagonale directrice
# Ard
# 3.6.1_Face Ardh
mesFaces_Ardh = [(Aaa, Arrhb, Arrha, Aad)]
print("Ardh=", mesFaces_Ardh)
# créer le mesh et l'objet
monMesh_Ardh = bpy.data.meshes.new("triangleMesh")
monObjet_Ardh = bpy.data.objects.new("Ardh", monMesh_Ardh)
# lier l'objet à la scène
maScene_Ardh = bpy.context.scene
maScene_Ardh.objects.link(monObjet_Ardh)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Ardh.from_pydata(coord, [], mesFaces_Ardh)

# 3.6.2_Face Ardv
mesFaces_Ardv = [(Aafi, Aae, Aad, Arrha, Arrhd)]
print("Ardv=", mesFaces_Ardv)
# créer le mesh et l'objet
monMesh_Ardv = bpy.data.meshes.new("triangleMesh")
monObjet_Ardv = bpy.data.objects.new("Ardv", monMesh_Ardv)
# lier l'objet à la scène
maScene_Ardv = bpy.context.scene
maScene_Ardv.objects.link(monObjet_Ardv)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Ardv.from_pydata(coord, [], mesFaces_Ardv)

# 3.6.3_Face Ardd
mesFaces_Ardd = [(Aaa, Arrhb, Aac)]
print("Ardd=", mesFaces_Ardv)
# créer le mesh et l'objet
monMesh_Ardd = bpy.data.meshes.new("triangleMesh")
monObjet_Ardd = bpy.data.objects.new("Ardd", monMesh_Ardd)
# lier l'objet à la scène
maScene_Ardd = bpy.context.scene
maScene_Ardd.objects.link(monObjet_Ardd)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Ardd.from_pydata(coord, [], mesFaces_Ardd)
# -------------- Fin CODE : Matrice Zajec 5x5 --------------#

##############################################################
##############################################################
"""#------suite de l'étape 1------------------------------#"""
##############################################################
##############################################################

### B_matrice_b 3x3 de base
Ba = 0 + (5 * 10) + jh
Bb = Ba + (3 * 10)
Bc = Ba + (3 * 90) + (3 * 10)
Bd = Ba + (3 * 90)

maMatrice_b = [(Ba, Bb, Bc, Bd)]
print(maMatrice_b)
if face == 1:
    print("pasdeface")
else:
    # Voir la matrice_b
    # créer le mesh et l'objet
    monMesh_b = bpy.data.meshes.new("triangleMesh")
    monObjet_b = bpy.data.objects.new("maMatrice_b", monMesh_b)
    # lier l'objet à la scène
    maScene_b = bpy.context.scene
    maScene_b.objects.link(monObjet_b)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_b.from_pydata(coord, [], maMatrice_b)

##############################################################
# -------------- Debut CODE : Matrice Zajec 3 x 3 --------------#
##############################################################
### 1_Intro
### 1_définir les points utiles a la matrice 3x3
Baai = Ba
Babi = Baai + 10
Baci = Baai + 20
Badi = Baai + 30
Bbai = Baai + 90
Bbbi = Bbai + 10
Bbci = Bbai + 20
Bbdi = Bbai + 30
Bcai = Baai + 180
Bcbi = Bcai + 10
Bcci = Bcai + 20
Bcdi = Bcai + 30
Bdai = Baai + 270
Bdbi = Bdai + 10
Bdci = Bdai + 20
Bddi = Bdai + 30
### 2_création des point des faces sans hauteur
### 2.1_Face directrice avec diagonale
BaaChoix = [Bbai, Bbbi, Bbci, Bcai, Bcbi, Bcci, Bdai, Bdbi, Bdci]
Baa = random.choice(BaaChoix)
Bab = Baa + 10
Bac = Bab - 90
if Baa in [Bbai, Bbbi, Bbci]:
    Bad = Bbdi
elif Baa in [Bcai, Bcbi, Bcci]:
    Bad = Bcdi
else:
    Bad = Bddi
Bae = Bad - 90

# 2.2_Face directrice avec la diagonale vide
if Baa in [Bbai, Bcai, Bdai]:
    Baf = Baai
elif Baa in [Bbbi, Bcbi, Bdbi]:
    Baf = Babi
else:
    Baf = Baci
Bag = Baf + 10
### 3_  ajouter de la hauteur au face
### 3_ faire un random des 3 hauteur possibles au préalabe
Bhachoix = [i, j, k]
Bha = random.choice(Bhachoix)
Bhbchoix = [i, j]
Bhcchoix = [j, k]
Bhdchoix = [i, k]
if Bha == k:
    Bhb = random.choice(Bhbchoix)
elif Bha == i:
    Bhb = random.choice(Bhcchoix)
else:
    Bhb = random.choice(Bhdchoix)

if Bha == j and Bhb == i:
    Bhc = k
elif Bha == i and Bhb == i:
    Bhc = k
elif Bha == k and Bhb == i:
    Bhc = j
elif Bha == i and Bhb == k:
    Bhc = j
else:
    Bhc = i

### 2.3_Créer un rectangle à subidiviser en 2-2-1 quider par les diagonales directrice sur y Bry
if Bad == Bddi:
    Bah = Bdai
    Ban = Baa
    Baj = Baa
    print("prout1")
else:
    Bah = Bad - 30
    Baj = Bah + 10
    Bak = Bdbi
    Ban = Bad - 10
    Bao = Bdci

    ### 3.1_Donner de la Hauteur a Bry1,2,3 avec Bha,Bhb,Bhc
    # 3.1.1.1_Bryh1
    Bryh1a = Bah + Bha
    Bryh1b = Baj + Bha
    Bryh1c = Bak + Bha
    Bryh1d = Bdai + Bha

    # 3.1.1.2_Face Brxh1
    mesFaces_Bryh1 = [(Bryh1a, Bryh1b, Bryh1c, Bryh1d)]
    print("face Bryh1=", mesFaces_Bryh1)
    if Bha == 0:
        print("proutBryh1")
    else:
        # créer le mesh et l'objet
        monMesh_Bryh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Bryh1 = bpy.data.objects.new("Bryh1", monMesh_Bryh1)
        # lier l'objet à la scène
        maScene_Bryh1 = bpy.context.scene
        maScene_Bryh1.objects.link(monObjet_Bryh1)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Bryh1.from_pydata(coord, [], mesFaces_Bryh1)

    # 3.1.2.1_Bryh2
    Bryh2a = Ban + Bhb
    Bryh2b = Bad + Bhb
    Bryh2c = Bddi + Bhb
    Bryh2d = Bao + Bhb

    # 3.1.2.2_Face Bryh2
    mesFaces_Bryh2 = [(Bryh2a, Bryh2b, Bryh2c, Bryh2d)]
    print("face Bryh2=", mesFaces_Bryh2)
    if Bhb == 0:
        print("pasBryh2")
    else:
        # créer le mesh et l'objet
        monMesh_Bryh2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Bryh2 = bpy.data.objects.new("Bryh2", monMesh_Bryh2)
        # lier l'objet à la scène
        maScene_Bryh2 = bpy.context.scene
        maScene_Bryh2.objects.link(monObjet_Bryh2)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Bryh2.from_pydata(coord, [], mesFaces_Bryh2)

    # 3.1.3.1_Bryh3
    Bryh3a = Ban + Bhc
    Bryh3b = Baj + Bhc
    Bryh3c = Bak + Bhc
    Bryh3d = Bao + Bhc

    # 3.1.3.2_Face Bryh3
    mesFaces_Bryh3 = [(Bryh3a, Bryh3b, Bryh3c, Bryh3d)]
    print("face Bryh3=", mesFaces_Bryh3)
    if Bhc == 0:
        print("pasBryh3")
    else:
        # créer le mesh et l'objet
        monMesh_Bryh3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Bryh3 = bpy.data.objects.new("Bryh3", monMesh_Bryh3)
        # lier l'objet à la scène
        maScene_Bryh3 = bpy.context.scene
        maScene_Bryh3.objects.link(monObjet_Bryh3)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Bryh3.from_pydata(coord, [], mesFaces_Bryh3)

    ### 3.1.4_créer les 2 faces de connection
    # Face  entre Bryh3 et Bryh2 == Bryhv1
    mesFaces_Bryhv1 = [(Bryh3d, Bryh2d, Bryh2a, Bryh3a)]
    print("face Bryhv1=", mesFaces_Bryhv1)
    # créer le mesh et l'objet
    monMesh_Bryhv1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Bryhv1 = bpy.data.objects.new("Bryhv1", monMesh_Bryhv1)
    # lier l'objet à la scène
    maScene_Bryhv1 = bpy.context.scene
    maScene_Bryhv1.objects.link(monObjet_Bryhv1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Bryhv1.from_pydata(coord, [], mesFaces_Bryhv1)

    # 3.1.5_Face  entre Bryh1 et Bryh3 == Bryhv3
    mesFaces_Bryhv2 = [(Bryh1b, Bryh1c, Bryh3c, Bryh3b)]
    print("face Bryhv2=", mesFaces_Bryhv2)
    # créer le mesh et l'objet
    monMesh_Bryhv2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Bryhv2 = bpy.data.objects.new("Bryhv2", monMesh_Bryhv2)
    # lier l'objet à la scène
    maScene_Bryhv2 = bpy.context.scene
    maScene_Bryhv2.objects.link(monObjet_Bryhv2)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Bryhv2.from_pydata(coord, [], mesFaces_Bryhv2)

    # 3.1.6_Face  arrete Bryh2 == Bryhv3
    mesFaces_Bryhv3 = [(Bryh2b, Bryh2c, Bddi, Bad)]
    print("face Bryhv3=", mesFaces_Bryhv3)
    # créer le mesh et l'objet
    monMesh_Bryhv3 = bpy.data.meshes.new("triangleMesh")
    monObjet_Bryhv3 = bpy.data.objects.new("Bryhv3", monMesh_Bryhv3)
    # lier l'objet à la scène
    maScene_Bryhv3 = bpy.context.scene
    maScene_Bryhv3.objects.link(monObjet_Bryhv3)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Bryhv3.from_pydata(coord, [], mesFaces_Bryhv3)

    # 3.1.7_Face  arrete Bryh1 == Bryhv4
    mesFaces_Bryhv4 = [(Bryh1a, Bah, Bdai, Bryh1d)]
    print("face Bryhv4=", mesFaces_Bryhv4)
    # créer le mesh et l'objet
    monMesh_Bryhv4 = bpy.data.meshes.new("triangleMesh")
    monObjet_Bryhv4 = bpy.data.objects.new("Bryhv4", monMesh_Bryhv4)
    # lier l'objet à la scène
    maScene_Bryhv4 = bpy.context.scene
    maScene_Bryhv4.objects.link(monObjet_Bryhv4)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Bryhv4.from_pydata(coord, [], mesFaces_Bryhv4)

### 2.2_Créer un rectangle à subidiviser en 2-2-1 quider par les diagonales directrice sur y Brx
if Baf == Baai:
    print("Baf=Baai")
else:
    Bai = Bac - 10
    Bap = Bae - 30
    Bah = Bad - 30
    Baj = Baa
    Bak = Baf + 270

    # 3.2_Donner de la Hauteur a Bry1,2,3 avec Aha,Ahb,Ahc
    # 3.2.1.1_Brxh1
    Brxh1a = Bah + Bhb
    Brxh1b = Baj + Bhb
    Brxh1c = Bai + Bhb
    Brxh1d = Bap + Bhb

    # 3.2.1.2_Face Brxh1
    mesFaces_Brxh1 = [(Brxh1a, Brxh1b, Brxh1c, Brxh1d)]
    print("face Brxh1=", mesFaces_Brxh1)
    if Bhb == 0:
        print("pasBrxh1")
    else:
        # créer le mesh et l'objet
        monMesh_Brxh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Brxh1 = bpy.data.objects.new("Brxh1", monMesh_Brxh1)
        # lier l'objet à la scène
        maScene_Brxh1 = bpy.context.scene
        maScene_Brxh1.objects.link(monObjet_Brxh1)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Brxh1.from_pydata(coord, [], mesFaces_Brxh1)

    # 3.2.2.1_Brxh2
    Brxh2a = Baai + Bhc
    Brxh2b = Baf + Bhc
    Brxh2c = Bai + Bhc
    Brxh2d = Bap + Bhc

    # 3.2.2.2_Face Brxh2
    mesFaces_Brxh2 = [(Brxh2a, Brxh2b, Brxh2c, Brxh2d)]
    print("face Brxh2=", mesFaces_Brxh2)
    if Bhc == 0:
        print("pasBrxh2")
    else:
        # créer le mesh et l'objet
        monMesh_Brxh2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Brxh2 = bpy.data.objects.new("Brxh2", monMesh_Brxh2)
        # lier l'objet à la scène
        maScene_Brxh2 = bpy.context.scene
        maScene_Brxh2.objects.link(monObjet_Brxh2)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_Brxh2.from_pydata(coord, [], mesFaces_Brxh2)

    # 3.2.3_Face  entre Brxh1 et Brxh2 == Brxhv1
    mesFaces_Brxhv1 = [(Brxh1d, Brxh1c, Brxh2c, Brxh2d)]
    print("face Brxhv1=", mesFaces_Brxhv1)
    # créer le mesh et l'objet
    monMesh_Brxhv1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv1 = bpy.data.objects.new("Brxhv1", monMesh_Brxhv1)
    # lier l'objet à la scène
    maScene_Brxhv1 = bpy.context.scene
    maScene_Brxhv1.objects.link(monObjet_Brxhv1)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv1.from_pydata(coord, [], mesFaces_Brxhv1)

    # 3.2.4_Face  arrete Brxh1 == Brxhv2
    mesFaces_Brxhv2 = [(Brxh1a, Brxh1b, Baa, Bah)]
    print("face Brxhv2=", mesFaces_Brxhv2)
    # créer le mesh et l'objet
    monMesh_Brxhv2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv2 = bpy.data.objects.new("Brxhv2", monMesh_Brxhv2)
    # lier l'objet à la scène
    maScene_Brxhv2 = bpy.context.scene
    maScene_Brxhv2.objects.link(monObjet_Brxhv2)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv2.from_pydata(coord, [], mesFaces_Brxhv2)

    # 3.2.5_Face  arrete Brxh1 == Brxhv3
    mesFaces_Brxhv3 = [(Brxh1c, Brxh1b, Baa, Bai)]
    print("face Brxhv3=", mesFaces_Brxhv3)
    # créer le mesh et l'objet
    monMesh_Brxhv3 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv3 = bpy.data.objects.new("Brxhv3", monMesh_Brxhv3)
    # lier l'objet à la scène
    maScene_Brxhv3 = bpy.context.scene
    maScene_Brxhv3.objects.link(monObjet_Brxhv3)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv3.from_pydata(coord, [], mesFaces_Brxhv3)

    # 3.2.6_Face  arrete Brxh1  == Brxhv4
    mesFaces_Brxhv44 = [(Bah, Brxh1a, Brxh1d, Bap)]
    print("face Brxhv44=", mesFaces_Brxhv44)
    # créer le mesh et l'objet
    monMesh_Brxhv44 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv44 = bpy.data.objects.new("Brxhv44", monMesh_Brxhv44)
    # lier l'objet à la scène
    maScene_Brxhv44 = bpy.context.scene
    maScene_Brxhv44.objects.link(monObjet_Brxhv44)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv44.from_pydata(coord, [], mesFaces_Brxhv44)

    # 3.2.7_Face  arrete Brxh2 == Brxhv5
    mesFaces_Brxhv5 = [(Brxh2c, Brxh2b, Baf, Bai)]
    print("face Brxhv5=", mesFaces_Brxhv5)
    # créer le mesh et l'objet
    monMesh_Brxhv5 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv5 = bpy.data.objects.new("Brxhv5", monMesh_Brxhv5)
    # lier l'objet à la scène
    maScene_Brxhv5 = bpy.context.scene
    maScene_Brxhv5.objects.link(monObjet_Brxhv5)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv5.from_pydata(coord, [], mesFaces_Brxhv5)

    # 3.2.8_Face  arrete Brxh2  == Brxhv6
    mesFaces_Brxhv6 = [(Baai, Brxh2a, Brxh2d, Bap)]
    print("face Brxhv6=", mesFaces_Brxhv6)
    # créer le mesh et l'objet
    monMesh_Brxhv6 = bpy.data.meshes.new("triangleMesh")
    monObjet_Brxhv6 = bpy.data.objects.new("Brxhv6", monMesh_Brxhv6)
    # lier l'objet à la scène
    maScene_Brxhv6 = bpy.context.scene
    maScene_Brxhv6.objects.link(monObjet_Brxhv6)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brxhv6.from_pydata(coord, [], mesFaces_Brxhv6)

### 3.3_Créer un rectangle entre les deux diago  directrice Brr avec une hauteur de Hj
Brrhc = Bae + Hi
Brrhd = Bac + Hi
if Bag == Badi:
    Brrha = Bag + Hi
    Brrhb = Badi + Hi
    print("Bag=Badi")
else:
    Brrha = Bag + Hi
    Brrhb = Badi + Hi

    # Face Brrh
    mesFaces_Brrh = [(Brrha, Brrhb, Brrhc, Brrhd)]
    print("face Brrh=", mesFaces_Brrh)
    # créer le mesh et l'objet
    monMesh_Brrh = bpy.data.meshes.new("triangleMesh")
    monObjet_Brrh = bpy.data.objects.new("Brrh", monMesh_Brrh)
    # lier l'objet à la scène
    maScene_Brrh = bpy.context.scene
    maScene_Brrh.objects.link(monObjet_Brrh)
    # "remplir" le mesh avec les informations vertices et faces
    monMesh_Brrh.from_pydata(coord, [], mesFaces_Brrh)

### 3.4_créer la face de la diago directrice
# Brd
# 3.4.1_Face Brdh
mesFaces_Brdh = [(Baa, Brrhd, Brrhc, Bad)]
print("face Brdh=", mesFaces_Brdh)
# créer le mesh et l'objet
monMesh_Brdh = bpy.data.meshes.new("triangleMesh")
monObjet_Brdh = bpy.data.objects.new("Brdh", monMesh_Brdh)
# lier l'objet à la scène
maScene_Brdh = bpy.context.scene
maScene_Brdh.objects.link(monObjet_Brdh)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Brdh.from_pydata(coord, [], mesFaces_Brdh)

# 3.4.2_Face Brdd
mesFaces_Brdd = [(Baa, Brrhd, Bac)]
print("face Brdd=", mesFaces_Brdd)
# créer le mesh et l'objet
monMesh_Brdd = bpy.data.meshes.new("triangleMesh")
monObjet_Brdd = bpy.data.objects.new("Brdd", monMesh_Brdd)
# lier l'objet à la scène
maScene_Brdd = bpy.context.scene
maScene_Brdd.objects.link(monObjet_Brdd)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Brdd.from_pydata(coord, [], mesFaces_Brdd)

# 3.4.3_Face arrete Brhh Brhh1
mesFaces_Brhh1 = [(Bac, Bag, Brrha, Brrhd)]
print("face Brhh1=", mesFaces_Brhh1)
# créer le mesh et l'objet
monMesh_Brhh1 = bpy.data.meshes.new("triangleMesh")
monObjet_Brhh1 = bpy.data.objects.new("Brhh1", monMesh_Brhh1)
# lier l'objet à la scène
maScene_Brhh1 = bpy.context.scene
maScene_Brhh1.objects.link(monObjet_Brhh1)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Brhh1.from_pydata(coord, [], mesFaces_Brhh1)

# 3.4.4_Face arrete Brhh == Brhh2
mesFaces_Brhh2 = [(Bad, Bae, Badi, Brrhb, Brrhc)]
print("face Brhh2=", mesFaces_Brhh2)
# créer le mesh et l'objet
monMesh_Brhh2 = bpy.data.meshes.new("triangleMesh")
monObjet_Brhh2 = bpy.data.objects.new("Brhh2", monMesh_Brhh2)
# lier l'objet à la scène
maScene_Brhh2 = bpy.context.scene
maScene_Brhh2.objects.link(monObjet_Brhh2)
# "remplir" le mesh avec les informations vertices et faces
monMesh_Brhh2.from_pydata(coord, [], mesFaces_Brhh2)
# -------------- Fin CODE : Matrice Zajec 3x3 --------------#

##############################################################
##############################################################
"""#-------------- ETAPE 2 : Option 1---------------------#"""
##############################################################
##############################################################

# ------- CODE : OPTION 1 de la matrice-----------#

if op in [2, 3]:
    print("pasloption1")
else:
    ### A_2eme matrice 3x3
    Cd = (5 * 90) + (5 * 10) + kh
    Ca = Cd - (3 * 90)
    Cb = Ca + (3 * 10)
    Cc = Cd + (3 * 10)
    maMatrice_c = [(Ca, Cb, Cc, Cd)]
    print(maMatrice_c)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_c
        # créer le mesh et l'objet
        monMesh_c = bpy.data.meshes.new("triangleMesh")
        monObjet_c = bpy.data.objects.new("maMatrice_c", monMesh_c)
        # lier l'objet à la scène
        maScene_c = bpy.context.scene
        maScene_c.objects.link(monObjet_c)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_c.from_pydata(coord, [], maMatrice_c)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Caai = Ca
    Cabi = Caai + 10
    Caci = Caai + 20
    Cadi = Caai + 30
    Cbai = Caai + 90
    Cbbi = Cbai + 10
    Cbci = Cbai + 20
    Cbdi = Cbai + 30
    Ccai = Caai + 180
    Ccbi = Ccai + 10
    Ccci = Ccai + 20
    Ccdi = Ccai + 30
    Cdai = Caai + 270
    Cdbi = Cdai + 10
    Cdci = Cdai + 20
    Cddi = Cdai + 30

    CaaChoix = [Cbai, Cbbi, Cbci, Ccai, Ccbi, Ccci, Cdai, Cdbi, Cdci]
    Caa = random.choice(CaaChoix)
    Cab = Caa + 10
    Cac = Cab - 90
    if Caa in [Cbai, Cbbi, Cbci]:
        Cad = Cbdi
    elif Caa in [Ccai, Ccbi, Ccci]:
        Cad = Ccdi
    else:
        Cad = Cddi
    Cae = Cad - 90

    if Caa in [Cbai, Ccai, Cdai]:
        Caf = Caai
    elif Caa in [Cbbi, Ccbi, Cdbi]:
        Caf = Cabi
    else:
        Caf = Caci
    Cag = Caf + 10

    Chachoix = [i, j, k]
    Cha = random.choice(Chachoix)
    Chbchoix = [i, j]
    Chcchoix = [j, k]
    Chdchoix = [i, k]

    if Cha == k:
        Chb = random.choice(Chbchoix)
    elif Cha == i:
        Chb = random.choice(Chcchoix)
    else:
        Chb = random.choice(Chdchoix)
    if Cha == j and Chb == i:
        Chc = k
    elif Cha == i and Chb == i:
        Chc = k
    elif Cha == k and Chb == i:
        Chc = j
    elif Cha == i and Chb == k:
        Chc = j
    else:
        Chc = i

    if Cad == Cddi:
        Cah = Cdai
        Can = Caa
        Caj = Caa
        print("prout1")
    else:
        Cah = Cad - 30
        Caj = Cah + 10
        Cak = Cdbi
        Can = Cad - 10
        Cao = Cdci

        Cryh1a = Cah + Cha
        Cryh1b = Caj + Cha
        Cryh1c = Cak + Cha
        Cryh1d = Cdai + Cha

        mesFaces_Cryh1 = [(Cryh1a, Cryh1b, Cryh1c, Cryh1d)]
        print("face Cryh1=", mesFaces_Cryh1)
        if Cha == 0:
            print("proutCryh1")
        else:
            monMesh_Cryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh1 = bpy.data.objects.new("Cryh1", monMesh_Cryh1)
            maScene_Cryh1 = bpy.context.scene
            maScene_Cryh1.objects.link(monObjet_Cryh1)
            monMesh_Cryh1.from_pydata(coord, [], mesFaces_Cryh1)

        Cryh2a = Can + Chb
        Cryh2b = Cad + Chb
        Cryh2c = Cddi + Chb
        Cryh2d = Cao + Chb

        mesFaces_Cryh2 = [(Cryh2a, Cryh2b, Cryh2c, Cryh2d)]
        print("face Cryh2=", mesFaces_Cryh2)
        if Chb == 0:
            print("proutCryh2")
        else:
            monMesh_Cryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh2 = bpy.data.objects.new("Cryh2", monMesh_Cryh2)
            maScene_Cryh2 = bpy.context.scene
            maScene_Cryh2.objects.link(monObjet_Cryh2)
            monMesh_Cryh2.from_pydata(coord, [], mesFaces_Cryh2)

        Cryh3a = Can + Chc
        Cryh3b = Caj + Chc
        Cryh3c = Cak + Chc
        Cryh3d = Cao + Chc

        mesFaces_Cryh3 = [(Cryh3a, Cryh3b, Cryh3c, Cryh3d)]
        print("face Cryh3=", mesFaces_Cryh3)
        if Chc == 0:
            print("proutCryh3")
        else:
            monMesh_Cryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh3 = bpy.data.objects.new("Cryh3", monMesh_Cryh3)
            maScene_Cryh3 = bpy.context.scene
            maScene_Cryh3.objects.link(monObjet_Cryh3)
            monMesh_Cryh3.from_pydata(coord, [], mesFaces_Cryh3)

        mesFaces_Cryhv1 = [(Cryh3d, Cryh2d, Cryh2a, Cryh3a)]
        print("face Cryhv1=", mesFaces_Cryhv1)
        monMesh_Cryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv1 = bpy.data.objects.new("Cryhv1", monMesh_Cryhv1)
        maScene_Cryhv1 = bpy.context.scene
        maScene_Cryhv1.objects.link(monObjet_Cryhv1)
        monMesh_Cryhv1.from_pydata(coord, [], mesFaces_Cryhv1)

        mesFaces_Cryhv2 = [(Cryh1b, Cryh1c, Cryh3c, Cryh3b)]
        print("face Cryhv2=", mesFaces_Cryhv2)
        monMesh_Cryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv2 = bpy.data.objects.new("Cryhv2", monMesh_Cryhv2)
        maScene_Cryhv2 = bpy.context.scene
        maScene_Cryhv2.objects.link(monObjet_Cryhv2)
        monMesh_Cryhv2.from_pydata(coord, [], mesFaces_Cryhv2)

        mesFaces_Cryhv3 = [(Cryh2b, Cryh2c, Cddi, Cad)]
        print("face Cryhv3=", mesFaces_Cryhv3)
        monMesh_Cryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv3 = bpy.data.objects.new("Cryhv3", monMesh_Cryhv3)
        maScene_Cryhv3 = bpy.context.scene
        maScene_Cryhv3.objects.link(monObjet_Cryhv3)
        monMesh_Cryhv3.from_pydata(coord, [], mesFaces_Cryhv3)

        mesFaces_Cryhv4 = [(Cryh1a, Cah, Cdai, Cryh1d)]
        print("face Cryhv4=", mesFaces_Cryhv4)
        monMesh_Cryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv4 = bpy.data.objects.new("Cryhv4", monMesh_Cryhv4)
        maScene_Cryhv4 = bpy.context.scene
        maScene_Cryhv4.objects.link(monObjet_Cryhv4)
        monMesh_Cryhv4.from_pydata(coord, [], mesFaces_Cryhv4)

    if Caf == Caai:
        print("prout5")
    else:
        Cai = Cac - 10
        Cap = Cae - 30
        Cah = Cad - 30
        Caj = Caa
        Cak = Caf + 270

        Crxh1a = Cah + Chb
        Crxh1b = Caj + Chb
        Crxh1c = Cai + Chb
        Crxh1d = Cap + Chb

        mesFaces_Crxh1 = [(Crxh1a, Crxh1b, Crxh1c, Crxh1d)]
        print("face Crxh1=", mesFaces_Crxh1)
        if Chb == 0:
            print("proutCrxh1")
        else:
            monMesh_Crxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh1 = bpy.data.objects.new("Crxh1", monMesh_Crxh1)
            maScene_Crxh1 = bpy.context.scene
            maScene_Crxh1.objects.link(monObjet_Crxh1)
            monMesh_Crxh1.from_pydata(coord, [], mesFaces_Crxh1)

        Crxh2a = Caai + Chc
        Crxh2b = Caf + Chc
        Crxh2c = Cai + Chc
        Crxh2d = Cap + Chc

        mesFaces_Crxh2 = [(Crxh2a, Crxh2b, Crxh2c, Crxh2d)]
        print("face Crxh2=", mesFaces_Crxh2)
        if Chc == 0:
            print("proutCrxh2")
        else:
            monMesh_Crxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh2 = bpy.data.objects.new("Crxh2", monMesh_Crxh2)
            maScene_Crxh2 = bpy.context.scene
            maScene_Crxh2.objects.link(monObjet_Crxh2)
            monMesh_Crxh2.from_pydata(coord, [], mesFaces_Crxh2)

        mesFaces_Crxhv1 = [(Crxh1d, Crxh1c, Crxh2c, Crxh2d)]
        print("face Crxhv1=", mesFaces_Crxhv1)
        monMesh_Crxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv1 = bpy.data.objects.new("Crxhv1", monMesh_Crxhv1)
        maScene_Crxhv1 = bpy.context.scene
        maScene_Crxhv1.objects.link(monObjet_Crxhv1)
        monMesh_Crxhv1.from_pydata(coord, [], mesFaces_Crxhv1)

        mesFaces_Crxhv2 = [(Crxh1a, Crxh1b, Caa, Cah)]
        print("face Crxhv2=", mesFaces_Crxhv2)
        monMesh_Crxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv2 = bpy.data.objects.new("Crxhv2", monMesh_Crxhv2)
        maScene_Crxhv2 = bpy.context.scene
        maScene_Crxhv2.objects.link(monObjet_Crxhv2)
        monMesh_Crxhv2.from_pydata(coord, [], mesFaces_Crxhv2)

        mesFaces_Crxhv3 = [(Crxh1c, Crxh1b, Caa, Cai)]
        print("face Crxhv3=", mesFaces_Crxhv3)
        monMesh_Crxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv3 = bpy.data.objects.new("Crxhv3", monMesh_Crxhv3)
        maScene_Crxhv3 = bpy.context.scene
        maScene_Crxhv3.objects.link(monObjet_Crxhv3)
        monMesh_Crxhv3.from_pydata(coord, [], mesFaces_Crxhv3)

        mesFaces_Crxhv44 = [(Cah, Crxh1a, Crxh1d, Cap)]
        print("face Crxhv44=", mesFaces_Crxhv44)
        monMesh_Crxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv44 = bpy.data.objects.new("Crxhv44", monMesh_Crxhv44)
        maScene_Crxhv44 = bpy.context.scene
        maScene_Crxhv44.objects.link(monObjet_Crxhv44)

        monMesh_Crxhv44.from_pydata(coord, [], mesFaces_Crxhv44)

        mesFaces_Crxhv5 = [(Crxh2c, Crxh2b, Caf, Cai)]
        print("face Crxhv5=", mesFaces_Crxhv5)
        monMesh_Crxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv5 = bpy.data.objects.new("Crxhv5", monMesh_Crxhv5)
        maScene_Crxhv5 = bpy.context.scene
        maScene_Crxhv5.objects.link(monObjet_Crxhv5)
        monMesh_Crxhv5.from_pydata(coord, [], mesFaces_Crxhv5)

        mesFaces_Crxhv6 = [(Caai, Crxh2a, Crxh2d, Cap)]
        print("face Crxhv6=", mesFaces_Crxhv6)
        monMesh_Crxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv6 = bpy.data.objects.new("Crxhv6", monMesh_Crxhv6)
        maScene_Crxhv6 = bpy.context.scene
        maScene_Crxhv6.objects.link(monObjet_Crxhv6)
        monMesh_Crxhv6.from_pydata(coord, [], mesFaces_Crxhv6)

    Crrhc = Cae + Hj
    Crrhd = Cac + Hj
    Crrha = Cag + Hj
    Crrhb = Cadi + Hj
    if Cag == Cadi:
        print("prout6")
    else:
        mesFaces_Crrh = [(Crrha, Crrhb, Crrhc, Crrhd)]
        print("face Crrh=", mesFaces_Crrh)
        monMesh_Crrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Crrh = bpy.data.objects.new("Crrh", monMesh_Crrh)
        maScene_Crrh = bpy.context.scene
        maScene_Crrh.objects.link(monObjet_Crrh)
        monMesh_Crrh.from_pydata(coord, [], mesFaces_Crrh)

    mesFaces_Crdh = [(Caa, Crrhd, Crrhc, Cad)]
    print("face Crdh=", mesFaces_Crdh)
    monMesh_Crdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdh = bpy.data.objects.new("Crdh", monMesh_Crdh)
    maScene_Crdh = bpy.context.scene
    maScene_Crdh.objects.link(monObjet_Crdh)
    monMesh_Crdh.from_pydata(coord, [], mesFaces_Crdh)

    mesFaces_Crdd = [(Caa, Crrhd, Cac)]
    print("face Crdd=", mesFaces_Crdd)
    monMesh_Crdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdd = bpy.data.objects.new("Crdd", monMesh_Crdd)
    maScene_Crdd = bpy.context.scene
    maScene_Crdd.objects.link(monObjet_Crdd)
    monMesh_Crdd.from_pydata(coord, [], mesFaces_Crdd)

    mesFaces_Crhh1 = [(Cac, Cag, Crrha, Crrhd)]
    print("face Crhh1=", mesFaces_Crhh1)
    monMesh_Crhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Crhh1 = bpy.data.objects.new("Crhh1", monMesh_Crhh1)
    maScene_Crhh1 = bpy.context.scene
    maScene_Crhh1.objects.link(monObjet_Crhh1)
    monMesh_Crhh1.from_pydata(coord, [], mesFaces_Crhh1)

    mesFaces_Crhh2 = [(Cad, Cae, Cadi, Crrhb, Crrhc)]
    print("face Crhh2=", mesFaces_Crhh2)
    monMesh_Crhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Crhh2 = bpy.data.objects.new("Crhh2", monMesh_Crhh2)
    maScene_Crhh2 = bpy.context.scene
    maScene_Crhh2.objects.link(monObjet_Crhh2)
    monMesh_Crhh2.from_pydata(coord, [], mesFaces_Crhh2)
    # -------------- Fin CODE : Matrice Zajec 3x3 --------------#

    ##############################################################
    ##############################################################
    # -------------- Suite Etape 2: Option 1--------------------#
    ##############################################################
    ##############################################################

    ### B_3eme matrice 3x3

    Da = (5 * 90) + lh
    Db = Da + (3 * 10)
    Dc = Da + (3 * 90) + (3 * 10)
    Dd = Da + (3 * 90)

    maMatrice_d = [(Da, Db, Dc, Dd)]
    print(maMatrice_d)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_d
        # créer le mesh et l'objet
        monMesh_d = bpy.data.meshes.new("triangleMesh")
        monObjet_d = bpy.data.objects.new("maMatrice_d", monMesh_d)
        # lier l'objet à la scène
        maScene_d = bpy.context.scene
        maScene_d.objects.link(monObjet_d)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_d.from_pydata(coord, [], maMatrice_d)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Daai = Da
    Dabi = Daai + 10
    Daci = Daai + 20
    Dadi = Daai + 30
    Dbai = Daai + 90
    Dbbi = Dbai + 10
    Dbci = Dbai + 20
    Dbdi = Dbai + 30
    Dcai = Daai + 180
    Dcbi = Dcai + 10
    Dcci = Dcai + 20
    Dcdi = Dcai + 30
    Ddai = Daai + 270
    Ddbi = Ddai + 10
    Ddci = Ddai + 20
    Dddi = Ddai + 30

    DaaChoix = [Dbai, Dbbi, Dbci, Dcai, Dcbi, Dcci, Ddai, Ddbi, Ddci]
    Daa = random.choice(DaaChoix)
    Dab = Daa + 10
    Dac = Dab - 90
    if Daa in [Dbai, Dbbi, Dbci]:
        Dad = Dbdi
    elif Daa in [Dcai, Dcbi, Dcci]:
        Dad = Dcdi
    else:
        Dad = Dddi
    Dae = Dad - 90

    if Daa in [Dbai, Dcai, Ddai]:
        Daf = Daai
    elif Daa in [Dbbi, Dcbi, Ddbi]:
        Daf = Dabi
    else:
        Daf = Daci
    Dag = Daf + 10

    Dhachoix = [i, j, k]
    Dha = random.choice(Dhachoix)
    Dhbchoix = [i, j]
    Dhcchoix = [j, k]
    Dhdchoix = [i, k]

    if Dha == k:
        Dhb = random.choice(Dhbchoix)
    elif Dha == i:
        Dhb = random.choice(Dhcchoix)
    else:
        Dhb = random.choice(Dhdchoix)

    if Dha == j and Dhb == i:
        Dhc = k
    elif Dha == i and Dhb == i:
        Dhc = k
    elif Dha == k and Dhb == i:
        Dhc = j
    elif Dha == i and Dhb == k:
        Dhc = j
    else:
        Dhc = i

    if Dad == Dddi:
        Dah = Ddai
        Dan = Daa
        Daj = Daa
        print("prout1")
    else:
        Dah = Dad - 30
        Daj = Dah + 10
        Dak = Ddbi
        Dan = Dad - 10
        Dao = Ddci

        Dryh1a = Dah + Dha
        Dryh1b = Daj + Dha
        Dryh1c = Dak + Dha
        Dryh1d = Ddai + Dha

        mesFaces_Dryh1 = [(Dryh1a, Dryh1b, Dryh1c, Dryh1d)]
        print("face Dryh1=", mesFaces_Dryh1)
        if Dha == 0:
            print("proutDryh1")
        else:
            monMesh_Dryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh1 = bpy.data.objects.new("Dryh1", monMesh_Dryh1)
            maScene_Dryh1 = bpy.context.scene
            maScene_Dryh1.objects.link(monObjet_Dryh1)
            monMesh_Dryh1.from_pydata(coord, [], mesFaces_Dryh1)

        Dryh2a = Dan + Dhb
        Dryh2b = Dad + Dhb
        Dryh2c = Dddi + Dhb
        Dryh2d = Dao + Dhb

        mesFaces_Dryh2 = [(Dryh2a, Dryh2b, Dryh2c, Dryh2d)]
        print("face Dryh2=", mesFaces_Dryh2)
        if Dhb == 0:
            print("proutDryh2")
        else:
            monMesh_Dryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh2 = bpy.data.objects.new("Dryh2", monMesh_Dryh2)
            maScene_Dryh2 = bpy.context.scene
            maScene_Dryh2.objects.link(monObjet_Dryh2)
            monMesh_Dryh2.from_pydata(coord, [], mesFaces_Dryh2)

        Dryh3a = Dan + Dhc
        Dryh3b = Daj + Dhc
        Dryh3c = Dak + Dhc
        Dryh3d = Dao + Dhc

        mesFaces_Dryh3 = [(Dryh3a, Dryh3b, Dryh3c, Dryh3d)]
        print("face Dryh3=", mesFaces_Dryh3)
        if Dhc == 0:
            print("proutDryh3")
        else:
            monMesh_Dryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh3 = bpy.data.objects.new("Dryh3", monMesh_Dryh3)
            maScene_Dryh3 = bpy.context.scene
            maScene_Dryh3.objects.link(monObjet_Dryh3)
            monMesh_Dryh3.from_pydata(coord, [], mesFaces_Dryh3)

        mesFaces_Dryhv1 = [(Dryh3d, Dryh2d, Dryh2a, Dryh3a)]
        print("face Dryhv1=", mesFaces_Dryhv1)
        monMesh_Dryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv1 = bpy.data.objects.new("Dryhv1", monMesh_Dryhv1)
        maScene_Dryhv1 = bpy.context.scene
        maScene_Dryhv1.objects.link(monObjet_Dryhv1)
        monMesh_Dryhv1.from_pydata(coord, [], mesFaces_Dryhv1)

        mesFaces_Dryhv2 = [(Dryh1b, Dryh1c, Dryh3c, Dryh3b)]
        print("face Dryhv2=", mesFaces_Dryhv2)
        monMesh_Dryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv2 = bpy.data.objects.new("Dryhv2", monMesh_Dryhv2)
        maScene_Dryhv2 = bpy.context.scene
        maScene_Dryhv2.objects.link(monObjet_Dryhv2)
        monMesh_Dryhv2.from_pydata(coord, [], mesFaces_Dryhv2)

        mesFaces_Dryhv3 = [(Dryh2b, Dryh2c, Dddi, Dad)]
        print("face Dryhv3=", mesFaces_Dryhv3)
        monMesh_Dryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv3 = bpy.data.objects.new("Dryhv3", monMesh_Dryhv3)
        maScene_Dryhv3 = bpy.context.scene
        maScene_Dryhv3.objects.link(monObjet_Dryhv3)
        monMesh_Dryhv3.from_pydata(coord, [], mesFaces_Dryhv3)

        mesFaces_Dryhv4 = [(Dryh1a, Dah, Ddai, Dryh1d)]
        print("face Dryhv4=", mesFaces_Dryhv4)
        monMesh_Dryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv4 = bpy.data.objects.new("Dryhv4", monMesh_Dryhv4)
        maScene_Dryhv4 = bpy.context.scene
        maScene_Dryhv4.objects.link(monObjet_Dryhv4)
        monMesh_Dryhv4.from_pydata(coord, [], mesFaces_Dryhv4)

    if Daf == Daai:
        print("prout5")
    else:
        Dai = Dac - 10
        Dap = Dae - 30
        Dah = Dad - 30
        Daj = Daa
        Dak = Daf + 270

        Drxh1a = Dah + Dhb
        Drxh1b = Daj + Dhb
        Drxh1c = Dai + Dhb
        Drxh1d = Dap + Dhb

        mesFaces_Drxh1 = [(Drxh1a, Drxh1b, Drxh1c, Drxh1d)]
        print("face Drxh1=", mesFaces_Drxh1)
        if Dhb == 0:
            print("proutDrxh1")
        else:
            monMesh_Drxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Drxh1 = bpy.data.objects.new("Drxh1", monMesh_Drxh1)
            maScene_Drxh1 = bpy.context.scene
            maScene_Drxh1.objects.link(monObjet_Drxh1)
            monMesh_Drxh1.from_pydata(coord, [], mesFaces_Drxh1)

        Drxh2a = Daai + Dhc
        Drxh2b = Daf + Dhc
        Drxh2c = Dai + Dhc
        Drxh2d = Dap + Dhc

        mesFaces_Drxh2 = [(Drxh2a, Drxh2b, Drxh2c, Drxh2d)]
        print("face Drxh2=", mesFaces_Drxh2)
        if Dhc == 0:
            print("proutDrxh2")
        else:
            monMesh_Drxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Drxh2 = bpy.data.objects.new("Drxh2", monMesh_Drxh2)
            maScene_Drxh2 = bpy.context.scene
            maScene_Drxh2.objects.link(monObjet_Drxh2)
            monMesh_Drxh2.from_pydata(coord, [], mesFaces_Drxh2)

        mesFaces_Drxhv1 = [(Drxh1d, Drxh1c, Drxh2c, Drxh2d)]
        print("face Drxhv1=", mesFaces_Drxhv1)
        monMesh_Drxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv1 = bpy.data.objects.new("Drxhv1", monMesh_Drxhv1)
        maScene_Drxhv1 = bpy.context.scene
        maScene_Drxhv1.objects.link(monObjet_Drxhv1)
        monMesh_Drxhv1.from_pydata(coord, [], mesFaces_Drxhv1)

        mesFaces_Drxhv2 = [(Drxh1a, Drxh1b, Daa, Dah)]
        print("face Drxhv2=", mesFaces_Drxhv2)
        monMesh_Drxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv2 = bpy.data.objects.new("Drxhv2", monMesh_Drxhv2)
        maScene_Drxhv2 = bpy.context.scene
        maScene_Drxhv2.objects.link(monObjet_Drxhv2)
        monMesh_Drxhv2.from_pydata(coord, [], mesFaces_Drxhv2)

        mesFaces_Drxhv3 = [(Drxh1c, Drxh1b, Daa, Dai)]
        print("face Drxhv3=", mesFaces_Drxhv3)
        monMesh_Drxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv3 = bpy.data.objects.new("Drxhv3", monMesh_Drxhv3)
        maScene_Drxhv3 = bpy.context.scene
        maScene_Drxhv3.objects.link(monObjet_Drxhv3)
        monMesh_Drxhv3.from_pydata(coord, [], mesFaces_Drxhv3)

        mesFaces_Drxhv44 = [(Dah, Drxh1a, Drxh1d, Dap)]
        print("face Drxhv44=", mesFaces_Drxhv44)
        monMesh_Drxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv44 = bpy.data.objects.new("Drxhv44", monMesh_Drxhv44)
        maScene_Drxhv44 = bpy.context.scene
        maScene_Drxhv44.objects.link(monObjet_Drxhv44)
        monMesh_Drxhv44.from_pydata(coord, [], mesFaces_Drxhv44)

        mesFaces_Drxhv5 = [(Drxh2c, Drxh2b, Daf, Dai)]
        print("face Drxhv5=", mesFaces_Drxhv5)
        monMesh_Drxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv5 = bpy.data.objects.new("Drxhv5", monMesh_Drxhv5)
        maScene_Drxhv5 = bpy.context.scene
        maScene_Drxhv5.objects.link(monObjet_Drxhv5)
        monMesh_Drxhv5.from_pydata(coord, [], mesFaces_Drxhv5)

        mesFaces_Drxhv6 = [(Daai, Drxh2a, Drxh2d, Dap)]
        print("face Drxhv6=", mesFaces_Drxhv6)
        monMesh_Drxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv6 = bpy.data.objects.new("Drxhv6", monMesh_Drxhv6)
        maScene_Drxhv6 = bpy.context.scene
        maScene_Drxhv6.objects.link(monObjet_Drxhv6)
        monMesh_Drxhv6.from_pydata(coord, [], mesFaces_Drxhv6)

    Drrhc = Dae + Hk
    Drrhd = Dac + Hk
    Drrha = Dag + Hk
    Drrhb = Dadi + Hk
    if Dag == Dadi:
        print("prout6")
    else:
        mesFaces_Drrh = [(Drrha, Drrhb, Drrhc, Drrhd)]
        print("face Drrh=", mesFaces_Drrh)
        monMesh_Drrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Drrh = bpy.data.objects.new("Drrh", monMesh_Drrh)
        maScene_Drrh = bpy.context.scene
        maScene_Drrh.objects.link(monObjet_Drrh)
        monMesh_Drrh.from_pydata(coord, [], mesFaces_Drrh)

    mesFaces_Drdh = [(Daa, Drrhd, Drrhc, Dad)]
    print("face Drdh=", mesFaces_Drdh)
    monMesh_Drdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Drdh = bpy.data.objects.new("Drdh", monMesh_Drdh)
    maScene_Drdh = bpy.context.scene
    maScene_Drdh.objects.link(monObjet_Drdh)
    monMesh_Drdh.from_pydata(coord, [], mesFaces_Drdh)

    mesFaces_Drdd = [(Daa, Drrhd, Dac)]
    print("face Drdd=", mesFaces_Drdd)
    monMesh_Drdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Drdd = bpy.data.objects.new("Drdd", monMesh_Drdd)
    maScene_Drdd = bpy.context.scene
    maScene_Drdd.objects.link(monObjet_Drdd)
    monMesh_Drdd.from_pydata(coord, [], mesFaces_Drdd)

    mesFaces_Drhh1 = [(Dac, Dag, Drrha, Drrhd)]
    print("face Drhh1=", mesFaces_Drhh1)
    monMesh_Drhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Drhh1 = bpy.data.objects.new("Drhh1", monMesh_Drhh1)
    maScene_Drhh1 = bpy.context.scene
    maScene_Drhh1.objects.link(monObjet_Drhh1)
    monMesh_Drhh1.from_pydata(coord, [], mesFaces_Drhh1)

    mesFaces_Drhh2 = [(Dad, Dae, Dadi, Drrhb, Drrhc)]
    print("face Drhh2=", mesFaces_Drhh2)
    monMesh_Drhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Drhh2 = bpy.data.objects.new("Drhh2", monMesh_Drhh2)
    maScene_Drhh2 = bpy.context.scene
    maScene_Drhh2.objects.link(monObjet_Drhh2)
    monMesh_Drhh2.from_pydata(coord, [], mesFaces_Drhh2)

    # -------------- Fin CODE : Matrice Zajec 3x3 --------------#

    ##############################################################
    ##############################################################
    # -------------- SUITE Etape 2 : option 1 --------------#
    ##############################################################
    ##############################################################

    ### C_4eme matrice 3x3

    Ea = (5 * 90) + (2 * 10) + mh
    Eb = Ea + (3 * 10)
    Ec = Ea + (3 * 90) + (3 * 10)
    Ed = Ea + (3 * 90)

    maMatrice_e = [(Ea, Eb, Ec, Ed)]
    print(maMatrice_e)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_e
        # créer le mesh et l'objet
        monMesh_e = bpy.data.meshes.new("triangleMesh")
        monObjet_e = bpy.data.objects.new("maMatrice_e", monMesh_e)
        # lier l'objet à la scène
        maScene_e = bpy.context.scene
        maScene_e.objects.link(monObjet_e)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_e.from_pydata(coord, [], maMatrice_e)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Eaai = Ea
    Eabi = Eaai + 10
    Eaci = Eaai + 20
    Eadi = Eaai + 30
    Ebai = Eaai + 90
    Ebbi = Ebai + 10
    Ebci = Ebai + 20
    Ebdi = Ebai + 30
    Ecai = Eaai + 180
    Ecbi = Ecai + 10
    Ecci = Ecai + 20
    Ecdi = Ecai + 30
    Edai = Eaai + 270
    Edbi = Edai + 10
    Edci = Edai + 20
    Eddi = Edai + 30

    EaaChoix = [Ebai, Ebbi, Ebci, Ecai, Ecbi, Ecci, Edai, Edbi, Edci]
    Eaa = random.choice(EaaChoix)
    Eab = Eaa + 10
    Eac = Eab - 90
    if Eaa in [Ebai, Ebbi, Ebci]:
        Ead = Ebdi
    elif Eaa in [Ecai, Ecbi, Ecci]:
        Ead = Ecdi
    else:
        Ead = Eddi
    Eae = Ead - 90

    if Eaa in [Ebai, Ecai, Edai]:
        Eaf = Eaai
    elif Eaa in [Ebbi, Ecbi, Edbi]:
        Eaf = Eabi
    else:
        Eaf = Eaci
    Eag = Eaf + 10

    Ehachoix = [i, j, k]
    Eha = random.choice(Ehachoix)
    Ehbchoix = [i, j]
    Ehcchoix = [j, k]
    Ehdchoix = [i, k]

    if Eha == k:
        Ehb = random.choice(Ehbchoix)
    elif Eha == i:
        Ehb = random.choice(Ehcchoix)
    else:
        Ehb = random.choice(Ehdchoix)

    if Eha == j and Ehb == i:
        Ehc = k
    elif Eha == i and Ehb == i:
        Ehc = k
    elif Eha == k and Ehb == i:
        Ehc = j
    elif Eha == i and Ehb == k:
        Ehc = j
    else:
        Ehc = i

    if Ead == Eddi:
        Eah = Edai
        Ean = Eaa
        Eaj = Eaa
        print("prout1")
    else:
        Eah = Ead - 30
        Eaj = Eah + 10
        Eak = Edbi
        Ean = Ead - 10
        Eao = Edci

        Eryh1a = Eah + Eha
        Eryh1b = Eaj + Eha
        Eryh1c = Eak + Eha
        Eryh1d = Edai + Eha

        mesFaces_Eryh1 = [(Eryh1a, Eryh1b, Eryh1c, Eryh1d)]
        print("face Eryh1=", mesFaces_Eryh1)
        if Eha == 0:
            print("proutEryh1")
        else:
            monMesh_Eryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Eryh1 = bpy.data.objects.new("Eryh1", monMesh_Eryh1)
            maScene_Eryh1 = bpy.context.scene
            maScene_Eryh1.objects.link(monObjet_Eryh1)
            monMesh_Eryh1.from_pydata(coord, [], mesFaces_Eryh1)

        Eryh2a = Ean + Ehb
        Eryh2b = Ead + Ehb
        Eryh2c = Eddi + Ehb
        Eryh2d = Eao + Ehb

        mesFaces_Eryh2 = [(Eryh2a, Eryh2b, Eryh2c, Eryh2d)]
        print("face Eryh2=", mesFaces_Eryh2)
        if Ehb == 0:
            print("proutEryh2")
        else:
            monMesh_Eryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Eryh2 = bpy.data.objects.new("Eryh2", monMesh_Eryh2)
            maScene_Eryh2 = bpy.context.scene
            maScene_Eryh2.objects.link(monObjet_Eryh2)
            monMesh_Eryh2.from_pydata(coord, [], mesFaces_Eryh2)

        Eryh3a = Ean + Ehc
        Eryh3b = Eaj + Ehc
        Eryh3c = Eak + Ehc
        Eryh3d = Eao + Ehc

        mesFaces_Eryh3 = [(Eryh3a, Eryh3b, Eryh3c, Eryh3d)]
        print("face Eryh3=", mesFaces_Eryh3)
        if Ehc == 0:
            print("proutEryh3")
        else:
            monMesh_Eryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Eryh3 = bpy.data.objects.new("Eryh3", monMesh_Eryh3)
            maScene_Eryh3 = bpy.context.scene
            maScene_Eryh3.objects.link(monObjet_Eryh3)
            monMesh_Eryh3.from_pydata(coord, [], mesFaces_Eryh3)

        mesFaces_Eryhv1 = [(Eryh3d, Eryh2d, Eryh2a, Eryh3a)]
        print("face Eryhv1=", mesFaces_Eryhv1)
        monMesh_Eryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Eryhv1 = bpy.data.objects.new("Eryhv1", monMesh_Eryhv1)
        maScene_Eryhv1 = bpy.context.scene
        maScene_Eryhv1.objects.link(monObjet_Eryhv1)
        monMesh_Eryhv1.from_pydata(coord, [], mesFaces_Eryhv1)

        mesFaces_Eryhv2 = [(Eryh1b, Eryh1c, Eryh3c, Eryh3b)]
        print("face Eryhv2=", mesFaces_Eryhv2)
        monMesh_Eryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Eryhv2 = bpy.data.objects.new("Eryhv2", monMesh_Eryhv2)
        maScene_Eryhv2 = bpy.context.scene
        maScene_Eryhv2.objects.link(monObjet_Eryhv2)
        monMesh_Eryhv2.from_pydata(coord, [], mesFaces_Eryhv2)

        mesFaces_Eryhv3 = [(Eryh2b, Eryh2c, Eddi, Ead)]
        print("face Eryhv3=", mesFaces_Eryhv3)
        monMesh_Eryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Eryhv3 = bpy.data.objects.new("Eryhv3", monMesh_Eryhv3)
        maScene_Eryhv3 = bpy.context.scene
        maScene_Eryhv3.objects.link(monObjet_Eryhv3)
        monMesh_Eryhv3.from_pydata(coord, [], mesFaces_Eryhv3)

        mesFaces_Eryhv4 = [(Eryh1a, Eah, Edai, Eryh1d)]
        print("face Eryhv4=", mesFaces_Eryhv4)
        monMesh_Eryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Eryhv4 = bpy.data.objects.new("Eryhv4", monMesh_Eryhv4)
        maScene_Eryhv4 = bpy.context.scene
        maScene_Eryhv4.objects.link(monObjet_Eryhv4)
        monMesh_Eryhv4.from_pydata(coord, [], mesFaces_Eryhv4)

    if Eaf == Eaai:
        print("prout5")
    else:
        Eai = Eac - 10
        Eap = Eae - 30
        Eah = Ead - 30
        Eaj = Eaa
        Eak = Eaf + 270

        Erxh1a = Eah + Ehb
        Erxh1b = Eaj + Ehb
        Erxh1c = Eai + Ehb
        Erxh1d = Eap + Ehb

        mesFaces_Erxh1 = [(Erxh1a, Erxh1b, Erxh1c, Erxh1d)]
        print("face Erxh1=", mesFaces_Erxh1)
        if Ehb == 0:
            print("proutErxh1")
        else:
            monMesh_Erxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Erxh1 = bpy.data.objects.new("Erxh1", monMesh_Erxh1)
            maScene_Erxh1 = bpy.context.scene
            maScene_Erxh1.objects.link(monObjet_Erxh1)
            monMesh_Erxh1.from_pydata(coord, [], mesFaces_Erxh1)

        Erxh2a = Eaai + Ehc
        Erxh2b = Eaf + Ehc
        Erxh2c = Eai + Ehc
        Erxh2d = Eap + Ehc

        mesFaces_Erxh2 = [(Erxh2a, Erxh2b, Erxh2c, Erxh2d)]
        print("face Erxh2=", mesFaces_Erxh2)
        if Ehc == 0:
            print("proutErxh2")
        else:
            monMesh_Erxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Erxh2 = bpy.data.objects.new("Erxh2", monMesh_Erxh2)
            maScene_Erxh2 = bpy.context.scene
            maScene_Erxh2.objects.link(monObjet_Erxh2)
            monMesh_Erxh2.from_pydata(coord, [], mesFaces_Erxh2)

        mesFaces_Erxhv1 = [(Erxh1d, Erxh1c, Erxh2c, Erxh2d)]
        print("face Erxhv1=", mesFaces_Erxhv1)
        monMesh_Erxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv1 = bpy.data.objects.new("Erxhv1", monMesh_Erxhv1)
        maScene_Erxhv1 = bpy.context.scene
        maScene_Erxhv1.objects.link(monObjet_Erxhv1)
        monMesh_Erxhv1.from_pydata(coord, [], mesFaces_Erxhv1)

        mesFaces_Erxhv2 = [(Erxh1a, Erxh1b, Eaa, Eah)]
        print("face Erxhv2=", mesFaces_Erxhv2)
        monMesh_Erxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv2 = bpy.data.objects.new("Erxhv2", monMesh_Erxhv2)
        maScene_Erxhv2 = bpy.context.scene
        maScene_Erxhv2.objects.link(monObjet_Erxhv2)
        monMesh_Erxhv2.from_pydata(coord, [], mesFaces_Erxhv2)

        mesFaces_Erxhv3 = [(Erxh1c, Erxh1b, Eaa, Eai)]
        print("face Erxhv3=", mesFaces_Erxhv3)
        monMesh_Erxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv3 = bpy.data.objects.new("Erxhv3", monMesh_Erxhv3)
        maScene_Erxhv3 = bpy.context.scene
        maScene_Erxhv3.objects.link(monObjet_Erxhv3)
        monMesh_Erxhv3.from_pydata(coord, [], mesFaces_Erxhv3)

        mesFaces_Erxhv44 = [(Eah, Erxh1a, Erxh1d, Eap)]
        print("face Erxhv44=", mesFaces_Erxhv44)
        monMesh_Erxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv44 = bpy.data.objects.new("Erxhv44", monMesh_Erxhv44)
        maScene_Erxhv44 = bpy.context.scene
        maScene_Erxhv44.objects.link(monObjet_Erxhv44)
        monMesh_Erxhv44.from_pydata(coord, [], mesFaces_Erxhv44)

        mesFaces_Erxhv5 = [(Erxh2c, Erxh2b, Eaf, Eai)]
        print("face Erxhv5=", mesFaces_Erxhv5)
        monMesh_Erxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv5 = bpy.data.objects.new("Erxhv5", monMesh_Erxhv5)
        maScene_Erxhv5 = bpy.context.scene
        maScene_Erxhv5.objects.link(monObjet_Erxhv5)
        monMesh_Erxhv5.from_pydata(coord, [], mesFaces_Erxhv5)

        mesFaces_Erxhv6 = [(Eaai, Erxh2a, Erxh2d, Eap)]
        print("face Drxhv6=", mesFaces_Erxhv6)
        monMesh_Erxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Erxhv6 = bpy.data.objects.new("Erxhv6", monMesh_Erxhv6)
        maScene_Erxhv6 = bpy.context.scene
        maScene_Erxhv6.objects.link(monObjet_Erxhv6)
        monMesh_Erxhv6.from_pydata(coord, [], mesFaces_Erxhv6)

    Errhc = Eae + Hl
    Errhd = Eac + Hl
    Errha = Eag + Hl
    Errhb = Eadi + Hl
    if Eag == Eadi:
        print("prout6")
    else:
        mesFaces_Errh = [(Errha, Errhb, Errhc, Errhd)]
        print("face Errh=", mesFaces_Errh)
        monMesh_Errh = bpy.data.meshes.new("triangleMesh")
        monObjet_Errh = bpy.data.objects.new("Errh", monMesh_Errh)
        maScene_Errh = bpy.context.scene
        maScene_Errh.objects.link(monObjet_Errh)
        monMesh_Errh.from_pydata(coord, [], mesFaces_Errh)

    mesFaces_Erdh = [(Eaa, Errhd, Errhc, Ead)]
    print("face Erdh=", mesFaces_Erdh)
    monMesh_Erdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Erdh = bpy.data.objects.new("Erdh", monMesh_Erdh)
    maScene_Erdh = bpy.context.scene
    maScene_Erdh.objects.link(monObjet_Erdh)
    monMesh_Erdh.from_pydata(coord, [], mesFaces_Erdh)

    mesFaces_Erdd = [(Eaa, Errhd, Eac)]
    print("face Erdd=", mesFaces_Erdd)
    monMesh_Erdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Erdd = bpy.data.objects.new("Erdd", monMesh_Erdd)
    maScene_Erdd = bpy.context.scene
    maScene_Erdd.objects.link(monObjet_Erdd)
    monMesh_Erdd.from_pydata(coord, [], mesFaces_Erdd)

    mesFaces_Erhh1 = [(Eac, Eag, Errha, Errhd)]
    print("face Erhh1=", mesFaces_Erhh1)
    monMesh_Erhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Erhh1 = bpy.data.objects.new("Erhh1", monMesh_Erhh1)
    maScene_Erhh1 = bpy.context.scene
    maScene_Erhh1.objects.link(monObjet_Erhh1)
    monMesh_Erhh1.from_pydata(coord, [], mesFaces_Erhh1)

    mesFaces_Erhh2 = [(Ead, Eae, Eadi, Errhb, Errhc)]
    print("face Erhh2=", mesFaces_Erhh2)
    monMesh_Erhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Erhh2 = bpy.data.objects.new("Erhh2", monMesh_Erhh2)
    maScene_Erhh2 = bpy.context.scene
    maScene_Erhh2.objects.link(monObjet_Erhh2)
    monMesh_Erhh2.from_pydata(coord, [], mesFaces_Erhh2)

    # -------------- Fin CODE : Matrice Zajec 3x3 --------------#

    ##############################################################
    ##############################################################
    # -------------- Suite Etape 2 : Option 1 --------------#
    ##############################################################
    ##############################################################

    ### D_5eme matrice 3x3

    Fa = (5 * 90) + (5 * 10) + nh
    Fb = Fa + (3 * 10)
    Fc = Fa + (3 * 90) + (3 * 10)
    Fd = Fa + (3 * 90)

    maMatrice_f = [(Fa, Fb, Fc, Fd)]
    print(maMatrice_f)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_e
        # créer le mesh et l'objet
        monMesh_f = bpy.data.meshes.new("triangleMesh")
        monObjet_f = bpy.data.objects.new("maMatrice_f", monMesh_f)
        # lier l'objet à la scène
        maScene_f = bpy.context.scene
        maScene_f.objects.link(monObjet_f)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_f.from_pydata(coord, [], maMatrice_f)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Faai = Fa
    Fabi = Faai + 10
    Faci = Faai + 20
    Fadi = Faai + 30
    Fbai = Faai + 90
    Fbbi = Fbai + 10
    Fbci = Fbai + 20
    Fbdi = Fbai + 30
    Fcai = Faai + 180
    Fcbi = Fcai + 10
    Fcci = Fcai + 20
    Fcdi = Fcai + 30
    Fdai = Faai + 270
    Fdbi = Fdai + 10
    Fdci = Fdai + 20
    Fddi = Fdai + 30

    FaaChoix = [Fbai, Fbbi, Fbci, Fcai, Fcbi, Fcci, Fdai, Fdbi, Fdci]
    Faa = random.choice(FaaChoix)
    Fab = Faa + 10
    Fac = Fab - 90
    if Faa in [Fbai, Fbbi, Fbci]:
        Fad = Fbdi
    elif Faa in [Fcai, Fcbi, Fcci]:
        Fad = Fcdi
    else:
        Fad = Fddi
    Fae = Fad - 90

    if Faa in [Fbai, Fcai, Fdai]:
        Faf = Faai
    elif Faa in [Fbbi, Fcbi, Fdbi]:
        Faf = Fabi
    else:
        Faf = Faci
    Fag = Faf + 10

    Fhachoix = [i, j, k]
    Fha = random.choice(Fhachoix)
    Fhbchoix = [i, j]
    Fhcchoix = [j, k]
    Fhdchoix = [i, k]

    if Fha == k:
        Fhb = random.choice(Fhbchoix)
    elif Fha == i:
        Fhb = random.choice(Fhcchoix)
    else:
        Fhb = random.choice(Fhdchoix)

    if Fha == j and Fhb == i:
        Fhc = k
    elif Fha == i and Fhb == i:
        Fhc = k
    elif Fha == k and Fhb == i:
        Fhc = j
    elif Fha == i and Fhb == k:
        Fhc = j
    else:
        Fhc = i

    if Fad == Fddi:
        Fah = Fdai
        Fan = Faa
        Faj = Faa
        print("prout1")
    else:
        Fah = Fad - 30
        Faj = Fah + 10
        Fak = Fdbi
        Fan = Fad - 10
        Fao = Fdci

        Fryh1a = Fah + Fha
        Fryh1b = Faj + Fha
        Fryh1c = Fak + Fha
        Fryh1d = Fdai + Fha

        mesFaces_Fryh1 = [(Fryh1a, Fryh1b, Fryh1c, Fryh1d)]
        print("face Fryh1=", mesFaces_Fryh1)
        if Fha == 0:
            print("proutFryh1")
        else:
            monMesh_Fryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Fryh1 = bpy.data.objects.new("Fryh1", monMesh_Fryh1)
            maScene_Fryh1 = bpy.context.scene
            maScene_Fryh1.objects.link(monObjet_Fryh1)
            monMesh_Fryh1.from_pydata(coord, [], mesFaces_Fryh1)

        Fryh2a = Fan + Fhb
        Fryh2b = Fad + Fhb
        Fryh2c = Fddi + Fhb
        Fryh2d = Fao + Fhb

        mesFaces_Fryh2 = [(Fryh2a, Fryh2b, Fryh2c, Fryh2d)]
        print("face Fryh2=", mesFaces_Fryh2)
        if Fhb == 0:
            print("proutFryh2")
        else:
            monMesh_Fryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Fryh2 = bpy.data.objects.new("Fryh2", monMesh_Fryh2)
            maScene_Fryh2 = bpy.context.scene
            maScene_Fryh2.objects.link(monObjet_Fryh2)
            monMesh_Fryh2.from_pydata(coord, [], mesFaces_Fryh2)

        Fryh3a = Fan + Fhc
        Fryh3b = Faj + Fhc
        Fryh3c = Fak + Fhc
        Fryh3d = Fao + Fhc

        mesFaces_Fryh3 = [(Fryh3a, Fryh3b, Fryh3c, Fryh3d)]
        print("face Fryh3=", mesFaces_Fryh3)
        if Fhc == 0:
            print("proutFryh3")
        else:
            monMesh_Fryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Fryh3 = bpy.data.objects.new("Fryh3", monMesh_Fryh3)
            maScene_Fryh3 = bpy.context.scene
            maScene_Fryh3.objects.link(monObjet_Fryh3)
            monMesh_Fryh3.from_pydata(coord, [], mesFaces_Fryh3)

        mesFaces_Fryhv1 = [(Fryh3d, Fryh2d, Fryh2a, Fryh3a)]
        print("face Fryhv1=", mesFaces_Fryhv1)

        monMesh_Fryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Fryhv1 = bpy.data.objects.new("Fryhv1", monMesh_Fryhv1)
        maScene_Fryhv1 = bpy.context.scene
        maScene_Fryhv1.objects.link(monObjet_Fryhv1)
        monMesh_Fryhv1.from_pydata(coord, [], mesFaces_Fryhv1)

        mesFaces_Fryhv2 = [(Fryh1b, Fryh1c, Fryh3c, Fryh3b)]
        print("face Fryhv2=", mesFaces_Fryhv2)
        monMesh_Fryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Fryhv2 = bpy.data.objects.new("Fryhv2", monMesh_Fryhv2)
        maScene_Fryhv2 = bpy.context.scene
        maScene_Fryhv2.objects.link(monObjet_Fryhv2)
        monMesh_Fryhv2.from_pydata(coord, [], mesFaces_Fryhv2)

        mesFaces_Fryhv3 = [(Fryh2b, Fryh2c, Fddi, Fad)]
        print("face Fryhv3=", mesFaces_Fryhv3)
        monMesh_Fryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Fryhv3 = bpy.data.objects.new("Fryhv3", monMesh_Fryhv3)
        maScene_Fryhv3 = bpy.context.scene
        maScene_Fryhv3.objects.link(monObjet_Fryhv3)
        monMesh_Fryhv3.from_pydata(coord, [], mesFaces_Fryhv3)

        mesFaces_Fryhv4 = [(Fryh1a, Fah, Fdai, Fryh1d)]
        print("face Fryhv4=", mesFaces_Fryhv4)
        monMesh_Fryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Fryhv4 = bpy.data.objects.new("Fryhv4", monMesh_Fryhv4)
        maScene_Fryhv4 = bpy.context.scene
        maScene_Fryhv4.objects.link(monObjet_Fryhv4)
        monMesh_Fryhv4.from_pydata(coord, [], mesFaces_Fryhv4)

    if Faf == Faai:
        print("prout5")
    else:
        Fai = Fac - 10
        Fap = Fae - 30
        Fah = Fad - 30
        Faj = Faa
        Fak = Faf + 270

        Frxh1a = Fah + Fhb
        Frxh1b = Faj + Fhb
        Frxh1c = Fai + Fhb
        Frxh1d = Fap + Fhb

        mesFaces_Frxh1 = [(Frxh1a, Frxh1b, Frxh1c, Frxh1d)]
        print("face Frxh1=", mesFaces_Frxh1)
        if Fhb == 0:
            print("proutFrxh1")
        else:
            monMesh_Frxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Frxh1 = bpy.data.objects.new("Frxh1", monMesh_Frxh1)
            maScene_Frxh1 = bpy.context.scene
            maScene_Frxh1.objects.link(monObjet_Frxh1)
            monMesh_Frxh1.from_pydata(coord, [], mesFaces_Frxh1)

        Frxh2a = Faai + Fhc
        Frxh2b = Faf + Fhc
        Frxh2c = Fai + Fhc
        Frxh2d = Fap + Fhc

        mesFaces_Frxh2 = [(Frxh2a, Frxh2b, Frxh2c, Frxh2d)]
        print("face Frxh2=", mesFaces_Frxh2)
        if Fhc == 0:
            print("proutFrxh2")
        else:
            monMesh_Frxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Frxh2 = bpy.data.objects.new("Frxh2", monMesh_Frxh2)
            maScene_Frxh2 = bpy.context.scene
            maScene_Frxh2.objects.link(monObjet_Frxh2)
            monMesh_Frxh2.from_pydata(coord, [], mesFaces_Frxh2)

        mesFaces_Frxhv1 = [(Frxh1d, Frxh1c, Frxh2c, Frxh2d)]
        print("face Frxhv1=", mesFaces_Frxhv1)
        monMesh_Frxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv1 = bpy.data.objects.new("Frxhv1", monMesh_Frxhv1)
        maScene_Frxhv1 = bpy.context.scene
        maScene_Frxhv1.objects.link(monObjet_Frxhv1)
        monMesh_Frxhv1.from_pydata(coord, [], mesFaces_Frxhv1)

        mesFaces_Frxhv2 = [(Frxh1a, Frxh1b, Faa, Fah)]
        print("face Frxhv2=", mesFaces_Frxhv2)
        monMesh_Frxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv2 = bpy.data.objects.new("Frxhv2", monMesh_Frxhv2)
        maScene_Frxhv2 = bpy.context.scene
        maScene_Frxhv2.objects.link(monObjet_Frxhv2)
        monMesh_Frxhv2.from_pydata(coord, [], mesFaces_Frxhv2)

        mesFaces_Frxhv3 = [(Frxh1c, Frxh1b, Faa, Fai)]
        print("face Frxhv3=", mesFaces_Frxhv3)
        monMesh_Frxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv3 = bpy.data.objects.new("Frxhv3", monMesh_Frxhv3)
        maScene_Frxhv3 = bpy.context.scene
        maScene_Frxhv3.objects.link(monObjet_Frxhv3)
        monMesh_Frxhv3.from_pydata(coord, [], mesFaces_Frxhv3)

        mesFaces_Frxhv44 = [(Fah, Frxh1a, Frxh1d, Fap)]
        print("face Frxhv44=", mesFaces_Frxhv44)
        monMesh_Frxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv44 = bpy.data.objects.new("Frxhv44", monMesh_Frxhv44)
        maScene_Frxhv44 = bpy.context.scene
        maScene_Frxhv44.objects.link(monObjet_Frxhv44)
        monMesh_Frxhv44.from_pydata(coord, [], mesFaces_Frxhv44)

        mesFaces_Frxhv5 = [(Frxh2c, Frxh2b, Faf, Fai)]
        print("face Frxhv5=", mesFaces_Frxhv5)
        monMesh_Frxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv5 = bpy.data.objects.new("Frxhv5", monMesh_Frxhv5)
        maScene_Frxhv5 = bpy.context.scene
        maScene_Frxhv5.objects.link(monObjet_Frxhv5)
        monMesh_Frxhv5.from_pydata(coord, [], mesFaces_Frxhv5)

        mesFaces_Frxhv6 = [(Faai, Frxh2a, Frxh2d, Fap)]
        print("face Frxhv6=", mesFaces_Frxhv6)
        monMesh_Frxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Frxhv6 = bpy.data.objects.new("Frxhv6", monMesh_Frxhv6)
        maScene_Frxhv6 = bpy.context.scene
        maScene_Frxhv6.objects.link(monObjet_Frxhv6)
        monMesh_Frxhv6.from_pydata(coord, [], mesFaces_Frxhv6)

    Frrhc = Fae + Hm
    Frrhd = Fac + Hm
    Frrha = Fag + Hm
    Frrhb = Fadi + Hm
    if Fag == Fadi:
        print("prout6")
    else:
        mesFaces_Frrh = [(Frrha, Frrhb, Frrhc, Frrhd)]
        print("face Frrh=", mesFaces_Frrh)
        monMesh_Frrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Frrh = bpy.data.objects.new("Frrh", monMesh_Frrh)
        maScene_Frrh = bpy.context.scene
        maScene_Frrh.objects.link(monObjet_Frrh)
        monMesh_Frrh.from_pydata(coord, [], mesFaces_Frrh)

    mesFaces_Frdh = [(Faa, Frrhd, Frrhc, Fad)]
    print("face Frdh=", mesFaces_Frdh)
    monMesh_Frdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Frdh = bpy.data.objects.new("Frdh", monMesh_Frdh)
    maScene_Frdh = bpy.context.scene
    maScene_Frdh.objects.link(monObjet_Frdh)
    monMesh_Frdh.from_pydata(coord, [], mesFaces_Frdh)

    mesFaces_Frdd = [(Faa, Frrhd, Fac)]
    print("face Frdd=", mesFaces_Frdd)
    monMesh_Frdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Frdd = bpy.data.objects.new("Frdd", monMesh_Frdd)
    maScene_Frdd = bpy.context.scene
    maScene_Frdd.objects.link(monObjet_Frdd)
    monMesh_Frdd.from_pydata(coord, [], mesFaces_Frdd)

    mesFaces_Frhh1 = [(Fac, Fag, Frrha, Frrhd)]
    print("face Frhh1=", mesFaces_Frhh1)
    monMesh_Frhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Frhh1 = bpy.data.objects.new("Frhh1", monMesh_Frhh1)
    maScene_Frhh1 = bpy.context.scene
    maScene_Frhh1.objects.link(monObjet_Frhh1)
    monMesh_Frhh1.from_pydata(coord, [], mesFaces_Frhh1)

    mesFaces_Frhh2 = [(Fad, Fae, Fadi, Frrhb, Frrhc)]
    print("face Frhh2=", mesFaces_Frhh2)
    monMesh_Frhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Frhh2 = bpy.data.objects.new("Frhh2", monMesh_Frhh2)
    maScene_Frhh2 = bpy.context.scene
    maScene_Frhh2.objects.link(monObjet_Frhh2)
    monMesh_Frhh2.from_pydata(coord, [], mesFaces_Frhh2)

# -------------- Fin CODE : Matrice Zajec 3x3 --------------#

##############################################################
##############################################################
""" ------------ETAPE 2 : OPTION  2----------------------"""
##############################################################
##############################################################


if op in [1, 3]:
    print("pasloption2")
else:
    ### A_2eme matrice 3x3

    Da = (5 * 90) + lh
    Db = Da + (3 * 10)
    Dc = Da + (3 * 90) + (3 * 10)
    Dd = Da + (3 * 90)

    maMatrice_d = [(Da, Db, Dc, Dd)]
    print(maMatrice_d)

    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_d
        # créer le mesh et l'objet
        monMesh_d = bpy.data.meshes.new("triangleMesh")
        monObjet_d = bpy.data.objects.new("maMatrice_d", monMesh_d)
        # lier l'objet à la scène
        maScene_d = bpy.context.scene
        maScene_d.objects.link(monObjet_d)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_d.from_pydata(coord, [], maMatrice_d)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Daai = Da
    Dabi = Daai + 10
    Daci = Daai + 20
    Dadi = Daai + 30
    Dbai = Daai + 90
    Dbbi = Dbai + 10
    Dbci = Dbai + 20
    Dbdi = Dbai + 30
    Dcai = Daai + 180
    Dcbi = Dcai + 10
    Dcci = Dcai + 20
    Dcdi = Dcai + 30
    Ddai = Daai + 270
    Ddbi = Ddai + 10
    Ddci = Ddai + 20
    Dddi = Ddai + 30

    DaaChoix = [Dbai, Dbbi, Dbci, Dcai, Dcbi, Dcci, Ddai, Ddbi, Ddci]
    Daa = random.choice(DaaChoix)
    Dab = Daa + 10
    Dac = Dab - 90
    if Daa in [Dbai, Dbbi, Dbci]:
        Dad = Dbdi
    elif Daa in [Dcai, Dcbi, Dcci]:
        Dad = Dcdi
    else:
        Dad = Dddi
    Dae = Dad - 90

    if Daa in [Dbai, Dcai, Ddai]:
        Daf = Daai
    elif Daa in [Dbbi, Dcbi, Ddbi]:
        Daf = Dabi
    else:
        Daf = Daci
    Dag = Daf + 10

    Dhachoix = [i, j, k]
    Dha = random.choice(Dhachoix)
    Dhbchoix = [i, j]
    Dhcchoix = [j, k]
    Dhdchoix = [i, k]

    if Dha == k:
        Dhb = random.choice(Dhbchoix)
    elif Dha == i:
        Dhb = random.choice(Dhcchoix)
    else:
        Dhb = random.choice(Dhdchoix)

    if Dha == j and Dhb == i:
        Dhc = k
    elif Dha == i and Dhb == i:
        Dhc = k
    elif Dha == k and Dhb == i:
        Dhc = j
    elif Dha == i and Dhb == k:
        Dhc = j
    else:
        Dhc = i

    if Dad == Dddi:
        Dah = Ddai
        Dan = Daa
        Daj = Daa
        print("pas1")
    else:
        Dah = Dad - 30
        Daj = Dah + 10
        Dak = Ddbi
        Dan = Dad - 10
        Dao = Ddci

        Dryh1a = Dah + Dha
        Dryh1b = Daj + Dha
        Dryh1c = Dak + Dha
        Dryh1d = Ddai + Dha

        mesFaces_Dryh1 = [(Dryh1a, Dryh1b, Dryh1c, Dryh1d)]
        print("face Dryh1=", mesFaces_Dryh1)
        if Dha == 0:
            print("pasDryh1")
        else:
            monMesh_Dryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh1 = bpy.data.objects.new("Dryh1", monMesh_Dryh1)
            maScene_Dryh1 = bpy.context.scene
            maScene_Dryh1.objects.link(monObjet_Dryh1)
            monMesh_Dryh1.from_pydata(coord, [], mesFaces_Dryh1)

        Dryh2a = Dan + Dhb
        Dryh2b = Dad + Dhb
        Dryh2c = Dddi + Dhb
        Dryh2d = Dao + Dhb

        mesFaces_Dryh2 = [(Dryh2a, Dryh2b, Dryh2c, Dryh2d)]
        print("face Dryh2=", mesFaces_Dryh2)
        if Dhb == 0:
            print("pasDryh2")
        else:
            monMesh_Dryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh2 = bpy.data.objects.new("Dryh2", monMesh_Dryh2)
            maScene_Dryh2 = bpy.context.scene
            maScene_Dryh2.objects.link(monObjet_Dryh2)
            monMesh_Dryh2.from_pydata(coord, [], mesFaces_Dryh2)

        Dryh3a = Dan + Dhc
        Dryh3b = Daj + Dhc
        Dryh3c = Dak + Dhc
        Dryh3d = Dao + Dhc

        mesFaces_Dryh3 = [(Dryh3a, Dryh3b, Dryh3c, Dryh3d)]
        print("face Dryh3=", mesFaces_Dryh3)
        if Dhc == 0:
            print("pasDryh3")
        else:
            monMesh_Dryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Dryh3 = bpy.data.objects.new("Dryh3", monMesh_Dryh3)
            maScene_Dryh3 = bpy.context.scene
            maScene_Dryh3.objects.link(monObjet_Dryh3)
            monMesh_Dryh3.from_pydata(coord, [], mesFaces_Dryh3)

        mesFaces_Dryhv1 = [(Dryh3d, Dryh2d, Dryh2a, Dryh3a)]
        print("face Dryhv1=", mesFaces_Dryhv1)
        monMesh_Dryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv1 = bpy.data.objects.new("Dryhv1", monMesh_Dryhv1)
        maScene_Dryhv1 = bpy.context.scene
        maScene_Dryhv1.objects.link(monObjet_Dryhv1)
        monMesh_Dryhv1.from_pydata(coord, [], mesFaces_Dryhv1)

        mesFaces_Dryhv2 = [(Dryh1b, Dryh1c, Dryh3c, Dryh3b)]
        print("face Dryhv2=", mesFaces_Dryhv2)
        monMesh_Dryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv2 = bpy.data.objects.new("Dryhv2", monMesh_Dryhv2)
        maScene_Dryhv2 = bpy.context.scene
        maScene_Dryhv2.objects.link(monObjet_Dryhv2)
        monMesh_Dryhv2.from_pydata(coord, [], mesFaces_Dryhv2)

        mesFaces_Dryhv3 = [(Dryh2b, Dryh2c, Dddi, Dad)]
        print("face Dryhv3=", mesFaces_Dryhv3)
        monMesh_Dryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv3 = bpy.data.objects.new("Dryhv3", monMesh_Dryhv3)
        maScene_Dryhv3 = bpy.context.scene
        maScene_Dryhv3.objects.link(monObjet_Dryhv3)
        monMesh_Dryhv3.from_pydata(coord, [], mesFaces_Dryhv3)

        mesFaces_Dryhv4 = [(Dryh1a, Dah, Ddai, Dryh1d)]
        print("face Dryhv4=", mesFaces_Dryhv4)
        monMesh_Dryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Dryhv4 = bpy.data.objects.new("Dryhv4", monMesh_Dryhv4)
        maScene_Dryhv4 = bpy.context.scene
        maScene_Dryhv4.objects.link(monObjet_Dryhv4)
        monMesh_Dryhv4.from_pydata(coord, [], mesFaces_Dryhv4)

    if Daf == Daai:
        print("pas5")
    else:
        Dai = Dac - 10
        Dap = Dae - 30
        Dah = Dad - 30
        Daj = Daa
        Dak = Daf + 270

        Drxh1a = Dah + Dhb
        Drxh1b = Daj + Dhb
        Drxh1c = Dai + Dhb
        Drxh1d = Dap + Dhb

        mesFaces_Drxh1 = [(Drxh1a, Drxh1b, Drxh1c, Drxh1d)]
        print("face Drxh1=", mesFaces_Drxh1)
        if Dhb == 0:
            print("pasDrxh1")
        else:
            monMesh_Drxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Drxh1 = bpy.data.objects.new("Drxh1", monMesh_Drxh1)
            maScene_Drxh1 = bpy.context.scene
            maScene_Drxh1.objects.link(monObjet_Drxh1)
            monMesh_Drxh1.from_pydata(coord, [], mesFaces_Drxh1)

        Drxh2a = Daai + Dhc
        Drxh2b = Daf + Dhc
        Drxh2c = Dai + Dhc
        Drxh2d = Dap + Dhc

        mesFaces_Drxh2 = [(Drxh2a, Drxh2b, Drxh2c, Drxh2d)]
        print("face Drxh2=", mesFaces_Drxh2)
        if Dhc == 0:
            print("pasDrxh2")
        else:
            monMesh_Drxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Drxh2 = bpy.data.objects.new("Drxh2", monMesh_Drxh2)
            maScene_Drxh2 = bpy.context.scene
            maScene_Drxh2.objects.link(monObjet_Drxh2)
            monMesh_Drxh2.from_pydata(coord, [], mesFaces_Drxh2)

        mesFaces_Drxhv1 = [(Drxh1d, Drxh1c, Drxh2c, Drxh2d)]
        print("face Drxhv1=", mesFaces_Drxhv1)
        monMesh_Drxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv1 = bpy.data.objects.new("Drxhv1", monMesh_Drxhv1)
        maScene_Drxhv1 = bpy.context.scene
        maScene_Drxhv1.objects.link(monObjet_Drxhv1)
        monMesh_Drxhv1.from_pydata(coord, [], mesFaces_Drxhv1)

        mesFaces_Drxhv2 = [(Drxh1a, Drxh1b, Daa, Dah)]
        print("face Drxhv2=", mesFaces_Drxhv2)
        monMesh_Drxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv2 = bpy.data.objects.new("Drxhv2", monMesh_Drxhv2)
        maScene_Drxhv2 = bpy.context.scene
        maScene_Drxhv2.objects.link(monObjet_Drxhv2)
        monMesh_Drxhv2.from_pydata(coord, [], mesFaces_Drxhv2)

        mesFaces_Drxhv3 = [(Drxh1c, Drxh1b, Daa, Dai)]
        print("face Drxhv3=", mesFaces_Drxhv3)
        monMesh_Drxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv3 = bpy.data.objects.new("Drxhv3", monMesh_Drxhv3)
        maScene_Drxhv3 = bpy.context.scene
        maScene_Drxhv3.objects.link(monObjet_Drxhv3)
        monMesh_Drxhv3.from_pydata(coord, [], mesFaces_Drxhv3)

        mesFaces_Drxhv44 = [(Dah, Drxh1a, Drxh1d, Dap)]
        print("face Drxhv44=", mesFaces_Drxhv44)
        monMesh_Drxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv44 = bpy.data.objects.new("Drxhv44", monMesh_Drxhv44)
        maScene_Drxhv44 = bpy.context.scene
        maScene_Drxhv44.objects.link(monObjet_Drxhv44)
        monMesh_Drxhv44.from_pydata(coord, [], mesFaces_Drxhv44)

        mesFaces_Drxhv5 = [(Drxh2c, Drxh2b, Daf, Dai)]
        print("face Drxhv5=", mesFaces_Drxhv5)
        monMesh_Drxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv5 = bpy.data.objects.new("Drxhv5", monMesh_Drxhv5)
        maScene_Drxhv5 = bpy.context.scene
        maScene_Drxhv5.objects.link(monObjet_Drxhv5)
        monMesh_Drxhv5.from_pydata(coord, [], mesFaces_Drxhv5)

        mesFaces_Drxhv6 = [(Daai, Drxh2a, Drxh2d, Dap)]
        print("face Drxhv6=", mesFaces_Drxhv6)
        monMesh_Drxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Drxhv6 = bpy.data.objects.new("Drxhv6", monMesh_Drxhv6)
        maScene_Drxhv6 = bpy.context.scene
        maScene_Drxhv6.objects.link(monObjet_Drxhv6)
        monMesh_Drxhv6.from_pydata(coord, [], mesFaces_Drxhv6)

    Drrhc = Dae + Hk
    Drrhd = Dac + Hk
    Drrha = Dag + Hk
    Drrhb = Dadi + Hk
    if Dag == Dadi:
        print("pas6")
    else:
        mesFaces_Drrh = [(Drrha, Drrhb, Drrhc, Drrhd)]
        print("face Drrh=", mesFaces_Drrh)
        monMesh_Drrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Drrh = bpy.data.objects.new("Drrh", monMesh_Drrh)
        maScene_Drrh = bpy.context.scene
        maScene_Drrh.objects.link(monObjet_Drrh)
        monMesh_Drrh.from_pydata(coord, [], mesFaces_Drrh)

    mesFaces_Drdh = [(Daa, Drrhd, Drrhc, Dad)]
    print("face Drdh=", mesFaces_Drdh)
    monMesh_Drdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Drdh = bpy.data.objects.new("Drdh", monMesh_Drdh)
    maScene_Drdh = bpy.context.scene
    maScene_Drdh.objects.link(monObjet_Drdh)
    monMesh_Drdh.from_pydata(coord, [], mesFaces_Drdh)

    mesFaces_Drdd = [(Daa, Drrhd, Dac)]
    print("face Drdd=", mesFaces_Drdd)
    monMesh_Drdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Drdd = bpy.data.objects.new("Drdd", monMesh_Drdd)
    maScene_Drdd = bpy.context.scene
    maScene_Drdd.objects.link(monObjet_Drdd)
    monMesh_Drdd.from_pydata(coord, [], mesFaces_Drdd)

    mesFaces_Drhh1 = [(Dac, Dag, Drrha, Drrhd)]
    print("face Drhh1=", mesFaces_Drhh1)
    monMesh_Drhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Drhh1 = bpy.data.objects.new("Drhh1", monMesh_Drhh1)
    maScene_Drhh1 = bpy.context.scene
    maScene_Drhh1.objects.link(monObjet_Drhh1)
    monMesh_Drhh1.from_pydata(coord, [], mesFaces_Drhh1)

    mesFaces_Drhh2 = [(Dad, Dae, Dadi, Drrhb, Drrhc)]
    print("face Drhh2=", mesFaces_Drhh2)
    monMesh_Drhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Drhh2 = bpy.data.objects.new("Drhh2", monMesh_Drhh2)
    maScene_Drhh2 = bpy.context.scene
    maScene_Drhh2.objects.link(monObjet_Drhh2)
    monMesh_Drhh2.from_pydata(coord, [], mesFaces_Drhh2)

    # -------------- Fin CODE : Matrice Zajec 3x3 ----------------#

    ##############################################################
    ##############################################################
    # -------------- Suite Etape 2 : Option 2 --------------------#
    ##############################################################
    ##############################################################

    ### J_3eme matrice 5x5

    Ca = (3 * 90) + (3 * 10) + nh
    Cb = Ca + (5 * 10)
    Cc = Ca + (5 * 90) + (5 * 10)
    Cd = Ca + (5 * 90)

    maMatrice_c = [(Ca, Cb, Cc, Cd)]
    print(maMatrice_c)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_c
        # créer le mesh et l'objet
        monMesh_c = bpy.data.meshes.new("triangleMesh")
        monObjet_c = bpy.data.objects.new("maMatrice_c", monMesh_c)
        # lier l'objet à la scène
        maScene_c = bpy.context.scene
        maScene_c.objects.link(monObjet_c)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_c.from_pydata(coord, [], maMatrice_c)

    ##############################################################
    # --Debut CODE : Matrice Zajec 5x5 Regarder le A pour le détail----#
    ##############################################################
    Caai = Ca
    Cabi = Caai + (10 * 1)
    Caci = Caai + (10 * 2)
    Cadi = Caai + (10 * 3)
    Caei = Caai + (10 * 4)
    Cafi = Caai + (10 * 5)

    Cbai = Ca + (90 * 1)
    Cbbi = Cbai + (10 * 1)
    Cbci = Cbai + (10 * 2)
    Cbdi = Cbai + (10 * 3)
    Cbei = Cbai + (10 * 4)
    Cbfi = Cbai + (10 * 5)

    Ccai = Ca + (90 * 2)
    Ccbi = Ccai + (10 * 1)
    Ccci = Ccai + (10 * 2)
    Ccdi = Ccai + (10 * 3)
    Ccei = Ccai + (10 * 4)
    Ccfi = Ccai + (10 * 5)

    Cdai = Ca + (90 * 3)
    Cdbi = Cdai + (10 * 1)
    Cdci = Cdai + (10 * 2)
    Cddi = Cdai + (10 * 3)
    Cdei = Cdai + (10 * 4)
    Cdfi = Cdai + (10 * 5)

    Ceai = Ca + (90 * 4)
    Cebi = Ceai + (10 * 1)
    Ceci = Ceai + (10 * 2)
    Cedi = Ceai + (10 * 3)
    Ceei = Ceai + (10 * 4)
    Cefi = Ceai + (10 * 5)

    Cfai = Ca + (90 * 5)
    Cfbi = Cfai + (10 * 1)
    Cfci = Cfai + (10 * 2)
    Cfdi = Cfai + (10 * 3)
    Cfei = Cfai + (10 * 4)
    Cffi = Cfai + (10 * 5)

    CaazChoix = [
        Cfai,
        Ceai,
        Cdai,
        Ccai,
        Cbai,
        Cfbi,
        Cebi,
        Cdbi,
        Ccbi,
        Cbbi,
        Cfci,
        Ceci,
        Cdci,
        Ccci,
        Cbci,
        Cfdi,
        Cedi,
        Cddi,
        Ccdi,
        Cbdi,
        Cfei,
        Ceei,
        Cedi,
        Ceci,
        Cebi,
    ]
    Caa = random.choice(CaazChoix)

    CibChoix = [10, 20]
    if Caa in [Cfbi, Cebi, Cdbi, Ccbi, Cbbi, Cfdi, Cedi, Cddi, Ccdi, Cbdi]:
        Cib = 20
    elif Caa in [Cfei, Ceei, Cdei, Ccei, Cbei]:
        Cib = 10
    else:
        Cib = random.choice(CibChoix)

    Cab = Caa + Cib

    CicChoix = [90, 180]
    if Caa in [Ceai, Cebi, Ceci, Cedi, Ceei, Cefi, Ccai, Ccbi, Ccci, Ccdi, Ccei, Ccfi]:
        Cic = 180
    elif Caa in [Cbai, Cbbi, Cbci, Cbdi, Cbei, Cbfi]:
        Cic = 90
    else:
        Cic = random.choice(CicChoix)
    Cac = Cab - Cic

    if Caa in [Cbai, Cbbi, Cbci, Cbdi, Cbei]:
        Cad = Cbfi
    elif Caa in [Ccai, Ccbi, Ccci, Ccdi, Ccei]:
        Cad = Ccfi
    elif Caa in [Cdai, Cdbi, Cdci, Cddi, Cdei]:
        Cad = Cdfi
    elif Caa in [Ceai, Cebi, Ceci, Cedi, Ceei]:
        Cad = Cefi
    else:
        Cad = Cffi

    Cae = Cad - (Cic)

    if Caa in [Cfai, Ceai, Cdai, Ccai, Cbai]:
        Caf = Caai
    elif Caa in [Cfbi, Cebi, Cdbi, Ccbi, Cbbi]:
        Caf = Cabi
    elif Caa in [Cfci, Ceci, Cdci, Ccci, Cbci]:
        Caf = Caci
    elif Caa in [Cfdi, Cedi, Cddi, Ccdi, Cbdi]:
        Caf = Cadi
    else:
        Caf = Caei

    Cag = Caf + Cib

    Chachoix = [i, j, k]
    Cha = random.choice(Chachoix)
    Chbchoix = [i, j]
    Chcchoix = [j, k]
    Chdchoix = [i, k]
    if Cha == k:
        Chb = random.choice(Chbchoix)
    elif Cha == i:
        Chb = random.choice(Chcchoix)
    else:
        Chb = random.choice(Chdchoix)
    if Cha == j and Chb == i:
        Chc = k
    elif Cha == i and Chb == i:
        Chc = k
    elif Cha == k and Chb == i:
        Chc = j
    elif Cha == i and Chb == k:
        Chc = j
    else:
        Chc = i

    if Cad == Cffi:
        Cah = Cfai
        Can = Caa
        print("pas")
    else:
        Cah = Cad - (5 * 10)

        CaxChoix = [10, 20]
        Cax = random.choice(CaxChoix)
        Caj = Cah + Cax
        Cak = Cfai + Cax
        Cal = Cfai

        CayChoix = [10, 20]
        if Cax == 10:
            Cay = 20
        else:
            Cay = random.choice(CayChoix)
        Can = Caj + Cay
        Cap = Cffi
        Cao = Cak + Cay

        Cryh1a = Cah + Cha
        Cryh1b = Caj + Cha
        Cryh1c = Cak + Cha
        Cryh1d = Cal + Cha

        mesFaces_Cryh1 = [(Cryh1a, Cryh1b, Cryh1c, Cryh1d)]
        print("Cryh1=", mesFaces_Cryh1)

        if Cha == 0:
            print("pasCry1")
        else:
            monMesh_Cryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh1 = bpy.data.objects.new("Cryh1", monMesh_Cryh1)
            maScene_Cryh1 = bpy.context.scene
            maScene_Cryh1.objects.link(monObjet_Cryh1)
            monMesh_Cryh1.from_pydata(coord, [], mesFaces_Cryh1)

        Cryh2a = Can + Chb
        Cryh2b = Cad + Chb
        Cryh2c = Cap + Chb
        Cryh2d = Cao + Chb

        mesFaces_Cryh2 = [(Cryh2a, Cryh2b, Cryh2c, Cryh2d)]
        print("Cryh2=", mesFaces_Cryh2)
        if Chb == 0:
            print("pasCry2")
        else:
            monMesh_Cryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh2 = bpy.data.objects.new("Cryh2", monMesh_Cryh2)
            maScene_Cryh2 = bpy.context.scene
            maScene_Cryh2.objects.link(monObjet_Cryh2)
            monMesh_Cryh2.from_pydata(coord, [], mesFaces_Cryh2)

        Cryh3a = Can + Chc
        Cryh3b = Caj + Chc
        Cryh3c = Cak + Chc
        Cryh3d = Cao + Chc

        mesFaces_Cryh3 = [(Cryh3a, Cryh3b, Cryh3c, Cryh3d)]
        print("Cryh3=", mesFaces_Cryh3)
        if Chc == 0:
            print("pasCry3")
        else:
            monMesh_Cryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh3 = bpy.data.objects.new("Cryh3", monMesh_Cryh3)
            maScene_Cryh3 = bpy.context.scene
            maScene_Cryh3.objects.link(monObjet_Cryh3)
            monMesh_Cryh3.from_pydata(coord, [], mesFaces_Cryh3)

        mesFaces_Cryhv1 = [(Cryh3d, Cryh2d, Cryh2a, Cryh3a)]
        print("face Cryhv1=", mesFaces_Cryhv1)
        monMesh_Cryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv1 = bpy.data.objects.new("Cryhv1", monMesh_Cryhv1)
        maScene_Cryhv1 = bpy.context.scene
        maScene_Cryhv1.objects.link(monObjet_Cryhv1)
        monMesh_Cryhv1.from_pydata(coord, [], mesFaces_Cryhv1)

        mesFaces_Cryhv2 = [(Cryh1b, Cryh1c, Cryh3c, Cryh3b)]
        print("face Cryhv2=", mesFaces_Cryhv2)
        monMesh_Cryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv2 = bpy.data.objects.new("Cryhv2", monMesh_Cryhv2)
        maScene_Cryhv2 = bpy.context.scene
        maScene_Cryhv2.objects.link(monObjet_Cryhv2)
        monMesh_Cryhv2.from_pydata(coord, [], mesFaces_Cryhv2)

        mesFaces_Cryhv3 = [(Cryh2b, Cryh2c, Cffi, Cad)]
        print("face Cryhv3=", mesFaces_Cryhv3)
        monMesh_Cryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv3 = bpy.data.objects.new("Cryhv3", monMesh_Cryhv3)
        maScene_Cryhv3 = bpy.context.scene
        maScene_Cryhv3.objects.link(monObjet_Cryhv3)
        monMesh_Cryhv3.from_pydata(coord, [], mesFaces_Cryhv3)

        mesFaces_Cryhv4 = [(Cryh1a, Cryh1d, Cfai, Cah)]
        print("face Cryhv4=", mesFaces_Cryhv4)
        monMesh_Cryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv4 = bpy.data.objects.new("Cryhv4", monMesh_Cryhv4)
        maScene_Cryhv4 = bpy.context.scene
        maScene_Cryhv4.objects.link(monObjet_Cryhv4)
        monMesh_Cryhv4.from_pydata(coord, [], mesFaces_Cryhv4)

    Cal = Cfai
    if Caf == Caai:
        print("pas2")
    else:
        Cav = Cal - Cah
        CawChoix = [90, 180]
        Caw = random.choice(CawChoix)
        if Cav == 90:
            Caw = 180
        else:
            Caw = random.choice(CawChoix)
        Caq = Caf + Caw
        Car = Caai
        Cap = Car + Caw

        Crxh1a = Cah + Chb
        Crxh1b = Cap + Chb
        Crxh1c = Caq + Chb
        Crxh1d = Caa + Chb

        mesFaces_Crxh1 = [(Crxh1a, Crxh1b, Crxh1c, Crxh1d)]
        print("Crxh1=", mesFaces_Crxh1)
        if Chb == 0:
            print("pasCrx1")
        else:
            monMesh_Crxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh1 = bpy.data.objects.new("Crxh1", monMesh_Crxh1)
            maScene_Crxh1 = bpy.context.scene
            maScene_Crxh1.objects.link(monObjet_Crxh1)
            monMesh_Crxh1.from_pydata(coord, [], mesFaces_Crxh1)

        Crxh2a = Car + Cha
        Crxh2b = Cap + Cha
        Crxh2c = Caq + Cha
        Crxh2d = Caf + Cha

        mesFaces_Crxh2 = [(Crxh2a, Crxh2b, Crxh2c, Crxh2d)]
        print("Crxh2=", mesFaces_Crxh2)
        if Cha == 0:
            print("pasCrx2")
        else:
            monMesh_Crxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh2 = bpy.data.objects.new("Crxh2", monMesh_Crxh2)
            maScene_Crxh2 = bpy.context.scene
            maScene_Crxh2.objects.link(monObjet_Crxh2)
            monMesh_Crxh2.from_pydata(coord, [], mesFaces_Crxh2)

        mesFaces_Crxhv1 = [(Crxh1b, Crxh1c, Crxh2c, Crxh2b)]
        print("face Crxhv1=", mesFaces_Crxhv1)
        monMesh_Crxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv1 = bpy.data.objects.new("Crxhv1", monMesh_Crxhv1)
        maScene_Crxhv1 = bpy.context.scene
        maScene_Crxhv1.objects.link(monObjet_Crxhv1)
        monMesh_Crxhv1.from_pydata(coord, [], mesFaces_Crxhv1)

        mesFaces_Crxhv11 = [(Crxh1a, Crxh1b, Cap, Cah)]
        print("Crxhv11=", mesFaces_Crxhv11)
        monMesh_Crxhv11 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv11 = bpy.data.objects.new("Crxhv11", monMesh_Crxhv11)
        maScene_Crxhv11 = bpy.context.scene
        maScene_Crxhv11.objects.link(monObjet_Crxhv11)
        monMesh_Crxhv11.from_pydata(coord, [], mesFaces_Crxhv11)

        mesFaces_Crxhv2 = [(Crxh2a, Crxh2b, Cap, Car)]
        print("Crxhv2=", mesFaces_Crxhv2)
        monMesh_Crxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv2 = bpy.data.objects.new("Crxhv2", monMesh_Crxhv2)
        maScene_Crxhv2 = bpy.context.scene
        maScene_Crxhv2.objects.link(monObjet_Crxhv2)
        monMesh_Crxhv2.from_pydata(coord, [], mesFaces_Crxhv2)

        if Cad == Cffi:
            Cryh1b = Caa
            Cryh1a = Cah
            if Cah == Cfai:
                Cryh3b = Caa
                Cryh3a = Caa
            else:
                Cryh3b = Can
                Cryh3a = Can
        else:
            Cryh1a = Cah + Cha
            Cryh1b = Caj + Cha

        mesFaces_Crxhv3 = [(Crxh1a, Crxh1d, Caa, Can, Cryh3a, Cryh3b, Cryh1b, Cryh1a)]
        print("Crxhv3=", mesFaces_Crxhv3)
        monMesh_Crxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv3 = bpy.data.objects.new("Crxhv3", monMesh_Crxhv3)
        maScene_Crxhv3 = bpy.context.scene
        maScene_Crxhv3.objects.link(monObjet_Crxhv3)
        monMesh_Crxhv3.from_pydata(coord, [], mesFaces_Crxhv3)

        mesFaces_Crxhvh1 = [(Crxh1d, Crxh1c, Caq, Caa)]
        print("Crxhvh1=", mesFaces_Crxhvh1)
        monMesh_Crxhvh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhvh1 = bpy.data.objects.new("Crxhvh1", monMesh_Crxhvh1)
        maScene_Crxhvh1 = bpy.context.scene
        maScene_Crxhvh1.objects.link(monObjet_Crxhvh1)
        monMesh_Crxhvh1.from_pydata(coord, [], mesFaces_Crxhvh1)

        mesFaces_Crxhvh2 = [(Crxh2c, Crxh2d, Caf, Caq)]
        print("Crxhvh2=", mesFaces_Crxhvh2)
        monMesh_Crxhvh2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhvh2 = bpy.data.objects.new("Crxhvh2", monMesh_Crxhvh2)
        maScene_Crxhvh2 = bpy.context.scene
        maScene_Crxhvh2.objects.link(monObjet_Crxhvh2)
        monMesh_Crxhvh2.from_pydata(coord, [], mesFaces_Crxhvh2)

    Crrha = Cae + Hj
    Crrhb = Cac + Hj
    Crrhc = Cag + Hj
    Crrhd = Cafi + Hj

    if Cag == Cafi:
        print("pas3")
    else:
        mesFaces_Crrh = [(Crrha, Crrhb, Crrhc, Crrhd)]
        print("Crrh=", mesFaces_Crrh)
        monMesh_Crrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Crrh = bpy.data.objects.new("Crrh", monMesh_Crrh)
        maScene_Crrh = bpy.context.scene
        maScene_Crrh.objects.link(monObjet_Crrh)
        monMesh_Crrh.from_pydata(coord, [], mesFaces_Crrh)

        mesFaces_Crrh1 = [(Crrhc, Crrhb, Cac, Cag)]
        print("Crrh1=", mesFaces_Crrh1)
        monMesh_Crrh1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crrh1 = bpy.data.objects.new("Crrh1", monMesh_Crrh1)
        maScene_Crrh1 = bpy.context.scene
        maScene_Crrh1.objects.link(monObjet_Crrh1)
        monMesh_Crrh1.from_pydata(coord, [], mesFaces_Crrh1)

    mesFaces_Crdh = [(Caa, Crrhb, Crrha, Cad)]
    print("Crdh=", mesFaces_Crdh)
    monMesh_Crdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdh = bpy.data.objects.new("Crdh", monMesh_Crdh)
    maScene_Crdh = bpy.context.scene
    maScene_Crdh.objects.link(monObjet_Crdh)
    monMesh_Crdh.from_pydata(coord, [], mesFaces_Crdh)

    mesFaces_Crdv = [(Cafi, Cae, Cad, Crrha, Crrhd)]
    print("Crdv=", mesFaces_Crdv)
    monMesh_Crdv = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdv = bpy.data.objects.new("Crdv", monMesh_Crdv)
    maScene_Crdv = bpy.context.scene
    maScene_Crdv.objects.link(monObjet_Crdv)
    monMesh_Crdv.from_pydata(coord, [], mesFaces_Crdv)

    mesFaces_Crdd = [(Caa, Crrhb, Cac)]
    print("Crdd=", mesFaces_Crdv)
    monMesh_Crdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdd = bpy.data.objects.new("Crdd", monMesh_Crdd)
    maScene_Crdd = bpy.context.scene
    maScene_Crdd.objects.link(monObjet_Crdd)
    monMesh_Crdd.from_pydata(coord, [], mesFaces_Crdd)

# -------------- Fin CODE : Matrice Zajec 5x5 --------------#

##############################################################
##############################################################
"""#-------------- ETAPE 2 : OPTION 3--------------#"""
##############################################################
##############################################################


if op in [1, 2]:
    print("pasloption3")
else:
    # A_2eme matrice 3x3
    Cd = (5 * 90) + (5 * 10) + kh
    Ca = Cd - (3 * 90)
    Cb = Ca + (3 * 10)
    Cc = Cd + (3 * 10)
    maMatrice_c = [(Ca, Cb, Cc, Cd)]
    print(maMatrice_c)
    if face == 1:
        print("pasdeface")
    else:
        # Voir la matrice_c
        # créer le mesh et l'objet
        monMesh_c = bpy.data.meshes.new("triangleMesh")
        monObjet_c = bpy.data.objects.new("maMatrice_c", monMesh_c)
        # lier l'objet à la scène
        maScene_c = bpy.context.scene
        maScene_c.objects.link(monObjet_c)
        # "remplir" le mesh avec les informations vertices et faces
        monMesh_c.from_pydata(coord, [], maMatrice_c)

    ##############################################################
    # -------------- Debut CODE : Matrice Zajec 3x3 --------------#
    ##############################################################
    Caai = Ca
    Cabi = Caai + 10
    Caci = Caai + 20
    Cadi = Caai + 30
    Cbai = Caai + 90
    Cbbi = Cbai + 10
    Cbci = Cbai + 20
    Cbdi = Cbai + 30
    Ccai = Caai + 180
    Ccbi = Ccai + 10
    Ccci = Ccai + 20
    Ccdi = Ccai + 30
    Cdai = Caai + 270
    Cdbi = Cdai + 10
    Cdci = Cdai + 20
    Cddi = Cdai + 30

    CaaChoix = [Cbai, Cbbi, Cbci, Ccai, Ccbi, Ccci, Cdai, Cdbi, Cdci]
    Caa = random.choice(CaaChoix)
    Cab = Caa + 10
    Cac = Cab - 90
    if Caa in [Cbai, Cbbi, Cbci]:
        Cad = Cbdi
    elif Caa in [Ccai, Ccbi, Ccci]:
        Cad = Ccdi
    else:
        Cad = Cddi
    Cae = Cad - 90

    if Caa in [Cbai, Ccai, Cdai]:
        Caf = Caai
    elif Caa in [Cbbi, Ccbi, Cdbi]:
        Caf = Cabi
    else:
        Caf = Caci
    Cag = Caf + 10

    Chachoix = [i, j, k]
    Cha = random.choice(Chachoix)
    Chbchoix = [i, j]
    Chcchoix = [j, k]
    Chdchoix = [i, k]

    if Cha == k:
        Chb = random.choice(Chbchoix)
    elif Cha == i:
        Chb = random.choice(Chcchoix)
    else:
        Chb = random.choice(Chdchoix)
    if Cha == j and Chb == i:
        Chc = k
    elif Cha == i and Chb == i:
        Chc = k
    elif Cha == k and Chb == i:
        Chc = j
    elif Cha == i and Chb == k:
        Chc = j
    else:
        Chc = i

    if Cad == Cddi:
        Cah = Cdai
        Can = Caa
        Caj = Caa
        print("pas1")
    else:
        Cah = Cad - 30
        Caj = Cah + 10
        Cak = Cdbi
        Can = Cad - 10
        Cao = Cdci

        Cryh1a = Cah + Cha
        Cryh1b = Caj + Cha
        Cryh1c = Cak + Cha
        Cryh1d = Cdai + Cha

        mesFaces_Cryh1 = [(Cryh1a, Cryh1b, Cryh1c, Cryh1d)]
        print("face Cryh1=", mesFaces_Cryh1)
        if Cha == 0:
            print("pasCryh1")
        else:
            monMesh_Cryh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh1 = bpy.data.objects.new("Cryh1", monMesh_Cryh1)
            maScene_Cryh1 = bpy.context.scene
            maScene_Cryh1.objects.link(monObjet_Cryh1)
            monMesh_Cryh1.from_pydata(coord, [], mesFaces_Cryh1)

        Cryh2a = Can + Chb
        Cryh2b = Cad + Chb
        Cryh2c = Cddi + Chb
        Cryh2d = Cao + Chb

        mesFaces_Cryh2 = [(Cryh2a, Cryh2b, Cryh2c, Cryh2d)]
        print("face Cryh2=", mesFaces_Cryh2)
        if Chb == 0:
            print("pasCryh2")
        else:
            monMesh_Cryh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh2 = bpy.data.objects.new("Cryh2", monMesh_Cryh2)
            maScene_Cryh2 = bpy.context.scene
            maScene_Cryh2.objects.link(monObjet_Cryh2)
            monMesh_Cryh2.from_pydata(coord, [], mesFaces_Cryh2)

        Cryh3a = Can + Chc
        Cryh3b = Caj + Chc
        Cryh3c = Cak + Chc
        Cryh3d = Cao + Chc

        mesFaces_Cryh3 = [(Cryh3a, Cryh3b, Cryh3c, Cryh3d)]
        print("face Cryh3=", mesFaces_Cryh3)
        if Chc == 0:
            print("pasCryh3")
        else:
            monMesh_Cryh3 = bpy.data.meshes.new("triangleMesh")
            monObjet_Cryh3 = bpy.data.objects.new("Cryh3", monMesh_Cryh3)
            maScene_Cryh3 = bpy.context.scene
            maScene_Cryh3.objects.link(monObjet_Cryh3)
            monMesh_Cryh3.from_pydata(coord, [], mesFaces_Cryh3)

        mesFaces_Cryhv1 = [(Cryh3d, Cryh2d, Cryh2a, Cryh3a)]
        print("face Cryhv1=", mesFaces_Cryhv1)
        monMesh_Cryhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv1 = bpy.data.objects.new("Cryhv1", monMesh_Cryhv1)
        maScene_Cryhv1 = bpy.context.scene
        maScene_Cryhv1.objects.link(monObjet_Cryhv1)
        monMesh_Cryhv1.from_pydata(coord, [], mesFaces_Cryhv1)

        mesFaces_Cryhv2 = [(Cryh1b, Cryh1c, Cryh3c, Cryh3b)]
        print("face Cryhv2=", mesFaces_Cryhv2)
        monMesh_Cryhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv2 = bpy.data.objects.new("Cryhv2", monMesh_Cryhv2)
        maScene_Cryhv2 = bpy.context.scene
        maScene_Cryhv2.objects.link(monObjet_Cryhv2)
        monMesh_Cryhv2.from_pydata(coord, [], mesFaces_Cryhv2)

        mesFaces_Cryhv3 = [(Cryh2b, Cryh2c, Cddi, Cad)]
        print("face Cryhv3=", mesFaces_Cryhv3)
        monMesh_Cryhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv3 = bpy.data.objects.new("Cryhv3", monMesh_Cryhv3)
        maScene_Cryhv3 = bpy.context.scene
        maScene_Cryhv3.objects.link(monObjet_Cryhv3)
        monMesh_Cryhv3.from_pydata(coord, [], mesFaces_Cryhv3)

        mesFaces_Cryhv4 = [(Cryh1a, Cah, Cdai, Cryh1d)]
        print("face Cryhv4=", mesFaces_Cryhv4)
        monMesh_Cryhv4 = bpy.data.meshes.new("triangleMesh")
        monObjet_Cryhv4 = bpy.data.objects.new("Cryhv4", monMesh_Cryhv4)
        maScene_Cryhv4 = bpy.context.scene
        maScene_Cryhv4.objects.link(monObjet_Cryhv4)
        monMesh_Cryhv4.from_pydata(coord, [], mesFaces_Cryhv4)

    if Caf == Caai:
        print("pas5")
    else:
        Cai = Cac - 10
        Cap = Cae - 30
        Cah = Cad - 30
        Caj = Caa
        Cak = Caf + 270

        Crxh1a = Cah + Chb
        Crxh1b = Caj + Chb
        Crxh1c = Cai + Chb
        Crxh1d = Cap + Chb

        mesFaces_Crxh1 = [(Crxh1a, Crxh1b, Crxh1c, Crxh1d)]
        print("face Crxh1=", mesFaces_Crxh1)
        if Chb == 0:
            print("pasCrxh1")
        else:
            monMesh_Crxh1 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh1 = bpy.data.objects.new("Crxh1", monMesh_Crxh1)
            maScene_Crxh1 = bpy.context.scene
            maScene_Crxh1.objects.link(monObjet_Crxh1)
            monMesh_Crxh1.from_pydata(coord, [], mesFaces_Crxh1)

        Crxh2a = Caai + Chc
        Crxh2b = Caf + Chc
        Crxh2c = Cai + Chc
        Crxh2d = Cap + Chc

        mesFaces_Crxh2 = [(Crxh2a, Crxh2b, Crxh2c, Crxh2d)]
        print("face Crxh2=", mesFaces_Crxh2)
        if Chc == 0:
            print("pasCrxh2")
        else:
            monMesh_Crxh2 = bpy.data.meshes.new("triangleMesh")
            monObjet_Crxh2 = bpy.data.objects.new("Crxh2", monMesh_Crxh2)
            maScene_Crxh2 = bpy.context.scene
            maScene_Crxh2.objects.link(monObjet_Crxh2)
            monMesh_Crxh2.from_pydata(coord, [], mesFaces_Crxh2)

        mesFaces_Crxhv1 = [(Crxh1d, Crxh1c, Crxh2c, Crxh2d)]
        print("face Crxhv1=", mesFaces_Crxhv1)
        monMesh_Crxhv1 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv1 = bpy.data.objects.new("Crxhv1", monMesh_Crxhv1)
        maScene_Crxhv1 = bpy.context.scene
        maScene_Crxhv1.objects.link(monObjet_Crxhv1)
        monMesh_Crxhv1.from_pydata(coord, [], mesFaces_Crxhv1)

        mesFaces_Crxhv2 = [(Crxh1a, Crxh1b, Caa, Cah)]
        print("face Crxhv2=", mesFaces_Crxhv2)
        monMesh_Crxhv2 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv2 = bpy.data.objects.new("Crxhv2", monMesh_Crxhv2)
        maScene_Crxhv2 = bpy.context.scene
        maScene_Crxhv2.objects.link(monObjet_Crxhv2)
        monMesh_Crxhv2.from_pydata(coord, [], mesFaces_Crxhv2)

        mesFaces_Crxhv3 = [(Crxh1c, Crxh1b, Caa, Cai)]
        print("face Crxhv3=", mesFaces_Crxhv3)
        monMesh_Crxhv3 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv3 = bpy.data.objects.new("Crxhv3", monMesh_Crxhv3)
        maScene_Crxhv3 = bpy.context.scene
        maScene_Crxhv3.objects.link(monObjet_Crxhv3)
        monMesh_Crxhv3.from_pydata(coord, [], mesFaces_Crxhv3)

        mesFaces_Crxhv44 = [(Cah, Crxh1a, Crxh1d, Cap)]
        print("face Crxhv44=", mesFaces_Crxhv44)
        monMesh_Crxhv44 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv44 = bpy.data.objects.new("Crxhv44", monMesh_Crxhv44)
        maScene_Crxhv44 = bpy.context.scene
        maScene_Crxhv44.objects.link(monObjet_Crxhv44)

        monMesh_Crxhv44.from_pydata(coord, [], mesFaces_Crxhv44)

        mesFaces_Crxhv5 = [(Crxh2c, Crxh2b, Caf, Cai)]
        print("face Crxhv5=", mesFaces_Crxhv5)
        monMesh_Crxhv5 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv5 = bpy.data.objects.new("Crxhv5", monMesh_Crxhv5)
        maScene_Crxhv5 = bpy.context.scene
        maScene_Crxhv5.objects.link(monObjet_Crxhv5)
        monMesh_Crxhv5.from_pydata(coord, [], mesFaces_Crxhv5)

        mesFaces_Crxhv6 = [(Caai, Crxh2a, Crxh2d, Cap)]
        print("face Crxhv6=", mesFaces_Crxhv6)
        monMesh_Crxhv6 = bpy.data.meshes.new("triangleMesh")
        monObjet_Crxhv6 = bpy.data.objects.new("Crxhv6", monMesh_Crxhv6)
        maScene_Crxhv6 = bpy.context.scene
        maScene_Crxhv6.objects.link(monObjet_Crxhv6)
        monMesh_Crxhv6.from_pydata(coord, [], mesFaces_Crxhv6)

    Crrhc = Cae + Hj
    Crrhd = Cac + Hj
    Crrha = Cag + Hj
    Crrhb = Cadi + Hj
    if Cag == Cadi:
        print("pas6")
    else:
        mesFaces_Crrh = [(Crrha, Crrhb, Crrhc, Crrhd)]
        print("face Crrh=", mesFaces_Crrh)
        monMesh_Crrh = bpy.data.meshes.new("triangleMesh")
        monObjet_Crrh = bpy.data.objects.new("Crrh", monMesh_Crrh)
        maScene_Crrh = bpy.context.scene
        maScene_Crrh.objects.link(monObjet_Crrh)
        monMesh_Crrh.from_pydata(coord, [], mesFaces_Crrh)

    mesFaces_Crdh = [(Caa, Crrhd, Crrhc, Cad)]
    print("face Crdh=", mesFaces_Crdh)
    monMesh_Crdh = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdh = bpy.data.objects.new("Crdh", monMesh_Crdh)
    maScene_Crdh = bpy.context.scene
    maScene_Crdh.objects.link(monObjet_Crdh)
    monMesh_Crdh.from_pydata(coord, [], mesFaces_Crdh)

    mesFaces_Crdd = [(Caa, Crrhd, Cac)]
    print("face Crdd=", mesFaces_Crdd)
    monMesh_Crdd = bpy.data.meshes.new("triangleMesh")
    monObjet_Crdd = bpy.data.objects.new("Crdd", monMesh_Crdd)
    maScene_Crdd = bpy.context.scene
    maScene_Crdd.objects.link(monObjet_Crdd)
    monMesh_Crdd.from_pydata(coord, [], mesFaces_Crdd)

    mesFaces_Crhh1 = [(Cac, Cag, Crrha, Crrhd)]
    print("face Crhh1=", mesFaces_Crhh1)
    monMesh_Crhh1 = bpy.data.meshes.new("triangleMesh")
    monObjet_Crhh1 = bpy.data.objects.new("Crhh1", monMesh_Crhh1)
    maScene_Crhh1 = bpy.context.scene
    maScene_Crhh1.objects.link(monObjet_Crhh1)
    monMesh_Crhh1.from_pydata(coord, [], mesFaces_Crhh1)

    mesFaces_Crhh2 = [(Cad, Cae, Cadi, Crrhb, Crrhc)]
    print("face Crhh2=", mesFaces_Crhh2)
    monMesh_Crhh2 = bpy.data.meshes.new("triangleMesh")
    monObjet_Crhh2 = bpy.data.objects.new("Crhh2", monMesh_Crhh2)
    maScene_Crhh2 = bpy.context.scene
    maScene_Crhh2.objects.link(monObjet_Crhh2)
    monMesh_Crhh2.from_pydata(coord, [], mesFaces_Crhh2)
# -------------- Fin CODE : Matrice Zajec 3x3 --------------#

##############################################################
##############################################################
"""#-------------- ETAPE 3 : Fusion et solidify --------------#"""
##############################################################
##############################################################


### A_Fusion de toutes les mesh avec l'origine choisie


def FusionObjets():
    bpy.context.scene.objects.active = bpy.data.objects["Ardh"]
    scene = bpy.context.scene
    obs = []

    for ob in scene.objects:
        if ob.type == "MESH":
            obs.append(ob)
    Fusion = bpy.context.copy()
    Fusion["active_object"] = obs[0]
    Fusion["selected_objects"] = obs
    Fusion["selected_editable_bases"] = [scene.object_bases[ob.name] for ob in obs]
    bpy.ops.object.join(Fusion)  # Fusionner toutes les mesh


FusionObjets()  # Fusion de toutes les Mesh de la matrice en une seule


### B_Donner de l'épaisseur à la mesh unique
def Epaisseur(Epaisseur_T):
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.modifier_add(type="SOLIDIFY")
    bpy.context.object.modifiers["Solidify"].thickness = Epaisseur_T
    bpy.context.object.modifiers["Solidify"].offset = 0
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Solidify")


Epaisseur(TY)  # Variable pour l'épaisseur des parrois

##############################################################
##############################################################
"""#-------------- ETAPE 4 : Mirroir et Duplicate --------------#"""
##############################################################
##############################################################


### A_Mirroir des objets en X Y Z
def Mirroir(Mirroir_X, Mirroir_Y, Mirroir_Z):
    bpy.ops.object.select_all(action="TOGGLE")
    bpy.ops.object.modifier_add(type="MIRROR")
    bpy.context.object.modifiers["Mirror"].use_x = Mirroir_X
    bpy.context.object.modifiers["Mirror"].use_y = Mirroir_Y
    bpy.context.object.modifiers["Mirror"].use_z = Mirroir_Z
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Mirror")


Mirroir(MX, MY, MZ)  # Variables pour le choix des mirroirs en X Y ou Z (True or False)

# B_Opération mirroir manuelle pour eviter une symétrie trop parfaite
if Duplicate == False:
    print("noduplicate")
else:
    bpy.context.scene.objects.active = bpy.data.objects["Ardh"]
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.duplicate()
    bpy.context.object.location[0] = 8
    bpy.context.object.rotation_euler[2] = -3.14159

# FusionObjets() #Fusion du mirroir

##############################################################
##############################################################
"""#-------------- ETAPE 5 : Camera --------------#"""
##############################################################
##############################################################


### Fonction Camera Isométrique
def CameraISO(FOV, DOV, COV, NOV):
    bpy.ops.object.camera_add(location=DOV, rotation=FOV)
    bpy.context.object.data.type = "ORTHO"
    bpy.context.object.data.ortho_scale = COV
    bpy.context.object.name = NOV


# Variables pour la caméra 1 et 2
CameraISO((0.785395, 0, -2.35619), (-15, 15, 25), CAM_Focale, "Isométrie SO")
CameraISO((0.785395, 0, -0.785395), (-15, -15, 25), CAM_Focale, "Isométrie SE")

##############################################################
##############################################################
######################FIN DU CODE#############################
##############################################################
