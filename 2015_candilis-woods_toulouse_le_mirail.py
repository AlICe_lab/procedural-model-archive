# SPDX-FileCopyrightText: 2015 Mathieu Chabbert
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Toulouse Le Mirail / Tripodes - Mathieu Chabbert
# Script realisé sous windows 7 avec blender 2015 - 01 - 20 18:16 Hash bbf09d9

import bpy
import random
from random import *
from math import *

# nettoyage de base
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()


# définition de la vue 3D >>> rotation en fonction du curseur 3D
def areas_tuple():
    res = {}
    count = 0
    for area in bpy.context.screen.areas:
        res[area.type] = count
        count += 1
    return res


areas = areas_tuple()
view3d = bpy.context.screen.areas[areas["VIEW_3D"]].spaces[0]


# creer une fonction pdm
def maboite(loc_x, loc_y, loc_z, dim_x, dim_y, dim_z):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=(loc_x, loc_y, loc_z))
    bpy.ops.transform.resize(value=(dim_x, dim_y, dim_z))


pdm = [2, 3, 4, 6]
hauteurs = [21, 30, 45]
pdmchoisi = choice(pdm)
hauteur_aleatoire = choice(hauteurs)

# def barre (pdmchoisi,locx,locy,rot,position):


def barre(pdmchoisi, locx, locy, rot, position, rot2):

    view3d.cursor_location = position

    liste_name = []

    for abcisse in range(0, pdmchoisi):
        if pdmchoisi == 2:
            hauteur_pdm = 21
        elif pdmchoisi == 3:
            hauteur_pdm = 45
        elif pdmchoisi == 4:
            hauteur_pdm = hauteur_aleatoire
        elif pdmchoisi == 6:
            hauteur_pdm = hauteur_aleatoire

        maboite(abcisse * 16, 1, (hauteur_pdm / 2) + 1, 4, 5, hauteur_pdm + 2)
        name = bpy.context.scene.objects.active.name
        liste_name.append(name)
        positionPdmX = abcisse * 16
        for i in range(0, int(hauteur_pdm / 3)):
            if i == 5 or i == 8 or i == 13:
                maboite(-4 + positionPdmX, -1.5, ((i * 3) + 1.5), 8, 11, 3)
                name = bpy.context.scene.objects.active.name
                liste_name.append(name)
                maboite(4 + positionPdmX, -1.5, ((i * 3) + 1.5), 8, 11, 3)
                name = bpy.context.scene.objects.active.name
                liste_name.append(name)
            else:
                maboite(-4 + positionPdmX, 0, ((i * 3) + 1.5), 8, 14, 3)
                name = bpy.context.scene.objects.active.name
                liste_name.append(name)
                maboite(4 + positionPdmX, 0, ((i * 3) + 1.5), 8, 14, 3)
                name = bpy.context.scene.objects.active.name
                liste_name.append(name)

    for obj in liste_name:
        bpy.ops.object.select_pattern(pattern=obj)

    bpy.ops.object.join()
    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
    bpy.context.object.location[0] = locx
    bpy.context.object.location[1] = locy

    bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)
    bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY")
    bpy.ops.transform.rotate(value=radians(rot), axis=(0, 0, 1))

    bpy.ops.object.origin_set(type="ORIGIN_CURSOR")

    bpy.ops.transform.rotate(value=radians(rot2), axis=(0, 0, 1))


# tripodes droite


def tripode66A(locX, locY, pos, rot):

    A = [
        (2, 17 + locX, -10 + locY, -30),
        (3, 24 + locX, -14 + locY, -30),
        (4, 31 + locX, -18 + locY, -30),
        (6, 45 + locX, -26 + locY, -30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(6, -45 + locX, -26 + locY, 30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode64A(locX, locY, pos, rot):

    A = [
        (2, 17 + locX, -10 + locY, -30),
        (3, 24 + locX, -14 + locY, -30),
        (4, 31 + locX, -18 + locY, -30),
        (6, 45 + locX, -26 + locY, -30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(4, -31 + locX, -18 + locY, 30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode33A(locX, locY, pos, rot):

    A = [
        (2, 17 + locX, -10 + locY, -30),
        (3, 24 + locX, -14 + locY, -30),
        (4, 31 + locX, -18 + locY, -30),
        (6, 45 + locX, -26 + locY, -30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(3, -24 + locX, -14 + locY, 30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode34A(locX, locY, pos, rot):

    A = [
        (2, 17 + locX, -10 + locY, -30),
        (3, 24 + locX, -14 + locY, -30),
        (4, 31 + locX, -18 + locY, -30),
        (6, 45 + locX, -26 + locY, -30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(4, -31 + locX, -18 + locY, 30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode43A(locX, locY, pos, rot):

    A = [
        (2, 17 + locX, -10 + locY, -30),
        (3, 24 + locX, -14 + locY, -30),
        (4, 31 + locX, -18 + locY, -30),
        (6, 45 + locX, -26 + locY, -30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(3, -24 + locX, -14 + locY, 30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


# tripodes gauche:


def tripode66B(locX, locY, pos, rot):

    A = [
        (2, -17 + locX, -10 + locY, 30),
        (3, -24 + locX, -14 + locY, 30),
        (4, -31 + locX, -18 + locY, 30),
        (6, -45 + locX, -26 + locY, 30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(6, 45 + locX, -26 + locY, -30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode64B(locX, locY, pos, rot):

    A = [
        (2, -17 + locX, -10 + locY, 30),
        (3, -24 + locX, -14 + locY, 30),
        (4, -31 + locX, -18 + locY, 30),
        (6, -45 + locX, -26 + locY, 30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(4, 31 + locX, -18 + locY, -30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode33B(locX, locY, pos, rot):

    A = [
        (2, -17 + locX, -10 + locY, 30),
        (3, -24 + locX, -14 + locY, 30),
        (4, -31 + locX, -18 + locY, 30),
        (6, -45 + locX, -26 + locY, 30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(3, 24 + locX, -14 + locY, -30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode34B(locX, locY, pos, rot):

    A = [
        (2, -17 + locX, -10 + locY, 30),
        (3, -24 + locX, -14 + locY, 30),
        (4, -31 + locX, -18 + locY, 30),
        (6, -45 + locX, -26 + locY, 30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(4, 31 + locX, -18 + locY, -30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


def tripode43B(locX, locY, pos, rot):

    A = [
        (2, -17 + locX, -10 + locY, 30),
        (3, -24 + locX, -14 + locY, 30),
        (4, -31 + locX, -18 + locY, 30),
        (6, -45 + locX, -26 + locY, 30),
    ]
    B = [
        (2, locX, 20 + locY, 90),
        (3, locX, 28 + locY, 90),
        (4, locX, 36 + locY, 90),
        (6, locX, 52 + locY, 90),
    ]
    C = [(3, 24 + locX, -14 + locY, -30)]

    Achoice = choice(A)
    w, x, y, z = Achoice

    barre(w, x, y, z, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice

    barre(w, x, y, z, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice

    barre(w, x, y, z, pos, rot)


# tripodes


def tripode(locX, locY, pos, rot):

    A = [(2, 17, 10, 30), (3, 24, 14, 30), (4, 31, 18, 30), (6, 45, 26, 30)]
    B = [(2, -17, 10, -30), (3, -24, 14, -30), (4, -31, 18, -30), (6, -45, 26, -30)]
    C = [(2, 0, -20, 90), (3, 0, -28, 90), (4, 0, -36, 90), (6, 0, -52, 90)]

    A6 = [(6, 135, 62, 30), (6, 121, 86, 30), (4, 121, 54, 30), (4, 107, 78, 30)]
    A3 = [(3, 73, 26, 30), (3, 59, 50, 30), (4, 78, 30, 30), (4, 66, 54, 30)]
    A4 = [(3, 87, 34, 30), (3, 73, 58, 30)]

    B6 = [
        (6, -135, 62, -30),
        (6, -121, 86, -30),
        (4, -121, 54, -30),
        (4, -107, 78, -30),
    ]
    B3 = [(3, -73, 26), (3, -59, 50, -30), (4, -80, 30, -30), (4, -66, 54, -30)]
    B4 = [(3, -87, 34, -30), (3, -73, 58, -30)]

    C6 = [(6, 14, -148, 90), (6, -14, -148, 90), (4, 14, -132, 90), (4, -14, -132, 90)]
    C3 = [(3, 14, -76, 90), (3, -14, -76, 90), (4, 14, -84, 90), (4, -14, -84, 90)]
    C4 = [(3, 14, -92, 90), (3, -14, -92, 90)]

    Achoice = choice(A)
    w, x, y, z = Achoice
    x = x + locX
    y = y + locY
    barre(w, x, y, z, pos, rot)

    if Achoice == (6, 45, 26, 30):
        tripode66A(180 + locX, 88 + locY, pos, rot)

    elif Achoice == (3, 24, 14, 30):
        tripode33A(97 + locX, 40 + locY, pos, rot)

    elif Achoice == (4, 31, 18, 30):
        tripode43A(111 + locX, 48 + locY, pos, rot)

    Bchoice = choice(B)
    w, x, y, z = Bchoice
    x = x + locX
    y = y + locY
    barre(w, x, y, z, pos, rot)

    if Bchoice == (6, -45, 26, -30):
        tripode66B(-180 + locX, 88 + locY, pos, rot)

    elif Bchoice == (3, -24, 14, -30):
        tripode33B(-97 + locX, 40 + locY, pos, rot)

    elif Bchoice == (4, -31, 18, -30):
        tripode43B(-111 + locX, 48 + locY, pos, rot)

    Cchoice = choice(C)
    w, x, y, z = Cchoice
    x = x + locX
    y = y + locY
    barre(w, x, y, z, pos, rot)

    if Cchoice == (6, 0, -52, 90):
        C6choice = choice(C6)
        w, x, y, z = C6choice
        x = x + locX
        y = y + locY
        barre(w, x, y, z, pos, rot)
    elif Cchoice == (3, 0, -28, 90):
        C3choice = choice(C3)
        w, x, y, z = C3choice
        x = x + locX
        y = y + locY
        barre(w, x, y, z, pos, rot)

    elif Cchoice == (4, 0, -36, 90):
        C4choice = choice(C4)
        w, x, y, z = C4choice
        x = x + locX
        y = y + locY
        barre(w, x, y, z, pos, rot)
    #######
    print(choice(A))
    print("---")


"""
        if Achoice = A[3]:
            A6=[(6,135,62,30),(6,121,86,30),(4,121,54,30),(4,107,78,30)]
            A6choice = choice (A6)
            w,x,y,z = A6choice
            barre (w,x,y,z,pos,rot)

        elif barre (w,x,y,z,pos,rot) = (3,24,14,30):
            A3=[(3,73,26,30),(3,59,50,30),(4,78,30,30),(4,66,54,30)]
            A3choice = choice (A3)
            w,x,y,z = A3choice
            barre (w,x,y,z,pos,rot)

        elif barre (w,x,y,z,pos,rot) = (4,31,18,30):
            A4=[(3,87,34,30),(3,73,58,30)]
            A4choice = choice (A4)
            w,x,y,z = A4choice
            barre (w,x,y,z,pos,rot)"""


"""
    si A=6
    A6=[(6,135,62,30),(6,121,86,30),(4,121,54,30),(4,107,78,30)]
    si A=3
    A3=[(3,73,26,30),(3,59,50,30),(4,78,30,30),(4,66,54,30)]
    si A=4
    A4=[(3,87,34,30),(3,73,58,30)]

    si B=6
    B6=[(6,-135,62,-30),(6,-121,86,-30),(4,-121,54,-30),(4,-107,78,-30)]
    si B=3
    B3=[(3,-73,26),(3,-59,50,-30),(4,-80,30,-30),(4,-66,54,-30)]
    si B=4
    B4=[(3,-87,34,-30),(3,-73,58,-30)]

    si C=6
    C6=[(6,14,-148,90),(6,-14,-148,90),(4,14,-132,90),(4,-14,-132,90)]
    si C=3
    C3=[(3,14,-76,90),(3,-14,-76,90),(4,14,-84,90),(4,-14,-84,90)]
    si C=4
    C4= [(3,14,-92,90),(3,-14,-92,90)]"""
######
tripode(0, 0, (0, 0, 0), 30)

# creation tripode


def triangle(nom, rayon, position):
    bpy.ops.mesh.primitive_circle_add(vertices=3, location=position)
    bpy.ops.transform.resize(value=(rayon, rayon, rayon))
    bpy.ops.object.transform_apply(location=True, rotation=False, scale=True)
    bpy.context.object.name = nom


triangle("triangle", 8.0829, (0, 0, 0))
