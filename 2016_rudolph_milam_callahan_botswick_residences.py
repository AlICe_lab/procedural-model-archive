# SPDX-FileCopyrightText: 2016 Adam Masset-Clément Panier des Touches
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# 1#  DEFINITON ###############################################################


# PLATEFORME

site_size = (randint(35, 40), randint(25, 30), 0)


def plan(location, scale):

    bpy.ops.mesh.primitive_plane_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=scale)


# VOLUMES PRINCIPAUX


def cube(location, scale):
    x, y, z = location
    sx, sy, sz = scale

    location = (x + sx / 2, y + sy / 2, z + sz / 2)

    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=scale)


# VOLUMES  EXTRUDES


def cube_extrud(location, scale):
    x, y, z = location
    sx, sy, sz = scale
    location = (x + sx / 2, y + sy / 2, z + sz / 2)

    bpy.ops.mesh.primitive_plane_add(radius=0.5, location=location)
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.delete(type="ONLY_FACE")
    bpy.ops.mesh.select_all(action="TOGGLE")
    bpy.ops.transform.rotate(value=1.5708, axis=(1, 0, 0))
    bpy.ops.transform.translate(value=(0, -0.5, 0))
    bpy.ops.mesh.extrude_region_move(
        MESH_OT_extrude_region={"mirror": False},
        TRANSFORM_OT_translate={"value": (0, 1, 0)},
    )
    bpy.ops.mesh.select_all(action="SELECT")
    bpy.ops.object.editmode_toggle()
    bpy.ops.transform.resize(value=scale)
    bpy.ops.object.modifier_add(type="SOLIDIFY")
    monExtrusion = bpy.context.object.modifiers["Solidify"]
    monExtrusion.thickness = 0.05
    monExtrusion.offset = 1
    monExtrusion.use_even_offset = True


# COPIER COLLER = IMPORTATION ELEMENT ANNEXES


def CopyPaste(name, newname):

    bpy.context.scene.objects.active = bpy.data.objects[name]
    obj = bpy.context.active_object

    Copy = bpy.data.objects.new(newname, obj.data)
    bpy.context.scene.objects.link(Copy)

    return Copy


# 2#  GESTION COLLISION VOLUMES PRINCIPAUX ####################################


l_module1 = []


def module(location, scale):

    x, y, z = location
    sx, sy, sz = scale

    if len(l_module1) == 0:

        cube(location, scale)

        l_module1.append((x, y, sx, sy, sz))

    else:

        All = []
        Collision = []

        X, Y, sX, sY, sZ = l_module1[0]

        Corners = [
            (X - sx, Y - sy, 0),
            (X + sX, Y - sy, 0),
            (X + sX, Y + sY, 0),
            (X - sx, Y + sY, 0),
        ]

        for ix in range(X - sx, X + sX + 1):
            for iy in range(Y - sy, Y + sY + 1):

                if (ix, iy, 0) not in Corners:
                    if ix + sx <= 20 and ix > -20:
                        All.append((ix, iy, 0))

        for module in l_module1:

            X, Y, sX, sY, sZ = module

            Corners = [
                (X + sX, Y + sY, 0),
                (X + sX, Y - sy, 0),
                (X - sx, Y - sy, 0),
                (X - sx, Y + sY, 0),
            ]

            for ix in range(X - sx, X + sX):
                for iy in range(Y - sy + 1, Y + sY):

                    Collision.append((ix, iy, 0))

        # Implantation volumes principaux sur plateforme

        X, Y, Z = site_size
        pt1 = [X / 2, Y / 2, 0]

        locations = list(set(All) - set(Collision))
        print(locations)
        location = choice(locations)
        x, y, z = location

        limit = [x + sx, y + sy, z + sz]

        if limit > pt1:
            sx = pt1[0] - x
            sy = pt1[1] - y

        elif limit[0] > pt1[0]:
            sx = pt1[0] - x

        cube(location, (sx, sy, sz))

        l_module1.append((x, y, sx, sy, sz))


# 3#  GESTION COLLISION CUBE EXTRUDE 1er ETAGE #################################


l_module2 = []


def module2(location, scale):

    x, y, z = location
    sx, sy, sz = scale

    All = []
    Collision = []

    for module in l_module1:

        X, Y, sX, sY, sZ = module

        Corners = [
            (X + sX, Y + sY, 0),
            (X + sX, Y - sy, 0),
            (X - sx, Y - sy, 0),
            (X - sx, Y + sY, 0),
        ]

        for ix in range(X - sx, X + sX + 1):
            for iy in range(Y - sy, Y + sY + 1):

                if (ix, iy, 0) not in Corners:
                    All.append((ix, iy, 0))

        for ix in range(X - sx + 1, X + sX):
            for iy in range(Y - sy + 1, Y + sY):

                Collision.append((ix, iy, 0))

    locations = list(set(All) - set(Collision))

    location = choice(locations)

    x, y, z = location

    cube_extrud(location, scale)

    l_module2.append((x, y, sx, sy))


# 4#  GESTION COLLISION CUBE EXTRUDER 2nd ETAGE ###############################


l_module3 = []


def module3(location, scale):

    x, y, z = location
    sx, sy, sz = scale

    coordonnees = []

    for module in l_module1:
        X, Y, sX, sY, sZ = module
        for ix in range(X, X + sX + 1 - sx):
            for iy in range(Y, Y + sY + 1 - sy):
                coordonnees.append((ix, iy, sZ))

    location = choice(coordonnees)

    x, y, z = location

    cube_extrud(location, scale)

    l_module3.append((x, y, sx, sy, sz))


# 5#  DEFINITON DES VOLUMES INDEPENDANTS ######################################


l_module4 = []


def module4(location, scale):
    x, y, z = location
    sx, sy, sz = scale

    Collision = []
    All = []

    for module in l_module1:
        X, Y, sX, sY, sZ = module
        for ix in range(X, sX + 1 - sx):
            for iy in range(Y, sY + 1 - sy):
                Collision.append((ix, iy))

    for module in l_module2:
        X, Y, sX, sY = module
        for ix in range(X, sX + 1 - sx):
            for iy in range(Y, sY + 1 - sy):
                Collision.append((ix, iy))

    sX, sY, sZ = site_size
    for ix in range(-sX // 2, sX // 2 - sx):
        for iy in range(-sY // 2, sY // 2 - sy):
            All.append((ix, iy))

    locations = list(set(All) - set(Collision))

    location = choice(locations)

    x, y = location

    cube_extrud((x, y, 0), scale)
    l_module4.append((x, y, sx, sy))


# 6# FORME ANNEXES = CHATEAU D'EAU ################################


l_module6 = []


def module6(location, scale):

    x, y, z = location
    sx, sy, sz = scale

    All = []
    Collision = []

    for module in l_module1:

        X, Y, sX, sY, sZ = module

        Corners = [
            (X + sX, Y + sY, 0),
            (X + sX, Y - sy, 0),
            (X - sx, Y - sy, 0),
            (X - sx, Y + sY, 0),
        ]

        for ix in range(X - sx, X + sX + 1):
            for iy in range(Y - sy, Y + sY + 1):

                if (ix, iy, 0) not in Corners:
                    All.append((ix, iy, 0))

        for ix in range(X - sx + 1, X + sX):
            for iy in range(Y - sy + 1, Y + sY):

                Collision.append((ix, iy, 0))

    locations = list(set(All) - set(Collision))

    location = choice(locations)

    x, y, z = location

    chateau_eau = CopyPaste("chateau_eau", "chateau_eau1")
    chateau_eau.location = [x, y, z]
    chateau_eau.scale = [1, 1, 1]
    l_module6.append((x, y, sx, sy))


# 7#  GENERATION ###############################################


# plateforme

for i in range(1):
    plan((0, 0, 0), site_size)


# volumes principaux

for i in range(randint(3, 4)):
    taille = (randint(7, 8), randint(5, 6), randint(2, 3))
    x, y, z = taille
    module((floor(-x / 2), floor(-y / 2), 0), taille)


# volumes secondaires niveau_0

for i in range(randint(12, 14)):
    taille = (randint(3, 4), randint(2, 3), randint(2, 3))
    x, y, z = taille
    module2((floor(-x / 2), floor(-y / 2), 0), taille)


# volumes secondaires niveau_1

for i in range(randint(8, 10)):
    taille = (randint(3, 4), randint(2, 3), randint(2, 3))
    x, y, z = taille
    module3((floor(-x / 2), floor(-y / 2), 0), taille)


# volumes independants
for i in range(randint(6, 7)):
    taille = (randint(3, 4), randint(2, 3), 2)
    x, y, z = taille
    module4((floor(-x / 2), floor(-y / 2), 0), taille)

# chateau_eau
for i in range(1):
    taille = (x, y, z)
    x, y, z = taille
    module6((floor(-x / 2), floor(-y / 2), 0), taille)

    # volumes niveau -1
    taille = (5, 4, 3)
    x, y, z = taille
    cube_extrud((-14, -14, -3), taille)
    cube_extrud((12, -14, -3), taille)

    # 8# COLONNES #######################################

    # colonnes

    taille = (0.5, 0.5, -8)
    x, y, z = taille
    cube((0, -12, 0), taille)
    cube((4, -12, 0), taille)
    cube((8, -12, 0), taille)
    cube((-4, -12, 0), taille)
    cube((-8, -12, 0), taille)
    cube((-16, -12, 0), taille)


####################################################

#### Residence Callahan +
#### Paul Rudolph


#### Clément PANIER & Adam MASSET
#### AIM Module 1
#### 2016

####################################################
