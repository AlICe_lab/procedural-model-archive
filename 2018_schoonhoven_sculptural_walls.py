# SPDX-FileCopyrightText: 2018 Daniel O'Driscoll & Michela Cardia
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from math import radians
from bpy import data as D

print("SCULPTURALS WALLS", 70 * "-")

# Script Michaela Cardia and Daniel O'Driscoll.
# Image and Media
# INTERPRETATION: Jan Schoonhoven - SCULPTURAL WALL

# 1)FUNCTION THAT DELETES EVERYTHING


def delete_all():
    for item in D.objects:
        D.objects.remove(item)
    for item in D.meshes:
        D.meshes.remove(item)


delete_all()

# 2)STRUCTURE: This is the element that holds the modules together. It is made of columns and beams.
"""operation on the object"""


def box1(location, dimention):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(0, 0, 0))


"""creating the structural dimentions, locations and repetitions and creating multiples of the same object""" ""


def structure(location, dimention, rotation):
    for j in range(0, 2):
        box1((4, j * 8, -3), (10, 0.08, 0.08))
        for j in range(0, 5):
            for i in range(0, 5):
                box1((i * 2, j * 2, 0), (0.08, 0.08, 10))


structure((0, 0, 0), (0, 0, 0), (0, 0, 0))

# ___list of angles ° That can be chosen for the rotation. The follow the artist's sequence.
RotationList1 = [
    -41,
    -44,
    -56,
    -62,
    -59,
    -71,
    -77,
    -86,
    -92,
    -107,
    -101,
    -131,
    -155,
    -170,
    -197,
]
randomAngles = random.choice(RotationList1)

""" we are randomly selecting an angle for the rows to be rotated by"""

RotationList2 = [41, 44, 56, 62, 59, 71, 77, 86, 92, 107, 101, 131, 155, 170, 197]
randomAngles2 = random.choice(RotationList2)

print("randomAngles -2-: ", randomAngles2)

"""we are printing what angle has been chosen to the console"""

# 3)THE MODULES: The modules have been created by mixing the original forms of the artist. Each module is the union of differents pieces.The purpose of this is to create a new module that shows 2/3 different faces from the point of view of the observer alike the artist's own work.


# module floor 1: Represents the first layer of modules. Is composed by two repeating elements and a horizontal element.The oblique elements are the ones that change according to the sequence.
def box2(location, dimention, rotation):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(7.445, 7.805, -4.1))
    bpy.context.object.rotation_euler[1] = radians(rotation)


""" With this we are insuring that the angle of rotation selected above randomly is the same for each module, in each row"""

rotationOfModules5 = random.choice(RotationList1)
rotationOfModules6 = random.choice(RotationList2)

""" creating the module 5/6 in two parts repeated twice with different locations, we then rotate them"""


def module1(location):
    box2((location[0] + 0.55, location[1] + 0.2, location[2]), (2, 2, 0.08), (0))
    for i in range(0, 2):
        box2(
            (i * 0.95 + location[0] + 0.09, location[1] + 0.2, location[2]),
            (2, 2, 0.08),
            (rotationOfModules5),
        )
        box2(
            (i * 0.95 + location[0] + 0.09, location[1] + 0.2, location[2]),
            (2, 2, 0.08),
            (rotationOfModules6),
        )


"""applying the rotation"""

for j in range(0, 5):
    for i in range(0, 5):
        module1((i * -2, j * -2, 0))


# module floor 2: Represents the second layer of modules.It is composed by two repeating elements and a vertical element.The oblique elements change according to the sequence.
def box3(location, dimention, rotation):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(7, 8, -2))
    bpy.context.object.rotation_euler[1] = radians(rotation)


rotationOfModules7 = random.choice(RotationList1)
rotationOfModules8 = random.choice(RotationList2)

""" creating the module 7/8 in two parts repeated twice with different locations, we then rotate them"""


def module2(location, dimention, rotation):
    box3((location[0] + 1.0, location[1], location[2]), (0.08, 2, 2), (0))
    for k in range(0, 2):
        box3(
            (location[0] + 0.5, location[1], k * -0.6 + location[2] + 0.4),
            (0.08, 2, 1.6),
            (rotationOfModules7),
        )
    for k in range(0, 2):
        box3(
            (location[0] + 1.51, location[1], k * -0.6 + location[2] + 0.4),
            (0.08, 2, 1.6),
            (rotationOfModules8),
        )


"""applying the rotation"""

for j in range(0, 5):
    for i in range(0, 5):
        module2((i * -2, j * -2, 0), (0, 0, 0), (0))


# module floor 3: Is the third layer of modules. Is made by two repeating vertical elements, a third vertical element and two pieces rotated according to the angles of the sequence.


def box4(location, dimention, rotation):
    bpy.ops.mesh.primitive_cube_add(radius=0.99, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(-0.15, 0.1, -0.6))
    bpy.context.object.rotation_euler[0] = radians(rotation[0])
    bpy.context.object.rotation_euler[1] = radians(rotation[1])
    bpy.context.object.rotation_euler[2] = radians(rotation[2])


rotationOfModules9 = random.choice(RotationList1)
rotationOfModules10 = random.choice(RotationList2)


""" This module has less repeated elements meaning that each piece has to be manipulated individualy"""


def module3(location, dimention, rotation):
    box4(
        (location[0] + 0.15, location[1] - 0.1, location[2] + 0.5),
        (1, 0.6, 0.05),
        (rotation[0] + 90, rotation[1], rotation[2] + 90),
    )
    for i in range(0, 2):
        box4(
            (i * 1.23 + location[0] - 0.5, location[1] - 0.1, location[2] + 0.8),
            (1, 1, 0.05),
            (rotation[0] + 90, rotation[1], rotation[2] + 90),
        )  # middle
    box4(
        (location[0] + 0.4, location[1] - 0.1, location[2] + 1.42),
        (1, 0.5, 0.05),
        (rotation[0] + rotationOfModules9, rotation[1], rotation[2] + 90),
    )
    box4(
        (location[0] - 0.2, location[1] - 0.1, location[2] + 1.42),
        (1, 0.5, 0.05),
        (rotation[0] + rotationOfModules10, rotation[1], rotation[2] + 90),
    )


""" outward faces of module 3 are created in a pair"""

for j in range(0, 5):
    for i in range(0, 5):
        module3((i * 2, j * 2, 0), (0, 0, 0), (0, 0, 0))


# module floor 4 : Are the 4th floor. Is composed by 1 repeating vertical element that rotates according to the angles of the sequence.


def box5(location, dimention, rotation):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(7.445, 7.805, 1))
    bpy.context.object.rotation_euler[1] = radians(rotation)


""" module created with many repeated elements, we placed duplicated then, and rotated them by 45 degreess to give the module it's default shape"""
rotationOfModules = random.choice(RotationList1)


def module4(location, dimension):

    for i in range(0, 4):
        box5(
            (location[0] + 0.5, location[1] + 0.18, location[2]),
            (2, 2, 0.08),
            (i * 45 + rotationOfModules),
        )


for j in range(0, 5):
    for i in range(0, 5):
        module4((i * -1.99, j * -1.99, 1.25), (0, 0, 0))


# module floor 5: Represents the last floor. It is expressed by 4 elements that rotates according to the angles of the sequence.
def box6(location, dimention, rotation):
    bpy.ops.mesh.primitive_cube_add(radius=0.9, location=location)
    bpy.ops.transform.resize(value=dimention)
    bpy.ops.transform.translate(value=(-0.25, -1.5, 3.2))
    bpy.context.object.rotation_euler[0] = radians(rotation[0])
    bpy.context.object.rotation_euler[1] = radians(rotation[1])
    bpy.context.object.rotation_euler[2] = radians(rotation[2])


rotationOfModules3 = random.choice(RotationList1)
rotationOfModules4 = random.choice(RotationList2)


def module5(location, dimention):

    box6(
        (location[0] + 0.4, location[1] + 1.2, location[2] + 1),
        (1, 1, 0.05),
        (0 + rotationOfModules3, 0, -504),
    )  # middle
    box6(
        (location[0] + 0.49, location[1] + 1.67, location[2] + 1),
        (1, 1, 0.05),
        (0 + rotationOfModules4, 0, 504),
    )
    box6(
        (location[0] + 0.065, location[1] + 1.69, location[2] + 1),
        (1, 1, 0.05),
        (0 + rotationOfModules3, 0, -143),
    )
    box6(
        (location[0] + 0.15, location[1] + 1.190, location[2] + 1),
        (1, 1, 0.0),
        (0 + rotationOfModules4, 0, 504),
    )


for i in range(0, 5):
    for j in range(0, 5):
        module5((i * 2, j * 2, 0), (0, 0, 0))


print("END", 70 * "-")
print()

print("randomAngles -1-: ", randomAngles, "°")

"""The selected angles are printed in the console"""
