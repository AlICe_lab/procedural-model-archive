# SPDX-FileCopyrightText: 2014 Khalil Bouguerch
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Blender 2.71 - Mac OSX
# Par : Bouguerch Khalil
# Script - FORET DE BETON

import bpy
import random

# -------------------------------------------------------------------------------------------#
#                           F O R Ê T  D E  B E T O N                                       #
#                           Kubuswoningen - Piet Blom                                       #
# -------------------------------------------------------------------------------------------#

"""                 ----------------------------------------------------------
                            D E F A U L T
                    ----------------------------------------------------------"""

import bpy
import random

# --------------------------------------------------
# Nettoyage de la scene:

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

"""                 ----------------------------------------------------------
                            D E F I N I T I O N S   D E S   O B J E T S
                    ----------------------------------------------------------"""

# --------------------------------------------------
# Definitions:


def cube(x):
    bpy.ops.mesh.primitive_cube_add(radius=1, location=x)
    bpy.ops.transform.rotate(
        value=0.785398,
        axis=(-0, -0, -1),
        constraint_orientation="GLOBAL",
        proportional="ENABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.transform.rotate(
        value=0.959934,
        axis=(-0, 1, 1.34359e-07),
        constraint_orientation="GLOBAL",
        proportional="ENABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.context.object.location[2] = 3


def cylindre(x):
    bpy.ops.mesh.primitive_cylinder_add(vertices=6, radius=1, depth=2, location=(x))
    bpy.ops.transform.rotate(
        value=-0.523599,
        axis=(-0, -0, 1),
        constraint_orientation="GLOBAL",
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.context.object.scale[0] = 0.5
    bpy.context.object.scale[1] = 0.5
    bpy.context.object.scale[2] = 1
    bpy.context.object.location[2] = 1


def module(x):
    cube(x)
    cylindre(x)


def module2(x):
    bpy.ops.mesh.primitive_cube_add(radius=1, location=x)
    bpy.ops.transform.rotate(
        value=0.785398,
        axis=(-0, -0, -1),
        constraint_orientation="GLOBAL",
        proportional="ENABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.ops.transform.rotate(
        value=0.959934,
        axis=(-0, 1, 1.34359e-07),
        constraint_orientation="GLOBAL",
        proportional="ENABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )
    bpy.context.object.location[2] = 3
    bpy.ops.transform.resize(
        value=(-2, -2, -2),
        constraint_orientation="GLOBAL",
        mirror=False,
        proportional="DISABLED",
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
    )


def clairiere(position, taille):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=taille)


def batiment(position, taille):
    bpy.ops.mesh.primitive_cube_add(radius=0.5, location=position)
    bpy.ops.transform.resize(value=taille)
    bpy.context.object.location[2] = 3


# --------------------------------------------------
# Clairiere 1:

positionClairiere1x = random.randint(5, 15)
positionClairiere1y = random.randint(5, 10)
tailleClairiere1x = random.randint(5, 20)
tailleClairiere1y = random.randint(5, 20)

tailleClairiere1 = (random.randint(5, 10), random.randint(5, 10), 1)

# clairiere((positionClairiere1x,positionClairiere1y,0),(tailleClairiere1x,tailleClairiere1y,1))

# Clairiere 2:

positionClairiere2x = random.randint(15, 25)
positionClairiere2y = random.randint(20, 30)
tailleClairiere2x = random.randint(8, 15)
tailleClairiere2y = random.randint(8, 15)

tailleClairiere2 = (random.randint(5, 10), random.randint(5, 10), 1)

# clairiere((positionClairiere2x,positionClairiere2y,0),(tailleClairiere2x,tailleClairiere2y,1))

# Clairiere 3:

positionClairiere3x = random.randint(5, 20)
positionClairiere3y = random.randint(5, 20)
tailleClairiere3x = random.randint(0, 10)
tailleClairiere3y = random.randint(0, 10)

tailleClairiere3 = (random.randint(5, 10), random.randint(5, 10), 1)

# clairiere((positionClairiere3x,positionClairiere3y,0),(tailleClairiere3x,tailleClairiere3y,1))

# Batiment 1:

positionBatiment1x = positionClairiere1x
positionBatiment1y = positionClairiere1y
tailleBatiment1x = tailleClairiere1x / 2
tailleBatiment1y = tailleClairiere1y / 2

batiment(
    (positionBatiment1x, positionBatiment1y, 0), (tailleBatiment1x, tailleBatiment1y, 5)
)

# Batiment 2:

positionBatiment2x = positionClairiere2x
positionBatiment2y = positionClairiere2y
tailleBatiment2x = tailleClairiere2x / 2
tailleBatiment2y = tailleClairiere2y / 2

batiment(
    (positionBatiment2x, positionBatiment2y, 0), (tailleBatiment2x, tailleBatiment2y, 5)
)

# Batiment 3:

positionBatiment3x = positionClairiere3x
positionBatiment3y = positionClairiere3y
tailleBatiment3x = tailleClairiere3x / 2
tailleBatiment3y = tailleClairiere3y / 2

batiment(
    (positionBatiment3x, positionBatiment3y, 0), (tailleBatiment3x, tailleBatiment3y, 5)
)

"""                 ----------------------------------------------------------
                            L I S T E S
                    ----------------------------------------------------------"""

# --------------------------------------------------
# Listes:

coordonne = []
for x in range(0, 7):
    for y in range(0, 7):
        coordonne.append((x * 4, y * 4, 1))
        coordonne.append(((x + 0.5) * 2, (y + 0.5) * 4, 1))
        coordonne.append(((x + 6.5) * 2, (y + 0.5) * 4, 1))

coordonne2 = []
for x in range(0, 3):
    for y in range(0, 3):
        coordonne2.append(((x + 0.25) * 8, (y + 1) * 8, 1))
        coordonne2.append(((x + 0.75) * 8, (y + 0.5) * 8, 1))

"""                 ----------------------------------------------------------
                            C O N S O L E   ( TEST )
                    ----------------------------------------------------------"""

# --------------------------------------------------
# Suprimer les modules qui s'interceptent avec les clairieres:
# Clairieres/Module1:

for x, y, z in coordonne:
    if (
        (positionClairiere1x - (tailleClairiere1x / 2))
        < x
        < (positionClairiere1x + (tailleClairiere1x / 2))
    ):
        if (
            (positionClairiere1y - (tailleClairiere1y / 2))
            < y
            < (positionClairiere1y + (tailleClairiere1y / 2))
        ):
            coordonne.remove((x, y, z))

for x, y, z in coordonne:
    if (
        (positionClairiere1x - (tailleClairiere1x / 2))
        < x + 0.5
        < (positionClairiere1x + (tailleClairiere1x / 2))
    ):
        if (
            (positionClairiere1y - (tailleClairiere1y / 2))
            < y + 0.5
            < (positionClairiere1y + (tailleClairiere1y / 2))
        ):
            coordonne.remove((x, y, z))

for x, y, z in coordonne:
    if (
        (positionClairiere2x - (tailleClairiere2x / 2))
        < x
        < (positionClairiere2x + (tailleClairiere2x / 2))
    ):
        if (
            (positionClairiere2y - (tailleClairiere2y / 2))
            < y
            < (positionClairiere2y + (tailleClairiere2y / 2))
        ):
            coordonne.remove((x, y, z))

for x, y, z in coordonne:
    if (
        (positionClairiere2x - (tailleClairiere2x / 2))
        < x + 0.5
        < (positionClairiere2x + (tailleClairiere2x / 2))
    ):
        if (
            (positionClairiere2y - (tailleClairiere2y / 2))
            < y + 0.5
            < (positionClairiere2y + (tailleClairiere2y / 2))
        ):
            coordonne.remove((x, y, z))

for x, y, z in coordonne:
    if (
        (positionClairiere3x - (tailleClairiere3x / 2))
        < x
        < (positionClairiere3x + (tailleClairiere3x / 2))
    ):
        if (
            (positionClairiere3y - (tailleClairiere3y / 2))
            < y
            < (positionClairiere3y + (tailleClairiere3y / 2))
        ):
            coordonne.remove((x, y, z))

for x, y, z in coordonne:
    if (
        (positionClairiere3x - (tailleClairiere3x / 2))
        < x + 0.5
        < (positionClairiere3x + (tailleClairiere3x / 2))
    ):
        if (
            (positionClairiere3y - (tailleClairiere3y / 2))
            < y + 0.5
            < (positionClairiere3y + (tailleClairiere3y / 2))
        ):
            coordonne.remove((x, y, z))

# --------------------------------------------------
# Clairieres/Module2:

for x, y, z in coordonne2:
    if (
        (positionClairiere1x - (tailleClairiere1x / 2))
        < x
        < (positionClairiere1x + (tailleClairiere1x / 2))
    ):
        if (
            (positionClairiere1y - (tailleClairiere1y / 2))
            < y
            < (positionClairiere1y + (tailleClairiere1y / 2))
        ):
            coordonne2.remove((x, y, z))

for x, y, z in coordonne2:
    if (
        (positionClairiere1x - (tailleClairiere1x / 2))
        < x + 0.5
        < (positionClairiere1x + (tailleClairiere1x / 2))
    ):
        if (
            (positionClairiere1y - (tailleClairiere1y / 2))
            < y + 0.5
            < (positionClairiere1y + (tailleClairiere1y / 2))
        ):
            coordonne2.remove((x, y, z))

for x, y, z in coordonne2:
    if (
        (positionClairiere2x - (tailleClairiere2x / 2))
        < x
        < (positionClairiere2x + (tailleClairiere2x / 2))
    ):
        if (
            (positionClairiere2y - (tailleClairiere2y / 2))
            < y
            < (positionClairiere2y + (tailleClairiere2y / 2))
        ):
            coordonne2.remove((x, y, z))

for x, y, z in coordonne2:
    if (
        (positionClairiere2x - (tailleClairiere2x / 2))
        < x + 0.5
        < (positionClairiere2x + (tailleClairiere2x / 2))
    ):
        if (
            (positionClairiere2y - (tailleClairiere2y / 2))
            < y + 0.5
            < (positionClairiere2y + (tailleClairiere2y / 2))
        ):
            coordonne2.remove((x, y, z))

for x, y, z in coordonne2:
    if (
        (positionClairiere3x - (tailleClairiere3x / 2))
        < x
        < (positionClairiere3x + (tailleClairiere3x / 2))
    ):
        if (
            (positionClairiere3y - (tailleClairiere3y / 2))
            < y
            < (positionClairiere3y + (tailleClairiere3y / 2))
        ):
            coordonne2.remove((x, y, z))

for x, y, z in coordonne2:
    if (
        (positionClairiere3x - (tailleClairiere3x / 2))
        < x + 0.5
        < (positionClairiere3x + (tailleClairiere3x / 2))
    ):
        if (
            (positionClairiere3y - (tailleClairiere3y / 2))
            < y + 0.5
            < (positionClairiere3y + (tailleClairiere3y / 2))
        ):
            coordonne2.remove((x, y, z))

"""                 ----------------------------------------------------------
                        G E N E R A T E U R S   D E S   F O R M A T I O N S
                    ----------------------------------------------------------"""

# --------------------------------------------------
# Générer la volumetrie:

for x in coordonne:
    module(x)

for x in coordonne2:
    module2(x)
