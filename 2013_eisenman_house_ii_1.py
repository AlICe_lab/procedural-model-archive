# SPDX-FileCopyrightText: 2013 Diego Necochea
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# CREATION DU PORGRAMME FINAL- 23/01/2014
# Script développ sous windows 64 Bit - Blender 2.68a r 58537

import bpy  # importation de la bibliotheque
import random  # importation de random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# Definir la fonction qui établit chaque objet


def plan(epaisseur, largeur, hauteur, pos_X, pos_Y, pos_Z):
    bpy.ops.mesh.primitive_cube_add(
        location=(pos_X + epaisseur / 2, pos_Y + largeur / 2, pos_Z + hauteur / 2)
    )  # positionner l'objet depuis un des coins
    bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))  # réajuster dimension à l'unité
    bpy.ops.transform.resize(
        value=(epaisseur, largeur, hauteur)
    )  # réajuster dimension des objets


# PROCEDE DE PROFIL
# Soit dix plaques qui vont contenir de multiples d'objets. On prend la décision d'espacer ces 10 plaques de 3 unités chacune d'entre elles.
# Après avoir fini de créer les éléments avec les relations d'interdépendance on va les repositionner dans la vrai position de la house II.

# PROCEDE DE FACE

# Les colonnes et les planchers sont fixes et définissent 6 cadres.
# Soit 6 Cadres, 3 Cadres supérieurs et 3 cadres inférieurs. Le deux premiers cadres supérieur et inférieur commençant par la droite
# se trouvera le point de départ de notre programme (1er rang). Quand le programme commence il crée une relation de dépendance en profondeur qui
# génère des nouveaux cadres en profondeur. Lorsque le 4è cadre se crée, il crée aussi un cadre à coté respectant la dépendance latérale.
# Quand le cadre à coté est créé (deuxième rang), il génère comme le premier une dépendance en profondeur, des nouveaux cadres.
# Lorsque un de ces nouveaux cadres (de deuxième rang) arrivent au 6è cadre, il crée encore une fois un relation de dépendance latérale (troisième rang)
# qui lui même crée aussi une relation de profondeur.
# Quelques éléments ont été ajoutés au niveaux de quelques cadres dans le vide créé dans le deuxième et troisième
# rang. Il s'agissait de les placer dans des endroits précisés dans la House II tout en respectant la dépendance
# en profondeur. Dans le programme, c'est le même procédé de profondeur mais à l'envers


# DEFINITION DES DEPENDANCES ET CREATION DES CADRES


def Cadre1(
    x, a, b, c, d, e, posycadre, poszcadre, n, l
):  # Soit x variable qui génère le cadre suivant en profondeur, abcde les hauteurs des objets pour chaque cadre, posycadre-poszcadre la position du cadre, n la variable qui va arreter le programme (n<10) et variable l pour différencier le cadre supérieur et inférieur
    if n < 10:
        plan(
            0.2, 0.8, a + b + c + d, x, posycadre, poszcadre
        )  # Objet composant le cadre
        plan(
            0.2, 1.2, a + b + c, x, 1 + posycadre, poszcadre
        )  # Objet composant le cadre
        Cadre2(
            x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l
        )  # Relation de dépendance avec le cadre suivant en profondeur
    if (
        n == 3
    ):  # Relation de dépendance latéral dans la plaque 3 (si cadre1 dans n=3 donc cadre2 latéralement
        if l == 0:
            Cadre2(
                x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2
            )  # niveau supérieur
        if l == 1:
            Cadre2(
                x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2
            )  # niveau inférieur
    if (
        n == 5
    ):  # Relation de dépendance latéral des cadres crées après dépendance latéral
        if l == 2:  # niveau supérieur
            Cadre2(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:  # niveau inférieur
            Cadre2(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre2(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, a + b + c, x, posycadre, poszcadre)
        plan(0.2, 1.2, a + b + c, x, 1 + posycadre, poszcadre)
        Cadre2Post = random.randint(
            0, 1
        )  # Relation de dépendance en profondeur avec plusieurs possibilités de cadres
        if Cadre2Post == 0:
            Cadre3(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre2Post == 1:
            Cadre6(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre2cote = random.randint(
            0, 1
        )  # Relation de dépendance latéralement avec plusieurs possibilités de cadre
        if l == 0:
            if Cadre2cote == 0:
                Cadre1(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre2cote == 1:
                Cadre2(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre2cote == 0:
                Cadre1(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre2cote == 1:
                Cadre2(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre2cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre2cote1 == 0:
                Cadre1(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre2cote1 == 1:
                Cadre2(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre2cote1 == 0:
                Cadre1(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre2cote1 == 0:
                Cadre2(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre3(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, a + b + c, x, posycadre, poszcadre)
        plan(0.2, 1.2, a + b + c, x, 1 + posycadre, poszcadre)
        plan(0.2, 3.9, e, x, posycadre, a + b + c + d + poszcadre)
        Cadre6(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre14(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre14(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre14(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre14(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre4(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 3.9, d + e, x, posycadre, a + b + c + poszcadre)
        Cadre4Post = random.randint(0, 1)
        if Cadre4Post == 0:
            Cadre5(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre4Post == 1:
            Cadre8(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre4cote = random.randint(0, 1)
        if l == 0:
            if Cadre4cote == 0:
                Cadre4(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre4cote == 1:
                Cadre14(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre4cote == 0:
                Cadre4(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre4cote == 1:
                Cadre14(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre4cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre4cote1 == 0:
                Cadre4(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre4cote1 == 1:
                Cadre14(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre4cote1 == 0:
                Cadre4(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre4cote1 == 1:
                Cadre14(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)

    if (
        n == 3
    ):  # Creation des éléments se trouvant à l'extérieur de la forme de base qui sont disposés respectant la dépendance de profondeur (arbre en sens inverse)
        Cadre4ext = random.randint(0, 1)
        if l == 2:
            if Cadre4ext == 0:
                Cadre7(
                    x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2
                )  # x+3 et n+6 pour positionner le cadre et parce qu'il est unique
            if Cadre4ext == 1:
                Cadre16(
                    x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2
                )
        if l == 3:
            if Cadre4ext == 0:
                Cadre7(
                    x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2
                )
            if Cadre4ext == 1:
                Cadre16(
                    x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2
                )
    if n == 5:  # Meme chose pour le troisième rang
        Cadre4ext1 = random.randint(0, 1)
        if l == 4:
            if Cadre4ext1 == 0:
                Cadre7(
                    x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2
                )
            if Cadre4ext1 == 1:
                Cadre16(
                    x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2
                )
        if l == 5:
            if Cadre4ext1 == 0:
                Cadre7(
                    x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2
                )
            if Cadre4ext1 == 1:
                Cadre16(
                    x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2
                )


def Cadre5(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 1, c, x, posycadre, a + b + poszcadre)
        plan(0.2, 1.7, c, x, 2 + posycadre, a + b + poszcadre)
        Cadre5Post = random.randint(0, 1)
        if Cadre5Post == 0:
            Cadre6(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre5Post == 1:
            Cadre9(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre9(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre9(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre9(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre9(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)

    if n == 3:
        if l == 2:
            Cadre4(x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2)
        if l == 3:
            Cadre4(x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2)
    if n == 5:
        if l == 4:
            Cadre4(x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2)
        if l == 5:
            Cadre4(x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2)


def Cadre6(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 1, d + e, x, posycadre, a + b + c + poszcadre)
        Cadre7(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre6cote = random.randint(0, 1)
        if l == 0:
            if Cadre6cote == 0:
                Cadre15(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre6cote == 1:
                Cadre7(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre6cote == 0:
                Cadre15(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre6cote == 1:
                Cadre7(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre6cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre6cote1 == 0:
                Cadre15(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre6cote1 == 1:
                Cadre7(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre6cote1 == 0:
                Cadre15(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre6cote1 == 1:
                Cadre7(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre7(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.9, d + e, x, 2 + posycadre, a + b + c + poszcadre)
        Cadre4(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre6(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre6(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre6(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre6(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)

    if n == 3:
        if l == 2:
            Cadre6(x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2)
        if l == 3:
            Cadre6(x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2)
    if n == 5:
        if l == 4:
            Cadre6(x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2)
        if l == 5:
            Cadre6(x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2)


def Cadre8(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 3.9, c, x, posycadre, a + b + poszcadre)
        Cadre8Post = random.randint(0, 2)
        if Cadre8Post == 0:
            Cadre11(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre8Post == 1:
            Cadre12(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre8Post == 2:
            Cadre13(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre8cote = random.randint(0, 2)
        if l == 0:
            if Cadre8cote == 0:
                Cadre8(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre8cote == 1:
                Cadre11(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre8cote == 2:
                Cadre17(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre8cote == 0:
                Cadre8(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre8cote == 1:
                Cadre11(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre8cote == 2:
                Cadre17(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre8cote1 = random.randint(0, 2)
        if l == 2:
            if Cadre8cote1 == 0:
                Cadre8(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre8cote1 == 1:
                Cadre11(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre8cote1 == 2:
                Cadre17(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre8cote1 == 0:
                Cadre8(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre8cote1 == 1:
                Cadre11(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre8cote1 == 2:
                Cadre17(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
    if n == 3:
        if l == 2:
            Cadre4(x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2)
        if l == 3:
            Cadre4(x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2)
    if n == 5:
        if l == 4:
            Cadre4(x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2)
        if l == 5:
            Cadre4(x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2)


def Cadre9(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 3.1, a + b, x, posycadre, poszcadre)
        plan(0.1, 1, c, x, posycadre, a + b + poszcadre)
        Cadre9Post = random.randint(0, 1)
        if Cadre9Post == 0:
            Cadre8(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre9Post == 1:
            Cadre10(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre9cote = random.randint(0, 1)
        if l == 0:
            if Cadre9cote == 0:
                Cadre5(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre9cote == 1:
                Cadre10(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre9cote == 0:
                Cadre5(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre9cote == 1:
                Cadre10(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre9cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre9cote1 == 0:
                Cadre5(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre9cote1 == 1:
                Cadre10(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre9cote1 == 0:
                Cadre5(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre9cote1 == 1:
                Cadre10(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)

    if n == 3:
        if l == 2:
            Cadre14(x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2)
        if l == 3:
            Cadre14(x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2)
    if n == 5:
        if l == 4:
            Cadre14(x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2)
        if l == 5:
            Cadre14(x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2)


def Cadre10(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, a + b + c + d, x, posycadre, poszcadre)
        plan(0.1, 0.2, c + d, x, 0.8 + posycadre, a + b + poszcadre)
        Cadre9(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre10cote = random.randint(0, 1)
        if l == 0:
            if Cadre10cote == 0:
                Cadre9(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre10cote == 1:
                Cadre12(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre10cote == 0:
                Cadre9(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre10cote == 1:
                Cadre12(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre10cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre10cote1 == 0:
                Cadre9(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre10cote1 == 1:
                Cadre12(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre10cote1 == 0:
                Cadre9(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre10cote1 == 1:
                Cadre12(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre11(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 3.9, e, x, posycadre, a + b + c + d + poszcadre)
        plan(0.2, 3.9, c, x, posycadre, a + b + poszcadre)
        Cadre5(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre11cote = random.randint(0, 1)
        if l == 0:
            if Cadre11cote == 0:
                Cadre8(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre11cote == 1:
                Cadre13(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre11cote == 0:
                Cadre8(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre11cote == 1:
                Cadre13(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre11cote1 = random.randint(0, 1)
        if l == 2:
            if Cadre11cote1 == 0:
                Cadre8(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre11cote1 == 1:
                Cadre13(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre11cote1 == 0:
                Cadre8(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre11cote1 == 0:
                Cadre13(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre12(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, d + e, x, posycadre, a + b + c + poszcadre)
        plan(0.1, 3.9, c, x, posycadre, a + b + poszcadre)
        plan(0.2, 0.8, b, x, posycadre, a + poszcadre)
        plan(0.1, 3.9, a, x, posycadre, poszcadre)
        Cadre8(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre10(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre10(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre10(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre10(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre13(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 3.9, c, x, posycadre, a + b + poszcadre)
        plan(0.1, 3.9, d, x, posycadre, a + b + c + poszcadre)
        Cadre8(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre11(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre11(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre11(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre10(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre14(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, e, x, posycadre, a + b + c + d + poszcadre)
        plan(0.2, 2.9, e, x, 1 + posycadre, a + b + c + d + poszcadre)
        Cadre14Post = random.randint(0, 1)
        if Cadre14Post == 0:
            Cadre15(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre14Post == 1:
            Cadre9(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre14cote = random.randint(0, 3)
        if l == 0:
            if Cadre14cote == 0:
                Cadre3(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre14cote == 1:
                Cadre4(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre14cote == 2:
                Cadre14(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre14cote == 3:
                Cadre15(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre14cote == 0:
                Cadre3(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote == 1:
                Cadre4(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote == 2:
                Cadre14(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote == 3:
                Cadre15(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre14cote1 = random.randint(0, 3)
        if l == 2:
            if Cadre14cote1 == 0:
                Cadre3(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre14cote1 == 1:
                Cadre4(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre14cote1 == 2:
                Cadre14(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre14cote1 == 3:
                Cadre15(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre14cote1 == 0:
                Cadre3(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote1 == 1:
                Cadre4(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote1 == 2:
                Cadre14(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre14cote1 == 3:
                Cadre15(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre15(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.2, a + b + c + d + e, x, 0.8 + posycadre, poszcadre)
        Cadre15Post = random.randint(0, 1)
        if Cadre15Post == 0:
            Cadre16(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
        if Cadre15Post == 1:
            Cadre17(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        Cadre15cote = random.randint(0, 2)
        if l == 0:
            if Cadre15cote == 0:
                Cadre6(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre15cote == 1:
                Cadre14(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
            if Cadre15cote == 2:
                Cadre16(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            if Cadre15cote == 0:
                Cadre6(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre15cote == 1:
                Cadre14(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre15cote == 2:
                Cadre16(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        Cadre15cote1 = random.randint(0, 2)
        if l == 2:
            if Cadre15cote1 == 0:
                Cadre6(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre15cote1 == 1:
                Cadre14(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
            if Cadre15cote1 == 2:
                Cadre16(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            if Cadre15cote1 == 0:
                Cadre6(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre15cote1 == 1:
                Cadre14(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)
            if Cadre15cote1 == 2:
                Cadre16(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)

    if n == 3:
        if l == 2:
            Cadre14(x + 3, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n + 6, l + 2)
        if l == 3:
            Cadre14(x + 6, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n + 6, l + 2)
    if n == 5:
        if l == 4:
            Cadre14(x + 3, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n + 6, l + 2)
        if l == 5:
            Cadre14(x + 6, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n + 6, l + 2)


def Cadre16(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 0.8, e, x, posycadre, a + b + c + d + poszcadre)
        plan(0.2, 2.1, e, x, 1 + posycadre, a + b + c + d + poszcadre)
        Cadre4(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre15(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre15(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre15(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre15(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


def Cadre17(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    if n < 10:
        plan(0.2, 1, a + b, x, posycadre, poszcadre)
        plan(0.1, 0.2, c + d, x, 0.8 + posycadre, a + b + poszcadre)
        plan(0.2, 1, e, x, posycadre, a + b + c + d + poszcadre)
        Cadre9(x - 3, a, b, c, d, e, posycadre, poszcadre, n + 1, l)
    if n == 3:
        if l == 0:
            Cadre8(x, 0.7, 0.9, 0.7, 0.5, 0.3, posycadre - 4.1, 3.5, n, l + 2)
        if l == 1:
            Cadre8(x, 0.5, 1.1, 0.5, 0.5, 0.5, posycadre - 4.1, 0.2, n, l + 2)
    if n == 5:
        if l == 2:
            Cadre8(x, 0.7, 1.2, 0.4, 1.2, 0.3, posycadre - 4.1, 2.8, n, l + 2)
        if l == 3:
            Cadre8(x, 0.5, 0.4, 0.7, 0.5, 0.3, posycadre - 4.1, 0.2, n, l + 2)


# FONCTION QUI PERMET DE DETERMINER LE CADRE INITIAL


def Premier(x, a, b, c, d, e, posycadre, poszcadre, n, l):
    Pr = random.randint(1, 17)
    if Pr == 1:
        Cadre1(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 2:
        Cadre2(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 3:
        Cadre3(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 4:
        Cadre4(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 5:
        Cadre5(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 6:
        Cadre6(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 7:
        Cadre7(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 8:
        Cadre8(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 9:
        Cadre9(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 10:
        Cadre10(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 11:
        Cadre11(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 12:
        Cadre12(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 13:
        Cadre13(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 14:
        Cadre14(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 15:
        Cadre15(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 16:
        Cadre16(x, a, b, c, d, e, posycadre, poszcadre, n, l)
    if Pr == 17:
        Cadre17(x, a, b, c, d, e, posycadre, poszcadre, n, l)


# DEBUT DE PROGRAMME


def PremierQuadranthaut():
    Premier(0, 0.5, 0.4, 0.7, 0.5, 0.3, 8.4, 4.2, 0, 0)


def PremierQuadrantbas():
    Premier(0, 0.5, 1.1, 0.5, 0.5, 1.2, 8.4, 0.2, 0, 1)


PremierQuadranthaut()
PremierQuadrantbas()


# MISE EN POSITION DES DIX PLAQUES

for object in bpy.data.objects:
    if object.location.x < -2:
        if object.location.x > -4:
            object.location.x = -0.8

for object in bpy.data.objects:
    if object.location.x < -5:
        if object.location.x > -7:
            object.location.x = -1.7

for object in bpy.data.objects:
    if object.location.x < -8:
        if object.location.x > -10:
            object.location.x = -4.2

for object in bpy.data.objects:
    if object.location.x < -11:
        if object.location.x > -13:
            object.location.x = -4.9

for object in bpy.data.objects:
    if object.location.x < -14:
        if object.location.x > -16:
            object.location.x = -8.1

for object in bpy.data.objects:
    if object.location.x < -17:
        if object.location.x > -19:
            object.location.x = -9

for object in bpy.data.objects:
    if object.location.x < -20:
        if object.location.x > -22:
            object.location.x = -12.2

for object in bpy.data.objects:
    if object.location.x < -23:
        if object.location.x > -25:
            object.location.x = -13.1

for object in bpy.data.objects:
    if object.location.x < -26:
        if object.location.x > -28:
            object.location.x = -14.1


# CREATION DE COLONNES

plan(0.2, 0.2, 6.7, -0.8, 0, 0.2)
plan(0.2, 0.2, 6.7, -0.8, 4.1, 0.2)
plan(0.2, 0.2, 6.7, -0.8, 8.2, 0.2)
plan(0.2, 0.2, 6.7, -0.8, 12.3, 0.2)

plan(0.2, 0.2, 6, -4.9, 0, 0.2)
plan(0.2, 0.2, 6, -4.9, 4.1, 0.2)
plan(0.2, 0.2, 6, -4.9, 8.2, 0.2)
plan(0.2, 0.2, 6, -4.9, 12.3, 0.2)

plan(0.2, 0.2, 6, -9, 0, 0.2)
plan(0.2, 0.2, 6, -9, 4.1, 0.2)
plan(0.2, 0.2, 6, -9, 8.2, 0.2)
plan(0.2, 0.2, 6, -9, 12.3, 0.2)

plan(0.2, 0.2, 6, -13.1, 0, 0.2)
plan(0.2, 0.2, 6, -13.1, 4.1, 0.2)
plan(0.2, 0.2, 6, -13.1, 8.2, 0.2)
plan(0.2, 0.2, 6, -13.1, 12.3, 0.2)

plan(0.2, 0.2, 6, -14.1, 4.1, 0.2)
plan(0.2, 0.2, 6, -14.1, 8.2, 0.2)

# CREATION DES PLANCHERS

plan(-14.4, 12.5, 0.2, 0.2, 0, 0.2)
plan(-4.2, 4.3, 0.2, 0, 8.2, 4.2)
plan(-3.2, 4.3, 0.2, -4.9, 8.2, 3.5)
plan(-3.9, 4.1, 0.2, -4.2, 4.1, 3.5)
plan(-4.3, 8.4, 0.2, -8.8, 4.1, 2.6)
plan(-5.2, 4.1, 0.2, -7.9, 0, 2.6)
