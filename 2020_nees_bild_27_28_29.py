# SPDX-FileCopyrightText: 2020 Ammar Aharchi Azami-Housni & Lauren Amrani
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Gearg Nees - Bild 27-28-29
# Amrani Lauren & Aharchi Azami-Housni Ammar
# 21/01/2021
# Option AIM 2020-2021 module 1


import random
from mathutils import Vector
import pdb
import math
import bpy
import copy
from math import radians
from math import cos
from math import sin


def nettoyage():
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete(use_global=False)
    bpy.ops.outliner.orphans_purge()
    # nettoie toute la zone


nettoyage()


def calcul_centres(f):
    # calcules les centres des petits cubes contenus dans le grand
    result = []
    coef = 10 / (f)
    points = [coef * i for i in range(1, (f + 1))]
    for x in points:
        for y in points:
            for z in points:
                x = round(x, 2)
                y = round(y, 2)
                z = round(z, 2)
                result.append((x, y, z))
    return result


def calcul_coordonees(segments, a, b):

    # donne une série de nombres correspondants aux données des coordonnées des segments avec segments= nombre de coudes-3
    # a et b sont les limites des différentes boucles
    serie = []
    while len(serie) < segments:
        n = random.uniform(a, b)
        if n != 0:
            serie.append(n)

    # déterminer les coordonnées de départ (0,0,0)
    # à chaque i, changer une cordonnée aléatoire entre x, y et z
    coords = [(0, 0, 0)]
    choice = [0, 1, 2]
    for i in range(len(serie)):
        new = coords[-1]
        new = list(new)
        new[random.randint(0, 2)] = serie[i]
        coords.append(new)
    # déterminer les coordonnées d'arrivée(0,0,0)
    random.shuffle(choice)
    for i in choice:
        new = coords[-1]
        new = list(new)
        new[i] = 0
        coords.append(new)
    # return pour avoir les coordonnées et pouvoir les utiliser
    return coords


def make_curves(coords, nom):

    # fonction qui va créer les "curves" et leur donner une épaisseur
    curveData = bpy.data.curves.new(nom, type="CURVE")
    curveData.dimensions = "3D"

    # donne l'onthométrie et l'utilisation des coordonnées
    polyline = curveData.splines.new("POLY")
    polyline.points.add(len(coords))
    for i, coord in enumerate(coords):
        x, y, z = coord
        polyline.points[i].co = (x, y, z, 1)

        # crée l'objet et le nomme, lui donne une épaisseur
        curveOB = bpy.data.objects.new(nom, curveData)
        curveData.bevel_depth = 0.1

        # met l'objet dans la collection
        myCollection = bpy.context.collection
        myCollection.objects.link(curveOB)


def get_objects(prefix):
    # prends la liste d'objetx commençant par le préfixe
    result = []
    for obj in bpy.data.objects:
        if obj.name.startswith(prefix):
            result.append(obj)
    return result


def select(nom):
    # selectionne les objets commençant par le préfixe "nom"
    bpy.ops.object.select_all(action="DESELECT")
    for obj in get_objects(nom):
        obj.select_set(True)


def translation(point_projete):
    # décale ces objets (la boucle faite des curves)
    bpy.ops.transform.translate(
        value=point_projete,
        orient_type="GLOBAL",
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
        orient_matrix_type="GLOBAL",
        mirror=True,
        use_proportional_edit=False,
        proportional_edit_falloff="SMOOTH",
        proportional_size=1,
        use_proportional_connected=False,
        use_proportional_projected=False,
        release_confirm=True,
    )


# déterminer la grille du cube
centres = calcul_centres(5)


# pour chaque petit cube, crée une boucle et la déplace vers ce dernier
def main():
    for i in range(0, len(centres)):
        k = calcul_coordonees(20, -2, 2)
        make_curves(k, str(i))
        select(str(i))
        translation(centres[i])


main()


# Definition de la fonction de création de camera en projection parallele
def axoCam(projection, canon):
    bpy.ops.object.camera_add()
    maScene = bpy.context.scene.render
    monAxoCam = bpy.context.object
    monAxoCam.data.type = "ORTHO"
    monAxoCam.data.ortho_scale = 10

    if projection == "axonometrique":
        if canon == "isometrie":  # OK
            monAxoCam.name = "axoIsometrie"
            monAxoCam.rotation_euler = (radians(54.74), 0.0, radians(45))
            monAxoCam.location = (10.0, -10.0, 10.0)
            maScene.pixel_aspect_x = 1
        if canon == "dimetrie":  # OK
            monAxoCam.name = "axoDimetrie"
            monAxoCam.rotation_euler = (radians(60), 0.0, radians(23))
            monAxoCam.location = (5.53, -13.04, 8.18)
            maScene.pixel_aspect_x = 1
        if canon == "trimetrie":  # OK
            monAxoCam.name = "axoTrimetrie"
            monAxoCam.rotation_euler = (radians(67), 0.0, radians(34))
            monAxoCam.location = (8.59, -12.734, 6.52)
            maScene.pixel_aspect_x = 1

    elif projection == "oblique":
        if canon == "militaire":  # OK
            monAxoCam.name = "oblMilitaire"
            monAxoCam.rotation_euler = (radians(45), 0.0, radians(30))
            monAxoCam.location = (7.071, -12.247, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(radians(45))
        if canon == "militaireDiminuee":  # OK
            monAxoCam.name = "oblMilitaireDiminuee"
            monAxoCam.rotation_euler = (radians(45 * 0.82), 0.0, radians(30))
            monAxoCam.location = (5.309, -9.195, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(radians(45 * 0.82))
        if canon == "cavalierePlan":  # OK
            monAxoCam.name = "oblCavalierePlan"
            monAxoCam.rotation_euler = (radians(45), 0.0, radians(45))
            monAxoCam.location = (10.0, -10.0, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(radians(45))
        if canon == "cavalierePlanDiminuee":  # OK
            monAxoCam.name = "oblCavalierePlanDiminuee"
            monAxoCam.rotation_euler = (radians(45 * 0.82), 0.0, radians(45))
            monAxoCam.location = (7.5, -7.5, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(radians(45 * 0.82))
        if canon == "cavaliereAngleCam":  # OK mais à recentrer manuellement
            angleZ = radians(30)  # entrer l'angle de rotation de la camera en Z
            angleZ_XY = radians(
                15
            )  # entrer l'angle d'inclinaison de la camera par rapport au sol
            monAxoCam.name = "oblCavalierePlanDiminuee"
            monAxoCam.rotation_euler = (angleZ_XY, 0.0, angleZ)
            monAxoCam.location = (10, -10, 10 * 1 / cos(radians(45)))
            maScene.pixel_aspect_x = 1 / cos(angleZ_XY)


####################
# PROJECTION OBLIQUE#
####################
# axoCam ('oblique','militaire')
# axoCam ('oblique','militaireDiminuee')
# axoCam ('oblique','cavalierePlan')
# axoCam ('oblique','cavalierePlanDiminuee')
# axoCam ('oblique','cavaliereAngleCam') #celle ci peut être réglée en angle de vue -> regarder dans la dernière formule

##########################
# PROJECTION AXONOMETRIQUE#
##########################
# axoCam ('axonometrique','isometrie')
# axoCam ('axonometrique','dimetrie')
axoCam("axonometrique", "trimetrie")
