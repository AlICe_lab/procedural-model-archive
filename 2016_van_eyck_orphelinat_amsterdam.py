# SPDX-FileCopyrightText: 2016 Ali Alami Talbi
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

add_cube = bpy.ops.mesh.primitive_cube_add
scene = bpy.context.scene
add_cube = bpy.ops.mesh.primitive_cube_add  # racourcis pour ajouter cube
scene = bpy.context.scene  # raccourcis pour la scene


########################################
# PARAMETRE DU PROJET
########################################

# Definition taille des branches
choice_pavillons_SE = random.randint(0, 10)
forme = "X"  # tout autre valeur que 'Y' donne une disposition en 'X'
if forme == "Y":
    choice_pavillons_SE = 0
# Definition tailles des Pavillons
choice_croix_NE = random.randint(0, 10)

choice_pavillons_NW = random.randint(0, 10)
choice_croix_SW = random.randint(0, 10)
if choice_croix_SW >= 4 and choice_pavillons_NW > 0:
    professeur_west = True
else:
    professeur_west = False

# Definition tailles diverses
y_noyau = 18
x_noyau = 15
x_pavillon = 18
y_pavillon = 9
x_croix1 = 12
y_croix1 = 15
x_croix2 = 21
y_croix2 = 6
espace_croix_x = x_croix1
espace_croix_x_haut = 2
espace_croix_y = y_croix1

hauteur_pavillon = 6
hauteur_croix_equipement = 3
hauteur_croix_b = 3

#### gros equipement
y_cube_parallele = 9
x_cube_perp = 12
espace_eq_x = y_pavillon + 3
#### equipement noyau
long_eq_noyau = y_croix1 / 2 + y_croix2 / 2 + y_croix1 + 3
#### professeur
taille_professeur = 3
#### ajouter ou Supprimer professeur
professeur_est = True


########################################
# CREATION DES FONCTIONS DES PAVILLONS
########################################


# Definition du noyau de base
def creer_noyau(y_noyau, x_noyau):
    add_cube(location=(0, 0, 1.5), radius=0.5)
    noyau = bpy.context.object
    noyau.scale.y = y_noyau
    noyau.scale.x = x_noyau
    noyau.scale.z = 3
    return noyau


# Definition du Pavillon
def creer_pavillon(
    x_pavillon, y_pavillon, pos_x, pos_y, espace_x, fin_de_ligne, hauteur
):
    add_cube(location=(pos_x, pos_y, hauteur / 2), radius=0.5)
    pavillon = bpy.context.object
    pavillon.scale.y = y_pavillon
    pavillon.scale.x = x_pavillon
    pavillon.scale.z = hauteur

    # ajout de la circulation 1 - Circulation vericale du pavillon
    pos_x_circulation1 = pos_x - x_pavillon / 2 - 1.5
    add_cube(location=(pos_x_circulation1, pos_y - 1.5, 1.5), radius=0.5)
    circulation1 = bpy.context.object
    circulation1.scale.y = y_pavillon + 3
    circulation1.scale.x = 3
    circulation1.scale.z = 3

    # circulation2 - Circulation Horizontale du pavillon
    x_circulation2 = espace_x / 2 + x_pavillon / 2
    pos_x_circulation2 = pos_x + espace_x / 4 - x_pavillon / 4
    pos_y_circulation2 = pos_y - y_pavillon / 2 - 1.5
    add_cube(location=(pos_x_circulation2, pos_y_circulation2, 1.5), radius=0.5)
    circulation2 = bpy.context.object
    circulation2.scale.y = 3
    circulation2.scale.z = 3
    circulation2.scale.x = x_circulation2

    return pavillon


# Definition du pavillon 2 - Forme croix
def creer_croix(
    x_croix1,
    y_croix1,
    x_croix2,
    y_croix2,
    pos_x,
    pos_y,
    espace_x,
    espace_y,
    encrage,
    fin_de_ligne,
    encrage_bas,
    hauteur_eq,
    hauteur_b,
):
    add_cube(location=(pos_x - 3, pos_y, hauteur_eq / 2), radius=0.5)
    croix1 = bpy.context.object
    croix1.scale.y = y_croix1
    croix1.scale.x = x_croix1
    croix1.scale.z = hauteur_eq

    add_cube(location=(pos_x, pos_y, hauteur_b / 2), radius=0.5)
    croix2 = bpy.context.object
    croix2.scale.y = y_croix2
    croix2.scale.x = x_croix2
    croix2.scale.z = hauteur_b

    support = [pos_x - 3, pos_y, x_croix1, y_croix1]

    # circulation1 -  Horizontale
    x_circulation1 = espace_x
    if encrage_bas == True:
        x_circulation1 = (x_croix2 - x_croix1) / 2 + 6
    pos_x_circulation1 = (pos_x - 3) + x_croix1 / 2 + x_circulation1 / 2
    pos_y_circulation1 = pos_y - y_croix2 / 2 - 1.5
    y_circulation1 = 3
    if fin_de_ligne == False:
        add_cube(location=(pos_x_circulation1, pos_y_circulation1, 1.5), radius=0.5)
        circulation1 = bpy.context.object
        circulation1.scale.y = y_circulation1
        circulation1.scale.x = x_circulation1
        circulation1.scale.z = 3
    else:
        if pos_y < 0:
            add_cube(location=(pos_x_circulation1, pos_y_circulation1, 1.5), radius=0.5)
            circulation1 = bpy.context.object
            circulation1.scale.y = y_circulation1
            circulation1.scale.x = x_circulation1
            circulation1.scale.z = 3

    # circulation2 - Vericale
    pos_x_circulation2 = (pos_x - 3) + x_croix1 / 2 + 1.5
    x_circulation2 = 3
    y_circulation2 = (y_croix1 - y_croix2) / 2 + espace_y
    if encrage == True:
        y_circulation2 = (y_croix1 - y_croix2) / 2
    pos_y_circulation2 = pos_y - y_croix2 / 2 - y_circulation2 / 2
    if fin_de_ligne == False:
        add_cube(location=(pos_x_circulation2, pos_y_circulation2, 1.5), radius=0.5)
        circulation2 = bpy.context.object
        circulation2.scale.y = y_circulation2
        circulation2.scale.x = x_circulation2
        circulation2.scale.z = 3
    else:
        if pos_y > 0:
            add_cube(location=(pos_x_circulation2, pos_y_circulation2, 1.5), radius=0.5)
            circulation2 = bpy.context.object
            circulation2.scale.y = y_circulation2
            circulation2.scale.x = x_circulation2
            circulation2.scale.z = 3
    return support


# Definition du pavillon 2- (forme en croix) : Ancrage
def creer_croix_haut(
    x_croix1,
    y_croix1,
    x_croix2,
    y_croix2,
    pos_x,
    pos_y,
    espace_x,
    fin_de_ligne,
    hauteur_h,
    hauteur_b,
):
    add_cube(location=(pos_x - 3, pos_y, hauteur_h / 2), radius=0.5)
    croix1 = bpy.context.object
    croix1.scale.y = y_croix1
    croix1.scale.x = x_croix1
    croix1.scale.z = hauteur_h

    add_cube(location=(pos_x, pos_y, hauteur_b / 2), radius=0.5)
    croix2 = bpy.context.object
    croix2.scale.y = y_croix2
    croix2.scale.x = x_croix2
    croix2.scale.z = hauteur_b

    # circulations1 - Verticale
    pos_x_circulation1 = pos_x - 1.5 - x_croix2 / 2
    x_circulation1 = 3
    y_circulation1 = (y_croix1 + y_croix2) / 2
    pos_y_circulation1 = pos_y + (-y_croix2 + y_circulation1) / 2
    add_cube(location=(pos_x_circulation1, pos_y_circulation1, 1.5), radius=0.5)
    circulation1 = bpy.context.object
    circulation1.scale.y = y_circulation1
    circulation1.scale.x = x_circulation1
    circulation1.scale.z = 3

    # circulation2 - Horizontale
    x_circulation2 = x_croix2 / 2 + x_croix1 / 2 + x_croix1
    pos_x_circulation2 = pos_x - x_croix2 / 2 - 3 + x_circulation2 / 2
    y_circulation2 = 3
    pos_y_circulation2 = pos_y + y_croix1 / 2 + 1.5
    add_cube(location=(pos_x_circulation2, pos_y_circulation2, 1.5), radius=0.5)
    circulation2 = bpy.context.object
    circulation2.scale.y = y_circulation2
    circulation2.scale.x = x_circulation2
    circulation2.scale.z = 3


########################################
# CREATION DES FONCTIONS DES EQUIPEMENTS
########################################


# Definition equipement en L
def creer_equipement_L(
    x_noyau,
    y_noyau,
    x_croix1,
    y_croix1,
    x_croix2,
    y_croix2,
    espace_y_croix,
    espace_x_croix,
    hauteur,
):
    y_circulation_l = (y_croix1 - y_croix2) / 2 + espace_y_croix
    # eq haut
    x_eq_haut = (x_croix2 - x_croix1) / 2 + 3
    pos_x_eq_haut = -x_noyau / 2 + 3 - x_eq_haut / 2
    y_eq_haut = 2 * (y_circulation_l - 3)
    pos_y_eq_haut = -y_noyau / 2 - 3 - y_eq_haut / 2
    add_cube(location=(pos_x_eq_haut, pos_y_eq_haut, hauteur / 2), radius=0.5)
    eq_haut = bpy.context.object
    eq_haut.scale.y = y_eq_haut
    eq_haut.scale.x = x_eq_haut
    eq_haut.scale.z = hauteur
    support1 = [pos_x_eq_haut, pos_y_eq_haut, x_eq_haut, y_eq_haut]

    # eq_bas
    y_eq_bas = y_circulation_l - 6
    x_eq_bas = 2 * espace_x_croix + x_eq_haut
    pos_x_eq_bas = pos_x_eq_haut + x_eq_haut / 2 - x_eq_bas / 2
    pos_y_eq_bas = pos_y_eq_haut - y_eq_haut / 2 - y_eq_bas / 2
    add_cube(location=(pos_x_eq_bas, pos_y_eq_bas, hauteur / 2), radius=0.5)
    eq_bas = bpy.context.object
    eq_bas.scale.y = y_eq_bas
    eq_bas.scale.x = x_eq_bas
    eq_bas.scale.z = hauteur
    return support1


# Definition equipement en U
def creer_gros_equipement(
    pos_x,
    pos_y,
    x_pavillons,
    y_pavillons,
    y_cube_parallele,
    x_cube_perp,
    espace_eq_x,
    hauteur,
):

    # cube // haut
    x_cube_haut = x_pavillons + espace_eq_x
    pos_x_cube_haut = pos_x + x_cube_haut / 2 - x_pavillons / 2
    pos_y_cube_haut = pos_y + y_pavillons / 2 + y_cube_parallele / 2
    add_cube(location=(pos_x_cube_haut, pos_y_cube_haut, hauteur / 2), radius=0.5)
    cube_haut = bpy.context.object
    cube_haut.scale.y = y_cube_parallele
    cube_haut.scale.x = x_cube_haut
    cube_haut.scale.z = hauteur

    # cube // bas
    x_cube_bas = x_cube_haut
    pos_x_cube_bas = pos_x_cube_haut
    pos_y_cube_bas = pos_y_cube_haut - y_pavillons - y_cube_parallele - 3
    add_cube(location=(pos_x_cube_bas, pos_y_cube_bas, hauteur / 2), radius=0.5)
    cube_bas = bpy.context.object
    cube_bas.scale.y = y_cube_parallele
    cube_bas.scale.x = x_cube_bas
    cube_bas.scale.z = hauteur

    # cube perp
    pos_x_cube_perp = pos_x + x_pavillons / 2 + espace_eq_x + x_cube_perp / 2
    pos_y_cube_perp = pos_y - 1.5
    y_cube_perp = y_pavillons + y_cube_parallele + y_cube_parallele + 3
    add_cube(location=(pos_x_cube_perp, pos_y_cube_perp, hauteur / 2), radius=0.5)
    cube_perp = bpy.context.object
    cube_perp.scale.y = y_cube_perp
    cube_perp.scale.x = x_cube_perp
    cube_perp.scale.z = hauteur
    support = [pos_x, pos_y_cube_bas, x_cube_bas, y_cube_parallele]
    return support


# Definition equipement en I
def creer_gros_equipement_I(pos_x, pos_y, x_pavillons, y_pavillons, hauteur):
    # cube bas
    x_cube_bas = x_pavillons * 2
    y_cube_bas = y_pavillons
    pos_y_cube_bas = pos_y - 3
    pos_x_cube_bas = pos_x - 0.5 * x_pavillon - 3 - x_cube_bas / 2
    add_cube(location=(pos_x_cube_bas, pos_y_cube_bas, hauteur / 2), radius=0.5)
    cube_bas = bpy.context.object
    cube_bas.scale.y = y_cube_bas
    cube_bas.scale.x = x_cube_bas
    cube_bas.scale.z = hauteur
    support = [pos_x_cube_bas, pos_y_cube_bas, x_cube_bas, y_cube_bas]
    return support


# Definition equipement Noyau
def creer_equipement_noyeau(y_noyau, x_noyau, x_croix1, x_croix2, y_eq, hauteur):
    x_eq = x_noyau - 3 - (x_croix2 / 2 - x_croix1 / 2 - 3)
    pos_x = -x_noyau / 2 + x_eq / 2
    pos_y = y_noyau / 2 + y_eq / 2
    add_cube(location=(pos_x, pos_y, hauteur / 2), radius=0.5)
    cube = bpy.context.object
    cube.scale.y = y_eq
    cube.scale.x = x_eq
    cube.scale.z = hauteur


def creer_professeur_west(support1, support2, hauteur, taille):
    # support = [ pos_x support, pos_y_support, taille_x_support, taille_y_support]
    y_batiment = support2[1] - support1[1] + support2[3] / 2 + support1[3] / 2
    x_batiment = support1[2]
    pos_y_bat = support2[1] + support2[3] / 2 - y_batiment / 2
    pos_x_bat = support1[0]
    add_cube(location=(pos_x_bat, pos_y_bat, hauteur + taille / 2), radius=0.5)
    batiment = bpy.context.object
    batiment.scale.y = y_batiment
    batiment.scale.x = x_batiment
    batiment.scale.z = taille


def creer_professeur_est(supportA, supportB, hauteur, taille):
    # support = [ pos_x support, pos_y_support, taille_x_support, taille_y_support]
    y_batiment = supportA[3]
    x_batiment = supportA[0] - supportB[0] + supportB[2] / 2
    pos_y_bat = supportA[1]
    pos_x_bat = supportA[0] - x_batiment / 2
    add_cube(location=(pos_x_bat, pos_y_bat, hauteur + taille / 2), radius=0.5)
    batiment = bpy.context.object
    batiment.scale.y = y_batiment
    batiment.scale.x = x_batiment
    batiment.scale.z = taille


####################################################################
# CREATION DU PROJET : BOUCLES
####################################################################

support1 = []
support2 = []
supportA = []
supportB = []

noyau = creer_noyau(y_noyau, x_noyau)

creer_equipement_noyeau(
    y_noyau, x_noyau, x_croix1, x_croix2, long_eq_noyau, hauteur_croix_equipement
)

for i in range(choice_pavillons_SE):  # X positifs et Y negatifs
    pav = creer_pavillon(
        x_pavillon,
        y_pavillon,  # dimensions
        x_noyau / 2 + (i + 0.5) * x_pavillon,  # decallage en x
        -y_noyau / 2 + y_pavillon / 2 + 3 - i * y_pavillon,  # decallage y
        x_pavillon,  # espacement x
        1,
        hauteur_pavillon,
    )  # fin de ligne
    if i == choice_pavillons_SE - 1:
        supportA = creer_gros_equipement(
            x_noyau / 2 + (i + 0.5) * x_pavillon,
            -y_noyau / 2 + y_pavillon / 2 + 3 - i * y_pavillon,
            x_pavillon,
            y_pavillon,
            y_cube_parallele,
            x_cube_perp,
            espace_eq_x,
            hauteur_croix_equipement,
        )


for i in range(choice_pavillons_NW):  # X negatifs et Y positifs
    pav = creer_pavillon(
        x_pavillon,
        y_pavillon,  # dimensions
        -x_noyau / 2 - (i + 0.5) * x_pavillon,  # decallage en x
        y_noyau / 2 + y_pavillon / 2 + i * y_pavillon,  # decallage y
        x_pavillon,  # espacement x
        1,
        hauteur_pavillon,
    )  # fin de ligne
    if i == 0:
        support2 = creer_gros_equipement_I(
            -x_noyau / 2 - 0.5 * x_pavillon,
            y_noyau / 2 + y_pavillon / 2,
            x_pavillon,
            y_pavillon,
            hauteur_croix_equipement,
        )


for i in range(choice_croix_SW):
    croix = creer_croix(
        x_croix1,
        y_croix1,
        x_croix2,
        y_croix2,  # X negatifs et Y negatifs
        -x_noyau / 2 - x_croix2 / 2 - i * (espace_croix_x),
        (-y_noyau + y_croix2) / 2 - i * (espace_croix_y + 1.5),
        espace_croix_x,
        espace_croix_y,
        False,
        i == (choice_croix_SW - 1),
        i == 0,
        hauteur_croix_equipement,
        hauteur_croix_b,
    )
    if i == 3:
        support1 = croix

for i in range(choice_croix_NE):
    croix = creer_croix(
        x_croix1,
        y_croix1,
        x_croix2,
        y_croix2,  # X positifs et positifs
        (x_noyau / 2 + x_croix1 / 2 - 1.5)
        + x_croix2
        - y_croix1 / 2
        + i * (espace_croix_x),
        (y_noyau / 2 + (y_croix2 - y_croix1) / 2)
        + y_croix2
        + 4.5
        + espace_croix_y
        + i * (espace_croix_y + 1.5),
        espace_croix_x,
        espace_croix_y,
        i == 0,
        i == (choice_croix_NE - 1),
        False,
        hauteur_croix_equipement,
        hauteur_croix_b,
    )


####################################################################
# CAS PARTICULIERS
####################################################################
if choice_croix_SW > 2:
    supportB = creer_equipement_L(
        x_noyau,
        y_noyau,
        x_croix1,
        y_croix1,
        x_croix2,
        y_croix2,
        espace_croix_y,
        espace_croix_x,
        hauteur_croix_equipement,
    )


croix = creer_croix_haut(
    x_croix1,
    y_croix1,
    x_croix2,
    y_croix2,  # X Positifs et Y positifs
    x_noyau / 2 + x_croix1 / 2 + 3,
    y_noyau / 2 + (y_croix2 - y_croix1) / 2 + y_croix1 / 2,
    espace_croix_x_haut,
    1,
    hauteur_croix_equipement,
    hauteur_croix_b,
)

if professeur_west == True:
    creer_professeur_west(
        support1, support2, hauteur_croix_equipement, taille_professeur
    )
if professeur_est == True:
    if (
        forme != "Y"
        and choice_pavillons_SE > 0
        and choice_pavillons_SE <= 4
        and choice_croix_SW > 2
    ):
        creer_professeur_est(
            supportA, supportB, hauteur_croix_equipement, taille_professeur
        )
