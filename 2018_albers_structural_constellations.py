# SPDX-FileCopyrightText: 2018 Giovanni Chionetti & Hakim Ben Baghdad
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Import blender tools
import bpy
import random


# Deleting all objects
def delete_all():
    for item in bpy.data.objects:
        bpy.data.objects.remove(item)
    for item in bpy.data.meshes:
        bpy.data.meshes.remove(item)


delete_all()

# Creating vertices
mesVertices = [
    (0, 0, 0),
    (0, 0, 1),
    (0, 1, 0.5),
    (0, 1, 0),
    (1, -1, 1.5),
    (1, -1, 0.5),
    (1, 0, 0.5),
    (1, 0, 1),
    (-1, 1, -0.5),
    (0, 1, 0),
    (0, 1, 0.5),
    (-1, 1, 0.5),
    (2, -1, 1),
    (2, -2, 1),
    (-1, 2, 0),
    (-2, 2, 0),
    (1, 0, 0),
    (1, 1, 0),
    (1, 1, 1),
    (1, 0, 1),
    (0, 1, 0.5),
    (0, 2, 0.5),
    (0, 2, -0.5),
    (0, 1, -0.5),
    (1, 0, 1),
    (1, 0, 0),
    (2, 0, 0.5),
    (2, 0, 1.5),
    (0, 1, 0.5),
    (1, 1, 1),
    (1, 1, 0),
    (0, 1, -0.5),
    (1, 1, 1),
    (3, -1, 1),
    (3, -2, 1),
    (1, 0, 1),
    (1, 0, 0),
    (1, 1, 0),
    (-1, 2, 0),
    (-1, 3, 0),
    (0, 0, 1),
    (1, 0, 1),
    (2, 0, 1.5),
    (1, 0, 1.5),
    (0, 1, 1),
    (1, 1, 1),
    (-1, 1, 0.5),
    (0, 1, 0.5),
    (0, 0, 1),
    (1, 0, 1),
    (1, 1, 1),
    (0, 1, 1),
    (0, 1, 0.5),
    (0, 2, 0.5),
    (-1, 2, 0.5),
    (-1, 1, 0.5),
    (1, -1, 1.5),
    (1, 0, 1.5),
    (1, 1, 1),
    (1, 0, 1),
    (0, 0, 1),
    (0, 1, 1),
    (0, 2, 0.5),
    (0, 1, 0.5),
]

# Creating list of possible faces
malist = [
    (0, 1, 2, 3),
    (4, 5, 6, 7),
    (6, 7, 1, 0),
    (8, 9, 10, 11),
    (1, 7, 12, 13),
    (0, 3, 14, 15),
]

malist2 = [
    (16, 17, 18, 19),
    (20, 21, 22, 23),
    (24, 25, 26, 27),
    (28, 29, 30, 31),
    (32, 33, 34, 35),
    (36, 37, 39, 38),
]

malist3 = [
    (40, 41, 42, 43),
    (44, 45, 47, 46),
    (48, 49, 50, 51),
    (52, 53, 54, 55),
    (56, 57, 58, 59),
    (60, 61, 62, 63),
]

malist4 = [
    (0, 1, 2, 3),
    (4, 5, 6, 7),
    (6, 7, 1, 0),
    (8, 9, 10, 11),
    (1, 7, 12, 13),
    (0, 3, 14, 15),
]

malist5 = [
    (16, 17, 18, 19),
    (20, 21, 22, 23),
    (24, 25, 26, 27),
    (28, 29, 30, 31),
    (32, 33, 34, 35),
    (36, 37, 39, 38),
]

malist6 = [
    (40, 41, 42, 43),
    (44, 45, 47, 46),
    (48, 49, 50, 51),
    (52, 53, 54, 55),
    (56, 57, 58, 59),
    (60, 61, 62, 63),
]

# Put random value for three variables
a = random.randint(0, 5)
b = random.randint(0, 5)
c = random.randint(0, 5)
d = random.randint(0, 5)
e = random.randint(0, 5)
f = random.randint(0, 5)

# Create faces
mesFaces = [(malist[a]), (malist2[b]), (malist3[c])]

# Create mesh and object
monMesh = bpy.data.meshes.new("face")
monObjet = bpy.data.objects.new("face", monMesh)

# Join object to the space
maScene = bpy.context.scene
maScene.objects.link(monObjet)

# Fill mesh with vertices and faces information
monMesh.from_pydata(mesVertices, [], mesFaces)
bpy.context.scene.objects.active = monObjet

# move and rotate the cube to make the upper cube
bpy.ops.object.select_all(action="TOGGLE")
bpy.ops.transform.rotate(
    value=-3.13749,
    axis=(-0.667249, 0.666093, -0.333314),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)
bpy.ops.transform.translate(
    value=(0, 1, 2),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

# create second cube
mesFaces = [(malist4[d]), (malist5[e]), (malist6[f])]

# Create mesh and object
monMesh = bpy.data.meshes.new("face")
monObjet = bpy.data.objects.new("face", monMesh)

# Join object to the space
maScene = bpy.context.scene
maScene.objects.link(monObjet)

# Fill mesh with vertices and faces information
monMesh.from_pydata(mesVertices, [], mesFaces)
bpy.context.scene.objects.active = monObjet

# Extrude the faces
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.join()
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.extrude_region_move(
    MESH_OT_extrude_region={"mirror": False},
    TRANSFORM_OT_translate={
        "value": (-1.5, 1.5, -0.75),
        "constraint_axis": (False, False, False),
        "constraint_orientation": "GLOBAL",
        "mirror": False,
        "proportional": "DISABLED",
        "proportional_edit_falloff": "SMOOTH",
        "proportional_size": 1,
        "snap": False,
        "snap_target": "CLOSEST",
        "snap_point": (0, 0, 0),
        "snap_align": False,
        "snap_normal": (0, 0, 0),
        "gpencil_strokes": False,
        "texture_space": False,
        "remove_on_cancel": False,
        "release_confirm": False,
        "use_accurate": False,
    },
)

# Delete useless vertices
bpy.ops.mesh.select_all(action="DESELECT")
bpy.ops.mesh.select_face_by_sides()
bpy.ops.mesh.select_all(action="INVERT")
bpy.ops.mesh.delete(type="VERT")
bpy.ops.object.editmode_toggle()

# Mark edges for rendering
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.editmode_toggle()
bpy.context.object.data.show_edge_crease = False
bpy.context.object.data.show_faces = False
bpy.ops.mesh.select_all(action="TOGGLE")
bpy.ops.mesh.mark_freestyle_edge(clear=False)
bpy.ops.object.editmode_toggle()

# rotate model
bpy.ops.object.select_all(action="SELECT")
bpy.ops.transform.rotate(
    value=0.785398,
    axis=(-0, -0, -1),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

# translate model
bpy.ops.object.select_all(action="SELECT")
bpy.ops.transform.translate(
    value=(0.70192, 1.47975, 0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)

# insert the box
bpy.ops.mesh.primitive_cube_add(
    radius=2.11368 / 2,
    view_align=False,
    enter_editmode=False,
    location=(2.11368 / 2, 2.11368 / 2, 2.11368 / 2),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.context.object.draw_type = "WIRE"
"""
#apply the boolean modification
bpy.data.objects['face.001'].select = True
bpy.context.scene.objects.active = bpy.data.objects['face.001']
bpy.ops.object.modifier_add(type='BOOLEAN')
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Cube"]
bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")
"""
# put top view camera
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(1.12894, 1.7272, 3.68052),
    rotation=(0, -0, 0),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.ops.transform.translate(
    value=(-1.66266, -0.273314, -0),
    constraint_axis=(False, False, False),
    constraint_orientation="GLOBAL",
    mirror=False,
    proportional="DISABLED",
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
)
bpy.context.object.data.type = "ORTHO"
bpy.context.object.data.ortho_scale = 12

# put side elevation camera
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(4.00775, 1.97089, 1.42532),
    rotation=(1.5708, -0, 1.5708),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.context.object.data.type = "ORTHO"
bpy.context.object.data.ortho_scale = 12

# put view camera
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(7.2563, -0.069643, 7.2374),
    rotation=(0.770001, -5.90978e-008, 1.043),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.context.object.data.type = "ORTHO"
bpy.context.object.data.ortho_scale = 12

# Put camera for illusion
bpy.ops.object.camera_add(
    view_align=True,
    enter_editmode=False,
    location=(1.72770, -10.08522, 5.02548),
    rotation=(1.230980, 0, 0.000867),
    layers=(
        True,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
        False,
    ),
)
bpy.context.object.data.type = "ORTHO"
