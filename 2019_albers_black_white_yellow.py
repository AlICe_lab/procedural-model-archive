# SPDX-FileCopyrightText: 2019 Jeano Mertens
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# -*- coding:Utf8 -*-

#################################################
# ANNI ALBERS - Black White Yellow              #
# Authors: MERTENS Jeano                        #
# Date: [26.08.2020]                            #
# Blender version: [2.8 & hash]                 #
# OS: [Windows 10]                              #
#################################################

import bpy
import random


###################################################
def reset():
    """
    Cleaning datafile through saving and reopening
    !!! only works if .blend file previously saved
    """
    # helpers
    save = bpy.ops.wm.save_mainfile
    open = bpy.ops.wm.open_mainfile
    path = bpy.data.filepath

    # action
    if path is "":
        save()
        path = bpy.data.filepath
        print(".blend file saved in home directory")

    save(filepath=path)
    open(filepath=path)


def delete():
    """
    Delete scene before running script
    """
    # helpers
    select = bpy.ops.object.select_all
    delete = bpy.ops.object.delete

    # action
    select(action="SELECT")
    delete(use_global=False)


##################################################
# Creating a list for each weaving patterns, plain and satin

plainweave1 = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
plainweave = plainweave1.copy()

satinweave1 = [1, 0, 0, 1, 0, 0, 1, 0, 0, 1]
satinweave = satinweave1.copy()


# shifting for the plain weave
def shift_p():
    plainweave.insert(0, plainweave[len(plainweave) - 1])
    plainweave.pop()
    return plainweave


patternshift_p = shift_p()


# Cubes with the plain pattern
def cubes_p():
    for x, y in zip(patternshift_p, range(len(plainweave))):
        bpy.ops.mesh.primitive_cube_add(location=(0, y, x * 0.9), size=1)
        item = "MESH"
    bpy.ops.object.select_all(action="DESELECT")
    bpy.ops.object.select_by_type(type=item)
    bpy.ops.transform.translate(value=(1, 0, 0))


# Shifting for the satin weave
def shift_s():
    satinweave.insert(0, satinweave[len(satinweave) - 1])
    satinweave.pop()
    return satinweave


patternshift_s = shift_s()


# Cubes with the satin pattern
def cubes_s():
    for x, z in zip(patternshift_s, range(len(satinweave))):
        bpy.ops.mesh.primitive_cube_add(location=(1, x * 0.9, z), size=1)
        item = "MESH"
    bpy.ops.object.select_all(action="DESELECT")
    bpy.ops.object.select_by_type(type=item)
    bpy.ops.transform.translate(value=(0, 0, 0))


""" trial: generate cylinders with a pattern/list
def cylinder():
    for x, y in zip(patternshift_p,range(len(plainweave))):
        bpy.ops.mesh.primitive_cylinder_add(location=(0, y, x*.5), radius = .25, depth = 5)
        item3='MESH'
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.object.select_by_type(type='MESH')
    bpy.ops.transform.translate(value=(0, 0, 0))
"""


# Generate cylinders
def weft():
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 0, 5), radius=0.25, depth=9.5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 1, 5), radius=0.25, depth=9.5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 2, 3), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 3, 2), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 4, 3), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 5, 2), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 6, 3), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 7, 3), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 8, 2), radius=0.25, depth=5)
    bpy.ops.mesh.primitive_cylinder_add(location=(0, 9, 3), radius=0.25, depth=5)


# Generate the meshes twice -> repetition

for i in range(10):
    cylinder()
    shift_p()
    shift_s()
    cubes_p()
    cubes_s()

for i in range(10):
    weft()
    shift_p()
    shift_s()
    cubes_p()
    cubes_s()

# Rescale everything
item = "MESH"
bpy.ops.object.select_all(action="DESELECT")
bpy.ops.object.select_by_type(type=item)
bpy.ops.transform.resize(value=(0.5, 1, 1))
