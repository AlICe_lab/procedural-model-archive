# SPDX-FileCopyrightText: 2015 Raphaëlle Bouvart
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

# VARIABLE INPUT

matrice_villa = [2, 2, 4]

dim_1 = 5

# VARIABLE GÉNÉRÉE

coef_1 = dim_1 / (dim_1 - 1)
dim_2 = dim_1 / coef_1


coef_2 = (dim_1 - 1) / (dim_1 - 2)
dim_3 = dim_2 / coef_2

# FONCTIONS


def cube(location, cote):
    x, y, z = location
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(x + cote / 2, y + cote / 2, z + cote / 2)
    )
    bpy.ops.transform.resize(value=(cote, cote, cote))


# CRÉATION DE LA MATRICE_1, MATRICE COMPOSÉE DES MODULES DE BASES

matrice_1 = []

for x in range(0, matrice_villa[0]):
    for y in range(0, matrice_villa[1]):
        for z in range(0, matrice_villa[2]):

            X, Y, Z = x * dim_1, y * dim_1, z * dim_1

            matrice_1.append((X, Y, Z))


# CRÉATION DE LA MATRICE_COMPO ET DE LA MATRICE_2, COMPRENANT RESPECTIVEMENT LE TYPE DE COMPOSITION POUR CHAQUE MODULE ET LES COORDONNÉES DES MODULES DE TRAME INFÉRIEURE

matrice_compo = []
matrice_2 = []


for module in matrice_1:

    compo = choice(["rien", "1", "multiple"])

    matrice_compo.append(compo)

    x, y, z = module

    pos_1 = (x, y, z)
    pos_2 = (x + dim_1 - dim_2, y, z)
    pos_3 = (x, y + dim_1 - dim_2, z)
    pos_4 = (x + dim_1 - dim_2, y + dim_1 - dim_2, z)

    position = choice([pos_1, pos_2, pos_3, pos_4])

    matrice_2.append(position)


# CRÉATION DE LA MATRICE_3, MATRICE COMPOSÉE DES MODULES DE TRAME DE DERNIER RANG

matrice_3 = []


for module in matrice_2:

    x, y, z = module
    pos_1 = (x, y, z)
    pos_2 = (x + dim_2 - dim_3, y, z)
    pos_3 = (x, y + dim_2 - dim_3, z)
    pos_4 = (x + dim_2 - dim_3, y + dim_2 - dim_3, z)
    position = choice([pos_1, pos_2, pos_3, pos_4])
    matrice_3.append(position)


# CRÉATION DES VOLUMES POUR TOUTE LES COORDONNÉES COLLECTÉES DANS MATRICE_1, MATRICE_2 ET MATRICE_3 SUIVANT LES VALEURS COLLECTÉES DANS MATRICE_COMPO

for i in range(0, len(matrice_1)):

    cube(matrice_1[i], dim_1)

    if matrice_compo[i] == "rien":
        pass
    elif matrice_compo[i] == "1":
        cube(matrice_2[i], dim_2)
    elif matrice_compo[i] == "multiple":
        cube(matrice_2[i], dim_2)
        cube(matrice_3[i], dim_3)
