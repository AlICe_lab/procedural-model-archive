# SPDX-FileCopyrightText: 2016 Axel Ricbourg
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
import random
from random import *
import math
from math import *


bpy.ops.object.select_all(action="SELECT")

bpy.ops.object.delete(use_global=False)


# Determination des Coefficients > Variables des configurations en fonction de l'éloignement la densité change !
rang = 6
col = 6
KnCell = 5
KnBranch = 5

# Définition Tronc

RayonTronc = 10


def Tronc(position, nom):
    bpy.ops.mesh.primitive_cylinder_add(radius=RayonTronc, depth=250, location=position)
    bpy.context.object.name = nom

    # Définition Cellule


def Cellule(position, nom, branche, rotation):
    bpy.ops.mesh.primitive_cube_add(enter_editmode=True, radius=0.5, location=branche)
    dimension = (6, 14, 3)
    bpy.ops.transform.resize(value=dimension)
    bpy.ops.transform.translate(value=(position[0], position[1], 3))
    bpy.ops.object.editmode_toggle()

    bpy.ops.transform.rotate(value=radians(rotation), axis=(0, 0, 1))

    bpy.context.object.name = nom

    # il y a la fonction branche et l'ensemble des branches (Bloc)


def Branche(position, dimension, nom, rotation, Quin, Nbcell):

    bpy.ops.mesh.primitive_cube_add(enter_editmode=True, radius=0.5, location=position)
    bpy.ops.transform.resize(value=dimension)
    xdim, ydim, zdim = dimension
    bpy.ops.transform.translate(value=(xdim / 2 + RayonTronc, 0, 0))
    bpy.ops.object.editmode_toggle()
    bpy.ops.transform.rotate(value=radians(rotation), axis=(0, 0, 1))
    bpy.context.object.name = nom

    # Création Boucle pour la position des cellules + Condition pour les Nbr de cellules

    boucle_A = dimension[0] // 6
    if Nbcell == 0:
        for ix in range(0, boucle_A - 1):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )

    if Nbcell == 1:
        for ix in range(0, boucle_A - 3):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )

    if Nbcell == 2:
        for ix in range(0, boucle_A - 5):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
    if Nbcell == 3:
        for ix in range(0, boucle_A - 7):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
    if Nbcell == 4:
        for ix in range(0, boucle_A - 9):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
    if Nbcell == 5:
        for ix in range(0, boucle_A - 11):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
    if Nbcell == 6:
        for ix in range(0, boucle_A - 13):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
    if Nbcell == 7:
        for ix in range(0, boucle_A - 15):
            # division par reste pour déterminer les impaires
            if Quin == True:
                if ix % 2 == 1:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 6,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
            else:
                if ix % 2 == 0 and ix < boucle_A - 3:
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            7 + dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )
                    Cellule(
                        (
                            ix * 6 + RayonTronc + 18,
                            -7 - dimension[1] / 2,
                            position[2] + 3,
                        ),
                        "cellule",
                        position,
                        rotation,
                    )

    # Création d'une variable pour le nombre de branche et que celles ci soit en quinconces.


NbrA = randint(8, 9)

if NbrA == 8:
    NbrB = 9
if NbrA == 9:
    NbrB = 8


def Bloc(position, dimension, nbr, nom, rotation, Nbcell):

    # on execute la fonction Branche pour la branche principale

    Branche(position, dimension, nom, rotation, True, Nbcell)

    # determiner nombre de branche au dessus et en dessous

    branche_dessus = []
    branche_dessous = []

    # déterminer les conditions

    if nbr == 7:

        branche_dessus.append((position[2] + 6, (90, 3, 3), "branche2"))
        branche_dessus.append((position[2] + 12, (36, 3, 3), "branche3"))
        branche_dessus.append((position[2] + 18, (18, 3, 3), "branche4"))

        branche_dessous.append((position[2] - 6, (72, 3, 3), "branche5"))
        branche_dessous.append((position[2] - 12, (54, 3, 3), "branche6"))
        branche_dessous.append((position[2] - 18, (36, 3, 3), "branche7"))

    if nbr == 8:

        branche_dessus.append((position[2] + 6, (114, 3, 3), "branche8"))
        branche_dessus.append((position[2] + 12, (48, 3, 3), "branche9"))
        branche_dessus.append((position[2] + 18, (30, 3, 3), "branche10"))

        branche_dessous.append((position[2] - 6, (96, 3, 3), "branche11"))
        branche_dessous.append((position[2] - 12, (84, 3, 3), "branche12"))
        branche_dessous.append((position[2] - 18, (60, 3, 3), "branche13"))
        branche_dessous.append((position[2] - 24, (42, 3, 3), "branche14"))

    if nbr == 9:

        branche_dessus.append((position[2] + 6, (102, 3, 3), "branche15"))
        branche_dessus.append((position[2] + 12, (48, 3, 3), "branche16"))
        branche_dessus.append((position[2] + 18, (30, 3, 3), "branche17"))
        branche_dessus.append((position[2] + 24, (24, 3, 3), "branche18"))

        branche_dessous.append((position[2] - 6, (84, 3, 3), "branche19"))
        branche_dessous.append((position[2] - 12, (66, 3, 3), "branche20"))
        branche_dessous.append((position[2] - 18, (48, 3, 3), "branche21"))
        branche_dessous.append((position[2] - 24, (30, 3, 3), "branche22"))

    # boucle pour créer les branches du dessus

    trigger = 0
    for branche in branche_dessus:
        trigger += 1
        position_z, dimension, nom = branche
        x, y, z = position
        if trigger % 2 == 1:
            Branche((x, y, position_z), dimension, nom, rotation, False, Nbcell)
        else:
            Branche((x, y, position_z), dimension, nom, rotation, True, Nbcell)

    # boucle pour créer les branches du dessous

    trigger = 0
    for branche in branche_dessous:
        trigger += 1
        position_z, dimension, nom = branche
        x, y, z = position
        if trigger % 2 == 1:
            Branche((x, y, position_z), dimension, nom, rotation, False, Nbcell)
        else:
            Branche((x, y, position_z), dimension, nom, rotation, True, Nbcell)

    # Création des Blocs/Plots + condition pour les différences suivant les distances par la suite


def Plot(localisation, nom, Nbcell, NbBra):

    xloc, yloc, zloc = localisation
    if NbBra == 0:

        Tronc(localisation, "Tronc1")
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocA", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocD", 270, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocH", 270, Nbcell)

    if NbBra == 1:

        Tronc(localisation, "Tronc1")
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocA", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocD", 270, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocH", 270, Nbcell)

    if NbBra == 2:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocH", 270, Nbcell)

    if NbBra == 5:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 270, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocH", 270, Nbcell)

    if NbBra == 6:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 270, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)

    if NbBra == 8:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 270, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 270, Nbcell)

    if NbBra == 3:

        Tronc(localisation, "Tronc1")
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocA", 120, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 240, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 120, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 240, Nbcell)

    if NbBra == 4:

        Tronc(localisation, "Tronc1")
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocA", 210, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 90, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 330, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 120, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 240, Nbcell)

    if NbBra == 7:

        Tronc(localisation, "Tronc1")
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocA", 120, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 240, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrB, "BlocF", 120, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 240, Nbcell)

    if NbBra == 9:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 270, Nbcell)

    if NbBra == 10:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)
        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)

    if NbBra == 11:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 0, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 90, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 270, Nbcell)

    if NbBra == 12:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocB", 90, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocE", 0, Nbcell)
        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)

    if NbBra == 13:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 180, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 270, Nbcell)

    if NbBra == 14:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 0, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)

    if NbBra == 15:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc + 67), (114, 3, 3), 7, "BlocC", 90, Nbcell)

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 0, Nbcell)

    if NbBra == 16:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 270, Nbcell)

    if NbBra == 17:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 180, Nbcell)

    if NbBra == 18:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 0, Nbcell)

    if NbBra == 19:

        Tronc(localisation, "Tronc1")

        Bloc((xloc, yloc, zloc - 58), (114, 3, 3), NbrA, "BlocG", 90, Nbcell)


# Création des plots, avec une boucles avec les facteurs de densité

for x in range(rang):
    for y in range(col):
        nCell = int(hypot(x, y) * KnCell)
        nBranch = int(hypot(x, y) * KnBranch)

        Plot((x * 238, y * 238, 0), "plot", nCell, nBranch)


# Quitte l'edit mode

bpy.ops.object.editmode_toggle()
