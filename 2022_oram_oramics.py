# SPDX-FileCopyrightText: 2022 Loan Le Merlus & Quentin Leroy
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# CURVE MANIPULATION - 27/11/22
# written under windows 11 - Blender 3.2.2

import bpy


##################
#    CLEANING    #
##################

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete()
bpy.ops.outliner.orphans_purge()

#####################
# IMPORT CURVES SVG #
#####################


bpy.ops.import_curve.svg(filepath="courbes 1 final PAR CALQUES.svg")
bpy.ops.import_curve.svg(filepath="courbes 1 final PAR CALQUES.svg")


#########################
# SET CURVES Z POSITION #
#########################

# Scale 1 Curves

bpy.data.objects["Curve"].location.y = +8.3
bpy.data.objects["Curve.009"].location.y = +8.3
bpy.data.objects["Curve.009"].location.z = +1.428

bpy.data.objects["Curve.001"].location.z = +0.0
bpy.data.objects["Curve.010"].location.z = +1.428

bpy.data.objects["Curve.002"].location.y = +8.3
bpy.data.objects["Curve.002"].location.z = +2.857
bpy.data.objects["Curve.011"].location.y = +8.3
bpy.data.objects["Curve.011"].location.z = +4.285


bpy.data.objects["Curve.003"].location.z = +2.857
bpy.data.objects["Curve.012"].location.z = +4.285

bpy.data.objects["Curve.004"].location.y = +0
bpy.data.objects["Curve.004"].location.z = +5.713
bpy.data.objects["Curve.013"].location.y = +0
bpy.data.objects["Curve.013"].location.z = +7.141

bpy.data.objects["Curve.005"].location.y = +8.3
bpy.data.objects["Curve.005"].location.z = +5.713
bpy.data.objects["Curve.014"].location.y = +8.3
bpy.data.objects["Curve.014"].location.z = +7.141

bpy.data.objects["Curve.006"].location.z = +8.569
bpy.data.objects["Curve.015"].location.z = +10

bpy.data.objects["Curve.007"].location.y = +8.3
bpy.data.objects["Curve.007"].location.z = +8.569
bpy.data.objects["Curve.016"].location.y = +8.3
bpy.data.objects["Curve.016"].location.z = +10


# Scale 2 Curves


bpy.data.objects["Curve.008"].location.y = +8.3
bpy.data.objects["Curve.017"].location.z = +0


#################################
# RENAME OBJECT FOR EACH CURVES #
#################################

obj0 = bpy.context.scene.objects["Curve"]
# bpy.ops.data['Curve'].name = ['Curve.000']
obj0.name = "Curve.000"
obj0 = bpy.context.scene.objects["Curve.000"]
obj01 = bpy.context.scene.objects["Curve.001"]
obj02 = bpy.context.scene.objects["Curve.002"]
obj03 = bpy.context.scene.objects["Curve.003"]
obj04 = bpy.context.scene.objects["Curve.004"]
obj05 = bpy.context.scene.objects["Curve.005"]
obj06 = bpy.context.scene.objects["Curve.006"]
obj07 = bpy.context.scene.objects["Curve.007"]

obj08 = bpy.context.scene.objects["Curve.008"]
obj09 = bpy.context.scene.objects["Curve.009"]
obj10 = bpy.context.scene.objects["Curve.010"]
obj11 = bpy.context.scene.objects["Curve.011"]

obj12 = bpy.context.scene.objects["Curve.012"]
obj13 = bpy.context.scene.objects["Curve.013"]
obj14 = bpy.context.scene.objects["Curve.014"]
obj15 = bpy.context.scene.objects["Curve.015"]
obj16 = bpy.context.scene.objects["Curve.016"]
obj17 = bpy.context.scene.objects["Curve.017"]


######################
# SCALE EACH OBJECTS #
######################

obj0.scale.xy = 12.5
obj01.scale.xy = 12.5
obj02.scale.xy = 12.5
obj03.scale.xy = 12.5
obj04.scale.xy = 12.5
obj05.scale.xy = 12.5
obj06.scale.xy = 12.5
obj07.scale.xy = 12.5

obj08.scale.xy = 22

obj09.scale.xy = 12.5
obj10.scale.xy = 12.5
obj11.scale.xy = 12.5
obj12.scale.xy = 12.5
obj13.scale.xy = 12.5
obj14.scale.xy = 12.5
obj15.scale.xy = 12.5
obj16.scale.xy = 12.5

obj17.scale.xy = 22


obj17.select_set(False)
#####################################################################################################
# Fix the origin of curve 01
###########################

bpy.context.view_layer.objects.active = obj01
obj01.select_set(True)
bpy.ops.object.convert(target="MESH")
obj01.select_set(False)

D = bpy.data
obj01 = D.objects["Curve.001"]

# get vertices from object as list
vertices = list(obj01.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj01.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj01.select_set(False)

# Fix the origin of curve 017
############################

bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="MESH")
obj17.select_set(False)

D = bpy.data
obj17 = D.objects["Curve.017"]

# get vertices from object as list
vertices = list(obj17.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj17.select_set(True)
bpy.context.view_layer.objects.active = obj17

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj17.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj17.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 001 & curve 017
##############################################

obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj01.select_set(False)

obj17.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 01 on curve 017 and scale
########################################################

bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj01
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj01.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Fix the origin of curve 010
############################

bpy.context.view_layer.objects.active = obj10
obj10.select_set(True)
bpy.ops.object.convert(target="MESH")
obj10.select_set(False)

D = bpy.data
obj010 = D.objects["Curve.013"]

# get vertices from object as list
vertices = list(obj10.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj10.select_set(True)
bpy.context.view_layer.objects.active = obj10

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj10.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj10.select_set(False)

# Set the same origin for curve 010 & curve 017
##############################################


obj10.select_set(True)
bpy.context.view_layer.objects.active = obj10

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj10.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 013 on curve 020 and scale
########################################################

bpy.context.view_layer.objects.active = obj10
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj10.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj10.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 001 & curve 013
################################################

obj10.select_set(True)
bpy.ops.object.duplicate()
obj18 = bpy.context.scene.objects["Curve.018"]
bpy.ops.object.select_all(action="DESELECT")
obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01
obj10.select_set(True)
bpy.context.view_layer.objects.active = obj10
bpy.ops.object.join()
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 03
###########################

bpy.context.view_layer.objects.active = obj03
obj03.select_set(True)
bpy.ops.object.convert(target="MESH")
obj03.select_set(False)

D = bpy.data
obj03 = D.objects["Curve.003"]

# get vertices from object as list
vertices = list(obj03.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj03.select_set(True)
bpy.context.view_layer.objects.active = obj03

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj03.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj03.select_set(False)

# Set the same origin for curve 003 & curve 017
##############################################

obj03.select_set(True)
bpy.context.view_layer.objects.active = obj03

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj03.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 003 on curve 017 and scale
########################################################


bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj03
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj03.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj03.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 003 & curve 018 (the duplicated 001)
####################################################################

obj03.select_set(True)
bpy.ops.object.duplicate()
bpy.ops.object.select_all(action="DESELECT")
obj18.select_set(True)
bpy.context.view_layer.objects.active = obj18
obj03.select_set(True)
bpy.context.view_layer.objects.active = obj03

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")

#####################################################################################################
# Fix the origin of curve 012
############################

bpy.context.view_layer.objects.active = obj12
obj12.select_set(True)
bpy.ops.object.convert(target="MESH")
obj12.select_set(False)

D = bpy.data
obj12 = D.objects["Curve.012"]

# get vertices from object as list
vertices = list(obj12.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj12.select_set(True)
bpy.context.view_layer.objects.active = obj12

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj12.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj12.select_set(False)

# Set the same origin for curve 012 & curve 017
##############################################

obj12.select_set(True)
bpy.context.view_layer.objects.active = obj12

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj12.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 012 on curve 017 and scale
########################################################


bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)

bpy.context.view_layer.objects.active = obj12
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj12.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj12.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 003 & curve 018 (the duplicated 001)
####################################################################

obj12.select_set(True)
bpy.ops.object.duplicate()
bpy.ops.object.select_all(action="DESELECT")
obj01 = bpy.context.scene.objects["Curve.001"]
obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01
obj12.select_set(True)
bpy.context.view_layer.objects.active = obj12

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################

# Fix the origin of curve 004
############################

bpy.context.view_layer.objects.active = obj04
obj04.select_set(True)
bpy.ops.object.convert(target="MESH")
obj04.select_set(False)

D = bpy.data
obj04 = D.objects["Curve.004"]

# get vertices from object as list
vertices = list(obj04.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj04.select_set(True)
bpy.context.view_layer.objects.active = obj04

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj04.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj04.select_set(False)

# Set the same origin for curve 004 & curve 017
##############################################

obj04.select_set(True)
bpy.context.view_layer.objects.active = obj04

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj04.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 004 on curve 017 and scale
########################################################


bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj04
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj04.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj04.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 004 & curve 018 (the duplicated 012)
####################################################################

obj04.select_set(True)
bpy.ops.object.duplicate()

bpy.ops.object.select_all(action="DESELECT")

obj18 = bpy.context.scene.objects["Curve.018"]
obj04.select_set(True)
bpy.context.view_layer.objects.active = obj04
obj18.select_set(True)
bpy.context.view_layer.objects.active = obj18

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 013
############################

bpy.context.view_layer.objects.active = obj13
obj13.select_set(True)
bpy.ops.object.convert(target="MESH")
obj13.select_set(False)

D = bpy.data
obj13 = D.objects["Curve.013"]

# get vertices from object as list
vertices = list(obj13.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj13.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj13.select_set(False)

# Set the same origin for curve 013 & curve 017
##############################################

obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj13.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 013 on curve 017 and scale
########################################################

bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj13
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj13.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj13.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 013 & curve 001 (the duplicated 013)
####################################################################

obj13.select_set(True)
bpy.ops.object.duplicate()

bpy.ops.object.select_all(action="DESELECT")

obj01 = bpy.context.scene.objects["Curve.001"]
obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13
obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 006
############################

bpy.context.view_layer.objects.active = obj06
obj06.select_set(True)
bpy.ops.object.convert(target="MESH")
obj06.select_set(False)

D = bpy.data
obj06 = D.objects["Curve.006"]

# get vertices from object as list
vertices = list(obj06.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj06.select_set(True)
bpy.context.view_layer.objects.active = obj06

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj06.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj06.select_set(False)

# Set the same origin for curve 013 & curve 017
##############################################

obj06.select_set(True)
bpy.context.view_layer.objects.active = obj06

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj06.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 013 on curve 017 and scale
########################################################

bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj06
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj06.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj06.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 006 & curve 004 (the duplicated 001)
####################################################################

obj06.select_set(True)
bpy.ops.object.duplicate()

bpy.ops.object.select_all(action="DESELECT")

obj04 = bpy.context.scene.objects["Curve.004"]
obj06.select_set(True)
bpy.context.view_layer.objects.active = obj06
obj04.select_set(True)
bpy.context.view_layer.objects.active = obj04

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 015
############################

bpy.context.view_layer.objects.active = obj15
obj15.select_set(True)
bpy.ops.object.convert(target="MESH")
obj15.select_set(False)

D = bpy.data
obj15 = D.objects["Curve.015"]

# get vertices from object as list
vertices = list(obj15.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj15.select_set(True)
bpy.context.view_layer.objects.active = obj15

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj15.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj15.select_set(False)

# Set the same origin for curve 015 & curve 017
##############################################

obj15.select_set(True)
bpy.context.view_layer.objects.active = obj15

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj15.select_set(False)

obj17.select_set(True)
for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj17.select_set(False)

# Apply curve Modifier to curve 015 on curve 017 and scale
########################################################

bpy.context.view_layer.objects.active = obj17
obj17.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj17.select_set(False)


bpy.context.view_layer.objects.active = obj15
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.017"]
obj15.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
obj15.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 015 & curve 013(the duplicated 004)
####################################################################

obj13 = bpy.context.scene.objects["Curve.013"]
obj15.select_set(True)
bpy.context.view_layer.objects.active = obj15
obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
#####################################################################################################
# Second Wall
############

# Fix the origin of curve 000
############################

bpy.context.view_layer.objects.active = obj0
obj0.select_set(True)
bpy.ops.object.convert(target="MESH")
obj0.select_set(False)

D = bpy.data
obj0 = D.objects["Curve.000"]

# get vertices from object as list
vertices = list(obj0.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj0.select_set(True)
bpy.context.view_layer.objects.active = obj0

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj0.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj0.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Fix the origin of curve 008
############################

bpy.context.view_layer.objects.active = obj08
obj08.select_set(True)
bpy.ops.object.convert(target="MESH")
obj08.select_set(False)

D = bpy.data
obj08 = D.objects["Curve.008"]

# get vertices from object as list
vertices = list(obj08.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj08.select_set(True)
bpy.context.view_layer.objects.active = obj08

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj08.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj08.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Rotate the curve 008
#####################

obj08.select_set(True)
bpy.context.view_layer.objects.active = obj08
bpy.ops.transform.rotate(value=3.14, orient_axis="X")
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 000 & curve 008
##############################################


obj0.select_set(True)
bpy.context.view_layer.objects.active = obj01

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj0.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 000 on curve 008 and scale
########################################################


bpy.context.view_layer.objects.active = obj08
obj08.select_set(True)
bpy.ops.object.convert(target="CURVE")
obj08.select_set(False)


bpy.context.view_layer.objects.active = obj0
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj0.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Fix the origin of curve 009
############################

bpy.context.view_layer.objects.active = obj09
obj09.select_set(True)
bpy.ops.object.convert(target="MESH")
obj09.select_set(False)

D = bpy.data
obj09 = D.objects["Curve.009"]

# get vertices from object as list
vertices = list(obj09.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj09.select_set(True)
bpy.context.view_layer.objects.active = obj09

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj09.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj09.select_set(False)
bpy.ops.object.select_all(action="DESELECT")


# Set the same origin for curve 009 & curve 008
##############################################


obj09.select_set(True)
bpy.context.view_layer.objects.active = obj09

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj09.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 009 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj09
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj09.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 000 & curve 009
################################################

obj09.select_set(True)
bpy.ops.object.duplicate()
obj06 = bpy.context.scene.objects["Curve.006"]
bpy.ops.object.select_all(action="DESELECT")
obj0.select_set(True)
bpy.context.view_layer.objects.active = obj0
obj09.select_set(True)
bpy.context.view_layer.objects.active = obj09

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 002
############################

bpy.context.view_layer.objects.active = obj02
obj02.select_set(True)
bpy.ops.object.convert(target="MESH")
obj02.select_set(False)

D = bpy.data
obj02 = D.objects["Curve.002"]

# get vertices from object as list
vertices = list(obj02.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj02.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj02.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 002 & curve 008
##############################################

obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj02.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 002 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj02
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj02.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 002 & curve 006 (the duplicated)
################################################################

obj02.select_set(True)
bpy.ops.object.duplicate()
obj15 = bpy.context.scene.objects["Curve.015"]
bpy.ops.object.select_all(action="DESELECT")
obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02
obj06.select_set(True)
bpy.context.view_layer.objects.active = obj06

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 011
############################

bpy.context.view_layer.objects.active = obj11
obj11.select_set(True)
bpy.ops.object.convert(target="MESH")
obj11.select_set(False)

D = bpy.data
obj11 = D.objects["Curve.011"]

# get vertices from object as list
vertices = list(obj11.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj11.select_set(True)
bpy.context.view_layer.objects.active = obj11

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj11.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj11.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 011 & curve 008
##############################################

obj11.select_set(True)
bpy.context.view_layer.objects.active = obj11

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj11.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 002 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj11
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj11.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 011 & curve 015 (the duplicated 002)
####################################################################

obj11.select_set(True)
bpy.ops.object.duplicate()
obj02 = bpy.context.scene.objects["Curve.002"]
bpy.ops.object.select_all(action="DESELECT")
obj11.select_set(True)
bpy.context.view_layer.objects.active = obj11
obj15.select_set(True)
bpy.context.view_layer.objects.active = obj15

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 005
############################

bpy.context.view_layer.objects.active = obj05
obj05.select_set(True)
bpy.ops.object.convert(target="MESH")
obj05.select_set(False)

D = bpy.data
obj05 = D.objects["Curve.005"]

# get vertices from object as list
vertices = list(obj05.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj05.select_set(True)
bpy.context.view_layer.objects.active = obj05

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj05.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj05.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 005 & curve 008
##############################################

obj05.select_set(True)
bpy.context.view_layer.objects.active = obj05

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj05.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 005 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj05
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj05.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 002 & curve 005
################################################

obj05.select_set(True)
bpy.ops.object.duplicate()
obj11 = bpy.context.scene.objects["Curve.011"]

bpy.ops.object.select_all(action="DESELECT")
obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02
obj05.select_set(True)
bpy.context.view_layer.objects.active = obj05

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 014
############################

bpy.context.view_layer.objects.active = obj14
obj14.select_set(True)
bpy.ops.object.convert(target="MESH")
obj14.select_set(False)

D = bpy.data
obj14 = D.objects["Curve.014"]

# get vertices from object as list
vertices = list(obj05.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj14.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj14.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 014 & curve 008
##############################################

obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj14.select_set(False)

obj08.select_set(True)
# bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 014 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj14
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj14.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 014 & curve 011 (duplicated 005)
##################################################################

obj14.select_set(True)
bpy.ops.object.duplicate()
obj02 = bpy.context.scene.objects["Curve.002"]

bpy.ops.object.select_all(action="DESELECT")
obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14
obj11.select_set(True)
bpy.context.view_layer.objects.active = obj11

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")

#####################################################################################################
# Fix the origin of curve 007
############################

bpy.context.view_layer.objects.active = obj07
obj07.select_set(True)
bpy.ops.object.convert(target="MESH")
obj07.select_set(False)

D = bpy.data
obj07 = D.objects["Curve.007"]

# get vertices from object as list
vertices = list(obj05.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert1.select = True
bpy.ops.object.editmode_toggle()
obj07.select_set(True)
bpy.context.view_layer.objects.active = obj07

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj07.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj07.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 007 & curve 008
##############################################

obj07.select_set(True)
bpy.context.view_layer.objects.active = obj07

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj07.select_set(False)

obj08.select_set(True)

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 014 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj07
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj07.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 007 & curve 002 (duplicated 014)
##################################################################

obj07.select_set(True)
bpy.ops.object.duplicate()
obj14 = bpy.context.scene.objects["Curve.014"]

bpy.ops.object.select_all(action="DESELECT")
obj07.select_set(True)
bpy.context.view_layer.objects.active = obj07
obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")
#####################################################################################################
# Fix the origin of curve 016
############################

bpy.context.view_layer.objects.active = obj16
obj16.select_set(True)
bpy.ops.object.convert(target="MESH")
obj16.select_set(False)

D = bpy.data
obj16 = D.objects["Curve.016"]

# get vertices from object as list
vertices = list(obj16.data.vertices)
# get first and last vertice from vertices list
vert1, vert2 = vertices[0], vertices[-1]

# read coordinates from vertice 1 and 2
print(vert1.co.xy, vert2.co.xy)

vert2.select = True
bpy.ops.object.editmode_toggle()
obj16.select_set(True)
bpy.context.view_layer.objects.active = obj16

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)

bpy.ops.object.editmode_toggle()
obj16.select_set(True)
bpy.ops.object.origin_set(type="ORIGIN_CURSOR", center="MEDIAN")
obj16.select_set(False)
bpy.ops.object.select_all(action="DESELECT")

# Set the same origin for curve 016 & curve 008
##############################################

obj16.select_set(True)
bpy.context.view_layer.objects.active = obj16

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj16.select_set(False)

obj08.select_set(True)

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj08.select_set(False)

# Apply curve Modifier to curve 016 on curve 008 and scale
########################################################

bpy.context.view_layer.objects.active = obj16
bpy.ops.object.modifier_add(type="CURVE")
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.008"]
obj16.scale.x = 22.12
bpy.ops.object.modifier_apply(modifier="Curve")
bpy.ops.object.select_all(action="DESELECT")

# Bridge edge loops between curve 007 & curve 002 (duplicated 014)
##################################################################

bpy.ops.object.select_all(action="DESELECT")
obj16.select_set(True)
bpy.context.view_layer.objects.active = obj16
obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action="SELECT")
bpy.ops.mesh.bridge_edge_loops()
bpy.ops.object.editmode_toggle()
bpy.ops.object.modifier_apply(modifier="bridge_edge_loops")
bpy.ops.object.select_all(action="DESELECT")

#####################################################################################################
##########################################################################################################################################################################################################
# Fix the mesh

# Select The wall 1

obj10.select_set(True)
bpy.context.view_layer.objects.active = obj10
obj03.select_set(True)
bpy.context.view_layer.objects.active = obj03
obj12.select_set(True)
bpy.context.view_layer.objects.active = obj12
obj18.select_set(True)
bpy.context.view_layer.objects.active = obj18
obj01.select_set(True)
bpy.context.view_layer.objects.active = obj01
obj04.select_set(True)
bpy.context.view_layer.objects.active = obj04
obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.remove_doubles(threshold=0.001)
bpy.ops.mesh.normals_make_consistent(inside=False)
bpy.ops.object.editmode_toggle()
bpy.ops.object.select_all(action="DESELECT")

# Fix the mesh

# Select The wall 2

obj09.select_set(True)
bpy.context.view_layer.objects.active = obj09
obj06.select_set(True)
bpy.context.view_layer.objects.active = obj06
obj15.select_set(True)
bpy.context.view_layer.objects.active = obj15
obj05.select_set(True)
bpy.context.view_layer.objects.active = obj05
obj11.select_set(True)
bpy.context.view_layer.objects.active = obj11
obj02.select_set(True)
bpy.context.view_layer.objects.active = obj02
obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14

bpy.ops.object.join()

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.remove_doubles(threshold=0.001)
bpy.ops.mesh.normals_make_consistent(inside=False)
bpy.ops.object.editmode_toggle()
bpy.ops.object.select_all(action="DESELECT")

# Boolean Operation
##################

# Set the origin of Wall 1

obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14
bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="MEDIAN")
bpy.ops.object.select_all(action="DESELECT")

# Add cube
bpy.ops.mesh.primitive_cube_add(
    size=2,
    enter_editmode=False,
    align="WORLD",
    location=(0.0244305, 8.58661, 10),
    scale=(1, 1, 1),
)
obj20 = bpy.context.scene.objects["Cube"]
bpy.ops.object.select_all(action="DESELECT")
# Set the same origin for cube and Wall 1

obj14.select_set(True)
bpy.context.view_layer.objects.active = obj14

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj14.select_set(False)

obj20.select_set(True)

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj20.select_set(False)

# Scale the cube

obj20.select_set(True)
obj20.scale.z = 4.94
obj20.scale.x = 4.77
obj20.scale.y = 3.14
bpy.ops.transform.translate(
    value=(0, 0, 0.35),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, False, True),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.ops.transform.translate(
    value=(-0.285, -0, -0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(True, False, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.context.view_layer.objects.active = obj20
bpy.ops.object.select_all(action="DESELECT")

# First boolean operation

obj20.select_set(True)
bpy.context.view_layer.objects.active = obj20
bpy.ops.object.modifier_add(type="BOOLEAN")
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Curve.014"]
bpy.context.object.modifiers["Boolean"].operation = "INTERSECT"
bpy.context.object.modifiers["Boolean"].solver = "EXACT"
bpy.context.object.modifiers["Boolean"].use_self = True

bpy.ops.object.modifier_apply(modifier="Boolean")
# bpy.data.scenes["Scene"].(null) = True

# Set the origin of Wall 2

obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13
bpy.ops.object.origin_set(type="ORIGIN_GEOMETRY", center="MEDIAN")
bpy.ops.object.select_all(action="DESELECT")

# Add cube
bpy.ops.mesh.primitive_cube_add(
    size=2,
    enter_editmode=False,
    align="WORLD",
    location=(0.0244305, 8.58661, 10),
    scale=(1, 1, 1),
)
obj21 = bpy.context.scene.objects["Cube.001"]
bpy.ops.object.select_all(action="DESELECT")
# Set the same origin for cube and Wall 1

obj13.select_set(True)
bpy.context.view_layer.objects.active = obj13

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_cursor_to_selected(override)
obj13.select_set(False)

obj21.select_set(True)

for area in bpy.context.screen.areas:
    if area.type == "VIEW_3D":
        override = bpy.context.copy()
        override["area"] = area
        override["region"] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor(override)
obj21.select_set(False)

# Scale the cube

obj21.select_set(True)
obj21.scale.z = 4.94
obj21.scale.x = 4.77
obj21.scale.y = 3.1
bpy.ops.transform.translate(
    value=(0, 0, 0.35),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, False, True),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.ops.transform.translate(
    value=(-0.285, -0, -0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(True, False, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.context.view_layer.objects.active = obj20
bpy.ops.object.select_all(action="DESELECT")

# Second boolean operation

obj21.select_set(True)
bpy.context.view_layer.objects.active = obj21
bpy.ops.object.modifier_add(type="BOOLEAN")
bpy.context.object.modifiers["Boolean"].object = bpy.data.objects["Curve.013"]
bpy.context.object.modifiers["Boolean"].operation = "DIFFERENCE"
bpy.context.object.modifiers["Boolean"].solver = "EXACT"
bpy.context.object.modifiers["Boolean"].use_self = True

bpy.ops.object.modifier_apply(modifier="Boolean")


bpy.ops.transform.translate(
    value=(0, 13.1969, 0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, True, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.ops.object.select_all(action="DESELECT")
obj20.select_set(True)
bpy.context.view_layer.objects.active = obj20
bpy.ops.transform.rotate(value=3.14159265, orient_axis="Y")
bpy.ops.object.select_all(action="DESELECT")
obj20.select_set(True)
bpy.context.view_layer.objects.active = obj20
bpy.ops.transform.translate(
    value=(0, 0, 0.467712),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, False, True),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
    release_confirm=True,
)
bpy.ops.transform.translate(
    value=(0, 0, 0.428736),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, False, True),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
    release_confirm=True,
)
bpy.ops.transform.translate(
    value=(0, 1.10006, 0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, True, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
    release_confirm=True,
)
bpy.ops.transform.translate(
    value=(-0, -0, -0.0250616),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, False, True),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
)
bpy.ops.transform.translate(
    value=(-1.47315, -0, -0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(True, False, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
    release_confirm=True,
)

obj21.select_set(True)
bpy.context.view_layer.objects.active = obj20

bpy.ops.object.join()
bpy.ops.transform.translate(
    value=(0, 6.85322, 0),
    orient_axis_ortho="X",
    orient_type="GLOBAL",
    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    orient_matrix_type="GLOBAL",
    constraint_axis=(False, True, False),
    mirror=False,
    use_proportional_edit=False,
    proportional_edit_falloff="SMOOTH",
    proportional_size=1,
    use_proportional_connected=False,
    use_proportional_projected=False,
    release_confirm=True,
)


"""
bpy.ops.mesh.extrude_manifold(MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":True, "mirror":False}, TRANSFORM_OT_translate={"value":(5.27461e-09, 1, 2.98023e-08), "orient_axis_ortho":'X', "orient_type":'GLOBAL', "orient_matrix":((-0.999681, 0.0247925, 0.00478722), (0.0129961, 0.342643, 0.939376), (0.0216491, 0.939138, -0.342856)), "orient_matrix_type":'NORMAL', "constraint_axis":(True, True, True), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_elements":{'INCREMENT'}, "use_snap_project":False, "snap_target":'CLOSEST', "use_snap_self":True, "use_snap_edit":True, "use_snap_nonedit":True, "use_snap_selectable_only":False, "use_snap_to_same_target":False, "snap_face_nearest_steps":1, "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":True, "use_accurate":False, "use_automerge_and_split":True})
"""

"""
snap_selected_to_cursor

#bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.editmode_toggle()
bpy.ops.mesh.select_all(action='SELECT')
obj = bpy.context.object

for vert in obj.data.vertices:
    if 1 > vert.location.x > 0 and 1 > vert.location.y > 0:        vert.select = true
"""

"""
obj20.select_set(True)
bpy.context.view_layer.objects.active = obj20

for area in bpy.context.screen.areas:
    if area.type == 'VIEW_3D':
        override = bpy.context.copy()
        override['area'] = area
        override['region'] = area.regions[4]
        bpy.ops.view3d.snap_selected_to_cursor( override, use_offset=False)
        
obj13.select_set(True)
bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
obj13.select_set(False)        

bpy.context.view_layer.objects.active = obj13
bpy.ops.object.modifier_add(type='CURVE')
bpy.context.selected_objects
bpy.context.object.modifiers["Curve"].object = bpy.data.objects["Curve.020"]        
"""

# obj13.select_set(True)
# bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='MEDIAN')
# obj13.select_set(False)
# if bpy.context.active_object is not None:
# bpy.context.scene.cursor_location = bpy.context.active_object.location
# bpy.ops.view3d.snap_cursor_to_active(3)

# obj13.select_set(True)
# bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')
# obj13.select_set(False)

##############################
# CONVERT EACH CURVE TO MESH #
##############################

"""
for obj in bpy.data.objects:
    print(obj)
    bpy.context.view_layer.objects.active = obj
    obj.select_set(True)
    bpy.ops.object.convert(target='MESH')
    obj.select_set(False)



####################
# SELECT VERTICES  #
####################
'''
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.join()

obj = bpy.context.object

for vert in obj.data.vertices:
    if 1 > vert.location.y >0 and 2>vert.location.z > 7:
           vert.select = true




'''
###############################
# LOFT WITH BRIDGE EDGE LOOPS #
###############################



obj0.select_set(True)
#bpy.ops.object.duplicate()
#courbeBasse = bpy.context.object
obj08.select_set(True)
#bpy.ops.object.duplicate()
bpy.ops.object.join()
#courbeHaute = bpy.context.object
obj15.select_set(False)



#bpy.ops.obj.select_all(action='DESELECT')
bpy.ops.outliner.item_activate(deselect_all=True)
bpy.ops.object.editmode_toggle()

obj15.select_set(False)
obj0.select_set(True)
obj08.select_set(True)
#bpy.ops.vert.select_all(action='SELECT')
'''
bpy.ops.mesh.bridge_edge_loops(type='SINGLE', use_merge=False, merge_factor=0.5, twist_offset=0, number_cuts=0, interpolation='PATH', smoothness=1.0, profile_shape_factor=0.0, profile_shape='SMOOTH')
'''
'''
bpy.ops.object.select_all(action='DESELECT')
'''

'''
obj08.select_set(True)
bpy.ops.object.duplicate()
courbeHaute = bpy.context.object
bpy.ops.object.select_all(action='DESELECT')

print('ouoiujoiuoiuoiuoiuo',courbeHaute)

courbeBasse.select_set(True)
courbeHaute.select_set(True)
bpy.ops.mesh.bridge_edge_loops(type='SINGLE', use_merge=False, merge_factor=0.5, twist_offset=0, number_cuts=0, interpolation='PATH', smoothness=1.0, profile_shape_factor=0.0, profile_shape='SMOOTH')

"""
