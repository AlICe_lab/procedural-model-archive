# SPDX-FileCopyrightText: 2013 Gennaro Notaro
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# blender 2.68a / MS windows
# cours du 03102013
# blender 2.68a / MS windows METABOLIST MOVEMENT
import bpy
import random

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)

#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#
#'LES FORMES'#
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#


def matour(
    posx, posy, posz, dimx, dimy, dimz, nom
):  # matour: nom d'un objet que l'on défini (def) par sa position, ses dimensions et son nom
    bpy.ops.mesh.primitive_cylinder_add(
        location=(posx, posy, posz)
    )  # c'est un cylindre que l'on ajoute à l'espace définit par sa localisation en x,y,z
    bpy.ops.transform.resize(
        value=(dimx, dimy, dimz)
    )  # la taille de ce cylindre sera définit par dimz,dimy,dimz
    bpy.ops.transform.resize(
        value=(5, 5, 1)
    )  # la dimension sur l'axe x et y sont contante, on peut leur attribuer 5sur x et 5 sur y)
    bpy.context.object.name = (
        nom  # ces options donneront la posiblité de nommer les tours
    )
    bpy.context.object.show_name = (
        True  # show_name = True, montré les noms = Vrai -->donc Oui
    )


def boite(
    posx, posy, posz, dimx, dimy, dimz
):  # boite: nom d'un objet qui fera les couloirs et les habitations
    bpy.ops.mesh.primitive_cube_add(
        location=(posx, posy, posz + 28)
    )  # la localisation de ses boites est définit ici, on rajoute 28 car dans le projet d'Isozaki les couloirs se situent à 28mètre au dessus du sol
    bpy.ops.transform.resize(
        value=(dimx, dimy, dimz)
    )  # on défini de nouveau les dimension
    bpy.ops.transform.resize(
        value=(1, 1, 1.5)
    )  # on définit ici les dimensions qui seront modifié dans les options


#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#
#'LES VARIABLES'#
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#

NbrtourX = random.randint(
    3, 5
)  # on peut définir et changer le nombre de tour en X et en Y, en changeant simplement les données ici
NbrtourY = random.randint(
    3, 5
)  #!! ne pas mettre de chiffre trop grand sinon la machine se crash!!

#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#
#'LES DEFINITIONS'#
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#
print("---------------")


def configArchi():

    ####OPTION DES TOURS####                                                #On commence par définir les tours qui influenceront tout le reste du calcule

    for i in range(
        0, NbrtourX
    ):  # NbrtourX: Nombre de tour en X au hasard voir random.randint plus haut
        for j in range(
            0, NbrtourY
        ):  # NbrtourX: Nombre de tour en Y au hasard voir random.randint plus haut
            global Hauteur  # global: pour avoir un effet de la Hauteur dans toute les options
            Hauteur = random.choice(
                [0, 0, 15, 21, 27, 33, 39, 45]
            )  # le random.choice: permet de choisir une hauteur au hasard dans ces chiffres
            global Nbrcouloir
            Nbrcouloir = int(
                ((Hauteur - 15) / 6) + 1
            )  # Nbrcouloir: Nombre de couloir adapté à la hauteur par une simple formule de math !!int: est pour arrondir à un nombre entier
            if Hauteur == 45:  # if: si la Hauteur est égal à 45
                Choix_H = random.choice(
                    [1, 1.5]
                )  # Une chance sur deux d'avoir une hauteur de tour double car mutliplié par 1.5
                matour(
                    i * 120,
                    j * 120,
                    Hauteur * Choix_H,
                    1,
                    1,
                    1 * Hauteur * (Choix_H),
                    "x" + str(i + 1) + "y" + str(j + 1),
                )  # formule ou l'on prend les options de'matour' et on les manipules
                print("---------------")
                print("Hauteur de la tour =", Hauteur * (Choix_H) * 2, "mètres")
                print(
                    "---------------"
                )  #'x'+str(i+1)+'y'+str(j+1) : on nomme les boites grâce à cette option
            else:  # esle: sinon (si on applique pas la formule pour 45 (plus haut), on utlise cette formule
                matour(
                    i * 120,
                    j * 120,
                    Hauteur,
                    1,
                    1,
                    1 * Hauteur,
                    "x" + str(i + 1) + "y" + str(j + 1),
                )  # l'autre formule pour toute les autres tours qui ne font pas 45 de Hauteur
                print("---------------")
                print("Hauteur de la tour =", Hauteur * 2, "mètres")
                print("---------------")
            ####OPTION DES COULOIRS####

            for l in range(
                0, Nbrcouloir
            ):  # Nbre de couloir est adapté à la hauteur(voir formule plus haut) et nous donne un chiffre entier, le range va de 0 à ce chiffre entier maximum
                global Lgrcouloir  # global : pour que la longueur de couloir puisse être utilisé dans les autres option
                Lgrcouloir = int(Nbrcouloir * 10)
                if Hauteur == 45:  # if:si la hauteur est de 45:
                    boite(
                        i * 120, j * 120, l * 6, 1 * 3, (l * 10) + 10, 1
                    )  # cette formule vient prendre la 'boite' et la placer et modifier dans l'espace !!La boite ici est un couloir et non une cellule d'habitation
                    print(
                        "Longueur de couloir ", (l * 10) + 10
                    )  # l*6 est la distance sur l'axe des x adapté au range et éspacé de 6
                    # (l*10)+10 est la modification sur l'axe des y et dans la formule suivante sur l'axe des x
                    # le faite de la multipler par le range(l) donne des boites de longueurs différentes selon la hauteur car elles sont influencées par le nombre de couloir(voir Nbrcouloir)
                    boite(
                        i * 120, j * 120, l * 6, (l * 10) + 10, 1 * 3, 1
                    )  # ici on déplace juste le (l*10)+10 pour modifier la boite sur l'axe x
                    if (
                        Choix_H == 1.5
                    ):  # si le choix de la hauteur est de 1.5, il faut penser que les couloirs doivent être placé également au dessus de la tour
                        boite(
                            i * 120, j * 120, (l * 6) + 60, 1 * 3, (l * 10) + 10, 1
                        )  # on rajoute 60 à sa situation dans l'espace sur l'axe z(la hauteur)
                        boite(i * 120, j * 120, (l * 6) + 60, (l * 10) + 10, 1 * 3, 1)
                        print("Longueur de couloir ", (l * 10) + 10)
                else:  # sinon : dans les cas ou la tour n'est pas 45
                    boite(
                        i * 120, j * 120, l * 6, 1 * 3, (l * 10) + 10, 1
                    )  # la formule s'adapte automatiquement comme dans le cas de la tour à 45 non doublé
                    boite(i * 120, j * 120, l * 6, (l * 10) + 10, 1 * 3, 1)
                    print("Longueur de couloir ", (l * 10) + 10)
                ####OPTION DES CELLULES D'HABITATIONS####

                for n in range(0, 1):
                    Nbrcellule = (
                        l + 1
                    )  # Nombre de cellule: c'est le range de l plus haut(donc adapté au nombre de couloir)+1
                    for o in range(
                        0, Nbrcellule
                    ):  # o: Nouveau range entre 0 et le Nombre de cellule adapté au nombre de couloir
                        if (
                            Hauteur == 45
                        ):  # si la Hauteur des de 45 ( deux possibilité de nouveau soit doublé, soit normal)
                            boite(
                                i * 120,
                                (j * 120 + ((o * (Lgrcouloir) / 6 + 8))),
                                (l * 6) + 3,
                                7,
                                3,
                                1,
                            )  # ici les formule si la tour fait 45, on additionne la paranthèse qui multiplie le range o et la longueur de couloir qui est divisé par 6 puis on rajoute 8 pour les décaler
                            boite(
                                i * 120,
                                (j * 120 - ((o * (Lgrcouloir) / 6 + 8))),
                                (l * 6) + 3,
                                7,
                                3,
                                1,
                            )  # la même chose cette fois on soustrait car on recule sur l'axe y
                            boite(
                                (i * 120 + ((o * (Lgrcouloir) / 6 + 8))),
                                j * 120,
                                (l * 6) + 3,
                                3,
                                7,
                                1,
                            )  # la même chose mais avec l'axe x cette fois
                            boite(
                                (i * 120 - ((o * (Lgrcouloir) / 6 + 8))),
                                j * 120,
                                (l * 6) + 3,
                                3,
                                7,
                                1,
                            )

                            if Choix_H == 1.5:
                                boite(
                                    i * 120,
                                    (j * 120 + ((o * (Lgrcouloir) / 6 + 8))),
                                    ((l * 6) + 3) + 60,
                                    7,
                                    3,
                                    1,
                                )  # on rajoute 60 pour décalé la hauteur de 60mètres sur l'axe z
                                boite(
                                    i * 120,
                                    (j * 120 - ((o * (Lgrcouloir) / 6 + 8))),
                                    ((l * 6) + 3) + 60,
                                    7,
                                    3,
                                    1,
                                )
                                boite(
                                    (i * 120 + ((o * (Lgrcouloir) / 6 + 8))),
                                    j * 120,
                                    ((l * 6) + 3) + 60,
                                    3,
                                    7,
                                    1,
                                )
                                boite(
                                    (i * 120 - ((o * (Lgrcouloir) / 6 + 8))),
                                    j * 120,
                                    ((l * 6) + 3) + 60,
                                    3,
                                    7,
                                    1,
                                )

                        else:
                            boite(
                                i * 120,
                                (
                                    j * 120
                                    + (((o * (Lgrcouloir) / (Lgrcouloir / 10)) + 9))
                                ),
                                (l * 6) + 3,
                                7,
                                3,
                                1,
                            )  # même calcule mais pour toute les autres tours, ici on divise par 10 la longueur et décale de 9
                            boite(
                                i * 120,
                                (
                                    j * 120
                                    - (((o * (Lgrcouloir) / (Lgrcouloir / 10)) + 9))
                                ),
                                (l * 6) + 3,
                                7,
                                3,
                                1,
                            )  # tout ces calcules sont le fruits de beaucoup d'essais, il peuvent parâitre compliqué, ils sont pour le moins tous logique
                            boite(
                                (
                                    i * 120
                                    + (((o * (Lgrcouloir) / (Lgrcouloir / 10)) + 9))
                                ),
                                j * 120,
                                (l * 6) + 3,
                                3,
                                7,
                                1,
                            )
                            boite(
                                (
                                    i * 120
                                    - (((o * (Lgrcouloir) / (Lgrcouloir / 10)) + 9))
                                ),
                                j * 120,
                                (l * 6) + 3,
                                3,
                                7,
                                1,
                            )

                print("Nombre de celulle sur le couloir ", Nbrcellule)


#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''#
#'LA CONSOLE'#
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''# +
print("---------------")
print("NOMBRE DE TOUR =", NbrtourX * NbrtourY)
print("---------------")
configArchi()  # Fin de la configuration architecturale

bpy.ops.object.select_all(
    action="DESELECT"
)  # déselectionner tous les objets (pour en prendre certains
L_SELECT = [
    random.randint(1, NbrtourX),
    random.randint(1, NbrtourY),
    random.randint(1, NbrtourX),
    random.randint(1, NbrtourY),
]  # liste avec 4 random qui varient de 1 au Nombre de tour en X et Y

bpy.data.objects["x" + str(L_SELECT[0]) + "y" + str(L_SELECT[1])].select = (
    True  # sélection dans le maillage en x et y à l'aide de la liste
)
bpy.data.objects["x" + str(L_SELECT[2]) + "y" + str(L_SELECT[3])].select = (
    True  # sélection dans le maillage en x et y à l'aide de la liste
)
