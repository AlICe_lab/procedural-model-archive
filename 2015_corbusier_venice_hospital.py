# SPDX-FileCopyrightText: 2015 Marc-Antoine Froger
# SPDX-FileCopyrightText: 2024 AlICe laboratory <https://alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Developpe sous Blender 2.76 Hash: f3377fea / Windows 7
# Script base sur le projet de l'hopital de Venise de Le Corbusier

import bpy
import random
from random import *

bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False)


# Liste des mesures principales

Cote_principale = 56.4
Cote_circulation = 13
Cote_Unite_de_soin = 9.5
Cote_Couloir = 3.5
Cote_Chambre = 21.7

# Créer la liste qui comprend toutes les positions de la trame

coordonnees = []
posCell = []
for x in range(0, 5):
    for y in range(0, 5):
        posCell.append((x, y, 0))


# Créer deux listes une contient le module classique la seconde le module inversé

posCellClassic = []
posCellInvers = []
for coord in posCell:
    x, y, z = coord
    if x % 2 == 0 and y % 2 == 0 or x % 2 == 1 and y % 2 == 1:
        posCellClassic.append(coord)
    else:
        x = x * Cote_principale
        y = y * Cote_principale
        posCellInvers.append((x, y, z))


# Creer la fonction cube initiale


def unCube(position, dimension):
    x, y, z = position
    dx, dy, dz = dimension
    bpy.ops.mesh.primitive_cube_add(
        radius=0.5, location=(x + dx / 2, y + dy / 2, z + dz / 2)
    )
    bpy.ops.transform.resize(value=dimension)


# Définition des modules à générer


def module(pos_init):

    if posCellInvers.count(pos_init) == 1:
        inversion = Cote_principale - Cote_Chambre

    else:
        inversion = 0

    # Variables initiale

    x, y, z = pos_init
    X = x * Cote_principale
    Y = y * Cote_principale

    etage = 5

    # Localisation des positions de chaque quadrant :

    # Premier quadrant

    Pos_Circu_1 = (
        Cote_principale / 2 - Cote_circulation / 2 + x,
        Cote_principale / 2 - Cote_circulation / 2 + y,
        0 + z,
    )
    Pos_Soin_1 = (Cote_Chambre + x, 0 + y + inversion, 0 + z)
    Pos_Couloir_1 = ((Cote_Unite_de_soin + Cote_Chambre) + x, 0 + y + inversion, 0 + z)
    Pos_Chambre_1 = (0 + x, 0 + y, 0 + z)

    # Deuxième quadrant

    Pos_Soin_2 = (
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + x - inversion,
        Cote_Chambre + y,
        0 + z,
    )
    Pos_Couloir_2 = (
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + x - inversion,
        (Cote_Chambre + Cote_Unite_de_soin) + y,
        0 + z,
    )
    Pos_Chambre_2 = (
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + x,
        0 + y,
        0 + z,
    )

    # Troisième quadrant

    Pos_Soin_3 = (
        (Cote_Chambre + Cote_Couloir) + x,
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + y - inversion,
        0 + z,
    )
    Pos_Couloir_3 = (
        (Cote_Chambre) + x,
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + y - inversion,
        0 + z,
    )
    Pos_Chambre_3 = (
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + x,
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + y,
        0 + z,
    )

    # Quatrième quadrant

    Pos_Soin_4 = (0 + x + inversion, (Cote_Chambre + Cote_Couloir) + y, 0 + z)
    Pos_Couloir_4 = (0 + x + inversion, Cote_Chambre + y, 0 + z)
    Pos_Chambre_4 = (
        0 + x,
        (Cote_Chambre + Cote_Unite_de_soin + Cote_Couloir) + y,
        0 + z,
    )

    # Définition de la dégradation des éléments inférieur en fonction des élémpents supérieur:

    def pil(position, dimx, dimy):
        for i in range(0, etage):
            if i == 0:
                liste = []
            x, y, z = position
            position_z = x, y, z - (i * 4)
            rand = choice([True, True, False])
            liste.append(rand)

            if liste.count(False) >= 1:
                liste.append(False)
                pass

            else:
                unCube(position_z, (dimx, dimy, 4))

    pil(Pos_Circu_1, Cote_circulation, Cote_circulation)

    pil(Pos_Soin_1, Cote_Unite_de_soin, Cote_Chambre)
    pil(Pos_Soin_2, Cote_Chambre, Cote_Unite_de_soin)
    pil(Pos_Soin_3, Cote_Unite_de_soin, Cote_Chambre)
    pil(Pos_Soin_4, Cote_Chambre, Cote_Unite_de_soin)

    pil(Pos_Couloir_1, Cote_Couloir, Cote_Chambre)
    pil(Pos_Couloir_2, Cote_Chambre, Cote_Couloir)
    pil(Pos_Couloir_3, Cote_Couloir, Cote_Chambre)
    pil(Pos_Couloir_4, Cote_Chambre, Cote_Couloir)

    pil(Pos_Chambre_1, Cote_Chambre, Cote_Chambre)
    pil(Pos_Chambre_2, Cote_Chambre, Cote_Chambre)
    pil(Pos_Chambre_3, Cote_Chambre, Cote_Chambre)
    pil(Pos_Chambre_4, Cote_Chambre, Cote_Chambre)


posCellCalibree = []


for coord in posCell:
    x, y, z = coord
    X = x * Cote_principale
    Y = y * Cote_principale

    pos_init = (X, Y, 0)
    posCellCalibree.append(pos_init)


# Supression alétoire d'une matrice complette dans la trame

posCellCalibree[:] = (i for i in posCellCalibree if i != choice(posCellCalibree))

posCellCalibree[:] = (i for i in posCellCalibree if i != choice(posCellCalibree))

posCellCalibree[:] = (i for i in posCellCalibree if i != choice(posCellCalibree))


for coord in posCellCalibree:

    module(coord)
